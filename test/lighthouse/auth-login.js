class AuthLogin {
  getUrls() {
    return [
      // Top 5 visited pages by WOGAA
      'https://employer-qa.ci.mcf.sh/',
      'https://employer-qa.ci.mcf.sh/jobs',
      'https://employer-qa.ci.mcf.sh/jobs/new',
      'https://employer-qa.ci.mcf.sh/session-timeout',
      'https://employer-qa.ci.mcf.sh/unauthorised',
      // 'https://employer-qa.ci.mcf.sh/talent-search',
    ];
  }

  connect(browser) {
    return new Promise(async (resolve) => {
      const page = await browser.newPage();
      await page.setDefaultNavigationTimeout(0);
      await page.goto('https://employer-qa.ci.mcf.sh/');
      // Click login button on home page
      await page.waitForSelector('button[data-cy="main-login-button"]');
      await page.click('button[data-cy="main-login-button"]');
      // Select an Entity ID and login
      await page.waitForTimeout(1000);
      await page.click('button[name="select"]');
      await page.waitForTimeout(1000);
      await page.click('button[type="submit"]');
      await page.waitForNavigation({waitUntil: 'networkidle0'});
      page.close();
      resolve(browser);
    });
  }
}

module.exports = new AuthLogin();
