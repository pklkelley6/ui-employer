module.exports = {
  plugins: [
    require('postcss-mixins'),
    require('postcss-preset-env')({
      stage: 0, // for nesting-rules
    }),
    require('postcss-inline-svg'),
    process.env.NODE_ENV !== 'development' ? require('cssnano') : null,
    require('postcss-reporter'),
  ].filter(Boolean), // remove null values
};
