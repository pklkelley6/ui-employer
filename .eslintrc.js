module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  parser: '@typescript-eslint/parser',
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:jest/recommended',
    'plugin:cypress/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: './tsconfig.json',
  },
  plugins: ['@typescript-eslint', 'cypress', 'jest', 'react', 'graphql', 'import'],
  rules: {
    'import/order': 'error',
    'no-console': ['error', {allow: ['error']}],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-unused-expressions': 'error',

    // unused variables
    '@typescript-eslint/no-unused-vars': [
      'error',
      {
        ignoreRestSiblings: true,
        argsIgnorePattern: '^_',
        varsIgnorePattern: '^_',
      },
    ],

    // race conditions
    'require-atomic-updates': 'warn',

    // switch statements
    'no-case-declarations': 'error',

    // lots of false positives
    '@typescript-eslint/no-use-before-define': ['error', {functions: false, variables: false, classes: true}],

    // don't enforce interface names need to start with I
    '@typescript-eslint/interface-name-prefix': 'off',

    // type inference and safety
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-explicit-any': 'off',

    //formatting / style
    'no-useless-escape': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'default',
        format: ['camelCase'],
        leadingUnderscore: 'allow',
        trailingUnderscore: 'allow',
        filter: {
          regex: '^__',
          match: false,
        },
      },
      {
        selector: 'variableLike',
        format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
        leadingUnderscore: 'allow',
        trailingUnderscore: 'allow',
        filter: {
          regex: '^__',
          match: false,
        },
      },
      {
        selector: 'method',
        format: ['camelCase', 'PascalCase'],
      },
      {
        selector: 'typeLike',
        format: ['PascalCase'],
      },
      {
        selector: ['enum', 'enumMember'],
        format: ['UPPER_CASE', 'PascalCase'],
      },
      {
        selector: 'interface',
        format: ['PascalCase'],
        prefix: ['I'],
      },
      {
        selector: ['objectLiteralProperty'],
        format: null,
        modifiers: ['requiresQuotes'],
      },
    ],

    semi: 'error',

    // prefer const
    'prefer-const': 'warn',

    // prefer rest params
    'prefer-rest-params': 'warn',

    // empty blocks and error handling
    '@typescript-eslint/no-empty-function': ['error', {allow: ['arrowFunctions']}],

    // Explicit types for function return values and arguments
    '@typescript-eslint/explicit-module-boundary-types': 'off',

    '@typescript-eslint/ban-types': 'warn',

    // jest
    'jest/expect-expect': [
      'error',
      {assertFunctionNames: ['expect', 'testSaga', 'expectSaga', 'expectGAToBeCalledWith', '*.verifyInteractions']},
    ],
    'jest/no-mocks-import': 'off',
    'jest/no-identical-title': 'warn',
    'jest/valid-expect': 'warn',
    'jest/no-export': 'warn',
    'jest/no-standalone-expect': 'warn',

    // react
    'react/prop-types': 'off',
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'react/jsx-no-undef': 'error',

    //graphql
    'graphql/template-strings': [
      'error',
      {env: 'apollo', schemaJson: require('./src/graphql/__generated__/schema.json')},
    ],
  },
  overrides: [
    {
      files: ['cypress/**/*.js'],
      rules: {
        '@typescript-eslint/no-unused-expressions': 'off',
        '@typescript-eslint/naming-convention': 'off',

        'jest/expect-expect': 'off',
        'jest/valid-expect': 'off',
        'jest/no-standalone-expect': 'off',
        'jest/valid-expect-in-promise': 'off',
        'jest/no-conditional-expect': 'off',

        'cypress/no-unnecessary-waiting': 'warn',
      },
    },
  ],
};
