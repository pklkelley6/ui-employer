const path = require('path');
const webpack = require('webpack');

const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {config} = require('./src/config/build');

const IS_DEV = process.env.NODE_ENV === 'development';
const IS_PROD = !IS_DEV;

const es6Dependencies = [
  'file-type',
  'readable-web-to-node-stream',
  'token-types',
  'strtok3',
  'peek-readable',
  'debug',
];

// tslint:disable object-literal-sort-keys
module.exports = {
  // Defaults to development, pass --mode production to override
  mode: 'development',

  context: path.resolve(__dirname),

  target: 'web',

  entry: {
    config: './src/static/configuration.js',
    analyticsScript: './src/static/analyticsScript.js',
    app: [
      '@babel/polyfill',
      'events-polyfill',
      'whatwg-fetch',
      'abortcontroller-polyfill/dist/polyfill-patch-fetch',
      './src/index.tsx',
    ],
  },

  output: {
    filename: (chunkData) => {
      return chunkData.chunk.name === 'config' ? '[name].js?[hash:7]' : '[name].[hash:7].js';
    },
    path: path.resolve(__dirname, 'dist'),
    // ./ is used when hosting in a subdirectory (eg. GitHub pages)
    publicPath: config.publicPath,
  },

  module: {
    rules: [
      // apollo is exporting mjs which are handled by Webpack and causes a lot of error
      // temporary fix should be removed ASAP because we shouldnt need this
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      // Vanilla CSS
      {
        test: /\.css$/,
        loaders: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: IS_DEV,
            },
          },
          'css-loader',
        ],
        include: /node_modules/,
      },
      {
        test: /\.s?css$/,
        loaders: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: IS_DEV,
            },
          },
          {
            loader: 'css-loader',
            options: {
              localsConvention: 'camelCase',
              modules: {
                localIdentName: '[name]__[local]___[hash:base64:5]',
              },
              importLoaders: 1,
            },
          },
          'postcss-loader',
        ],
        include: path.resolve(__dirname, 'src'),
      },
      {
        test: /\.(jpg|png|gif|mp4|webm|mp3|ogg|svg)$/,
        loader: 'file-loader',
        options: {
          name: './f/[hash:16].[ext]',
        },
      },
      // transpile dependencies which uses ES6 or later syntax to make them work on older browsers such as IE
      {
        test: /\.js?$/,
        exclude: new RegExp(`node_modules(?!\/(${es6Dependencies.join('|')}))`),
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /\/node_modules\//,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          babelrc: false,
          plugins: [
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-optional-chaining',
            '@babel/plugin-proposal-nullish-coalescing-operator',
            'react-hot-loader/babel',
          ],
          presets: ['@babel/preset-env', '@babel/preset-typescript', '@babel/preset-react'],
        },
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      __WEBPACK_DEFINE_CONFIG_JS_OBJ__: JSON.stringify(config),
    }),
    new ForkTsCheckerWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      favicon: './src/static/favicon.ico',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[hash:7].css',
    }),
    // Generate .gz for production builds
    // Consider adding brotli-webpack-plugin if your server supports .br
    ...(IS_PROD
      ? [
          new CompressionPlugin({test: /((?<!config)\.js|\.(css|html|svg))$/}),
          new BrotliPlugin({test: /((?<!config)\.js|\.(css|html|svg))$/}),
          new CopyWebpackPlugin([{from: 'src/static/robots.txt', to: ''}]),
        ]
      : []),
  ],

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /\/node_modules\//,
          filename: 'vendor.[hash:7].js',
          name: 'vendor',
          chunks: 'all',
        },
      },
    },
  },

  // Using cheap-eval-source-map for build times
  // switch to inline-source-map if detailed debugging needed
  devtool: IS_PROD ? false : 'cheap-eval-source-map',

  devServer: {
    compress: true,
    disableHostCheck: true,
    historyApiFallback: true,
    host: '0.0.0.0',
    hot: true,
    inline: true,
    port: 3500,
    stats: {
      colors: true,
      progress: true,
    },
  },

  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },

  resolve: {
    extensions: ['.js', '.ts', '.tsx'], // TODO cant remove js it s failing
    modules: ['node_modules', path.resolve(__dirname, 'src')],
    alias: {
      '~': path.resolve(__dirname, 'src'),
      'react-dom': '@hot-loader/react-dom',
    },
  },
  bail: true,
};
