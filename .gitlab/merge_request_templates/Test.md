<!-- Make sure Merge Request title following "<type>: <subject> [#<pivotal_story_id>]" -->

### General Checklist:

- [ ] One `describe` per file;
- [ ] File name should reflect describe title;
- [ ] Do not use word `test` in filename, example are `*test.spec.js`;
- [ ] Put in copy at least two potential reviewers

/cc

/label ~"Review Me"
/label ~"Test Me"

[![pipeline status](https://gitlab.com/mycf.sg/ui-employer/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/ui-employer/-/commits/master)
