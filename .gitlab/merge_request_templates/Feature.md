Pivotal Story:

<!-- Make sure Merge Request title following "<type>: <subject> [#<pivotal_story_id>]" -->

<!-- Brief description. Examples are, screenshots, steps to reproduce, links to dependent MRs -->

### General Checklist:

- [ ] Don't forget to check changes in IE11, please refer to https://gitlab.com/mycf.sg/ui-employer/-/issues/62
- [ ] Exact versions in package.json
- [ ] Testing instructions?
- [ ] Docs? Examples are, update `README.md` file, or add ADR in `doc/architecture/decisions`
- [ ] Tests?
- [ ] Put in copy at least two potential reviewers

/cc

/label ~"Review Me"

[![pipeline status](https://gitlab.com/mycf.sg/ui-employer/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/ui-employer/-/commits/master)
