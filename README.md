# Employer UI [React]

This repository contains the front-end view layer for MyCareersFuture Employer Site.

The QA version can be found here https://employer-qa.ci.mcf.sh/

| **Presentation, state**                                                   |                              |
| ------------------------------------------------------------------------- | ---------------------------- |
| [react](https://facebook.github.io/react/docs/hello-world.html)           | ui framework                 |
| [redux](http://redux.js.org/)                                             | state management             |
| [react-redux](http://redux.js.org/docs/basics/UsageWithReact.html)        | react-redux integration      |
| [react-router-redux](https://github.com/reactjs/react-router-redux)       | routing                      |
| [typesafe-actions](https://github.com/piotrwitek/typesafe-actions)        | async actions                |
| [emotion](https://github.com/emotion-js/emotion)                          | css-in-javascript, styling   |
| [plain css](https://developer.mozilla.org/en-US/docs/Web/CSS)             | legacy css escape hatch      |
| **Testing, linting**                                                      |                              |
| [jest](https://jestjs.io/docs/en/api)                                     | test framework               |
| [enzyme](http://airbnb.io/enzyme/index.html)                              | react test library           |
| [cypress](https://docs.cypress.io/guides/overview/why-cypress.html)       | integration test library     |
| [pact](https://docs.pact.io/)                                             | contract test for HTTP       |
| [typescript](https://www.typescriptlang.org/docs/home.html)               | type checking                |
| [eslint](http://eslint.org/docs/rules/)                                   | javascript linting           |
| [prettier](https://github.com/prettier/prettier/)                         | (type/java)script formatting |
| [stylelint](https://stylelint.io/user-guide/)                             | legacy css linting           |
| [karma](http://karma-runner.github.io/1.0/config/configuration-file.html) | test runner (with electron)  |
| **Building, CI, deploying**                                               |                              |
| [webpack](https://webpack.js.org/concepts/)                               | javascript bundler           |
| [docker](https://docs.docker.com/engine/reference/builder/)               | container                    |
| [docker-compose](https://docs.docker.com/compose/compose-file/)           | multi-container              |
| [travis](https://docs.travis-ci.com/user/customizing-the-build)           | ci                           |

[Ditherer](https://github.com/gyng/ditherer) is a project built using an older version of this boilerplate.

## Usage

### Getting Started

To get started with default setings run:

1. Running ui-employer locally

   ```sh
   yarn dev:init;
   ```

The above will trigger the build, install and start steps as follows:

- `yarn dev:build` to initialise Docker images
- `yarn dev:install` to install dependencies
- `yarn dev:start` to start with development environment
- `yarn dev:clean` to clear everything related to this

The development environment is accessible at [http://localhost:3500](http://localhost:3500).

2. Besides running ui-employer locally, we will also need to set up other services:

- [ops-base](https://gitlab.ci.mcf.sh/wsg/ops-base) - bridging service that helps link up all the different services

- [api-job](https://gitlab.ci.mcf.sh/wsg/api-job) - ui-employer service calls this API to get data related to jobs. See [README.md](https://gitlab.ci.mcf.sh/wsg/api-job/blob/master/readme.md) on how to start `api-job` project. If you cannot see data from `api-job`, that might be because it's configured to see port `:3500`. In this case, please configure `api-job`.

- [api-profile](https://gitlab.ci.mcf.sh/wsg/api-profile) - ui-employer service calls this API to get data related to user profiles

```
# in /path/to/ops-base
make dev

# in /path/to/api-job
yarn dev:start

# in /path/to/api-profile
yarn dev:start
```

### Detailed Scripts

#### Build

    yarn install
    yarn build                      # test build, builds in /dist
    yarn build:prod                 # production build, builds in /dist

#### Test

    yarn test                       # runs unit tests once
    yarn test:watch                 # runs unit tests using karma in watch mode
    yarn cypress:open               # runs end to end integration tests
    yarn lint                       # runs eslint, stylelint
    yarn lint:fix
    yarn eslint
    yarn prettier                   # prettier style enforced by eslint
    yarn prettier:fix
    yarn stylelint

#### Develop

    yarn dev:start                  # runs webpack-dev-server (yarn dev) or use
    yarn test:watch                 # runs unit tests using karma in watch mode

## Graphql

Graphql schema and types are automatically generated thanks to `apollo-cli` and `graphql-codegen`. Schema is fetched from QA environment and types are generated from the downloaded schema. To generate them, run `yarn dev:install`.

As for node_modules, in case of project updates, developers may need to updates types and schema accordingly. If that happen, just run `yarn dev:install`.

When developing and creating queries, developers will need to generate new types matching new / updated queries. If that happen, just run `yarn dev:install` or use file watchers from your IDE to watch for files containing graphql queries and run `yarn dev:install`.

## ADR docs

We started to record decision which we made during development of this project. Please have a look, and you may find them in `doc/adr` folder. More details in `doc/adr/0001-record-architecture-decisions.md`.

In order to use it, please install `adr-tools` https://github.com/npryce/adr-tools/blob/master/INSTALL.md.

Examples are:

1. Create Architecture Decision Records

   ```sh
   adr new Implement as Unix shell scripts
   ```

2. For further information, use the built in help:

   ```sh
   adr help
   ```

## Troubleshooting

### Versioning is failing

Versioning use the latest tag to determine the next version. The CI pull only 100 latest commit (cf. GIT_DEPTH in gitlab-ci.yml), so if by any chance none of the commit is a tag, no version will be computed and the build will fail.

## How to ADR

We use Architecture Decision Records (ADRs) in our project. And we keep them in `./doc/architecture` directory.

Tool to create an ADRs are <https://github.com/npryce/adr-tools>.
To install it please see this guide, <https://github.com/npryce/adr-tools/blob/master/INSTALL.md>

Example of usage, once `adr-tools` installed,

```
adr help
adr new Implement new script
```

## How to Issue

How do we use gitlab `issues` on this project?

### To follow-up

`Follow-up issue` is gitlab issue for unresolved discussions in Merge Request.

Examples of `follow-ups issues`,

- Refactoring. If reviewer on your Merge Request is adding his comments and thoughts, and you want to act on them. But it can be done separately and it's not a priority of Merge Request.
- Adding integration tests in separate Merge Request (there's special branch namespace for that `ops/integration-FEATURE`)

Example are, https://gitlab.ci.mcf.sh/wsg/api-profile/issues/67

### To explain

`Idea issue` is gitlab issue for discussing the implementation of a new idea, or feature.

Explaining technical task in great detail, like so
https://gitlab.ci.mcf.sh/wsg/api-profile/issues/52

In this case, it become place where we can discuss, to add new details, and see if there's anything we can do and later make a chore after that. It's kind of Pivotal chore starter.

### To troubleshoot

`Troubleshoot issue` is gitlab issue to trace and solve problem with repository code.

Detailed explanation of technical obstacle, which required use of code, and some steps to reproduce. And later be shared with team.

Example are, https://gitlab.ci.mcf.sh/wsg/ops-base/issues/5

Once gitlab issue are created, please try to follow along. Close issue if there's no need in it.

To conclude, how to gitlab `issue` if you want to

- follow-up,
- explain,
- or troubleshoot.
