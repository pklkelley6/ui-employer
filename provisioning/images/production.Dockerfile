ARG APPLICATION_NAME='ui-employer'
FROM registry.gitlab.com/mycf.sg/img-ui-server:0.0.57
ENV APPLICATION_NAME=${APPLICATION_NAME}
COPY ./dist /server/static
COPY ./.version /server/static/version
COPY ./.version /server/.version
USER root
RUN chown 1000:1000 -R ./
USER 1000
