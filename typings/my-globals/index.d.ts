declare module 'my-globals' {
  global {
    /* eslint-disable @typescript-eslint/naming-convention */
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
      msCrypto: Crypto;
    }
  }
}

declare module '*.scss';
declare module '*.png';
