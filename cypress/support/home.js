// auth options code make it outside since it's used for authenticate while routing a page
export function getOptions() {
  return Cypress.env('username')
    ? {
        auth: {
          username: Cypress.env('username'),
          password: Cypress.env('password'),
        },
      }
    : {};
}
Cypress.Commands.add('home', () => {
  cy.visit('/', getOptions());
});

Cypress.Commands.add('openJobPageByJobPostId', (jobPostId, options = {mode: 'applications'}) => {
  cy.task('jobDB', `SELECT uuid FROM jobs where job_post_id = '${jobPostId}';`).then((result) => {
    cy.visit(`/jobs/${result[0].uuid}/${options.mode}`, getOptions()).wait(1500).shouldHaveCardLoaderLoaded();
  });
});
