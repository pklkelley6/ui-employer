export const massSelectionUpdate = {
  clickUpdateStatusButton() {
    cy.get('[data-cy="mass-application-status-update-button"]').click();
    cy.get('div[class*=JobList__spinner]').should('not.exist');
  },

  verifyModalPopupContents(totalApplicantsChecked) {
    cy.get('[data-cy="dropdown-mass-application-status-update"]').contains(
      `Update application status for ${totalApplicantsChecked} applicants to`,
    );
    cy.get('button[data-cy="submit-button-mass-application-status-update-modal"]:disabled').should('exist');
    cy.get('button[data-cy="cancel-button-mass-application-status-update-modal"]').should('exist');
  },

  verifyModalPopupApplicationStatusDropDown() {
    cy.get('[data-cy="dropdown-mass-application-status-update"] div[class$="-control"]').click();
    cy.get('div[id*="react-select"]')
      .should('contain', 'To Interview')
      .and('contain', 'Unsuccessful')
      .and('contain', 'Hired');
    cy.get('[data-cy="dropdown-mass-application-status-update"] div[class$="-control"]').click();
  },

  clickModalPopupButtonBy(buttonName, {mockError} = {mockError: false}) {
    cy.captureGraphqlRequest();
    buttonName = buttonName === 'Update' ? 'submit' : buttonName === 'Ok' ? 'success' : 'cancel';
    cy.get(`button[data-cy=${buttonName}-button-mass-application-status-update-modal]`).click();
    // For submit wait request to complete
    buttonName === 'submit' && !mockError && cy.waitGraphqlRequest(['setMassApplicationShortlistAndStatusUpdate']);
  },

  selectApplicantStatus(status) {
    cy.get('[data-cy="dropdown-mass-application-status-update"] div[class$="-control"]').click();
    cy.get('div[id*=react-select]').contains(status).click();
  },

  verifyModalPopupIs(assertion) {
    cy.get('[data-cy="mass-application-status-update"]').should(assertion);
  },

  verifySuccessModalText(numOfApplicants, status) {
    const applicantMessage = numOfApplicants === 1 ? `${numOfApplicants} applicant` : `${numOfApplicants} applicants`;
    cy.get('[data-cy=mass-application-status-update-success]')
      .should('be.visible')
      .should('contain', `${applicantMessage} updated to ${status}`);
  },

  verifyModalPopupSubmitButtonEnabled() {
    cy.get('button[data-cy="submit-button-mass-application-status-update-modal"]:disabled').should('not.exist');
  },
};
