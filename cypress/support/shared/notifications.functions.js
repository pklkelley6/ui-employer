export const verifyFeatureBanner = (bannerText) => {
  cy.get('[data-cy="feature-notification"]').contains(bannerText);
};

export const verifyFeatureBannerShouldNotExist = () => {
  cy.get('[data-cy="feature-notification"]').should('not.exist');
};

export const closeFeatureBanner = () => {
  cy.get('[data-cy="feature-close-button"]').click();
};

export const verifyMaintenanceBanner = (bannerText) => {
  cy.get('[data-cy="maintenance-notification"]').contains(bannerText);
};

export const verifyMaintenancePage = (pageText) => {
  cy.get('[data-cy="maintenance-page"] #subtitle').contains(pageText);
};
