export const screeningQuestions = {
  validateIncludeScreeningQuestionsCheckboxIs(assertion = 'not.be.checked') {
    cy.get('#screening-questions-checkbox').scrollIntoView().should(assertion);
  },

  validateScreeningQuestionsFieldsIs(assertion = 'not.exist') {
    cy.get('[data-cy=screening-questions-field]').should(assertion);
  },

  checkOrUncheckIncludeScreeningQuestions(action = 'check') {
    const assertion = action === 'check' ? 'exist' : 'not.exist';
    cy.get('#screening-questions-checkbox')
      .scrollIntoView()
      .then((el) => {
        action === 'check' ? cy.wrap(el).check({force: true}) : cy.wrap(el).uncheck({force: true});
      });
    this.validateScreeningQuestionsFieldsIs(assertion);
  },

  clickRemoveButtonOnQuestion(questionNumber) {
    cy.get('[data-cy=screening-questions-remove]').eq(questionNumber).scrollIntoView().click();
  },

  clickOnAddAQuestionButton() {
    cy.get('[data-cy=screening-questions-add]').scrollIntoView().click();
  },

  inputQuestionDetails(questions = {}, {paste} = {paste: false}) {
    Cypress._.each(questions, (question, num) => {
      num > 1 && this.clickOnAddAQuestionButton(); // Add a question
      cy.get('[data-cy=screening-questions-field]')
        .find('input')
        .eq(num - 1)
        .clear()
        .click() // to trigger error if no question given
        .then((el) => {
          question && !paste && cy.wrap(el).type(question); // input if questions given
          question && paste && cy.wrap(el).paste(question); // input if questions given
          cy.wrap(el).blur();
        });
    });
  },

  removeQuestionByDetail(questionDetail) {
    cy.contains('#screening-question', questionDetail)
      .find('[data-cy=screening-questions-remove]')
      .scrollIntoView()
      .click();
  },

  validateErrorMessageForQuestion(questionNumber, errorMessage) {
    cy.get('[data-cy=screening-questions-field]')
      .eq(questionNumber - 1)
      .find('#screening-question-error')
      .should('contain', errorMessage);
  },

  validateQuestionsDetailsInPreviewReviewPage(questions = {}) {
    cy.get('[data-cy=screening-questions_preview]').scrollIntoView().should('be.visible');
    Cypress._.each(questions, (question, num) => {
      cy.contains('label[class*=title]', `Question ${num}`).next().should('contain', question);
    });
  },

  validateNoScreeningQuestionsDetailsInPreviewReviewPage() {
    cy.contains(
      '[data-cy=screening-questions-preview-empty]',
      'There are no screening questions for this job posting.',
    ).should('exist');
  },

  removeAllExistingQuestionsExceptDefault() {
    // removes all except default (1st), which have `Remove button disabled`
    // get all active remove button and remove length - 1 buttons (to avoid clicking 1st disabled button)
    cy.get('[data-cy=screening-questions-remove]:enabled').then((el) => {
      cy.wrap(el)
        .its('length')
        .then((len) => {
          cy.wrap(el).each((btn, index) => {
            index < len - 1 && cy.wrap(btn).click();
          });
        });
    });
  },

  validateScreeningQuestionsTooltipExistance() {
    cy.contains("Why can't I edit screening questions?")
      .should('exist')
      .then((el) => {
        cy.wrap(el).trigger('mouseover');
        cy.get('#screening-questions-tooltip').should('exist');
      });
  },

  validateNoQuestionsSectionInEditScreeningQuestionsTab() {
    cy.contains(
      '[data-cy=screening-questions-preview-empty]',
      'There are no screening questions for this job posting.',
    ).should('exist');
  },

  validatePreviewEditLinkExistence(assertion) {
    cy.get(`[data-cy=screening-questions]`).find('a').should(assertion);
  },

  validateRemoveScreeningQuestionsMenuExistance(assertion) {
    cy.get('[data-cy=manage-job-remove-screening-questions]').should(assertion);
  },

  clickOnRemoveScreeningQuestionsOption() {
    cy.get('[data-cy=manage-job-remove-screening-questions]').click();
  },

  modalPopup: {
    validateWarningModalPopupExistence(assertion) {
      cy.get('[data-cy=remove-screening-questions-modal]').should(assertion);
    },

    validateWarningModalPopupShowsQuestions(questions = {}) {
      this.validateWarningModalPopupExistence('exist');
      cy.get('[data-cy=remove-screening-question-modal] div').should('contain.text', 'Remove Screening Questions');
      Cypress._.each(questions, (question, num) => {
        cy.get('[data-cy=remove-screening-question-modal]').should('contain.text', `Q${num}.${question}`);
      });
    },

    clickOnButton(buttonName) {
      const buttonSelector = buttonName === 'Yes, Remove' ? 'confirm' : buttonName === 'Ok' ? 'success-ok' : 'cancel';
      cy.get(`[data-cy=remove-screening-question-${buttonSelector}]`).click();
    },

    validateSuccessModalPopupExistenceAndClickOk() {
      cy.get('[data-cy=remove-screening-question-modal-success]')
        .should('exist')
        .and('contain.text', 'Screening questions removed successfully');
      this.clickOnButton('Ok');
    },
  },
};
