const verifyTermsOfUseLink = () => {
  cy.get('footer a[title="Terms of Use"]')
    .should('have.text', 'Terms of Use')
    .should('have.attr', 'href', '/terms-of-use');
};

const verifySurveyLink = () => {
  cy.get('footer a[title="Survey"]').should('have.text', 'Survey').should('have.attr', 'href', '/survey');
};

const verifyFAQLink = () => {
  cy.get('footer a[title="FAQ"]')
    .should('have.text', 'FAQ')
    .should('have.attr', 'target', '_blank')
    .should('have.attr', 'href', 'https://www.mycareersfuture.gov.sg/docs/mycareersfuture_sg_user_faqs.pdf');
};

const verifyVDPLink = () => {
  cy.get('footer a[title="Report Vulnerability"]')
    .should('have.text', 'Report Vulnerability')
    .should('have.attr', 'href', 'https://www.tech.gov.sg/report_vulnerability')
    .should('have.attr', 'target', '_blank');
};

const verifyTAFEPLink = () => {
  cy.get('footer a[title="Pledge for Fair Employment"]')
    .should('have.text', 'Pledge for Fair Employment')
    .should('have.attr', 'href', 'https://www.tal.sg/tafep/getting-started/fair/employers-pledge')
    .should('have.attr', 'target', '_blank');
};

const verifyMOMLink = () => {
  cy.get('footer a[title="Second Job Arrangements"]')
    .should('have.text', 'Second Job Arrangements')
    .should('have.attr', 'href', 'https://www.mom.gov.sg/covid-19/second-job-arrangements')
    .should('have.attr', 'target', '_blank');
};

const verifyCareerTrialLink = () => {
  cy.get('footer a[title="Career Trial"]')
    .should('have.text', 'Career Trial')
    .should('have.attr', 'href', 'https://programmes.mycareersfuture.gov.sg/CareerTrialEmployers/ProgrammeDetails.aspx')
    .should('have.attr', 'target', '_blank');
};

export const verifyFooterLinks = () => {
  verifyTermsOfUseLink();
  verifySurveyLink();
  verifyFAQLink();
  verifyVDPLink();
  verifyTAFEPLink();
  verifyMOMLink();
  verifyCareerTrialLink();
};
