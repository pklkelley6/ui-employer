import {mcf} from '@mcf/constants';
import {getPagination} from './common.functions';

const selectorDetails = {
  candidateListItem: '[data-cy="candidates-list-item"]',
};
const applicantStatusColorCode = {
  'To Interview': 'black',
  Hired: 'green',
  Unsuccessful: 'red',
};

export const manageApplicantsView = ($job) => {
  cy.log('Has job post id');
  cy.get('[data-cy="job-post-id"]').invoke('text').should('contain', 'MCF-');
  cy.log('Has Job Status as OPEN');
  cy.get('[data-cy="job-post-status"]').invoke('text').should('equal', 'OPEN');
  cy.log('Has Job Title');
  cy.get('[data-cy="job-post-title"]').invoke('text').should('exist');
  cy.log('Has Address Field');
  cy.get('[data-cy="job-post-address"]').should('exist');
  cy.log('Has Employment Type');
  cy.get('[data-cy="job-post-employment-types"]').should('exist');
  cy.log('Has Job Category');
  cy.get('[data-cy="job-post-categories"]').should('exist');
  cy.log('Has Vancancy');
  cy.get('[data-cy="job-post-vacancy"]').should('exist');
  cy.log('Has Salary Field');
  cy.get('[data-cy="job-post-monthly-salary"]').should('exist');
  cy.log('Has Open Date');
  cy.get('[data-cy="job-post-posted-date"]').invoke('text').should('contain', 'Posted');
  cy.log('Has Closing Date');
  cy.get('[data-cy="job-post-expiry-date"]').invoke('text').should('contain', 'Closing');
  cy.log('Has Applicants Field');
  cy.get('#tab-applications')
    .invoke('text')
    .should('contain', 'Applicants ' + $job.Applicants);

  cy.log('Has Suggested Talents Field');
  cy.get('#tab-suggested-talents').invoke('text').should('contain', 'Suggested Talents');

  //Need to add more checking here...
};

export function applicantLeftPanelNameValidation($applicant, $index) {
  cy.get(selectorDetails.candidateListItem)
    .eq($index)
    .find('[data-cy="candidates-name"]')
    .invoke('text')
    .should('equal', $applicant.name);
}

export const isCandidateViewed = (isViewed) => (candidateListItem) => {
  if (isViewed) {
    cy.log('Check Candidate has been viewed');
    cy.wrap(candidateListItem)
      .children('div')
      .should('have.class', 'bg-white-80')
      .children('div')
      .should('have.class', 'b--black-10');
  } else {
    cy.log('Check Candidate has not been viewed');
    cy.wrap(candidateListItem)
      .children('div')
      .should('have.class', 'bg-white')
      .children('div')
      .should('have.class', 'b--blue');
  }
  cy.wrap(candidateListItem);
};

export function isCandidateSelected(candidateListItem) {
  const div = candidateListItem.children('div')[0];
  expect(div.className).to.match(/CandidatesList__card-active___[a-zA-Z0-9]{5}/);
}

export function checkCandidateRightBottomPanelSkillsTab($skills) {
  cy.get('[data-cy="skill-icon"]').should('have.length', $skills.length);
  for (let i = 0; i < $skills.length; i++) {
    cy.get('[data-cy="skill-icon"]').eq(i).contains($skills[i]);
  }
}

export function checkCandidateRightBottomPanelJobsTab($jobs) {
  cy.get('[data-cy="work-experience-field"]').should('have.length', $jobs.length);
  for (let i = 0; i < $jobs.length; i++) {
    if ($jobs[i].year != null) {
      cy.get('[data-cy="work-experience-field"]').eq(i).find('[data-cy="experience-time"]').contains($jobs[i].year);
    } else {
      cy.get('[data-cy="work-experience-field"]')
        .eq(i)
        .find('[data-cy="experience-time"]')
        .invoke('text')
        .should('equal', '');
    }
    //job title is mandatory
    cy.get('[data-cy="work-experience-field"]').eq(i).find('[data-cy="experience-title"]').contains($jobs[i].title);
    //job company is mandatory
    cy.get('[data-cy="work-experience-field"]')
      .eq(i)
      .find('[data-cy="experience-location"]')
      .contains($jobs[i].company);

    if ($jobs[i].description) {
      cy.get('[data-cy="work-experience-field"]')
        .eq(i)
        .scrollIntoView()
        .find('[data-cy="experience-description"]')
        .should('have.text', $jobs[i].description);
    }
  }
}

export function checkCandidateRightBottomPanelEducationTab($educations) {
  cy.get('[data-cy="education-history-field"]').should('have.length', $educations.length);
  for (let i = 0; i < $educations.length; i++) {
    cy.get('[data-cy="education-history-field"]')
      .eq(i)
      .find('[data-cy="experience-time"]')
      .contains($educations[i].year);
    if ($educations[i].ssec_foc != null) {
      cy.get('[data-cy="education-history-field"]')
        .eq(i)
        .find('[data-cy="experience-title"]')
        .contains($educations[i].ssec_foc);
    } else {
      cy.get('[data-cy="education-history-field"]')
        .eq(i)
        .find('[data-cy="experience-title"]')
        .invoke('text')
        .should('equal', '');
    }

    //institution is mandatory
    cy.get('[data-cy="education-history-field"]')
      .eq(i)
      .find('[data-cy="experience-location"]')
      .contains($educations[i].institution);

    if ($educations[i].description != null) {
      cy.get('[data-cy="education-history-field"]')
        .eq(i)
        .scrollIntoView()
        .find('[data-cy="experience-description"]')
        .should('have.text', $educations[i].description);
    }
  }
}

export function checkCandidateRightBottomPanelOverviewTab($applicant) {
  if ($applicant.skills.length != 0) {
    cy.log('Has skills field in Overview');
    cy.get('[data-cy="overview-skills"]').contains('Skills');
    if ($applicant.skills.length < 10) {
      checkCandidateRightBottomPanelSkillsTab($applicant.skills);
    } else {
      cy.get('[data-cy="skill-icon"]').should('have.length', 10);
      for (let i = 0; i < 10; i++) {
        cy.get('[data-cy="skill-icon"]').eq(i).contains($applicant.skills[i]);
      }
    }
  }
  if ($applicant.jobs.length != 0) {
    cy.log('Has jobs field in Overview');
    cy.get('[data-cy="overview-work-experience"]').contains('Work Experience');
    if ($applicant.jobs.length < 3) {
      checkCandidateRightBottomPanelJobsTab($applicant.jobs);
    } else {
      for (let i = 0; i < 3; i++) {
        if ($applicant.jobs[i].year != null) {
          cy.get('[data-cy="work-experience-field"]')
            .eq(i)
            .find('[data-cy="experience-time"]')
            .contains($applicant.jobs[i].year);
        } else {
          cy.get('[data-cy="work-experience-field"]')
            .eq(i)
            .find('[data-cy="experience-time"]')
            .invoke('text')
            .should('equal', '');
        }
        //job title is mandatory
        cy.get('[data-cy="work-experience-field"]')
          .eq(i)
          .find('[data-cy="experience-title"]')
          .contains($applicant.jobs[i].title);
        //job company is mandatory
        cy.get('[data-cy="work-experience-field"]')
          .eq(i)
          .find('[data-cy="experience-location"]')
          .contains($applicant.jobs[i].company);
        if ($applicant.jobs[i].description != null) {
          cy.get('[data-cy="work-experience-field"]')
            .eq(i)
            .scrollIntoView()
            .find('[data-cy="experience-description"]')
            .should('have.text', $applicant.jobs[i].description);
        }
      }
    }
  }
  if ($applicant.educations.length != 0) {
    cy.log('Has education field in Overview');
    cy.get('[data-cy="overview-education-history"]').contains('Education History');
    if ($applicant.educations.length < 3) {
      checkCandidateRightBottomPanelEducationTab($applicant.educations);
    } else {
      for (let i = 0; i < 3; i++) {
        cy.get('[data-cy="education-history-field"]')
          .eq(i)
          .find('[data-cy="experience-time"]')
          .contains($applicant.educations[i].year);
        if ($applicant.educations[i].ssec_foc != null) {
          cy.get('[data-cy="education-history-field"]')
            .eq(i)
            .find('[data-cy="experience-title"]')
            .contains($applicant.educations[i].ssec_foc);
        } else {
          cy.get('[data-cy="education-history-field"]')
            .eq(i)
            .find('[data-cy="experience-title"]')
            .invoke('text')
            .should('equal', '');
        }
        //institution is mandatory
        cy.get('[data-cy="education-history-field"]')
          .eq(i)
          .find('[data-cy="experience-location"]')
          .contains($applicant.educations[i].institution);
        if ($applicant.educations[i].description != null) {
          cy.get('[data-cy="education-history-field"]')
            .eq(i)
            .scrollIntoView()
            .find('[data-cy="experience-description"]')
            .should('have.text', $applicant.educations[i].description);
        }
      }
    }
  }
}

function getJobTitleAndCompany(jobTitle, companyName) {
  const emptyDataString = '(Data unavailable)';
  if (jobTitle.length) {
    return `${jobTitle}, ${companyName}`;
  } else {
    jobTitle = emptyDataString;
    return jobTitle;
  }
}

function getJobTitle(jobTitle) {
  const emptyDataString = '(Data unavailable)';
  return jobTitle || emptyDataString;
}

export function getCandidateFromList(index) {
  return cy.get(selectorDetails.candidateListItem).eq(index);
}

export const shouldHaveCandidateListItemInfo =
  (candidate, page = 'applicants') =>
  (candidateListItem) => {
    cy.wrap(candidateListItem).find('[data-cy="candidates-name"]').should('have.text', candidate.name);
    cy.wrap(candidateListItem)
      .find('[data-cy="candidates-job-title"]')
      .should('have.text', getJobTitle(candidate.jobtitle));
    cy.wrap(candidateListItem)
      .find('[data-cy="candidates-apply-date"]')
      .invoke('text')
      .should('contain', candidate.applyDate || 'Active');
    if (page !== 'searchTalent') {
      //should not valdidate this for searchTalent page details
      if (candidate.hasResume) {
        cy.log('Checking Resume field');
        cy.wrap(candidateListItem)
          .find('[data-cy="candidates-list-item-download-button"]')
          .should('have.attr', 'class')
          .and('include', 'dim pointer');
      } else {
        cy.log('Resume not available');
        cy.wrap(candidateListItem)
          .find('[data-cy=candidates-get-resume-text]')
          .should('have.text', 'No Resume Available');
      }
    }
    cy.wrap(candidateListItem);
  };

export function shouldNotHaveScore(candidateListItem) {
  cy.wrap(candidateListItem).find('[title="wccScore"]').should('not.exist');
  cy.wrap(candidateListItem);
}

export function shouldHaveCandidateRightTopPanelInfo(candidate) {
  cy.log('Verifying candidate name');
  cy.get('[data-cy="candidate-right-panel-1-applicant-name"]').should('have.text', candidate.name);
  cy.log('Verifying candidate job title and company name');
  if (candidate.jobtitle == '' && candidate.jobcompany == '') {
    cy.get('[data-cy="candidate-right-panel-1-position"]').should(
      'have.text',
      getJobTitleAndCompany(candidate.jobtitle, candidate.jobcompany),
    );
  } else {
    cy.get('[data-cy="candidate-right-panel-1-position"]').should(
      'have.text',
      getJobTitleAndCompany(candidate.jobtitle, candidate.jobcompany),
    );
  }

  cy.log('Verifying candidate job apply date');
  cy.get('[data-cy="candidate-right-panel-1-applied-date"]')
    .invoke('text')
    .should('contain', candidate.applyDate || 'Active');

  cy.log('Verifying candidate email address');
  cy.get('[data-cy="candidate-right-panel-1-email"]')
    .should('have.text', candidate.email)
    .should('have.attr', 'href', `mailto: ${candidate.email}`);

  if (candidate.mobileNumber) {
    cy.log('Verifying candidate mobile');
    cy.get('[data-cy="candidate-right-panel-1-mobile"]')
      .should('have.text', candidate.mobileNumber)
      .should('have.attr', 'href', `tel: ${candidate.mobileNumber}`);
  }

  if (candidate.hasResume) {
    cy.log('Verifying resume download link');
    cy.get('[data-cy="applicant-resume-download-button"]')
      .should('have.attr', 'class')
      .and('not.include', 'o-20 cursor-not-allowed');
  } else {
    cy.log('Resume not available');
    cy.get('[data-cy="applicant-resume-download-button"]').should('not.exist');
  }
}

export function shouldHaveCandidateRightBottomPanelInfo(candidate) {
  cy.get('#tab-0').contains('Overview');
  checkCandidateRightBottomPanelOverviewTab(candidate);

  cy.get('#tab-2').contains('Skills').click({force: true});
  checkCandidateRightBottomPanelSkillsTab(candidate.skills);

  cy.get('#tab-3').contains('Work Experience').click({force: true});
  checkCandidateRightBottomPanelJobsTab(candidate.jobs);

  cy.get('#tab-1').contains('Education History').click({force: true});
  checkCandidateRightBottomPanelEducationTab(candidate.educations);
}

export function shouldHaveEmptyTalentRightPanel() {
  cy.get('[data-cy="empty-suggested-talent"]').contains('Pick a talent from the left to view profile details.');
}

export function shouldHaveNumberOfApplicantsOnEachPage($number) {
  cy.log('each page should have ' + $number + ' applicants');
  cy.get(selectorDetails.candidateListItem, {timeout: 30000}).should(($candidate) => {
    expect($candidate).to.have.length($number);
  });
}

export function shouldHavePageProperty($index, spanText, isSelected) {
  if (isSelected) {
    cy.log('In the ' + $index + ' button,should have text ' + spanText + ' is selected');
    getPagination($index).should('have.class', 'bg-secondary').contains(spanText);
  } else {
    cy.log('In the ' + $index + ' button,should have text ' + spanText + ' is not selected');
    getPagination($index).should('have.class', 'hover-bg-white').contains(spanText);
  }
}

export function isSurveyBarShown(isShown) {
  if (isShown) {
    cy.log('The survey tab should appear');
    cy.get('[data-cy="surveybar"]').contains("Let us know how we're doing so we can improve your future visits.");
    cy.get('[data-cy="surveybar-close-button"]').contains('Close');
    cy.get('[data-cy="surveybar-take-survey-button"]').contains('Take survey');
    cy.get('[data-cy="surveybar-take-survey-button"]').should('have.attr', 'href', '/survey');
  } else {
    cy.log('The survey tab should not appear');
    cy.get('[data-cy="surveybar"]').should('not.exist');
  }
}

export function shouldHaveTopMatchTotalShown(numberOfTopMatches) {
  cy.log('Display' + numberOfTopMatches + ' top matches');
  cy.get('[data-cy="applications-top-match"]', {timeout: 30000})
    .trigger('mouseover')
    .contains(`Show "Among top matches" (${numberOfTopMatches})`);
}

export function shouldHaveTotalApplicantsInApplicantList(length) {
  cy.log('Verifying the total number of applicants should have' + length);
  cy.get(selectorDetails.candidateListItem).should((applicationList) => {
    expect(applicationList).to.have.length(length);
  });
}

export function shouldHaveTopMatcherInApplicantList(index) {
  cy.log('Should have Top Matcher Label in the list with index no: ' + index);
  cy.get(selectorDetails.candidateListItem).eq(index).find('[data-cy="top-matcher"]').contains('Among top matches');
}

export function shouldNotHaveTopMatcherInApplicantList(index) {
  cy.log('Should have Top Matcher Label in the list with index no: ' + index);
  cy.get(selectorDetails.candidateListItem).eq(index).find('[data-cy="top-matcher"]').should('not.exist');
}

export function shouldHaveTopMatcherForAllApplicants() {
  cy.log('Should have top matcher label for all applicants');
  cy.get(selectorDetails.candidateListItem).each((el) => {
    cy.wrap(el).find('[data-cy="top-matcher"]').should('exist');
  });
}

export function shouldHaveTopMatcherInApplicantDetail() {
  cy.log('Should have top matcher label in the applicant detail panel 1');
  cy.get('[data-cy="candidate-right-panel-1"]').find('[data-cy="top-matcher"]').contains('Among top matches');
}

export function shouldNotHaveTopMatcherInApplicantDetail() {
  cy.log('Should have top matcher label in the applicant detail panel 1');
  cy.get('[data-cy="candidate-right-panel-1"]').find('[data-cy="top-matcher"]').should('not.exist');
}

export function shouldHaveApplicantOnboarderContent() {
  cy.log('Check for onboader modal');
  cy.get('[data-cy="onboarding-module"]').should('exist');
  cy.get('[data-cy="onboarding-module-close"]').should('exist');
  cy.get('[data-cy="skip-button"]').contains('skip');
  cy.get('[data-cy="next-button"]').contains('Next');
  cy.get('[data-cy=onboarding-module-close]')
    .prev()
    .should('have.text', 'How do I sort applicants by suitability for my job?');
  cy.get('[data-cy="applicant-onboarding-string-1"]').contains(
    'You may sort applicants by job match, which ranks applicants based on their skills, work experience and education against your job description.',
  );
  cy.get('[data-cy="applicant-onboarding-string-1"]').contains('The job matching engine is powered by WCC.');
  cy.get('[data-cy="next-button"]').trigger('mouseover').click();
  cy.get('[data-cy="applicant-onboarding-string-2"]').contains(
    'Applicants bearing the ‘Among top matches’ label have been assessed to be a close match to the job by the job matching engines.',
  );
  cy.get('[data-cy="gotit-button"]', {timeout: 20000}).contains('Got it');
  cy.get('[data-cy="gotit-button"]').click();
}

export function shouldNotHaveOnboarderModule() {
  cy.get('[data-cy="onboarding-module"]').should('not.exist');
}

export function getSuggestedTalentTabCount() {
  return cy.get('#tab-suggested-talents div');
}

export function shouldHaveSuggestedTalentOnboarderContent() {
  cy.log('Check for onboader modal');
  cy.get('[data-cy="onboarding-module"]').should('exist');
  cy.get('[data-cy="onboarding-module-close"]').should('exist');
  cy.get('[data-cy="skip-button"]').contains('skip');
  cy.get('[data-cy="next-button"]').contains('Next');
  cy.get('[data-cy=onboarding-module-close]')
    .prev()
    .should('have.text', 'How are suggested talents identified for my job?');
  cy.get('[data-cy="talent-onboarding-string-1"]').contains(
    'Suggested talents did not apply for the job. However, they have relevant skills, work experience and education to match your job posting.',
  );
  cy.get('[data-cy="next-button"]').click();
  cy.get('[data-cy="talent-onboarding-string-2"]').contains(
    'These suggested talents are candidates who are open to being contacted for job opportunities. You may choose to download their resumes and contact them directly.',
  );
  cy.get('[data-cy="next-button"]').trigger('mouseover').click();
  cy.get('[data-cy="talent-onboarding-string-3"]').contains(
    'The suggested talents list is dynamic and refreshed regularly. New talents are constantly added as our talent pool grows, while existing talents may be removed if their profile is no longer relevant.',
  );
  cy.get('[data-cy="talent-onboarding-string-3"]').contains(
    'You are encouraged to download the resumes of suggested talents you are keen to contact.',
  );
  cy.get('[data-cy="gotit-button"]', {timeout: 20000}).contains('Got it');
  cy.get('[data-cy="gotit-button"]').click();
}
export const verifyApplicantStatusList = () => {
  cy.get('#update-application-status-dropdown').click();
  cy.contains('To Interview');
  cy.contains('Unsuccessful');
  cy.contains('Hired');
};

export const verifyCandidateTypeLabelLeftPanel = (candidateType) => {
  cy.log(`Should have ${candidateType} labels in the candidate details left panel`);
  const labelSelector = candidateType === 'Applicant' ? '[data-cy=Applicant-label]' : '[data-cy=Talent-label]';

  return cy.get('[data-cy=candidates-list-item]').each((item) => {
    cy.wrap(item).find(labelSelector).should('exist').invoke('text').should('contain', candidateType);
  });
};

export const verifyCandidateTypeLabelRightPanel = (candidateType) => {
  cy.log(`Should have ${candidateType} labels in the candidate details right panel`);
  const labelSelector = candidateType === 'Applicant' ? '[data-cy=Applicant-label]' : '[data-cy=Talent-label]';

  return cy
    .get('[data-cy=candidate-right-panel-1]')
    .find(labelSelector)
    .invoke('text')
    .should('contain', candidateType);
};

export const applicantStatus = {
  selectApplicantStatus(status, {mockError} = {mockError: false}) {
    cy.captureGraphqlRequest();
    cy.get('#update-application-status-dropdown').click();
    cy.get('div[id*=react-select]').contains(status).click();
    !mockError &&
      cy.waitGraphqlRequest(['setApplicationShortlistAndStatusUpdate', 'getApplicationsWithStatusCountByJob']);
    cy.get('div[class*=JobList__spinner]').should('not.exist');
    cy.get('#update-application-status-dropdown').invoke('text').should('contain', status);
  },

  verifyApplicantStatusRightPaneUpdated(applicantStatus) {
    cy.get('#update-application-status-dropdown').scrollIntoView();
    cy.contains('#update-application-status-dropdown', applicantStatus).should('be.visible');
  },

  verifyApplicantStatusLeftPanelUpdated(applicantStatus, candidateIndex = 0) {
    cy.get('[data-cy=candidates-name]')
      .eq(candidateIndex)
      .next()
      .should('have.text', applicantStatus)
      .should('be.visible');
    cy.get('[data-cy=candidates-name]')
      .eq(candidateIndex)
      .next()
      .invoke('attr', 'class')
      .should('contain', applicantStatusColorCode[applicantStatus]);
  },

  verifyApplicantStatusNotExist(candidate) {
    cy.get('[data-cy=application-status-label]').eq(candidate).should('not.exist');
  },

  verifyApplicationsStatusIs(assertion = 'match', regExp) {
    cy.get('[data-cy=application-status-label]')
      .should('be.visible')
      .each((el) => {
        cy.wrap(el).invoke('text').should(assertion, regExp);
      });
  },

  verifyApplicantStatusNotExistForAllApplicants() {
    cy.get('[data-cy=application-status-label]').should('not.exist');
  },
};

export const applicantCheckbox = {
  verifyMassApplicantCheckboxIndeterminate() {
    cy.get('[data-cy="mass-application-selection-checkbox"]').should('not.be.checked');
    cy.get('[data-cy="mass-application-selection-checkbox"]:indeterminate').should('exist');
  },

  verifyMassApplicantCheckboxChecked() {
    cy.get('[data-cy="mass-application-selection-checkbox"]').should('be.checked');
    cy.get('[data-cy="mass-application-selection-checkbox"]:indeterminate').should('not.exist');
  },

  verifyMassApplicantCheckboxUnchecked() {
    cy.get('[data-cy="mass-application-selection-checkbox"]').should('not.be.checked');
    cy.get('[data-cy="mass-application-selection-checkbox"]:indeterminate').should('not.exist');
  },

  verifyAllCheckboxesChecked() {
    cy.get('[data-cy="candidate-checkbox"]').should('be.checked');
  },

  verifyAllCheckboxesUnchecked() {
    cy.get('[data-cy="candidate-checkbox"]').should('not.be.checked');
  },

  verifyUpdateStatusButtonTextCountValue(totalApplicantsChecked) {
    cy.get('[data-cy="mass-application-status-update-button"]').should(
      'have.value',
      `Update status (${totalApplicantsChecked})`,
    );
  },

  verifyUpdateStatusLabel() {
    cy.get('[data-cy="mass-application-status-select-all-text"]').should('exist');
  },

  verifyApplicantCheckboxStatusByIndex(index, assertion = 'be.checked') {
    cy.log(`checking applicant index ${index} checkbox is ${assertion}`);
    cy.get('[data-cy=candidate-checkbox]').eq(index).should(assertion);
  },
};

export const validateScreeningQuestionsTabForApplicantExistance = (assertion = 'exist') => {
  cy.get('[class*=CandidateInfo__info]').find('[title="Screening Questions"]').should(assertion);
};

export const validateScreeningQuestionsAndApplicantResponses = (questions = {}, responses = []) => {
  // questions = {1: 'questions',...}
  cy.get('[title="Screening Questions"] span').should('be.visible').click();

  Cypress._.each(questions, (question, num) => {
    cy.get(`[data-cy=screening-question-question-${num - 1}]`).should('have.text', `Q${num}. ${question}`);
    cy.get(`[data-cy=screening-question-answer-${num - 1}]`).should(
      'have.text',
      mcf.getScreeningQuestionResponse(responses[num - 1]).label,
    );
  });
};

export const validateApplicationStatusForListOfApplicantsInLHSAndRHS = (applicantList, status) => {
  cy.wrap(applicantList).each((index) => {
    cy.clickOnCandidate(index);
    applicantStatus.verifyApplicantStatusRightPaneUpdated(status);
    applicantStatus.verifyApplicantStatusLeftPanelUpdated(status, index);
  });
};
