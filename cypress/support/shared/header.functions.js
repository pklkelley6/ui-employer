const verifyMCFLogoLink = () => {
  cy.get('[data-cy=mcf-logo]').should('have.attr', 'href', '/');
};

const verifySwitchToJobseekerLink = () => {
  cy.get('[data-cy=switch-jobseeker]')
    .should('have.text', 'Switch to Jobseeker')
    .should('have.attr', 'href', 'https://www.mycareersfuture.gov.sg');
};

const verifyPostJobsLink = () => {
  cy.get('[data-cy=post-jobs-link] span').should('have.text', 'Post Jobs');
  cy.get('[data-cy=post-jobs-link]').should('have.attr', 'href', '/jobs');
};

const verifyEmployerToolkitLink = () => {
  cy.get('[data-cy=employers-toolkit-link]')
    .should('have.text', 'Employers Toolkit')
    .should('have.attr', 'target', '_blank')
    .should('have.attr', 'href', 'https://content.mycareersfuture.gov.sg/category/employers-toolkit');
};

const verifyMastheadAndLink = () => {
  cy.get('div[class*="MastHead"] a').then((masthead) => {
    //Checks link, opens in new tab, and text
    cy.wrap(masthead)
      .should('have.attr', 'href', 'https://www.gov.sg')
      .and('have.attr', 'target', '_blank')
      .and('have.text', 'A Singapore Government Agency Website');

    //Checks logo
    cy.wrap(masthead).find('span[class*="masthead-crest"]').should('be.visible');
  });
};

export const verifyHeaderLinks = () => {
  verifyMCFLogoLink();
  verifySwitchToJobseekerLink();
  verifyPostJobsLink();
  verifyEmployerToolkitLink();
  verifyMastheadAndLink();
};
