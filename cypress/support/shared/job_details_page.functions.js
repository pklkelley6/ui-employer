const editCountLeftSelector = '[data-cy="edits-left"]';
const editCountTextSelector = '[data-cy="edit-text"]';
export const editJobPostButtonSelector = 'a[data-cy="edit-job-post"]';
export const editRemainCounterSelector = '[data-cy="edits-remaining-counter"]';
export const viewJobPostButtonSelector = '[data-cy="view-job-post-button"]';

export const checkEditCountOfJobPost = (numberOfEditsLeft) => {
  if (numberOfEditsLeft >= 1) {
    cy.get(editCountLeftSelector).should('contain', numberOfEditsLeft);
    cy.get(editCountTextSelector).should('contain', `${numberOfEditsLeft === 1 ? 'Edit' : 'Edits'}`);
  } else {
    cy.get(editCountLeftSelector).should('contain', numberOfEditsLeft);
    cy.get(editCountTextSelector).should('contain', 'Edits left');
  }
};

export const verifyEditJobPostButton = (numberOfEditsLeft) => {
  if (numberOfEditsLeft === 0) {
    cy.get('div[data-cy="disabled-edit-job-post"]');
  } else {
    cy.get(editJobPostButtonSelector)
      .should('have.attr', 'href')
      .and('match', /\/jobs\/.+\/edit$/);
  }
};

export const viewJobPostingClickAndLoaded = (selector) => {
  cy.get(selector).click();
  cy.url().should('include', '/view');
  cy.shouldHaveFormLoaderLoaded();
};

export const verifyEditJobPostSubmitModal = () => {
  cy.get('[data-cy="cancel-button-edit-job-submit-modal"]').should('have.text', 'No, Cancel');
  cy.get('[data-cy="submit-button-edit-job-submit-modal"]').should('have.text', 'Yes, Submit');
  cy.get('[data-cy="edit-job-submit-modal"]').contains('Are you sure you want to submit this edited job posting?');
  cy.get('[data-cy="edit-job-submit-modal"]').contains('After submitting, you will have 1 edit left');
};
