const applicantsAllRejectReasonValues = [
  'Did not turn up for interview',
  'Did not meet competency requirements',
  'Did not have enough relevant experience',
  'Did not fit company culture',
  'Turned down job offer',
  'Others',
];

export const selectorDetails = {
  rejectReasonSaveButton: '[data-cy=application-rejection-reason-save-button]',
  rejectReasonEditLink: '[data-cy=readonly-application-rejection-reason-edit-button]',
  rejectReasonReadOnlyDropdownValue: '[data-cy=readonly-application-rejection-reason]',
  rejectReasonReadOnlyOthersFreeTextValue: '[data-cy=readonly-application-rejection-reason-others]',
  rejectReasonDropdownValueRequiredError: '[data-cy=application-rejection-reason-dropdown] span[id=-error]',
  rejectReasonOthersFreeTextError: '[data-cy=text-editor-error]',
  rejectReasonFillOthersFreeText: '[data-cy=application-rejection-reason-others] div[role=textbox]',
  rejectReasonClickDropdown: '[data-cy="application-rejection-reason-dropdown"] div[class$="-control"]',
};

export const applicantRejectReasonUpdate = {
  selectApplicantRejectReasons(reason) {
    cy.get(selectorDetails.rejectReasonClickDropdown).click();
    cy.get('div[id*=react-select]').contains(reason).click();
  },

  verifyOthersRejectReasonTextSectionIs(assertion) {
    cy.get(
      '[data-cy="application-rejection-reason-others"] div[class="DraftEditor-root"] div[contenteditable="true"]',
    ).should(assertion);
    cy.get('[data-cy="text-editor-character-count"]').should(assertion);
  },

  verifyApplicantsAllRejectReasonValuesInDropdown() {
    cy.get(selectorDetails.rejectReasonClickDropdown).click();
    applicantsAllRejectReasonValues.forEach((rejectReason) => {
      cy.get('div[id*="react-select"]').should('contain', rejectReason);
    });
    cy.get(selectorDetails.rejectReasonClickDropdown).click();
  },

  clickOnSaveButton({mockError} = {mockError: false}) {
    cy.captureGraphqlRequest();
    cy.get(selectorDetails.rejectReasonSaveButton).click();
    !mockError && cy.waitGraphqlRequest(['setApplicationRejectionReason']);
    cy.get(selectorDetails.rejectReasonEditLink).should('exist');
    cy.get(selectorDetails.rejectReasonSaveButton).should('not.exist');
  },
};
