export const massUpdateApplicationsStatus = {
  verifyMassUpdateGreyBarIs(assertion = 'exist') {
    cy.get('[data-cy=update-remaining-applications]').should(assertion);
  },

  verifyMassUpdateGreyBarTextAndLink() {
    cy.get('[data-cy=update-remaining-applications-text]')
      .should('exist')
      .scrollIntoView()
      .and('contain', 'All vacancies filled. Do you want to update the remaining applicants as Unsuccessful?');
    cy.get('[data-cy=update-remaining-applications-link]').should('exist').and('contain', 'Yes, Update');
  },

  clickMassUpdateGreyBarLink({mockError} = {mockError: false}) {
    cy.captureGraphqlRequest();
    cy.get('[data-cy=update-remaining-applications-link]').click();
    !mockError && cy.waitGraphqlRequest(['setApplicationStatusByJob', 'getApplicationsWithStatusCountByJob']);
    cy.get('div[class*=JobList__spinner]').should('not.exist');
  },

  waitForGreyBarToDisappear() {
    cy.get('[data-cy=update-remaining-applications-link]', {timeout: 5000}).should('not.exist');
  },
};
