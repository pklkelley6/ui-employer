const selectorDetails = {
  bookmarkToggleIcon: '[data-cy="bookmark-toggle"]',
  bookmarkIndicator: '[data-cy="candidates-list-item-bookmark-indicator"]',
  candidateListItem: '[data-cy="candidates-list-item"]',
  bookmarkCandidateList: '[data-cy=bookmarked-candidate-list] > div',
  bookmarkApplicantLabel: 'div[data-cy=Applicant-label]',
  bookmarkTalentLabel: 'div[data-cy=Talent-label]',
};

export const clickBookmarkIconIfTrue = (clickBookmark, {mockError} = {mockError: false}) => {
  if (clickBookmark) {
    cy.captureGraphqlRequest();
    cy.get(selectorDetails.bookmarkToggleIcon).scrollIntoView({duration: 500}).should('be.visible').click();
    !mockError && cy.waitGraphqlRequest(['getBookmarkedCandidatesCount']);
  }
};

export const verifyBookmarkIconTooltipText = (bookmarkTooltipText) => {
  cy.get(selectorDetails.bookmarkToggleIcon).should('exist').trigger('mouseover');
  cy.get('#bookmark-toggle-tooltip').scrollIntoView().should('to.be.visible').should('contain', bookmarkTooltipText);
};

export const verifyBookmarkIconFromCandidateList = (index, bookmarkIconAssertion) => {
  cy.get(selectorDetails.candidateListItem)
    .eq(index)
    .find(selectorDetails.bookmarkIndicator)
    .should(bookmarkIconAssertion);
};

export const verifyBookmarkSaveText = (exist) => {
  if (exist) {
    cy.get('[data-cy="bookmark-candidate"] span').should('have.text', 'Saved').should('exist');
  }
};

export const verifyBookmarkCount = (count) => {
  cy.get('#tab-saved').scrollIntoView({duration: 500}).find('div').invoke('text').should('contain', count);
};

export const validateBookmarkSortingOrder = (name, type, bookmarkIndex) => {
  cy.log(`Validate ${name} is showing at ${bookmarkIndex} position`);
  cy.get(selectorDetails.bookmarkCandidateList)
    .eq(bookmarkIndex)
    .then(($candidate) => {
      cy.wrap($candidate).should('contain', name);
      cy.wrap($candidate)
        .find(type === 'applicant' ? selectorDetails.bookmarkApplicantLabel : selectorDetails.bookmarkTalentLabel)
        .should('exist');
    });
};

export const selectUnavailableTalentByName = (talentName) => {
  cy.contains('[data-cy=hidden-candidate-list-name]', talentName).click();
};

export const validateUnavailableTalentNotExist = (talentName) => {
  cy.contains('[data-cy=hidden-candidate-list-name]', talentName).should('not.exist');
};

export const validateUnavailableTalentLeftPanelInfo = (talentName) => {
  cy.log('Validate Unavailable Talent ListItem Info in LHS panel');
  cy.contains(selectorDetails.candidateListItem, talentName)
    .invoke('text')
    .should('contain', 'This talent is no longer available');
};

export const validateUnavailableTalentRightPanelInfo = (text, assertion = 'contain') => {
  cy.log('Validate Unavailable Talent ListItem Info in RHS panel');
  cy.get('[data-cy=hidden-candidate-info]').should(assertion, text);
};

export const validateBookmarkRibbonNotExistFor = (name) => {
  cy.log(`Should not show bookmark ribbon for ${name} talent`);
  // LHS
  cy.contains(selectorDetails.candidateListItem, name)
    .find('[data-cy=candidates-list-item-bookmark-indicator]')
    .should('not.exist');
  // RHS
  cy.get('[data-cy=hidden-candidate-info]').find('[data-cy=bookmark-candidate]').should('not.exist');
};
export const selectBookmarkedCandidatesFilterOptionBy = (optionName) => {
  cy.get('[data-cy=filter-slot-dropdown-saved-tab] div[class$="-container"]').click();
  cy.get('[data-cy=filter-slot-dropdown-saved-tab] div[id*=react-select]').contains(optionName).click();
};
export const validateApplicantsExist = () => {
  cy.get(selectorDetails.bookmarkApplicantLabel).should('exist');
};
export const validateTalentsExist = () => {
  cy.get(selectorDetails.bookmarkTalentLabel).should('exist');
};
export const validateApplicantsNotExist = () => {
  cy.get(selectorDetails.bookmarkApplicantLabel).should('not.exist');
};
export const validateTalentsNotExist = () => {
  cy.get(selectorDetails.bookmarkTalentLabel).should('not.exist');
};
