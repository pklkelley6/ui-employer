import faker from 'faker';
import {userInfos} from '../../support/users';

const workplaceCountry = '[data-cy="workplace-country"]';
const workplaceForeignAddress1 = '[data-cy="workplace-foreign-address1"]';
const workplaceForeignAddress2 = '[data-cy="workplace-foreign-address2"]';
const errorCard = '[data-cy="error-card"]';
export const workplacePostalCode = '#postal-code';
export const workplaceBlockOrHouseNumber = '#block-house-num';
export const workplaceStreetName = '#street-name';
export const workplaceBuildingName = '#building-name';
const typeDelay = 150;

export function verifyLabelAndDefaultValuesInWorkplaceDetailsTab(jobPostWorkplaceDetails) {
  cy.get('[data-cy="workplace-label"]').should('have.text', 'Workplace Address');
  cy.get('[value="local"]').should('be.checked');
  cy.get('#checkbox-sameLocation').should('be.checked');
  cy.get('[value="local"]').next().should('have.text', jobPostWorkplaceDetails.jobPostWorkLocation[0]);
  cy.get('[value="overseas"]').next().should('have.text', jobPostWorkplaceDetails.jobPostWorkLocation[1]);
  cy.get('[for="checkbox-sameLocation"]').should(
    'have.text',
    'The workplace address is the same as the company address',
  );
  cy.get('[for="checkbox-multipleLocation"]').should(
    'have.text',
    'This position involves multiple workplace locations in Singapore',
  );
  cy.get(workplacePostalCode).prev().should('have.text', 'Postal Code');
  cy.get(workplaceBlockOrHouseNumber).prev().should('have.text', 'Block/House No.');
  cy.get(workplaceStreetName).prev().should('have.text', 'Street Name');
  cy.get(workplaceBuildingName).prev().should('have.text', 'Building Name (optional)');
}
export function verifyEmptyLocalValuesInEditWorkplaceDetailsTab() {
  cy.get('#checkbox-sameLocation').should('not.be.checked');
  cy.get('#checkbox-multipleLocation').should('not.be.checked');
  cy.get(workplacePostalCode).should('have.text', '');
  cy.get(workplaceBlockOrHouseNumber).should('have.text', '');
  cy.get(workplaceStreetName).should('have.text', '');
  cy.get(workplaceBuildingName).should('have.text', '');
}
export function verifyEmptyOverseasValuesInEditWorkplaceDetailsTab() {
  cy.get(workplaceCountry).find('input').should('have.text', '');
  cy.get(workplaceForeignAddress1).find('input').should('have.text', '');
  cy.get(workplaceForeignAddress2).find('input').should('have.text', '');
}
export function verifyLabelAndDefaultValuesInEditWorkplaceDetailsTab(jobPostWorkplaceDetails) {
  cy.get('[data-cy="workplace-label"]').should('have.text', 'Workplace Address');
  cy.get('[value="local"]').should('be.checked');
  cy.get('#checkbox-sameLocation').should('not.be.checked');
  cy.get('[value="local"]').next().should('have.text', jobPostWorkplaceDetails.jobPostWorkLocation[0]);
  cy.get('[value="overseas"]').next().should('have.text', jobPostWorkplaceDetails.jobPostWorkLocation[1]);
  cy.get('[for="checkbox-sameLocation"]').should(
    'have.text',
    'The workplace address is the same as the company address',
  );
  cy.get('[for="checkbox-multipleLocation"]').should(
    'have.text',
    'This position involves multiple workplace locations in Singapore',
  );
  cy.get(workplacePostalCode).prev().should('have.text', 'Postal Code');
  cy.get(workplaceBlockOrHouseNumber).prev().should('have.text', 'Block/House No.');
  cy.get(workplaceStreetName).prev().should('have.text', 'Street Name');
  cy.get(workplaceBuildingName).prev().should('have.text', 'Building Name (optional)');
}
export function verifyFieldsOnSelectedOverseasOptionFromWorkplaceDetails() {
  cy.get(workplaceCountry).find('label').should('have.text', 'Country');
  cy.get(workplaceForeignAddress1).find('label').should('have.text', 'Overseas Address 1');
  cy.get(workplaceForeignAddress2).find('label').should('have.text', 'Overseas Address 2 (optional)');
}
export function checkMultipleLocationFromWorkplaceDetails() {
  cy.get('#checkbox-multipleLocation').check();
}
export function uncheckMultipleLocationFromWorkplaceDetails() {
  cy.get('#checkbox-multipleLocation').uncheck({force: true});
}
export function checkSameLocationFromWorkplaceDetails() {
  cy.get('#checkbox-sameLocation').check();
}
export function uncheckSameLocationFromWorkplaceDetails() {
  cy.get('#checkbox-sameLocation').uncheck({force: true});
}
export function verifyLocalWorkplaceDetailsReadonlyValues() {
  cy.get(workplacePostalCode).invoke('attr', 'readonly').should('contain', '');
  cy.get(workplaceBlockOrHouseNumber).invoke('attr', 'readonly').should('contain', '');
  cy.get(workplaceStreetName).invoke('attr', 'readonly').should('contain', '');
  cy.get(workplaceBuildingName).invoke('attr', 'readonly').should('contain', '');
}
export function selectOverseasOptionFromWorkplaceDetails() {
  cy.get('[value="overseas"]').click({
    force: true,
  });
  cy.get(errorCard).should('not.exist');
}
export function selectLocalWorkplaceOptionFromWorkplaceDetails() {
  cy.get('[value="local"]')
    .click({
      force: true,
    })
    .should('be.checked');
}
// Added typeDelay 150 max since it is flaky test.
export function fillLocalWorkplaceDetails(jobPostWorkplaceDetails) {
  cy.get('[data-cy="workplace-label"]').should('have.text', 'Workplace Address');
  cy.get('#checkbox-sameLocation').should('not.be.checked');
  cy.get(workplacePostalCode)
    .clear()
    .type(jobPostWorkplaceDetails.jobPostPostalCodeFillValue, {delay: typeDelay})
    .trigger('change')
    .should('have.value', jobPostWorkplaceDetails.jobPostPostalCodeFillValue);
  cy.get(workplaceBlockOrHouseNumber)
    .clear()
    .type(jobPostWorkplaceDetails.jobPostBlockOrHouseNoFillValue, {delay: typeDelay})
    .trigger('change')
    .should('have.value', jobPostWorkplaceDetails.jobPostBlockOrHouseNoFillValue);
  cy.get(workplaceStreetName)
    .clear()
    .type(jobPostWorkplaceDetails.jobPostStreetNameFillValue, {delay: typeDelay})
    .trigger('change')
    .should('have.value', jobPostWorkplaceDetails.jobPostStreetNameFillValue);
  cy.get(workplaceBuildingName)
    .clear()
    .type(jobPostWorkplaceDetails.jobPostBuildingNameFileValue, {delay: typeDelay})
    .trigger('change')
    .should('have.value', jobPostWorkplaceDetails.jobPostBuildingNameFileValue);
}
export function verifyLocalWorkplaceDetailValueExists(jobPostWorkplaceDetails) {
  cy.get(workplacePostalCode).should('have.value', jobPostWorkplaceDetails.jobPostPostalCodeFillValue);
  cy.get(workplaceBlockOrHouseNumber).should('have.value', jobPostWorkplaceDetails.jobPostBlockOrHouseNoFillValue);
  cy.get(workplaceStreetName).should('have.value', jobPostWorkplaceDetails.jobPostStreetNameFillValue);
  cy.get(workplaceBuildingName).should('have.value', jobPostWorkplaceDetails.jobPostBuildingNameFileValue);
}
export function fillMultipleWorkplaceDetails() {
  //Don't need to select the value for the checkbox Local as it's the default selected value
  cy.get('[data-cy="workplace-label"]').should('have.text', 'Workplace Address');
  cy.get('#checkbox-multipleLocation')
    .click({
      force: true,
    })
    .should('be.checked');
}
export function verifyMultipleWorkplaceDetailValues() {
  cy.get('[value="local"]').should('be.checked');
  cy.get('#checkbox-multipleLocation').should('be.checked');
  cy.get(workplacePostalCode).should('have.value', '');
  cy.get(workplaceBlockOrHouseNumber).should('have.value', '');
  cy.get(workplaceStreetName).should('have.value', '');
  cy.get(workplaceBuildingName).should('have.value', '');
}
export function fillOverseasWorkplaceDetails(jobPostWorkplaceDetails) {
  cy.get('[value="overseas"]').click({
    force: true,
  });
  cy.get(workplaceCountry).find('input').type(jobPostWorkplaceDetails.jobPostCountryFillValue, {
    force: true,
  });
  cy.get(workplaceCountry).find('div').contains(jobPostWorkplaceDetails.jobPostCountryFillValue).click();
  cy.get(workplaceForeignAddress1)
    .find('input')
    .type(jobPostWorkplaceDetails.jobPostOverseasAddress1FillValue)
    .should('have.value', jobPostWorkplaceDetails.jobPostOverseasAddress1FillValue);
  cy.get(workplaceForeignAddress2)
    .find('input')
    .type(jobPostWorkplaceDetails.jobPostOverseasAddress2FillValue)
    .should('have.value', jobPostWorkplaceDetails.jobPostOverseasAddress2FillValue);
}
export function verifyOverseasWorkplaceDetailValueExists(jobPostWorkplaceDetails) {
  cy.get('[value="overseas"]').should('be.checked');
  cy.get(workplaceCountry).find('input').should('have.value', jobPostWorkplaceDetails.jobPostCountryFillValue);
  cy.get(workplaceForeignAddress1)
    .find('input')
    .should('have.value', jobPostWorkplaceDetails.jobPostOverseasAddress1FillValue);
  cy.get(workplaceForeignAddress2)
    .find('input')
    .should('have.value', jobPostWorkplaceDetails.jobPostOverseasAddress2FillValue);
}
export function verifyPostalCodeValidation() {
  cy.get(workplacePostalCode).clear().blur();
  cy.get(`${workplacePostalCode}-error`).contains('Please fill this in');

  cy.get(workplacePostalCode).clear().type('123').blur();
  cy.get(`${workplacePostalCode}-error`).contains('This should be a 6-digit number');

  cy.get(workplacePostalCode)
    .clear()
    .type('555555', {
      delay: typeDelay,
    })
    .blur();
  cy.get(`${workplacePostalCode}-error`).contains('Please check your postal code');

  cy.log('Delaying the one map response by more than 2sec, it should fall back to 2 digit district validation.');
  const inputPostalCode = '991233';
  cy.intercept(
    {
      method: 'GET',
      url: `https://developers.onemap.sg/commonapi/search?searchVal=${inputPostalCode}**`,
    },
    {delay: 3000},
  );
  cy.get(workplacePostalCode)
    .clear()
    .type('991233', {
      delay: typeDelay,
    })
    .blur();
  cy.get(`${workplacePostalCode}-error`).contains('Please check your postal code');
  cy.log('149999 is an invalid postal code with valid first two digit');
  cy.get(workplacePostalCode)
    .clear()
    .type('149999', {
      delay: typeDelay,
    })
    .blur();
  cy.get(`${workplacePostalCode}-error`).should('exist');
}
export function verifyBlockOrHouseNumberValidation() {
  const BLOCK_OR_HOUSE_NUMBER_MAX_LENGTH = 10;
  cy.get(workplaceBlockOrHouseNumber).clear().blur();
  cy.get(`${workplaceBlockOrHouseNumber}-error`).contains('Please fill this in');

  cy.get(workplaceBlockOrHouseNumber)
    .fill(faker.random.alphaNumeric(BLOCK_OR_HOUSE_NUMBER_MAX_LENGTH + 1))
    .blur();
  cy.get(`${workplaceBlockOrHouseNumber}-error`).contains(
    `Please keep within ${BLOCK_OR_HOUSE_NUMBER_MAX_LENGTH} characters`,
  );

  cy.get(workplaceBlockOrHouseNumber).fill(faker.random.alphaNumeric(BLOCK_OR_HOUSE_NUMBER_MAX_LENGTH)).blur();
  cy.get(`${workplaceBlockOrHouseNumber}-error`).should('not.exist');
}
export function verifyStreetNameValidation() {
  const STREET_NAME_MAX_LENGTH = 32;
  cy.get(workplaceStreetName).clear().blur();
  cy.get(`${workplaceStreetName}-error`).contains('Please fill this in');

  cy.get(workplaceStreetName)
    .fill(faker.address.streetAddress().padEnd(STREET_NAME_MAX_LENGTH + 1, faker.lorem.word()))
    .blur();
  cy.get(`${workplaceStreetName}-error`).contains(`Please keep within ${STREET_NAME_MAX_LENGTH} characters`);

  cy.get(workplaceStreetName).fill(faker.address.streetAddress()).blur();
  cy.get(`${workplaceStreetName}-error`).should('not.exist');
}
export function verifyBuildingNameValidation() {
  const BUILDING_NAME_MAX_LENGTH = 66;
  cy.get(workplaceBuildingName)
    .fill(faker.lorem.words(20).substring(0, BUILDING_NAME_MAX_LENGTH + 1))
    .blur();
  cy.get(`${workplaceBuildingName}-error`).contains(`Please keep within ${BUILDING_NAME_MAX_LENGTH} characters`);

  cy.get(workplaceBuildingName).fill(faker.lorem.words(20).substring(0, BUILDING_NAME_MAX_LENGTH)).blur();
  cy.get(`${workplaceBuildingName}-error`).should('not.exist');
}
export function verifyLocalAddressFieldsValidation() {
  cy.get(`${workplacePostalCode}-error`).contains('Please fill this in');
  cy.get(`${workplaceBlockOrHouseNumber}-error`).contains('Please fill this in');
  cy.get(`${workplaceStreetName}-error`).contains('Please fill this in');
  cy.get(`${workplaceBuildingName}-error`).should('not.exist');
  cy.get(errorCard).should('exist');
}
export function verifyOverseasAddressEmptyFields() {
  cy.get('#country-error').contains('Please fill this in');
  cy.get('#foreignAddress1-error').contains('Please fill this in');
  cy.get('#foreignAddress2-error').should('not.exist');
  cy.get(errorCard).should('exist');
}

export function verifyOverseasAddressMaxLength() {
  const overseasAddressMaxLength = 60;
  cy.get(workplaceForeignAddress1)
    .find('input')
    .type('x'.repeat(overseasAddressMaxLength + 1))
    .blur();
  cy.get(workplaceForeignAddress2)
    .find('input')
    .type('x'.repeat(overseasAddressMaxLength + 1))
    .blur();
  cy.get('#foreignAddress1-error').contains(`Please keep within ${overseasAddressMaxLength} characters`);
  cy.get('#foreignAddress2-error').contains(`Please keep within ${overseasAddressMaxLength} characters`);
  cy.get(errorCard).should('exist');
}

export function verifyWorkplaceAddressBasedOnUENAndDisabledField(companyName) {
  const user = userInfos.find((user) => {
    return user.companyName === companyName;
  });
  const {entityPostalCode, entityBlockOrHouseNo, entityStreetName, entityBuildingName} = user;
  cy.get('#checkbox-sameLocation').should('be.checked');
  cy.shouldHaveValueAndReadOnlyAttr(workplacePostalCode, entityPostalCode);
  cy.shouldHaveValueAndReadOnlyAttr(workplaceBlockOrHouseNumber, entityBlockOrHouseNo);
  cy.shouldHaveValueAndReadOnlyAttr(workplaceStreetName, entityStreetName);
  cy.shouldHaveValueAndReadOnlyAttr(workplaceBuildingName, entityBuildingName);
}
