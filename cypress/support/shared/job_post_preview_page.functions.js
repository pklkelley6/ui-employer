const previewJobDescriptionSelector = '[data-cy=job-description_preview]';
const previewSkillsSelector = '[data-cy=skills_preview] [data-cy=skill-pill] span';
const previewKeyInformationSelector = '[data-cy=key-information_preview]';
const workplaceDetailsPreviewSelector = '[data-cy=workplace-details_preview]';
const numberFormat = new Intl.NumberFormat('en-SG');

export function previewUENCompanyDetails(previewUENCompanyDetails) {
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    'Company Name / UEN',
    previewUENCompanyDetails.previewUenOrCompanyName,
  );
  cy.verifyPreviewValue(previewJobDescriptionSelector, 'Industry', previewUENCompanyDetails.previewIndustry);
}
export function previewJobDescriptionDetails(previewJobDescriptionValue) {
  cy.verifyPreviewValue(previewJobDescriptionSelector, 'Job Title', previewJobDescriptionValue.jobTitle);
  cy.verifyPreviewValue(previewJobDescriptionSelector, 'Occupation', previewJobDescriptionValue.jobOccupationSelection);
  cy.verifyPreviewValue(
    previewJobDescriptionSelector,
    'Job Description & Requirements',
    previewJobDescriptionValue.jobDescriptionValueFill,
  );
}
export function previewSkillsDetails(previewSkillsValue) {
  cy.verifyAllSelectedValue(previewSkillsSelector, previewSkillsValue);
}
export function previewKeyInformationDetails(previewKeyInformationValue) {
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Job Post Duration',
    previewKeyInformationValue.previewJobPostDurationValue,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Number of Vacancies',
    previewKeyInformationValue.jobPostNoOfVacancyValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Job Function',
    previewKeyInformationValue.jobPostCategoryPreviewValue,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Position Level',
    previewKeyInformationValue.jobPostPositionLevelValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Minimum Years of Experience',
    previewKeyInformationValue.jobPostMinNoOfExpValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Employment Type',
    previewKeyInformationValue.jobPostEmploymentTypeValueFill,
  );
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Minimum Qualification Level',
    previewKeyInformationValue.jobPostMinOfQualificationValueFill,
  );

  if (previewKeyInformationValue.jobPostFieldOfStudyValueFill.includes('Blank')) {
    cy.get(previewKeyInformationSelector).contains('Field of Study').should('not.exist');
  } else {
    cy.verifyPreviewValue(
      previewKeyInformationSelector,
      'Field of Study',
      previewKeyInformationValue.jobPostFieldOfStudyValueFill,
    );
  }
  cy.verifyPreviewValue(
    previewKeyInformationSelector,
    'Monthly Salary Range (SGD)',
    `$${numberFormat.format(previewKeyInformationValue.jobPostSalaryMinValueFill)} - ${numberFormat.format(
      previewKeyInformationValue.jobPostSalaryMaxValueFill,
    )}`,
  );
}

export function previewWorkplaceDetails(previewWorkplaceValue) {
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Workplace Address',
    previewWorkplaceValue.jobPostWorkLocationValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Postal Code',
    previewWorkplaceValue.jobPostPostalCodeFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Block/House No.',
    previewWorkplaceValue.jobPostBlockOrHouseNoFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Street Name',
    previewWorkplaceValue.jobPostStreetNameFillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Building Name (optional)',
    previewWorkplaceValue.jobPostBuildingNameFileValue,
  );
}
export function previewOverseasWorkPlaceDetails(previewWorkplaceValue) {
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Workplace Address',
    previewWorkplaceValue.jobPostWorkLocationValue,
  );
  cy.verifyPreviewValue(workplaceDetailsPreviewSelector, 'Country', previewWorkplaceValue.jobPostCountryFillValue);
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Overseas Address 1',
    previewWorkplaceValue.jobPostOverseasAddress1FillValue,
  );
  cy.verifyPreviewValue(
    workplaceDetailsPreviewSelector,
    'Overseas Address 2',
    previewWorkplaceValue.jobPostOverseasAddress2FillValue,
  );
}
export function verifyLinkAndColorCodeEnable(jobPostTabDetails) {
  const label = jobPostTabDetails['label'].replace(/\s+/g, '-');
  cy.get(`[data-cy=${label}-link]`)
    .should('have.class', 'blue')
    .should('have.attr', 'href', jobPostTabDetails['link'])
    .find('div')
    .contains(jobPostTabDetails['label']);
}

export function verifyLinkAndColorCodeDisable(jobPostTabDetails) {
  const label = jobPostTabDetails['label'].replace(/\s+/g, '-');
  cy.get(`[data-cy=${label}-link]`)
    .should('have.class', 'disabled-link')
    .should('have.attr', 'href', jobPostTabDetails['link'])
    .find('div')
    .contains(jobPostTabDetails['label']);
}

export function reviewPagesOnJobPosting() {
  cy.get('[data-cy="job-description"]').find('h4').should('contain', 'JOB DESCRIPTION');
  cy.get('[data-cy="skills"]').find('h4').should('contain', 'SKILLS');
  cy.get('[data-cy="key-information"]').find('h4').should('contain', 'KEY INFORMATION');
  cy.get('[data-cy="workplace-details"]').find('h4').should('contain', 'WORKPLACE DETAILS');
}

export function verifyEditLinkOptionOnJobPosting() {
  cy.get('[data-cy="job-description"]')
    .find('a')
    .should('have.attr', 'href', '/#job-description')
    .should('have.text', 'Edit');
  cy.get('[data-cy="skills"]').find('a').should('have.attr', 'href', '/#skills').should('have.text', 'Edit');
  cy.get('[data-cy="key-information"]')
    .find('a')
    .should('have.attr', 'href', '/#key-information')
    .should('have.text', 'Edit');
  cy.get('[data-cy="workplace-details"]')
    .find('a')
    .should('have.attr', 'href', '/#workplace-details')
    .should('have.text', 'Edit');
}

export const clickOnLinkIn = (selector) => {
  cy.log('click on link by' + selector);
  cy.get(`[data-cy="${selector}"]`).find('a').click();
};

export const verifyGovernmentSupportScheme = (scheme, assertion = 'contain') => {
  cy.contains('div', 'Government Support').should(assertion, scheme);
};

export const clickSubmitJobPost = () => {
  cy.get('[data-cy=new-post-next]').click();
};
