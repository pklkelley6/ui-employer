import {DOCUMENT_DOWNLOAD_ALIAS} from '../../support/commands/common.commands';

const selector = {
  Search: '[data-cy=search-button]',
  Cancel: 'button[data-cy=invite-to-apply-modal-cancel]',
  Send: 'button[data-cy*=invite-to-apply-modal-send-button]',
  Ok: 'button[data-cy=invite-to-apply-modal-ok-button]',
  'Invite to Apply': 'button[data-cy*=invite-talent-apply-button]',
};

export const talentSearch = {
  openTalentSearchPage() {
    cy.get('[data-cy=talent-search-label]').click();
    cy.url().should('contain', '/talent-search');
    cy.get('[data-cy=search-input]').should('be.visible');
  },

  validateTalentSearchResultMessages(messages = [], assertion = 'exist') {
    cy.get('[data-cy=talent-search-results]').then((el) => {
      messages.forEach((message) => {
        cy.wrap(el).should('contain', message).should(assertion);
      });
    });
  },

  inputAndClickSearchButton(inputText) {
    cy.get('[data-cy=search-input]').scrollIntoView().clear().type(inputText);
    cy.get(selector.Search).click();
  },

  validateTalentSearchResultIs(assertion) {
    cy.get('[data-cy=talent-search-results-list] div').should(assertion);
  },

  selectTalentByName(talentName) {
    return cy.contains('[data-cy="candidates-list-item"]', talentName).click();
  },

  validateTalentLabelOnLHS(talentItem) {
    cy.wrap(talentItem).find('[data-cy=Talent-label]').should('exist').and('contain', 'Talent');
  },

  validateTalentLabelOnRHS() {
    cy.get('[data-cy=candidate-right-panel-1]').find('[data-cy=Talent-label]').should('exist').and('contain', 'Talent');
  },

  validateTalentSearchResultCountIs(assertion) {
    cy.contains('[data-cy=talent-search-results-total]', /\d+ talen(t|ts) found/).should(assertion);
  },

  validateResumeButtonStatusAndDownloadOnEnabledInAllRHSTabs(assertion = 'enabled') {
    cy.log('validate resume status in all tab and download if enabled and validate download status');
    cy.get('div[id*=tab]').each((tab) => {
      cy.wrap(tab).click();
      if (assertion === 'disabled') {
        cy.get('[data-cy=candidates-info-no-resume-text]').should('have.text', 'No Resume Available');
      } else {
        cy.get('[data-cy=applicant-resume-download-button]')
          .scrollIntoView()
          .then((btn) => {
            cy.captureDocumentDownloadRequest().wrap(btn).click();
            cy.wait(`@${DOCUMENT_DOWNLOAD_ALIAS}`).its('response.statusCode').should('eq', 200);
          });
      }
    });
  },

  validateInviteToApplyButtonStatusIs(assertion = 'be.enabled') {
    cy.get(selector['Invite to Apply']).should('exist').should(assertion);
  },

  validateInviteToApplyToolTipIs(tooltipMessage) {
    cy.get(selector['Invite to Apply']).trigger('mouseover', {force: true});
    cy.get('#invite-talent-apply-tooltip')
      .scrollIntoView()
      .should('be.visible')
      .invoke('text')
      .should('contain', tooltipMessage);
  },

  clickOnButton(buttonName) {
    cy.get(selector[buttonName]).should('exist').scrollIntoView().click();
  },

  validateSendButtonIsDisabled() {
    cy.get(selector['Send']).invoke('attr', 'class').should('contain', 'cursor-not-allowed');
  },

  modalPopup: {
    validateExistence(assertion = 'be.visible') {
      cy.get('[data-cy=invite-to-apply-modal]').should(assertion);
    },

    shouldShowOpenJobDetailsAndViewJobsLink(jobDetails = {}) {
      cy.contains('[data-cy=job-to-invite-item]', jobDetails.jobPostId).then((el) => {
        cy.wrap(el).should('contain', jobDetails.jobTitle);
        cy.wrap(el).find('[data-cy=job-to-invite-view-job]').invoke('attr', 'href').should('contain', jobDetails.uuid);
      });
    },

    shouldNotShowClosedJobDetails(jobPostId) {
      cy.contains('[data-cy=job-to-invite-item]', jobPostId).should('not.exist');
    },

    selectAJob(jobPostId) {
      cy.get(`#job-to-invite-selection-checkbox-${jobPostId}`).check();
      cy.get(`#job-to-invite-selection-checkbox-${jobPostId}`).should('be.checked');
    },

    validateInvitedJobsInSuccessModalPopupAndClickOk(jobDetails = {}) {
      cy.contains('ul[class*=InvitationsSentList]', jobDetails.jobTitle).should('exist');
      cy.contains('ul[class*=InvitationsSentList]', jobDetails.jobPostId).should('exist');
    },

    validateInvitationSentMessageShowsForInvitedJob(jobPostId) {
      cy.contains('[data-cy=job-to-invite-item-sent]', jobPostId).then((el) => {
        cy.wrap(el).find('[data-cy=job-to-invite-selection-checkbox]').should('be.checked');
        cy.wrap(el).invoke('text').should('contain', 'Invitation sent');
      });
    },
  },
};
