import {filter, includes, times, xor} from 'lodash';
export const otherSkillSectionsSelector = '[data-cy=added-skills] [data-cy=skill-icon] span';
export const recommendedSkillSectionsSelector = '[data-cy=recommended-skills] [data-cy=skill-icon] span';
const inputSkillSearch = '[data-cy=input-job-skills] input';
const inputSkillsSection = '[data-cy=input-job-skills]';
const foundSkillBasedOnSearch = '[data-cy=found-skills] span';
const clearAllRecommendedSKillsLinkSelector = '[data-cy=recommended-skills] [data-cy=clear-all-skills]';
const clearAllAddMoreSKillsLinkSelector = '[data-cy=add-more-skills] [data-cy=clear-all-skills]';
const recommendedSKillsSelectedSelector = '[data-cy=recommended-skills] [data-cy=skill-icon][class*=skill-icon-tick]';
const recommendedSKillsUnSelectedSelector = '[data-cy=recommended-skills] [data-cy=skill-icon][class*=skill-icon-plus]';
const addMoreSKillsSelectedSelector = '[data-cy=added-skills] [data-cy=skill-icon][class*=skill-icon-tick]';
const addMoreSKillsUnSelectedSelector = '[data-cy=added-skills] [data-cy=skill-icon][class*=skill-icon-plus]';

export function removeDuplicationFrom(original) {
  const unique = filter(original, (val, i, iteratee) => includes(iteratee, val, i + 1));
  return xor(original, unique);
}

export function searchAndSelectAddMoreSkillSection(jobSkills) {
  cy.get(inputSkillSearch).type(jobSkills.searchSkill, {
    force: true,
  });
  cy.get('.lookup__menu').contains(jobSkills.searchSkillSelect).click();
  cy.wrap(
    Cypress.config('backendFeatureFlag')['allowMcfApiConstant'] && jobSkills.apiConstSelectSkillList
      ? jobSkills.apiConstSelectSkillList
      : jobSkills.selectSkillList,
  ).each((skill) => {
    cy.contains('[data-cy=found-skills] div[data-cy=skill-icon]', skill).click();
  });
}

export function searchAndSelectAllSkillsFromSearchBar(searchSkill, searchSkillSelect) {
  cy.get(inputSkillSearch).type(searchSkill, {
    force: true,
  });
  cy.get('.lookup__menu').contains(searchSkillSelect).click();
  cy.get(foundSkillBasedOnSearch, {
    timeout: 30000,
  }) //FLAKY. Some times take longer time to load and select job title
    .then((foundSkills) => {
      times(foundSkills.length, () => {
        cy.get(foundSkillBasedOnSearch).first().click();
      });
    });
}

export function shouldHaveAddMoreSkillsValueExists(otherAddedSkills) {
  cy.log('shouldHaveAddMoreSkillsValueExists', otherAddedSkills);
  cy.verifyAllSelectedValue(otherSkillSectionsSelector, otherAddedSkills);
}

export function verifyAutoPopulateRecommendedSkillsValue(jobPostRecommendedSkills, {exist: sameSkillsExist}) {
  if (sameSkillsExist) {
    cy.verifyAllSelectedValue(recommendedSkillSectionsSelector, jobPostRecommendedSkills);
  } else {
    cy.verifyAllSelectedValueNotExist(recommendedSkillSectionsSelector, jobPostRecommendedSkills);
  }
}

export function shouldCountMatchRecommendedSkills(count) {
  // NOTE: flaky
  cy.get(recommendedSkillSectionsSelector, {
    timeout: 10000,
  })
    .its('length')
    .should('be.eq', count);
}

export function unSelectSkillsFromRecommendedSection(unSelectRecommendedSkills) {
  cy.selectOrUnSelectSkills(recommendedSkillSectionsSelector, unSelectRecommendedSkills);
}

export function verifyUnSelectedSkillsFromRecommendedSection(unSelectedRecommendedSkillsValue) {
  cy.verifySelectedOrUnselectedSkills(recommendedSKillsUnSelectedSelector, unSelectedRecommendedSkillsValue);
}

export function verifySelectedSkillsFromRecommendedSection(selectedRecommendedSkillsValue) {
  cy.verifySelectedOrUnselectedSkills(recommendedSKillsSelectedSelector, selectedRecommendedSkillsValue);
}

export function unSelectSkillsFromAddMoreSection(unSelectAddMoreSkills) {
  cy.selectOrUnSelectSkills(otherSkillSectionsSelector, unSelectAddMoreSkills);
}

export function verifyUnSelectedSkillsFromAddMoreSection(unSelectAddMoreSkillsValue) {
  cy.verifySelectedOrUnselectedSkills(addMoreSKillsUnSelectedSelector, unSelectAddMoreSkillsValue);
}

export function verifySelectedSkillsFromAddMoreSection(selectedAddMoreSkillsValue) {
  cy.verifySelectedOrUnselectedSkills(addMoreSKillsSelectedSelector, selectedAddMoreSkillsValue);
}

export function clearAllSkillsFromRecommendedSection() {
  cy.clearAllSkills(clearAllRecommendedSKillsLinkSelector);
}

export function clearAllSkillsFromAddMoreSection() {
  cy.clearAllSkills(clearAllAddMoreSKillsLinkSelector);
}

export function verifyMinSkillsRequired() {
  cy.get('[data-cy="skills-error-title"]').find('span').should('have.text', 'Please add at least 10 skills');
  cy.get('[data-cy="error-card"]').should('exist');
}

export function addMaxSkills() {
  cy.log('it should show empty skills message when there are no found skills from the job title in the search bar');
  searchAndSelectAllSkillsFromSearchBar('Java', 'Java');
}

export function addMaxSkillsForTesting() {
  cy.log('it should show empty skills message when there are no found skills from the job title in the search bar');
  searchAndSelectAllSkillsFromSearchBar('Testing', 'Testing');
}

export function addOneMoreSkills() {
  cy.log('it should display growl notification when selecting more than maximum skills');
  cy.get('[data-cy="skills-error-title"]').should('not.exist');
  cy.get('[data-cy=recommended-skills] [data-cy=skill-icon] span').first().click();
}

export function verifyMaxSkillsAdded() {
  /**
   * An edge case where the user adds skills, goes back and change the job title,
   * then the new generated skills along with the added skills exceed the maximum limit.
   */
  cy.log('it should display error border if there are more than maximum skills selected');
  cy.get('[data-cy="skills-error-title"]').find('span').should('have.text', 'Please add no more than 20 skills');
  cy.get('[data-cy="error-card"]').should('exist');
}

export function skillsNotFound() {
  cy.log('it should display error in border if there are not skills found for job title');
  cy.get(inputSkillSearch).type('xyz', {
    force: true,
  });
  cy.get('.lookup__menu').contains('xyz').click();
  cy.get('[data-cy="found-skills-empty"]', {timeout: 20000}).should(
    'have.text',
    'No skills found. Check to see if relevant skills have already been displayed above, or try finding another skill.',
  );
  cy.get('[data-cy="found-skills-empty"]').should('exist');
}

export function relevantSkillsAddedAlready() {
  searchAndSelectAllSkillsFromSearchBar('Selenium', 'Selenium');
  cy.get('[data-cy="found-skills-empty"]').should(
    'have.text',
    'All relevant skills have already been added. Please try finding another skill.',
  );
  cy.get('[data-cy="found-skills-empty"]').should('exist');
}

export const selectedSkillsShouldNotExists = () => {
  cy.get(recommendedSKillsSelectedSelector).should('not.exist');
};

export const searchAndSelectXSkills = (keyword, numberOfSkills) => {
  cy.get(inputSkillSearch).type(keyword, {
    force: true,
  });
  cy.get('.lookup__menu').contains(keyword).click();
  cy.wrap(Cypress._.range(0, numberOfSkills)).each(() => {
    cy.get(foundSkillBasedOnSearch).eq(0).click();
  });
};

export const validateSkillsPageExists = () => {
  cy.log('Validate job creation - step 2 - skills page exists');
  cy.get(inputSkillsSection).should('exist');
};
