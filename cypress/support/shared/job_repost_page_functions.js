// This page contains collection of selectors and methods related to job repost function

const selectorDetails = {
  viewRepostJobButton: '[data-cy=repost-job-post-button]',
  repostJobButton: '[data-cy=repost-job]',
  repostJobDisableButton: '[data-cy=disabled-repost-job-post]',
  repostJobModal: '[data-cy=repost-jobpost-modal]',
  repostDurationDropdown: 'div[id=repost-jobpost-modal-duration-dropdown-select]',
  repostDurationList: 'div[id*=react-select]',
  modalRepostButton: '[data-cy=submit-button-repost-jobpost-modal]',
  modalCancelButton: '[data-cy=cancel-button-repost-jobpost-modal]',
  asideRepostCount: '[data-cy=repost-job-counter]',
  asideRepostDuration: '[data-cy=repost-duration-validity]',
  repostModalDurationDropDown: '#repost-jobpost-modal-duration-dropdown',
  repostCloseDateSelector: '[data-cy=repost-close-date]',
  repostAcknowledgementModal: '[data-cy=repost-acknowledgement]',
  repostAcknowledgementModalOKButton: '[data-cy=repost-acknowledgement] a',
  jobApplicantBar: 'div[class*=JobBar]',
};
export const jobPostDurationOptionValue = ['7 days', '14 days', '21 days', '30 days'];

export const repostJob = {
  clickViewRepostJobPost() {
    cy.log('Clicking View/Repost Job Post button ').then(() => {
      cy.get(selectorDetails.viewRepostJobButton).click();
    });
  },

  clickRepostJobButton() {
    cy.log('Clicking Repost Job button and validating modal popup is display').then(() => {
      cy.get(selectorDetails.repostJobButton).should('exist').click();
      cy.get(selectorDetails.repostJobModal).should('exist');
    });
  },

  inputRepostJobDuration(days) {
    cy.log(`Input ${days} in Duration dropdown`).then(() => {
      cy.get(selectorDetails.repostDurationDropdown).click();
      cy.contains(selectorDetails.repostDurationList, days).click();
    });
  },

  clickButtonInRepostJob(buttonName = 'Repost') {
    cy.log(`clicking ${buttonName} button in Modal popup`).then(() => {
      if (buttonName === 'Repost') {
        cy.get(selectorDetails.modalRepostButton).click();
      } else {
        cy.get(selectorDetails.modalCancelButton).click();
      }
    });
  },

  validateRepostJobDetailsInModal(autoCloseDate, repostLeft = '2 reposts') {
    cy.log('Validating repost job details - autoClose, repost count in Modal popup').then(() => {
      cy.get(selectorDetails.repostJobModal).then((modal) => {
        cy.wrap(modal).should('contain', autoCloseDate);
        cy.wrap(modal).should('contain', repostLeft);
      });
    });
  },

  validateRepostDetailsInAsideBar(repostLeft, repostTill) {
    cy.log('Validating repost job details - repost available till, repost count aside bar').then(() => {
      if (repostLeft) cy.get(selectorDetails.asideRepostCount).invoke('text').should('contain', repostLeft);
      if (repostTill) cy.get(selectorDetails.asideRepostDuration).invoke('text').should('contain', repostTill);
    });
  },

  validateRepostButtonStatus(assertion = 'enabled') {
    cy.log(`Validating Repost Job button ${assertion}`).then(() => {
      if (assertion === 'enabled') {
        cy.get(selectorDetails.repostJobButton).should('exist');
      } else {
        cy.get(selectorDetails.repostJobDisableButton).should('exist');
      }
    });
  },

  repostJobWithDays(days) {
    cy.log(`Reposting job with ${days}`).then(() => {
      this.clickRepostJobButton();
      if (days) this.inputRepostJobDuration(days);
      this.clickButtonInRepostJob();
    });
  },

  clickRepostModalDropDown() {
    cy.log('Clicking on duration dropdown in repost').then(() => {
      cy.get(selectorDetails.repostModalDurationDropDown).click();
    });
  },

  validateRepostModalDropdownListValue() {
    cy.log('Validate list of values from repost modal drop down').then(() => {
      jobPostDurationOptionValue.forEach(function (value) {
        cy.log(`Days display in modal popup ${value}`).then(() => {
          cy.get(selectorDetails.repostModalDurationDropDown).then((modal) => {
            cy.wrap(modal).should('contain', value);
          });
        });
      });
    });
  },

  validateRepostAcknowledgementAndClickOK(autoCloseDate) {
    cy.get(selectorDetails.repostAcknowledgementModal)
      .should('exist')
      .then((el) => {
        cy.wrap(el).should('contain', autoCloseDate);
        cy.get(selectorDetails.repostAcknowledgementModalOKButton).click();
        cy.get(selectorDetails.jobApplicantBar, {timeout: 30000}).should('exist'); // Wait till job Applicant page load.
      });
  },
};
