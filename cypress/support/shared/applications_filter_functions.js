/* eslint-disable cypress/no-unnecessary-waiting */
const filtersSelector = {
  Filter: '[data-cy=filter-modal-button]',
  Cancel: '[data-cy=filter-modal-cancel]',
  'Show Results': '[data-cy=filter-modal-show-results]',
  'Reset all filters': '[data-cy=filter-modal-reset-all-filters]',
  'review the filters': 'a:contains("review the filters")',
};

export const applicationsFilter = {
  clickOnButtonOrLink(buttonName) {
    cy.get(filtersSelector[buttonName]).click();
  },

  toggleTopMatchesSwitch(action = 'check') {
    cy.get('[data-cy=top-match-switch-toggle-checkbox]').then((el) => {
      if (action === 'check') {
        cy.wrap(el).check({force: true});
      } else {
        cy.wrap(el).uncheck({force: true});
      }
    });
  },

  checkOrUncheckApplicationStatus(applicationStatus = [], action = 'check') {
    applicationStatus.forEach((status) => {
      cy.contains('[data-cy=applicant-statuses-checkbox-filter] li', status)
        .find('[data-cy=applicant-status-selection-checkbox]')
        .then((el) => {
          if (action === 'check') {
            cy.wrap(el).check({force: true});
          } else {
            cy.wrap(el).uncheck({force: true});
          }
        });
    });
  },

  toggleScreenQuestionSwitch(action = 'check') {
    cy.get('[data-cy=screening-questions-switch-toggle-checkbox]').then((el) => {
      if (action === 'check') {
        cy.wrap(el).check({force: true});
      } else {
        cy.wrap(el).uncheck({force: true});
      }
    });
  },

  selectScreenQuestionResponse(responses = []) {
    responses.forEach((answer, index) => {
      cy.get(`[data-cy=screening-question-answer-${index + 1}]`)
        .find(`[data-cy=question-${index + 1}-${answer}]`)
        .then(() => {
          if (answer === 'yes') {
            cy.get(`#question-${index + 1}-yes`).click({
              force: true,
            });
          } else {
            cy.get(`#question-${index + 1}-no`).click({
              force: true,
            });
          }
        });
    });
  },

  verifyScreenQuestionSwitchExist(assertion = 'exist') {
    cy.get('[data-cy=screening-questions-switch-toggle-checkbox]').should(assertion);
  },

  verifyTopMatchesSwitchIs(assertion = 'to.be.checked') {
    cy.get('[data-cy=top-match-switch-toggle-checkbox]').should(assertion);
  },

  verifyApplicationStatusesCheckBoxAre(applicationStatus = [], assertion = 'to.be.checked') {
    applicationStatus.forEach((status) => {
      cy.contains('[data-cy=applicant-statuses-checkbox-filter] li', status)
        .find('[data-cy=applicant-status-selection-checkbox]')
        .should(assertion);
    });
  },

  applyTopMatchInFilter() {
    this.clickOnButtonOrLink('Filter');
    this.toggleTopMatchesSwitch('check');
    this.clickOnButtonOrLink('Show Results');
  },

  verifyDefaultFiltersValue() {
    cy.get('[data-cy=filter-modal] input[type=checkbox]').each((el) => {
      cy.wrap(el).should('not.to.be.checked');
    });
  },

  verify0ResultFoundAndLink() {
    this.verifyFilterResultMessageIs(['0 results found based on your filters.', 'Please review the filters applied.']);
    cy.contains('a', 'review the filters').should('exist').and('have.attr', 'href', '#');
  },

  verifyFilterResultMessageIs(messages = []) {
    cy.wrap(messages).each((message) => {
      cy.get('[data-cy=applications-view]').wait(1500).invoke('text').should('contain', message);
    });
  },

  verifyFiltersButtonAppliedTextIs(textAndAssertion = {}) {
    cy.get(filtersSelector.Filter)
      .invoke('text')
      .should(textAndAssertion.assertion ? textAndAssertion.assertion : 'contain', textAndAssertion.text);
  },

  verifyFilterResultsCountExistenceAndCountIs(assertion = 'not.exist', count) {
    cy.get('[data-cy=filtered-applications-results-total]').should(assertion);
    if (assertion === 'exist')
      // validate count if exists
      cy.get('[data-cy=filtered-applications-results-total]')
        .invoke('text')
        .should('contain', `${count} result${count === 1 ? '' : 's'} found`);
  },
};
