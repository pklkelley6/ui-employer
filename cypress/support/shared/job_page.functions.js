/// <reference types="Cypress" />

const jobSortOptionSelector = '[data-cy=jobs-sort-selection-dropdown] div[id*=react-select-]';
const jobSortDropDownSelector = '[data-cy=jobs-sort-selection-dropdown] div[id*=-select]';
const jobCloseCancelButtonSelector = 'button[data-cy=cancel-close-job]';
const jobCloseProceedButtonSelector = 'button[data-cy=proceed-close-job]';
const jobCloseReturnAllJobsSelector = 'div[data-cy=return-to-jobs]';
const jobPostViewEditSelector = '[data-cy=manage-job-edit-item]';
export const clickAndCheckDefaultSortOptions = (defaultJobSortOption) => {
  cy.log('Select sort dropdown and assert default sorting options:');
  cy.get(jobSortDropDownSelector).click();
  cy.verifyAllSelectedValue(jobSortOptionSelector, defaultJobSortOption);
  cy.get('[data-cy=sort-by]').click(); //to hide dropdown list
};

export const checkHasUserName = (username) => {
  cy.log('Check should have user name ' + username);
  cy.get('[data-cy=user-fullname]').invoke('text').should('equal', username);
};
export const checkHasCompanyName = (companyname) => {
  cy.log('Check should have company name ' + companyname);
  cy.get('[data-cy=company-name]').invoke('text').should('equal', companyname);
};
export const checkHasCompanyLogo = (logourl) => {
  cy.log('Check should have comany logo url ' + logourl);
  cy.get('[data-cy=company-details] img').should('have.attr', 'src', logourl);
};
export const checkHasSideBarAllJobs = () => {
  cy.log('Check should have All Jobs side label');
  cy.get('[data-cy=all-jobs-label]').contains('All Jobs');
};
//To be update with actual number of opening jobs
export const checkHasOpenTab = (numberOfOpenJobs) => {
  cy.log('Check should have OPEN tab');
  cy.get('#tab-1').contains('Open ' + numberOfOpenJobs);
};
export const checkHasClosedTab = (numberOfClosedJobs) => {
  cy.log('Check should have CLOSED tab');
  cy.get('#tab-2').contains('Closed ' + numberOfClosedJobs);
};
export const checkHasOpenTabAndCountSameAsJobListing = () => {
  cy.get('li[data-cy=job-list-item]', {
    timeout: 60000,
  })
    .its('length')
    .then((openJobsCount) => checkHasOpenTab(openJobsCount));
};
export const checkHasCloseTabAndCountSameAsJobListing = () => {
  cy.get('[data-cy=job-list]')
    .find('[data-cy=job-list-item]')
    .its('length')
    .then((closeJobsCount) => checkHasClosedTab(closeJobsCount));
};
export const checkHasSortByOption = () => {
  cy.log('Check should have Sort By:');
  cy.get('[data-cy=sort-by]').invoke('text').should('equal', 'Sort by:');
};
export const checkHasSortBySelected = (sortAlgo) => {
  cy.log('Check should have Sort By Selected at ' + sortAlgo);
  cy.get('[data-cy="jobs-sort-selection-dropdown"]').find('div').contains(sortAlgo);
};
export const validateJobListExistence = (jobListAssertion) => {
  const jobListItem = '[data-cy=job-list-item]';
  if (jobListAssertion === 'found') {
    cy.get(jobListItem, {timeout: 60000}).should('exist');
  } else {
    cy.get(jobListItem, {timeout: 60000}).should('not.exist');
  }
};
export const check0JobList = () => {
  cy.contains('[data-cy=job-list] h1', 'There are currently no jobs to display.');
};
export const checkHasSearchBarAndSearchButton = () => {
  cy.log('Check should have search bar');
  cy.get('[data-cy=search-input]')
    .should('have.attr', 'placeholder', 'Search by job title or JOB-ID')
    .should('have.attr', 'type', 'text');
  cy.log('Check should have search button');
  cy.get('[data-cy=search-button]').should('have.attr', 'type', 'button');
};

export const validateJobSearchResultMessage = (searchMsgAssertion) => {
  const searchResults = '[data-cy="search-result"]';
  const resultMessage =
    searchMsgAssertion === 'found' ? /\d+ jo.*/ : 'No jobs found. Please try a different search term.';
  if (searchMsgAssertion == 'noMessage') {
    cy.get(searchResults).should('not.exist');
  } else {
    cy.contains(searchResults, resultMessage).should('exist');
  }
};

export const checkUnviewedBadge = (index, numOfUnviewed) => {
  cy.log('Verify the total number of unviewed');
  //Added timeout since site performance on pipeline integration test(so added explicit wait)
  cy.get('[data-cy="job-list-item"]', {timeout: 30000})
    .eq(index)
    .find('[data-cy="unviewed-badge"]', {timeout: 60000})
    .contains(numOfUnviewed);
};

export const verifyJobListingOrderIn = (jobIdList) => {
  cy.wrap(jobIdList).each((jobId, index) => {
    cy.get('[data-cy=job-list-item_job-id]').eq(index).should('have.text', jobId);
  });
};

export const verifyJobListField = ($job, $index) => {
  shouldHaveCardLoaderLoaded();
  shouldHaveApplicantsCountGet();
  cy.log('Verifying the job id');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="job-list-item_job-id"]')
    .invoke('text')
    .should('equal', $job.Id);
  cy.log('Verifying the job title');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="job-list-item_job-title"]')
    .invoke('text')
    .should('equal', $job.Title);
  cy.log('Verifying the date of post');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="job-list-item_job-post-date"]')
    .invoke('text')
    .should('equal', $job.PostDate);
  cy.log('Verifying the expiring date');
  cy.log($job.ExpiryDate + '-This is the expiry date');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="job-list-item_job-expiry-date"]')
    .invoke('text')
    .should('equal', $job.ExpiryDate);
  // FLAKY test, time taken for loading applicant count
  cy.log('Verifying the total number of applicants');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="total-count"]', {
      timeout: 30000,
    })
    .eq(0)
    .contains($job.Applicants);
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="total-count-title"]')
    .eq(0)
    .invoke('text')
    .should('equal', 'Applicants');
  cy.get('[data-cy="job-list-item"]')
    .eq($index)
    .find('[data-cy="total-count-title"]')
    .eq(1)
    .invoke('text')
    .should('equal', 'Suggested Talents');
  if ($job.Unviewed != 0) {
    cy.log('Verify the unviewed badge exist');
    cy.get('[data-cy="job-list-item"]').eq($index).find('[data-cy="unviewed-badge"]').should('exist');
  } else {
    cy.log('Verify the unviewed badge should not exist');
    cy.get('[data-cy="job-list-item"]').eq($index).find('[data-cy="unviewed-badge"]').should('not.exist');
  }
};

export function shouldHaveApplicantsCountGet() {
  //Workaround wait for extra time to check the applicants count should be loaded
  cy.get('[data-cy="job-list-item-applicant-count"] h4 div', {timeout: 20000}).should('not.exist');
}

export function shouldHaveCardLoaderLoaded() {
  // FLAKY: Sometime loading suggestant talents getting more than 30 seconds.
  cy.get('[data-cy="card-loader"]', {
    timeout: 50000,
  })
    .should('not.exist')
    .wait(500); // wait 500 ms to page render
}

export function shouldHaveCardLoaderLoading() {
  cy.get('[data-cy="card-loader"]').should('exist');
}

const GET_TIMEOUT = 30000;

/**
 * FIXME: Card loader could stay longer due to getting query back.
 *
 * This should be removed when the suggested talent list query time has reduced.
 */
export function cardLoaderShouldNotExist() {
  cy.get('[data-cy="card-loader"]', {
    timeout: GET_TIMEOUT,
  })
    .should('not.exist')
    .wait(1000); // waiting 1000 ms to make sure that after loader disappear UI has some time to render, due to async cypress some times fails.
}

export function getJobListSuggestedTalentCountWithIndex(index) {
  return cy
    .applicantsAndTalentsCountLoaderShouldBeLoaded()
    .get('[data-cy="job-list-item-suggested-talent-count"]')
    .eq(index)
    .find('[data-cy="total-count"]');
}

export const mouseoverOnManageMenuButton = () => {
  cy.get('[data-cy=manage-job-post-button]').trigger('mouseover');
};

export const clickOnCloseJobItemOption = () => {
  cy.get('[data-cy=manage-job-close-job-item]').click();
};

export const mouseoverOnExtendJobDisabledItemOption = () => {
  cy.get('[data-cy=manage-job-extend-item-disabled]').trigger('mouseover');
};

export const verifyTooltipOnExtendJobDisabledItemOption = () => {
  cy.get('[data-cy=manage-job-extend-item-disabled-tooltip]').contains(
    'The maximum duration has already been selected for this job',
  );
};

export const clickOnExtendJobItemOption = () => {
  cy.get('[data-cy=manage-job-extend-item]').click();
};

export const verifyExtendJobPostModalDropdownLabel = (jobDuration) => {
  cy.get('#extend-jobpost-modal-duration-dropdown label').contains(
    `Extend posting duration from ${jobDuration} days to:`,
  );
};

export const clickOnExtendJobPostModalDropdown = () => {
  cy.get('#extend-jobpost-modal-duration-dropdown').click();
};

export const clickOnExtendJobPostModalDropdownOption = (option) => {
  cy.get('#extend-jobpost-modal-duration-dropdown-select div[id*=react-select]')
    .contains(option)
    .trigger('mouseover', {force: true})
    .click({force: true}); //added force: true since it is not working if we removed it.
};

export const verifyExtendJobPostModalCancelButton = () => {
  cy.get('[data-cy=cancel-button-extend-jobpost-modal').contains('Cancel');
};

export const verifyExtendJobPostModalExtendButton = () => {
  cy.get('[data-cy=extend-button-extend-jobpost-modal').contains('Extend');
};

export const closeExtendJobPostModal = () => {
  cy.get('[data-cy=cancel-button-extend-jobpost-modal').click();
};

export const verifyOptionFieldsForJobClose = () => {
  cy.contains('please select one:');
  cy.contains('This position has been filled');
  cy.contains('This position has not been filled');
  cy.get(jobCloseCancelButtonSelector).should('have.text', 'Cancel');
  cy.get(jobCloseProceedButtonSelector).should('have.text', 'Proceed to Close');
};
export const validateErrorWhenSubmitWithoutOptionsForJobClose = () => {
  cy.clickButton(jobCloseProceedButtonSelector);
  cy.contains('Please select an option');
};
export const selectOptionForCloseJob = ($id) => {
  cy.get(`#${$id}`).click({
    force: true,
  });
};
export const submitCloseJob = () => {
  cy.clickButton(jobCloseProceedButtonSelector);
};
export const verifySuccessMessageForJobClose = () => {
  cy.contains('has been closed successfully', {
    timeout: 50000,
  });
  cy.contains('Return to All Jobs');
};
export const returnToAllJobs = () => {
  cy.get(jobCloseReturnAllJobsSelector).click();
};
export const validateClosedJobFoundInClosedTap = ($jobPostID) => {
  cy.contains($jobPostID);
};
export const verifyManageJobPostViewEdit = () => {
  cy.get(jobPostViewEditSelector).contains('View/Edit Job Details');
};
export const clickExtendJobPostModalExtendButton = () => {
  cy.get('[data-cy=extend-button-extend-jobpost-modal]').click();
};
export const verifyNewExtendJobPostDurationDate = ($newJobPostDurationDate) => {
  cy.contains('New auto-close date: ');
  cy.get('[data-cy=extend-jobpost-new-expiry-date]').contains($newJobPostDurationDate);
};
export const clickOkSuccessButtonExtendJobPostModel = () => {
  cy.get('[data-cy="success-button-extend-jobpost-modal"]').click();
};
export const verifyUpdatedNewExtendJobPostDurationLabel = ($newJobPostDurationDate) => {
  cy.contains('Closing');
  cy.get('[data-cy=job-post-expiry-date] time').contains($newJobPostDurationDate);
};
export const clickViewEditJobPostButton = () => {
  cy.get(jobPostViewEditSelector).contains('View/Edit Job Details').click();
};

export const searchAndOpenJobOnTab = (tabName, searchInput) => {
  cy.clickOnAllJobs().clickOnJobTab(tabName).searchForJobs(searchInput);
  cy.clickJobByJobPostID(searchInput);
  cy.skipApplicantsOnboardingIfExist();
};

export const closeJobWithOption = (option = 'Filled') => {
  // option: Available / Filled
  mouseoverOnManageMenuButton();
  clickOnCloseJobItemOption();
  selectOptionForCloseJob(option);
  submitCloseJob();
  returnToAllJobs();
};

export const clickEditJobPost = () => {
  cy.get('a[data-cy=edit-job-post]').click();
};

export const selectYesOrNoInEditJobModal = (select = 'Yes') => {
  if (select === 'Yes') {
    cy.get('button[data-cy=submit-button-edit-job-submit-modal]').click();
  } else {
    cy.get('button[data-cy=cancel-button-edit-job-submit-modal]').click();
  }
};

export const openJobPostEdit = (jobPostId) => {
  cy.searchForJobs(jobPostId);
  shouldHaveCardLoaderLoaded();
  cy.clickJobByJobPostID(jobPostId);
  cy.skipApplicantsOnboardingIfExist();
  shouldHaveCardLoaderLoaded();
  mouseoverOnManageMenuButton();
  verifyManageJobPostViewEdit();
  clickViewEditJobPostButton();
  shouldHaveCardLoaderLoaded();
};
