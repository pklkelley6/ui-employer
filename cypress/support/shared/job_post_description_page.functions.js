import faker from 'faker';

export const jobTitleSelector = '[data-cy="jobtitle"] #job-title';
export const jobOccupationSelector = '[data-cy="joboccupation"] #job-occupation';
export const jobDescriptionSelector = 'div[class="DraftEditor-root"] div[contenteditable="true"]';
export const jobTitleValidationErrorSelector = '[data-cy="jobtitle"] #job-title-error';
export const jobOccupationValidationErrorSelector = '[data-cy="joboccupation"] #job-occupation-error';
export const jobDescriptionValidationError = '[data-cy="text-editor-error"]';
const navigateJobDescriptionStepLink = 'a[data-cy=Job-Description-link]';

export const jobDescriptionShouldHaveValue = (value) => {
  cy.get(jobDescriptionSelector)
    .type('{selectall}{backspace}')
    .type(value)
    // NOTE: flaky
    .find('[data-text="true"]', {
      timeout: 30000,
    }) //add explicit wait since span tag newly populate when value enter in to textbox
    .should('have.text', value);
};

export function fillJobDescriptionStep(jobPost, {paste} = {paste: false}) {
  cy.get('#job-title')
    .clear()
    .then((el) => {
      paste ? cy.wrap(el).paste(jobPost.jobTitle) : cy.wrap(el).type(jobPost.jobTitle);
      cy.wrap(el).should('have.value', jobPost.jobTitle);
    });
  cy.get('[data-cy="joboccupation"]')
    .find('[placeholder="Search..."]')
    .clear()
    .then((el) => {
      paste
        ? cy.wrap(el).paste(jobPost.jobOccupationPartial)
        : cy.wrap(el).type(jobPost.jobOccupationPartial, {
            force: true,
          });
      cy.wrap(el).should('have.value', jobPost.jobOccupationPartial);
    });
  cy.get('div[role=listbox]').contains(jobPost.jobOccupationSelection).click();

  cy.log('Type Job description');
  cy.fillJobDescriptionValue(jobPost.jobDescriptionValueFill);
}
export function isEditableJobDescription() {
  cy.get(jobDescriptionSelector)
    .focus()
    .clear()
    .type('Test', {
      force: true,
    })
    .find('[data-text="true"]')
    .should('have.text', 'Test');
}

export function checkCustomizableTextIconForJobDescriptionFields(jobDescriptionTextAreaIcon) {
  cy.get('[data-cy="text-editor-toolbar"]')
    .find('button')
    .each(($el, index) => {
      expect($el.attr('class')).to.include(jobDescriptionTextAreaIcon[index]);
    });
}

export function checkCharacterCountShow(jobDescriptionText) {
  cy.fillJobDescriptionValue(jobDescriptionText);
  cy.get('[data-cy="text-editor-character-count"]').contains(jobDescriptionText.length);
  cy.get(jobDescriptionSelector).focus().clear().type('Test').find('[data-text="true"]').should('have.text', 'Test');
  cy.get('[data-cy="text-editor-character-count"]').contains('Test'.length);
}

export function checkCharacterCountExistsMax(jobPostDescription) {
  cy.fillJobDescriptionValue(jobPostDescription);
  cy.get('[data-cy="text-editor-character-count"]').contains(
    new Intl.NumberFormat('en-EN').format(jobPostDescription.length),
  );
}

export function shouldHaveJobDescriptionValueExists(jobPostJobDescription) {
  cy.get('#job-title').should('have.value', jobPostJobDescription.jobTitle);
  cy.get('[data-cy="joboccupation"]').find('input').should('have.value', jobPostJobDescription.jobOccupationSelection);
  cy.get('span[data-text="true"]').should('have.text', jobPostJobDescription.jobDescriptionValueFill);
}

export function verifyDefaultThirdPartyEmployerUnChecked() {
  cy.get('#third-party-employer-checkbox').should('not.to.be.checked');
}

export function verifyThirdPartyEmployerLabel(jobPostThirdPartyLabel) {
  cy.get('label[for=third-party-employer-checkbox]').should('have.text', jobPostThirdPartyLabel);
}

export function selectThirdPartyEmployerOption() {
  cy.get('#third-party-employer-checkbox')
    .click({
      force: true,
    })
    .should('be.checked');
}

export function unSelectThirdPartyEmployerOption() {
  cy.get('#third-party-employer-checkbox')
    .click({
      force: true,
    })
    .should('not.to.be.checked');
}

export function verifyThirdPartyEmployerFieldsLabel() {
  cy.get('div[data-cy=company-uen]').find('label').should('have.text', 'Company Name / UEN');
  cy.get('#industry').prev().should('have.text', 'Industry');
}

export function verifyThirdPartyEmployerEnabledFields() {
  cy.get('#entity-id').should('not.be.disabled');
  cy.get('#industry').invoke('attr', 'readonly').should('contain', '');
}

export function verifyThirdPartyEmployerFieldLabelNotExists() {
  cy.contains('label', 'Company Name / UEN').should('not.exist');
  cy.contains('label', 'Industry').should('not.exist');
}
export function verifyUENSelectionError(uenName) {
  cy.get('#entity-id-error').should('have.text', 'Please fill this in');
  cy.get('#entity-id').find('input').clear({force: true}).type(uenName, {force: true});
  cy.get('#entity-id-error', {timeout: 20000}).should('have.text', 'Please fill this in');
}
export function verifyUENSelectionIsValid(partialUEN, uenName) {
  cy.get('#entity-id').find('input').clear({force: true}).type(partialUEN, {force: true});
  cy.contains('[id*=react-select]', uenName, {timeout: 20000}).click({force: true});
  cy.get('#entity-id-error').should('not.exist');
}
export function shouldCheckedThirdPartyEmployerOption() {
  cy.get('#third-party-employer-checkbox').should('be.checked');
}

export const shouldHaveValueForUENDetailsInJobPostEditStep1Page = (jobPostEditDetails) => {
  cy.get('#entity-id').contains(jobPostEditDetails.previewUenOrCompanyName);
  cy.get('#industry')
    .should('have.value', jobPostEditDetails.previewIndustry)
    .invoke('attr', 'readonly')
    .should('contain', '');
};
export function navigateJobDescriptionStep() {
  cy.get(navigateJobDescriptionStepLink).click();
}
export const typeAndSelectUEN = (uenName) => {
  cy.get('.lookup__single-value').click();
  cy.get('.lookup__input input').type(uenName + '{enter}');
  cy.get('#entity-id-error').should('not.exist');
};

export const inputJobDescriptionStep = (
  {jobTitle, jobOccupationPartial, jobOccupationSelection, jobDescriptionValueFill} = {},
  {paste} = {paste: false},
) => {
  const inputDetails = {
    jobTitle: jobTitle ? jobTitle : faker.name.jobTitle(),
    jobOccupationPartial: jobOccupationPartial ? jobOccupationPartial : 'Senior Automation Engineer',
    jobOccupationSelection: jobOccupationSelection ? jobOccupationSelection : 'Senior Automation Engineer',
    jobDescriptionValueFill: jobDescriptionValueFill ? jobDescriptionValueFill : faker.lorem.sentences(10),
  };
  fillJobDescriptionStep(inputDetails, {paste: paste});
};
