export const contactSuggestedTalent = {
  shouldExistSuggestedTalentInviteToApplyButton() {
    cy.log('Should have Talents invite to apply button and tooltip on hover');
    cy.get('[data-cy=invite-talent-apply-button]').should('exist').and('contain', 'Invite to Apply');

    cy.get('[data-cy=invite-talent-apply-button]').trigger('mouseover');
    cy.get('#invite-talent-apply-tooltip')
      .scrollIntoView()
      .should('to.be.visible')
      .should(
        'contain',
        'Click to send this talent an email with the job description and invitation to apply for the job.',
      );
  },

  shouldNotExistSuggestedTalentInviteToApplyButton() {
    cy.log('Should not have Talents invite to apply button');
    cy.get('[data-cy=invite-talent-apply-button]').should('not.exist');
  },

  shouldExistInvitationSentButton() {
    cy.log('Should show Invitation sent button for the talent');
    cy.get('[data-cy=invite-talent-apply-button-sent]').should('exist').and('contain', 'Invitation sent');
  },

  shouldExistInvitedTalentTagInApplicantsLHSAndRHS(index) {
    cy.log('Should show Invited talent tag in LHS');
    cy.get('[data-cy=candidate-info]')
      .eq(index)
      .find('[data-cy=invited-talent-label]')
      .should('exist')
      .and('contain', 'INVITED TALENT');

    cy.log('Should show Invited talent tag in RHS');
    cy.get('[data-cy=candidate-right-panel-1-applicant-details]')
      .find('[data-cy=invited-talent-label]')
      .should('exist')
      .and('contain', 'INVITED TALENT');
  },

  clickInviteToApplyButton() {
    cy.get('[data-cy=invite-talent-apply-button]').click();
  },
};
