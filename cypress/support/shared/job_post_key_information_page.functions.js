import {mcf} from '@mcf/constants';
const keyJobCategorySelectionsSelector = '#job-categories-select div[class*=job-categories-search__multi-value__label]';
const keyJobPostDurationOptionSelector = '#job-post-duration-select div[id*=react-select]';
const schemeHyperlink = {
  // schemeId: hyperlink
  5: 'https://www.wsg.gov.sg/SGUnitedTraineeships-HostCompanies.html',
  6: 'https://www.wsg.gov.sg/SGUnitedMidCareerPathways-HostOrganisations.html',
};
const numberFormat = new Intl.NumberFormat('en-SG');

export const schemeDetails = {
  PCP: {
    schemeSelector: `[data-cy=scheme-name-${mcf.SCHEME_ID.PCP}]`,
    schemeLinkSelector: `[data-cy=scheme-link-${mcf.SCHEME_ID.PCP}]`,
    schemeSectionSelector: `[data-cy=scheme-${mcf.SCHEME_ID.PCP}]`,
    schemeValidateErrorSelector: '#subschemes-dropdown-error',
    labelText: 'Professional Conversion Programme',
    url: 'http://www.wsg.gov.sg/programmes-and-initiatives/professional-conversion-programmes-employers.html',
  },
  PMax: {
    schemeSelector: `[data-cy=scheme-name-${mcf.SCHEME_ID.P_MAX}]`,
    schemeLinkSelector: `[data-cy=scheme-link-${mcf.SCHEME_ID.P_MAX}]`,
    labelText: 'P-Max',
    url: 'http://www.wsg.gov.sg/programmes-and-initiatives/p-max-employer.html',
  },
  CareerTrial: {
    schemeSelector: `[data-cy=scheme-name-${mcf.SCHEME_ID.CAREER_TRIAL}]`,
    schemeLinkSelector: `[data-cy=scheme-link-${mcf.SCHEME_ID.CAREER_TRIAL}]`,
    labelText: 'Career Trial',
    url: 'http://www.wsg.gov.sg/programmes-and-initiatives/career-trial-employers.html',
  },
};

export function verifyJobPostDurationDisabledField() {
  cy.get('#job-post-duration-readonly').invoke('attr', 'readonly').should('contain', '');
}
export function verifyQualificationFieldOfStudyValidationsOnJobEditing(jobPostKeyLabel) {
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(jobPostKeyLabel.keyInformation.jobPostMinOfQualificationValueFill)
    .click();
  cy.get('#job-field-of-study-select input[id*=-input]').focus().blur();
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]').contains('No Formal Qualification').click();
  cy.get('#job-field-of-study-select').should('not.exist');
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(jobPostKeyLabel.keyInformation.jobPostMinOfQualificationValueFill)
    .click();
  cy.get('#job-field-of-study-select input[id*=-input]').focus().blur();
}
export function verifyJobCategoriesValidationsOnJobEditing() {
  cy.get('#job-categories-select input[id*=-input]').focus().blur();
  cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
  cy.get('#job-categories-select div[id*=react-select]').contains('Engineering').click();
  cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
  cy.get('#job-categories-select div[id*=react-select]').contains('General Work').click();
  cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
  cy.get('#job-categories-select div[id*=react-select]').contains('Entertainment').click();
  cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
  cy.get('#job-categories-select div[id*=react-select]').contains('Design').click();
  cy.get('#job-categories-error').contains('You can only add a maximum of 5 functions');
}
export function verifyJobDurationLabel(jobPostKeyLabel, {isJobDurationFieldEnable}) {
  if (isJobDurationFieldEnable) {
    cy.get('#job-post-duration > label').should('have.text', 'Job Post Duration');
  } else {
    cy.get('#job-post-duration-readonly').prev('label').should('have.text', 'Job Post Duration');
  }
}
export function verifyKeyInformationLabelOnJobPosting() {
  cy.get('#number-of-vacancies').prev().should('have.text', 'Number of Vacancies');
  cy.get('#job-categories-select').prev().should('have.text', 'Job Function (Max 5 functions)');
  cy.get('#job-position-level-select').prev().should('have.text', 'Position Level');
  cy.get('#job-minimum-years-of-experience').prev().should('have.text', 'Minimum Years of Experience');
  cy.get('#job-employment-type-select').prev().should('have.text', 'Employment Type');
  cy.get('#job-qualification-select').prev().should('have.text', 'Minimum Qualification Level');
  cy.get('[data-cy="job-month-salary-label"]').should('have.text', 'Monthly Salary Range (SGD)');
}
export function fillKeyInformationOnJobPosting(jobPostKey, isCreateJobPost = true) {
  if (isCreateJobPost) {
    cy.get('#job-post-duration-select').click();
    cy.verifyAllSelectedValue(keyJobPostDurationOptionSelector, jobPostKey.keyInformation.jobPostDurationValueCheck);
    cy.get('#job-post-duration-select div[id*=react-select]')
      .contains(jobPostKey.keyInformation.jobPostDurationValueFill)
      .trigger('mouseover', {force: true})
      .click();
  }
  cy.get('#number-of-vacancies')
    .clear()
    .type(jobPostKey.keyInformation.jobPostNoOfVacancyValueFill)
    .should('have.value', jobPostKey.keyInformation.jobPostNoOfVacancyValueFill);
  for (const jobPostCategoryValueSelect of jobPostKey.keyInformation.jobPostCategoryValueSelect) {
    cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
    cy.get('#job-categories-select div[id*=react-select]')
      .contains(jobPostCategoryValueSelect)
      .trigger('mouseover', {force: true})
      .click();
  }
  cy.get('#job-position-level-select').click();
  cy.get('#job-position-level-select div[id*=react-select]')
    .contains(jobPostKey.keyInformation.jobPostPositionLevelValueFill)
    .trigger('mouseover', {force: true})
    .click();
  cy.get('#job-minimum-years-of-experience')
    .clear()
    .type(jobPostKey.keyInformation.jobPostMinNoOfExpValueFill)
    .should('have.value', jobPostKey.keyInformation.jobPostMinNoOfExpValueFill);
  jobPostKey.keyInformation.jobPostEmploymentTypeValueSelect.forEach(function (jobPostEmploymentTypeValueSelect) {
    cy.get('#job-employment-type-select').click();
    cy.get('#job-employment-type-select div[id*=react-select]')
      .contains(jobPostEmploymentTypeValueSelect)
      .trigger('mouseover', {force: true})
      .click();
  });
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(jobPostKey.keyInformation.jobPostMinOfQualificationValueFill)
    .trigger('mouseover', {force: true})
    .click();
  cy.log("field of study will be filled up only when it does not contain 'Blank'");
  if (jobPostKey.keyInformation.jobPostFieldOfStudyValueFill.includes('Blank')) {
    cy.get('div[id="job-field-of-study-select"]').should('not.exist');
  } else {
    cy.get('[data-cy="field-of-study"] div[id*=select]').click();
    cy.get('[data-cy="field-of-study"] div[id*=select]')
      .find('div')
      .contains(jobPostKey.keyInformation.jobPostFieldOfStudyValueFill)
      .trigger('mouseover', {force: true})
      .click();
  }
  cy.get('input[placeholder="Min"]')
    .clear()
    .type(jobPostKey.keyInformation.jobPostSalaryMinValueFill)
    .should('have.value', numberFormat.format(jobPostKey.keyInformation.jobPostSalaryMinValueFill));
  cy.get('input[placeholder="Max"]')
    .clear()
    .type(jobPostKey.keyInformation.jobPostSalaryMaxValueFill)
    .should('have.value', numberFormat.format(jobPostKey.keyInformation.jobPostSalaryMaxValueFill));
}
export function verifyJobDurationValue(jobPostKey, {isJobDurationFieldEnable}) {
  if (isJobDurationFieldEnable) {
    cy.get('#job-post-duration-select input[id*="react-select"]')
      .prev()
      .should('have.text', jobPostKey.keyInformation.jobPostDurationValueFill);
  } else {
    cy.get('#job-post-duration-readonly').should('have.value', jobPostKey.keyInformation.jobPostDurationValueFill);
  }
}
export function verifyKeyInformationValueExists(jobPostKey) {
  cy.get('#number-of-vacancies').should('have.value', jobPostKey.keyInformation.jobPostNoOfVacancyValueFill);
  cy.verifyAllSelectedValue(keyJobCategorySelectionsSelector, jobPostKey.keyInformation.jobPostCategoryValueSelect);
  cy.get('#job-position-level-select input[id*="react-select"]')
    .prev()
    .should('have.text', jobPostKey.keyInformation.jobPostPositionLevelValueFill);
  cy.get('#job-minimum-years-of-experience').should('have.value', jobPostKey.keyInformation.jobPostMinNoOfExpValueFill);
  cy.verifyAllSelectedValue(
    '#job-employment-type-select div[class*="job-employment-type-select__multi-value__label"]',
    jobPostKey.keyInformation.jobPostEmploymentTypeValueFill.split(', '),
  );
  cy.get('#job-qualification-select input[id*="react-select"]')
    .prev()
    .should('have.text', jobPostKey.keyInformation.jobPostMinOfQualificationValueFill);
  cy.get('[data-cy="field-of-study"] div').contains(jobPostKey.keyInformation.jobPostFieldOfStudyValueFill);
  cy.get('input[placeholder="Min"]').should(
    'have.value',
    numberFormat.format(jobPostKey.keyInformation.jobPostSalaryMinValueFill),
  );
  cy.get('input[placeholder="Max"]').should(
    'have.value',
    numberFormat.format(jobPostKey.keyInformation.jobPostSalaryMaxValueFill),
  );
}
export function verifyJobPostDurationValidations() {
  cy.get('#job-post-duration-select input[id*=-input]').focus().blur();
  cy.get('#job-post-duration-error').contains('Please select an option');
}
export function verifyNumberOfVacanciesValidations() {
  const numberOfVacanciesError = '#number-of-vacancies-error';
  const numberOfVacanciesInput = '#number-of-vacancies';
  cy.get(numberOfVacanciesInput).focus().blur();
  cy.get(numberOfVacanciesError).contains('Please fill this in');
  cy.get(numberOfVacanciesInput).type('0');
  cy.get(numberOfVacanciesError).contains('This number should be more than 0');
  cy.get(numberOfVacanciesInput).clear().type('-1');
  cy.get(numberOfVacanciesError).contains('This number should be more than 0');
  cy.get(numberOfVacanciesInput).clear().type('-123');
  cy.get(numberOfVacanciesError).contains('This number should be more than 0');
  cy.get(numberOfVacanciesInput).clear().type('1000');
  cy.get(numberOfVacanciesError).contains('This number should not be more than 999');
  cy.get(numberOfVacanciesInput)
    .clear()
    .type(+12);
  cy.get(numberOfVacanciesInput).focus().blur();
  cy.get(numberOfVacanciesInput).should('have.attr', 'value', '12');
  cy.get(numberOfVacanciesError).should('not.exist');
}
export function verifyJobCategoriesValidations(jobPostKeyLabel) {
  cy.get('#job-categories-select input[id*=-input]').focus().blur();
  cy.get('#job-categories-error').contains('Please type and select');
  for (const jobPostCategoryValueSelect of jobPostKeyLabel.keyInformation.jobPostCategoryValueSelect) {
    cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
    cy.get('#job-categories-select div[id*=react-select]').contains(jobPostCategoryValueSelect).click();
  }
  cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
  cy.get('#job-categories-select div[id*=react-select]').contains('Design').click();
  cy.get('#job-categories-error').contains('You can only add a maximum of 5 functions');
}
export function verifyPositionLevelValidations() {
  cy.get('#job-position-level input[id*=-input]').focus().blur();
  cy.get('#job-position-level-error').contains('Please select an option');
}
export function verifyMinimumYearsExperienceValidations() {
  const jobMinimumYearsOfExperienceInput = '#job-minimum-years-of-experience';
  const jobMinimumYearsOfExperienceError = '#job-minimum-years-of-experience-error';
  cy.get(jobMinimumYearsOfExperienceInput).focus().blur();
  cy.get(jobMinimumYearsOfExperienceError).contains('Please fill this in');
  cy.get(jobMinimumYearsOfExperienceInput).type('-1');
  cy.get(jobMinimumYearsOfExperienceError).contains('Please enter a non-negative value');
  cy.get(jobMinimumYearsOfExperienceInput).clear().type('100');
  cy.get(jobMinimumYearsOfExperienceError).contains('This number should not be more than 99');
  cy.get(jobMinimumYearsOfExperienceInput).clear().type('0').should('have.value', '0');
  cy.get(jobMinimumYearsOfExperienceError).should('not.exist');
  cy.get(jobMinimumYearsOfExperienceInput)
    .clear()
    .type(+12)
    .should('have.value', '12');
  cy.get(jobMinimumYearsOfExperienceError).should('not.exist');
  // verify that minimum years of experience does not allow decimal value
  cy.get(jobMinimumYearsOfExperienceInput).clear().type('1.5').should('have.value', '1');
  cy.get(jobMinimumYearsOfExperienceError).should('not.exist');
}
export function verifyEmploymentTypeValidations() {
  cy.get('#job-employment-type-select input[id*=-input]').focus().blur();
  cy.get('#job-employment-type-error').contains('Please select an option');
}
export function verifyQualificationValidations() {
  cy.get('#job-qualification-select input[id*=-input]').focus().blur();
  cy.get('#job-qualification-error').contains('Please select an option');
}
export function verifyFieldOfStudyValidations(jobPostKeyLabel) {
  // need to get field of study to show up
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(jobPostKeyLabel.keyInformation.jobPostMinOfQualificationValueFill)
    .click();
  cy.get('#job-field-of-study-select input[id*=-input]').focus().blur();
  cy.get('#job-field-of-study-error').contains('Please type and select');
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]').contains('No Formal Qualification').click();
  cy.get('#job-field-of-study-select').should('not.exist');
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(jobPostKeyLabel.keyInformation.jobPostMinOfQualificationValueFill)
    .click();
  cy.get('#job-field-of-study-select input[id*=-input]').focus().blur();
  cy.get('#job-field-of-study-error').contains('Please type and select');
}
export function verifySalaryValidations() {
  const minimumSalaryInput = '#job-minimum-salary';
  const maximumSalaryInput = '#job-maximum-salary';
  const minimumSalaryError = '#job-minimum-salary-error';
  const maximumSalaryError = '#job-maximum-salary-error';
  const salaryError = '#job-salary-error';
  cy.get(minimumSalaryInput).focus().blur();
  cy.get(minimumSalaryError).contains('Please fill this in');
  cy.get(maximumSalaryInput).focus().blur();
  cy.get(maximumSalaryError).contains('Please fill this in');
  cy.get(minimumSalaryInput).type('0');
  cy.get(maximumSalaryInput).type('0');
  cy.get(minimumSalaryError).contains('This number should be more than 0');
  cy.get(maximumSalaryError).contains('This number should be more than 0');
  cy.get(minimumSalaryInput).clear().type('123456789');
  cy.get(maximumSalaryInput).clear().type('123456789');
  cy.get(minimumSalaryError).contains('This number should not be more than 8 digits');
  cy.get(maximumSalaryError).contains('This number should not be more than 8 digits');
  cy.get(minimumSalaryInput)
    .clear()
    .type(+555);
  cy.get(maximumSalaryInput)
    .clear()
    .type(+666);
  cy.get(minimumSalaryInput).focus().blur();
  cy.get(minimumSalaryInput).should('have.attr', 'value', '555');
  cy.get(maximumSalaryInput).should('have.attr', 'value', '666');
  cy.get(minimumSalaryError).should('not.exist');
  cy.get(maximumSalaryError).should('not.exist');
  cy.get(minimumSalaryInput).clear().type('2');
  cy.get(maximumSalaryInput).clear().type('1');
  cy.get(salaryError).contains('Please check that the Min is not greater than the Max');
  cy.get(maximumSalaryInput).clear().type('3');
  cy.get(salaryError).should('be.empty');
}

export function verifyScheme(schemeName) {
  const details = schemeDetails[schemeName];

  cy.get(details.schemeSelector).should('have.text', details.labelText);
  cy.get(details.schemeLinkSelector)
    .should('have.text', 'Learn more')
    .should('have.attr', 'target', '_blank')
    .should('have.attr', 'href', details.url);
}

export const verifySGUnitedSchemeDetails = (schemeName, schemeId) => {
  cy.get(`[data-cy=scheme-name-${schemeId}]`).should('have.text', schemeName);
  cy.get(`[data-cy=scheme-link-${schemeId}]`)
    .should('have.text', 'Learn more')
    .should('have.attr', 'target', '_blank')
    .should('have.attr', 'href', schemeHyperlink[schemeId]);
};

export const selectSGUnitedScheme = (schemeId) => {
  cy.get(`input[id=schemes-checkbox-${schemeId}]`).should('be.visible').check();
};

export const unselectSGUnitedScheme = (schemeId) => {
  cy.get(`input[id=schemes-checkbox-${schemeId}]`).should('be.visible').next().click(); // uncheck have flaky as its not triggering events, even when do force, its probably due to span DOM blocking
};

export function selectPCPScheme() {
  cy.get(schemeDetails['PCP'].schemeSectionSelector).find('input').click({force: true});
}
export function verifyPCPSchemeValidations() {
  cy.get(schemeDetails['PCP'].schemeValidateErrorSelector).should('have.text', 'Please fill this in');
}

export function verifySchemeNotExists(schemeName) {
  const selector = schemeDetails[schemeName].schemeSelector;
  cy.get(selector).should('not.exist');
}

export const verifySGUnitedSchemeNotExists = (schemeId) => {
  cy.get(`[data-cy=scheme-name-${schemeId}]`).should('not.exist');
};

export const verifySchemeSectionNotExists = () => {
  cy.get(`[data-cy="job-govt-optional-section"]`).should('not.exist');
};

export const inputKeyInformation = (
  {
    jobPostDuration,
    numberOfVacancies,
    jobCategory,
    positionLevel,
    minimumYearsOfExperience,
    employmentType,
    minimumQualificationLevel,
    fieldOfStudy,
    minMonthlySalary,
    maxMonthlySalary,
  } = {},
  options = {
    isCreate: true,
  },
) => {
  cy.log('Input Key Information');
  const inputDetails = {
    jobPostDuration: jobPostDuration ? jobPostDuration : '7 days',
    numberOfVacancies: numberOfVacancies ? numberOfVacancies : '10',
    jobCategory: jobCategory ? jobCategory : ['Accounting / Auditing / Taxation', 'Banking and Finance'],
    positionLevel: positionLevel ? positionLevel : 'Senior Management',
    minimumYearsOfExperience: minimumYearsOfExperience ? minimumYearsOfExperience : '10',
    employmentType: employmentType ? employmentType : ['Full Time', 'Permanent'],
    minimumQualificationLevel: minimumQualificationLevel ? minimumQualificationLevel : 'Doctorate or equivalent',
    fieldOfStudy: fieldOfStudy ? fieldOfStudy : 'Accountancy',
    minMonthlySalary: minMonthlySalary ? minMonthlySalary : '10000',
    maxMonthlySalary: maxMonthlySalary ? maxMonthlySalary : '12000',
  };
  if (options.isCreate) {
    // Job Post Duration
    cy.log('Input Job Post Duration');
    cy.get('#job-post-duration-select').click();
    cy.get('#job-post-duration-select div[id*=react-select]')
      .contains(inputDetails.jobPostDuration)
      .trigger('mouseover', {force: true})
      .click();
  }

  // Number of Vacancies
  cy.log('Input Number of Vacancies');
  cy.get('#number-of-vacancies').clear().type(inputDetails.numberOfVacancies);

  // Job Category
  cy.log('Input Job Category');
  cy.wrap(inputDetails.jobCategory).each((category) => {
    cy.get('#job-categories-select div[class*=job-categories-search__indicators]').click();
    cy.get('#job-categories-select div[id*=react-select]')
      .contains(category)
      .trigger('mouseover', {force: true})
      .click();
  });

  // Position Level
  cy.log('Input Position Level');
  cy.get('#job-position-level-select').click();
  cy.get('#job-position-level-select div[id*=react-select]')
    .contains(inputDetails.positionLevel)
    .trigger('mouseover', {force: true})
    .click();

  // Minimum Years of Experience
  cy.log('Input Minimum Years of Experience');
  cy.get('#job-minimum-years-of-experience').clear().type(inputDetails.minimumYearsOfExperience);

  // Employment Type
  cy.log('Input Employment Type');
  cy.wrap(inputDetails.employmentType).each((type) => {
    cy.get('#job-employment-type-select').click();
    cy.get('#job-employment-type-select div[id*=react-select]')
      .contains(type)
      .trigger('mouseover', {force: true})
      .click();
  });

  // Minimum Qualification Level
  cy.log('Input Minimum Qualification Level');
  cy.get('#job-qualification-select').click();
  cy.get('#job-qualification-select div[id*=react-select]')
    .contains(inputDetails.minimumQualificationLevel)
    .trigger('mouseover', {force: true})
    .click();

  // Field of Study
  cy.log('Input Field of Study');
  if (inputDetails.fieldOfStudy) {
    cy.get('[data-cy="field-of-study"] div[id*=select]').click();
    cy.get('[data-cy="field-of-study"] div[id*=select]')
      .find('div')
      .contains(inputDetails.fieldOfStudy)
      .trigger('mouseover', {force: true})
      .click();
  }

  // Monthly Salary Range
  cy.log('Input Monthly Salary Range');
  cy.get('input[placeholder="Min"]').clear().type(inputDetails.minMonthlySalary);

  cy.get('input[placeholder="Max"]').clear().type(inputDetails.maxMonthlySalary);
};
