import faker from 'faker';

export const fillValueAndBlur = (selector, value) => {
  cy.get(selector).clear().type(value).should('have.value', value).blur();
};
export const validationErrorTextExists = (selector, errorMessage) => {
  cy.get(selector).contains(errorMessage).should('exist');
};
export const validationSelectorNotExists = (selector) => {
  cy.get(selector).should('not.exist');
};
export function onClickNext() {
  cy.get('button').contains('Next').click();
  cy.shouldHaveFormLoaderLoaded();
}
export function onClickBack() {
  cy.get('button').contains('Back').click();
}

export function onClickSubmitJobPost() {
  cy.get('button').contains('Submit Job Post').click();
  cy.shouldHaveFormLoaderLoaded();
}
/**
 * This is for suppressing the popup whenever you refresh or navigate out of the jobPosting page,
 * since it causes timeout errors when a test fails.
 * For more information visit the link https://github.com/cypress-io/cypress/issues/2118
 */
export function suppressBeforeUnloadEvent() {
  Cypress.on('window:load', function (window) {
    const original = window.addEventListener;
    window.addEventListener = function (...args) {
      if (args && args[0] === 'beforeunload') {
        return;
      }
      return original.apply(this, args);
    };
  });
}

export function getPagination(index) {
  return cy.get('[data-cy="pagination-number"]').eq(index);
}

export function isCurrentActive(pagination) {
  cy.wrap(pagination).should('have.class', 'bg-secondary');
}

export function shouldHasPaginationButtonCount(count) {
  cy.get('[data-cy="pagination-number"]').should('have.length', count);
}
export function checkLogoutPage() {
  //Workaround with extra wait for the go back to home link to appear due to intermittent fail when no extra timeout added
  cy.get('#action-button', {
    timeout: 10000,
  })
    .contains('Go back to Home')
    .should('have.attr', 'href', '/');
  cy.get('#subtitle').invoke('text').should('equal', 'To review the candidates for your jobs, please log in again.');
  cy.get('#title').contains('You have successfully logged out');
}
export function checkTimeoutPage() {
  cy.get('#title').invoke('text').should('equal', 'You have successfully logged out');
}
export function validateLoginFirstTimeScreenFields() {
  cy.contains('Name').should('be.visible');
  cy.contains('Designation (optional)').should('be.visible');
  cy.contains('Designation (optional)')
    .next()
    .then(($input) => {
      expect($input).to.be.enabled;
    });
  cy.contains('Email Address').should('be.visible');
  cy.contains('Email Address')
    .next()
    .then(($input) => {
      expect($input).to.be.enabled;
    });
  cy.contains('Contact Number').should('be.visible');
  cy.contains('Contact Number')
    .next()
    .then(($input) => {
      expect($input).to.be.enabled;
    });
  //cy.contains('For third party employers only').should('be.visible');
  cy.contains('Terms and Conditions').should('be.visible');
  cy.contains('Terms of Use')
    .should('have.attr', 'href')
    .and('include', 'https://employer.mycareersfuture.gov.sg/terms-of-use');
  cy.contains('Terms of Use').invoke('attr', 'target').should('include', '_blank');
  cy.contains('Tripartite Guideline on Fair Employment Practices')
    .should('have.attr', 'href')
    .and('include', 'https://www.tal.sg/tafep/getting-started/progressive/tripartite-standards');
  cy.contains('Tripartite Guideline on Fair Employment Practices').invoke('attr', 'target').and('include', '_blank');
  cy.contains('http://www.tafep.sg').should('have.attr', 'href').and('include', 'http://www.tafep.sg');
  cy.contains('http://www.tafep.sg').invoke('attr', 'target').and('include', '_blank');
}

export const inputAndValidateFirstTimeLoginPage = () => {
  cy.get('input#email').type(faker.internet.email());
  cy.get('input#contact-number').type(faker.phone.phoneNumber('6#######'));
  cy.get('label[for=terms-and-conditions]').click();
  cy.get('label[for=checkbox-tripartite]').click();

  cy.get('button[type=submit]').click();

  cy.url().should('include', '/jobs');
  cy.get('[data-cy=job-list-items]').should('exist');
};

export const verifyStaticNudgeContent = () => {
  cy.get('[data-cy="static-nudge"]').each(($el) => {
    cy.get($el).contains('Be sure to follow TAFEP fair advertisement guidelines');
    cy.get($el).contains('Write fair job advertisements. Avoid discriminatory phrases in your job posting.');
    cy.get($el)
      .contains('Learn More')
      .should('have.attr', 'href')
      .and('include', 'https://www.tal.sg/tafep/Employment-Practices/Recruitment/Job-Advertisements');
    cy.get($el).contains('Learn More').invoke('attr', 'target').and('include', '_blank');
  });
};
