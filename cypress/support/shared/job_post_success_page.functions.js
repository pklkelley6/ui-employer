import {shouldHaveCardLoaderLoading, cardLoaderShouldNotExist} from './job_page.functions';

export function verifyFieldsInAcknowledgementPage(jobPostSuccessfulFields, options = {isCreate: true}) {
  const successMessage = options.isCreate
    ? 'You have successfully posted the following job:'
    : 'You have successfully edited the following job:';
  cy.contains('div', successMessage);
  cy.get('[data-cy="success-job-title"]').should('have.text', jobPostSuccessfulFields.jobTitle);
  cy.get('[data-cy="success-job-expiry-date"]').contains('Auto-closes on:');
  cy.get('[data-cy="success-job-post-id"]').should(($div) => {
    const text = $div.text();
    expect(text).to.match(/^MCF-(\d){4}-(\d){7}/gi);
    expect(text).not.to.include('TEST');
  });
  cy.get('[data-cy="success-redirect-jobseeker"]').should('have.text', 'View Posted Job');
  cy.get('[data-cy="success-redirect-employer"]').should('have.text', 'Return to All Jobs');
}

export function getJobPostId() {
  return cy.get('[data-cy="success-job-post-id"]').invoke('text');
}

export function readLinkFromSuccessfulJobPostForJobSeekerPage() {
  return cy.get('a[data-cy="success-redirect-jobseeker"]').invoke('attr', 'href');
}

export function verifyJobPostPresentInAllJobsList(jobPostId, jobTitle) {
  cy.get('[data-cy="success-redirect-employer"]').click();
  cy.clickOnSortJobsBy('Posted Date');
  shouldHaveCardLoaderLoading();
  cardLoaderShouldNotExist();
  cy.contains('[data-cy=job-list-item-info]', jobPostId)
    .should('exist')
    .find('[data-cy="job-list-item_job-title"]')
    .should('have.text', jobTitle);
}
