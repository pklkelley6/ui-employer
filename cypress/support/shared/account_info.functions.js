/* eslint-disable no-prototype-builtins */
export const accountInfo = {
  openAccountInfoPage() {
    cy.shouldHaveCardLoaderLoaded();
    // click account info menu
    cy.get('a[data-cy=user-fullname]').trigger('mouseover');
    cy.get('.rc-tooltip-inner').should('be.visible');
    cy.get('#account-info-button').should('be.visible').click();
    cy.title().should('contain', 'Account Info');
    cy.url().should('contain', '/account-info');
    // page should loaded and menu should disappeared
    cy.get('#designation').should('exist');
    cy.get('a[data-cy=user-fullname]').should('exist');
    cy.get('#account-info-button').should('not.exist');
  },

  clickOnButton(buttonName) {
    cy.get(`[data-cy=account-info-${buttonName === 'Save' ? 'save' : 'cancel'}]`).click();
  },

  shouldShowSavedModalPopupAndRedirect() {
    cy.get('[data-cy=update-account-info-modal]')
      .should('exist')
      .invoke('text')
      .should('contain', 'Account Info Saved')
      .and('contain', 'You have successfully saved your account info.');
    cy.get('[data-cy=ok-button-redirect-account-info]').click();
    cy.get('[data-cy=search-input]').should('be.visible'); // redirect to /jobs page
  },

  inputAccountInfoDetails({designation, emailAddress, contactNumber}) {
    designation && cy.get('#designation').type(designation);
    emailAddress && cy.get('#email').type(emailAddress);
    contactNumber && cy.get('#contact-number').type(contactNumber);
  },

  clearAccountInfoFields(inputDetails = {}) {
    // inputDetails = {designation, emailAddress, contactNumber}
    inputDetails.hasOwnProperty('designation') && cy.get('#designation').clear().blur();
    inputDetails.hasOwnProperty('emailAddress') && cy.get('#email').clear().blur();
    inputDetails.hasOwnProperty('contactNumber') && cy.get('#contact-number').clear().blur();
  },

  validateAccountInfoDetails({name, designation, emailAddress, contactNumber}) {
    name && cy.contains('div[class*=db]', 'Name').next().invoke('text').should('contain', name);
    designation !== undefined && // optional field should able to validate blank input
      cy.get('#designation').invoke('attr', 'value').should('contain', designation);
    emailAddress && cy.get('#email').invoke('attr', 'value').should('contain', emailAddress);
    contactNumber && cy.get('#contact-number').invoke('attr', 'value').should('contain', contactNumber);
  },

  validateAccountInfoErrorMessages({emailAddress, contactNumber}) {
    emailAddress && cy.contains('#email-error', emailAddress).should('exist');
    contactNumber && cy.contains('#contact-number-error', contactNumber).should('exist');
  },
};
