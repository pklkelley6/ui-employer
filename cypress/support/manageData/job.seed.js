import faker from 'faker';
import {apiJobData} from '../../fixtures/seedData/api_job_post_seed_data';
import {apiJobs} from '../api/jobsApi';
import {couchDBApi} from '../api/couchDBApi';
const moment = require('moment');

export const validateAccountDetails = () => {
  cy.request({
    method: 'POST',
    url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/account`,
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((res) => {
    expect(res.status).to.eq(200);
    expect(res.body.authorization).to.exist;
    cy.wrap(res.body.authorization).each(({id, scopes}) => {
      cy.log(`${id} - ${scopes.join(';')}`);
    });
  });
};

export const seedXNewJobsForUEN = (numberOfJobs, payload, uen) => {
  return new Cypress.Promise((resolve) => {
    validateAccountDetails();
    const jobResponse = [];
    cy.wrap(Cypress._.range(0, numberOfJobs))
      .each(() => {
        if (!payload) {
          payload = apiJobData.defaultJobPost;
          payload.postedCompany.uen = uen;
        }
        apiJobs.createJob(payload).then((response) => {
          jobResponse.push(response);
          cy.log(response.metadata.jobPostId);
        });
      })
      .then(() => {
        resolve(jobResponse);
      });
  });
};

export const closeJobs = (jobResponse = [], isFilled) => {
  return new Cypress.Promise((resolve) => {
    cy.wrap(jobResponse).each((response) => {
      apiJobs.closeJob(response.uuid, isFilled);
    });
    resolve();
  });
};

export const seedXCloseJobsForUEN = (numberOfJobs, payload, uen, isFilled = false) => {
  return new Cypress.Promise((resolve) => {
    let newJobResponse = [];
    seedXNewJobsForUEN(numberOfJobs, payload, uen)
      .then((jobResponse) => {
        newJobResponse = jobResponse;
        closeJobs(jobResponse, isFilled);
      })
      .then(() => {
        resolve(newJobResponse);
      });
  });
};

export const seedNewJobInDB = (amendData = {}) => {
  return new Cypress.Promise((resolve) => {
    const jobToSeed = {
      ...defaultJobDetails.job,
      ...amendData.job,
    };
    const othersToSeed = {
      ...defaultJobDetails.others,
      ...amendData.others,
    };

    const columns = Object.keys(jobToSeed).join(', ');
    const values = Object.values(jobToSeed)
      .map((value) => (value !== null ? `'${value}'` : 'null')) // null check to avoid assigning null for boolean value.
      .join(', ');

    cy.task('jobDB', `INSERT INTO jobs (${columns}) VALUES (${values})`)
      .then(() => {
        othersToSeed.categories.forEach((category) => {
          seedJobCategory(jobToSeed.job_post_id, category);
        });

        othersToSeed.employmentTypes.forEach((employmentType) => {
          seedJobEmploymentType(jobToSeed.job_post_id, employmentType);
        });

        othersToSeed.positions.forEach((position) => {
          seedJobPosition(jobToSeed.job_post_id, position);
        });

        othersToSeed.districtIds.forEach((districtId) => {
          seedJobDistrict(jobToSeed.job_post_id, districtId);
        });

        othersToSeed.schemes.forEach((scheme) => {
          seedJobScheme(jobToSeed.job_post_id, scheme.scheme, scheme.sub_scheme || 0);
        });

        othersToSeed.skills.forEach((skill) => {
          seedJobSkill(skill, jobToSeed.job_post_id);
        });
      })
      .then(() => {
        resolve({
          job: jobToSeed,
          others: othersToSeed,
        });
      });
  });
};

export const seedXNewJobsInDB = (numberOfJobs, amendData = {}) => {
  return new Cypress.Promise((resolve) => {
    const data = [];
    cy.wrap(Cypress._.range(0, numberOfJobs))
      .each(() => {
        amendData.job.job_post_id = `MCF-1000-${faker.random.number({min: 1000000, max: 9999999})}`;
        amendData.job.uuid = faker.random.uuid().replace(/-/g, '');
        amendData.job.job_title = faker.name.jobTitle();
        seedNewJobInDB(amendData).then((responseData) => {
          data.push(responseData);
        });
      })
      .then(() => {
        resolve(data);
      });
  });
};

export const removeSeededJobInDB = (jobPostId) => {
  const condition = `job_post_id = '${jobPostId}'`;
  cy.task('jobDB', `SELECT uuid FROM jobs WHERE ${condition};`).then((results) => {
    couchDBApi.deleteApplicantsForJob(results[0].uuid);
    cy.task('jobDB', `DELETE FROM suggested_talents_email_invitations WHERE job_uuid = '${results[0].uuid}';`);
  });
  cy.task('jobDB', `DELETE FROM job_category WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM job_employment_type WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM job_position WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM job_district WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM job_scheme WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM job_skill WHERE ${condition};`);
  cy.task('jobDB', `DELETE FROM jobs WHERE ${condition}`);
  cy.task('jobDB', `DELETE FROM audit_trails WHERE ${condition}`);
};

export const removeJobsByPostedUen = (postedUen) => {
  cy.task('jobDB', `SELECT job_post_id FROM jobs where posted_uen = '${postedUen}';`).then((results) => {
    cy.wrap(results).each((result) => {
      removeSeededJobInDB(result.job_post_id);
    });
  });
};

const defaultJobDetails = {
  job: {
    // key naming are as same as job table column name
    job_post_id: `MCF-1000-${faker.random.number({min: 1000000, max: 9999999})}`,
    posted_uen: 'UEN',
    hiring_uen: null,
    new_posting_date: moment().format('YYYY-MM-DD'),
    original_posting_date: moment().format('YYYY-MM-DD'),
    expiry_date: moment().add(7, 'days').format('YYYY-MM-DD'),
    job_source_code: 'Employer Portal',
    job_status_id: '102',
    is_vacant: 0,
    job_title: faker.name.jobTitle(),
    job_description: faker.lorem.sentences(25),
    other_requirements: faker.lorem.sentences(10),
    ssoc_code: '3183',
    min_years_experience: 10,
    working_hours: null,
    job_shift_pattern_id: '31',
    number_of_vacancies: 1,
    min_monthly_salary: 10000,
    max_monthly_salary: 15000,
    is_overseas: 0,
    overseas_country: null,
    foreign_address1: null,
    foreign_address2: null,
    block: 100,
    street: 'NORTH BRIDGE ROAD',
    floor: '01',
    unit: '02',
    building: 'RAFFLES CITY TOWER',
    postal_code: 520858,
    is_hide_employer_name: 0,
    is_hide_hiring_employer_name: 0,
    is_posted_on_behalf: 0,
    is_hide_company_address: 0,
    is_hide_salary: 0,
    number_of_job_referral: 0,
    total_number_job_application: 0,
    created_at: moment().format('YYYY-MM-DD'),
    updated_at: moment().format('YYYY-MM-DD'),
    deleted_at: null,
    uuid: faker.random.uuid().replace(/-/g, ''),
    total_number_of_view: 0,
    psd_url: null,
    salary_type_id: '4',
    ssec_eqa: '92',
    ssec_fos: '0521',
    created_by: 'cypress-db-seed',
    updated_by: 'cypress-db-seed',
    deleted_by: null,
    edit_count: 0,
    repost_count: 0,
  },
  others: {
    categories: ['Customer Service'],
    employmentTypes: ['Permanent'],
    positions: ['Senior Management'],
    districtIds: [9],
    schemes: [],
    skills: [
      'Aerospace',
      'Java',
      'Java Application Development',
      'Java Enterprise Edition',
      'Java Software Development',
      'Java Web Services',
      'JavaBeans',
      'JavaSE',
      'Business Analysis',
      'Cost',
      'Court Reporting',
      'CQE',
      'Databases',
      'ERP',
      'Essential Oils',
      'Fashion Blogging',
      'Heuristic Evaluation',
    ],
  },
};

const seedJobCategory = (jobPostId, category) => {
  cy.task('jobDB', `SELECT id FROM categories WHERE category = '${category}'`).then(($data) => {
    cy.task('jobDB', `INSERT INTO job_category (job_post_id,category_id) VALUES ('${jobPostId}',${$data[0].id});`);
  });
};

const seedJobEmploymentType = (jobPostId, employmentType) => {
  cy.task('jobDB', `SELECT id FROM employment_types WHERE employment_type = '${employmentType}'`).then(($data) => {
    cy.task(
      'jobDB',
      `INSERT INTO job_employment_type (job_post_id,employment_type_id) VALUES ('${jobPostId}', ${$data[0].id});`,
    );
  });
};

const seedJobPosition = (jobPostId, position) => {
  cy.task('jobDB', `SELECT id FROM position_levels WHERE position = '${position}'`).then(($data) => {
    cy.task('jobDB', `INSERT INTO job_position (job_post_id,position_id) VALUES ('${jobPostId}', ${$data[0].id});`);
  });
};

const seedJobDistrict = (jobPostId, districtId) => {
  cy.task('jobDB', `INSERT INTO job_district (job_post_id,district_id) VALUES ('${jobPostId}', ${districtId});`);
};

const seedJobScheme = (jobPostId, scheme, subScheme = 0) => {
  let subSchemeId = 0;
  if (subSchemeId != 0) {
    cy.task('jobDB', `SELECT id FROM schemes where programme = '${subScheme}';`).then(($subSchemeID) => {
      subSchemeId = $subSchemeID;
    });
  }
  cy.task('jobDB', `SELECT id FROM schemes where scheme = '${scheme}';`).then(($schemeID) => {
    cy.task(
      'jobDB',
      `INSERT INTO job_scheme (job_post_id, scheme_id, sub_scheme_id) VALUES ('${jobPostId}', ${$schemeID[0].id}, ${subSchemeId});`,
    );
  });
};

const seedJobSkill = (skill, jobPostId, confidence = '5.0000') => {
  cy.task('jobDB', `select uuid from skill where skill = '${skill}';`).then(($data) => {
    cy.task(
      'jobDB',
      `INSERT INTO  job_skill (job_post_id, skill_uuid, confidence) VALUES('${jobPostId}', '${$data[0].uuid}', ${confidence});`,
    );
  });
};
