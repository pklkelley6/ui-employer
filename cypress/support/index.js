// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './login';
import './logout';
import './home';
import './commands/job_page.commands';
import './commands/job_post_creation.commands';
import './commands/manage_applicants.commands';
import './commands/common.commands';
import moment from 'moment';
import 'cypress-fail-fast';
import 'cypress-mochawesome-reporter/register';

// Alternatively you can use CommonJS syntax:
// require('./commands')

Cypress.on('fail', (error) => {
  throw new Error(`${moment().format('YYYY-MM-DDTHH:mm:ss.SSS')} -  ${error}`);
});

Cypress.on('uncaught:exception', (error) => {
  const errorsToExclude = [
    /Network error: Response not successful: Received status code (\d){3}/, // we expect apollo client to throw network error when failed to fetch graphql request
    'Document update conflict',
    'You are denied from accessing',
    'Unexpected token < in JSON at position 0',
    /Job application .+ is not found/,
    'Cannot read property',
  ];
  for (const errorMessage of errorsToExclude) {
    if (new RegExp(errorMessage).test(error.message)) {
      return false;
    }
  }
  throw new Error(`${moment().format('YYYY-MM-DDTHH:mm:ss.SSS')} -  ${error}`);
});
