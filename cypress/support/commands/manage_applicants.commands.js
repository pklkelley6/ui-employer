Cypress.Commands.add('clickOnCandidate', (index) => {
  cy.log('clicking on the applicants with index ' + index);

  cy.get('[data-cy="candidates-list-item"]', {timeout: 30000}).eq(index).scrollIntoView().click();
  cy.get('div[data-cy*=right-panel]').should('be.visible');
});

Cypress.Commands.add('clickOnAllJobs', (location = 'left') => {
  cy.log('clicking on the all jobs located at ' + location);
  cy.get(location === 'top' ? '[data-cy="all-jobs-top-label"]' : '[data-cy="all-jobs-label"]')
    .click()
    .wait(500); // to avoid DOM detached issue in subsequent actions (clicking on tabs, buttons in /jobs page) due to async adding 500ms delay.
});

Cypress.Commands.add('clickOnSortBy', (sortBy) => {
  cy.log('Click on sort by dropdown box and select ' + sortBy);
  cy.get('[data-cy="applications-sort-criteria-dropdown"]').click();
  cy.get('div[id*=react-select]').contains(sortBy).click();
  cy.shouldHaveFormLoaderLoaded();
});

Cypress.Commands.add('skipApplicantsOnboarding', () => {
  cy.log('clicking on skip button in the applicant onboarding ');
  cy.get('[data-cy="skip-button"]').click();
});

Cypress.Commands.add('skipApplicantsOnboardingIfExist', () => {
  const skipButton = '[data-cy="skip-button"]';
  cy.contains('[data-cy=job-post-id]', /(MCF|JOB)-.*-.*/).should('exist'); // job should be loaded
  cy.log('clicking on skip button if exist in the applicant onboarding ');
  cy.shouldHaveCardLoaderLoaded()
    .get('#modal-root')
    .then((modal) => {
      if (modal.find(skipButton).length > 0) cy.get(skipButton).click();
    });
});

Cypress.Commands.add('clickCloseButtonOnSurveyBar', () => {
  cy.log('clicking on close button on survey bar ');
  cy.get('[data-cy="surveybar-close-button"]').click();
});

Cypress.Commands.add('clickSurveyButtonOnSurveyBar', () => {
  cy.log('clicking on take survey button on survey bar ');
  cy.get('[data-cy="surveybar-take-survey-button"]').click();
});

Cypress.Commands.add('clickOnSuggestedTalentTab', () => {
  cy.log('clicking on the Suggested Talents tab');
  cy.get('#tab-suggested-talents').click();
});

Cypress.Commands.add('clickOnApplicantTab', () => {
  cy.log('clicking on the Applicants tab');
  cy.get('#tab-applications').click();
});

Cypress.Commands.add('clickOnSavedTab', () => {
  cy.log('clicking on the Saved tab');
  cy.get('#tab-saved').click();
});

Cypress.Commands.add('clickOnTopMatchCheckbox', () => {
  cy.log('Click on top matches checkbox');
  cy.get('[data-cy="top-match-checkbox"]').check({force: true});
});

Cypress.Commands.add('clickTabOnCandidateInfo', (tabName) => {
  cy.log(`Clicking on ${tabName} in CandidateInfo right panel`);
  cy.get(`[title="${tabName}"]`).scrollIntoView().click();
});

Cypress.Commands.add('validateEducationLabelAndTooltipIfExist', (searchInput, assertion) => {
  cy.log(`Validating education label existence, and label name, tooltip if exists`);
  cy.contains('[data-cy=education-history-field]', searchInput)
    .should('exist')
    .find('[data-cy=verified-label]')
    .should(assertion);

  if (assertion === 'exist') {
    cy.get('[data-cy=verified-label]').then(($lb) => {
      cy.wrap($lb).should('contain', 'VERIFIED');
      cy.wrap($lb).trigger('mouseover');
      cy.get('[data-cy="verified-label-tooltip"]')
        .should('to.be.visible')
        .should(
          'contain',
          'This certification has been verified. It has been awarded by the educational institution in the stated year.',
        );
    });
  }
});

Cypress.Commands.add('clickUnviewedCandidateByIndex', (index = 0) => {
  cy.log(`clicking unviewed candidate by index ${index}`);
  cy.get('[data-cy=candidates-list-item] > div:not([class*=CandidatesList__card-viewed])').eq(index).click();
  cy.get('div[data-cy*=right-panel]').should('be.visible');
});

Cypress.Commands.add('clickOnMassApplicantCheckbox', () => {
  cy.log('clicked on mass applicant checkbox');
  cy.get('[data-cy="mass-application-selection-checkbox"]').click().wait(250); // wait 250ms for browser UI to render
});

Cypress.Commands.add('checkedMassApplicantCheckbox', () => {
  cy.log('checked mass applicant checkbox');
  cy.get('[data-cy="mass-application-selection-checkbox"]').check();
});

Cypress.Commands.add('uncheckedMassApplicantCheckbox', () => {
  cy.log('Unchecking mass applicant checkbox');
  cy.get('[data-cy="mass-application-selection-checkbox"]').uncheck();
});

Cypress.Commands.add('clickApplicantCheckboxByIndex', (index = 0) => {
  cy.log(`clicking applicant checkbox by index ${index}`);
  cy.get('[data-cy="candidate-checkbox"]').eq(index).click();
});
