Cypress.Commands.add('clearAllSkills', (selector) => {
  cy.get(selector).click();
});

Cypress.Commands.add('selectOrUnSelectSkills', (selector, expectedSkills) => {
  expectedSkills.forEach(function (value) {
    cy.get(selector).contains(value).click();
  });
});

Cypress.Commands.add('verifySelectedOrUnselectedSkills', (selector, expectedSkills) => {
  const skillsValueSection = [];
  cy.get(selector, {
    timeout: 20000,
  })
    .each(($skillButton) => {
      skillsValueSection.push($skillButton.text());
    })
    .then(() => {
      expect(skillsValueSection).to.have.members(expectedSkills);
    });
});

Cypress.Commands.add('clickNewJobPosting', () => {
  cy.get('[data-cy="create-job-posting-button-sidebar"]').click({
    force: true,
  });
});
Cypress.Commands.add('shouldHaveFormLoaderLoaded', () => {
  // FLAKY: add 30 seconds timeout since wcc getting delay to generate recommended skills
  cy.get('[data-cy=form-loader]', {timeout: 30000}).should('not.exist');
});
// Added this method to fill job description(300 char) by invoke text and trigger change instead of typing one by one char
Cypress.Commands.add('fillJobDescriptionValue', (descriptionValue) => {
  cy.get('div[class="DraftEditor-root"] div[contenteditable="true"]').clear().paste(descriptionValue);
});
