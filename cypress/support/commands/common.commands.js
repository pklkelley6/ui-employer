import {getOptions} from '../home';
import * as employerFeatureFlag from '../../fixtures/featureFlags/employer.json';

export const OPEN_JOB_REQUEST_ALIAS = 'openJobRequest';
export const CLOSE_JOB_REQUEST_ALIAS = 'closeJobRequest';
export const GRAPHQL_REQUEST_ALIAS = 'graphqlRequest';
export const COMPANY_PROFILE_LOAD_ALIAS = 'companyProfileLoadRequest';
export const COMPANY_PROFILE_SAVE_ALIAS = 'companyProfileSaveRequest';
export const VIRUS_SCANNER_ALIAS = 'virusScannerRequest';
export const FEATURE_FLAG_ALIAS = 'featureFlagRequest';
export const DOCUMENT_DOWNLOAD_ALIAS = 'downloadDocumentRequest';
export const COMPANY_PROFILE_PAGE_LOAD_ALIAS = 'companyProfilePageLoad';
export const LOGIN_REQUEST_ALIAS = 'loginRequest';
export const TALENT_SEARCH_INVITE_ALIAS = 'talentSearchInviteRequest';
export const COMPANY_SCHEMES_ALIAS = 'companySchemesRequest';
export const INVITATION_REQUEST_ALIAS = 'invitationRequest';

// After login, store "mcf-employer-account-expiry" value in localStorage since navigate page without refresh & re-authenticate.
export const ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY = 'mcf-employer-account-expiry';

Cypress.Commands.add('captureOpenJobRequest', () => {
  cy.intercept({
    method: 'GET',
    url: '**/jobs?jobStatuses=[102,103*',
  }).as(OPEN_JOB_REQUEST_ALIAS);
});

Cypress.Commands.add('captureClosedJobRequest', () => {
  cy.intercept({
    method: 'GET',
    url: '**/jobs?jobStatuses=[9*',
  }).as(CLOSE_JOB_REQUEST_ALIAS);
});

Cypress.Commands.add('captureGraphqlRequest', () => {
  cy.intercept({
    method: 'POST',
    url: '**/profile',
  }).as(GRAPHQL_REQUEST_ALIAS);
});

Cypress.Commands.add('captureCompanyProfileLoad', () => {
  cy.intercept({
    method: 'GET',
    url: '**/companies/**',
  }).as(COMPANY_PROFILE_LOAD_ALIAS);
});

Cypress.Commands.add('captureCompanyProfileSave', () => {
  cy.intercept({
    method: 'PATCH',
    url: '**/companies/**',
  }).as(COMPANY_PROFILE_SAVE_ALIAS);
});

Cypress.Commands.add('captureVirusScannerRequest', () => {
  cy.intercept({
    method: 'POST',
    url: Cypress.config('apiVirusScanner') + '/',
  }).as(VIRUS_SCANNER_ALIAS);
});

Cypress.Commands.add('captureDocumentDownloadRequest', () => {
  cy.intercept({
    method: 'GET',
    url: '**/documents/**',
  }).as(DOCUMENT_DOWNLOAD_ALIAS);
});

Cypress.Commands.add('captureCompanyProfilePageLoad', () => {
  cy.intercept('**/company-profile').as(COMPANY_PROFILE_PAGE_LOAD_ALIAS);
});

Cypress.Commands.add('captureLoginRequest', () => {
  cy.intercept('**/login**').as(LOGIN_REQUEST_ALIAS);
});

Cypress.Commands.add('captureTalentSearchInvite', () => {
  cy.intercept('**/invitations/batch').as(TALENT_SEARCH_INVITE_ALIAS);
});

Cypress.Commands.add('captureCompanySchemes', () => {
  cy.intercept('**/schemes').as(COMPANY_SCHEMES_ALIAS);
});

Cypress.Commands.add('captureInvitationsRequest', () => {
  cy.intercept('**/invitations?talent**').as(INVITATION_REQUEST_ALIAS);
});

Cypress.Commands.add('openCompanyProfilePage', () => {
  // Due to cypress open issue https://github.com/cypress-io/cypress/issues/7306
  // And to avoid DOM getting detaching, we are waiting until page and profile loads
  // And also ensuing the UI completely loaded by checking the visibility of the button.
  // This fix is temp, once Cypress address the issue will make the necessary changes.
  cy.captureCompanyProfilePageLoad()
    .captureCompanyProfileLoad()
    .visit('/company-profile')
    .wait(`@${COMPANY_PROFILE_PAGE_LOAD_ALIAS}`)
    .get('[data-cy=company-profile-save]')
    .should('be.visible');
});

Cypress.Commands.add('mockFeatureFlag', (featureFlagName, status = false) => {
  employerFeatureFlag[featureFlagName][Cypress.env('environment')] = status;
  cy.intercept(
    {
      method: 'GET',
      url: '**/features/employer/latest.json',
    },
    employerFeatureFlag,
  ).as(FEATURE_FLAG_ALIAS);
});

let perserveLocalStorageKeys = [];
Cypress.Commands.add('preserveLocalStorageOnce', (localStorageKey) => {
  perserveLocalStorageKeys.push(localStorageKey);
});

const clear = Cypress.LocalStorage.clear;
Cypress.LocalStorage.clear = (keys = []) => {
  if (perserveLocalStorageKeys.length > 0 && keys.length == 0) {
    const localStorageKeys = Object.keys(localStorage);
    const filteredKeys = localStorageKeys.filter((key) => perserveLocalStorageKeys.indexOf(key) < 0);
    perserveLocalStorageKeys = [];
    if (filteredKeys.length > 0) {
      return clear.apply(Cypress.LocalStorage, [filteredKeys]);
    }
  } else {
    return clear.apply(Cypress.LocalStorage, [keys]);
  }
};

Cypress.Commands.add('verifyAllSelectedValue', (selectedValueSelector, selectedValueFromFixture) => {
  const selectedValueInDropDown = [];
  cy.get(selectedValueSelector, {
    timeout: 20000,
  })
    .each(($el) => {
      selectedValueInDropDown.push($el.text());
    })
    .then(() => {
      expect(selectedValueFromFixture).to.have.members(selectedValueInDropDown);
    });
});

Cypress.Commands.add('verifyAllSelectedValueNotExist', (selectedValueSelector, selectedValueFromFixture) => {
  const selectedValueInDropDown = [];
  cy.get(selectedValueSelector, {
    timeout: 20000,
  })
    .each(($el) => {
      selectedValueInDropDown.push($el.text());
    })
    .then(() => {
      expect(selectedValueInDropDown).not.have.members(selectedValueFromFixture);
    });
});

Cypress.Commands.add('verifyPreviewValue', (selector, label, value) => {
  cy.get(selector).contains(label).next().should('have.text', value);
});

Cypress.Commands.add('waitGraphqlRequest', (operationName) => {
  const isAliases = Array.isArray(operationName) && operationName.length > 1;
  const waitAlias = isAliases ? operationName.map(() => `@${GRAPHQL_REQUEST_ALIAS}`) : `@${GRAPHQL_REQUEST_ALIAS}`;
  cy.wait(waitAlias).then((xhr) => {
    if (isAliases) {
      const requestOperationName = xhr.map(({request}) => request.body.operationName);
      const differences = operationName.filter((opName) => {
        const i = requestOperationName.indexOf(opName);
        if (i >= 0) {
          requestOperationName.splice(i, 1);
          return false;
        } else {
          return true;
        }
      });
      if (differences.length > 0) {
        return cy.waitGraphqlRequest(differences);
      }
    } else {
      if (xhr.request.body.operationName !== String(operationName)) {
        return cy.waitGraphqlRequest(operationName);
      }
    }
  });
});

Cypress.Commands.add('shouldHaveValueAndReadOnlyAttr', (selector, value) => {
  cy.get(selector).should('have.value', value).should('have.attr', 'readonly');
});

Cypress.Commands.add('waitForGrowlToDisappear', () => {
  cy.get('.Toastify__toast-body', {timeout: 20000}).should('not.exist');
});

Cypress.Commands.add('verifyNotificationGrowl', (message) => {
  cy.get('[role=alert]').should('have.text', message).should('exist');
  cy.waitForGrowlToDisappear();
});

Cypress.Commands.add('clickButton', (selector) => {
  cy.get(selector).click();
});

Cypress.Commands.add(
  'paste',
  {
    prevSubject: 'element',
  },
  (subject, pastePayload) => {
    Cypress.log({name: 'paste', message: pastePayload});

    // if element type=input, do the below hack. Somehow input paste event doesn't trigger propagate to trigger change event
    if (subject.is('input')) {
      subject[0].value = pastePayload;
      // type a space and then removing it to trigger the onChange event
      return cy.wrap(subject).type(' {backspace}');
    }

    const clipboardData = new DataTransfer();
    clipboardData.setData('text/plain', pastePayload);
    const pasteEvent = new ClipboardEvent('paste', {bubbles: true, cancelable: true, clipboardData});
    subject[0].dispatchEvent(pasteEvent);

    return subject;
  },
);

Cypress.Commands.add(
  'uploadFile',
  {
    prevSubject: 'element',
  },
  (subject, base64Content, filename) => {
    Cypress.log({name: 'uploadFile', message: filename});
    const blob = Cypress.Blob.base64StringToBlob(base64Content);
    const file = new File([blob], filename);
    const dataTransfer = new DataTransfer();
    dataTransfer.items.add(file);
    subject[0].files = dataTransfer.files;
    return cy.wrap(subject).trigger('change');
  },
);

Cypress.Commands.add(
  'fill',
  {
    prevSubject: 'element',
  },
  (subject, text, {paste = false} = {}) => {
    Cypress.log({name: 'fill', message: text});
    if (paste) {
      return cy.wrap(subject).clear().paste(text);
    }
    return cy.wrap(subject).clear().type(text);
  },
);

Cypress.Commands.add('mockGraphQL', (endPoint, stubResponses) => {
  cy.intercept('POST', `**/${endPoint}`, (req) => {
    const {body} = req;
    // eslint-disable-next-line no-prototype-builtins
    if (body.hasOwnProperty('operationName') && Object.keys(stubResponses).includes(req.body.operationName)) {
      const stubVariables =
        stubResponses[req.body.operationName].variables === undefined
          ? true
          : Cypress._.isMatch(req.body.variables, stubResponses[req.body.operationName].variables);
      if (stubVariables) {
        req.alias = `${req.body.operationName}_MOCK_GRAPHQL`;
        req.reply({
          body: stubResponses[req.body.operationName].response,
          statusCode: stubResponses[req.body.operationName].statusCode || 200,
        });
      }
    }
  });
});

Cypress.Commands.add('shouldHaveCardLoaderLoaded', () => {
  // FLAKY: Sometime loading suggestant talents getting more than 30 seconds.
  cy.get('[data-cy="card-loader"]', {
    timeout: 50000,
  })
    .should('not.exist')
    .wait(500); // wait 500 ms to page render
});

Cypress.Commands.overwrite('visit', (originalFn, url, options) => {
  return originalFn(url, options ? options : getOptions());
});

Cypress.Commands.add('reloadPageUntilElementContains', (selector, text, retry = 2) => {
  cy.log(`reloading page until contains ${text} `)
    .wrap(Cypress._.range(0, retry))
    .each(() => {
      cy.get(selector).then((el) => {
        if (!el.text().includes(text)) cy.reload();
      });
    });
});

Cypress.Commands.add('applicantsAndTalentsCountLoaderShouldBeLoaded', () => {
  cy.get('div[class*=JobList__spinner]').should('not.exist');
});
