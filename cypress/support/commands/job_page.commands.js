import {getPagination} from '../shared/common.functions';

Cypress.Commands.add('searchForJobs', (searchTerm) => {
  cy.log('search jobs with ' + searchTerm);
  if (searchTerm == '') {
    cy.get('[data-cy=search-input]').clear();
  } else {
    cy.get('[data-cy=search-input]').clear().scrollIntoView().type(searchTerm, {force: true});
  }
  cy.get('[data-cy=search-button]').click({force: true});
});
Cypress.Commands.add('clickOnJobItem', (index) => {
  cy.log('Click on the job post with index of ' + index);
  cy.get('[data-cy="job-list-item"]', {timeout: 30000}).eq(index).find('[data-cy="job-list-item_job-title"]').click();
});

Cypress.Commands.add('clickJobByJobPostID', (jobPostID) => {
  cy.log('Click on the job post by jobID ' + jobPostID);
  cy.get('[data-cy=job-list-item_job-id]').contains(jobPostID).click();
});

Cypress.Commands.add('clickOnJobItemSuggestedTalent', (index) => {
  cy.log('Click on the job post suggested talent with index of ' + index);
  cy.get('[data-cy="job-list-item-suggested-talent-count"]').eq(index).click();
});

Cypress.Commands.add('clickOnPaginationIndex', (index) => {
  cy.log('Click on the pagingation number in index ' + index);
  getPagination(index).click();
});

Cypress.Commands.add('clickOnPaginationBy', (number) => {
  cy.log(`Click on the pagingation by ${number}`);
  cy.contains('[data-cy=pagination-number]', number).click();
});

Cypress.Commands.add('clickOnJobTab', (status) => {
  cy.log('Click on the status to' + status);
  cy.get(status === 'Open' ? '#tab-1' : '#tab-2').click();
});

Cypress.Commands.add('clickOnSortJobsBy', (sortBy) => {
  cy.log('click on the sort by' + sortBy);
  cy.get('[data-cy=jobs-sort-selection-dropdown]').click();
  cy.get('div[id*=react-select]').contains(sortBy).click();
});

Cypress.Commands.add('getJobPostedDate', () => {
  cy.log('Getting Job Posted date').then(() => {
    cy.get('[data-cy=job-post-posted-date] time')
      .invoke('attr', 'datetime')
      .then((postedDateAttr) => {
        return postedDateAttr;
      });
  });
});

Cypress.Commands.add('validateJobStatus', (status) => {
  cy.log('Validating Job Status').then(() => {
    cy.reload() //For some reason Cypress refer below object from cache, so reloading to get correct page.
      .get('[data-cy=job-post-status]', {timeout: 30000})
      .scrollIntoView()
      .should('exist')
      .should('contain', status);
  });
});

Cypress.Commands.add('validateJobClosingDate', (autoCloseDate) => {
  cy.log('Validating Job auto-closing date').then(() => {
    cy.get('[data-cy=job-post-expiry-date]', {timeout: 30000})
      .scrollIntoView()
      .should('exist')
      .should('contain', autoCloseDate);
  });
});

Cypress.Commands.add('getUnviewedBadgeCountByIndex', (index) => {
  cy.log(`Getting the unviewed badge count by index ${index}`);
  cy.get('[data-cy="job-list-item"]')
    .eq(index)
    .find('[data-cy="unviewed-badge"]')
    .invoke('text')
    .then((count) => {
      return Number(count);
    });
});
