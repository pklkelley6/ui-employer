// This section contains collections of method to manage applications for the jobs via CouchDB API's
import {couchDBMockData} from '../../fixtures/seedData/couchdb_applicant_seed_data';

export const couchDBApi = {
  find(query) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('couchDbUrl')}/applications/_find`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: query,
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  update(updatePayLoad) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('couchDbUrl')}/applications/${updatePayLoad._id}?conflicts=true`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: updatePayLoad,
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  getDocumentUuid() {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'GET',
        url: `${Cypress.config('couchDbUrl')}/_uuids?count=1`,
        headers: {
          'Content-Type': 'application/json',
        },
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  createDocument(applicantMockData) {
    return new Cypress.Promise((resolve) => {
      this.getDocumentUuid().then((idResponse) => {
        const documentId = idResponse.uuids[0];
        applicantMockData._id = documentId;
        cy.request({
          method: 'PUT',
          url: `${Cypress.config('couchDbUrl')}/applications/${documentId}`,
          headers: {
            'Content-Type': 'application/json',
          },
          body: applicantMockData,
          timeout: 25000,
        }).then((response) => {
          resolve(response.body);
        });
      });
    });
  },

  bulkDocs(applicantsMockData = []) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('couchDbUrl')}/applications/_bulk_docs`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: {docs: applicantsMockData},
        timeout: 25000,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  seedBulkApplicants(numOfApplicants, jobUuid) {
    return new Cypress.Promise((resolve) => {
      const applicantsMockData = [];
      cy.wrap(Cypress._.range(0, numOfApplicants))
        .each(() => {
          applicantsMockData.push(couchDBMockData.defaultApplicantData(jobUuid));
        })
        .then(() => {
          cy.log(applicantsMockData);
          this.bulkDocs(applicantsMockData).then((response) => {
            resolve(response);
          });
        });
    });
  },

  seedBulkApplicantsWithDifferentStatus(applicantsStatus = [], jobUuid) {
    // applicantsStatus = [{numberOfApplications, statusId, statusDescription, topMatches, jobScreeningQuestionResponses}]
    return new Cypress.Promise((resolve) => {
      const applicantsMockData = [];
      cy.wrap(applicantsStatus)
        .each(({numberOfApplications, statusId, statusDescription, topMatches, jobScreeningQuestionResponses}) => {
          cy.wrap(Cypress._.range(0, numberOfApplications)).each(() => {
            let couchDBApplicantMockData = couchDBMockData.defaultApplicantData(jobUuid);
            couchDBApplicantMockData = {
              ...couchDBApplicantMockData,
              ...(statusId ? {statusId} : {}),
              ...(statusDescription ? {statusDescription} : {}),
              ...(topMatches
                ? {
                    scores: {
                      wcc: 0.7602,
                    },
                  }
                : {}),
              ...(jobScreeningQuestionResponses.length > 0
                ? {
                    jobScreeningQuestionResponses: [...jobScreeningQuestionResponses],
                  }
                : []),
            };
            applicantsMockData.push(couchDBApplicantMockData);
          });
        })
        .then(() => {
          cy.log(applicantsMockData);
          this.bulkDocs(applicantsMockData).then((response) => {
            resolve(response);
          });
        });
    });
  },

  deleteApplicantsForJob(jobUuid) {
    this.find({selector: {jobId: jobUuid}, limit: 99999}).then((responseBody) => {
      const bulkApplicants = [];
      cy.wrap(responseBody.docs)
        .each((doc) => {
          const applicant = {_id: doc._id, _rev: doc._rev, _deleted: true};
          bulkApplicants.push(applicant);
        })
        .then(() => {
          cy.log(bulkApplicants);
          this.bulkDocs(bulkApplicants);
        });
    });
  },
};
