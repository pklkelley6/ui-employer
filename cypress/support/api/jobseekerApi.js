/// <reference types="Cypress" />

// This section contains collections of method related to applying jobs via API.
// These methods can be used directly for applying jobs by passing Uuid. However, if the
// employer portal is logged in before calling these methods or if jobs are created using apiJobs,
// then the existing employer session will be logged out, need to re-login
// into the employer portal.
// TODO: application status update for applied job.

import faker from 'faker';
import moment from 'moment';
import {couchDBApi} from '../api/couchDBApi';

export const apiJobseeker = {
  getAccessToken(nric) {
    return new Cypress.Promise((resolve) => {
      const authCode = btoa(`{"userName": "${nric}"}`);
      cy.request({
        method: 'GET',
        url: `${Cypress.config('jobseekerAccount')}/sp/token?authCode=${authCode}`,
      }).then((response) => {
        resolve(response.headers['authorization']);
      });
    });
  },

  getIndividualId(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    id    }  }"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (JSON.stringify(response.body).includes('errors')) {
          this.createProfile(accessToken).then((response) => {
            resolve(response.body.data.profile.create.id);
          });
        } else {
          resolve(response.body.data.profile.id);
        }
      });
    });
  },

  createProfile(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
          "operationName":"createPersonalParticular",
          "variables":{
              "personalParticular":{
                  "name":"${faker.name.findName()}",
                  "preferredName":"${faker.name.firstName()}",
                  "email":"${faker.internet.email()}",
                  "employmentStatus":{"id":"1"},
                  "contactNumber":{
                      "areaCode":null,
                      "countryCode":"65",
                      "telephoneNumber":"91234567"
                    },"unemploymentSince":null
                }
            },
            "query":"mutation createPersonalParticular($personalParticular: ProfileInput!) {  profile {    create(input: $personalParticular) {      id    }  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  createEductionIfNot(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    academicQualifications {      id         }  }}"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.academicQualifications.length < 1) {
          this.createEducation(accessToken).then((response) => {
            resolve(response);
          });
        }
        resolve(response);
      });
    });
  },

  createEducation(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
          "operationName":"createEducation",
          "variables":{
              "education":{
                  "qualification":{
                      "qualificationAttainedCode":"70",
                      "fieldOfStudyCode":"1152"
                    },
                    "institution":"MIT",
                    "yearAttained":"2001-01-01",
                    "isHighest":true
                }
            },
            "query":"mutation createEducation($education: EducationInput!) {  education {    create(input: $education) {      id      qualification {        qualificationAttained        qualificationAttainedCode        fieldOfStudy        fieldOfStudyCode        name      }      institution      yearAttained      isHighest    }  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  getDocumentID(nric, accessToken, index = 0) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    documents {      id    }  }}"
    }`;

      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.documents.length < 1) {
          this.uploadDocument(accessToken).then((response) => {
            resolve(response.body.data.document.create.id);
          });
        } else {
          resolve(response.body.data.profile.documents[index].id);
        }
      });
    });
  },

  uploadDocument(accessToken) {
    return new Cypress.Promise((resolve) => {
      const documentName = 'functionalSample.pdf';
      cy.fixture(`resume/${documentName}`, 'base64').then((resumeBase64) => {
        const body = `{
            "operationName":"uploadDocument",
            "variables":{
                "base64File":"${resumeBase64}",
                "documentName":"${documentName}"
            },
            "query":"mutation uploadDocument($base64File: String!, $documentName: String!) {  document {    create(base64File: $base64File, documentName: $documentName) {      id      fileName      fileSize      isDefault      url      createdOn    }  }}"}`;
        this.profileTemplate(accessToken, body).then((response) => {
          resolve(response);
        });
      });
    });
  },

  createApplication(jobUuid, individualId, documentID, accessToken) {
    return new Cypress.Promise((resolve) => {
      const source = 'MCF';
      const body = `{
          "operationName":"createApplication",
          "variables":{
            "documentId":"${documentID}",
            "individualId":"${individualId}",
            "jobUuid":"${jobUuid}",
            "source":{"name":"${source}"}
          },
        "query":"mutation createApplication($documentId: ID!, $individualId: ID!, $jobUuid: String!, $source: JobApplicationSourceAndEvent!) {  createApplication(documentId: $documentId, individualId: $individualId, jobUuid: $jobUuid, source: $source) {    id  }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  postWorkExperienceIfNot(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName": "profileByNric",
        "variables": {
            "nric": "${nric}"
        },
        "query":"query profileByNric($nric: String!) {  profile: profileByNric(nric: $nric) {    employmentHistories {      id         }  }}"
    }`;
      this.profileTemplate(accessToken, body).then((response) => {
        if (response.body.data.profile.employmentHistories.length < 1) {
          this.postWorkExperience(accessToken).then((response) => {
            resolve(response);
          });
        }
        resolve(response);
      });
    });
  },

  postWorkExperience(accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"postWorkExperience",
        "variables": {
          "workExperience": {
            "isCurrentJob": true,
            "organizationName": "MCF",
            "jobTitle": "Senior Software Architect",
            "occupations": [{"ssocCode": 10237}],
            "startDate": "2001-01-01",
            "industry": {
              "code": "62014"
            },
            "employmentTypes": [{"id": "07"}],
            "endDate": null
          }
        },
      "query":"mutation postWorkExperience($workExperience: EmploymentHistoryInput!) {  employmentHistory {create(input: $workExperience) { id      organizationName jobTitle jobDescription startDate endDate isCurrentJob occupations { ssocCode } industry { code } employmentTypes { id } } }}"}`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  getSkills(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"getSkills",
        "variables":{
          "nric":"${nric}"
        },
        "query":"query getSkills($nric: String!) {  profile: profileByNric(nric: $nric) { skills { ${
          Cypress.config('backendFeatureFlag')['allowMcfApiConstant'] ? 'uuid' : 'id'
        } skill }  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  deleteSkills(accessToken, skills = []) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"deleteSkills",
        "variables":{
          "input":{
            "skills":[${skills.toString()}]
            }
          },
        "query":"mutation deleteSkills($input: SkillInput!) {  skills { delete(input: $input)  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  createSkills(accessToken, skills = []) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"createSkills",
        "variables":{
          "input":{
            "skills":[${skills.toString()}]
          }
        },
        "query":"mutation createSkills($input: SkillInput!) { skills { create(input: $input)  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  deleteAllAndCreateSkills(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const defaultSkills = [25, 48, 189, 191, 192, 217, 580, 2653, 3487, 2412, 100084, 100085, 8747, 7935];
      this.getSkills(nric, accessToken).then((skillsResponse) => {
        const skillsId = skillsResponse.body.data.profile.skills.map((skill) => skill.id);
        this.deleteSkills(accessToken, skillsId).then(() => {
          this.createSkills(accessToken, defaultSkills).then(() => {
            resolve();
          });
        });
      });
    });
  },

  profileByNric(nric, accessToken) {
    return new Cypress.Promise((resolve) => {
      const body = `{
        "operationName":"profileByNric",
        "variables":{
          "nric":"${nric}",
          "isFetchContactInfoFromMyInfo":false,
          "isSyncWithMSF":false
        },
        "query":"query profileByNric($nric: String!, $isFetchContactInfoFromMyInfo: Boolean, $isSyncWithMSF: Boolean) {  profile: profileByNric(nric: $nric, isFetchContactInfoFromMyInfo: $isFetchContactInfoFromMyInfo, isSyncWithMSF: $isSyncWithMSF) {    id    name    preferredName    email                contactNumber { telephoneNumber }    employmentHistories {      id      organizationName      jobTitle      jobDescription      startDate      endDate      isCurrentJob      occupations {        ssocCode      }      industry {        code      }      employmentTypes {        id      }      hasValidationError    }    academicQualifications {      qualification {        qualificationAttained        qualificationAttainedCode        fieldOfStudy        fieldOfStudyCode        name      }      institution      yearAttained      isHighest      hasValidationError    }    documents {       fileName      fileSize      isDefault      url    }    skills {      ${
          Cypress.config('backendFeatureFlag')['allowMcfApiConstant'] ? 'uuid' : 'id'
        }      skill    }  }}"
      }`;
      this.profileTemplate(accessToken, body).then((response) => {
        resolve(response);
      });
    });
  },

  profileTemplate(accessToken, body) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('api')}/profile`,
        headers: {
          'Content-Type': 'application/json',
          Cookie: `access-token=${accessToken}`,
        },
        body: body,
        timeout: 25000,
      }).then((response) => {
        resolve(response);
      });
    });
  },

  applyJob(jobUuid, nrics = []) {
    const applyJobResponses = [];
    return new Cypress.Promise((resolve) => {
      cy.wrap(nrics)
        .each((nric) => {
          this.getAccessToken(nric).then((accessToken) => {
            this.getIndividualId(nric, accessToken).then((individualId) => {
              this.postWorkExperienceIfNot(nric, accessToken);
              this.deleteAllAndCreateSkills(nric, accessToken);
              this.createEductionIfNot(nric, accessToken).then(() => {
                this.getDocumentID(nric, accessToken).then((documentID) => {
                  this.createApplication(jobUuid, individualId, documentID, accessToken).then((response) => {
                    applyJobResponses.push({
                      individualId: individualId,
                      applicaionId: response.body,
                    });
                  });
                });
              });
            });
          });
        })
        .then(() => {
          resolve(applyJobResponses);
        });
    });
  },

  // this method to create candidate details by calling profileByNric API in specific format for
  // below methods in manage_applicants_page.functions
  // shouldHaveCandidateListItemInfo,
  // shouldHaveCandidateRightTopPanelInfo,
  // shouldHaveCandidateRightBottomPanelInfo
  getCandidateProfileDetails(nric, type, jobUuid) {
    return new Cypress.Promise((resolve) => {
      if (type === 'applicant') {
        cy.task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${nric}';`).then((results) => {
          this.getApplicantProfileDetails(jobUuid, results[0].individual_id).then((response) => {
            cy.log('getApplicantProfileDetails', response);
            resolve(response);
          });
        });
      } else {
        this.getTalentProfileDetails(nric).then((response) => {
          cy.log('getTalentProfileDetails', response);
          resolve(response);
        });
      }
    });
  },

  getApplicantProfileDetails(jobUuid, individualId) {
    return new Cypress.Promise((resolve) => {
      couchDBApi
        .find({
          selector: {
            jobId: jobUuid,
            applicant: {
              id: individualId,
            },
          },
        })
        .then((response) => {
          const candidateDetails = response.docs[0];
          cy.log(candidateDetails);
          const candidateDataToValidate = {
            name: candidateDetails.applicant.name,
            jobtitle: candidateDetails.applicant.workExperiences[0].jobTitle,
            applyDate: `Applied on ${moment(candidateDetails.createdOn).format('D MMM YYYY')}`,
            lastLoginDate: 'Active',
            hasResume: true,
            jobcompany: candidateDetails.applicant.workExperiences[0].companyName,
            email: candidateDetails.applicant.email,
            mobileNumber: candidateDetails.applicant.mobileNumber,
            skills: candidateDetails.applicant.skills.map((s) => s.skill),
            jobs: [
              {
                title: candidateDetails.applicant.workExperiences[0].jobTitle,
                company: candidateDetails.applicant.workExperiences[0].companyName,
                year: candidateDetails.applicant.workExperiences[0].startDate.substring(0, 4),
              },
            ],
            educations: [
              {
                year: candidateDetails.applicant.education[0].yearAttained,
                ssec_foc: candidateDetails.applicant.education[0].ssecEqaDescription,
                institution: candidateDetails.applicant.education[0].institution,
              },
            ],
          };
          cy.log(candidateDataToValidate);
          resolve(candidateDataToValidate);
        });
    });
  },

  getTalentProfileDetails(nric) {
    return new Cypress.Promise((resolve) => {
      this.getAccessToken(nric).then((candidateAccessToken) => {
        this.profileByNric(nric, candidateAccessToken)
          .then((response) => {
            return response.body.data.profile;
          })
          .then((talentDetails) => {
            return {
              candidateDataToValidate: {
                name: talentDetails.name,
                ...(talentDetails.employmentHistories.length > 0
                  ? {jobtitle: talentDetails.employmentHistories[talentDetails.employmentHistories.length - 1].jobTitle}
                  : talentDetails.academicQualifications.length > 0
                  ? {
                      jobtitle:
                        talentDetails.academicQualifications[talentDetails.academicQualifications.length - 1]
                          .qualification.qualificationAttained,
                    }
                  : {jobtitle: ''}),
                lastLoginDate: 'Active',
                ...(talentDetails.documents.length > 0 ? {hasResume: true} : {hasResume: false}),
                ...(talentDetails.employmentHistories.length > 0
                  ? {
                      jobcompany:
                        talentDetails.employmentHistories[talentDetails.employmentHistories.length - 1]
                          .organizationName,
                    }
                  : talentDetails.academicQualifications.length > 0
                  ? {
                      jobcompany:
                        talentDetails.academicQualifications[talentDetails.academicQualifications.length - 1]
                          .qualification.fieldOfStudy,
                    }
                  : {jobcompany: ''}),
                email: talentDetails.email,
                ...(talentDetails.contactNumber
                  ? {mobileNumber: talentDetails.contactNumber.telephoneNumber}
                  : {mobileNumber: null}),
                skills: talentDetails.skills.map((s) => s.skill),
              },
              talentDetails: talentDetails,
            };
          })
          .then(({candidateDataToValidate, talentDetails}) => {
            this.getTalentWorkExperienceDetails(talentDetails).then((exp) => {
              this.getTalentEducationHistoryDetails(talentDetails).then((edu) => {
                resolve({
                  ...candidateDataToValidate,
                  ...{jobs: exp},
                  ...{educations: edu},
                });
              });
            });
          });
      });
    });
  },

  getTalentWorkExperienceDetails(talentDetails) {
    return new Cypress.Promise((resolve) => {
      const workExperience = [];
      cy.wrap(talentDetails.employmentHistories)
        .each((employment) => {
          workExperience.push({
            title: employment.jobTitle,
            company: employment.organizationName,
            year: employment.startDate.substring(0, 4),
          });
        })
        .then(() => {
          resolve(workExperience.reverse());
        });
    });
  },

  getTalentEducationHistoryDetails(talentDetails) {
    return new Cypress.Promise((resolve) => {
      const educationHistory = [];
      cy.wrap(talentDetails.academicQualifications)
        .each((education) => {
          educationHistory.push({
            year: education.yearAttained.substring(0, 4),
            ssec_foc: education.qualification.fieldOfStudy || education.qualification.qualificationAttained,
            institution: education.institution,
          });
        })
        .then(() => {
          resolve(educationHistory.reverse());
        });
    });
  },
};
