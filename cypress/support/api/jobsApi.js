/// <reference types="Cypress" />

// This section contains collections of method to manage Job for employer via API's
// All below method should be used after login into employer portal.
// TODO: Login employer via API

let retry = 1;
export const apiJobs = {
  createJob(jobDataPayLoad) {
    let jobResponse;
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'POST',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jobDataPayLoad,
        failOnStatusCode: false,
      })
        .then((response) => {
          jobResponse = response;
          // retry job post api for 504 - Bad gateway
          // Cypress in-build retryApiOnFailStatus dont have effect due to cypress-fast-fail, so implemented customs
          if (response.status === 504 && retry <= Cypress.config('retryApiOnFailStatus')['job']) {
            cy.log(`Retrying job by API, retry: ${retry}`);
            this.createJob(jobDataPayLoad);
            retry++;
          }
        })
        .then(() => {
          resolve(jobResponse.body);
        });
    });
  },

  editJob(uuid, jobUpdateDataPayLoad) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: jobUpdateDataPayLoad,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  extendJobDuration(uuid, durationDate) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PATCH',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: `{"expiryDate":"${durationDate}"}`,
      }).then((response) => {
        resolve(response.body);
      });
    });
  },

  closeJob(uuid, isVacant = false) {
    return new Cypress.Promise((resolve) => {
      cy.request({
        method: 'PUT',
        url: `${Cypress.config('api')}/${Cypress.config('apiVersion')}/jobs/${uuid}/close`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: `{"isVacant":${isVacant}}`,
      }).then((response) => {
        resolve(response);
      });
    });
  },
};
