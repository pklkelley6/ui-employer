import {LOGIN_REQUEST_ALIAS} from '../support/commands/common.commands';
import {validateAccountDetails} from '../support/manageData/job.seed.js';
import {userInfos, authAccess} from './users';
import {checkLogoutPage} from './shared/common.functions';
import {getOptions} from './home';

let retry = 1;
Cypress.Commands.add('login', (companyName, role = 'Job Admin') => {
  const auth =
    role &&
    authAccess.find((auth) => {
      return auth.role === role;
    });
  const {
    accountType,
    userId,
    userCountry,
    userFullName,
    userName,
    singpassHolder,
    entityId,
    entityStatus,
    entityType,
    entityRegNo,
    entityCountry,
    entityName,
  } = userInfos.find((user) => {
    return user.companyName === companyName;
  });
  const account = {
    userInfo: {
      accountType,
      userId,
      userCountry,
      userFullName,
      userName,
      singpassHolder,
      entityId,
      entityStatus,
      entityType,
      entityRegNo,
      entityCountry,
      entityName,
    },
    authAccess: {auths: [auth]},
  };
  cy.captureLoginRequest();
  const authCode = btoa(JSON.stringify(account));
  cy.url().then((currentUrl) => {
    const relayState =
      currentUrl && currentUrl !== 'about:blank' && !currentUrl.includes('logout')
        ? currentUrl
        : Cypress.config('baseUrl');
    const visitOptions = getOptions();

    cy.visit(`${Cypress.env('loginUrl')}`, {
      qs: {authCode, type: 'corppass', relayState: visitOptions.auth ? '' : relayState},
      failOnStatusCode: false,
    });
    if (visitOptions.auth) {
      cy.visit(relayState, {...visitOptions, failOnStatusCode: false});
    }
    cy.wait(`@${LOGIN_REQUEST_ALIAS}`)
      .then((responses) => {
        // retry login by API, Cypress in-build
        // Cypress in-build retryApiOnFailStatus dont have effect due to cypress-fast-fail, so implemented customs
        if (responses.response.statusCode !== 302 && retry <= Cypress.config('retryApiOnFailStatus')['login']) {
          cy.log(`Retrying login by API, retry: ${retry}`).login(companyName);
          retry++;
        }
      })
      .then(() => {
        validateAccountDetails();
        cy.url().should('match', /jobs|terms-and-conditions/);
      });
  });
});
Cypress.Commands.add('loginFromLoginButton', (companyName, role = 'Job Admin') => {
  cy.get('[data-cy=main-login-button]').click();
  login(companyName, role);
});
Cypress.Commands.add('loginFromNavbar', (companyName, role = 'Job Admin') => {
  cy.get('[data-cy=navbar-login-with-corppass]').click();
  login(companyName, role);
});
Cypress.Commands.add('reLogin', (companyName, role = 'Job Admin') => {
  cy.logout();
  checkLogoutPage();
  cy.login(companyName, role);
});
const login = (companyName, role) => {
  cy.url().should('include', Cypress.env('corppassUrl'));

  if (role) {
    const auth = authAccess.find((auth) => {
      return auth.role === role;
    });

    cy.get('#role').type(auth.role);
    cy.get('#startDate').type(auth.startDate);
    cy.get('button[name="add"').click();
  }

  const user = userInfos.find((user) => {
    return user.companyName === companyName;
  });

  const {accountType, singpassHolder, userId, userName, userFullName, entityType, entityId} = user;

  cy.get('#accountType').select(accountType);
  if (singpassHolder) {
    cy.get('#singpassHolder').check();
  }
  cy.get('#entityId').type(entityId);
  cy.get('#userName').type(userName);
  cy.get('#userId').type(userId);
  cy.get('#userFullName').type(userFullName);
  if (entityType === 'UEN') {
    cy.get('#uen').check();
  } else {
    cy.get('#nonuen').check();
  }
  cy.captureLoginRequest().get('form:first').submit();

  cy.wait(`@${LOGIN_REQUEST_ALIAS}`).its('response.statusCode').should('eq', 302).wait(1500); // delay to allow cypress to store cookie details before proceeding, temp fix.
  validateAccountDetails();
  cy.url().should('match', /jobs|terms-and-conditions/);
};
