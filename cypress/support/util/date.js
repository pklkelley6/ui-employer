import {parseISO, addMonths, format, addDays, subDays} from 'date-fns';

export const sixMonthsFrom = (currentDate) => {
  const postedDate = parseISO(currentDate);
  // subtract 1 day since validity is 6 months inclusive of the start date.
  // ex. first day valid: Jan 1, last day valid: Jun 30
  const sixMonths = format(subDays(addMonths(postedDate, 6), 1), 'd MMM yyyy');
  return sixMonths;
};
export const daysFrom = (daysToAdd, fromDate) => {
  const currentDate = fromDate ? parseISO(fromDate) : new Date();
  return format(addDays(currentDate, daysToAdd), 'd MMM yyyy');
};

export const formatDate = (date) => date.format('YYYY-MM-DD');
