const faker = require('faker');

export const apiJobData = {
  defaultJobPost: {
    address: {
      postalCode: '738964',
      block: '9',
      street: 'WOODLANDS AVENUE 9',
      building: '',
      isOverseas: false,
    },
    categories: [{id: 11}],
    description: faker.lorem.sentences(25),
    employmentTypes: [{id: 7}],
    jobPostDuration: 21,
    minimumYearsExperience: 15,
    numberOfVacancies: 1,
    positionLevels: [{id: 1}],
    postedCompany: {uen: '100000008C'},
    salary: {maximum: 15000, minimum: 12000, type: {id: 4}},
    schemes: [],
    skills: [
      {uuid: '27044a3e3cf7cb55fb49b6b966a6d9a2'}, // Aerodynamics
      {uuid: '5fd71557fc131cdcc9b1d377f73584c6'}, // Aerospace
      {uuid: '088363f89f663943d043b6c991f0c0ad'}, // Aerospace Engineering
      {uuid: '77981789ee8210327cde6db56bf00980'}, // Distributed Generation
      {uuid: '89374c81b73686baca13bb49bfb074b0'}, // Fluid Power
      {uuid: '330f49df8243756a8a4dc7f7f7ee6dfe'}, // Development
      {uuid: 'a12f6b2ea61787fb708831cad00f0a33'}, // Java Servlets
      {uuid: 'b9519b14f0a5ea36728ae697e2affcaf'}, // Jakarta Tomcat
      {uuid: 'be34f05f837d8dba4134095129ee3c06'}, // Styling
      {uuid: 'ddfdc2a2b4c277158589a9f95fa2fbd8'}, // Scheduling
      {uuid: 'fe4dbcab9b910577e5035e97ac068dae'}, // Management
      {uuid: 'f0eec0cab28c0a5b0f9f67d0eedc582a'}, // e-commerce application
    ],
    ssecEqa: '92',
    ssecFos: '1152',
    ssocCode: 2284,
    status: {id: 102},
    title: faker.name.jobTitle(),
  },
};
