import faker from 'faker';

export const couchDBMockData = {
  defaultApplicantData(jobUuid) {
    return {
      statusId: 4,
      statusDescription: 'Received',
      statusUpdatedDate: faker.date.past(),
      createdBy: faker.datatype.uuid(),
      createdOn: faker.date.past(),
      modifiedBy: faker.datatype.uuid(),
      modifiedOn: faker.date.past(),
      isViewed: false,
      jobId: jobUuid,
      applicant: {
        id: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        mobileNumber: '+6585123456',
        expectedSalary: 'null',
        education: [
          {
            name: 'Education exists but Skill API return false',
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosCode: '0399',
            ssecFosDescription:
              'Humanities & Social Sciences n.e.c. (including regional studies, international relations, demography, labour studies)',
            yearAttained: 2017,
            institution: 'ROPSTEN: Test University_Test',
            isHighest: false,
          },
        ],
        workExperiences: [
          {
            jobTitle: 'developer',
            companyName: 'TestABC company',
            startDate: '2019-11-01',
            jobDescription: 'application developer',
            ssocCode: 2422,
            ssocDescription: 'Application Analyst',
            ssicCode: '26113',
            ssicDescription: 'Assembly and testing of semiconductors',
          },
        ],
        skills: [
          {
            uuid: '303cb0ef9edb9082d61bbbe5825d972a',
            skill: '.NET',
          },
          {
            uuid: '54fee414eb866d1816c93daa9facdf22',
            skill: 'A/B Testing',
          },
          {
            uuid: '09f0c5159c5e34504e453eff3fc70324',
            skill: 'Account Management',
          },
          {
            uuid: '010e865a2bbd442936c04869911261ce',
            skill: 'Account Planning',
          },
          {
            uuid: '098772ec0f6bbc36e10e8515a32237fc',
            skill: 'Account Reconciliation',
          },
          {
            uuid: '8fdb6417a9fb84b139ba4bc1014e697f',
            skill: 'Account Servicing',
          },
          {
            uuid: 'b2e0166ea571c0f94d00264cbf0cac8b',
            skill: 'Accountability',
          },
          {
            uuid: '19841527d7381d566a12c3f0db88855a',
            skill: 'Accountable Care',
          },
          {
            uuid: '9bbd45bad55cfc620803907f2d8a0217',
            skill: 'Accounting',
          },
          {
            uuid: '80f02cec3e05a1d93e286971a1b3bf22',
            skill: 'Accounting System',
          },
          {
            uuid: '1ed711fc7b4a53484ce612ee65c8e3b8',
            skill: 'Accounts Payable',
          },
        ],
        resume: {
          id: '892471',
          fileName: 'Sample.pdf',
          filePath: 'resume/892471',
        },
      },
      scores: {},
      source: {
        name: 'MCF',
      },
      skillsMatchedScore: 0.75,
      isShortlisted: true,
    };
  },
};
