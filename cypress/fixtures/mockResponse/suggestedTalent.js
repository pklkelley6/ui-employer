const faker = require('faker');
export const getSuggestedTalentMock = (jobUuid, numberOfTalents = 1, nrics = []) => {
  return {
    ok: true,
    variables: {jobId: jobUuid},
    response: {
      data: {
        suggestedTalentsForJob: {
          talents: getTalents(numberOfTalents, nrics),
          total: numberOfTalents,
          __typename: 'SuggestedTalents',
        },
      },
    },
  };
};

export const getSuggestedTalentsCount = (count) => {
  return {
    response: {
      data: {suggestedTalentsForJob: {total: count, __typename: 'SuggestedTalents'}},
    },
  };
};

const getTalents = (numberOfTalents, nrics = []) => {
  const talents = [];
  let result;
  for (let i = 0; i < numberOfTalents; i++) {
    cy.task('profileDB', `Select email, preferred_name, individual_id  from jobseekers where nric = '${nrics[i]}';`)
      .then((results) => {
        result = results[0];
      })
      .then(() => {
        const talent = {
          talent: {
            email: result ? result.email : faker.internet.email(),
            name: result ? result.preferred_name : faker.name.findName(),
            id: result ? result.individual_id : faker.datatype.uuid(),
            lastLogin: faker.date.recent(),
            mobileNumber: faker.phone.phoneNumber('9########'),
            education: [
              {
                name: 'Degree',
                isHighest: true,
                institution: 'University of Western Australia',
                yearAttained: 2001,
                ssecEqaCode: '70',
                ssecEqaDescription: "Bachelor's Degree or equivalent",
                ssecFosDescription: 'Business Management',
                isVerified: null,
                __typename: 'Education',
              },
            ],
            skills: [
              {uuid: '9bbd45bad55cfc620803907f2d8a0217', skill: 'Accounting', __typename: 'Skill'},
              {uuid: '78f48b8a03facd61000db8f7ae6f9227', skill: 'Auditing', __typename: 'Skill'},
              {uuid: '822bead6c149ffacbe7a12c44c3958ed', skill: 'Budget', __typename: 'Skill'},
              {uuid: '26338cc3fc50556c51e1f9ffb4cc3f2b', skill: 'External Audit', __typename: 'Skill'},
              {uuid: '7e135053874921bc74029efd7b4a4371', skill: 'Financial Accounting', __typename: 'Skill'},
              {uuid: '4a8fd50e4f2ecc69fa9aa31928cd2155', skill: 'Financial Audits', __typename: 'Skill'},
            ],
            workExperiences: [
              {
                jobTitle: faker.name.jobTitle(),
                companyName: faker.company.companyName(),
                startDate: '2012-09-01',
                endDate: '2018-08-01',
                jobDescription: faker.name.jobDescriptor(),
                __typename: 'WorkExperience',
              },
            ],
            resume: {
              fileName: 'Sample.pdf',
              filePath: 'resume/892471',
              __typename: 'Resume',
            },
            __typename: 'Candidate',
          },
          scores: {wcc: 0.5853646, __typename: 'TalentScore'},
          bookmarkedOn: null,
          __typename: 'Talent',
        };
        talents.push(talent);
      });
  }
  return talents;
};
