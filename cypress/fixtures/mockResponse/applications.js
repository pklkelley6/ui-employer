const faker = require('faker');

export const getApplicationsCount = (jobUuid, count, unviewedTotal) => {
  return {
    variables: {jobId: jobUuid},
    response: {
      data: {applicationsForJob: {total: count, unviewedTotal: unviewedTotal, __typename: 'Applications'}},
    },
  };
};

export const getApplicationsMock = (jobUuid, numberOfApplications) => {
  const applications = [];
  for (let i = 0; i < numberOfApplications; i++) {
    const application = {
      job: {
        uuid: jobUuid,
        __typename: 'Job',
      },
      applicant: {
        email: faker.internet.email(),
        name: faker.name.findName(),
        id: faker.datatype.uuid(),
        lastLogin: faker.date.recent(),
        mobileNumber: faker.phone.phoneNumber('9########'),
        education: [
          {
            name: 'Degree',
            isHighest: true,
            institution: 'Verified University',
            yearAttained: 1970,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: true,
            __typename: 'Education',
          },
          {
            name: 'Degree',
            isHighest: false,
            institution: 'University of Eastern Australia',
            yearAttained: 2000,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: false,
            __typename: 'Education',
          },
          {
            name: 'Degree',
            isHighest: false,
            institution: 'NUS',
            yearAttained: 2000,
            ssecEqaCode: '70',
            ssecEqaDescription: "Bachelor's Degree or equivalent",
            ssecFosDescription: 'Business Management',
            isVerified: null,
            __typename: 'Education',
          },
        ],
        skills: [
          {uuid: '9bbd45bad55cfc620803907f2d8a0217', skill: 'Accounting', __typename: 'Skill'},
          {uuid: '78f48b8a03facd61000db8f7ae6f9227', skill: 'Auditing', __typename: 'Skill'},
          {uuid: '822bead6c149ffacbe7a12c44c3958ed', skill: 'Budget', __typename: 'Skill'},
          {uuid: '26338cc3fc50556c51e1f9ffb4cc3f2b', skill: 'External Audit', __typename: 'Skill'},
          {uuid: '7e135053874921bc74029efd7b4a4371', skill: 'Financial Accounting', __typename: 'Skill'},
          {uuid: '4a8fd50e4f2ecc69fa9aa31928cd2155', skill: 'Financial Audits', __typename: 'Skill'},
        ],
        workExperiences: [
          {
            jobTitle: faker.name.jobTitle(),
            companyName: faker.company.companyName(),
            startDate: '2012-09-01',
            endDate: '2018-08-01',
            jobDescription: faker.name.jobDescriptor(),
            __typename: 'WorkExperience',
          },
        ],
        resume: {
          fileName: 'Sample.pdf',
          filePath: 'resume/892471',
          lastDownloadedAt: null,
          __typename: 'Resume',
        },
        __typename: 'Candidate',
      },
      createdOn: faker.date.past(),
      id: faker.datatype.uuid(),
      isViewed: true,
      isShortlisted: null,
      bookmarkedOn: null,
      statusId: 4,
      rejectionReason: null,
      rejectionReasonExtended: null,
      jobScreeningQuestionResponses: null,
      scores: {
        wcc: 0.632,
        __typename: 'ApplicationScore',
      },
      __typename: 'Application',
    };
    applications.push(application);
  }

  return {
    variables: {
      jobId: jobUuid,
    },
    response: {
      data: {
        applicationsForJob: {
          applications,
          total: numberOfApplications,
          __typename: 'Applications',
        },
      },
    },
  };
};
