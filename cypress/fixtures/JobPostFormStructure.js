export default {
  jobPostEachTabVerification: {
    jobDescriptionTab: {
      label: 'Job Description',
      link: '/#job-description',
    },
    skillsTab: {
      label: 'Skills',
      link: '/#skills',
    },
    keyInformationTab: {
      label: 'Key Information',
      link: '/#key-information',
    },
    workplaceDetailsTab: {
      label: 'Workplace Details',
      link: '/#workplace-details',
    },
    previewScreeningQuestionsTab: {
      label: 'Screening Questions',
      link: '/#screening-questions',
    },
    previewJobPostTab: {
      label: 'Review',
      link: '/#preview',
    },
  },
};
