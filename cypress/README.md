# Cypress Integration Test


## <ins>DB config</ins>
Almost all spec could be executed locally, however some spec has DB check/validation so below setup is required for those spec.

create .env.[environment to run] (example `.env.qa`) in cypress/config/dbConfig

Input required value based on vault: https://vault.ci.mcf.sh/ui/vault/secrets/secret/show/gds-ace/mcf/database

If .env file not available script will default value to dev DBs.

<!-- blank line -->
## <ins>To run test in local:</ins>
1. ### with Dev config or localhost
`yarn cypress run --env configFile=dev --spec "<spec path | comma seperated for more than 1>"` Use this command to run test in cli with localhost.

`cypress:open-dev` Use this command to run test in Cypress UI with localhost.

Ref. config - cypress/config/dev.json <br>


2. ### with QA config
`yarn cypress run --spec "<spec path | comma seperated for more than 1>"` Use this command to run test in cli with QA.

`yarn cypress:open-qa` or `yarn cypress:open` Use this command to run test in Cypress UI with QA.

Ref. config - cypress/config/qa.json and cypress/config/dbConfig/.env.qa <br>


3. ### with UAT config
`yarn cypress run --env configFile=uat,username=<BASIC_AUTH_USERNAME>,password=<BASIC_AUTH_PASSWORD> --spec "<spec path | comma seperated for more than 1>"` Use this command to run test in cli with UAT.

`yarn cypress:open --env configFile=uat,username=<BASIC_AUTH_USERNAME>,password=<BASIC_AUTH_PASSWORD>` Use this command to open the suite with UAT URL - ref. cypress/config/uat.json.
or `yarn cypress:open-uat` and needs to add username and password in cypress/config/uat.json under env.

Pass the UAT public access username and password (https://vault.ci.mcf.sh/ui/vault/secrets/secret/show/gds-ace/mcf/mcf-uat.gds-gov.tech)

Ref. config - cypress/config/uat.json and cypress/config/dbConfig/.env.uat <br>

<!-- blank line -->

## <ins>Seed test Data</ins>
Any of the following can seed the test data for the test: <br>
1. ### api-jobs and api-profile
Other repos control data, and these data cause issue when multiple pipeline run as the data are shared.
The data are seeded in
api-jobs repo - src/seeds/manual/qa/jobs.ts
api-profile - src/database/couchdb/seeds/qa/applications.ts <br>

2. ### APIs
Can use to create non-shared dynamic data which cant be affected by other tests and can be controlled by the test it self.
APIs are recommended way to seed test data. <br>

3. ### DB
Data inserted via backend without any business validation from the application can use to create non-shared dynamic data which cant be affected by other tests and controlled by test itself.

Available methods for both APIs and DB - cypress/support/manageData/applicant.seed.js, job.seed.js and test data - cypress/fixtures/seedData/api_job_post_seed_data.js <br>


4. ### Other URLs to refer for seed data

https://gitlab.com/mycf.sg/ui-employer/wikis/How-to-get-your-candidate-appear-in-the-suggested-talent-list%3F

https://gitlab.com/mycf.sg/ui-employer/wikis/Test-Data-used-for-Cypress-Integration-in-UI-Employer

https://gitlab.com/mycf.sg/ui-employer/wikis/Where-to-update-the-Test-Data-(For-UI-Employer) <br>

<!-- blank line -->
## <ins>Cypress - dependency</ins>
All cypress related test dependencies moved into Cypress docker image, any changes/addition to the dependence needs new docker image creation, follow below repo for the same.
https://github.com/GovTechSG/cicd-images/tree/master/cypress

[Note: any changes to @mcf/constants version needs to be updated in .gitlab-ci.yml under `.integration_test:` ] <br>