import moment from 'moment';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {deleteCompanyAddress, seedCompanyAddressInfo} from '../../../../cypress/support/manageData/company.seed';

describe('Company Profile registered data', () => {
  const testDataNoCompanyAddressAndIndustry = {
    uen: '100000010C',
    addressExists: false,
  };
  const testDataAddCompanyAddressAndIndustry = {
    uen: '100000010C',
    ssicCode: '94912',
    ssicCodeDescr: 'Mosques',
    block: '856',
    street: 'TAMPINES STREET 82',
    floor: '',
    unit: '',
    building: 'TAMPINES VILLA',
    postalCode: '520856',
    purposeId: '1',
    addressExists: true,
  };

  before(() => {
    cy.home().login('Company Profile Registered Data Validation', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Validate for company address profile data', () => {
    it('should delete company address and ssic_code in DB', () => {
      cy.task(
        'jobDB',
        `UPDATE company_info SET ssic_code = '', last_sync_date = '${moment().format('YYYY-MM-DD HH:mm:ss')}'
           WHERE uen = '${testDataNoCompanyAddressAndIndustry.uen}';`,
      ).then(() => {
        deleteCompanyAddress(testDataNoCompanyAddressAndIndustry.uen);
      });
    });

    it('should have dash icon in address and industry field from company profile page', () => {
      cy.openCompanyProfilePage();
      verifyDisplayIndustryAndCompanyAddress(testDataNoCompanyAddressAndIndustry);
    });

    it('should add company address and ssic_code in DB', () => {
      cy.task(
        'jobDB',
        `UPDATE company_info SET ssic_code = '${testDataAddCompanyAddressAndIndustry.ssicCode}',
          last_sync_date = '${moment().format('YYYY-MM-DD HH:mm:ss')}'
           WHERE uen = '${testDataAddCompanyAddressAndIndustry.uen}';`,
      ).then(() => {
        seedCompanyAddressInfo(testDataAddCompanyAddressAndIndustry);
      });
    });

    it('should have values in address and industry field from company profile page', () => {
      cy.openCompanyProfilePage().then(() => {
        verifyDisplayIndustryAndCompanyAddress(testDataAddCompanyAddressAndIndustry);
      });
    });
  });

  describe('ACRA website link for company profile page', () => {
    it('should have content for changes of address', () => {
      cy.get('[data-cy="acra-website"]').should(
        'have.text',
        'To change the registered details, please visit the ACRA website.',
      );
    });

    it('should verify hyperlink for ACRA Website', () => {
      cy.get('[data-cy="acra-website"] a')
        .should('have.text', 'ACRA website')
        .should('have.attr', 'href', 'https://www.acra.gov.sg/')
        .should('have.attr', 'target', '_blank');
    });
  });
});

const verifyDisplayIndustryAndCompanyAddress = (company) => {
  const verifyIndustry = company.addressExists ? `${company.ssicCode} - ${company.ssicCodeDescr}` : '-';
  const verifyAddress = company.addressExists
    ? `${company.building}, ${company.block} ${company.street} ${company.postalCode}`
    : '-';

  cy.reloadPageUntilElementContains('[data-cy=address]>p', verifyAddress); // due to DB operation async on previous action, waiting until latest value get reflect

  cy.contains('[data-cy=uen]>label', 'Business UEN').should('exist');
  cy.contains('[data-cy=uen]>p', `${company.uen}`).should('exist');

  cy.contains('[data-cy=industry]>label', 'Industry').should('exist');
  cy.contains('[data-cy=industry]>p', `${verifyIndustry}`).should('exist');

  cy.contains('[data-cy=address]>label', 'Address').should('exist');
  cy.contains('[data-cy=address]>p', `${verifyAddress}`).should('exist');
};
