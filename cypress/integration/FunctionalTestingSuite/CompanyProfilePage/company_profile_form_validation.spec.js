import path from 'path';
import faker from 'faker';
import {
  COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR,
  COMPANY_PROFILE_LOGO_UPLOADER_ERROR_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR,
  COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR,
  COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR,
  COMPANY_PROFILE_NUM_EMPLOYEE_ERROR_SELECTOR,
  COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR,
  COMPANY_PROFILE_WEBSITE_ERROR_SELECTOR,
  COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR,
} from '../../../support/shared/company_profile_page';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Company Profile Form Validation', () => {
  before(() => {
    cy.home().login('Empty Company Profile', 'Job Admin').openCompanyProfilePage();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('logo uploader', () => {
    context('validate invalid logo upload', () => {
      const INVALID_FILE_ERROR = "We can't seem to read this file - please try uploading a different file";
      [
        {
          file: 'companyProfile/logo/invalid.png',
          errorMessage: INVALID_FILE_ERROR,
        },
        {
          file: 'companyProfile/logo/SampleCSVFile_2kb.csv',
          errorMessage: INVALID_FILE_ERROR,
        },
        {
          file: 'companyProfile/logo/SampleTextFile_10kb.txt',
          errorMessage: INVALID_FILE_ERROR,
        },
        {
          file: 'companyProfile/logo/huge.jpg',
          errorMessage: 'File exceeds 100kb - please try uploading a smaller file',
        },
      ].forEach(({file, errorMessage}) => {
        const logoFilename = path.basename(file);
        it(`should throw "${errorMessage}" when upload ${logoFilename}`, () => {
          cy.fixture(file, 'base64').then((invalidFile) => {
            cy.get(COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR).uploadFile(invalidFile, logoFilename);
          });

          validateCompanyProfileError({logo: errorMessage});
        });
      });
    });

    context('validate valid logo upload', () => {
      [
        {file: 'companyProfile/logo/SampleJPGImage_50kbmb.jpg'},
        {file: 'companyProfile/logo/SampleJPGImage_50kbmb.jpg', filename: 'SampleJPGImage_ChangeExtension.png'},
        {file: 'companyProfile/logo/Test_noExt'},
      ].forEach(({file, filename}) => {
        const logoFilename = filename || path.basename(file);
        it(`should not show error when upload valid image file  ${logoFilename}`, () => {
          cy.fixture(file, 'base64').then((validFile) => {
            cy.get(COMPANY_PROFILE_LOGO_UPLOADER_SELECTOR).uploadFile(validFile, logoFilename);
          });

          cy.get(COMPANY_PROFILE_LOGO_UPLOADER_ERROR_SELECTOR).should('not.exist');
        });
      });
    });
  });

  describe('description', () => {
    describe('should show error message', () => {
      [
        {
          description: 'less than minimal',
          errorMessage: 'Please enter at least 100 characters',
        },
        {
          description: faker.lorem.paragraphs(100),
          errorMessage: 'Please keep within 4,000 characters',
          fillOption: {paste: true},
        },
      ].forEach(({description, errorMessage, fillOption}) => {
        it(`should throw "${errorMessage}" when ${description.length} characters entered`, () => {
          cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).fill(description, fillOption).blur();

          validateCompanyProfileError({description: errorMessage});
        });
      });
    });

    describe('should not show error message', () => {
      [
        {
          description: faker.lorem.paragraphs(150).substring(0, 100),
        },
        {
          description: faker.lorem.paragraphs(150).substring(0, 2000),
          fillOption: {paste: true},
        },
        {
          description: faker.lorem.paragraphs(150).substring(0, 4000),
          fillOption: {paste: true},
        },
      ].forEach(({description, fillOption}) => {
        it(`should not show error message when input within ${description.length} characters`, () => {
          cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).fill(description, fillOption).blur();
          cy.get(COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR).should('not.exist');
        });
      });
    });

    describe('should not show error message when valid description given', () => {
      [
        {
          testDescription: 'Input less description followed by valid description',
          errorInput: '',
          validInput: faker.lorem.paragraphs(150).substring(0, 1500),
        },
        {
          testDescription: 'Input higher than the limit followed by valid description',
          errorInput: faker.lorem.paragraphs(150).substring(0, 4500),
          validInput: faker.lorem.paragraphs(150).substring(0, 150),
        },
      ].forEach(({testDescription, errorInput, validInput}) => {
        it(`${testDescription}`, () => {
          cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).fill(errorInput, {paste: true}).blur();
          cy.get(COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR).should('exist');
          cy.get(COMPANY_PROFILE_DESCRIPTION_FIELD_SELECTOR).fill(validInput, {paste: true}).blur();
          cy.get(COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR).should('not.exist');
        });
      });
    });
  });

  describe('number of employees', () => {
    const LESS_THAN_MINIMAL_ERROR = 'This number should be more than 0';
    [{numberOfEmployees: '0'}, {numberOfEmployees: '-10'}].forEach(({numberOfEmployees}) => {
      it(`should throw "${LESS_THAN_MINIMAL_ERROR}" when ${numberOfEmployees} entered`, () => {
        cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).fill(numberOfEmployees).blur();

        validateCompanyProfileError({numberOfEmployees: LESS_THAN_MINIMAL_ERROR});
      });
    });

    it('should not allow enter of decimal', () => {
      cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_FIELD_SELECTOR).fill('1.5').should('have.value', '1');
    });
  });

  describe('website', () => {
    const MORE_THAN_MAXIMAL_ERROR = 'Please keep within 2,000 characters';
    it(`should throw "${MORE_THAN_MAXIMAL_ERROR}" when more than 2000 characters entered and same should disappear for valid input`, () => {
      cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR)
        .fill(`${faker.internet.url()}${faker.random.words(2000)}`, {paste: true})
        .blur();
      validateCompanyProfileError({website: MORE_THAN_MAXIMAL_ERROR});
      cy.get(COMPANY_PROFILE_WEBSITE_FIELD_SELECTOR).fill(faker.internet.url()).blur();
      cy.get(COMPANY_PROFILE_WEBSITE_ERROR_SELECTOR).should('not.exist');
    });
  });

  describe('empty form submit', () => {
    before(() => {
      cy.openCompanyProfilePage();
    });

    it('should throw logo and description required', () => {
      cy.get(COMPANY_PROFILE_SUBMIT_BUTTON_SELECTOR).click();

      validateCompanyProfileError({
        logo: 'Please upload your company logo to continue',
        description: 'Please fill this in',
      });
    });
  });

  const validateCompanyProfileError = ({logo, description, numberOfEmployees, website}) => {
    logo && cy.get(COMPANY_PROFILE_LOGO_UPLOADER_ERROR_SELECTOR).should('have.text', logo);
    description && cy.get(COMPANY_PROFILE_DESCRIPTION_ERROR_SELECTOR).should('have.text', description);
    numberOfEmployees && cy.get(COMPANY_PROFILE_NUM_EMPLOYEE_ERROR_SELECTOR).should('have.text', numberOfEmployees);
    website && cy.get(COMPANY_PROFILE_WEBSITE_ERROR_SELECTOR).should('have.text', website);
  };
});
