import {
  shouldHaveCardLoaderLoaded,
  mouseoverOnManageMenuButton,
  verifyManageJobPostViewEdit,
  clickViewEditJobPostButton,
} from '../../../support/shared/job_page.functions';
import {checkEditCountOfJobPost, verifyEditJobPostButton} from '../../../support/shared/job_details_page.functions';
import jobs from '../../../fixtures/JobList';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('View/Edit Job Post - verify edit counter', function () {
  const MAX_JOB_POST_EDIT_COUNT = 2;
  before(() => {
    cy.home();
    cy.login('Manage Applicant Status', 'Job Admin');
  });
  beforeEach(() => {
    cy.home();
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('Should display edit counter with 2 edits left with a clickable edit job post link for jobs that have not been edited', function () {
    cy.clickOnJobTab('Open');
    cy.searchForJobs(jobs.jobPostViewEditCheck.Id);
    shouldHaveCardLoaderLoaded();
    cy.clickJobByJobPostID(jobs.jobPostViewEditCheck.Id);
    cy.skipApplicantsOnboarding();
    shouldHaveCardLoaderLoaded();
    mouseoverOnManageMenuButton();
    verifyManageJobPostViewEdit();
    clickViewEditJobPostButton();
    checkEditCountOfJobPost(MAX_JOB_POST_EDIT_COUNT);
    verifyEditJobPostButton(MAX_JOB_POST_EDIT_COUNT);
  });

  it('Should display edit counter with 0 edits left and disable editing for jobs with maximum edits made', function () {
    cy.clickOnJobTab('Open');
    cy.searchForJobs(jobs.jobPostMaxEditCount.Id);
    shouldHaveCardLoaderLoaded();
    cy.clickJobByJobPostID(jobs.jobPostMaxEditCount.Id);
    cy.skipApplicantsOnboarding();
    shouldHaveCardLoaderLoaded();
    mouseoverOnManageMenuButton();
    verifyManageJobPostViewEdit();
    clickViewEditJobPostButton();
    checkEditCountOfJobPost(0);
    verifyEditJobPostButton(0);
  });
});
