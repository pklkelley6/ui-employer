import {verifyStaticNudgeContent} from '../../../support/shared/common.functions';
import {
  shouldHaveCardLoaderLoaded,
  searchAndOpenJobOnTab,
  mouseoverOnManageMenuButton,
  verifyManageJobPostViewEdit,
  clickViewEditJobPostButton,
} from '../../../support/shared/job_page.functions';
import {
  previewJobDescriptionDetails,
  previewSkillsDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
  previewUENCompanyDetails,
} from '../../../support/shared/job_post_preview_page.functions';
import jobs from '../../../fixtures/JobList';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('View Job Post', () => {
  before(() => {
    cy.home();
    cy.login('Manage Applicant Status', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Verify View Edit Job Post details page and content value - UI', function () {
    before(() => {
      // add `Wholesale Trade category`
      cy.task(
        'jobDB',
        `Insert ignore into job_category (job_post_id, category_id) values ('${jobs.jobPostViewEditCheck.Id}', '42');`,
      ).then(() => {
        searchAndOpenJobOnTab('Open', jobs.jobPostViewEditCheck.Id);
        shouldHaveCardLoaderLoaded();
      });
    });

    it('UI checking - verify view/edit job post details are display and valid data', function () {
      mouseoverOnManageMenuButton();
      verifyManageJobPostViewEdit();
      clickViewEditJobPostButton();
      verifyStaticNudgeContent();
      previewUENCompanyDetails(jobs.jobPostViewEditCheck);
      previewJobDescriptionDetails(jobs.jobPostViewEditCheck);
      previewSkillsDetails(jobs.jobPostViewEditCheck.previewSkills);
      previewKeyInformationDetails(jobs.jobPostViewEditCheck.keyInformation);
      previewWorkplaceDetails(jobs.jobPostViewEditCheck.jobWorkplaceDetail);
    });
  });
  describe('Verify failed page to load job post data - UI', function () {
    before(() => {
      cy.home();
      searchAndOpenJobOnTab('Open', jobs.jobPostViewEditCheck.Id);
      shouldHaveCardLoaderLoaded();
    });

    it('should display Temporary Unavailable Message when failed to fetch job details', () => {
      mouseoverOnManageMenuButton();
      verifyManageJobPostViewEdit();
      cy.intercept(`**/jobs/${jobs.jobPostViewEditCheck.uuid}`, {
        statusCode: 504,
        body: {},
      });
      clickViewEditJobPostButton();
      cy.get('[data-cy=error-message]')
        .should('have.text', 'This information is temporarily unavailable. \n Please try again later.')
        .next('a')
        .should('have.text', 'Go back to previous page');
    });

    it('should back to previous page from failed job post view page when click on go back to previous page', () => {
      cy.get('[data-cy=previous-page]').contains('Go back to previous page').click();
      cy.skipApplicantsOnboarding();
      cy.get('[data-cy=job-post-id]').should('have.text', jobs.jobPostViewEditCheck.Id);
    });
  });
});
