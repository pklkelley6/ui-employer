import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';

describe('Verify Back to Applications button', function () {
  let jobPostId;
  const UEN = '200610233W';
  const jobStatusAndPage = [
    {jobStatus: 'Open', jobStatusId: 102, pageName: 'View/Edit'},
    {jobStatus: 'Closed', jobStatusId: 9, pageName: 'View/Repost'},
  ];

  before(() => {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.log('seed data - post new job');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
    removeSeededJobInDB(jobPostId);
  });

  jobStatusAndPage.forEach(({jobStatus, jobStatusId, pageName}) => {
    it(`should show Back to Applications button in ${pageName} Job Posting for ${jobStatus} job`, () => {
      cy.log(`Updating the application status in DB as ${jobStatus}`)
        .task('jobDB', `Update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}';`)
        .then(() => {
          viewJobsPageAndVerifyBackToApplicationsButtonExistence(jobPostId, 'exist');
          clickBackToApplicationsButtonAndVerifyPageRedirectToApplications();
        });
    });
  });
});

const viewJobsPageAndVerifyBackToApplicationsButtonExistence = (jobPostID, assertion = 'not.exist') => {
  cy.openJobPageByJobPostId(jobPostID, {mode: 'view'}).get('[data-cy=back-to-job-applications]').should(assertion);
};

const clickBackToApplicationsButtonAndVerifyPageRedirectToApplications = () => {
  cy.get('[data-cy=back-to-job-applications]').click();
  cy.get('[data-cy=back-to-job-applications]').should('not.exist');
  cy.url().should('contain', '/applications');
};
