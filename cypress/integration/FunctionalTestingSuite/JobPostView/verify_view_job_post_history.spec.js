import moment from 'moment';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';

const jobPostExpiryDate = '2020-05-07';

describe('Validate Job Post history', () => {
  const UEN = '100000003C';
  let uuid, jobPostId;
  const jobHistoryData = {
    1: {actionType: 'EDIT', uiLabel: 'Edited on', jobPostUpdatedAt: '2020-05-01'},
    2: {actionType: 'CLOSE', uiLabel: 'Closed on', jobPostUpdatedAt: '2020-05-02'},
    3: {actionType: 'REPOST', uiLabel: 'Reposted on', jobPostUpdatedAt: '2020-05-03'},
    4: {actionType: 'EDIT', uiLabel: 'Edited on', jobPostUpdatedAt: '2020-05-04'},
    5: {actionType: 'CLOSE', uiLabel: 'Closed on', jobPostUpdatedAt: '2020-05-05'},
    6: {actionType: 'REPOST', uiLabel: 'Reposted on', jobPostUpdatedAt: '2020-05-06'},
    7: {actionType: 'EXPIRED', uiLabel: 'Closed on', jobPostUpdatedAt: '2020-05-08'},
  };

  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('should not display job post history for jobs which doesnt have audit trail', () => {
    before(() => {
      cy.log('seed data - posting a job using API');
      seedXNewJobsForUEN(1, null, UEN).then((response) => {
        uuid = response[0].uuid;
        jobPostId = response[0].metadata.jobPostId;
      });
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
    });

    it('should not show in View/Repost Job Posting for Closed job', () => {
      shouldNotShowJobPostHistorySectionInJobViewPage(uuid);
    });

    it('should not show in View/Edit Job Posting for Open job', () => {
      cy.log('seed data - update job status to Open job')
        .task('jobDB', `update jobs set job_status_id = '102' where job_post_id = '${jobPostId}';`)
        .then(() => {
          shouldNotShowJobPostHistorySectionInJobViewPage(uuid);
        });
    });
  });

  describe('should display try again later message in Job History section when history API returns error response', () => {
    let uuid, jobPostId;
    before(() => {
      cy.log('seed data - posting, editing a job using API');
      seedXNewJobsForUEN(1, null, UEN).then((response) => {
        uuid = response[0].uuid;
        jobPostId = response[0].metadata.jobPostId;
      });
    });

    beforeEach(() => {
      cy.log('seed data - mock history API to return error').intercept(`**/jobs/${uuid}/history`, {statusCode: 504});
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
    });

    it('should show try again later message in View/Edit Job Posting for Open job', () => {
      shouldShowTryAgainLaterMessageInJobPostHistorySectionInJobViewPage(uuid);
    });

    it('should show try again later message in View/Repost Job Posting for Closed job', () => {
      cy.log('seed data - update job status to Closed job')
        .task('jobDB', `update jobs set job_status_id = '9' where uuid = '${uuid}';`)
        .then(() => {
          shouldShowTryAgainLaterMessageInJobPostHistorySectionInJobViewPage(uuid);
        });
    });
  });

  describe('should display Job History details', () => {
    // creating a job using API and after that directly seeding audit trail for a job in DB to validate in UI
    before(() => {
      cy.home();
      cy.login('Account with a lot of jobs', 'Job Admin');
      cy.log('seed data - create a job via API and insert audit trail for all types');
      seedXNewJobsForUEN(1, null, UEN)
        .then((response) => {
          uuid = response[0].uuid;
          jobPostId = response[0].metadata.jobPostId;
        })
        .then(() => {
          jobHistoryData[0] = {
            actionType: 'CREATE',
            uiLabel: 'Created on',
            jobPostUpdatedAt: '2020-04-26',
          };
          cy.log('seed data - remove existing audit').task(
            'jobDB',
            `Delete FROM audit_trails where job_post_id = '${jobPostId}'`,
          );
          Cypress._.forEach(jobHistoryData, (details) => {
            cy.task(
              'jobDB',
              `Insert into audit_trails (job_post_id, action_type, job_post_expiry_date, job_post_updated_at) values ('${jobPostId}', '${details.actionType}', '${jobPostExpiryDate}', '${details.jobPostUpdatedAt}');`,
            );
          });
        });
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
    });

    it('should show details in View/Edit Job Posting for Open job', () => {
      shouldShowJobHistoryDetailsInJobViewPage(uuid, jobHistoryData);
    });

    it('should show details in View/Repost Job Posting for Closed job', () => {
      cy.task('jobDB', `Update jobs set job_status_id = '9' where job_post_id = '${jobPostId}';`).visit(
        `/jobs/${uuid}/view`,
      );
      shouldShowJobHistoryDetailsInJobViewPage(uuid, jobHistoryData);
    });

    it('should display Job History details for job which has CREATE audit and it should not display EXTEND audit details', () => {
      cy.log('seed data - removing existing audit trail from a job other than CREATE and seed EXTENSION audit')
        .task('jobDB', `Delete FROM audit_trails where job_post_id = '${jobPostId}' and action_type != 'CREATE'`)
        .task(
          'jobDB',
          `Insert into audit_trails (job_post_id, action_type, job_post_updated_at) values ('${jobPostId}', 'EXTENSION', '2021-02-09');`,
        );
      shouldShowCreatedOnHistoryOnlyAndShouldNotShowExtensionHistoryInJobViewPage(
        uuid,
        jobHistoryData[0].jobPostUpdatedAt,
      );
    });
  });
});

const shouldNotShowJobPostHistorySectionInJobViewPage = (uuid) => {
  cy.visit(`/jobs/${uuid}/view`).shouldHaveFormLoaderLoaded().get('[data-cy=job-post-history]').should('not.exist');
};

const shouldShowTryAgainLaterMessageInJobPostHistorySectionInJobViewPage = (uuid) => {
  cy.visit(`/jobs/${uuid}/view`)
    .shouldHaveFormLoaderLoaded()
    .get('[data-cy=job-post-history]')
    .should('contain', 'This information is temporarily unavailable. Please try again later.');
};

const shouldShowJobHistoryDetailsInJobViewPage = (uuid, jobHistoryData) => {
  cy.visit(`/jobs/${uuid}/view`).shouldHaveFormLoaderLoaded();
  cy.get('[data-cy=job-post-history] p[class*=fw4]').each(($el, index) => {
    cy.wrap($el).should('contain', jobHistoryData[index].uiLabel);
    cy.get('[data-cy=job-post-history] p[class*=fw6]')
      .eq(index)
      .should(
        'contain',
        moment(
          jobHistoryData[index].actionType === 'EXPIRED' ? jobPostExpiryDate : jobHistoryData[index].jobPostUpdatedAt,
        ).format('D MMM YYYY'), // for EXPIRED action should show expiry date instead of updated at
      );
  });
};

const shouldShowCreatedOnHistoryOnlyAndShouldNotShowExtensionHistoryInJobViewPage = (uuid, jobPostUpdatedAt) => {
  cy.visit(`/jobs/${uuid}/view`).shouldHaveFormLoaderLoaded();
  cy.get('[data-cy=job-post-history] p[class*=fw4]').then(($el) => {
    cy.wrap($el).its('length').should('eq', 1);
    cy.wrap($el).invoke('text').should('contain', 'Created on');
  });
  cy.get('[data-cy=job-post-history] p[class*=fw6]').then(($el) => {
    cy.wrap($el).its('length').should('eq', 1);
    cy.wrap($el).invoke('text').should('eq', moment(jobPostUpdatedAt).format('D MMM YYYY'));
  });
};
