import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {searchAndOpenJobOnTab, cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import {massUpdateApplicationsStatus} from '../../../support/shared/applicants_mass_status_update.function';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions.js';
import {clickBookmarkIconIfTrue} from '../../../support/shared/manage_saved_tab.functions.js';

describe('validate mass application status bar update', () => {
  const UEN = '100000003C';
  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const jobStatusAndTab = [
    {jobStatus: 'Open', tabName: 'Open', jobStatusId: 102},
    {jobStatus: 'Re-open', tabName: 'Open', jobStatusId: 103},
    {jobStatus: 'Closed', tabName: 'Closed', jobStatusId: 9},
  ];
  jobStatusAndTab.forEach(({jobStatus, tabName, jobStatusId}) => {
    describe(`validate mass update grey bar existence flow for ${jobStatus} jobs`, () => {
      let jobPostId;
      before(() => {
        cy.log('seed data - create a job with 2 vacancy and apply applicants via couchDB with Received status');
        const payLoad = apiJobData.defaultJobPost;
        payLoad.postedCompany.uen = UEN;
        payLoad.numberOfVacancies = 2;
        seedXNewJobsForUEN(1, payLoad, UEN)
          .then((response) => {
            jobPostId = response[0].metadata.jobPostId;
            const couchDBApplicantsMockData = [];
            cy.wrap(Cypress._.range(0, 25))
              .each(() => {
                couchDBApplicantsMockData.push(couchDBMockData.defaultApplicantData(response[0].uuid));
              })
              .then(() => {
                couchDBApi.bulkDocs(couchDBApplicantsMockData);
              });
          })
          .then(() => {
            cy.log('seed data - update job status');
            if (jobStatus !== 'Open')
              cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
          });
      });

      afterEach(() => {
        cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
        Cypress.Cookies.preserveOnce('access-token');
      });

      after(() => {
        removeSeededJobInDB(jobPostId);
      });

      it('should not display mass update grey bar when application status is not updated', () => {
        searchAndOpenJobOnTab(tabName, jobPostId);
        cardLoaderShouldNotExist();
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');
      });

      it('should not display mass update grey bar when update 2 application status as To Interview', () => {
        cy.clickOnCandidate(0);
        applicantStatus.selectApplicantStatus('To Interview');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');

        cy.clickOnCandidate(7);
        applicantStatus.selectApplicantStatus('To Interview');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');
      });

      it('should not display mass update grey bar when update 2 application status as Unsuccessful', () => {
        cy.clickOnCandidate(3);
        applicantStatus.selectApplicantStatus('Unsuccessful');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');

        cy.clickOnCandidate(18);
        applicantStatus.selectApplicantStatus('Unsuccessful');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');
      });

      it('should not display mass update grey bar when one of the application status changed as Hired in 1st page but should display on 2nd application changes in 2nd page and verify text', () => {
        cy.clickOnCandidate(3);
        applicantStatus.selectApplicantStatus('Hired');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');

        cy.clickOnPaginationIndex(1); //click on page 2
        cardLoaderShouldNotExist();
        cy.clickOnCandidate(0);
        applicantStatus.selectApplicantStatus('Hired');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('exist');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarTextAndLink();
      });

      it('should not display when existing Hired application status changed to Unsuccessful in 1st page as the Hired count not match with vacancy', () => {
        cy.clickOnPaginationIndex(0); //click on page 1
        cardLoaderShouldNotExist();
        cy.clickOnCandidate(3);
        applicantStatus.selectApplicantStatus('Unsuccessful');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');
      });

      it('should display mass update grey bar when application status is updated as hired in saved tab for bookmarked applicant', () => {
        cy.clickOnCandidate(10);
        clickBookmarkIconIfTrue(true);
        cy.clickOnSavedTab();
        cardLoaderShouldNotExist();
        cy.clickOnCandidate(0);
        applicantStatus.selectApplicantStatus('Hired');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('exist');
      });

      it('should not display mass update grey bar when application status is updated as To Interview in saved tab and should get display when status is reverted', () => {
        cy.clickOnCandidate(0);
        applicantStatus.selectApplicantStatus('To Interview');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');

        applicantStatus.selectApplicantStatus('Hired');
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('exist');
      });

      it('should not display mass update grey bar when job vacancy is increased and should get display when application status is changed as Hired in Applicants tab', function () {
        if (jobStatus === 'Closed') this.skip(); // skip this test for closed job
        cy.task('jobDB', `update jobs set number_of_vacancies = 3 where job_post_id = '${jobPostId}'`)
          .wait(1000) // wait 1 sec after DB update and then reload, due to async sometimes changes are not reflecting
          .reload()
          .then(() => {
            cy.skipApplicantsOnboardingIfExist();
            cardLoaderShouldNotExist();
            massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('not.exist');

            cy.clickOnApplicantTab().skipApplicantsOnboardingIfExist();
            cardLoaderShouldNotExist();
            cy.clickOnCandidate(13);
            applicantStatus.selectApplicantStatus('Hired');
            massUpdateApplicationsStatus.verifyMassUpdateGreyBarIs('exist');
          });
      });
    });
  });
});
