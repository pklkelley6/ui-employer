import {mcf} from '@mcf/constants';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {massUpdateApplicationsStatus} from '../../../support/shared/applicants_mass_status_update.function';

const UEN = '100000003C';
let jobPostId, jobUuid;
const jobStatusAndTab = [
  {jobStatus: 'Open', jobStatusId: 102},
  {jobStatus: 'Closed', jobStatusId: 9},
];
describe('validate mass update applicant status bar failure', () => {
  before(() => {
    cy.home().login('Account with a lot of jobs', 'Job Admin');
    cy.log('seed data - create a job and apply applicants via couchDB with Received and Successful status');
    const payLoad = apiJobData.defaultJobPost;
    payLoad.postedCompany.uen = UEN;
    payLoad.numberOfVacancies = 1;
    seedXNewJobsForUEN(1, payLoad, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;
      couchDBApi.bulkDocs([
        couchDBMockData.defaultApplicantData(response[0].uuid),
        {...couchDBMockData.defaultApplicantData(response[0].uuid), statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL},
      ]);
    });
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    removeSeededJobInDB(jobPostId);
  });
  jobStatusAndTab.forEach(({jobStatus, jobStatusId}) => {
    describe(`validate mass update failure for ${jobStatus} jobs`, () => {
      before(() => {
        cy.log(`seed data - update job status as - ${jobStatus}`);
        cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
      });
      it('should see full failure error message for mass update applicant status', () => {
        cy.log('mock full failure request response payload');
        cy.mockGraphQL('profile', getMassUpdateApplicantStatusFullFailurePayload())
          .openJobPageByJobPostId(jobPostId)
          .skipApplicantsOnboardingIfExist();
        massUpdateApplicationsStatus.clickMassUpdateGreyBarLink({mockError: true});
        cy.verifyNotificationGrowl('Temporarily unable to update application status. Please try again.');
        cy.get('[data-cy="loader-modal"]').should('not.exist');
      });
      it('should see partial failure error message for mass update applicant status', () => {
        cy.log('mock partial failure request response payload');
        cy.mockGraphQL('profile', getMassUpdateApplicantStatusPartialFailurePayload(jobUuid))
          .openJobPageByJobPostId(jobPostId)
          .skipApplicantsOnboardingIfExist();
        massUpdateApplicationsStatus.clickMassUpdateGreyBarLink({mockError: true});
        cy.verifyNotificationGrowl(
          'Temporarily unable to update application status for some applicants. Please try again.',
        );
        cy.get('[data-cy="loader-modal"]').should('not.exist');
      });
    });
  });
});

const getMassUpdateApplicantStatusFullFailurePayload = () => {
  return {
    setApplicationStatusByJob: {
      response: {
        errors: [{message: 'mock applicant error response'}],
        data: {setApplicationStatusByJob: null},
      },
    },
  };
};
const getMassUpdateApplicantStatusPartialFailurePayload = (uuid) => {
  const partialFailurePayload = {
    setApplicationStatusByJob: {
      response: {
        data: {setApplicationStatusByJob: []},
      },
    },
  };
  couchDBApi.find({selector: {jobId: uuid}}).then((responseBody) => {
    partialFailurePayload.setApplicationStatusByJob.response.data.setApplicationStatusByJob.push({
      id: responseBody.docs[0]._id,
      ok: false,
      __typename: 'ApplicationUpdateResult',
    });
  });
  return partialFailurePayload;
};
