import {mcf} from '@mcf/constants';
import moment from 'moment';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {searchAndOpenJobOnTab, cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import {massUpdateApplicationsStatus} from '../../../support/shared/applicants_mass_status_update.function';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {validateApplicationStatusForListOfApplicantsInLHSAndRHS} from '../../../support/shared/manage_applicants_page.functions.js';

describe('validate mass update status bar success', () => {
  const UEN = '100000003C';
  const successfulApplicationsIndex = [0, 20];
  const toInterviewApplicationsIndex = [1, 21];
  const applicationsCount = 25;
  let jobPostId;

  const jobStatusAndTab = [
    {jobStatus: 'Open', tabName: 'Open', jobStatusId: 102},
    {jobStatus: 'Closed', tabName: 'Closed', jobStatusId: 9},
  ];

  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  jobStatusAndTab.forEach(({jobStatus, tabName, jobStatusId}) => {
    describe(`validate mass update grey bar success flow for ${jobStatus} jobs`, () => {
      before(() => {
        cy.log(
          `seed data - create an open job with 2 vacancy and apply applicants via couchDB with Received status, To Interview status for index ${toInterviewApplicationsIndex} and Successful status for index ${successfulApplicationsIndex}`,
        );
        const payLoad = apiJobData.defaultJobPost;
        payLoad.postedCompany.uen = UEN;
        payLoad.numberOfVacancies = 2;
        seedXNewJobsForUEN(1, payLoad, UEN)
          .then((response) => {
            jobPostId = response[0].metadata.jobPostId;
            const couchDBApplicantsMockData = [];
            cy.wrap(Cypress._.range(0, applicationsCount))
              .each((_, index) => {
                const statusId = successfulApplicationsIndex.includes(index)
                  ? mcf.JOB_APPLICATION_STATUS.SUCCESSFUL
                  : toInterviewApplicationsIndex.includes(index)
                  ? mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW
                  : undefined;
                couchDBApplicantsMockData.push({
                  ...couchDBMockData.defaultApplicantData(response[0].uuid),
                  ...(statusId ? {statusId} : {}),
                  createdOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
                });
              })
              .then(() => {
                couchDBApi.bulkDocs(couchDBApplicantsMockData);
              });
          })
          .then(() => {
            if (jobStatus !== 'Open') {
              cy.log(`seed data - update job status as - ${jobStatus}`);
              cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
            }
          });
      });

      afterEach(() => {
        cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
        Cypress.Cookies.preserveOnce('access-token');
      });

      after(() => {
        removeSeededJobInDB(jobPostId);
      });

      it(`should display grey bar if number of hired is equal to number of vacancies`, () => {
        searchAndOpenJobOnTab(tabName, jobPostId);
        cardLoaderShouldNotExist();
        massUpdateApplicationsStatus.verifyMassUpdateGreyBarTextAndLink();
      });

      it('should update not hired applications to unsuccessful in all pages when update button in grey bar is clicked', () => {
        massUpdateApplicationsStatus.clickMassUpdateGreyBarLink();
        massUpdateApplicationsStatus.waitForGreyBarToDisappear();

        validateApplicationStatusForListOfApplicantsInLHSAndRHS([0], 'Hired');
        validateApplicationStatusForListOfApplicantsInLHSAndRHS(Cypress._.range(1, 20), 'Unsuccessful');
        cy.clickOnPaginationIndex(1); //click on page 2

        validateApplicationStatusForListOfApplicantsInLHSAndRHS([0], 'Hired');
        validateApplicationStatusForListOfApplicantsInLHSAndRHS(Cypress._.range(1, 5), 'Unsuccessful');
      });
    });
  });
});
