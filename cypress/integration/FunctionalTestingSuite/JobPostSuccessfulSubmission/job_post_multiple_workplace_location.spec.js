import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {fillKeyInformationOnJobPosting} from '../../../support/shared/job_post_key_information_page.functions';
import {checkMultipleLocationFromWorkplaceDetails} from '../../../support/shared/job_post_workplace_details_page.functions';
import {previewKeyInformationDetails} from '../../../support/shared/job_post_preview_page.functions';
import {
  verifyFieldsInAcknowledgementPage,
  verifyJobPostPresentInAllJobsList,
  getJobPostId,
} from '../../../support/shared/job_post_success_page.functions';
import {onClickNext, onClickSubmitJobPost, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';

describe('Job post with multiple workplace location address', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('JobPosting.json').as('jobPost');
  });
  it('should validate & verify success of job post with multiple workplace location address', function () {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(this.jobPost.jobPostMultipleWorkplaceLocation);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPostMultipleWorkplaceLocation.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPostMultipleWorkplaceLocation.skillFromDropDown);
    onClickNext();
    fillKeyInformationOnJobPosting(this.jobPost.jobPostMultipleWorkplaceLocation, true);
    onClickNext();
    checkMultipleLocationFromWorkplaceDetails();
    onClickNext();
    onClickNext();
    previewKeyInformationDetails(this.jobPost.jobPostMultipleWorkplaceLocation.keyInformation);
    onClickSubmitJobPost();
    verifyFieldsInAcknowledgementPage(this.jobPost.jobPostMultipleWorkplaceLocation);
    getJobPostId().then((jobPostId) => {
      verifyJobPostPresentInAllJobsList(jobPostId, this.jobPost.jobPostMultipleWorkplaceLocation.jobTitle);
    });
  });
});
