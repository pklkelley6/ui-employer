import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {fillKeyInformationOnJobPosting} from '../../../support/shared/job_post_key_information_page.functions';
import {
  uncheckSameLocationFromWorkplaceDetails,
  fillLocalWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  previewJobDescriptionDetails,
  previewSkillsDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
} from '../../../support/shared/job_post_preview_page.functions';
import {
  verifyFieldsInAcknowledgementPage,
  verifyJobPostPresentInAllJobsList,
  getJobPostId,
} from '../../../support/shared/job_post_success_page.functions';
import {onClickNext, onClickSubmitJobPost, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';

describe('Validate Job Posting successful acknowledgment page & presented posted job from all jobs in Employer page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('JobPosting.json').as('jobPost');
  });
  it('should verify all fields in job post acknowledgement page & verify job post in all jobs page', function () {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(this.jobPost.jobPost1);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillFromDropDown);
    onClickNext();
    fillKeyInformationOnJobPosting(this.jobPost.jobPost1);
    onClickNext();
    uncheckSameLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    onClickNext();
    onClickNext();
    previewJobDescriptionDetails(this.jobPost.jobPost1);
    previewSkillsDetails(
      Object.values(
        Cypress.config('backendFeatureFlag')['allowMcfApiConstant']
          ? this.jobPost.jobPost1.apiConstPreviewSkills
          : this.jobPost.jobPost1.previewSkills,
      ),
    );
    previewKeyInformationDetails(this.jobPost.jobPost1.keyInformation);
    previewWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    onClickSubmitJobPost();
    verifyFieldsInAcknowledgementPage(this.jobPost.jobPost1);
    getJobPostId().then((jobPostId) => {
      verifyJobPostPresentInAllJobsList(jobPostId, this.jobPost.jobPost1.jobTitle);
    });
  });
});
