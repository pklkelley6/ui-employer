import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {fillKeyInformationOnJobPosting} from '../../../support/shared/job_post_key_information_page.functions';
import {
  uncheckSameLocationFromWorkplaceDetails,
  fillLocalWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {previewKeyInformationDetails} from '../../../support/shared/job_post_preview_page.functions';
import {
  verifyFieldsInAcknowledgementPage,
  verifyJobPostPresentInAllJobsList,
  getJobPostId,
} from '../../../support/shared/job_post_success_page.functions';
import {onClickNext, onClickSubmitJobPost, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {removeSeededJobInDB} from '../../../support/manageData/job.seed.js';

describe('Job Posting success with mininum qualification', function () {
  let openJobPostId;
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('JobPosting.json').as('jobPost');
  });

  after(() => {
    removeSeededJobInDB(openJobPostId);
  });

  // should enable this step once given bug is fixed https://www.pivotaltracker.com/n/projects/2004917/stories/167309680
  it('should verify all values in job post acknowledgement page & test min qualification job post success', function () {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(this.jobPost.jobPostMinQualification);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPostMinQualification.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPostMinQualification.skillFromDropDown);
    onClickNext();
    fillKeyInformationOnJobPosting(this.jobPost.jobPostMinQualification, true);
    onClickNext();
    uncheckSameLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.jobPost.jobPostMinQualification.jobWorkplaceDetail);
    onClickNext();
    onClickNext();
    previewKeyInformationDetails(this.jobPost.jobPostMinQualification.keyInformation);
    onClickSubmitJobPost();
    verifyFieldsInAcknowledgementPage(this.jobPost.jobPostMinQualification);
    getJobPostId().then((jobPostId) => {
      openJobPostId = jobPostId;
      verifyJobPostPresentInAllJobsList(openJobPostId, this.jobPost.jobPostMinQualification.jobTitle);
    });
  });
});
