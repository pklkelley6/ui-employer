import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {getSuggestedTalentsCount} from '../../../fixtures/mockResponse/suggestedTalent';

describe('Applicants - List/Detail view data validation', () => {
  let testData;
  before(() => {
    cy.fixture('Candidate.json').then((candidateTestData) => {
      testData = candidateTestData;
    });
    cy.home()
      .mockGraphQL('profile', {
        getSuggestedTalentsCount: getSuggestedTalentsCount(2),
      })
      .login('Manage Applicant Test Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.logout();
  });

  it('Validate the list data and the detail data for applicant', () => {
    cy.clickJobByJobPostID('MCF-2018-9111111');
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboarding();
    ManageJobsNavigate.shouldHaveTotalApplicantsInApplicantList(6);
    ManageJobsNavigate.verifyCandidateTypeLabelLeftPanel('Applicant');
    ManageJobsNavigate.getCandidateFromList(5)
      .then(ManageJobsNavigate.shouldHaveCandidateListItemInfo(testData.Applicant.Applicant6))
      .then(ManageJobsNavigate.shouldNotHaveScore);
    ManageJobsNavigate.getCandidateFromList(4)
      .then(ManageJobsNavigate.shouldHaveCandidateListItemInfo(testData.Applicant.Applicant5))
      .then(ManageJobsNavigate.shouldNotHaveScore);
  });

  it('Verifying view and unviews', () => {
    ManageJobsNavigate.getCandidateFromList(0).then(ManageJobsNavigate.isCandidateViewed(false));
    ManageJobsNavigate.getCandidateFromList(1).then(ManageJobsNavigate.isCandidateViewed(false));
    ManageJobsNavigate.getCandidateFromList(2).then(ManageJobsNavigate.isCandidateViewed(false));
    ManageJobsNavigate.getCandidateFromList(3).then(ManageJobsNavigate.isCandidateViewed(false));
    ManageJobsNavigate.getCandidateFromList(4).then(ManageJobsNavigate.isCandidateViewed(true));
    ManageJobsNavigate.getCandidateFromList(5).then(ManageJobsNavigate.isCandidateViewed(false));
  });

  it('Verify the top matchers in applicant list and suggested talent count details', () => {
    ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(0);
    ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(1);
    ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(2);
    ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(3);
    ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(4);
    ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(5);

    cy.get('#tab-suggested-talents')
      .children()
      .contains('span', 'Suggested Talents')
      .next()
      .invoke('text')
      .should((suggestedTalentCount) => {
        expect(Number(suggestedTalentCount)).to.be.within(1, 60);
      });
    cy.clickOnCandidate(4);
    ManageJobsNavigate.getCandidateFromList(4).then(ManageJobsNavigate.isCandidateSelected);
    ManageJobsNavigate.verifyCandidateTypeLabelRightPanel('Applicant');
    ManageJobsNavigate.shouldHaveCandidateRightTopPanelInfo(testData.Applicant.Applicant5);
    ManageJobsNavigate.shouldHaveTopMatcherInApplicantDetail();
    ManageJobsNavigate.shouldHaveCandidateRightBottomPanelInfo(testData.Applicant.Applicant5);
  });
});
