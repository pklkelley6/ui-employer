import moment from 'moment';
import faker from 'faker';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {
  applicantRejectReasonUpdate,
  selectorDetails,
} from '../../../support/shared/applicant_reject_reason_update.functions';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions';

describe(`Verify save and edit save applicant rejection reasons dropdown values`, () => {
  const UEN = '100000008C';
  let jobPostId;
  before(() => {
    cy.home().login('Manage Applicant Status', 'Job Admin');
    cy.log('seed data - create a job and apply applicants via couchDB');
    const payLoad = apiJobData.defaultJobPost;
    payLoad.postedCompany.uen = UEN;
    seedXNewJobsForUEN(1, payLoad, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      const couchDBApplicantsMockData = [];
      cy.wrap(Cypress._.range(0, 15))
        .each((_, index) => {
          const statusId = 3;
          const statusDescription = 'Unsuccessful';
          couchDBApplicantsMockData.push({
            ...couchDBMockData.defaultApplicantData(response[0].uuid),
            ...(statusId ? {statusId} : {}),
            ...(statusDescription ? {statusDescription} : {}),
            createdOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
            bookmarkedOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
          });
        })
        .then(() => {
          couchDBApi.bulkDocs(couchDBApplicantsMockData);
        });
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const jobStatusAndTab = [
    {jobStatus: 'Open', jobStatusId: 102, applicantIndex: [0, 1, 2, 3]},
    {jobStatus: 'Re-open', jobStatusId: 103, applicantIndex: [4, 5, 6, 7]},
    {jobStatus: 'Closed', jobStatusId: 9, applicantIndex: [8, 9, 10, 11]},
  ];

  jobStatusAndTab.forEach(({jobStatus, jobStatusId, applicantIndex}) => {
    describe(`Verify save and edit save applicant rejection reasons for ${jobStatus} jobs`, () => {
      before(() => {
        cy.log('seed data - update job status');
        cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
        cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
      });

      it('should throw dropdown values required when unselect rejection reason and click save button', () => {
        cy.clickOnCandidate(applicantIndex[0]);
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        validateRejectReasonDropdownAndFreeTextError({dropdown: 'Please select an option'});
      });

      it('should throw free text values required when select Others dropdown with empty free text of rejections reason and click save button', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        validateRejectReasonDropdownAndFreeTextError({othersFreeText: 'Please fill this in'});
      });

      it('should throw maximum character exists when select Others dropdown and type more than 1000 char for free text area', () => {
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .fill(faker.lorem.words(1000), {paste: true})
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        validateRejectReasonDropdownAndFreeTextError({othersFreeText: 'Please keep within 1,000 characters'});
      });

      it('should save rejection reason dropdown values from applicant tab', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Did not have enough relevant experience');
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should(
          'have.text',
          'Did not have enough relevant experience',
        );
      });

      it('should throw free text values required when edit and select Others dropdown value and click save button', () => {
        cy.get(selectorDetails.rejectReasonEditLink).click();
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        validateRejectReasonDropdownAndFreeTextError({othersFreeText: 'Please fill this in'});
      });

      it('should throw maximum character exists when edit and select Others dropdown value and type more than 1000 char for free text area', () => {
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .fill(faker.lorem.words(1000), {paste: true})
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        validateRejectReasonDropdownAndFreeTextError({othersFreeText: 'Please keep within 1,000 characters'});
      });

      it('should edit and save rejection reason dropdown for other free text values from applicant tab', () => {
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Others');
        cy.get(selectorDetails.rejectReasonReadOnlyOthersFreeTextValue).should(
          'have.text',
          'Have study knowledge but not real time experience',
        );
      });

      it('should edit and save rejection reason dropdown values from applicant tab', () => {
        cy.get(selectorDetails.rejectReasonEditLink).click();
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Turned down job offer');
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Turned down job offer');
      });

      it('should reset reject reason dropdown values while change of applicant status from application tab', () => {
        applicantStatus.selectApplicantStatus('Hired');
        applicantStatus.selectApplicantStatus('Unsuccessful');
        cy.get(selectorDetails.rejectReasonEditLink).should('not.exist');
        cy.get(selectorDetails.rejectReasonSaveButton).should('exist');
        cy.get(selectorDetails.rejectReasonClickDropdown).should('contain', 'Select one');
      });

      it('should save rejection reason dropdown for other free text values from applicant tab', () => {
        cy.clickOnCandidate(applicantIndex[1]);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText).clear().type('Testing edit save reject reason').blur();
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Others');
        cy.get(selectorDetails.rejectReasonReadOnlyOthersFreeTextValue).should(
          'have.text',
          'Testing edit save reject reason',
        );
      });

      it('should save rejection reason dropdown values from saved tab', () => {
        cy.clickOnSavedTab().shouldHaveCardLoaderLoaded().clickOnCandidate(applicantIndex[2]);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Turned down job offer');
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Turned down job offer');
      });

      it('should edit and save rejection reason dropdown for other free text values from saved tab', () => {
        cy.get(selectorDetails.rejectReasonEditLink).click();
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Others');
        cy.get(selectorDetails.rejectReasonReadOnlyOthersFreeTextValue).should(
          'have.text',
          'Have study knowledge but not real time experience',
        );
      });

      it('should edit and save rejection reason dropdown values exists from saved tab', () => {
        cy.get(selectorDetails.rejectReasonEditLink).click();
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Did not have enough relevant experience');
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should(
          'have.text',
          'Did not have enough relevant experience',
        );
      });

      it('should save rejection reason dropdown for other free text values from saved tab', () => {
        cy.clickOnCandidate(applicantIndex[3]);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText).clear().type('Testing edit save reject reason').blur();
        applicantRejectReasonUpdate.clickOnSaveButton();
        cy.get(selectorDetails.rejectReasonReadOnlyDropdownValue).should('have.text', 'Others');
        cy.get(selectorDetails.rejectReasonReadOnlyOthersFreeTextValue).should(
          'have.text',
          'Testing edit save reject reason',
        );
      });

      it('should reset reject reason dropdown values while change of applicant status from saved tab', () => {
        applicantStatus.selectApplicantStatus('To Interview');
        applicantStatus.selectApplicantStatus('Unsuccessful');
        cy.get(selectorDetails.rejectReasonEditLink).should('not.exist');
        cy.get(selectorDetails.rejectReasonSaveButton).should('exist');
        cy.get(selectorDetails.rejectReasonClickDropdown).should('contain', 'Select one');
      });
    });
  });
  const validateRejectReasonDropdownAndFreeTextError = ({dropdown, othersFreeText}) => {
    dropdown && cy.get(selectorDetails.rejectReasonDropdownValueRequiredError).should('have.text', dropdown);
    othersFreeText && cy.get(selectorDetails.rejectReasonOthersFreeTextError).should('have.text', othersFreeText);
  };
});
