import {seedXNewJobsForUEN, seedXCloseJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {getApplicationsMock} from '../../../fixtures/mockResponse/applications';
import {getSuggestedTalentMock} from '../../../fixtures/mockResponse/suggestedTalent';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

const UEN = '100000003C';
const tabNames = ['Overview', 'Education History'];
const applicantTestData = [
  {
    testDescription: 'should display education label if isVerified is true',
    institution: 'Verified University',
    educationLabelAssertion: 'exist',
  },
  {
    testDescription: 'should not display education label if isVerified is false',
    institution: 'University of Eastern Australia',
    educationLabelAssertion: 'not.exist',
  },
  {
    testDescription: 'should not display education label if isVerified is null',
    institution: 'NUS',
    educationLabelAssertion: 'not.exist',
  },
];

describe('Open Job - Validate education label', () => {
  let openJobResponse;
  before(() => {
    cy.home().login('Account with a lot of jobs', 'Job Admin');
    cy.log('Post a Jobs and mock applicant and talent response');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      openJobResponse = response[0];
    });
  });

  beforeEach(() => {
    cy.mockGraphQL('profile', {
      getApplications: getApplicationsMock(openJobResponse.uuid, 1),
      getSuggestedTalents: getSuggestedTalentMock(openJobResponse.uuid, 1),
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(openJobResponse.metadata.jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Applicant - Should display Education label if isVerified is true; otherwise no', () => {
    it('Open a posted Job and select first applicant', () => {
      searchAndOpenJobOnTab('Open', openJobResponse.metadata.jobPostId);
      cy.skipApplicantsOnboardingIfExist();
      shouldHaveCardLoaderLoaded();
      cy.clickOnCandidate(0);
    });

    tabNames.forEach((tabName) => {
      describe(`Validate ${tabName} tab`, () => {
        applicantTestData.forEach(({testDescription, institution, educationLabelAssertion}) => {
          it(`${testDescription}`, () => {
            cy.clickTabOnCandidateInfo(tabName);
            cy.validateEducationLabelAndTooltipIfExist(institution, educationLabelAssertion);
          });
        });
      });
    });
  });

  describe('Suggested talent - Should not display Education label', () => {
    it('Open suggested talent and select first talent', () => {
      cy.clickOnSuggestedTalentTab().skipApplicantsOnboardingIfExist();
      shouldHaveCardLoaderLoaded();
      cy.clickOnCandidate(0);
    });

    tabNames.forEach((tabName) => {
      it(`should not display in ${tabName}`, () => {
        cy.validateEducationLabelAndTooltipIfExist('University of Western Australia', 'not.exist');
      });
    });
  });
});

describe('Close Job - Validate education label', () => {
  let closeJobResponse;
  before(() => {
    cy.home().login('Account with a lot of jobs', 'Job Admin');
    cy.log('Close a Jobs and mock applicant response');
    seedXCloseJobsForUEN(1, null, UEN).then((response) => {
      closeJobResponse = response[0];
      cy.mockGraphQL('profile', {
        getApplications: getApplicationsMock(closeJobResponse.uuid, 1),
      });
      searchAndOpenJobOnTab('Close', closeJobResponse.metadata.jobPostId);
      cy.clickOnCandidate(0);
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(closeJobResponse.metadata.jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Applicant - Should display Education label if isVerified is true; otherwise no', () => {
    tabNames.forEach((tabName) => {
      describe(`Validate ${tabName} tab`, () => {
        applicantTestData.forEach(({testDescription, institution, educationLabelAssertion}) => {
          it(`${testDescription}`, () => {
            cy.clickTabOnCandidateInfo(tabName);
            cy.validateEducationLabelAndTooltipIfExist(institution, educationLabelAssertion);
          });
        });
      });
    });
  });
});
