import moment from 'moment';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {
  applicantRejectReasonUpdate,
  selectorDetails,
} from '../../../support/shared/applicant_reject_reason_update.functions';

describe(`Verify error message for save and edit save applicant rejection reasons`, () => {
  const UEN = '100000008C';
  let jobPostId;
  const applicantRejectReasonIndex = [1];
  before(() => {
    cy.home().login('Manage Applicant Status', 'Job Admin');
    cy.log('seed data - create a job and apply applicants via couchDB');
    const payLoad = apiJobData.defaultJobPost;
    payLoad.postedCompany.uen = UEN;
    seedXNewJobsForUEN(1, payLoad, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      const couchDBApplicantsMockData = [];
      cy.wrap(Cypress._.range(0, 15))
        .each((_, index) => {
          const statusId = 3;
          const statusDescription = 'Unsuccessful';
          const rejectionReason = applicantRejectReasonIndex.includes(index)
            ? 'Did not have enough relevant experience'
            : undefined;
          couchDBApplicantsMockData.push({
            ...couchDBMockData.defaultApplicantData(response[0].uuid),
            ...(statusId ? {statusId} : {}),
            ...(statusDescription ? {statusDescription} : {}),
            ...(rejectionReason ? {rejectionReason} : {}),
            createdOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
            bookmarkedOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
          });
        })
        .then(() => {
          couchDBApi.bulkDocs(couchDBApplicantsMockData);
        });
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const jobStatusAndTab = [
    {jobStatus: 'Open', jobStatusId: 102},
    {jobStatus: 'Re-open', jobStatusId: 103},
    {jobStatus: 'Closed', jobStatusId: 9},
  ];

  jobStatusAndTab.forEach(({jobStatus, jobStatusId}) => {
    describe(`Verify error message while save and edit save applicant rejection reasons for ${jobStatus} jobs`, () => {
      before(() => {
        cy.log('seed data - update job status');
        cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
        cy.log('mock failure request response payload for save rejection reason');
        cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
      });

      beforeEach(() => {
        cy.mockGraphQL('profile', getRejectionReasonFailurePayload());
      });

      it('should see error message while saving rejection reason dropdown values from applicant tab', () => {
        cy.clickOnCandidate(0);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Did not have enough relevant experience');
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while saving rejection reason dropdown for other free text values from applicant tab', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while edit saving rejection reason dropdown values from applicant tab', () => {
        cy.clickOnCandidate(1);
        cy.get(selectorDetails.rejectReasonEditLink).click();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while edit saving rejection reason dropdown for other free text values from applicant tab', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while saving rejection reason dropdown values from saved tab', () => {
        cy.clickOnSavedTab().shouldHaveCardLoaderLoaded().clickOnCandidate(0);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Did not have enough relevant experience');
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while saving rejection reason dropdown for other free text values from saved tab', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while edit saving rejection reason dropdown values from saved tab', () => {
        cy.clickOnCandidate(1);
        cy.get(selectorDetails.rejectReasonEditLink).click();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });

      it('should see error message while edit saving rejection reason dropdown for other free text values from saved tab', () => {
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        cy.get(selectorDetails.rejectReasonFillOthersFreeText)
          .clear()
          .type('Have study knowledge but not real time experience')
          .blur();
        cy.get(selectorDetails.rejectReasonSaveButton).click();
        cy.verifyNotificationGrowl('Temporarily unable to save reason. Please try again.');
      });
    });
  });
});

const getRejectionReasonFailurePayload = () => {
  return {
    setApplicationRejectionReason: {
      response: {
        errors: [{message: 'mock applicant error response'}],
        data: {setApplicationRejectionReason: null},
      },
    },
  };
};
