import moment from 'moment';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {applicantRejectReasonUpdate} from '../../../support/shared/applicant_reject_reason_update.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe(`Verify Applicant reject reasons`, () => {
  const UEN = '100000008C';
  let jobPostId;
  const toInterviewApplicationsIndex = [0];
  const successfulApplicationsIndex = [1];
  const unSuccessfulApplicationsIndex = [2, 3, 4];
  before(() => {
    cy.home().login('Manage Applicant Status', 'Job Admin');
    cy.log('seed data - create a job and apply applicants via couchDB with Received status');
    const payLoad = apiJobData.defaultJobPost;
    payLoad.postedCompany.uen = UEN;
    const couchDBApplicantsMockData = [];
    seedXNewJobsForUEN(1, payLoad, UEN)
      .then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        const pushToMockApplicantsArray = (index, statusId, statusDescription, bookmarkedOn) => {
          couchDBApplicantsMockData.push({
            ...couchDBMockData.defaultApplicantData(response[0].uuid),
            ...(statusId ? {statusId} : {}),
            ...(statusDescription ? {statusDescription} : {}),
            createdOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
            ...(bookmarkedOn
              ? {
                  bookmarkedOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
                }
              : {}),
          });
        };
        toInterviewApplicationsIndex.forEach((index) => pushToMockApplicantsArray(index, 1, 'Under Review', false));
        successfulApplicationsIndex.forEach((index) => pushToMockApplicantsArray(index, 2, 'Successful', false));
        unSuccessfulApplicationsIndex.forEach((index) => pushToMockApplicantsArray(index, 3, 'Unsuccessful', true));
        Cypress._.range(6, 15).forEach((index) => pushToMockApplicantsArray(index));
      })
      .then(() => {
        couchDBApi.bulkDocs(couchDBApplicantsMockData);
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const jobStatusAndTab = [
    {jobStatus: 'Open', jobStatusId: 102},
    {jobStatus: 'Re-open', jobStatusId: 103},
    {jobStatus: 'Closed', jobStatusId: 9},
  ];

  jobStatusAndTab.forEach(({jobStatus, jobStatusId}) => {
    describe(`Verify Applicant reject reasons when status unsuccessful in ${jobStatus} jobs`, () => {
      before(() => {
        cy.log('seed data - update job status');
        cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
        cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
      });

      it('should not display application reject reason dropdown when status is To Interview', () => {
        cy.clickOnCandidate(0);
        cy.get('[data-cy=application-rejection-reason]').should('not.exist');
      });

      it('should not display application reject reason dropdown when status is Successful', () => {
        cy.clickOnCandidate(1);
        cy.get('[data-cy=application-rejection-reason]').should('not.exist');
      });

      it('should display application reject reason dropdown and values when status is unsuccessful', () => {
        cy.clickOnCandidate(2);
        cy.get('[data-cy=application-rejection-reason]').should('exist');
        applicantRejectReasonUpdate.verifyApplicantsAllRejectReasonValuesInDropdown();
      });

      it('should display an additional textbox when dropdown reason is only Others', () => {
        cy.clickOnCandidate(2);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Turned down job offer');
        applicantRejectReasonUpdate.verifyOthersRejectReasonTextSectionIs('not.exist');
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        applicantRejectReasonUpdate.verifyOthersRejectReasonTextSectionIs('exist');
      });

      it('should display reject reason dropdown and values when application status is unsuccessful in saved tab for bookmarked applicant', () => {
        cy.clickOnSavedTab().shouldHaveCardLoaderLoaded().clickOnCandidate(0);
        cy.get('[data-cy=application-rejection-reason]').should('exist');
        applicantRejectReasonUpdate.verifyApplicantsAllRejectReasonValuesInDropdown();
      });

      it('should display an additional textbox when dropdown reason is only Others in saved tab for bookmarked applicant', () => {
        cy.clickOnCandidate(0);
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Turned down job offer');
        applicantRejectReasonUpdate.verifyOthersRejectReasonTextSectionIs('not.exist');
        applicantRejectReasonUpdate.selectApplicantRejectReasons('Others');
        applicantRejectReasonUpdate.verifyOthersRejectReasonTextSectionIs('exist');
      });
    });
  });
});
