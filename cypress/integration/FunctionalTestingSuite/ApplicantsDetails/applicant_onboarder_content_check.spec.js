import * as ApplicantNavigate from '../../../support/shared/manage_applicants_page.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';
import {seedNewJobInDB, removeSeededJobInDB} from '../../../support/manageData/job.seed.js';

describe('Applicant - on boarder test', function () {
  const UEN = '100000003C';
  let jobPostID;
  before(() => {
    cy.log('seed data - create a job');
    seedNewJobInDB({job: {posted_uen: UEN}}).then((results) => {
      cy.log(results.job.job_post_id);
      jobPostID = results.job.job_post_id;
    });
  });

  beforeEach(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });

  afterEach(() => {
    cy.logout();
  });

  after(() => {
    removeSeededJobInDB(jobPostID);
  });

  describe('Applicant - on boarder test 1: check the onboarder content', function () {
    it('Should have on boarder when go to applicant view 1st time', function () {
      cy.openJobPageByJobPostId(jobPostID);
      ApplicantNavigate.shouldHaveApplicantOnboarderContent();
      cy.clickOnAllJobs('side');
      cy.openJobPageByJobPostId(jobPostID);
      ApplicantNavigate.shouldNotHaveOnboarderModule();
      cy.get('[data-cy="onboarding-link"]').contains('How do I sort applicants by suitability for my job?').click(); //click on applicant onboarder link
      ApplicantNavigate.shouldHaveApplicantOnboarderContent();
    });
  });

  describe('Applicant - on boarder test 2: onboarder should not appear after skip button pressed', function () {
    it('Should have on boarder when go to applicant view', function () {
      cy.openJobPageByJobPostId(jobPostID).skipApplicantsOnboarding().logout();
      checkLogoutPage();
      cy.home().login('Account with a lot of jobs', 'Job Admin').openJobPageByJobPostId(jobPostID);
      ApplicantNavigate.shouldNotHaveOnboarderModule();
    });
  });
});
