import {mcf} from '@mcf/constants';
import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {fillKeyInformationOnJobPosting} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyPostalCodeValidation,
  verifyLocalAddressFieldsValidation,
  verifyOverseasAddressEmptyFields,
  verifyOverseasAddressMaxLength,
  selectLocalWorkplaceOptionFromWorkplaceDetails,
  selectOverseasOptionFromWorkplaceDetails,
  verifyLocalWorkplaceDetailValueExists,
  fillOverseasWorkplaceDetails,
  fillLocalWorkplaceDetails,
  verifyLabelAndDefaultValuesInWorkplaceDetailsTab,
  checkMultipleLocationFromWorkplaceDetails,
  uncheckMultipleLocationFromWorkplaceDetails,
  checkSameLocationFromWorkplaceDetails,
  verifyLocalWorkplaceDetailsReadonlyValues,
  verifyFieldsOnSelectedOverseasOptionFromWorkplaceDetails,
  verifyMultipleWorkplaceDetailValues,
  verifyOverseasWorkplaceDetailValueExists,
  verifyWorkplaceAddressBasedOnUENAndDisabledField,
  verifyBlockOrHouseNumberValidation,
  verifyStreetNameValidation,
  verifyBuildingNameValidation,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  selectThirdPartyEmployerOption,
  verifyUENSelectionIsValid,
} from '../../../support/shared/job_post_description_page.functions';
import {deleteCompanyAddress, seedCompanyAddressInfo} from '../../../../cypress/support/manageData/company.seed';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Validate and Verify Workplace Details Tab in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.fixture('JobPosting.json')
      .as('jobPost')
      .then((jobPost) => {
        cy.clickNewJobPosting();
        cy.shouldHaveFormLoaderLoaded();
        fillJobDescriptionStep(jobPost.jobPost1);
        onClickNext();
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillNotFromDropDown);
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillFromDropDown);
        onClickNext();
        fillKeyInformationOnJobPosting(jobPost.jobPost1);
        onClickNext();
      });
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should have correct field values and labels based on the selected location type', function () {
    cy.log('it should have the correct default field labels and checkbox values');
    verifyLabelAndDefaultValuesInWorkplaceDetailsTab(this.jobPost.jobPost1.jobWorkplaceDetail);

    cy.log('it should contain autopopulated employers address when same as company address is selected');
    verifyWorkplaceAddressBasedOnUENAndDisabledField('Job Post Testing Account');
    verifyLocalWorkplaceDetailsReadonlyValues();

    cy.log('it should have all field values to be readonly and empty when multiple location is selected');
    checkMultipleLocationFromWorkplaceDetails();
    verifyMultipleWorkplaceDetailValues();
    verifyLocalWorkplaceDetailsReadonlyValues();

    cy.log('it should have the correct field values when no location type is selected');
    uncheckMultipleLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);

    cy.log('it should have the correct field values and labels when overseas location selected');
    selectOverseasOptionFromWorkplaceDetails();
    verifyFieldsOnSelectedOverseasOptionFromWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    fillOverseasWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
  });

  it('should retain the same values when moving from another tab', function () {
    cy.log('it should retain field values if returning from another tab when same as company address is selected');
    selectLocalWorkplaceOptionFromWorkplaceDetails();
    onClickNext();
    onClickBack();
    verifyWorkplaceAddressBasedOnUENAndDisabledField('Job Post Testing Account');

    cy.log('it should retain field values if returning from another tab when multiple location is selected');
    checkMultipleLocationFromWorkplaceDetails();
    onClickNext();
    onClickBack();
    verifyMultipleWorkplaceDetailValues();

    cy.log('it should retain field values if returning from another tab when no location type is selected');
    uncheckMultipleLocationFromWorkplaceDetails();
    fillLocalWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    onClickNext();
    onClickBack();
    verifyLocalWorkplaceDetailValueExists(this.jobPost.jobPost1.jobWorkplaceDetail);

    cy.log('it should retain field values if returning from another tab when overseas location is selected');
    selectOverseasOptionFromWorkplaceDetails();
    fillOverseasWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
    onClickNext();
    onClickBack();
    verifyOverseasWorkplaceDetailValueExists(this.jobPost.jobPost1.jobWorkplaceDetail);
  });

  it('show third party employer address when same as company address selected for posting on behalf', function () {
    const thirdPartyEmployerUen = '100000005C';
    const thirdPartyEmployerAddress = {
      uen: thirdPartyEmployerUen,
      block: '135A',
      street: 'PANDAN CRESCENT',
      building: 'INNOCOM CENTRE',
      postalCode: '128462',
      purposeId: mcf.COMPANY_ADDRESS_PURPOSE.OPERATING,
    };
    cy.log('seed company address for third party employer');
    seedCompanyAddressInfo(thirdPartyEmployerAddress).then(() => {
      cy.log('it should show third party employer address when same as company address selected for posting on behalf');
      // click back 3 times to Job Description step
      onClickBack();
      onClickBack();
      onClickBack();

      selectThirdPartyEmployerOption();
      verifyUENSelectionIsValid(thirdPartyEmployerUen, thirdPartyEmployerUen);

      // click next 3 times to Workplace Details step
      onClickNext();
      onClickNext();
      onClickNext();

      selectLocalWorkplaceOptionFromWorkplaceDetails();
      checkSameLocationFromWorkplaceDetails();
      verifyLocalWorkplaceDetailValueExists({
        jobPostPostalCodeFillValue: thirdPartyEmployerAddress.postalCode,
        jobPostBlockOrHouseNoFillValue: thirdPartyEmployerAddress.block,
        jobPostStreetNameFillValue: thirdPartyEmployerAddress.street,
        jobPostBuildingNameFileValue: thirdPartyEmployerAddress.building,
      });

      cy.log('clean up company address for third party employer');
      deleteCompanyAddress(thirdPartyEmployerUen);
    });
  });

  it('should validate fields correctly', function () {
    selectLocalWorkplaceOptionFromWorkplaceDetails();
    checkMultipleLocationFromWorkplaceDetails();
    uncheckMultipleLocationFromWorkplaceDetails();
    verifyPostalCodeValidation();
    verifyBlockOrHouseNumberValidation();
    verifyStreetNameValidation();
    verifyBuildingNameValidation();

    checkMultipleLocationFromWorkplaceDetails();
    uncheckMultipleLocationFromWorkplaceDetails();
    onClickNext();
    verifyLocalAddressFieldsValidation();

    selectOverseasOptionFromWorkplaceDetails();
    onClickNext();
    verifyOverseasAddressEmptyFields();
    verifyOverseasAddressMaxLength();
  });
});
