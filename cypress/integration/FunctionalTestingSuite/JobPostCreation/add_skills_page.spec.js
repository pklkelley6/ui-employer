import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {
  shouldHaveAddMoreSkillsValueExists,
  searchAndSelectAddMoreSkillSection,
  shouldCountMatchRecommendedSkills,
  verifyAutoPopulateRecommendedSkillsValue,
} from '../../../support/shared/job_post_skills_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Add more skills tab in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should verify max allowed 16 recommended skills based on job title and job description and verify duplicate skills removed and also verify dedup skills from bottom section', function () {
    fillJobDescriptionStep(this.jobPost.jobPostTitleAndDescriptionRecommendedSkill);
    onClickNext();
    shouldCountMatchRecommendedSkills(this.jobPost.jobPostTitleAndDescriptionRecommendedSkill.recommendedSkillsCount);
    verifyAutoPopulateRecommendedSkillsValue(
      this.jobPost.jobPostTitleAndDescriptionRecommendedSkill.autoRecommendedSkills,
      {exist: true},
    );
    onClickBack();
    fillJobDescriptionStep(this.jobPost.jobPostValidateUniqueRecommendedSkills);
    onClickNext();
    shouldCountMatchRecommendedSkills(this.jobPost.jobPostValidateUniqueRecommendedSkills.recommendedSkillsCount);
    verifyAutoPopulateRecommendedSkillsValue(
      this.jobPost.jobPostValidateUniqueRecommendedSkills.autoRecommendedSkills,
      {exist: true},
    );
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPostValidateUniqueRecommendedSkills.skillFromDropDown);
    verifyAutoPopulateRecommendedSkillsValue(
      this.jobPost.jobPostValidateUniqueRecommendedSkills.dedupRecommendedSkills,
      {exist: true},
    );
    shouldHaveAddMoreSkillsValueExists(
      Cypress.config('backendFeatureFlag')['allowMcfApiConstant']
        ? this.jobPost.jobPostValidateUniqueRecommendedSkills.skillFromDropDown.apiConstSelectSkillList
        : this.jobPost.jobPostValidateUniqueRecommendedSkills.skillFromDropDown.selectSkillList,
    );
  });

  it('should option to search and add more skills and verify skills are moved to add more skills section if skills are exist in recommended section', function () {
    fillJobDescriptionStep(this.jobPost.jobPost1);
    onClickNext();
    verifyAutoPopulateRecommendedSkillsValue(
      Cypress.config('backendFeatureFlag')['allowMcfApiConstant']
        ? this.jobPost.jobPost1.apiConstAutoRecommendedSkills
        : this.jobPost.jobPost1.autoRecommendedSkills,
      {
        exist: true,
      },
    );
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillFromDropDown);
    shouldHaveAddMoreSkillsValueExists([
      ...this.jobPost.jobPost1.skillNotFromDropDown.selectSkillList,
      ...this.jobPost.jobPost1.skillFromDropDown.selectSkillList,
    ]);
  });
});
