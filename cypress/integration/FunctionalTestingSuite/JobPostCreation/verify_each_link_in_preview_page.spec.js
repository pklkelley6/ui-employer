import {
  shouldHaveJobDescriptionValueExists,
  fillJobDescriptionStep,
} from '../../../support/shared/job_post_description_page.functions';
import {
  shouldHaveAddMoreSkillsValueExists,
  searchAndSelectAddMoreSkillSection,
} from '../../../support/shared/job_post_skills_page.functions';
import {
  fillKeyInformationOnJobPosting,
  verifyKeyInformationValueExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyLocalWorkplaceDetailValueExists,
  uncheckSameLocationFromWorkplaceDetails,
  fillLocalWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  clickOnLinkIn,
  verifyLinkAndColorCodeEnable,
  verifyLinkAndColorCodeDisable,
} from '../../../support/shared/job_post_preview_page.functions';
import {onClickNext, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import jobPostFormStructure from '../../../fixtures/JobPostFormStructure';
import {screeningQuestions} from '../../../support/shared/job_post_manage_screening_questions.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import * as jobPost from '../../../fixtures/JobPosting.json';

describe('Verify job post steps links color code and  edit link in preview page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(jobPost.jobPost1);
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.logout();
  });

  describe('Validate job post step link color code', () => {
    it('should disabled links for all steps', () => {
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      onClickNext();
    });

    it('should enabled for step 1 - "job description" and disabled for other steps', () => {
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillNotFromDropDown);
      searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillFromDropDown);
      onClickNext();
    });

    it('should enabled for step 1 and 2 - "job description, skills" and disabled for other steps', () => {
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      fillKeyInformationOnJobPosting(jobPost.jobPost1);
      onClickNext();
    });

    it('should enabled for Step 1, 2 and 3 - "job description, skills, key information" and disabled for other steps', () => {
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      uncheckSameLocationFromWorkplaceDetails();
      fillLocalWorkplaceDetails(jobPost.jobPost1.jobWorkplaceDetail);
      onClickNext();
    });

    it('should enabled for Step 1, 2, 3 and 4 - "job description, skills, key information, Workplace Details" and disabled for other tabs', () => {
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      onClickNext();
    });

    it('should enabled for Step 1, 2, 3, 4 and 5 - "job description, skills, key information, Workplace Details, Screening Questions" and disabled for other tabs', () => {
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.previewScreeningQuestionsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
    });
  });

  describe('Validate Edit links in preview page for all sections', () => {
    it('should able to navigate to navigate job-description tab', () => {
      clickOnLinkIn('job-description');
      cy.shouldHaveFormLoaderLoaded();
      shouldHaveJobDescriptionValueExists(jobPost.jobPost1);
      onClickNext();
      onClickNext();
      onClickNext();
      onClickNext();
      onClickNext();
    });

    it('should able to navigate to navigate skills tab', () => {
      clickOnLinkIn('skills');
      cy.shouldHaveFormLoaderLoaded();
      shouldHaveAddMoreSkillsValueExists([
        ...jobPost.jobPost1.skillNotFromDropDown.selectSkillList,
        ...jobPost.jobPost1.skillFromDropDown.selectSkillList,
      ]);
      onClickNext();
      onClickNext();
      onClickNext();
      onClickNext();
    });

    it('should able to navigate to navigate Key-Information tab', () => {
      clickOnLinkIn('key-information');
      cy.shouldHaveFormLoaderLoaded();
      verifyKeyInformationValueExists(jobPost.jobPost1);
      onClickNext();
      onClickNext();
      onClickNext();
    });

    it('should able to navigate to navigate Workplace-Details tab', () => {
      clickOnLinkIn('workplace-details');
      cy.shouldHaveFormLoaderLoaded();
      verifyLocalWorkplaceDetailValueExists(jobPost.jobPost1.jobWorkplaceDetail);
      onClickNext();
      onClickNext();
    });

    it('should able to navigate to navigate Screening-Questions tab', () => {
      clickOnLinkIn('screening-questions');
      cy.shouldHaveFormLoaderLoaded();
      screeningQuestions.validateIncludeScreeningQuestionsCheckboxIs('not.be.checked');
      screeningQuestions.validateScreeningQuestionsFieldsIs('not.exist');
    });
  });
});
