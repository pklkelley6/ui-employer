import {
  shouldHaveJobDescriptionValueExists,
  fillJobDescriptionStep,
} from '../../../support/shared/job_post_description_page.functions';
import {
  shouldHaveAddMoreSkillsValueExists,
  searchAndSelectAddMoreSkillSection,
} from '../../../support/shared/job_post_skills_page.functions';
import {
  fillKeyInformationOnJobPosting,
  verifyKeyInformationValueExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyLocalWorkplaceDetailValueExists,
  fillLocalWorkplaceDetails,
  uncheckSameLocationFromWorkplaceDetails,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  previewJobDescriptionDetails,
  previewSkillsDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
  verifyEditLinkOptionOnJobPosting,
  clickOnLinkIn,
  reviewPagesOnJobPosting,
} from '../../../support/shared/job_post_preview_page.functions';
import {
  onClickNext,
  suppressBeforeUnloadEvent,
  verifyStaticNudgeContent,
} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Validate and verify each and every tab details in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.fixture('JobPosting.json')
      .as('jobPost')
      .then((jobPost) => {
        cy.clickNewJobPosting();
        cy.shouldHaveFormLoaderLoaded();
        verifyStaticNudgeContent();
        fillJobDescriptionStep(jobPost.jobPost1);
        onClickNext();
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillNotFromDropDown);
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillFromDropDown);
        onClickNext();
        fillKeyInformationOnJobPosting(jobPost.jobPost1);
        onClickNext();
        uncheckSameLocationFromWorkplaceDetails();
        fillLocalWorkplaceDetails(jobPost.jobPost1.jobWorkplaceDetail);
        onClickNext();
        onClickNext();
      });
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should have edit behavior for all tab from job post review page', function () {
    reviewPagesOnJobPosting();
    verifyEditLinkOptionOnJobPosting();
  });

  it('should preview all the content is correct from all tab in job post review page', function () {
    previewJobDescriptionDetails(this.jobPost.jobPost1);
    previewSkillsDetails(
      Object.values(
        Cypress.config('backendFeatureFlag')['allowMcfApiConstant']
          ? this.jobPost.jobPost1.apiConstPreviewSkills
          : this.jobPost.jobPost1.previewSkills,
      ),
    );
    previewKeyInformationDetails(this.jobPost.jobPost1.keyInformation);
    previewWorkplaceDetails(this.jobPost.jobPost1.jobWorkplaceDetail);
  });

  it('should have edit behavior for job description tab from job post review page', function () {
    clickOnLinkIn('job-description');
    cy.shouldHaveFormLoaderLoaded();
    shouldHaveJobDescriptionValueExists(this.jobPost.jobPost1);
  });

  it('should have edit behavior for more add skills tab from job post review page', function () {
    onClickNext();
    onClickNext();
    onClickNext();
    onClickNext();
    onClickNext();
    clickOnLinkIn('skills');
    cy.shouldHaveFormLoaderLoaded();
    shouldHaveAddMoreSkillsValueExists([
      ...this.jobPost.jobPost1.skillNotFromDropDown.selectSkillList,
      ...this.jobPost.jobPost1.skillFromDropDown.selectSkillList,
    ]);
  });

  it('should have edit behavior for key information tab from job post review page', function () {
    onClickNext();
    onClickNext();
    onClickNext();
    onClickNext();
    clickOnLinkIn('key-information');
    cy.shouldHaveFormLoaderLoaded();
    verifyKeyInformationValueExists(this.jobPost.jobPost1);
  });

  it('should have edit behavior for workplace details tab from job post review page', function () {
    onClickNext();
    onClickNext();
    onClickNext();
    clickOnLinkIn('workplace-details');
    cy.shouldHaveFormLoaderLoaded();
    verifyLocalWorkplaceDetailValueExists(this.jobPost.jobPost1.jobWorkplaceDetail);
  });
});
