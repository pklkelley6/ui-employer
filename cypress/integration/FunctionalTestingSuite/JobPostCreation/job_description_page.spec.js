import {
  verifyThirdPartyEmployerFieldsLabel,
  jobDescriptionShouldHaveValue,
  checkCharacterCountExistsMax,
  jobTitleSelector,
  jobOccupationSelector,
  jobDescriptionSelector,
  jobTitleValidationErrorSelector,
  jobOccupationValidationErrorSelector,
  jobDescriptionValidationError,
  fillJobDescriptionStep,
  isEditableJobDescription,
  checkCustomizableTextIconForJobDescriptionFields,
  checkCharacterCountShow,
  verifyDefaultThirdPartyEmployerUnChecked,
  verifyThirdPartyEmployerLabel,
  selectThirdPartyEmployerOption,
  verifyThirdPartyEmployerEnabledFields,
  unSelectThirdPartyEmployerOption,
  verifyThirdPartyEmployerFieldLabelNotExists,
  verifyUENSelectionError,
  verifyUENSelectionIsValid,
  inputJobDescriptionStep,
} from '../../../support/shared/job_post_description_page.functions';
import {
  validationErrorTextExists,
  validationSelectorNotExists,
  fillValueAndBlur,
  onClickNext,
  suppressBeforeUnloadEvent,
  onClickBack,
} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {
  searchAndOpenJobOnTab,
  mouseoverOnManageMenuButton,
  clickViewEditJobPostButton,
  clickEditJobPost,
} from '../../../support/shared/job_page.functions';
import {validateSkillsPageExists} from '../../../support/shared/job_post_skills_page.functions.js';
const faker = require('faker');

describe('Job description tab in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should verify and validate job title field', () => {
    cy.get(jobTitleSelector).focus().blur();
    validationErrorTextExists(jobTitleValidationErrorSelector, 'Please fill this in');
    fillValueAndBlur(jobTitleSelector, 'A');
    validationErrorTextExists(jobTitleValidationErrorSelector, 'Please enter at least 3 characters');
    fillValueAndBlur(jobTitleSelector, faker.lorem.sentences(25).slice(0, 201));
    validationErrorTextExists(jobTitleValidationErrorSelector, 'Please keep within 200 characters');
    fillValueAndBlur(jobTitleSelector, faker.lorem.sentences(25).slice(0, 200));
    validationSelectorNotExists(jobTitleValidationErrorSelector);
  });

  it('should verify job occupation validations', () => {
    cy.get(jobOccupationSelector).focus().blur();
    validationErrorTextExists(jobOccupationValidationErrorSelector, 'Please type and select');
  });

  it('should verify job occuption exact selection match from ssoc list values', () => {
    cy.get(jobOccupationSelector).clear().type('manager');
    cy.get('li[id*=react-autowhatever] div').first().should('have.text', 'Manager');
  });

  it('should verify job description validations', function () {
    cy.get(jobDescriptionSelector).focus().blur();
    validationErrorTextExists(jobDescriptionValidationError, 'Please fill this in');
    jobDescriptionShouldHaveValue('Short Description');
    validationErrorTextExists(jobDescriptionValidationError, 'Please enter at least 300 characters');
    checkCharacterCountExistsMax(this.jobPost.jobPost1.jobDescriptionMaxLimitText);
    validationErrorTextExists(jobDescriptionValidationError, 'Please keep within 20,000 characters');
  });

  it('should have job title & description field as editable and validate description max character allowed', function () {
    fillJobDescriptionStep(this.jobPost.jobPost1);
    isEditableJobDescription();
    checkCustomizableTextIconForJobDescriptionFields(this.jobPost.jobPost1.jobDescriptionTextCustomizeIcon);
    checkCharacterCountShow(this.jobPost.jobPost1.jobDescriptionValueFill);
  });

  it('should have 3rd party employer enable field and verify labels', function () {
    verifyDefaultThirdPartyEmployerUnChecked();
    verifyThirdPartyEmployerLabel("You're posting on behalf of another company");
    selectThirdPartyEmployerOption();
    verifyThirdPartyEmployerFieldsLabel('Company Name / UEN');
    verifyThirdPartyEmployerEnabledFields();
    onClickNext();
    verifyUENSelectionError('xyz');
    verifyUENSelectionIsValid('GOVERNMENT', 'GOVERNMENT TECHNOLOGY AGENCY');
    unSelectThirdPartyEmployerOption();
    verifyThirdPartyEmployerFieldLabelNotExists();
  });
});

describe('Validate Minimum JD character based on selected SSOC code', function () {
  let jobPostId;
  const UEN = '200610233W';
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
    cy.log('seed data - create new job to validate edit scenario');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
    });
    cy.clickNewJobPosting().shouldHaveFormLoaderLoaded();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const character100ErrorMessage = 'Please enter at least 100 characters';
  const character300ErrorMessage = 'Please enter at least 300 characters';

  const ssocDetails100Character = {
    4906: 'Academic Assistant',
    516: 'Customer Service Clerk',
    64: 'Book Editor',
    741: 'Audio and Video Equipment Engineer',
    858: 'Business Consultant',
    919: 'Other Insulation Workers',
  };

  const ssocDetails300Character = {
    10: 'Database Architect',
    2207: 'Talent Management Manager',
    33: 'Librarian',
  };

  describe('should show corresponding error message in create job based on Occupation selected', () => {
    for (const [ssocId, ssocDesc] of Object.entries(ssocDetails100Character)) {
      it(`should show 100 character error message when job description less than 100 character for SSOC ID ${ssocId} - ${ssocDesc}`, () => {
        inputAndValidateErrorDescriptionForOccupation(
          ssocDesc,
          faker.lorem.paragraphs(150).substring(0, 15),
          character100ErrorMessage,
        );
      });
    }

    for (const [ssocId, ssocDesc] of Object.entries(ssocDetails300Character)) {
      it(`should show 300 character error message when job description less than 300 character for SSOC ID ${ssocId} - ${ssocDesc}`, () => {
        inputAndValidateErrorDescriptionForOccupation(
          ssocDesc,
          faker.lorem.paragraphs(150).substring(0, 150),
          character300ErrorMessage,
        );
      });
    }
  });

  describe('should not show error message in create job - step 1 page and should allow to next page', () => {
    it(`should allow next page when select Occupation as ${ssocDetails100Character[64]} and description is 100 character`, () => {
      inputValidDescriptionAndValidateSkillsPageExists(
        ssocDetails100Character[64],
        faker.lorem.paragraphs(150).substring(0, 100),
      );
      onClickBack(); // Click back for next test to continue
    });

    it(`should allow next page when select Occupation as ${ssocDetails300Character[33]} and description is 300 character`, () => {
      inputValidDescriptionAndValidateSkillsPageExists(
        ssocDetails300Character[33],
        faker.lorem.paragraphs(150).substring(0, 300),
      );
    });
  });

  describe('should show corresponding error message in edit job based on Occupation updated', () => {
    it('search and open job to edit', () => {
      searchAndOpenJobOnTab('Open', jobPostId);
      mouseoverOnManageMenuButton();
      clickViewEditJobPostButton();
      clickEditJobPost();
    });

    for (const [ssocId, ssocDesc] of Object.entries(ssocDetails100Character)) {
      it(`should show 100 character error message when job description less than 100 character for SSOC ID ${ssocId} - ${ssocDesc}`, () => {
        inputAndValidateErrorDescriptionForOccupation(
          ssocDesc,
          faker.lorem.paragraphs(150).substring(0, 15),
          character100ErrorMessage,
        );
      });
    }

    for (const [ssocId, ssocDesc] of Object.entries(ssocDetails300Character)) {
      it(`should show 300 character error message when job description less than 300 character for SSOC ID ${ssocId} - ${ssocDesc}`, () => {
        inputAndValidateErrorDescriptionForOccupation(
          ssocDesc,
          faker.lorem.paragraphs(150).substring(0, 150),
          character300ErrorMessage,
        );
      });
    }
  });

  describe('should not show error message in edit job - step 1 page and should allow to next page', () => {
    it(`should allow next page when select Occupation as ${ssocDetails100Character[64]} and description is 100 character`, () => {
      inputValidDescriptionAndValidateSkillsPageExists(
        ssocDetails100Character[64],
        faker.lorem.paragraphs(150).substring(0, 100),
      );
      onClickBack(); // Click back for next test to continue
    });

    it(`should allow next page when select Occupation as ${ssocDetails300Character[33]} and description is 300 character`, () => {
      inputValidDescriptionAndValidateSkillsPageExists(
        ssocDetails300Character[33],
        faker.lorem.paragraphs(150).substring(0, 300),
      );
    });
  });
});

const inputValidDescriptionAndValidateSkillsPageExists = (occupation, jobDescription) => {
  inputJobDescriptionStep(
    {
      jobOccupationPartial: occupation,
      jobOccupationSelection: occupation,
      jobDescriptionValueFill: jobDescription,
    },
    {paste: true},
  );
  onClickNext();
  validateSkillsPageExists();
};

const inputAndValidateErrorDescriptionForOccupation = (occupation, jobDescription, errorMessage) => {
  inputJobDescriptionStep(
    {
      jobOccupationPartial: occupation,
      jobOccupationSelection: occupation,
      jobDescriptionValueFill: jobDescription,
    },
    {paste: true},
  );
  onClickNext();
  cy.contains(errorMessage).should('exist');
};
