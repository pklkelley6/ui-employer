import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {
  fillKeyInformationOnJobPosting,
  verifyKeyInformationLabelOnJobPosting,
  verifyJobDurationValue,
  verifyJobDurationLabel,
  verifyKeyInformationValueExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
describe('Key information tab in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('JobPosting.json').as('jobPost');
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
  });
  after(() => {
    cy.logout();
  });

  it('should have all field label from Key information tab in Job Posting Page', function () {
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
    fillJobDescriptionStep(this.jobPost.jobPost1);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
    onClickNext();
    verifyJobDurationLabel(this.jobPost.jobPost1, {isJobDurationFieldEnable: true});
    verifyKeyInformationLabelOnJobPosting(this.jobPost.jobPost1);
    fillKeyInformationOnJobPosting(this.jobPost.jobPost1);
    onClickNext();
    onClickBack();
    verifyJobDurationValue(this.jobPost.jobPost1, {isJobDurationFieldEnable: true});
    verifyKeyInformationValueExists(this.jobPost.jobPost1);
  });
});
