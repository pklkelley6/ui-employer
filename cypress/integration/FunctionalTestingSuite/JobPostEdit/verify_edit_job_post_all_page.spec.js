import {mcf} from '@mcf/constants';
import faker from 'faker';
import {shouldHaveCardLoaderLoaded, openJobPostEdit} from '../../../support/shared/job_page.functions';
import {
  checkEditCountOfJobPost,
  verifyEditJobPostSubmitModal,
} from '../../../support/shared/job_details_page.functions';
import {
  fillJobDescriptionStep,
  shouldHaveJobDescriptionValueExists,
  verifyThirdPartyEmployerFieldsLabel,
  shouldHaveValueForUENDetailsInJobPostEditStep1Page,
  shouldCheckedThirdPartyEmployerOption,
  jobTitleSelector,
  jobOccupationSelector,
  jobDescriptionSelector,
  jobDescriptionShouldHaveValue,
  checkCharacterCountExistsMax,
  jobTitleValidationErrorSelector,
  jobOccupationValidationErrorSelector,
  jobDescriptionValidationError,
  typeAndSelectUEN,
} from '../../../support/shared/job_post_description_page.functions';
import {
  searchAndSelectAddMoreSkillSection,
  shouldHaveAddMoreSkillsValueExists,
  clearAllSkillsFromAddMoreSection,
  verifyMinSkillsRequired,
  verifyUnSelectedSkillsFromRecommendedSection,
  selectedSkillsShouldNotExists,
  recommendedSkillSectionsSelector,
  otherSkillSectionsSelector,
} from '../../../support/shared/job_post_skills_page.functions';
import {
  verifyKeyInformationValueExists,
  verifyJobPostDurationDisabledField,
  verifyQualificationFieldOfStudyValidationsOnJobEditing,
  verifyJobCategoriesValidationsOnJobEditing,
  fillKeyInformationOnJobPosting,
  verifyKeyInformationLabelOnJobPosting,
  verifyNumberOfVacanciesValidations,
  verifyMinimumYearsExperienceValidations,
  verifyEmploymentTypeValidations,
  verifySalaryValidations,
  verifyScheme,
  selectPCPScheme,
  verifyPCPSchemeValidations,
  verifySchemeNotExists,
  verifyJobDurationValue,
  verifyJobDurationLabel,
  schemeDetails,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyLabelAndDefaultValuesInEditWorkplaceDetailsTab,
  verifyPostalCodeValidation,
  verifyLocalAddressFieldsValidation,
  verifyOverseasAddressEmptyFields,
  verifyOverseasAddressMaxLength,
  verifyEmptyLocalValuesInEditWorkplaceDetailsTab,
  verifyEmptyOverseasValuesInEditWorkplaceDetailsTab,
  selectLocalWorkplaceOptionFromWorkplaceDetails,
  selectOverseasOptionFromWorkplaceDetails,
  checkSameLocationFromWorkplaceDetails,
  workplacePostalCode,
  workplaceBlockOrHouseNumber,
  workplaceBuildingName,
  verifyLocalWorkplaceDetailValueExists,
  fillOverseasWorkplaceDetails,
  workplaceStreetName,
  verifyBlockOrHouseNumberValidation,
  verifyStreetNameValidation,
  verifyBuildingNameValidation,
} from '../../../support/shared/job_post_workplace_details_page.functions';
import {
  previewJobDescriptionDetails,
  previewSkillsDetails,
  previewKeyInformationDetails,
  previewWorkplaceDetails,
  previewOverseasWorkPlaceDetails,
  verifyEditLinkOptionOnJobPosting,
  clickOnLinkIn,
  verifyLinkAndColorCodeEnable,
  verifyLinkAndColorCodeDisable,
} from '../../../support/shared/job_post_preview_page.functions';
import {
  verifyFieldsInAcknowledgementPage,
  verifyJobPostPresentInAllJobsList,
} from '../../../support/shared/job_post_success_page.functions';
import {
  validationErrorTextExists,
  validationSelectorNotExists,
  fillValueAndBlur,
  onClickNext,
  onClickBack,
  onClickSubmitJobPost,
  suppressBeforeUnloadEvent,
  verifyStaticNudgeContent,
} from '../../../support/shared/common.functions';
import jobs from '../../../fixtures/JobList';
import jobPostFormStructure from '../../../fixtures/JobPostFormStructure';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {dbJobData} from '../../../fixtures/seedData/db_job_post_seed_data';
import {seedNewJobInDB, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {deleteCompanyAddress, seedCompanyAddressInfo} from '../../../../cypress/support/manageData/company.seed';

describe('Verify edit job post for all pages', () => {
  const MAX_JOB_POST_EDIT_COUNT = 2;
  let jobSeedDataForEdit;
  let jobSeedDataForExpiry;
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Manage Applicant Status', 'Job Admin');
    cy.log('seed test data - Insert job details in DB for jobPostViewEditCheck spec');
    seedNewJobInDB(dbJobData.jobPostViewEditCheck).then((data) => {
      jobSeedDataForEdit = data;
    });
    cy.log('seed test data - Insert job details in DB for jobPostEditWithExpiryScheme spec');
    seedNewJobInDB(dbJobData.jobPostEditWithExpiryScheme).then((data) => {
      jobSeedDataForExpiry = data;
    });
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
    removeSeededJobInDB(jobSeedDataForEdit.job.job_post_id);
    removeSeededJobInDB(jobSeedDataForExpiry.job.job_post_id);
  });

  describe('Verify edit job post for job description (step 1) page', () => {
    it('Verify exists values are correct from job description (step 1) page - UI', () => {
      openJobPostEdit(jobSeedDataForEdit.job.job_post_id);
      verifyStaticNudgeContent();
      cy.clickButton('[data-cy=edit-job-post]');
      assertHeaderJobTitle(jobs.jobPostViewEditCheck.jobTitle);
      shouldCheckedThirdPartyEmployerOption();
      verifyThirdPartyEmployerFieldsLabel();
      shouldHaveValueForUENDetailsInJobPostEditStep1Page(jobs.jobPostViewEditCheck);
      shouldHaveJobDescriptionValueExists(jobs.jobPostViewEditCheck);
      onClickNext();
      onClickBack();
      shouldCheckedThirdPartyEmployerOption();
      verifyThirdPartyEmployerFieldsLabel();
      shouldHaveValueForUENDetailsInJobPostEditStep1Page(jobs.jobPostViewEditCheck);
      shouldHaveJobDescriptionValueExists(jobs.jobPostViewEditCheck);
      checkEditCountOfJobPost(MAX_JOB_POST_EDIT_COUNT);
    });
    it('Verify validation error for edit job post job description (step 1) page- UI', () => {
      cy.get(jobTitleSelector).clear().focus().blur();
      validationErrorTextExists(jobTitleValidationErrorSelector, 'Please fill this in');
      fillValueAndBlur(jobTitleSelector, 'A');
      validationErrorTextExists(jobTitleValidationErrorSelector, 'Please enter at least 3 characters');
      fillValueAndBlur(jobTitleSelector, faker.lorem.sentences(25).slice(0, 201));
      validationErrorTextExists(jobTitleValidationErrorSelector, 'Please keep within 200 characters');
      fillValueAndBlur(jobTitleSelector, faker.lorem.sentences(25).slice(0, 200));
      validationSelectorNotExists(jobTitleValidationErrorSelector);
      cy.get(jobOccupationSelector).clear().focus().blur();
      validationErrorTextExists(jobOccupationValidationErrorSelector, 'Please type and select');
      cy.get(jobDescriptionSelector).clear().focus().blur();
      validationErrorTextExists(jobDescriptionValidationError, 'Please fill this in');
      jobDescriptionShouldHaveValue('Short Description');
      validationErrorTextExists(jobDescriptionValidationError, 'Please enter at least 300 characters');
      checkCharacterCountExistsMax(jobs.jobPostViewEditCheck.jobDescriptionMaxLimitText);
      validationErrorTextExists(jobDescriptionValidationError, 'Please keep within 20,000 characters');
    });
  });
  describe('Verify edit job post for skills (step 2) page', () => {
    it('Verify exists values are correct from skills (step 2) page - UI', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForEdit.job.job_post_id);
      cy.clickButton('[data-cy=edit-job-post]');
      onClickNext();
      assertHeaderJobTitle(jobs.jobPostViewEditCheck.jobTitle);
      shouldHaveAddMoreSkillsValueExists(jobs.jobPostViewEditCheck.previewSkills);
      checkEditCountOfJobPost(MAX_JOB_POST_EDIT_COUNT);
      cy.get('[data-cy=edit-skills-header-description]')
        .should('have.text', 'Include at least 10 skills and no more than 20 skills.')
        .and(
          'not.have.text',
          'Check the pre-selected skills and add other relevant skills. Include at least 10 skills and no more than 20 skills.',
        );
      cy.get('[data-cy=recommended-skills] [class*=skills-header]')
        .should('have.text', 'New skills suggested based on job description')
        .and('not.have.text', 'Skills based on job description');
      cy.get('[data-cy=add-more-skills] [class*=skills-header]')
        .should('have.text', 'Other skills you have added (Includes previously added skills)')
        .and('not.have.text', 'Other skills you have added');
    });
    it('Verify validation error for edit job post skills (step 2) page- UI', () => {
      searchAndSelectAddMoreSkillSection(jobs.jobPostViewEditCheck.skillFromDropDown);
      cy.verifyNotificationGrowl('You can only include up to 20 skills');
      onClickBack();
      cy.get('#job-title').clear().type('Developer').should('have.value', 'Developer');
      onClickNext();
      cy.shouldHaveFormLoaderLoaded();
      selectedSkillsShouldNotExists();
      verifyUnSelectedSkillsFromRecommendedSection(jobs.jobPostViewEditCheck.recommendedUnSelectSkillsSection);
      clearAllSkillsFromAddMoreSection();
      onClickNext();
      verifyMinSkillsRequired();
    });
  });
  describe('Verify edit job post for key information (step 3) page', () => {
    it('Verify exists values are correct from key information (step 3) page - UI', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForEdit.job.job_post_id);
      cy.clickButton('[data-cy=edit-job-post]');
      onClickNext();
      onClickNext();
      checkEditCountOfJobPost(MAX_JOB_POST_EDIT_COUNT);
      verifyJobPostDurationDisabledField();
      verifyJobDurationLabel(jobs.jobPostViewEditCheck, {isJobDurationFieldEnable: false});
      verifyKeyInformationLabelOnJobPosting(jobs.jobPostViewEditCheck);
      verifyJobDurationValue(jobs.jobPostViewEditCheck, {isJobDurationFieldEnable: false});
      verifyKeyInformationValueExists(jobs.jobPostViewEditCheck);
      onClickNext();
      onClickBack();
    });
    it('Verify validation error for edit job post key information (step 3) page- UI', () => {
      verifyJobPostDurationDisabledField();
      cy.get('#number-of-vacancies').clear();
      verifyNumberOfVacanciesValidations();
      verifyJobCategoriesValidationsOnJobEditing();
      cy.get('#job-minimum-years-of-experience').clear();
      verifyMinimumYearsExperienceValidations();
      cy.get('#job-employment-type-select div[class*=multi-value__remove]').each(() => {
        // Due to object being detached (index change) when doing multiple click, looping and clicking first DOM
        cy.get('#job-employment-type-select div[class*=multi-value__remove]').eq(0).click();
      });
      verifyEmploymentTypeValidations();
      verifyQualificationFieldOfStudyValidationsOnJobEditing(jobs.jobPostViewEditCheck);
      cy.get('#job-minimum-salary').clear();
      cy.get('#job-maximum-salary').clear();
      verifySalaryValidations();
    });
    it('should not have Career Trial scheme for selection', function () {
      verifySchemeNotExists('CareerTrial');
    });
    it('should have govt company scheme for edit job post key information (step 3) page- UI', () => {
      verifyScheme('PCP');
      verifyScheme('PMax');
      selectPCPScheme();
      onClickNext();
      verifyPCPSchemeValidations();
    });
    it('should have govt company scheme and expired P max scheme for edit job post key information (step 3) page- UI', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForExpiry.job.job_post_id);
      cy.contains('P-Max');
      cy.clickButton('[data-cy=edit-job-post]');
      shouldHaveCardLoaderLoaded();
      onClickNext();
      onClickNext();
      verifySchemeNotExists('PMax');
    });
  });
  describe('Verify edit job post for workplace details (step 4) page', () => {
    let thirdPartyEmployerAddress;
    before(() => {
      thirdPartyEmployerAddress = {
        uen: jobSeedDataForEdit.job.hiring_uen,
        block: '135A',
        street: 'EUNOS AVENUE 3',
        building: 'GORDON INDUSTRIAL BLDG',
        postalCode: '409838',
        purposeId: mcf.COMPANY_ADDRESS_PURPOSE.OPERATING,
      };

      cy.log('seed company address for third party employer');
      seedCompanyAddressInfo(thirdPartyEmployerAddress);
    });

    it('Verify exists values are correct from workplace details (step 4) page - UI', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForEdit.job.job_post_id);
      cy.clickButton('[data-cy=edit-job-post]');
      onClickNext();
      onClickNext();
      onClickNext();
      verifyLabelAndDefaultValuesInEditWorkplaceDetailsTab(jobs.jobPostViewEditCheck.jobWorkplaceDetail);
    });
    it('Verify address fields cleared when switching between local and overseas', () => {
      selectOverseasOptionFromWorkplaceDetails();
      verifyEmptyOverseasValuesInEditWorkplaceDetailsTab();
      selectLocalWorkplaceOptionFromWorkplaceDetails();
      verifyEmptyLocalValuesInEditWorkplaceDetailsTab();
    });
    it('Verify validation error for edit job post workplace details (step 4) page- UI', () => {
      selectOverseasOptionFromWorkplaceDetails();
      onClickNext();
      verifyOverseasAddressEmptyFields();
      verifyOverseasAddressMaxLength();

      selectLocalWorkplaceOptionFromWorkplaceDetails();
      verifyPostalCodeValidation();
      verifyBlockOrHouseNumberValidation();
      verifyStreetNameValidation();
      verifyBuildingNameValidation();

      cy.get(workplacePostalCode).clear().focus().blur();
      cy.get(workplaceBlockOrHouseNumber).clear().focus().blur();
      cy.get(workplaceStreetName).clear().focus().blur();
      cy.get(workplaceBuildingName).clear().focus().blur();

      onClickNext();

      verifyLocalAddressFieldsValidation();
    });
    it('verify third party employer address shown when same as company address selected', () => {
      cy.log('it should show third party employer address when same as company address selected for posting on behalf');
      selectLocalWorkplaceOptionFromWorkplaceDetails();
      checkSameLocationFromWorkplaceDetails();
      verifyLocalWorkplaceDetailValueExists({
        jobPostPostalCodeFillValue: thirdPartyEmployerAddress.postalCode,
        jobPostBlockOrHouseNoFillValue: thirdPartyEmployerAddress.block,
        jobPostStreetNameFillValue: thirdPartyEmployerAddress.street,
        jobPostBuildingNameFileValue: thirdPartyEmployerAddress.building,
      });

      cy.log('clean up company address for third party employer');
      deleteCompanyAddress(jobSeedDataForEdit.job.hiring_uen);
    });
  });
  describe('Verify edit job post for review page', () => {
    it('Verify exists values are correct from review page - UI', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForEdit.job.job_post_id);
      cy.clickButton('[data-cy=edit-job-post]');
      onClickNext();
      onClickNext();
      onClickNext();
      onClickNext();
      onClickNext();
      assertHeaderJobTitle(jobs.jobPostViewEditCheck.jobTitle);
      checkEditCountOfJobPost(MAX_JOB_POST_EDIT_COUNT);
      previewJobDescriptionDetails(jobs.jobPostViewEditCheck);
      previewSkillsDetails(Object.values(jobs.jobPostViewEditCheck.previewSkills));
      previewKeyInformationDetails(jobs.jobPostViewEditCheck.keyInformation);
      previewWorkplaceDetails(jobs.jobPostViewEditCheck.jobWorkplaceDetail);
    });
    it('Verify model popup message when clicking on submit job post button - UI', () => {
      onClickSubmitJobPost();
      verifyEditJobPostSubmitModal();
      cy.get('[data-cy="cancel-button-edit-job-submit-modal"]').click();
      cy.get('[data-cy="edit-job-submit-modal"]').should('not.exist');
    });
    it('Verify there is edit links to various steps', () => {
      verifyEditLinkOptionOnJobPosting();
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.jobDescriptionTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.skillsTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.keyInformationTab);
      verifyLinkAndColorCodeEnable(jobPostFormStructure.jobPostEachTabVerification.workplaceDetailsTab);
      verifyLinkAndColorCodeDisable(jobPostFormStructure.jobPostEachTabVerification.previewJobPostTab);
      clickOnLinkIn('job-description');
      cy.shouldHaveFormLoaderLoaded();
      shouldHaveJobDescriptionValueExists(jobs.jobPostViewEditCheck);
      cy.go('back');

      clickOnLinkIn('skills');
      cy.shouldHaveFormLoaderLoaded();
      shouldHaveAddMoreSkillsValueExists(jobs.jobPostViewEditCheck.previewSkills);
      cy.go('back');

      clickOnLinkIn('key-information');
      cy.shouldHaveFormLoaderLoaded();
      verifyKeyInformationValueExists(jobs.jobPostViewEditCheck);
      cy.go('back');

      clickOnLinkIn('workplace-details');
      cy.shouldHaveFormLoaderLoaded();
      verifyLocalWorkplaceDetailValueExists(jobs.jobPostViewEditCheck.jobWorkplaceDetail);
    });
  });
  describe('Verify edit job post success case', () => {
    it('Verify edit job post successs and edited values are correctly updated with edit job post', () => {
      cy.get('[data-cy="all-jobs-label"]').click();
      shouldHaveCardLoaderLoaded();
      openJobPostEdit(jobSeedDataForExpiry.job.job_post_id);
      cy.clickButton('[data-cy=edit-job-post]');
      cy.shouldHaveFormLoaderLoaded();
      typeAndSelectUEN(jobs.jobPostEditWithExpiryScheme.hiringUEN);
      fillJobDescriptionStep(jobs.jobPostEditWithExpiryScheme);
      onClickNext();
      cy.selectOrUnSelectSkills(
        recommendedSkillSectionsSelector,
        jobs.jobPostEditWithExpiryScheme.recommendedSelectSkillsSection,
      );
      cy.selectOrUnSelectSkills(
        otherSkillSectionsSelector,
        jobs.jobPostEditWithExpiryScheme.addMoreUnSelectSkillsSection,
      );
      onClickNext();
      fillKeyInformationOnJobPosting(jobs.jobPostEditWithExpiryScheme, false);
      cy.get(schemeDetails['PMax'].schemeSelector).click({force: true});
      onClickNext();
      fillOverseasWorkplaceDetails(jobs.jobPostEditWithExpiryScheme.jobWorkplaceDetail);
      onClickNext();
      onClickNext();
      onClickSubmitJobPost();
      verifyEditJobPostSubmitModal();
      cy.get('[data-cy="submit-button-edit-job-submit-modal"]').click();
      cy.get('[data-cy="edit-job-submit-modal"]').should('not.exist');
      verifyFieldsInAcknowledgementPage(jobs.jobPostEditWithExpiryScheme, {isCreate: false});
      checkEditCountOfJobPost(1);
      verifyJobPostPresentInAllJobsList(
        jobSeedDataForExpiry.job.job_post_id,
        jobs.jobPostEditWithExpiryScheme.jobTitle,
      );
      openJobPostEdit(jobSeedDataForExpiry.job.job_post_id);
      previewJobDescriptionDetails(jobs.jobPostEditWithExpiryScheme);
      previewSkillsDetails(Object.values(jobs.jobPostEditWithExpiryScheme.previewSkills));
      previewKeyInformationDetails(jobs.jobPostEditWithExpiryScheme.keyInformation);
      previewOverseasWorkPlaceDetails(jobs.jobPostEditWithExpiryScheme.jobWorkplaceDetail);
    });
  });
});

const assertHeaderJobTitle = (jobTitle) => {
  cy.get('header > div').should('have.text', `View/Edit Job Posting - ${jobTitle}`);
};
