import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {getPagination, isCurrentActive, shouldHasPaginationButtonCount} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {apiJobseeker} from '../../../support/api/jobseekerApi.js';

describe('SuggestedTalent - List/Detail view data validation', () => {
  const jobPostId = 'MCF-2001-1006935';
  before(() => {
    cy.home().login('Employer for suggested talent', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('Validate the list data and the detail data for suggested talent', () => {
    cy.log('Check 2nd job in all jobs page should have suggested talent');
    JobsPageNavigate.getJobListSuggestedTalentCountWithIndex(0).should('not.have.text', '0').and('not.have.text', '-'); // should have count number other than 0 or -
    cy.captureGraphqlRequest();
    cy.clickJobByJobPostID(jobPostId);
    cy.skipApplicantsOnboarding();
    cy.clickOnSuggestedTalentTab();
    JobsPageNavigate.shouldHaveCardLoaderLoading();
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.waitGraphqlRequest('getSuggestedTalents')
      .then((responses) => {
        return responses.response.body.data.suggestedTalentsForJob.talents;
      })
      .then((talentDetails) => {
        cy.skipApplicantsOnboarding();
        cy.log('No talent selected when click on suggested talent tab by default');
        ManageJobsNavigate.shouldHaveEmptyTalentRightPanel();
        ManageJobsNavigate.verifyCandidateTypeLabelLeftPanel('Talent');
        cy.wrap(talentDetails.slice(0, 2)).each((talentDetail, index) => {
          // validate first two talent details
          cy.clickOnCandidate(index)
            .task('profileDB', `SELECT nric FROM jobseekers where individual_id = '${talentDetail.talent.id}';`)
            .then((results) => {
              apiJobseeker.getTalentProfileDetails(results[0].nric).then((talentDataToValidate) => {
                ManageJobsNavigate.shouldHaveCandidateListItemInfo(talentDataToValidate);
                ManageJobsNavigate.shouldHaveCandidateRightTopPanelInfo(talentDataToValidate);
                ManageJobsNavigate.shouldHaveCandidateRightBottomPanelInfo(talentDataToValidate);
              });
            });
        });
      });
  });

  it('Validate suggested talent pagination to be a maximum of 3 pages', () => {
    cy.openJobPageByJobPostId(jobPostId, {mode: 'suggested-talents'});
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboarding();
    getPagination(0).should('have.text', '1').then(isCurrentActive);
    getPagination(1).should('have.text', '2');
    getPagination(2).should('have.text', '3');
    getPagination(3).should('have.text', '❯');

    cy.captureGraphqlRequest();
    cy.clickOnPaginationIndex(2); //click on page 3
    cy.waitGraphqlRequest('getSuggestedTalents');

    getPagination(0).should('have.text', '❮');
    getPagination(1).should('have.text', '1');
    getPagination(2).should('have.text', '2');
    getPagination(3).should('have.text', '3').then(isCurrentActive);

    shouldHasPaginationButtonCount(4);
  });
});
