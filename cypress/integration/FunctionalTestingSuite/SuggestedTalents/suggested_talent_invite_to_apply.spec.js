import {removeSeededJobInDB, seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {bookmarkApi} from '../../../support/api/bookmarkApi';
import {getSuggestedTalentMock} from '../../../fixtures/mockResponse/suggestedTalent';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {contactSuggestedTalent} from '../../../support/shared/contact_suggested_talent.functions';
import {userInfos} from '../../../support/users';

// For the purpose of testing we are mocking the active applicant talent under suggested talent tab and
// under saved tab using real details by bookmarking it.
describe('Suggested talent - validate Invite to Apply, Invitation sent, Invited talent tag - end to end flow', () => {
  const loginEmployer = 'Employer for suggested talent';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;
  const applicantNricAsTalent = ['S2405325C', 'T7481565G'];
  let jobPostId, jobUuid;

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
    cy.log('seed data - Create a job and mock two active suggested talent and bookmark');
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        cy.mockGraphQL('profile', {
          getSuggestedTalents: getSuggestedTalentMock(jobUuid, 2, applicantNricAsTalent),
        });
      })
      .then(() => {
        cy.log('seed data - bookmark the talents');
        bookmarkApi.bookmarkApplicantOrTalent(jobUuid, applicantNricAsTalent[0]);
        bookmarkApi.bookmarkApplicantOrTalent(jobUuid, applicantNricAsTalent[1]);
        cy.openJobPageByJobPostId(jobPostId, {mode: 'suggested-talents'}).skipApplicantsOnboardingIfExist();
      })
      .then(() => {
        cy.log('seed data - enabling applicant profile').task(
          'profileDB',
          `Update jobseekers set is_show_profile = 1 where nric in ('${applicantNricAsTalent[0]}', '${applicantNricAsTalent[1]}');`,
        );
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Open job', () => {
    beforeEach(() => {
      cy.mockGraphQL('profile', {
        getSuggestedTalents: getSuggestedTalentMock(jobUuid, 2, applicantNricAsTalent),
      })
        .reload()
        .skipApplicantsOnboardingIfExist();
    });

    it('validate Invite to Apply button in Suggested talent tab and ensure it shows Invitation sent in Talent and Saved tab once invited', () => {
      cy.clickOnCandidate(0);
      contactSuggestedTalent.shouldExistSuggestedTalentInviteToApplyButton();
      contactSuggestedTalent.clickInviteToApplyButton();
      contactSuggestedTalent.shouldExistInvitationSentButton();

      cy.clickOnSavedTab().shouldHaveCardLoaderLoaded().clickOnCandidate(1);
      contactSuggestedTalent.shouldExistInvitationSentButton();
    });

    it('validate Invite to Apply button in Saved tab and ensure it shows Invitation sent in Saved and Talent tab once invited', () => {
      cy.clickOnCandidate(0);
      contactSuggestedTalent.shouldExistSuggestedTalentInviteToApplyButton();
      contactSuggestedTalent.clickInviteToApplyButton();
      contactSuggestedTalent.shouldExistInvitationSentButton();

      cy.clickOnSuggestedTalentTab().skipApplicantsOnboardingIfExist().shouldHaveCardLoaderLoaded().clickOnCandidate(1);
      contactSuggestedTalent.shouldExistInvitationSentButton();
    });

    it('seed talent as applicants and update is_email_sent in DB', () => {
      cy.log('seed talent as applicant via couchDB ');
      cy.task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${applicantNricAsTalent[0]}';`)
        .then((results) => {
          const mockApplicationData = couchDBMockData.defaultApplicantData(jobUuid);
          mockApplicationData.applicant.id = results[0].individual_id;
          couchDBApi.createDocument(mockApplicationData);
        })
        .task(
          'jobDB',
          `Update suggested_talents_email_invitations set is_email_sent = 1 where job_uuid = '${jobUuid}';`,
        );
    });

    it('validate Invited Talent tag shows in Applicants tab for invited applicant', () => {
      cy.clickOnApplicantTab().shouldHaveCardLoaderLoaded().skipApplicantsOnboardingIfExist().clickOnCandidate(0);
      contactSuggestedTalent.shouldExistInvitedTalentTagInApplicantsLHSAndRHS(0);
    });
  });

  describe('Closed job', () => {
    before(() => {
      cy.log('seed data - Close the job via DB and mock suggested talent').task(
        'jobDB',
        `Update jobs set job_status_id = 9 where job_post_id = '${jobPostId}';`,
      );
    });

    beforeEach(() => {
      cy.mockGraphQL('profile', {
        getSuggestedTalents: getSuggestedTalentMock(jobUuid, 2, applicantNricAsTalent),
      });
    });

    it('validate Invited Talent tag shows in Applicants tab for invited applicant', () => {
      cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist().clickOnCandidate(0);
      contactSuggestedTalent.shouldExistInvitedTalentTagInApplicantsLHSAndRHS(0);
    });

    it('validate Invite to Apply button is not showing up in Suggested talent and Saved tabs', () => {
      cy.clickOnSuggestedTalentTab().skipApplicantsOnboardingIfExist().shouldHaveCardLoaderLoaded().clickOnCandidate(0);
      contactSuggestedTalent.shouldNotExistSuggestedTalentInviteToApplyButton();
    });
  });
});
