import moment from 'moment';
import {seedXCloseJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {getJobListSuggestedTalentCountWithIndex} from '../../../support/shared/job_page.functions';
import {getSuggestedTalentTabCount} from '../../../support/shared/manage_applicants_page.functions.js';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';

describe('Suggested talent - Closed jobs error message validation for jobs original posting date more than a year', () => {
  const loginEmployer = 'Employer for suggested talent';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;
  const talentErrorMessage = 'This information is no longer available for this job.';
  let closedJobPostId;

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
    cy.log('seed data - close job and update original posting date to past more than one year');
    seedXCloseJobsForUEN(1, null, UEN).then((response) => {
      closedJobPostId = response[0].metadata.jobPostId;
      cy.task(
        'jobDB',
        `UPDATE jobs SET original_posting_date = '${moment()
          .subtract(1.1, 'years')
          .format('YYYY-MM-DD')}' WHERE job_post_id = '${closedJobPostId}';`,
      );
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(closedJobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should show suggested talent count as "-" in job list page after search', () => {
    cy.clickOnAllJobs().clickOnJobTab('Close').searchForJobs(closedJobPostId);
    getJobListSuggestedTalentCountWithIndex(0).should('have.text', '-');
  });

  it('should not show count after opening job in Suggested Talents tab', () => {
    cy.clickJobByJobPostID(closedJobPostId);
    cy.skipApplicantsOnboardingIfExist();
    getSuggestedTalentTabCount().should('not.exist');
  });

  it(`should show message ${talentErrorMessage} in Suggested talent tab`, () => {
    cy.clickOnSuggestedTalentTab();
    cy.contains(talentErrorMessage).should('exist');
  });
});
