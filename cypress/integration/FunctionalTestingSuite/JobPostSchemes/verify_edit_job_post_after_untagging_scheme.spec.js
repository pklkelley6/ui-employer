import {mcf} from '@mcf/constants';
import moment from 'moment';
import {onClickNext, onClickSubmitJobPost} from '../../../support/shared/common.functions';
import {verifyFieldsInAcknowledgementPage} from '../../../support/shared/job_post_success_page.functions';
import {verifySchemeSectionNotExists} from '../../../support/shared/job_post_key_information_page.functions';
import {verifyGovernmentSupportScheme} from '../../../support/shared/job_post_preview_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {userInfos} from '../../../support/users';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {selectYesOrNoInEditJobModal} from '../../../support/shared/job_page.functions';

describe('Verify edit job post with no active schemes', () => {
  let jobPostId;
  const jobPostDetails = {
    jobTitle: '',
  };

  const activeSchemeDetails = {
    schemeId: mcf.SCHEME_ID.SG_UNITED_TRAINEESHIPS,
    subSchemeId: 0,
  };
  const loginCompany = 'Edit Job Post After Expiring Active Scheme';
  const UEN = Cypress._.find(userInfos, ['companyName', `${loginCompany}`]).entityId;

  before(() => {
    cy.home();
    cy.login(`${loginCompany}`, 'Job Admin');

    cy.log('Seed data - Only 1 active scheme available')
      .task('jobDB', `DELETE FROM company_scheme where UEN = '${UEN}';`)
      .task(
        'jobDB',
        `INSERT INTO company_scheme (UEN, SCHEME_ID, SUB_SCHEME_ID) VALUES ('${UEN}', ${activeSchemeDetails.schemeId}, ${activeSchemeDetails.subSchemeId});`,
      );

    cy.log(`Seed new job with active schemeId: ${activeSchemeDetails.schemeId}`);
    const payLoad = apiJobData.defaultJobPost;
    payLoad.postedCompany.uen = UEN;
    payLoad.schemes = [{id: activeSchemeDetails.schemeId}];

    seedXNewJobsForUEN(1, payLoad, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobPostDetails.jobTitle = response[0].title;
    });

    cy.log('Expire the scheme').task(
      'jobDB',
      `UPDATE company_scheme SET expiry_date = '${moment()
        .subtract(2, 'days')
        .format('YYYY-MM-DD')}' WHERE UEN = '${UEN}';`,
    );
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
    removeSeededJobInDB(jobPostId);
  });

  it('should not show scheme details in Edit Step 3', () => {
    cy.openJobPageByJobPostId(jobPostId, {mode: 'edit#key-information'});
    verifySchemeSectionNotExists();
  });

  it('should not show scheme section in Edit Step 5', () => {
    onClickNext();
    onClickNext();
    onClickNext();
    verifyGovernmentSupportScheme('No schemes selected');
  });

  it('should be able to submit successfully', () => {
    onClickSubmitJobPost();
    selectYesOrNoInEditJobModal('Yes');
    verifyFieldsInAcknowledgementPage(jobPostDetails, {isCreate: false});
  });

  it('should still have no schemes attached after successful submission', () => {
    cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
    verifyGovernmentSupportScheme('No schemes selected');
  });
});
