import {suppressBeforeUnloadEvent, onClickNext} from '../../../support/shared/common.functions';
import {inputJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectXSkills} from '../../../support/shared/job_post_skills_page.functions';
import {
  inputKeyInformation,
  verifySGUnitedSchemeDetails,
  verifySGUnitedSchemeNotExists,
  selectSGUnitedScheme,
  unselectSGUnitedScheme,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  verifyGovernmentSupportScheme,
  clickSubmitJobPost,
} from '../../../support/shared/job_post_preview_page.functions';
import {getJobPostId} from '../../../support/shared/job_post_success_page.functions';
import {
  searchAndOpenJobOnTab,
  mouseoverOnManageMenuButton,
  clickViewEditJobPostButton,
  clickEditJobPost,
  selectYesOrNoInEditJobModal,
} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY, COMPANY_SCHEMES_ALIAS} from '../../../support/commands/common.commands';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {seedXNewJobsForUEN, seedXCloseJobsForUEN} from '../../../support/manageData/job.seed';
import {repostJob} from '../../../support/shared/job_repost_page_functions';
const moment = require('moment');

describe('SGUnited schemes end to end flow', () => {
  const UEN = '200610233W';
  let jobPostID;
  let validSchemeJobPostId;
  let closeJobPostId;
  const sgUnitedSchemes = [
    {schemeName: 'SGUnited Traineeships', schemeId: 5},
    {schemeName: 'SGUnited Mid-Career Pathways Programme', schemeId: 6},
  ];

  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  sgUnitedSchemes.forEach(({schemeName, schemeId}) => {
    describe(`validate ${schemeName} scheme`, () => {
      describe('should not show scheme in step 3 job creation, when scheme not added for the employer', () => {
        it('Remove scheme, if already exists', () => {
          cy.task('jobDB', `DELETE FROM company_scheme where uen = '${UEN}' and scheme_id = '${schemeId}'`);
        });

        it('create a job post and in step 3 scheme should not display', () => {
          cy.clickNewJobPosting();

          inputJobDescriptionStep({}, {paste: true});
          onClickNext();

          searchAndSelectXSkills('testing', 10);
          onClickNext();

          verifySGUnitedSchemeNotExists(schemeId);
        });
      });

      describe('should show scheme in step 3 job add/edit, when employer have valid scheme added and should not show in view page after unselecting in edit', () => {
        beforeEach(() => {
          cy.captureCompanySchemes();
        });

        it('add scheme for the employer via DB with valid expiry date', () => {
          cy.task('jobDB', `DELETE FROM company_scheme where uen = '${UEN}' and scheme_id = '${schemeId}'`).task(
            'jobDB',
            `INSERT INTO company_scheme
              (uen, scheme_id, sub_scheme_id, start_date, expiry_date)
              values ('${UEN}', '${schemeId}', 0, null, '${moment().add(2, 'days').format('YYYY-MM-DD')}');`,
          );
        });

        it('should display scheme in step 3 post job with correct details and also in preview page', () => {
          cy.clickNewJobPosting();

          inputJobDescriptionStep({}, {paste: true});
          onClickNext();

          searchAndSelectXSkills('testing', 10);
          onClickNext();

          inputKeyInformation();
          cy.wait(`@${COMPANY_SCHEMES_ALIAS}`);
          verifySGUnitedSchemeDetails(schemeName, schemeId);
          selectSGUnitedScheme(schemeId);
          onClickNext();

          onClickNext();
          onClickNext();
          verifyGovernmentSupportScheme(schemeName);

          clickSubmitJobPost();
          getJobPostId().then((id) => {
            jobPostID = id;
          });
        });

        it('should display scheme in view job page', () => {
          searchAndOpenJobOnTab('Open', jobPostID);
          mouseoverOnManageMenuButton();
          clickViewEditJobPostButton();
          verifyGovernmentSupportScheme(schemeName);
        });

        it('should display scheme in edit job page and should not show in preview after removing in step 3', () => {
          clickEditJobPost();
          onClickNext();
          onClickNext();
          cy.wait(`@${COMPANY_SCHEMES_ALIAS}`);
          unselectSGUnitedScheme(schemeId);
          onClickNext();

          onClickNext();
          onClickNext();
          verifyGovernmentSupportScheme(schemeName, 'not.contain');
          clickSubmitJobPost();
          selectYesOrNoInEditJobModal('Yes');
        });

        it('should not display scheme in view job page after edit', () => {
          searchAndOpenJobOnTab('Open', jobPostID);
          mouseoverOnManageMenuButton();
          clickViewEditJobPostButton();
          verifyGovernmentSupportScheme(schemeName, 'not.contain');
        });

        it('should able to add back scheme in edit page and should display in preview page', () => {
          clickEditJobPost();
          onClickNext();
          onClickNext();
          cy.wait(`@${COMPANY_SCHEMES_ALIAS}`);
          selectSGUnitedScheme(schemeId);
          onClickNext();

          onClickNext();
          onClickNext();
          verifyGovernmentSupportScheme(schemeName);
          clickSubmitJobPost();
          selectYesOrNoInEditJobModal('Yes');
        });

        it('should display scheme in view job page after edit', () => {
          searchAndOpenJobOnTab('Open', jobPostID);
          mouseoverOnManageMenuButton();
          clickViewEditJobPostButton();
          verifyGovernmentSupportScheme(schemeName);
        });
      });

      describe('should not show scheme in step 3 job add/edit, after scheme expired', () => {
        it('create a job with scheme, when scheme is valid to valid edit job with expired scheme', () => {
          cy.task('jobDB', `DELETE FROM company_scheme where uen = '${UEN}' and scheme_id = '${schemeId}'`)
            .task(
              'jobDB',
              `INSERT INTO company_scheme
                  (uen, scheme_id, sub_scheme_id, start_date, expiry_date)
                  values ('${UEN}', '${schemeId}', 0, null, '${moment().add(2, 'days').format('YYYY-MM-DD')}');`,
            )
            .then(() => {
              const payLoad = apiJobData.defaultJobPost;
              payLoad.postedCompany.uen = UEN;
              payLoad.schemes = [{id: schemeId}];
              seedXNewJobsForUEN(1, payLoad, UEN).then((response) => {
                validSchemeJobPostId = response[0].metadata.jobPostId;
              });
            });
        });

        it('Update scheme expiry date to past', () => {
          cy.task(
            'jobDB',
            `UPDATE company_scheme SET expiry_date = '${moment()
              .subtract(2, 'days')
              .format('YYYY-MM-DD')}'  where uen = '${UEN}' and scheme_id = '${schemeId}'`,
          );
        });

        it('should show scheme detail in job view page', () => {
          searchAndOpenJobOnTab('Open', validSchemeJobPostId);
          mouseoverOnManageMenuButton();
          clickViewEditJobPostButton();
          verifyGovernmentSupportScheme(schemeName);
        });

        it('should not show scheme detail in step 3 job edit page', () => {
          clickEditJobPost();
          onClickNext();
          onClickNext();
          verifySGUnitedSchemeNotExists(schemeId);
        });

        it('should not show scheme detail in step 3 job post page', () => {
          cy.clickNewJobPosting();

          inputJobDescriptionStep({}, {paste: true});
          onClickNext();

          searchAndSelectXSkills('testing', 10);
          onClickNext();

          verifySGUnitedSchemeNotExists(schemeId);
        });
      });

      describe('should show for closed job', () => {
        it('add scheme for the employer via DB with valid expiry date and post close job with scheme', () => {
          cy.task('jobDB', `DELETE FROM company_scheme where uen = '${UEN}' and scheme_id = '${schemeId}'`)
            .task(
              'jobDB',
              `INSERT INTO company_scheme
                      (uen, scheme_id, sub_scheme_id, start_date, expiry_date)
                      values ('${UEN}', '${schemeId}', 0, null, '${moment().add(2, 'days').format('YYYY-MM-DD')}');`,
            )
            .then(() => {
              const payLoad = apiJobData.defaultJobPost;
              payLoad.postedCompany.uen = UEN;
              payLoad.schemes = [{id: schemeId}];
              seedXCloseJobsForUEN(1, payLoad, UEN).then((response) => {
                closeJobPostId = response[0].metadata.jobPostId;
              });
            });
        });

        it('should show scheme in close job', () => {
          searchAndOpenJobOnTab('Close', closeJobPostId);
          repostJob.clickViewRepostJobPost();
          verifyGovernmentSupportScheme(schemeName);
        });
      });
    });
  });
});
