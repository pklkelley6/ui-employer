import {mcf} from '@mcf/constants';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {onClickNext} from '../../../support/shared/common.functions';
import {inputJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectXSkills} from '../../../support/shared/job_post_skills_page.functions';
import {
  inputKeyInformation,
  verifySGUnitedSchemeNotExists,
  verifySchemeSectionNotExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {
  clickSubmitJobPost,
  verifyGovernmentSupportScheme,
} from '../../../support/shared/job_post_preview_page.functions';
import {getJobPostId} from '../../../support/shared/job_post_success_page.functions';
import {removeSeededJobInDB} from '../../../support/manageData/job.seed.js';
import {userInfos} from '../../../support/users';

describe('Verify Career Trial scheme hide flow', () => {
  let jobPostId;
  const careerTrialSchemeId = mcf.SCHEME_ID.CAREER_TRIAL;

  const loginCompany = 'Employer for Career trial';
  const UEN = Cypress._.find(userInfos, ['companyName', `${loginCompany}`]).entityId;

  before(() => {
    cy.home();
    cy.login(`${loginCompany}`, 'Job Admin');

    cy.log('Seed data - remove all existing scheme and tag Career trial only')
      .task('jobDB', `DELETE FROM company_scheme where UEN = '${UEN}';`)
      .task('jobDB', `INSERT INTO company_scheme (UEN, SCHEME_ID) VALUES ('${UEN}', '${careerTrialSchemeId}');`);
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
    removeSeededJobInDB(jobPostId);
  });

  describe('Job Post Create - Career trial should not show in while create but should show in view after tag', () => {
    it('should not show Career trial scheme in Key Information - Step 3', () => {
      // navigate to step 3
      cy.clickNewJobPosting();

      inputJobDescriptionStep({}, {paste: true});
      onClickNext();

      searchAndSelectXSkills('testing', 10);
      onClickNext();

      inputKeyInformation();
      verifySchemeSectionNotExists();
      verifySGUnitedSchemeNotExists(careerTrialSchemeId);
    });

    it('should not show Career trial scheme in Preview and should not show in View page after Submit', () => {
      // Navigate to Step 5 and Submit the application
      onClickNext();
      onClickNext();
      onClickNext();
      verifyGovernmentSupportScheme('No schemes selected');

      // Submit the application and validate scheme in review
      clickSubmitJobPost();
      getJobPostId().then((id) => {
        jobPostId = id;
        cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
        verifyGovernmentSupportScheme('No schemes selected');
      });
    });

    it('should show Career trial scheme in View page after the scheme is tagged for the job', () => {
      cy.log('seed data - tag Career trial scheme for the job in DB')
        .task(
          'jobDB',
          `INSERT INTO job_scheme (job_post_id, scheme_id) VALUES ('${jobPostId}', '${careerTrialSchemeId}');`,
        )
        .then(() => {
          cy.reload();
          verifyGovernmentSupportScheme('Career Trial');
        });
    });
  });

  describe('Job Post Edit - Career trial should not show in Edit page but should show in Preview and View page', () => {
    it('should not show Career trial scheme in Edit - Key Information - Step 3', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'edit'});
      onClickNext();
      onClickNext();
      verifySchemeSectionNotExists();
      verifySGUnitedSchemeNotExists(careerTrialSchemeId);
    });

    it('should show Career trial scheme in Preview and in View page after Submit', () => {
      // Validate in Preview page
      onClickNext();
      onClickNext();
      onClickNext();
      verifyGovernmentSupportScheme('Career Trial');

      // Validate in View page
      clickSubmitJobPost();
      cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
      verifyGovernmentSupportScheme('Career Trial');
    });
  });
});
