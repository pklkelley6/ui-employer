import {
  fillJobDescriptionStep,
  selectThirdPartyEmployerOption,
  navigateJobDescriptionStep,
  unSelectThirdPartyEmployerOption,
  verifyUENSelectionIsValid,
} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {
  verifyScheme,
  selectPCPScheme,
  verifyPCPSchemeValidations,
  verifySchemeNotExists,
} from '../../../support/shared/job_post_key_information_page.functions';
import {onClickNext, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Scheme validation checking', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Scheme Account', 'Job Admin');
    cy.fixture('JobPosting.json')
      .as('jobPost')
      .then((jobPost) => {
        cy.clickNewJobPosting();
        cy.shouldHaveFormLoaderLoaded();
        fillJobDescriptionStep(jobPost.jobPost1);
        onClickNext();
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillNotFromDropDown);
        searchAndSelectAddMoreSkillSection(jobPost.jobPost1.skillFromDropDown);
        onClickNext();
      });
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should verify schemes based on login uen and nric', function () {
    verifyScheme('PCP');
    verifyScheme('PMax');
    selectPCPScheme();
    onClickNext();
    verifyPCPSchemeValidations();
  });
  it('should verify schemes based on select posting on behalf of another company', function () {
    navigateJobDescriptionStep();
    selectThirdPartyEmployerOption();
    verifyUENSelectionIsValid('CYPRESS02', 'CYPRESS02');
    onClickNext();
    onClickNext();
    verifySchemeNotExists('PMax');
    verifySchemeNotExists('PCP');
  });
  it('should uncheck on select posting on behalf of another company and verify schemes', function () {
    navigateJobDescriptionStep();
    unSelectThirdPartyEmployerOption();
    onClickNext();
    onClickNext();
    verifyScheme('PCP');
    verifyScheme('PMax');
  });
});
