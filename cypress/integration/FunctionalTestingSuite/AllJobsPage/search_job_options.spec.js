/// <reference types="Cypress" />

import {validateJobListExistence, validateJobSearchResultMessage} from '../../../support/shared/job_page.functions';
import {getPagination, isCurrentActive} from '../../../support/shared/common.functions';
import {apiJobs} from '../../../support/api/jobsApi';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Jobs Page - Search Jobs', () => {
  let searchJobUuid, searchJobPostId;
  const UEN = '100000003C';
  const payLoad = apiJobData.defaultJobPost;
  payLoad.postedCompany.uen = UEN;
  payLoad.title = 'For Open Job Search 404';
  const searchTestData = [
    {
      testDescription: 'Search for jobs with "open job search" has results',
      searchInput: 'open job search',
      searchResults: 'found',
      searchMessage: 'found',
    },
    {
      testDescription: 'Search for jobs with keywords+space has results',
      searchInput: '   open job search   ',
      searchResults: 'found',
      searchMessage: 'found',
    },
    {
      testDescription: 'Search for specific job should have result',
      searchInput: 'For Open Job Search 404',
      searchResults: 'found',
      searchMessage: 'found',
    },
    {
      testDescription: 'Search for jobs with no input has all results',
      searchInput: '',
      searchResults: 'found',
      searchMessage: 'noMessage',
    },
    {
      testDescription: 'Search for jobs with space only has all results',
      searchInput: '      ',
      searchResults: 'found',
      searchMessage: 'noMessage',
    },
    {
      testDescription: 'Search for jobs with "sdfjalksjfaklsdfasfasdfaf" has 0 results',
      searchInput: 'dxfcgvhbjnkjhgf',
      searchResults: 'notFound',
      searchMessage: 'notFound',
    },
    {
      testDescription: 'Search for jobs with jobs ID',
      searchInput: 'dummy',
      searchResults: 'found',
      searchMessage: 'found',
    },
  ];
  before(() => {
    cy.home().login('Account with a lot of jobs', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Should be able to search for job/jobs with different terms', () => {
    before(() => {
      cy.log('Seed data - Post Jobs').then(() => {
        apiJobs
          .createJob(payLoad)
          .then((response) => {
            searchJobUuid = response.uuid;
            searchJobPostId = response.metadata.jobPostId;
          })
          .then(() => {
            // Update testData with JobID
            searchTestData[
              Cypress._.findIndex(searchTestData, {testDescription: 'Search for jobs with jobs ID'})
            ].searchInput = searchJobPostId;
          });
      });
    });
    searchTestData.forEach((data) => {
      it(`${data.testDescription}`, () => {
        cy.searchForJobs(data.searchInput);
        validateJobListExistence(data.searchResults);
        validateJobSearchResultMessage(data.searchMessage);
      });
    });
  });

  describe('Should be able to search for job/jobs with different terms for closed jobs', () => {
    before(() => {
      cy.log('Seed data - Close Jobs').then(() => {
        apiJobs.closeJob(searchJobUuid);
      });
    });

    searchTestData.forEach((data) => {
      it(`${data.testDescription}`, () => {
        cy.clickOnJobTab('Closed').searchForJobs(data.searchInput);
        validateJobListExistence(data.searchResults);
        validateJobSearchResultMessage(data.searchMessage);
      });
    });
  });

  describe('Should be showing pagination for more than 20 jobs under Open & Closed jobs', () => {
    const tabs = ['Open', 'Close'];
    const searchInput = 'Job Title';
    tabs.forEach((tab) => {
      it(`${tab} - Search for jobs with results with more than 1 page`, () => {
        cy.clickOnJobTab(tab).searchForJobs(searchInput);
        validateJobListExistence('found');
        validateJobSearchResultMessage('found');
        getPagination(0).should('have.text', '1').then(isCurrentActive);

        //click on page 2
        cy.clickOnPaginationIndex(1);
        validateJobListExistence('found');
        validateJobSearchResultMessage('found');
        getPagination(2).should('have.text', '2').then(isCurrentActive);
      });
    });
  });

  describe('Should not be showing closed job in Open tab and open job in Closed tab', () => {
    let jobUuid = '';
    let JobPostID = '';
    const {searchResults, searchMessage} = 'notFound';
    it('Creating a Job, should not be showing under Closed tab', () => {
      apiJobs
        .createJob(payLoad)
        .then((response) => {
          jobUuid = response.uuid;
          JobPostID = response.metadata.jobPostId;
        })
        .then(() => {
          cy.clickOnJobTab('Closed').searchForJobs(JobPostID);
          validateJobListExistence(searchResults);
          validateJobSearchResultMessage(searchMessage);
        });
    });
    it('Closing a Job, should not be showing under Open tab', () => {
      apiJobs.closeJob(jobUuid).then(() => {
        cy.clickOnJobTab('Open').searchForJobs(JobPostID);
        validateJobListExistence(searchResults);
        validateJobSearchResultMessage(searchMessage);
      });
    });
  });
});
