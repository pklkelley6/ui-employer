import moment from 'moment';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';
import {seedXNewJobsForUEN, removeJobsByPostedUen} from '../../../support/manageData/job.seed';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';

describe('Jobs Page - Sorting logic check', function () {
  const sortingOptionsOpenJobs = ['Auto-close Date', 'Posted Date'];
  const sortingOptionsClosedJobs = ['Closed Date', 'Posted Date'];
  const loginEmployer = 'Employer for jobs sorting';
  const employerInfo = Cypress._.find(userInfos, ['companyName', loginEmployer]);
  const seedJobExpiryDays = [7, 21, 30];
  const seededJobsResponse = [];

  before(() => {
    cy.home()
      .login(loginEmployer)
      .then(() => {
        removeJobsByPostedUen(employerInfo.entityId);
      })
      .then(() => {
        cy.wrap(seedJobExpiryDays)
          .each((expiryDays) => {
            const payload = {
              ...apiJobData.defaultJobPost,
              postedCompany: {uen: employerInfo.entityId},
              jobPostDuration: expiryDays,
            };
            seedXNewJobsForUEN(1, payload).then((response) => {
              seededJobsResponse.push(response[0].metadata.jobPostId);
            });
          })
          .home();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('validate sorting order for open jobs', () => {
    it('should show default sort as Auto-close Date, jobs should display in closing date order and should have correct sorting order', () => {
      cy.clickOnJobTab('Open').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.checkHasSortBySelected('Auto-close Date');
      JobsPageNavigate.verifyJobListingOrderIn(seededJobsResponse);
      JobsPageNavigate.clickAndCheckDefaultSortOptions(sortingOptionsOpenJobs);
    });

    it('should able to sort by posted date and should list jobs in posted date order - latest first', () => {
      cy.clickOnSortJobsBy('Posted Date').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.verifyJobListingOrderIn([...seededJobsResponse].reverse());
    });

    it('should able to sort by auto-close date and should list jobs in closing date order - latest first', () => {
      cy.clickOnSortJobsBy('Auto-close Date').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.verifyJobListingOrderIn(seededJobsResponse);
    });
  });

  describe('validate sorting order for closed jobs', () => {
    before(() => {
      cy.log('seed data - close jobs via DB');
      cy.wrap(seededJobsResponse)
        .each((jobPostId, index) => {
          const expiryDate = moment().subtract(index, 'days').format('YYYY-MM-DD');
          cy.task(
            'jobDB',
            `UPDATE jobs SET expiry_date = '${expiryDate}', deleted_at = '${expiryDate}', job_status_id = '9' WHERE job_post_id = '${jobPostId}';`,
          );
        })
        .home();
    });

    it('should show default sort as Closed Date Date, jobs should display in closed order and should have correct sorting order', () => {
      cy.clickOnJobTab('Closed').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.checkHasSortBySelected('Closed Date');
      JobsPageNavigate.verifyJobListingOrderIn(seededJobsResponse);
      JobsPageNavigate.clickAndCheckDefaultSortOptions(sortingOptionsClosedJobs);
    });

    it('should able to sort by posted date and should list jobs in posted date order - latest first', () => {
      cy.clickOnSortJobsBy('Posted Date').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.verifyJobListingOrderIn([...seededJobsResponse].reverse());
    });

    it('should able to sort by auto-close date and should list jobs in closing order - latest first', () => {
      cy.clickOnSortJobsBy('Closed Date').shouldHaveCardLoaderLoaded();
      JobsPageNavigate.verifyJobListingOrderIn(seededJobsResponse);
    });
  });
});
