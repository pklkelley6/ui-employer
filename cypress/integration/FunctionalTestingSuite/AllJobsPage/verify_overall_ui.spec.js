import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import jobs from '../../../fixtures/JobList';
import {verifyFooterLinks} from '../../../support/shared/footer.functions';
import {getLocalMoment} from '../../../fixtures/JobList';
import {formatDate} from '../../../support/util/date';

describe('Jobs Page - Overall UI', function () {
  const updateJobDetails = [
    {
      jobPostId: 'MCF-2018-9111113',
      newPostingDate: formatDate(getLocalMoment()),
      expiryDate: formatDate(getLocalMoment().add(12, 'days')),
    },
    {
      jobPostId: 'MCF-2018-9111111',
      newPostingDate: formatDate(getLocalMoment().add(3, 'months').subtract(7, 'days')),
      expiryDate: formatDate(getLocalMoment().add(3, 'months').add(1, 'day')),
    },
    {
      jobPostId: 'MCF-2017-9111112',
      newPostingDate: formatDate(getLocalMoment().add(3, 'years').subtract(7, 'days')),
      expiryDate: formatDate(getLocalMoment().add(3, 'years')),
    },
  ];

  before(() => {
    cy.home();
    cy.login('Manage Applicant Test Account', 'Job Admin');
    cy.log('seed data - update job details'); // jobs are created in api-jobs
    cy.wrap(updateJobDetails)
      .each(({jobPostId, newPostingDate, expiryDate}) => {
        cy.task(
          'jobDB',
          `UPDATE jobs SET new_posting_date = '${newPostingDate}', original_posting_date = '${newPostingDate}', expiry_date = '${expiryDate}' WHERE job_post_id = '${jobPostId}';`,
        );
      })
      .home();
  });

  after(() => {
    cy.logout();
  });

  it('Should have all UI, context showing correctly in job list page and footer', function () {
    verifyFooterLinks();
    JobsPageNavigate.cardLoaderShouldNotExist();
    JobsPageNavigate.checkHasUserName('Republic Poly Lecturer');
    // JobsPageNavigate.checkHasCompanyName('CYPRESS00'); //CYPRESS01 TEST
    // Need to add check for company logo
    JobsPageNavigate.checkHasSideBarAllJobs();
    JobsPageNavigate.checkHasSortByOption();
    JobsPageNavigate.checkHasSortBySelected('Auto-close Date');
    JobsPageNavigate.checkHasSearchBarAndSearchButton();
    JobsPageNavigate.validateJobListExistence('found'); //default in open tab
    JobsPageNavigate.checkHasOpenTabAndCountSameAsJobListing();
    JobsPageNavigate.verifyJobListField(jobs.J1, 0);
    JobsPageNavigate.verifyJobListField(jobs.J2, 1);
    JobsPageNavigate.verifyJobListField(jobs.J3, 2);
  });
});
