import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {getPagination, isCurrentActive} from '../../../support/shared/common.functions';

function checkPaginationFuc() {
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '1').then(isCurrentActive);
  getPagination(1).should('have.text', '2');
  getPagination(2).should('have.text', '3');
  getPagination(3).should('have.text', '4');
  getPagination(4).should('have.text', '5');
  getPagination(5).should('have.text', '❯');
  cy.clickOnPaginationIndex(1); //click on page 2
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2').then(isCurrentActive);
  getPagination(3).should('have.text', '3');
  getPagination(4).should('have.text', '4');
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '❯');
  cy.clickOnPaginationIndex(7); //click on page 3 via > botton
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2');
  getPagination(3).should('have.text', '3').then(isCurrentActive);
  getPagination(4).should('have.text', '4');
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '7');
  getPagination(8).should('have.text', '❯');
  cy.clickOnPaginationIndex(0); //click on < button should go back to previous page
  JobsPageNavigate.cardLoaderShouldNotExist();
  JobsPageNavigate.validateJobListExistence('found');
  getPagination(0).should('have.text', '❮');
  getPagination(1).should('have.text', '1');
  getPagination(2).should('have.text', '2').then(isCurrentActive);
  getPagination(3).should('have.text', '3');
  getPagination(4).should('have.text', '4');
  getPagination(5).should('have.text', '5');
  getPagination(6).should('have.text', '6');
  getPagination(7).should('have.text', '❯');
}

describe('Jobs Page - Pagination', function () {
  beforeEach(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
  });
  afterEach(() => {
    cy.logout();
  });

  it('Should have pagination in open jobs tab', function () {
    checkPaginationFuc();
  });

  it('Should have pagination in closed jobs tab', function () {
    cy.clickOnJobTab('Closed');
    checkPaginationFuc();
  });
});
