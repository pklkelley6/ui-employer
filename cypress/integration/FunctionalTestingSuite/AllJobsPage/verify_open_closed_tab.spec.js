import * as JobsPageNavigate from '../../../support/shared/job_page.functions';

describe('Jobs Page - Test Open/Closed Job tabs', function () {
  beforeEach(() => {
    cy.home();
  });
  afterEach(() => {
    cy.logout();
  });

  it('Should have correct amount of jobs in the open and closed job tab both >0', () => {
    cy.login('Manage Applicant Test Account', 'Job Admin');

    JobsPageNavigate.checkHasOpenTabAndCountSameAsJobListing();
    cy.clickOnJobTab('Closed');
    JobsPageNavigate.checkHasCloseTabAndCountSameAsJobListing();
  });

  it('Should have correct display when no jobs in open and closed tab', () => {
    cy.login('Empty Job Test Account', 'Job Admin');

    JobsPageNavigate.checkHasOpenTab(0);
    JobsPageNavigate.checkHasClosedTab(0);
    JobsPageNavigate.check0JobList();
    cy.clickOnJobTab('Closed');
    JobsPageNavigate.check0JobList();
  });
});
