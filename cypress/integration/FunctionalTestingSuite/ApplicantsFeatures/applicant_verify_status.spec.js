import moment from 'moment';
import * as ApplicantNavigate from '../../../support/shared/manage_applicants_page.functions';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';

const UEN = '100000008C';
const jobPosts = ['Open', 'Closed'];
const applicants = [
  {nric: 'S1643276H', bookmarkIndex: 0},
  {nric: 'S5704830A', bookmarkIndex: 1},
  {nric: 'S7566272F', bookmarkIndex: 2},
  {nric: 'S1210455C', bookmarkIndex: 3},
];
jobPosts.forEach((tab) => {
  describe(`Manage Applicants Status - ${tab} job`, function () {
    let jobPostId, jobUuid;
    before(() => {
      cy.home();
      cy.login('Manage Applicant Status', 'Job Admin');
      cy.log('seed data - create a job and apply application via couchDB with bookmarkedOn');
      seedXNewJobsForUEN(1, null, UEN).then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        applicants.forEach(({nric, bookmarkIndex}) => {
          cy.task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${nric}';`)
            .then((results) => {
              const mockApplicationData = couchDBMockData.defaultApplicantData(jobUuid);
              mockApplicationData.applicant.id = results[0].individual_id;
              couchDBApi.createDocument({
                ...mockApplicationData,
                bookmarkedOn: moment().subtract(bookmarkIndex, 'days').format('YYYY-MM-DDTHH:mm:ss'),
              });
            })
            .then(() => {
              if (tab === 'Closed')
                cy.task('jobDB', `update jobs set job_status_id = 9 where job_post_id = '${jobPostId}'`);
            });
        });
      });
      cy.visit('/');
    });

    beforeEach(() => {
      cy.home();
      openJobpost(tab, jobPostId);
    });

    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
    });

    after(() => {
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
      removeSeededJobInDB(jobPostId);
    });

    it('UI checking - verify list of applicants status', function () {
      cy.clickOnCandidate(0);
      ApplicantNavigate.verifyApplicantStatusList();
    });

    it('UI checking - update job applicants status as "To interview" and re-login page and verify its updated right and left panel', function () {
      cy.clickOnCandidate(0);
      applicantStatus.selectApplicantStatus('To Interview');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(0);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('To Interview');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('To Interview', 0);
    });

    it('UI checking - update job applicants status as "Unsuccessful" and re-login page and verify its updated right and left panel', function () {
      cy.clickOnCandidate(1);
      applicantStatus.selectApplicantStatus('Unsuccessful');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(1);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Unsuccessful');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('Unsuccessful', 1);
    });

    it('UI checking - update job applicants status as "Hired" and re-login page and verify its updated right and left panel', function () {
      cy.clickOnCandidate(2);
      applicantStatus.selectApplicantStatus('Hired');
      cy.reLogin('Manage Applicant Status', 'Job Admin');
      openJobpost(tab, jobPostId);
      cy.clickOnCandidate(2);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Hired');
      applicantStatus.verifyApplicantStatusLeftPanelUpdated('Hired', 2);
    });

    it('UI checking - job applicants status right and left label as empty when applicant status are not updated', function () {
      cy.clickOnCandidate(3);
      applicantStatus.verifyApplicantStatusNotExist(3);
      applicantStatus.verifyApplicantStatusRightPaneUpdated('Not updated');
    });
  });
});

const openJobpost = (tabJobPost, jobPostId) => {
  searchAndOpenJobOnTab(tabJobPost, jobPostId);
  cy.skipApplicantsOnboardingIfExist();
  shouldHaveCardLoaderLoaded();
};
