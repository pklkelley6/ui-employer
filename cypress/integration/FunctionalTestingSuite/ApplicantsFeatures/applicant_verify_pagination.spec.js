import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import {removeSeededJobInDB} from '../../../support/manageData/job.seed.js';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';

describe('Applicant - Pagination Test', function () {
  const UEN = '100000002C';
  let jobPostID, uuid;

  before(() => {
    cy.home();
    cy.login('Test may update data', 'Job Admin');
    cy.log('seed data - create a job and applicants apply via couchDB');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      cy.log(response);
      jobPostID = response[0].metadata.jobPostId;
      uuid = response[0].uuid;
      couchDBApi.seedBulkApplicants(126, uuid);
    });
    cy.home();
  });

  after(() => {
    removeSeededJobInDB(jobPostID);
  });

  it('Should have pagination in applicant page', function () {
    cy.clickJobByJobPostID(jobPostID);
    cy.skipApplicantsOnboarding();

    cy.log('1st page should have 3 pages follow by a > button');
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(4);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '1', 1);
    ManageJobsNavigate.shouldHavePageProperty(1, '2', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '3', 0);
    ManageJobsNavigate.shouldHavePageProperty(3, '❯', 0);

    cy.log('Click on the page 2');
    cy.clickOnPaginationIndex(1);
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(6);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '❮', 0);
    ManageJobsNavigate.shouldHavePageProperty(1, '1', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '2', 1);
    ManageJobsNavigate.shouldHavePageProperty(3, '3', 0);
    ManageJobsNavigate.shouldHavePageProperty(4, '4', 0);
    ManageJobsNavigate.shouldHavePageProperty(5, '❯', 0);

    cy.log('Click on the page 4');
    cy.clickOnPaginationIndex(4);
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(7);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '❮', 0);
    ManageJobsNavigate.shouldHavePageProperty(1, '2', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '3', 0);
    ManageJobsNavigate.shouldHavePageProperty(3, '4', 1);
    ManageJobsNavigate.shouldHavePageProperty(4, '5', 0);
    ManageJobsNavigate.shouldHavePageProperty(5, '6', 0);
    ManageJobsNavigate.shouldHavePageProperty(6, '❯', 0);

    cy.log('Click on >');
    cy.clickOnPaginationIndex(6);
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(7);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '❮', 0);
    ManageJobsNavigate.shouldHavePageProperty(1, '3', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '4', 0);
    ManageJobsNavigate.shouldHavePageProperty(3, '5', 1);
    ManageJobsNavigate.shouldHavePageProperty(4, '6', 0);
    ManageJobsNavigate.shouldHavePageProperty(5, '7', 0);
    ManageJobsNavigate.shouldHavePageProperty(6, '❯', 0);

    cy.log('Click on page 7');
    cy.clickOnPaginationIndex(5);
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(6);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(4);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '❮', 0);
    ManageJobsNavigate.shouldHavePageProperty(1, '5', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '6', 0);
    ManageJobsNavigate.shouldHavePageProperty(3, '7', 1);

    cy.log('Click on <');
    cy.clickOnPaginationIndex(0);
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.get('[data-cy="pagination-number"]').should(($pages) => {
      expect($pages).to.have.length(6);
    });
    ManageJobsNavigate.shouldHavePageProperty(0, '❮', 0);
    ManageJobsNavigate.shouldHavePageProperty(1, '4', 0);
    ManageJobsNavigate.shouldHavePageProperty(2, '5', 0);
    ManageJobsNavigate.shouldHavePageProperty(3, '6', 1);
    ManageJobsNavigate.shouldHavePageProperty(4, '7', 0);
    ManageJobsNavigate.shouldHavePageProperty(5, '❯', 0);
  });
});
