import * as JobsNavigate from '../../../support/shared/job_page.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';
import {couchDBApi} from '../../../support/api/couchDBApi.js';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';

describe('Applicant - View/unviewed badging test', function () {
  let jobPostId, uuid;
  const UEN = '100000001C';

  beforeEach(() => {
    cy.home();
    cy.login('Account For WriteDB', 'Job Admin');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      uuid = response[0].uuid;
      jobPostId = response[0].metadata.jobPostId;
      couchDBApi.seedBulkApplicants(5, uuid);
    });
    cy.captureGraphqlRequest().visit('/');
  });
  afterEach(() => {
    cy.logout();
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
  });

  it('Validate the view and unview badging', function () {
    cy.waitGraphqlRequest('getApplicationsCount')
      .getUnviewedBadgeCountByIndex(1)
      .then((count) => {
        cy.clickJobByJobPostID(jobPostId);
        cy.skipApplicantsOnboardingIfExist();
        cy.clickUnviewedCandidateByIndex(0);
        JobsNavigate.shouldHaveCardLoaderLoaded();
        //TODO: data validation
        cy.clickOnAllJobs('left');
        //verify the unview has reduced 1
        JobsNavigate.checkUnviewedBadge(1, count - 1);
        //log out and login again check the unviewed has been remained.
        cy.logout();
        checkLogoutPage();

        cy.home();
        cy.login('Account For WriteDB', 'Job Admin');
        JobsNavigate.shouldHaveApplicantsCountGet();
        JobsNavigate.checkUnviewedBadge(1, count - 1);
      });
  });
});
