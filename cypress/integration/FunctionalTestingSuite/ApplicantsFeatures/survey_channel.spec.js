import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {getApplicationsMock} from '../../../fixtures/mockResponse/applications';
import {getSuggestedTalentMock} from '../../../fixtures/mockResponse/suggestedTalent';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';

function checkSurveyForm() {
  cy.url().should('eq', Cypress.config('baseUrl') + '/survey');
  cy.get('[data-cy="mcf-logo"]').should('have.attr', 'href', '/');
  cy.get('iframe').invoke('attr', 'src').should('include', 'form.gov.sg');
}

// eslint-disable-next-line jest/valid-describe
describe('Survey channel', {retries: 2}, function () {
  let jobPostId, uuid;

  const UEN = '100000002C';
  before(() => {
    cy.home();
    cy.login('Test may update data', 'Job Admin');
    cy.clearLocalStorage('survey-RP192');
    cy.log('Post a Jobs and mock applicant and talent response');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      uuid = response[0].uuid;
      jobPostId = response[0].metadata.jobPostId;
    });
  });

  beforeEach(() => {
    cy.home();
    cy.mockGraphQL('profile', {
      getApplications: getApplicationsMock(uuid, 8),
      getSuggestedTalents: getSuggestedTalentMock(uuid, 8),
    });
    cy.visit('/');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('Should have the survey channel after click on 5 applicants', function () {
    //should not appear before 5th applicants
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.clickJobByJobPostID(jobPostId);
    JobsPageNavigate.cardLoaderShouldNotExist();
    //JobsPageNavigate.shouldHaveCardLoaderLoaded();
    cy.skipApplicantsOnboardingIfExist();

    cy.clickOnCandidate(0);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(1);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(2);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(3);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(4);

    //should appear after click on 5 applicants
    ManageJobsNavigate.isSurveyBarShown(1);

    //click on close the survey link should disappear
    cy.clickCloseButtonOnSurveyBar();
    ManageJobsNavigate.isSurveyBarShown(0);

    //click on 5 applicants again, should appear again
    cy.clickOnCandidate(0);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(1);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(2);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(3);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(4);
    //should appear after click on 5 applicants
    ManageJobsNavigate.isSurveyBarShown(1);

    //click on take survey button should open the survey link
    cy.clickSurveyButtonOnSurveyBar();
    checkSurveyForm(); //should have all elements correct for survey page

    cy.go('back');
    //Click on 5 applicants again, the survey bar should not appear
    JobsPageNavigate.cardLoaderShouldNotExist();
    //JobsPageNavigate.shouldHaveCardLoaderLoaded();
    cy.clickOnCandidate(0);
    cy.clickOnCandidate(1);
    cy.clickOnCandidate(2);
    cy.clickOnCandidate(3);
    cy.clickOnCandidate(4);
    //should appear not after click on 5 applicants
    ManageJobsNavigate.isSurveyBarShown(0);
  });

  it('Should have the survey channel after click on 2 suggested talents', function () {
    //should not appear before 2nd Suggested Talent
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.clickJobByJobPostID(jobPostId);
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboardingIfExist();

    cy.clickOnSuggestedTalentTab();
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.skipApplicantsOnboardingIfExist();
    cy.clickOnCandidate(0);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(1);
    //should appear after click on 2nd suggested talent
    ManageJobsNavigate.isSurveyBarShown(1);

    //click on close the survey link should disappear
    cy.clickCloseButtonOnSurveyBar();
    ManageJobsNavigate.isSurveyBarShown(0);

    //click on 2 suggested talent again, should appear again
    cy.clickOnCandidate(0);
    ManageJobsNavigate.isSurveyBarShown(0);
    cy.clickOnCandidate(1);
    //should appear after click on 2nd suggested talent
    ManageJobsNavigate.isSurveyBarShown(1);

    //click on take survey button should open the survey link
    cy.clickSurveyButtonOnSurveyBar();
    checkSurveyForm(); //should have all elements correct for survey page

    cy.go('back');
    JobsPageNavigate.cardLoaderShouldNotExist();
    cy.clickOnCandidate(2);
    //cy.clickOnCandidate(3); commented this one since value are not comes from wcc
    //should not appear after click on 2nd suggested talent
    ManageJobsNavigate.isSurveyBarShown(0);
  });
});
