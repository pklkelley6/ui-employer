import moment from 'moment';
import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import {cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {userInfos} from '../../../support/users';

const loginEmployer = 'Employer for applicants sorting';
const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;

describe('Applicant - Sort applicants test', function () {
  let jobPostId, jobUuid;
  const applicantsSeedingScore = [
    {score: 0.8901, isTopMatch: true},
    {score: 0, isTopMatch: false},
    {score: 0.21, isTopMatch: false},
    {score: 0.912, isTopMatch: true},
    {score: 0.6232, isTopMatch: true},
    {score: 0.734, isTopMatch: true},
  ];

  before(() => {
    cy.login('Employer for applicants sorting', 'Job Admin');
    cy.log('seed data - create a job and seed applications via couchDB with different WCC scores');
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        cy.wrap(applicantsSeedingScore).each(({score}, index) => {
          const couchDBApplicantMockData = couchDBMockData.defaultApplicantData(jobUuid);
          couchDBApplicantMockData.scores = {wcc: score};
          couchDBApi.createDocument({
            ...couchDBApplicantMockData,
            createdOn: moment().subtract(index, 'days').format('YYYY-MM-DDTHH:mm:ss'),
          });
          applicantsSeedingScore[index].name = couchDBApplicantMockData.applicant.name;
        });
      })
      .then(() => {
        cy.home().clickJobByJobPostID(jobPostId).skipApplicantsOnboardingIfExist();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Test to sort applicants by Job match/DateApplied', () => {
    it('Sort by job match', function () {
      cy.clickOnSortBy('Job match');
      cardLoaderShouldNotExist();
      cy.wrap([...Cypress._.sortBy(applicantsSeedingScore, ['isTopMatch', 'score'])].reverse()).each(
        ({name, isTopMatch}, index) => {
          // sort applicantsSeedingScore by score and topMatch to validate highest score on asc order
          isTopMatch
            ? ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(index)
            : ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(index);
          ManageJobsNavigate.applicantLeftPanelNameValidation({name: name}, index);
        },
      );
    });

    it('Sort by Date applied', function () {
      cy.clickOnSortBy('Date applied');
      cardLoaderShouldNotExist();
      cy.wrap(applicantsSeedingScore).each(({name, isTopMatch}, index) => {
        // validate applicant by created order
        isTopMatch
          ? ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(index)
          : ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(index);
        ManageJobsNavigate.applicantLeftPanelNameValidation({name: name}, index);
      });
    });
  });
});

describe('To sort applicants by Job match which includes empty scores and pagination', () => {
  let jobPostId, jobUuid;

  before(() => {
    cy.login('Employer for applicants sorting', 'Job Admin');
    cy.log('seed data - create a job and seed applications via couchDB with WCC and empty scores');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;
      cy.wrap(Cypress._.range(0, 20)).each(() => {
        const couchDBApplicantMockData = couchDBMockData.defaultApplicantData(jobUuid);
        couchDBApplicantMockData.scores = {
          wcc: 0.7602,
        }; // update mock data to seed WCC with scores
        couchDBApi.createDocument(couchDBApplicantMockData);
      });
      couchDBApi.seedBulkApplicants(20, jobUuid); // seed WCC with empty scores
    });
    cy.home();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should sort scores by job match and empty scores are attached to end of the descending list', () => {
    JobsPageNavigate.searchAndOpenJobOnTab('Open', jobPostId);
    cy.log('Sort by job match');
    cy.clickOnSortBy('Job match');
    cy.shouldHaveCardLoaderLoaded();
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.wrap(Cypress._.range(0, 20)).each((index) => {
      // check those with scores - via topmatch badge
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(index);
    });
    cy.log('Click on the page 2');
    cy.clickOnPaginationBy(2);
    cy.shouldHaveCardLoaderLoaded();
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.wrap(Cypress._.range(0, 20)).each((index) => {
      // check no topmatch badge since seeded data are empty scores
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(index);
    });
  });

  it('should reset the sorted list back to page 1 when there is a change in sorting type', () => {
    JobsPageNavigate.searchAndOpenJobOnTab('Open', jobPostId);
    cy.log('Sort by job match');
    cy.clickOnSortBy('Job match');
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.log('Click on the page 2');
    cy.clickOnPaginationBy(2);
    cy.shouldHaveCardLoaderLoaded();
    ManageJobsNavigate.shouldHaveNumberOfApplicantsOnEachPage(20);
    cy.log('Sort by Date applied');
    cy.clickOnSortBy('Date applied');
    ManageJobsNavigate.shouldHavePageProperty(0, '1', 1);
  });
});
