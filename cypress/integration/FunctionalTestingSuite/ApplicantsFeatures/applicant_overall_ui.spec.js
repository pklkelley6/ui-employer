import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {seedNewJobInDB, removeSeededJobInDB} from '../../../support/manageData/job.seed.js';

describe('Manage Applicants Page - UI', function () {
  const UEN = '100000003C';
  let jobPostID, jobDetails;
  before(() => {
    cy.log('seed data - create a job and construct job details to validate in UI');
    seedNewJobInDB({job: {posted_uen: UEN}}).then((results) => {
      cy.log(results);
      jobPostID = results.job.job_post_id;
      jobDetails = {
        Title: results.job.job_title,
        Id: results.job.job_post_id,
        PostDate: results.job.new_posting_date,
        ExpiryDate: results.job.expiry_date,
        Applicants: 0,
        Unviewed: 0,
      };
    });
  });

  beforeEach(() => {
    cy.home().login('Account with a lot of jobs', 'Job Admin');
  });

  afterEach(() => {
    cy.logout();
  });

  after(() => {
    removeSeededJobInDB(jobPostID);
  });

  it('UI checking - Manage Applicants View', function () {
    JobsPageNavigate.validateJobListExistence('found');
    cy.openJobPageByJobPostId(jobPostID);
    cy.skipApplicantsOnboarding();
    ManageJobsNavigate.manageApplicantsView(jobDetails);
  });
});
