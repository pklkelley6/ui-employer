import {mcf} from '@mcf/constants';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import * as ManageJobsNavigate from '../../../support/shared/manage_applicants_page.functions';
import * as JobsPageNavigate from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {applicationsFilter} from '../../../support/shared/applications_filter_functions';
import {userInfos} from '../../../support/users';
import {
  applicantStatus,
  shouldHaveTopMatcherForAllApplicants,
  validateScreeningQuestionsTabForApplicantExistance,
  validateScreeningQuestionsAndApplicantResponses,
} from '../../../support/shared/manage_applicants_page.functions';

const loginDetail = 'Account with a lot of jobs';
const UEN = Cypress._.find(userInfos, ['companyName', loginDetail]).entityId;
const yesValue = mcf.SCREEN_QUESTION_RESPONSE.YES;
const noValue = mcf.SCREEN_QUESTION_RESPONSE.NO;

describe('Applicant - Top Matches applicants test', function () {
  let jobPostId, jobUuid;
  before(() => {
    cy.home();
    cy.login(loginDetail, 'Job Admin');
    cy.log('seed data - create a job and seed applications via couchDB with JK, WCC and top match');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;
      cy.wrap(Cypress._.range(0, 5)).each(() => {
        const couchDBApplicantMockData = couchDBMockData.defaultApplicantData(jobUuid);
        couchDBApplicantMockData.scores = {
          wcc: 0.7602,
        }; // update mock data to seed JK, WCC with top match applications
        couchDBApi.createDocument(couchDBApplicantMockData);
      });
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('validate sort applicants by job match/DateApplied and top match toggle is turned on', () => {
    before(() => {
      cy.log('seed data - update applications with WCC and top match');
      cy.home();
      couchDBApi.find({selector: {jobId: jobUuid}}).then((responseBody) => {
        const applicantsDocs = responseBody.docs;
        cy.wrap(applicantsDocs).each((doc) => {
          doc.scores = {
            wcc: 0.7602,
          };
          couchDBApi.update(doc);
        });
      });
    });

    it('filters top matching applicants only when sort applicants by job match/DateApplied and top match toggle is turned on', () => {
      JobsPageNavigate.searchAndOpenJobOnTab('Open', jobPostId);
      cy.log('Sort by job match');
      cy.clickOnSortBy('Job match');
      applicationsFilter.applyTopMatchInFilter();
      JobsPageNavigate.cardLoaderShouldNotExist();
      verifyTopMatcherInApplicationList(5);
      cy.log('Sort by Date applied');
      cy.clickOnSortBy('Date applied');
      applicationsFilter.applyTopMatchInFilter();
      // JobsPageNavigate.shouldHaveCardLoaderLoading();
      JobsPageNavigate.cardLoaderShouldNotExist();
      verifyTopMatcherInApplicationList(5);
    });
    it('should ensure toggle state returns to original state, if changes were made and abandon when cancel button is clicked', () => {
      JobsPageNavigate.searchAndOpenJobOnTab('Open', jobPostId);
      JobsPageNavigate.cardLoaderShouldNotExist();
      //Toggle Top Matches once then Cancel
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.toggleTopMatchesSwitch('check');
      applicationsFilter.clickOnButtonOrLink('Cancel');
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.verifyTopMatchesSwitchIs('be.not.checked');
      applicationsFilter.clickOnButtonOrLink('Cancel');

      //Toggle Top Matches twice then Cancel
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.toggleTopMatchesSwitch('check');
      applicationsFilter.toggleTopMatchesSwitch('uncheck');
      applicationsFilter.clickOnButtonOrLink('Cancel');
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.verifyTopMatchesSwitchIs('be.not.checked');
    });
  });

  describe('validate no top matches', () => {
    before(() => {
      cy.log(`seed data - remove top matches for ${jobPostId} job applications by reducing scores to < 0.6`);
      couchDBApi.find({selector: {jobId: jobUuid}}).then((responseBody) => {
        const applicantsDocs = responseBody.docs;
        cy.wrap(applicantsDocs).each((doc) => {
          doc.scores = {
            wcc: 0.1602,
          };
          couchDBApi.update(doc);
        });
      });
    });

    it('should set top match switch toggle turned off upon opening filter modal', () => {
      cy.home();
      JobsPageNavigate.searchAndOpenJobOnTab('Open', jobPostId);
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.verifyTopMatchesSwitchIs('not.be.checked');
      applicationsFilter.clickOnButtonOrLink('Show Results');
      verifyTopMatcherInApplicationList(0);
    });
  });
});

describe('Verify applicants filter -  no results, backend fail, results count and filter applied numbers', () => {
  let jobPostId, jobUuid;
  before(() => {
    cy.home();
    cy.login(loginDetail, 'Job Admin');
    cy.log('seed data - create a job');
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        couchDBApi.seedBulkApplicants(5, jobUuid);
      })
      .then(() => {
        cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('verify 0 result found, review the filters link and no results count', () => {
    it('should show 0 result found message and should not show results count', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.toggleTopMatchesSwitch();
      applicationsFilter.checkOrUncheckApplicationStatus(['To Interview']);
      applicationsFilter.verifyScreenQuestionSwitchExist('not.exist'); // job screen questions not seeded
      applicationsFilter.clickOnButtonOrLink('Show Results');
      applicationsFilter.verify0ResultFoundAndLink();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('not.exist');
    });

    it('should show application filter popup with preserved selected options on clicking review the filters link', () => {
      applicationsFilter.clickOnButtonOrLink('review the filters');
      applicationsFilter.verifyTopMatchesSwitchIs('to.be.checked');
      applicationsFilter.verifyApplicationStatusesCheckBoxAre(['To Interview'], 'to.be.checked');
    });

    it('should show applicants when filter options are updated and should show results count', () => {
      applicationsFilter.toggleTopMatchesSwitch('Uncheck');
      applicationsFilter.checkOrUncheckApplicationStatus(['Not updated']);
      applicationsFilter.verifyScreenQuestionSwitchExist('not.exist');
      applicationsFilter.clickOnButtonOrLink('Show Results');
      JobsPageNavigate.cardLoaderShouldNotExist();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 5); // count is based on seed data
    });
  });

  describe('verify backend error on applying filter and no results count', () => {
    before(() => {
      cy.mockGraphQL('profile', {getApplications: {response: {}, statusCode: 502}});
    });

    it('should show information is temporarily unavailable', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.toggleTopMatchesSwitch('check');
      applicationsFilter.clickOnButtonOrLink('Show Results');
      applicationsFilter.verifyFilterResultMessageIs([
        'This information is temporarily unavailable.',
        ' Please try again later.',
      ]);
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('not.exist');
    });
  });

  describe('verify filter applied numbers when filter applied and not applied', () => {
    const jobScreenQuestions = {1: 'This is Q1', 2: 'This is Q2', 3: 'This is Q3'};
    before(() => {
      cy.home();
      for (const questionNum in jobScreenQuestions) {
        cy.log('Seed data - Job with screen questions').task(
          'jobDB',
          `INSERT INTO job_screening_questions (job_uuid, sequence, question) VALUES ('${jobUuid}', ${
            questionNum - 1
          }, '${jobScreenQuestions[questionNum]}');`,
        );
      }
      cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
    });
    const testDatas = [
      {
        testDescription:
          'should show all Filters applied when top match, application status and screen question responses toggled and responses are selected',
        selectTopMatches: 'check',
        selectApplicationFilters: ['To Interview', 'Hired'],
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [yesValue, yesValue, yesValue],
        appliedFilterText: {text: '3 Filters applied'},
      },
      {
        testDescription:
          'should show 1 Filter applied when screen question responses toggled and responses are selected',
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [noValue, yesValue, noValue],
        appliedFilterText: {text: '1 Filter applied'},
      },
      {
        testDescription: 'should show 1 Filter applied when Top match toggle only selected',
        selectTopMatches: 'check',
        appliedFilterText: {text: '1 Filter applied'},
      },
      {
        testDescription: 'should show 1 Filter applied when application status only selected',
        selectApplicationFilters: ['To Interview', 'Hired'],
        appliedFilterText: {text: '1 Filter applied'},
      },
      {
        testDescription: 'should show 2 Filter applied when both top match and application status are selected',
        selectTopMatches: 'check',
        selectApplicationFilters: ['To Interview', 'Hired'],
        appliedFilterText: {text: '2 Filters applied'},
      },
      {
        testDescription: 'should not show Filter applied text in filter button when filters are reset',
        appliedFilterText: {text: 'Filter applied', assertion: 'not.contain'},
      },
    ];

    testDatas.forEach(
      ({
        testDescription,
        selectTopMatches,
        selectApplicationFilters,
        appliedFilterText,
        selectScreeningQuestionToggle,
        selectScreeningQuestionResponse,
      }) => {
        it(`${testDescription}`, () => {
          applicationsFilter.clickOnButtonOrLink('Filter');
          applicationsFilter.clickOnButtonOrLink('Reset all filters');
          selectTopMatches && applicationsFilter.toggleTopMatchesSwitch(selectTopMatches);
          selectApplicationFilters && applicationsFilter.checkOrUncheckApplicationStatus(selectApplicationFilters);
          selectScreeningQuestionResponse &&
            applicationsFilter.toggleScreenQuestionSwitch(selectScreeningQuestionToggle);
          selectScreeningQuestionResponse &&
            applicationsFilter.selectScreenQuestionResponse(selectScreeningQuestionResponse);
          applicationsFilter.clickOnButtonOrLink('Show Results');
          applicationsFilter.verifyFiltersButtonAppliedTextIs(appliedFilterText);
        });
      },
    );
  });
});

describe('Verify applicants filter - applications status, results count and combined filter (app status, top matches, screen questions)', () => {
  let jobPostId, jobUuid;
  const jobScreenQuestions = {1: 'This is Q1', 2: 'This is Q2', 3: 'This is Q3'};

  before(() => {
    cy.home();
    cy.login(loginDetail, 'Job Admin');
    cy.log('seed data - create a job and seed applications with different status and top matches via couchDB');

    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;

      cy.home();
      for (const questionNum in jobScreenQuestions) {
        cy.log('Seed data - Job with screen questions').task(
          'jobDB',
          `INSERT INTO job_screening_questions (job_uuid, sequence, question) VALUES ('${jobUuid}', ${
            questionNum - 1
          }, '${jobScreenQuestions[questionNum]}');`,
        );
      }

      const applicationsSeedData = [
        {
          numberOfApplications: 5, //with default status - Received
          jobScreeningQuestionResponses: [yesValue, yesValue, yesValue],
        },
        {
          numberOfApplications: 25,
          statusId: 1,
          statusDescription: 'Under Review', // To Interview in UI
          topMatches: true,
          jobScreeningQuestionResponses: [noValue, noValue, noValue],
        },
        {
          numberOfApplications: 1,
          statusId: 2,
          statusDescription: 'Successful', // Hired in UI
          topMatches: true,
          jobScreeningQuestionResponses: [yesValue, noValue, yesValue],
        },
        {
          numberOfApplications: 2,
          statusId: 2,
          statusDescription: 'Successful', // Hired in UI
          topMatches: false, // Hired applications without top matches
          jobScreeningQuestionResponses: [noValue, noValue, noValue],
        },
        {
          numberOfApplications: 5,
          statusId: 3,
          statusDescription: 'Unsuccessful', // Unsuccessful in UI
          jobScreeningQuestionResponses: [noValue, noValue, noValue],
        },
        {
          numberOfApplications: 2,
          statusId: 3,
          statusDescription: 'Unsuccessful', // Unsuccessful in UI
          topMatches: true, // Unsuccessful applications with top matches
          jobScreeningQuestionResponses: [yesValue, noValue, yesValue],
        },
      ];
      couchDBApi.seedBulkApplicantsWithDifferentStatus(applicationsSeedData, jobUuid);
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  context('Verify single filter applications status and results count', () => {
    it('should show only Hired status applications when Hired filter applied and should show correct filter count', () => {
      cy.openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.checkOrUncheckApplicationStatus(['Hired']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 3); // count is based on seed data
      applicantStatus.verifyApplicationsStatusIs('match', /Hired/);
    });
  });

  context('Verify applications status update, filter re-apply and result count update', () => {
    it('should show Not updated application status only', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Not updated']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 5); // count is based on seed data
      applicantStatus.verifyApplicantStatusNotExistForAllApplicants();
    });

    it('should show updated application status when To Interview selected', () => {
      cy.clickOnCandidate(0);
      applicantStatus.selectApplicantStatus('To Interview');
      cy.shouldHaveCardLoaderLoaded();
      applicantStatus.verifyApplicationsStatusIs('match', /To Interview/);
    });

    it('should remove recently updated applications from results when filter re-applied without any changes and should show updated count', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 4); // count is based update status from above step
      applicantStatus.verifyApplicantStatusNotExistForAllApplicants();
    });
  });

  context('Verify pagination after applying filters and results count', () => {
    it('should show pagination after applying filters', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['To Interview']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicantStatus.verifyApplicationsStatusIs('match', /To Interview/);
    });

    context('should able to navigate to different pages and should show correct applications status', () => {
      const pagination = ['2', '❮', '❯', '1'];
      pagination.forEach((page) => {
        it(`click ${page}`, () => {
          cy.clickOnPaginationBy(page);
          cy.shouldHaveCardLoaderLoaded();
          applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 26); // count is based on seed data
          applicantStatus.verifyApplicationsStatusIs('match', /To Interview/);
        });
      });
    });
  });

  context('Verify filters applied when list gets sorted by job match', () => {
    before(() => {
      cy.home('/').openJobPageByJobPostId(jobPostId).skipApplicantsOnboardingIfExist();
    });
    it('should not reset filtered list when sort changes', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Hired', 'Unsuccessful']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 10);
      cy.log('Sort by job match');
      cy.clickOnSortBy('Job match');
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 10);
      applicantStatus.verifyApplicationsStatusIs('match', /Hired|Unsuccessful/);
    });

    it('should start the sorted list back to page 1 when there is a change in sort type on the filtered list', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Unsuccessful', 'Not updated', 'To Interview']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 37);
      cy.log('Click on the page 2');
      cy.clickOnPaginationBy(2);
      cy.shouldHaveCardLoaderLoaded();
      cy.log('Sort by job match');
      cy.clickOnSortBy('Job match');
      ManageJobsNavigate.shouldHavePageProperty(0, '1', 1);
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 37);
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.verifyApplicationStatusesCheckBoxAre(
        ['Unsuccessful', 'Not updated', 'To Interview'],
        'to.be.checked',
      );
    });

    it('should return a sorted list by scores including empty scores, with applications status filter applied, when sorted by job match', () => {
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Hired']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 3);
      cy.log('Sort by job match');
      cy.clickOnSortBy('Job match');
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 3);
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(0);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(1);
      ManageJobsNavigate.shouldNotHaveTopMatcherInApplicantList(2);
    });
  });

  context('Verify multiple applications status filter', () => {
    it('should show Hired and Unsuccessful applications only when corresponding filters are applied', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Hired', 'Unsuccessful']);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 10); // count is based on seed data
      applicantStatus.verifyApplicationsStatusIs('match', /Hired|Unsuccessful/);
    });
  });

  context('Verify filters - screening question responses, top matches, applications status', () => {
    const testFilterData = [
      {
        testDescription: 'Should return exact applicants with responses all yes',
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [yesValue, yesValue, yesValue],
        resultCount: 5,
      },
      {
        testDescription: 'Should return exact applicants with responses all no and with status description: Hired',
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [noValue, noValue, noValue],
        statusId: 2,
        selectApplicationFilters: ['Hired'],
        resultCount: 2,
        validatePreview: true,
      },
      {
        testDescription:
          'Should return exact applicants with responses: [yes, no, yes], topMatches and status description: Unsuccessful',
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [yesValue, noValue, yesValue],
        selectTopMatches: 'check',
        statusId: 3,
        selectApplicationFilters: ['Unsuccessful'],
        resultCount: 2,
      },
      {
        testDescription: 'Should return no filtered applicants given the queried screening responses',
        selectScreeningQuestionToggle: 'check',
        selectScreeningQuestionResponse: [noValue, yesValue, noValue],
        resultCount: 0,
      },
    ];

    testFilterData.forEach(
      ({
        testDescription,
        selectScreeningQuestionToggle,
        selectScreeningQuestionResponse,
        resultCount,
        selectTopMatches,
        selectApplicationFilters,
        validatePreview,
      }) => {
        it(`${testDescription}`, () => {
          applicationsFilter.clickOnButtonOrLink('Filter');
          applicationsFilter.clickOnButtonOrLink('Reset all filters');
          selectTopMatches && applicationsFilter.toggleTopMatchesSwitch(selectTopMatches);
          selectApplicationFilters && applicationsFilter.checkOrUncheckApplicationStatus(selectApplicationFilters);
          selectScreeningQuestionToggle && applicationsFilter.toggleScreenQuestionSwitch();
          selectScreeningQuestionToggle &&
            applicationsFilter.selectScreenQuestionResponse(selectScreeningQuestionResponse);
          applicationsFilter.clickOnButtonOrLink('Show Results');
          cy.shouldHaveCardLoaderLoaded();
          applicationsFilter.verifyFilterResultsCountExistenceAndCountIs(
            resultCount ? 'exist' : 'not.exist',
            resultCount,
          );
          validatePreview &&
            verifyScreeningQuestionResponseInApplicant(
              resultCount,
              jobScreenQuestions,
              selectScreeningQuestionResponse,
            );
        });
      },
    );
  });

  context('Verify applications status, top match filters, screening question filters and results count', () => {
    it('should show only applications with filters: Hired, Unsuccessful - Top match and screening responses of yes, no, yes', () => {
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.clickOnButtonOrLink('Reset all filters');
      applicationsFilter.checkOrUncheckApplicationStatus(['Hired', 'Unsuccessful']);
      applicationsFilter.toggleTopMatchesSwitch('check');
      applicationsFilter.toggleScreenQuestionSwitch();
      applicationsFilter.selectScreenQuestionResponse([yesValue, noValue, yesValue]);
      applicationsFilter.clickOnButtonOrLink('Show Results');
      cy.shouldHaveCardLoaderLoaded();
      shouldHaveTopMatcherForAllApplicants();
      applicationsFilter.verifyFilterResultsCountExistenceAndCountIs('exist', 3); // count is based on seed data
      applicantStatus.verifyApplicationsStatusIs('match', /Hired|Unsuccessful/);
    });
  });
});

const verifyTopMatcherInApplicationList = (numOfApplicants) => {
  cy.wrap(Cypress._.range(0, numOfApplicants))
    .wait(500) // wait till page render after loader disappear
    .each((index) => {
      ManageJobsNavigate.shouldHaveTopMatcherInApplicantList(index);
    });
};

const verifyScreeningQuestionResponseInApplicant = (count, questions, responses) => {
  for (let i = 0; i < count; i++) {
    cy.clickOnCandidate(i);
    validateScreeningQuestionsTabForApplicantExistance('exist');
    validateScreeningQuestionsAndApplicantResponses(questions, responses);
  }
};
