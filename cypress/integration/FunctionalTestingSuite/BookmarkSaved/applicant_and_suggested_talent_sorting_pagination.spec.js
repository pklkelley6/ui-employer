import moment from 'moment';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {bookmarkApi} from '../../../support/api/bookmarkApi.js';
import {validateBookmarkSortingOrder} from '../../../support/shared/manage_saved_tab.functions';
import {getPagination} from '../../../support/shared/common.functions';
import {cardLoaderShouldNotExist} from '../../../support/shared/job_page.functions';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {apiJobseeker} from '../../../support/api/jobseekerApi';

describe('Validate Saved tab - Sorting and Pagination', () => {
  const UEN = '100000003C';
  let jobPostID, jobUuid;
  // For the purpose of testing, after applications created for below candidates NRIC,
  // some of the applications will be used as the talent to bookmark using API.
  const applicantsTalentsSorting = [
    {nric: 'S4854022H', type: 'applicant', bookmarkIndex: 3},
    {nric: 'T1167450F', type: 'talent', bookmarkIndex: 2},
    {nric: 'S8758251E', type: 'applicant', bookmarkIndex: 1},
    {nric: 'T0884683E', type: 'talent', bookmarkIndex: 0},
  ];
  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
    cy.log('seed data - create a job and seed applications via couchDB');
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostID = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        applicantsTalentsSorting.forEach(({nric, type}) => {
          cy.task('profileDB', `Update jobseekers Set is_show_profile = 1 where nric = '${nric}';`) // enable the profile
            .task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${nric}';`)
            .then((results) => {
              cy.log(results);
              const mockApplicationData = couchDBMockData.defaultApplicantData(jobUuid);
              mockApplicationData.applicant.id = results[0].individual_id;
              couchDBApi.createDocument(mockApplicationData);
            })
            .then(() => {
              cy.log('seed data - bookmarking applicant and talent - separately to manipulate bookmark time');
              bookmarkApi.bookmarkApplicantOrTalent(jobUuid, nric, type);
            });
        });
      })
      .then(() => {
        cy.log('Search and open the job and click Saved tab');
        searchAndOpenJobOnTab('Open', jobPostID);
        cy.clickOnSavedTab();
        shouldHaveCardLoaderLoaded();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostID);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('validate applicant and talents sorting order', () => {
    it('should show applicant and suggested talent in descending order (latest first)', () => {
      applicantsTalentsSorting.forEach(({nric, type, bookmarkIndex}) => {
        apiJobseeker.getCandidateProfileDetails(nric, type, jobUuid).then((candidateDetail) => {
          validateBookmarkSortingOrder(candidateDetail.name, type, bookmarkIndex);
        });
      });
    });
  });

  describe('validate pagination', () => {
    const testDataPagination = [
      {
        testDescription:
          'should able to click page 2 and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 1,
        pageText: '2',
      },
      {
        testDescription:
          'should able to click page ❮ and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 0,
        pageText: '❮',
      },
      {
        testDescription:
          'should able to click page ❯ and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 2,
        pageText: '❯',
      },
      {
        testDescription:
          'should able to click page 1 and should show candidates and able to click first candidate in the page',
        pageIndexToClick: 1,
        pageText: '1',
      },
    ];

    it('should show page 1 only and not show page 2', () => {
      getPagination(0).should('exist'); // page 1 should exists
      getPagination(1).should('not.exist'); // page 2 should not exists
    });

    it('bookmark talents via DB to get pagination, reload the page and open saved tab', () => {
      cy.log(
        'seed data - getting 30 individual id from jobseekers table and inserting in job_bookmarked_talents table to get pagination',
      );
      cy.task('profileDB', 'SELECT individual_id FROM jobseekers where is_show_profile = 1 LIMIT 30;')
        .then((results) => {
          results.forEach((result) => {
            cy.log(`${result.individual_id}, ${jobUuid}, ${moment().format('YYYY-MM-DD HH:mm:ss')}`);
            cy.task(
              'profileDB',
              `INSERT INTO
            job_bookmarked_talents (individual_id, job_uuid, created_on)
            VALUES
            ('${result.individual_id}', '${jobUuid}', '${moment().format('YYYY-MM-DD HH:mm:ss')}');`,
            );
          });
        })
        .then(() => {
          cy.reload().clickOnSavedTab();
          shouldHaveCardLoaderLoaded();
        });
    });

    it('should show pagination with 2 and next page symbol', () => {
      getPagination(0).should('have.text', '1');
      getPagination(1).should('have.text', '2');
      getPagination(2).should('have.text', '❯');
    });

    testDataPagination.forEach(({testDescription, pageIndexToClick, pageText}) => {
      it(`${testDescription}`, () => {
        getPagination(pageIndexToClick).should('have.text', pageText);
        cy.clickOnPaginationIndex(pageIndexToClick);
        cardLoaderShouldNotExist();
        cy.clickOnCandidate(0);
      });
    });
  });
});
