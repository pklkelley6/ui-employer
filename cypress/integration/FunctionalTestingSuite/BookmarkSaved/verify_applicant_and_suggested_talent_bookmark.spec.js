import moment from 'moment';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {clickBookmarkIconIfTrue, verifyBookmarkCount} from '../../../support/shared/manage_saved_tab.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {applicantStatus} from '../../../support/shared/manage_applicants_page.functions';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';

const UEN = '100000008C';
const applicantsBookmark = [
  {nric: 'S1210455C', nricName: 'Maximo', bookmarkIndex: 2},
  {nric: 'S3466230D', nricName: 'S3466230D SkillsPassport Guy', bookmarkIndex: 1},
  {nric: 'S5165745D', nricName: 'Rhett', bookmarkIndex: 0},
];
const jobPosts = ['Open', 'Closed'];
let jobPostId, jobUuid;

jobPosts.forEach((tab) => {
  describe(`Verify Applicant saved bookmark details in ${tab} job`, () => {
    before(() => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
      cy.log('seed data - create a job and seed applications, bookmark via couchDB with required applicant name');
      seedXNewJobsForUEN(1, null, UEN)
        .then((response) => {
          jobPostId = response[0].metadata.jobPostId;
          jobUuid = response[0].uuid;
          applicantsBookmark.forEach(({nric, nricName}) => {
            cy.task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${nric}';`).then((results) => {
              const mockApplicationData = couchDBMockData.defaultApplicantData(jobUuid);
              mockApplicationData.applicant.name = nricName;
              mockApplicationData.createdOn = moment();
              mockApplicationData.applicant.id = results[0].individual_id;
              couchDBApi.createDocument(mockApplicationData);
            });
          });
        })
        .then(() => {
          if (tab === 'Closed')
            cy.log(`seed data - update job status in DB as Closed`).task(
              'jobDB',
              `update jobs set job_status_id = '9' where job_post_id = '${jobPostId}'`,
            );
        });
      cy.visit('/');
    });

    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
    });

    it('should save applicants bookmark from applicant tab', () => {
      const applicantSaveBookmark = {
        candidateIndexes: applicantsBookmark.map((applicant) => applicant.bookmarkIndex),
      };
      searchAndOpenJobOnTab(tab, jobPostId);
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      applicantSaveBookmark.candidateIndexes.forEach((index) => {
        cy.clickOnCandidate(index).wait(100); //wait 100ms before bookmark to manipulate bookmark time
        clickBookmarkIconIfTrue(true);
      });
    });

    it('Verify bookmark counts and applicants record exists in saved tab', () => {
      const applicantSaveBookmark = {
        bookmarkCount: applicantsBookmark.length,
      };
      reloadAndClickOnSavedTab();
      verifyBookmarkCount(applicantSaveBookmark.bookmarkCount);
      cy.get('[data-cy=bookmarked-application-item]')
        .its('length')
        .should('be.eq', applicantSaveBookmark.bookmarkCount);
      applicantsBookmark.forEach((data) => {
        cy.get('[data-cy=candidates-name]').eq(data.bookmarkIndex).should('have.text', data.nricName);
      });
    });

    it('should unsave tool tip and revist the tab, applicant should disappear in saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        unBookmark: true,
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.unBookmark);
      reloadAndClickOnSavedTab();
      cy.get('[data-cy=candidates-name]').contains(applicantSaveBookmark.nricName).should('not.exist');
    });

    it('should bookmark applicant and applicant should appear and count increased in saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        bookmark: true,
      };
      cy.clickOnApplicantTab();
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.bookmark);
      reloadAndClickOnSavedTab();
      verifyBookmarkCount(applicantsBookmark.length);
      cy.get('[data-cy=candidates-name]').contains(applicantSaveBookmark.nricName).should('exist');
    });

    it('Verify tool tip validation for unsave and save from saved tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        unBookmark: true,
        bookmark: true,
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      clickBookmarkIconIfTrue(applicantSaveBookmark.unBookmark);
      clickBookmarkIconIfTrue(applicantSaveBookmark.bookmark);
      reloadAndClickOnSavedTab();
      verifyBookmarkCount(applicantsBookmark.length);
      cy.get('[data-cy=candidates-name]').contains(applicantSaveBookmark.nricName).should('exist');
    });

    it('should reflect applicant status update both place saved tab and applicant tab', () => {
      const applicantSaveBookmark = {
        nricName: 'Rhett',
        candidateIndex: 0,
        statusUpdate: 'Hired',
      };
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      applicantStatus.selectApplicantStatus(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusRightPaneUpdated(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusLeftPanelUpdated(
        applicantSaveBookmark.statusUpdate,
        applicantSaveBookmark.candidateIndex,
      );
      cy.clickOnApplicantTab();
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
      cy.clickOnCandidate(applicantSaveBookmark.candidateIndex);
      applicantStatus.verifyApplicantStatusRightPaneUpdated(applicantSaveBookmark.statusUpdate);
      applicantStatus.verifyApplicantStatusLeftPanelUpdated(
        applicantSaveBookmark.statusUpdate,
        applicantSaveBookmark.candidateIndex,
      );
    });
  });
});

const reloadAndClickOnSavedTab = () => {
  cy.reload()
    .then(() => {
      shouldHaveCardLoaderLoaded();
      cy.skipApplicantsOnboardingIfExist();
    })
    .clickOnSavedTab();
};
