import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY, DOCUMENT_DOWNLOAD_ALIAS} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {bookmarkApi} from '../../../support/api/bookmarkApi';
import {apiJobseeker} from '../../../support/api/jobseekerApi';
import {
  getCandidateFromList,
  shouldHaveCandidateListItemInfo,
  shouldHaveCandidateRightTopPanelInfo,
  shouldHaveCandidateRightBottomPanelInfo,
} from '../../../support/shared/manage_applicants_page.functions';
import {contactSuggestedTalent} from '../../../support/shared/contact_suggested_talent.functions';
import {
  selectUnavailableTalentByName,
  validateUnavailableTalentLeftPanelInfo,
  validateUnavailableTalentRightPanelInfo,
  clickBookmarkIconIfTrue,
  validateBookmarkRibbonNotExistFor,
  validateUnavailableTalentNotExist,
} from '../../../support/shared/manage_saved_tab.functions';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';

describe('Validate Saved tab - applicant and suggested talent details', () => {
  const UEN = '100000003C';
  let jobPostID, jobUuid;
  // For the purpose of testing, after applications created for below candidates NRIC,
  // some of the applications will be used as the talent to bookmark using API.
  const applicantsTalentsSorting = [
    {nric: 'S1040617Z', type: 'applicant', bookmarkIndex: 1},
    {nric: 'T7807887H', type: 'talent', bookmarkIndex: 0}, // when candidate is bookmarked latest will be first
  ];

  before(() => {
    cy.home();
    cy.login('Account with a lot of jobs', 'Job Admin');
    cy.log('seed data - create a job and seed application via couchDB with default value and valid individual id');
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostID = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
        applicantsTalentsSorting.forEach(({nric, type}) => {
          cy.task('profileDB', `SELECT individual_id FROM jobseekers where nric = '${nric}';`)
            .then((results) => {
              cy.log(results);
              const mockApplicationData = couchDBMockData.defaultApplicantData(jobUuid);
              mockApplicationData.applicant.id = results[0].individual_id;
              couchDBApi.createDocument(mockApplicationData);
            })
            .then(() => {
              cy.log('seed data - bookmarking applicant and talent - separately to manipulate bookmark time');
              bookmarkApi.bookmarkApplicantOrTalent(jobUuid, nric, type);
            });
        });
      })
      .then(() => {
        cy.log('set talent profile to visible');
        cy.task(
          'profileDB',
          `UPDATE jobseekers SET is_show_profile = '1' WHERE nric = '${applicantsTalentsSorting[1].nric}';`,
        );
      })
      .then(() => {
        cy.log('Search and open the job and click Saved tab');
        searchAndOpenJobOnTab('Open', jobPostID);
        cy.clickOnSavedTab();
        shouldHaveCardLoaderLoaded();
      });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostID);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  applicantsTalentsSorting.forEach(({nric, type, bookmarkIndex}) => {
    describe(`validate ${type} - LHS and RHS details`, () => {
      it(`should show ${type} details in LHS`, () => {
        cy.clickOnCandidate(bookmarkIndex);
        apiJobseeker.getCandidateProfileDetails(nric, type, jobUuid).then((candidateDetails) => {
          getCandidateFromList(bookmarkIndex).then(shouldHaveCandidateListItemInfo(candidateDetails));
        });
      });

      it(`should show ${type} details in RHS top panel`, () => {
        apiJobseeker.getCandidateProfileDetails(nric, type, jobUuid).then((candidateDetails) => {
          shouldHaveCandidateRightTopPanelInfo(candidateDetails);
          if (type === 'talent') {
            contactSuggestedTalent.shouldExistSuggestedTalentInviteToApplyButton();
          } else {
            contactSuggestedTalent.shouldNotExistSuggestedTalentInviteToApplyButton();
          }
        });
      });

      it(`should show ${type} details in RHS bottom panel - all tabs`, () => {
        apiJobseeker.getCandidateProfileDetails(nric, type, jobUuid).then((candidateDetails) => {
          shouldHaveCandidateRightBottomPanelInfo(candidateDetails);
        });
      });
    });
  });

  describe('validate no longer available talent preview', () => {
    let talentDetails;
    it('set profile preferences as Not seeking opportunities via DB, reload the page and get talent details', () => {
      cy.task(
        'profileDB',
        `UPDATE jobseekers SET is_show_profile = '0' WHERE nric = '${applicantsTalentsSorting[1].nric}';`,
      ).reload();
      apiJobseeker
        .getCandidateProfileDetails(applicantsTalentsSorting[1].nric, applicantsTalentsSorting[1].type, jobUuid)
        .then((candidateDetails) => {
          talentDetails = candidateDetails;
        });
    });

    it('should show "This talent no longer available" along with name in LHS', () => {
      selectUnavailableTalentByName(talentDetails.name);
      validateUnavailableTalentLeftPanelInfo(talentDetails.name);
    });

    it('should show "This talent no longer available" along with name in RHS panel', () => {
      validateUnavailableTalentRightPanelInfo(talentDetails.name);
      validateUnavailableTalentRightPanelInfo('This talent is no longer available');
      validateUnavailableTalentRightPanelInfo('You may want to unsave this talent');
      contactSuggestedTalent.shouldNotExistSuggestedTalentInviteToApplyButton();
    });

    it('should not show ribbon and unsave static text when unbookmarked', () => {
      clickBookmarkIconIfTrue(true, {mockError: true});
      validateBookmarkRibbonNotExistFor(talentDetails.name);
      validateUnavailableTalentRightPanelInfo('You may want to unsave this talent', 'not.contain');
    });

    it('should not show talent after page is reloaded', () => {
      cy.reload().then(() => {
        validateUnavailableTalentNotExist(talentDetails.name);
      });
    });
  });

  describe('validate resume disable/enable and download for talents', () => {
    before(() => {
      cy.log(`seed data - bookmark applicant as talent without resume via DB for job - ${jobPostID}`)
        .task(
          'profileDB',
          `Insert job_bookmarked_talents (individual_id, job_uuid) values
          ((SELECT individual_id FROM jobseekers where is_show_profile = 1 and individual_id not in (SELECT individual_id FROM jobseeker_documents) limit 1), '${jobUuid}')`,
        )
        .log(`seed data - bookmark applicant as talent with resume via DB for job - ${jobPostID}`)
        .task(
          'profileDB',
          `Insert job_bookmarked_talents (individual_id, job_uuid) values
          ((SELECT individual_id FROM jobseekers where is_show_profile = 1 and individual_id in (SELECT individual_id FROM jobseeker_documents where is_default = 1 and is_deleted = 0) limit 1), '${jobUuid}')`,
        );
      cy.reload();
    });

    it('should enabled resume button and should able to download it for applicant', () => {
      verifyResumeDownloadButtonForBookmarkedCandidateIs(0, 'exist');
      cy.captureDocumentDownloadRequest();
      cy.get('[data-cy=applicant-resume-download-button]').click();
      cy.wait(`@${DOCUMENT_DOWNLOAD_ALIAS}`).its('response.statusCode').should('eq', 200);
    });

    it('should not show resume button option for talent', () => {
      verifyResumeDownloadButtonForBookmarkedCandidateIs(1, 'not.exist');
    });
  });
});

const verifyResumeDownloadButtonForBookmarkedCandidateIs = (index, assertion = 'exist') => {
  cy.get('[data-cy=bookmarked-candidate-list] > div')
    .eq(index)
    .then((el) => {
      cy.wrap(el).click();
      if (assertion === 'exist') {
        cy.wrap(el)
          .find('[data-cy=candidates-list-item-download-button]')
          .invoke('attr', 'class')
          .should('not.contain', 'cursor-not-allowed');
        cy.get('[data-cy=applicant-resume-download-button]')
          .invoke('attr', 'class')
          .should('not.contain', 'cursor-not-allowed');
      } else {
        cy.wrap(el).find('[data-cy=candidates-get-resume-text]').should('have.text', 'No Resume Available');
        cy.get('[data-cy=applicant-resume-download-button]').should('not.exist');
      }
    });
};
