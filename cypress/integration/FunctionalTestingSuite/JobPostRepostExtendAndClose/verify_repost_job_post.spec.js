import {searchAndOpenJobOnTab, closeJobWithOption} from '../../../support/shared/job_page.functions';
import {suppressBeforeUnloadEvent, verifyStaticNudgeContent} from '../../../support/shared/common.functions';
import {repostJob} from '../../../support/shared/job_repost_page_functions';
import {editJobPostButtonSelector, editRemainCounterSelector} from '../../../support/shared/job_details_page.functions';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {sixMonthsFrom, daysFrom} from '../../../support/util/date';
import {seedXCloseJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';

describe('Verify repost job post for all pages', () => {
  const loginEmployer = 'Job Repost Account';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;

  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login(loginEmployer, 'Job Admin');
  });
  beforeEach(() => {
    cy.visit('/');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Repost Job Counter', () => {
    let jobPostId;

    before(() => {
      const payload = apiJobData.defaultJobPost;
      payload.postedCompany.uen = UEN;
      payload.jobPostDuration = 30;
      seedXCloseJobsForUEN(1, payload, UEN, true).then((response) => {
        jobPostId = response[0].metadata.jobPostId;
      });
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
    });

    it('should verify Repost Button not disabled and both repost counter and date counter are valid', () => {
      searchAndOpenJobOnTab('Close', jobPostId);
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('enabled');
        repostJob.validateRepostDetailsInAsideBar('2 Reposts left', sixMonthsFrom(postedDateAttr));
      });
      verifyStaticNudgeContent();
    });

    it('should not exists edit job post button or edit job post count label for MCF close jobs', () => {
      searchAndOpenJobOnTab('Close', jobPostId);
      repostJob.clickViewRepostJobPost();
      cy.shouldHaveFormLoaderLoaded();
      cy.get(editJobPostButtonSelector).should('not.exist');
      cy.get(editRemainCounterSelector).should('not.exist');
    });

    it('should verify Repost Job Modal Pop-up for MCF close jobs', function () {
      searchAndOpenJobOnTab('Close', jobPostId);
      repostJob.clickViewRepostJobPost();
      cy.shouldHaveFormLoaderLoaded();
      repostJob.clickRepostJobButton();
      repostJob.validateRepostJobDetailsInModal('30 days', '');
      repostJob.validateRepostJobDetailsInModal('Repost Job', 'Repost this job for the duration of:');
      repostJob.validateRepostJobDetailsInModal('After submitting this repost, you will have', '1 repost left');
      repostJob.validateRepostJobDetailsInModal(daysFrom(30), '');
      repostJob.clickRepostModalDropDown();
      repostJob.validateRepostModalDropdownListValue();
      repostJob.clickRepostModalDropDown();
      repostJob.clickButtonInRepostJob('Cancel');
    });

    it('should verify Repost Button is disabled and repost counter is invalid', () => {
      cy.log('seed data - update repost count to 2')
        .task('jobDB', `UPDATE jobs SET repost_count = '2' WHERE job_post_id = '${jobPostId}';`)
        .then(() => {
          searchAndOpenJobOnTab('Close', jobPostId);
          repostJob.clickViewRepostJobPost();
          repostJob.validateRepostButtonStatus('disabled');
          repostJob.validateRepostDetailsInAsideBar('0 Reposts left');
        });
    });

    it('should verify Repost Button is disabled and repost duration is invalid', () => {
      cy.log('seed data - update job post and expiry date to past and reset repost count')
        .task(
          'jobDB',
          `UPDATE jobs SET repost_count = '0', new_posting_date = '2020-06-03', original_posting_date = '2020-06-03', expiry_date = '2020-07-03' WHERE job_post_id = '${jobPostId}';`,
        )
        .then(() => {
          searchAndOpenJobOnTab('Close', jobPostId);
          cy.getJobPostedDate().then((postedDateAttr) => {
            repostJob.clickViewRepostJobPost();
            repostJob.validateRepostButtonStatus('disabled');
            repostJob.validateRepostDetailsInAsideBar('', `Repost unavailable after  ${sixMonthsFrom(postedDateAttr)}`);
          });
        });
    });

    it('should verify Repost Button is disabled; and both repost count and repost duration is invalid', () => {
      cy.log('seed data - update repost count to 2 for job expired past 6 months')
        .task('jobDB', `UPDATE jobs SET repost_count = '2' WHERE job_post_id = '${jobPostId}';`)
        .then(() => {
          searchAndOpenJobOnTab('Close', jobPostId);
          cy.getJobPostedDate().then((postedDateAttr) => {
            repostJob.clickViewRepostJobPost();
            repostJob.validateRepostButtonStatus('disabled');
            repostJob.validateRepostDetailsInAsideBar(
              '0 Reposts left',
              `Repost unavailable after  ${sixMonthsFrom(postedDateAttr)}`,
            );
          });
        });
    });
  });

  describe('Repost end to end flow', () => {
    let jobPostId;
    const firstRepostDays = 7;
    const secondRepostDays = 21;
    const payload = apiJobData.defaultJobPost;
    payload.postedCompany.uen = UEN;
    payload.jobPostDuration = 14;
    before(() => {
      cy.log('seed Data - Create and close a Job').then(() => {
        seedXCloseJobsForUEN(1, payload, UEN, true).then((response) => {
          jobPostId = response[0].metadata.jobPostId;
        });
      });
    });

    after(() => {
      removeSeededJobInDB(jobPostId);
    });

    it(`Should be able to search closed job, validate repost details and repost for ${firstRepostDays} days`, () => {
      searchAndOpenJobOnTab('Close', jobPostId);
      cy.getJobPostedDate().then((postedDateAttr) => {
        repostJob.clickViewRepostJobPost();
        repostJob.validateRepostButtonStatus('enabled');
        repostJob.validateRepostDetailsInAsideBar('2 Reposts left', sixMonthsFrom(postedDateAttr));
        repostJob.repostJobWithDays(firstRepostDays);
        repostJob.validateRepostAcknowledgementAndClickOK(daysFrom(firstRepostDays));
        cy.validateJobStatus('REOPENED').validateJobClosingDate(daysFrom(firstRepostDays));
      });
    });
    it('Should be able to search repost job in Open tab and close it with Filled option', () => {
      searchAndOpenJobOnTab('Open', jobPostId);
      closeJobWithOption();
    });
    it(`Should be able to search closed job, validate repost details and repost for ${secondRepostDays} days`, () => {
      searchAndOpenJobOnTab('Close', jobPostId);
      repostJob.clickViewRepostJobPost();
      repostJob.validateRepostButtonStatus('enabled');
      repostJob.validateRepostDetailsInAsideBar('1 Repost left');
      repostJob.repostJobWithDays(secondRepostDays);
      repostJob.validateRepostAcknowledgementAndClickOK(daysFrom(secondRepostDays));
      cy.validateJobStatus('REOPENED').validateJobClosingDate(daysFrom(secondRepostDays));
    });
    it('Should be able to search repost job, close it and validate repost details', () => {
      searchAndOpenJobOnTab('Open', jobPostId);
      closeJobWithOption();
      searchAndOpenJobOnTab('Close', jobPostId);
      repostJob.clickViewRepostJobPost();
      repostJob.validateRepostButtonStatus('disabled');
      repostJob.validateRepostDetailsInAsideBar('0 Reposts left');
    });
  });
});
