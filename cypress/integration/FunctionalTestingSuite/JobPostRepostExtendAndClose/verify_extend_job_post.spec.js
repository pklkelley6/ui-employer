import {addDays, differenceInDays, format} from 'date-fns';
import {
  mouseoverOnManageMenuButton,
  shouldHaveCardLoaderLoaded,
  mouseoverOnExtendJobDisabledItemOption,
  verifyTooltipOnExtendJobDisabledItemOption,
  clickOnExtendJobItemOption,
  verifyExtendJobPostModalDropdownLabel,
  verifyExtendJobPostModalCancelButton,
  verifyExtendJobPostModalExtendButton,
  closeExtendJobPostModal,
  clickOnExtendJobPostModalDropdown,
  clickOnExtendJobPostModalDropdownOption,
  clickExtendJobPostModalExtendButton,
  verifyNewExtendJobPostDurationDate,
  clickOkSuccessButtonExtendJobPostModel,
  verifyUpdatedNewExtendJobPostDurationLabel,
} from '../../../support/shared/job_page.functions';
import {searchAndOpenJobOnTab} from '../../../support/shared/job_page.functions';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
const jobPostDurationOptionValue = ['14 days', '21 days', '30 days'];

describe('Job Post - Extend Job Post', function () {
  let jobPostID = '';
  const UEN = '100000006C';
  before(() => {
    cy.home().login('Job Post Close Account', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.logout();
  });
  const seedNewJobPostWithExpiryDays = (days) => {
    const jobPayload = apiJobData.defaultJobPost;
    jobPayload.postedCompany.uen = UEN;
    jobPayload.jobPostDuration = days;
    cy.log(`seed data - post new job with ${days} days expiry`);
    return seedXNewJobsForUEN(1, jobPayload, UEN);
  };
  it('should verify that extend job option is disabled and has tooltip if duration of the job is set to 30 days', () => {
    seedNewJobPostWithExpiryDays(30).then((response) => {
      jobPostID = response[0].metadata.jobPostId;
      searchAndOpenJobOnTab('Open', jobPostID);
      shouldHaveCardLoaderLoaded();
      mouseoverOnManageMenuButton();
      mouseoverOnExtendJobDisabledItemOption();
      verifyTooltipOnExtendJobDisabledItemOption();
    });
  });

  it('should find job with duration of 7 days, and see modal with options: 14 days, 21 days, and 30 days', () => {
    seedNewJobPostWithExpiryDays(7).then((response) => {
      jobPostID = response[0].metadata.jobPostId;
      searchAndOpenJobOnTab('Open', jobPostID);
      shouldHaveCardLoaderLoaded();
      cy.get('[data-cy=job-post-posted-date] time')
        .invoke('attr', 'datetime')
        .then((postedDateAttr) => {
          const postedDate = new Date(postedDateAttr);
          cy.get('[data-cy=job-post-expiry-date] time')
            .invoke('attr', 'datetime')
            .then((expiryDateAttr) => {
              const expiryDate = new Date(expiryDateAttr);
              const jobDuration = differenceInDays(expiryDate, postedDate);
              mouseoverOnManageMenuButton();
              clickOnExtendJobItemOption();
              verifyExtendJobPostModalDropdownLabel(jobDuration);
              verifyExtendJobPostModalCancelButton();
              verifyExtendJobPostModalExtendButton();

              cy.log('Click on 14 days option, and verify that date text changed');
              clickOnExtendJobPostModalDropdown();
              clickOnExtendJobPostModalDropdownOption(jobPostDurationOptionValue[0]);
              cy.get('[data-cy=extend-jobpost-modal-original-expiry-date]').contains(format(expiryDate, 'd MMM yyyy'));
              cy.get('[data-cy=extend-jobpost-modal-new-date]').contains(format(addDays(postedDate, 14), 'd MMM yyyy'));

              cy.log('Click on 21 days option, and verify that date text changed');
              clickOnExtendJobPostModalDropdown();
              clickOnExtendJobPostModalDropdownOption(jobPostDurationOptionValue[1]);
              cy.get('[data-cy=extend-jobpost-modal-new-date]').contains(format(addDays(postedDate, 21), 'd MMM yyyy'));

              cy.log('Click on 30 days option, and verify that date text changed');
              clickOnExtendJobPostModalDropdown();
              clickOnExtendJobPostModalDropdownOption(jobPostDurationOptionValue[2]);
              cy.get('[data-cy=extend-jobpost-modal-new-date]').contains(format(addDays(postedDate, 30), 'd MMM yyyy'));

              clickOnExtendJobPostModalDropdown();
              cy.verifyAllSelectedValue(
                '#extend-jobpost-modal-duration-dropdown-select div[id*=react-select]',
                jobPostDurationOptionValue,
              );

              closeExtendJobPostModal();
            });
        });
    });
  });
  it('should verify extend job post duration successfully for 21 days', () => {
    searchAndOpenJobOnTab('Open', jobPostID);
    shouldHaveCardLoaderLoaded();
    cy.get('[data-cy=job-post-posted-date] time')
      .invoke('attr', 'datetime')
      .then((postedDateAttr) => {
        const postedDate = new Date(postedDateAttr);
        cy.get('[data-cy=job-post-expiry-date] time')
          .invoke('attr', 'datetime')
          .then((expiryDateAttr) => {
            const expiryDate = new Date(expiryDateAttr);
            const jobDuration = differenceInDays(expiryDate, postedDate);
            mouseoverOnManageMenuButton();
            clickOnExtendJobItemOption();
            verifyExtendJobPostModalDropdownLabel(jobDuration);
            cy.log('Click on 21 days option, and verify that expiry date changed successfully');
            clickOnExtendJobPostModalDropdown();
            clickOnExtendJobPostModalDropdownOption(jobPostDurationOptionValue[1]);
            cy.get('[data-cy=extend-jobpost-modal-original-expiry-date]').contains(format(expiryDate, 'd MMM yyyy'));
            cy.get('[data-cy=extend-jobpost-modal-new-date]').contains(format(addDays(postedDate, 21), 'd MMM yyyy'));
            clickExtendJobPostModalExtendButton();
            cy.contains('Extension Successful');
            verifyNewExtendJobPostDurationDate(format(addDays(postedDate, 21), 'd MMM yyyy'));
            clickOkSuccessButtonExtendJobPostModel();
            cy.reload();
            verifyUpdatedNewExtendJobPostDurationLabel(format(addDays(postedDate, 21), 'd MMM yyyy'));
          });
      });
  });
});
