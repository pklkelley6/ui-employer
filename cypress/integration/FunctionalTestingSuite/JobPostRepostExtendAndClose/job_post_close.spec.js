import {
  verifyOptionFieldsForJobClose,
  validateErrorWhenSubmitWithoutOptionsForJobClose,
  selectOptionForCloseJob,
  submitCloseJob,
  verifySuccessMessageForJobClose,
  returnToAllJobs,
  validateClosedJobFoundInClosedTap,
  mouseoverOnManageMenuButton,
  shouldHaveCardLoaderLoaded,
  clickOnCloseJobItemOption,
} from '../../../support/shared/job_page.functions';
import {searchAndOpenJobOnTab} from '../../../support/shared/job_page.functions';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';

describe('Job Post - Close Job Post', function () {
  let jobPostID;
  const UEN = '100000006C';
  beforeEach(() => {
    cy.home();
    cy.login('Job Post Close Account', 'Job Admin');
    cy.log('seed data - post new job');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostID = response[0].metadata.jobPostId;
    });
  });

  afterEach(() => {
    cy.logout();
  });

  it('should have close job option in collapsed menu when job is open; and submit close job', () => {
    searchAndOpenJobOnTab('Open', jobPostID);
    shouldHaveCardLoaderLoaded();
    mouseoverOnManageMenuButton();
    clickOnCloseJobItemOption();
    verifyOptionFieldsForJobClose();
    validateErrorWhenSubmitWithoutOptionsForJobClose();
    selectOptionForCloseJob('Available');
    submitCloseJob();
    verifySuccessMessageForJobClose();
    returnToAllJobs();
    cy.reload();
    cy.clickOnJobTab('Closed');
    validateClosedJobFoundInClosedTap(jobPostID);
  });
});
