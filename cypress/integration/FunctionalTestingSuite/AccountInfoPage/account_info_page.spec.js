import {accountInfo} from '../../../support/shared/account_info.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';

describe('Account Info Page', () => {
  const loginEmployer = 'Test may update data';
  const employerInfo = Cypress._.find(userInfos, ['companyName', loginEmployer]);

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
    accountInfo.openAccountInfoPage();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Validate account info frontend error', () => {
    const testData = [
      {
        testDescription: 'without email address and contact number',
        emailAddress: '',
        contactNumber: '',
        expectedError: {
          emailAddress: 'Please fill this in',
          contactNumber: 'Please fill this in',
        },
      },
      {
        testDescription: 'wrong email address without domain',
        emailAddress: 'a',
        contactNumber: '91234567',
        expectedError: {
          emailAddress: 'Please check your email address',
        },
      },
      {
        testDescription: 'email address without incomplete domain',
        emailAddress: 'a@',
        contactNumber: '91234567',
        expectedError: {
          emailAddress: 'Please check your email address',
        },
      },
      {
        testDescription: 'wrong contact number',
        emailAddress: 'test@tech.gov.sg',
        contactNumber: '11234567',
        expectedError: {
          contactNumber: 'Please check that your phone number starts with 6, 8 or 9',
        },
      },
      {
        testDescription: 'contact number less than 8 digit',
        emailAddress: 'test@tech.gov.sg',
        contactNumber: '6123',
        expectedError: {
          contactNumber: 'Please check that your phone number has 8 digits',
        },
      },
      {
        testDescription: 'contact number greater than 8 digit',
        emailAddress: 'test@tech.gov.sg',
        contactNumber: '612345675354',
        expectedError: {
          contactNumber: 'Please check that your phone number has 8 digits',
        },
      },
      {
        testDescription: 'wrong email address and contact number',
        emailAddress: 'test@tech.gov.',
        contactNumber: '612345675354',
        expectedError: {
          emailAddress: 'Please check your email address',
          contactNumber: 'Please check that your phone number has 8 digits',
        },
      },
    ];

    testData.forEach((data) => {
      it(`should show error message input with ${data.testDescription}`, () => {
        accountInfo.clearAccountInfoFields(data);
        accountInfo.inputAccountInfoDetails(data);
        accountInfo.validateAccountInfoErrorMessages(data.expectedError);
      });
    });
  });

  describe('Validate account info successful save', () => {
    const testData = [
      {
        testDescription: 'save without Designation',
        designation: '',
        emailAddress: 'test@a.gov.sg',
        contactNumber: '61234567',
      },
      {
        testDescription: 'save with all field',
        designation: 'Senior Recruiter',
        emailAddress: 'test123456787@t123.com',
        contactNumber: '81234567',
      },
      {
        testDescription: 'save contact number starts with 9',
        designation: 'HR Manager',
        emailAddress: 'test.test@t123.com',
        contactNumber: '91234567',
      },
    ];

    testData.forEach((data) => {
      data.name = employerInfo.userFullName; // to validate name detail in below tests

      context(`should able to ${data.testDescription} and should able to view saved details`, () => {
        it('should able to input all details and save successfully', () => {
          accountInfo.openAccountInfoPage();
          accountInfo.clearAccountInfoFields(data);
          accountInfo.inputAccountInfoDetails(data);
          accountInfo.clickOnButton('Save');
          accountInfo.shouldShowSavedModalPopupAndRedirect();
        });

        it('should show saved information in account info page', () => {
          accountInfo.openAccountInfoPage();
          accountInfo.validateAccountInfoDetails(data);
        });
      });
    });
  });

  describe('Validate account info backend error on save', () => {
    before(() => {
      cy.log('mock graphQL request to simulate backend error');
      cy.mockGraphQL('profile', {
        UpdateSystemContact: {
          response: {},
          statusCode: 502,
        },
      }).then(() => accountInfo.openAccountInfoPage());
    });

    it('should display "Temporarily unable to save account info" message when Save returns error', () => {
      accountInfo.clickOnButton('Save');
      cy.verifyNotificationGrowl('Temporarily unable to save account info. Please try again later.');
    });
  });
});
