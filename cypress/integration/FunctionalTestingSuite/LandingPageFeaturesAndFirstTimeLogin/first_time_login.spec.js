import {
  validateLoginFirstTimeScreenFields,
  inputAndValidateFirstTimeLoginPage,
} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {searchAndOpenJobOnTab} from '../../../support/shared/job_page.functions';

describe('first time login - new user', function () {
  before(() => {
    cy.home();
    cy.login('First Time Login Account', 'Job Admin');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should land on terms and conditions page', () => {
    cy.contains('First time here ?').should('be.visible');
  });

  describe('terms and condition page', () => {
    it('should have all the fields along with t&c', () => {
      validateLoginFirstTimeScreenFields();
    });

    it('go to all jobs page after submit', () => {
      inputAndValidateFirstTimeLoginPage();
    });
  });
});

describe('first time login - new company', () => {
  let jobPostId;
  const loginDetail = 'First Time Login New Employer';
  const UEN = Cypress._.find(userInfos, ['companyName', loginDetail]).entityId;

  before(() => {
    cy.log('seed data - to simulate new employer, removing all company related details from DB')
      .task('jobDB', `DELETE FROM jobs where hiring_uen = '${UEN}' or posted_uen = '${UEN}';`)
      .task('profileDB', `DELETE FROM system_contact where uen = '${UEN}';`)
      .task('jobDB', `DELETE FROM company_info where uen = '${UEN}';`);
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should able to login as new employer and should able to fill terms and conditions page', () => {
    cy.home().login(loginDetail, 'Job Admin');
    inputAndValidateFirstTimeLoginPage();
  });

  it('should fetch company details from MSF and saved in DB and should show same in company profile page', () => {
    cy.visit('/company-profile');
    cy.task(
      'jobDB',
      `SELECT name, ssic_code, street  FROM company_info info join company_addresses addresses on info.uen = addresses.uen where info.uen = '${UEN}';
    `,
    ).then((results) => {
      cy.log(results);
      cy.get('[data-cy=name]').should('contain', results[0].name);
      cy.get('[data-cy=uen]').should('contain', UEN);
      cy.get('[data-cy=industry]').should('contain', results[0].ssic_code);
      cy.get('[data-cy=address]').should('contain', results[0].street); // doing partial address validation as our scope is just to ensure values shows
    });
  });

  it('should able to post new job successfully via API and search in UI', () => {
    seedXNewJobsForUEN(1, null, UEN)
      .then((response) => {
        jobPostId = response[0].metadata.jobPostId;
      })
      .then(() => {
        searchAndOpenJobOnTab('Open', jobPostId);
      });
  });
});
