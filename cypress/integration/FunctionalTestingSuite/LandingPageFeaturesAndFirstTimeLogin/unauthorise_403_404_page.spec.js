import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Unauthorised Page', function () {
  before(() => {
    const options = Cypress.env('username')
      ? {
          auth: {
            username: Cypress.env('username'),
            password: Cypress.env('password'),
          },
        }
      : {};
    cy.visit('/jobs', options);
  });

  it('redirects to unauthorised page', function () {
    cy.url().should('eq', Cypress.config('baseUrl') + '/unauthorised');
    cy.get('[data-cy="login-with-corppass-link"]').click();
    cy.url().should('include', Cypress.env('corppassUrl'));
  });
});

describe('403 - Error page - Potential Access control - job post access with different employer login', () => {
  let accessUrl;
  const authUen = '100000006C';
  const unAuthUen = '100000007C';
  before(() => {
    cy.home();
    cy.log(`seed data - post new job as ${authUen} and logout`);
    cy.login('Job Post Close Account', 'Job Admin');
    seedXNewJobsForUEN(1, null, authUen).then((response) => {
      accessUrl = `/jobs/${response[0].title.replace(/ /g, '-').toLowerCase()}-${response[0].postedCompany.name
        .replace(/ /g, '-')
        .toLowerCase()}-${response[0].uuid}`;
      cy.log(accessUrl);
      cy.logout();
      cy.log('Login as different employer').loginFromNavbar('Job Post Scheme Account', 'Job Admin');
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  const pages = ['applications', 'view', 'edit'];

  pages.forEach((page) => {
    it(`should not access the job post ${page} url by different employer login (${unAuthUen})`, () => {
      cy.visit(`${accessUrl}/${page}`).then(() => {
        cy.url().should('include', '/jobs');
        validateErrorPageTextAndHyperlink('You are unable to view this page.');
      });
    });
  });

  it(`should access the job post application url by same access employer login (${authUen})`, () => {
    cy.logout().loginFromNavbar('Job Post Close Account', 'Job Admin'); // logout previous logged in unauth employer and login as auth employer
    cy.visit(`${accessUrl}/applications`).then(() => {
      cy.skipApplicantsOnboardingIfExist();
      cy.url().should('include', `${accessUrl}/applications`);
    });
  });
});

describe('404 - Error page - We cant find the page', () => {
  const errorText = "We can't find the page you're looking for. Please check your URL.";
  const pages = ['/test', '/terms-of-us', '/job'];

  it("should show can't find page even when employer access wrong before login", () => {
    pages.forEach((page) => {
      cy.visit(page).then(() => validateErrorPageTextAndHyperlink(errorText));
    });
  });

  it("should show can't find page when employer access wrong after login", () => {
    cy.home().loginFromNavbar('Job Post Close Account', 'Job Admin');
    pages.forEach((page) => {
      cy.visit(page).then(() => validateErrorPageTextAndHyperlink(errorText));
    });
  });
});

const validateErrorPageTextAndHyperlink = (text) => {
  cy.get('#subtitle').should('contain', text);
  cy.get('[data-cy=homepage-link]')
    .should('exist')
    .and('have.text', 'Go back to homepage')
    .and('have.attr', 'href', '/');
};
