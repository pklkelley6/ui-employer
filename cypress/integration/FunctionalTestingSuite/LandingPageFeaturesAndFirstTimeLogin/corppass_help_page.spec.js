import {verifyFooterLinks} from '../../../support/shared/footer.functions';

describe('CorpPass Help Page', () => {
  before(() => {
    cy.home().get('[data-cy="banner-corppass-help"]').click();
  });

  it('Should have the correct path, page title and visible footer', () => {
    cy.title().should('eq', 'Corppass Help | MyCareersFuture Employer');
    cy.url().should('eq', Cypress.config('baseUrl') + '/corppass-help');
    verifyFooterLinks();
  });

  it('The hyperlinks should point to the correct urls and open in new tabs', () => {
    const expectedData = [
      {url: 'https://go.gov.sg/corporate-login', text: 'Find out more about the change in Corppass login process here'},
      {url: 'https://www.corppass.gov.sg/corppass/common/findoutmore', text: 'Find out more about Corppass here'},
      {url: 'https://www.corppass.gov.sg/corppass/enquirecp/enquire/singpassauth', text: 'here'},
      {url: 'https://www.corppass.gov.sg', text: 'Corppass'},
    ];

    cy.get('#corppass-help a').each(($el, index) => {
      //For each link: Check the url, that it opens in a new tab, and the link text
      cy.wrap($el)
        .should('have.attr', 'href', expectedData[index].url)
        .and('have.attr', 'target', '_blank')
        .invoke('text')
        .should('eq', expectedData[index].text);
    });
  });
});
