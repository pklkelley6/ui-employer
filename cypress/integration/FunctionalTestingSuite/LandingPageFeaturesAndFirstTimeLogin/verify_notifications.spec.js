import {
  closeFeatureBanner,
  verifyFeatureBanner,
  verifyFeatureBannerShouldNotExist,
  verifyMaintenanceBanner,
  verifyMaintenancePage,
} from '../../../support/shared/notifications.functions';

// The test cases have their date adjusted within the date range of their corresponding test data in the QA notification spreadsheet.
describe('Validate - Feature notification', () => {
  afterEach(() => {
    cy.clearLocalStorage().clearCookies();
  });

  context('Validate feature notification for the same day', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-01-07').valueOf());
    });

    it('should display feature notification on home page', () => {
      cy.home();
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
    });

    it('should display feature notification after login and after close should not display', () => {
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
      closeFeatureBanner();
      verifyFeatureBannerShouldNotExist();
    });
  });

  context('Validate feature notification on next day', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-01-09').valueOf());
    });

    it('should display feature notification on next day in home page and also should display after login', () => {
      cy.home();
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBanner('[Cypress Test] Should display this if there is an active employer feature notification');
    });
  });
});

describe('Validate - Maintenance notification', () => {
  context('Validate maintenance notification within active dates', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-01-22').valueOf());
    });

    it('should display maintenance notification if it is within the date range', () => {
      cy.home();
      verifyMaintenanceBanner('[Cypress Test] Should display this if there is an active employer maintenance');
    });

    it('should display maintenance notification after login', () => {
      cy.login('Manage Applicant Status', 'Job Admin');
      verifyMaintenanceBanner('[Cypress Test] Should display this if there is an active employer maintenance');
    });
  });

  context('Validate maintenance notification after active dates', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-02-02').valueOf());
    });

    it('should not display maintenance notification after the end date after re-login', () => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
      verifyFeatureBannerShouldNotExist();
    });
  });
});

describe('Validate - Maintenance page', () => {
  context('Validate maintenance page within the downtime date range', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-01-31'));
    });

    it('should display maintenance page it is within the downtime date range', () => {
      cy.home();
      verifyMaintenancePage('[Cypress Test] Should display this if there is an active employer maintenance');
    });
  });

  context('Validate maintenance page after the downtime date range', () => {
    beforeEach(() => {
      cy.clock(new Date('2019-02-02'));
    });

    it('should not display maintenance page after the end date and should be able to login', () => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
    });
  });
});
