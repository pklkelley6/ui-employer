import {verifyFooterLinks} from '../../../support/shared/footer.functions';
import {verifyHeaderLinks} from '../../../support/shared/header.functions';
import {checkLogoutPage} from '../../../support/shared/common.functions';

describe('Landing Page', function () {
  before(() => {
    cy.home();
  });

  it('has MCF logo, once clicked should stay on same page', () => {
    cy.get('[data-cy=mcf-logo]').click();
    cy.url().should('include', Cypress.config('baseUrl'));
  });

  it('has employer carousel', () => {
    cy.get('[data-cy="employer-carousel"]').should('exist');
    cy.get('[data-cy="employer-carousel"]').find('[data-cy="banner-0-title"]').contains('Discover suggested talents');
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-0-text"]')
      .contains(
        'Not enough applicants? Find a list of potential candidates who are open to being contacted for opportunities.',
      );
    cy.get('[data-cy="employer-carousel"] :nth-child(2) > button').click();
    cy.get('[data-cy="employer-carousel"]').find('[data-cy="banner-1-title"]').contains('Screen applicants quickly');
    cy.get('[data-cy="employer-carousel"]')
      .find('[data-cy="banner-1-text"]')
      .contains('Add screening questions to your job post to help you find the most suitable candidates.');
  });

  it('has Login with CorpPass', () => {
    cy.get('[data-cy="banner-corppass-login"]')
      .should('have.attr', 'class', 'tc bg-black-30 pa4 z-1 banner__banner-login-container___13VRR')
      .find('h3.f3.fw3.white.pb2')
      .invoke('text')
      .should('equal', 'For Employers');
    cy.get('[data-cy="banner-corppass-login"]')
      .find('[data-cy="main-login-button"]')
      .invoke('text')
      .should('equal', 'Log in now');
    cy.get('[data-cy="banner-corppass-help"]').then((el) => {
      cy.wrap(el).invoke('text').should('equal', 'Need help logging in?');
      cy.wrap(el).should('have.attr', 'href', '/corppass-help');
    });
  });

  it('should be able to login with login button', () => {
    cy.loginFromLoginButton('Job Post Testing Account', 'Job Admin');
    cy.getCookie('access-token').should('exist');
    cy.logout();
    checkLogoutPage();
    cy.home();
  });

  it('has Employer Content Pieces', () => {
    cy.get('[data-cy="employer-content"]').should(($content) => {
      expect($content).to.have.length(3);
    });
  });

  it('has Footer & Header & Masthead with various links', () => {
    verifyFooterLinks();
    verifyHeaderLinks();
    cy.loginFromNavbar('Job Post Testing Account');
    verifyFooterLinks();
    verifyHeaderLinks();
    cy.logout();
  });

  it('has login link, and after login should not have login and switch to jobseeker link', () => {
    cy.loginFromNavbar('Job Post Testing Account');
    cy.getCookie('access-token').should('exist');
    cy.get('[data-cy=navbar-login-with-corppass]').should('not.exist');
    cy.logout();
  });
});
