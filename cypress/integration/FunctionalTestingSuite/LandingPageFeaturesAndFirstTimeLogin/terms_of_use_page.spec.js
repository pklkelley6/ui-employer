import tou from '../../../fixtures/tou/TermOfUse.js';
describe('Terms Of Use Page', () => {
  before(() => {
    cy.home();
  });

  it('should leads to Terms of Use page from Footer Link', () => {
    cy.get('footer a[title="Terms of Use"]')
      .should('have.text', 'Terms of Use')
      .click()
      .get('#title')
      .should('have.text', 'TERMS OF USE')
      .get('footer')
      .should('exist');
  });
  it('should verify hyperlink from MyCareersFuture Privacy Policy', () => {
    cy.get('section[id=term-of-use] a')
      .should('have.text', 'MyCareersFuture Privacy Policy')
      .should('have.attr', 'href', 'https://www.mycareersfuture.gov.sg/privacy-policy')
      .should('have.attr', 'target', '_blank');
  });
  it('should verify all the contents of Terms of Use page', () => {
    cy.get('section[id=term-of-use] div')
      .children()
      .each(($el) => {
        //added condition for verify siblings text
        if ($el.find('li').length > 0) {
          cy.wrap($el)
            .find('li')
            .each(($li) => {
              cy.wrap($li)
                .invoke('text')
                .then((text) => {
                  expect(tou.TOUContent).to.include(text);
                });
            });
        } else {
          cy.wrap($el)
            .invoke('text')
            .then((text) => {
              expect(tou.TOUContent).to.include(text);
            });
        }
      });
  });
});
