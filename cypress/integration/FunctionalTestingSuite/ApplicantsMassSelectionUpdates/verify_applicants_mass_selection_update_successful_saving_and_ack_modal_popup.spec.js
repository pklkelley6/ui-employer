/* eslint-disable jest/no-identical-title */
import {searchAndOpenJobOnTab, shouldHaveCardLoaderLoaded} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {massSelectionUpdate} from '../../../support/shared/mass_selection_update_applicant_status.functions';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {
  validateApplicationStatusForListOfApplicantsInLHSAndRHS,
  applicantCheckbox,
} from '../../../support/shared/manage_applicants_page.functions';
import {applicationsFilter} from '../../../support/shared/applications_filter_functions';

// As its common API used in job manage to update status, the test cases are created in such a way to make sure that
// all possible scenarios are tested across the functionality either by using same applicant or same application status.
// On underline Sort by job match, Date Applied and top match are same, so added tests for WCC and top match together below.
// also for all tests default sorting is Date applied, so no extra test needed.
describe('verify applicants mass selection successful saving and ack modal popup flow', () => {
  let jobPostId, jobUuid;
  const tabNameJobStatusAndId = [
    {tabName: 'Open', jobStatus: 'Open', jobStatusId: 102},
    {tabName: 'Open', jobStatus: 'Re-Open', jobStatusId: 103},
    {tabName: 'Closed', jobStatus: 'Closed', jobStatusId: 9},
  ];
  const UEN = '100000008C';

  before(() => {
    cy.home().login('Manage Applicant Status', 'Job Admin');
    cy.log('seed data - create a job and apply applications via couchDB');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobUuid = response[0].uuid;
      jobPostId = response[0].metadata.jobPostId;
      const couchDBApplicantsMockData = [];
      cy.wrap(Cypress._.range(0, 25))
        .each(() => {
          const couchDBApplicantMockData = couchDBMockData.defaultApplicantData(jobUuid);
          couchDBApplicantMockData.scores = {
            wcc: 0.7602,
          }; // update mock data to seed wcc with top match applications
          couchDBApplicantsMockData.push(couchDBApplicantMockData);
        })
        .then(() => {
          couchDBApi.bulkDocs(couchDBApplicantsMockData);
        });
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  tabNameJobStatusAndId.forEach(({tabName, jobStatus, jobStatusId}) => {
    describe(`For ${jobStatus} job`, () => {
      before(() => {
        cy.log(`seed data - update job status in DB directly`).task(
          'jobDB',
          `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`,
        );
        searchAndOpenJobOnTab(tabName, jobPostId);
        shouldHaveCardLoaderLoaded();
      });

      context('validate single application status update to To Interview', () => {
        const candidateIndex = 1;
        const status = 'To Interview';

        after(() => {
          cy.clickOnMassApplicantCheckbox(); // reset all selected checkbox
        });

        it('should able update status for required applicant and should display correct success message', () => {
          cy.clickApplicantCheckboxByIndex(candidateIndex);
          updateApplicantStatusAndValidateSuccessMessageAndClickOk(1, status);
        });

        it('should checked mass and applicant checkbox and should display correct status in LHS and RHS', () => {
          applicantCheckbox.verifyMassApplicantCheckboxIndeterminate();
          applicantCheckbox.verifyApplicantCheckboxStatusByIndex(candidateIndex); //  checkbox should remain selected
          validateApplicationStatusForListOfApplicantsInLHSAndRHS([candidateIndex], status);
        });
      });

      context('validate multiple application status to Hired', () => {
        const candidateIndexes = [1, 15, 2];
        const status = 'Hired';

        after(() => {
          cy.clickOnMassApplicantCheckbox(); // reset all selected checkbox
        });

        it('should able update status to Hired for required applicants and validate details in success modal popup', () => {
          candidateIndexes.forEach((candidateIndex) => {
            cy.clickApplicantCheckboxByIndex(candidateIndex);
          });
          updateApplicantStatusAndValidateSuccessMessageAndClickOk(candidateIndexes.length, status);
        });

        it('should checked mass and applicant checkbox and should display correct status in LHS and RHS', () => {
          applicantCheckbox.verifyMassApplicantCheckboxIndeterminate();
          validateApplicationStatusForListOfApplicantsInLHSAndRHS(candidateIndexes, status);
        });
      });

      context('validate mass selection application status to Unsuccessful', () => {
        const status = 'Unsuccessful';

        after(() => {
          cy.clickOnMassApplicantCheckbox(); // reset all selected checkbox
        });

        it('should able update status to Unsuccessful for all applicants in the page and validate details in success modal popup', () => {
          cy.clickOnMassApplicantCheckbox();
          updateApplicantStatusAndValidateSuccessMessageAndClickOk(20, status);
        });

        it('should checked mass and applicant checkbox and should display correct status in LHS and RHS', () => {
          applicantCheckbox.verifyMassApplicantCheckboxChecked();
          applicantCheckbox.verifyAllCheckboxesChecked();
          validateApplicationStatusForListOfApplicantsInLHSAndRHS(Cypress._.range(0, 20), status);
        });
      });

      context(
        'validate multiple selection application status update to Hired by applying sort by job match and among top match',
        () => {
          before(() => {
            searchAndOpenJobOnTab(tabName, jobPostId);
            shouldHaveCardLoaderLoaded();
          });
          const candidateIndexes = [1, 15, 2, 19];
          const status = 'Hired';

          after(() => {
            cy.clickOnMassApplicantCheckbox(); // reset all selected checkbox
          });

          it('should able update status to To Interview for all applicants after applying job match filter and validate details in success modal popup', () => {
            cy.clickOnSortBy('Job match'); // wait till all applicant loads
            shouldHaveCardLoaderLoaded();
            applicationsFilter.applyTopMatchInFilter();
            shouldHaveCardLoaderLoaded();
            candidateIndexes.forEach((candidateIndex) => {
              cy.clickApplicantCheckboxByIndex(candidateIndex);
            });
            updateApplicantStatusAndValidateSuccessMessageAndClickOk(candidateIndexes.length, status);
          });

          it('should checked mass and applicant checkbox and should display correct status in LHS and RHS', () => {
            applicantCheckbox.verifyMassApplicantCheckboxIndeterminate();
            validateApplicationStatusForListOfApplicantsInLHSAndRHS(candidateIndexes, status);
          });
        },
      );

      context('validate single selection application status to Unsuccessful on 2nd pagination', () => {
        const candidateIndex = 4;
        const status = 'Unsuccessful';

        it('should able update status for required applicant and should display correct success message', () => {
          cy.clickOnPaginationIndex(1); //click on page 2
          shouldHaveCardLoaderLoaded();
          cy.clickApplicantCheckboxByIndex(candidateIndex);
          updateApplicantStatusAndValidateSuccessMessageAndClickOk(1, status);
        });

        it('should checked mass and applicant checkbox and should display correct status in LHS and RHS', () => {
          applicantCheckbox.verifyMassApplicantCheckboxIndeterminate();
          applicantCheckbox.verifyApplicantCheckboxStatusByIndex(candidateIndex); //  checkbox should remain selected
          validateApplicationStatusForListOfApplicantsInLHSAndRHS([candidateIndex], status);
        });
      });
    });
  });
});

const updateApplicantStatusAndValidateSuccessMessageAndClickOk = (numOfApplicants, status) => {
  massSelectionUpdate.clickUpdateStatusButton();
  massSelectionUpdate.selectApplicantStatus(status);
  massSelectionUpdate.clickModalPopupButtonBy('Update');
  massSelectionUpdate.verifySuccessModalText(numOfApplicants, status);
  massSelectionUpdate.clickModalPopupButtonBy('Ok');
  shouldHaveCardLoaderLoaded();
};
