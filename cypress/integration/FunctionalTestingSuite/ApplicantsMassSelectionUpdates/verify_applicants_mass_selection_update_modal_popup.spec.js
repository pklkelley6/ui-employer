import {searchAndOpenJobOnTab} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {massSelectionUpdate} from '../../../support/shared/mass_selection_update_applicant_status.functions';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';

const jobPosts = ['Open', 'Closed'];
const UEN = '100000008C';
jobPosts.forEach((tabName) => {
  describe(`Verify Applicant saved bookmark details in ${tabName} job`, () => {
    let jobPostId;
    before(() => {
      cy.home().login('Manage Applicant Status', 'Job Admin');
      cy.log('seed data - create a job and apply');
      seedXNewJobsForUEN(1, null, UEN).then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        // seed default applicants
        cy.wrap(Cypress._.range(0, 5)).each(() => {
          couchDBApi.createDocument(couchDBMockData.defaultApplicantData(response[0].uuid));
        });
        if (tabName === 'Closed') {
          cy.task(
            'jobDB',
            `UPDATE jobs SET job_status_id = 9, deleted_at = '2020-08-21 03:18:22' WHERE job_post_id = '${jobPostId}';`,
          );
        }
      });
      cy.visit('/');
    });

    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
      cy.uncheckedMassApplicantCheckbox();
    });

    after(() => {
      cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      cy.clearCookie('access-token');
    });
    it('should display modal popup window and verify contents', () => {
      searchAndOpenJobOnTab(tabName, jobPostId);
      cy.checkedMassApplicantCheckbox();
      massSelectionUpdate.clickUpdateStatusButton();
      massSelectionUpdate.verifyModalPopupIs('exist');
      massSelectionUpdate.verifyModalPopupContents(5);
      massSelectionUpdate.verifyModalPopupApplicationStatusDropDown();
      massSelectionUpdate.clickModalPopupButtonBy('Cancel');
    });
    it('should select applicant status to update and verify mass update button enabled', () => {
      cy.checkedMassApplicantCheckbox();
      massSelectionUpdate.clickUpdateStatusButton();
      massSelectionUpdate.selectApplicantStatus('Unsuccessful');
      massSelectionUpdate.verifyModalPopupSubmitButtonEnabled();
      massSelectionUpdate.clickModalPopupButtonBy('Cancel');
    });
    it('should select 2 mass applicants checkbox and verify counts in modal popup window', () => {
      cy.clickApplicantCheckboxByIndex(0);
      cy.clickApplicantCheckboxByIndex(1);
      massSelectionUpdate.clickUpdateStatusButton();
      massSelectionUpdate.verifyModalPopupContents(2);
      massSelectionUpdate.clickModalPopupButtonBy('Cancel');
    });
    it('should click model popup cancel button and window should disappear', () => {
      massSelectionUpdate.clickUpdateStatusButton();
      massSelectionUpdate.clickModalPopupButtonBy('Cancel');
      massSelectionUpdate.verifyModalPopupIs('not.exist');
    });
  });
});
