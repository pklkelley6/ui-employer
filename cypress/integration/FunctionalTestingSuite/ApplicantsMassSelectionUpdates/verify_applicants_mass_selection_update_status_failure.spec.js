import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {applicantCheckbox} from '../../../support/shared/manage_applicants_page.functions';
import {massSelectionUpdate} from '../../../support/shared/mass_selection_update_applicant_status.functions';

const UEN = '100000008C';
let jobPostId, jobUuid;
const totalApplicants = 5;
const jobStatusAndTab = [
  {jobStatus: 'Open', jobStatusId: 102},
  {jobStatus: 'Re-open', jobStatusId: 103},
  {jobStatus: 'Closed', jobStatusId: 9},
];
describe('mass update applicant status failure', () => {
  before(() => {
    cy.home().login('Manage Applicant Status', 'Job Admin');
    cy.log('seed data - create a job and apply applicants via couchDB with Received status');
    seedXNewJobsForUEN(1, null, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;
      cy.wrap(Cypress._.range(0, totalApplicants)).each(() => {
        couchDBApi.createDocument(couchDBMockData.defaultApplicantData(response[0].uuid));
      });
    });
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  jobStatusAndTab.forEach(({jobStatus, jobStatusId}) => {
    describe(`validate mass update failure for ${jobStatus} jobs`, () => {
      before(() => {
        cy.log('seed data - update job status');
        cy.task('jobDB', `update jobs set job_status_id = '${jobStatusId}' where job_post_id = '${jobPostId}'`);
      });
      it('should see full failure error message for mass update applicant status', () => {
        cy.log('mock full failure request response payload');
        cy.mockGraphQL('profile', getMassUpdateApplicantStatusFullFailurePayload())
          .openJobPageByJobPostId(jobPostId)
          .skipApplicantsOnboardingIfExist()
          .clickOnMassApplicantCheckbox();
        massSelectionUpdate.clickUpdateStatusButton();
        massSelectionUpdate.selectApplicantStatus('Unsuccessful');
        massSelectionUpdate.clickModalPopupButtonBy('Update', {mockError: true});
        cy.verifyNotificationGrowl('Temporarily unable to update application status. Please try again.');
        applicantCheckbox.verifyAllCheckboxesChecked();
        applicantCheckbox.verifyUpdateStatusButtonTextCountValue(totalApplicants);
      });
      it('should see partial failure error message for mass update applicant status', () => {
        cy.log('mock partial failure request response payload');
        cy.mockGraphQL('profile', getMassUpdateApplicantStatusPartialFailurePayload(jobUuid))
          .openJobPageByJobPostId(jobPostId)
          .skipApplicantsOnboardingIfExist();
        cy.clickOnMassApplicantCheckbox();
        massSelectionUpdate.clickUpdateStatusButton();
        massSelectionUpdate.selectApplicantStatus('Unsuccessful');
        massSelectionUpdate.clickModalPopupButtonBy('Update', {mockError: true});
        cy.verifyNotificationGrowl(
          'Temporarily unable to update application status for some applicants. Please try again.',
        );
        applicantCheckbox.verifyAllCheckboxesChecked();
        applicantCheckbox.verifyUpdateStatusButtonTextCountValue(totalApplicants);
      });
    });
  });
});

const getMassUpdateApplicantStatusFullFailurePayload = () => {
  return {
    setMassApplicationShortlistAndStatusUpdate: {
      response: {
        errors: [{message: 'mock applicant error response'}],
        data: {setMassApplicationShortlistAndStatusUpdate: null},
      },
    },
  };
};
const getMassUpdateApplicantStatusPartialFailurePayload = (uuid) => {
  const partialFailurePayload = {
    setMassApplicationShortlistAndStatusUpdate: {
      response: {
        data: {setMassApplicationShortlistAndStatusUpdate: []},
      },
    },
  };
  couchDBApi.find({selector: {jobId: uuid}}).then((responseBody) => {
    partialFailurePayload.setMassApplicationShortlistAndStatusUpdate.response.data.setMassApplicationShortlistAndStatusUpdate.push(
      {id: responseBody.docs[0]._id, ok: false, __typename: 'ApplicationUpdateResult'},
    );
  });
  return partialFailurePayload;
};
