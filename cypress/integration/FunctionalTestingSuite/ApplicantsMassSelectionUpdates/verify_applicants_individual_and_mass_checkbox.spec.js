import {applicantCheckbox, applicantStatus} from '../../../support/shared/manage_applicants_page.functions';
import {searchAndOpenJobOnTab} from '../../../support/shared/job_page.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {clickBookmarkIconIfTrue} from '../../../support/shared/manage_saved_tab.functions';
import {couchDBApi} from '../../../support/api/couchDBApi.js';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';
import {seedXNewJobsForUEN} from '../../../support/manageData/job.seed';
import {applicationsFilter} from '../../../support/shared/applications_filter_functions';

const tabNames = ['Open', 'Closed'];
const UEN = '100000002C';

tabNames.forEach((tabName) => {
  describe(`Applicants - Verify mass checkbox and individual checkbox (${tabName} Job)`, () => {
    let jobPostId;
    const couchDBApplicantsMockData = [];

    before(() => {
      cy.home();
      cy.login('Test may update data', 'Job Admin');
      cy.log('seed data - create a job and seed default applicants');
      seedXNewJobsForUEN(1, null, UEN)
        .then((response) => {
          jobPostId = response[0].metadata.jobPostId;
          cy.wrap(Cypress._.range(0, 25))
            .each(() => {
              couchDBApplicantsMockData.push({
                ...couchDBMockData.defaultApplicantData(response[0].uuid),
                scores: {
                  wcc: 0.6602,
                },
              });
            })
            .then(() => {
              couchDBApi.bulkDocs(couchDBApplicantsMockData);
            });
        })
        .then(() => {
          if (tabName === 'Closed') {
            cy.task(
              'jobDB',
              `UPDATE jobs SET job_status_id = 9, deleted_at = '2020-08-21 03:18:22' WHERE job_post_id = '${jobPostId}';`,
            );
          }
          cy.visit('/');
          searchAndOpenJobOnTab(tabName, jobPostId);
        });
    });
    afterEach(() => {
      cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
      Cypress.Cookies.preserveOnce('access-token');
      cy.uncheckedMassApplicantCheckbox();
    });
    after(() => {
      cy.logout();
    });

    it('should see indeterminate checkbox and update status button if one applicant checkbox is checked', () => {
      cy.clickApplicantCheckboxByIndex(0);
      applicantCheckbox.verifyMassApplicantCheckboxIndeterminate();
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(1);

      cy.clickApplicantCheckboxByIndex(0);
      applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
    });

    it('should see all individual checkbox checked when mass checkbox is checked and unchecked', () => {
      cy.checkedMassApplicantCheckbox();
      applicantCheckbox.verifyAllCheckboxesChecked();
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(20);
      cy.uncheckedMassApplicantCheckbox();
      applicantCheckbox.verifyAllCheckboxesUnchecked();
    });

    it('should see checked checkbox and update status button if all applicants checkbox are checked', () => {
      Array.from(Array(20).keys()).forEach((key) => cy.clickApplicantCheckboxByIndex(key));
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(20);
      applicantCheckbox.verifyMassApplicantCheckboxChecked();

      Array.from(Array(20).keys()).forEach((key) => cy.clickApplicantCheckboxByIndex(key));
      applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
    });

    it('should see an increase in count for update status button value according to applications checked', () => {
      Array.from(Array(2).keys()).forEach((key) => cy.clickApplicantCheckboxByIndex(key));
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(2);
      cy.clickApplicantCheckboxByIndex(2);
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(3);
      cy.clickOnMassApplicantCheckbox();
    });

    it('should see a decrease in count for update status button value according to applications checked', () => {
      Array.from(Array(3).keys()).forEach((key) => cy.clickApplicantCheckboxByIndex(key));
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(3);
      cy.clickApplicantCheckboxByIndex(2);
      applicantCheckbox.verifyUpdateStatusButtonTextCountValue(2);
      cy.clickOnMassApplicantCheckbox();
    });

    it('should remained checked for individual and mass checkbox if unviewed applicants is clicked', () => {
      cy.checkedMassApplicantCheckbox();
      cy.clickUnviewedCandidateByIndex(0);
      applicantCheckbox.verifyMassApplicantCheckboxChecked();
      applicantCheckbox.verifyAllCheckboxesChecked();
    });

    it('should remained checked for individual and mass checkbox if applicant is being bookmarked', () => {
      cy.checkedMassApplicantCheckbox();
      cy.clickOnCandidate(0);
      clickBookmarkIconIfTrue(true);
      applicantCheckbox.verifyMassApplicantCheckboxChecked();
      applicantCheckbox.verifyAllCheckboxesChecked();
      clickBookmarkIconIfTrue(true);
    });

    it('should remained checked for individual and mass checkbox if applicant status is changed', () => {
      cy.checkedMassApplicantCheckbox();
      cy.clickOnCandidate(0);
      applicantStatus.selectApplicantStatus('Unsuccessful');
      applicantCheckbox.verifyMassApplicantCheckboxChecked();
      applicantCheckbox.verifyAllCheckboxesChecked();
    });

    it('should unchecked all individual checkbox and mass applicant checkbox when page reloads', () => {
      cy.checkedMassApplicantCheckbox();
      cy.reload().then(() => {
        cy.skipApplicantsOnboarding();
        applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
        applicantCheckbox.verifyAllCheckboxesUnchecked();
      });
    });

    it('should unchecked mass and individual applicant checkboxes when moving to next page', () => {
      cy.checkedMassApplicantCheckbox();
      cy.log('Click on the page 2');
      cy.clickOnPaginationIndex(1);
      applicantCheckbox.verifyAllCheckboxesUnchecked();
      applicantCheckbox.verifyUpdateStatusLabel();
      cy.clickOnPaginationIndex(0);
    });

    it('should unchecked all individual checkbox and mass applicant checkbox when sorting is applied', () => {
      cy.checkedMassApplicantCheckbox();
      cy.clickOnSortBy('Job match');
      applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
      applicantCheckbox.verifyAllCheckboxesUnchecked();
      cy.clickOnSortBy('Date applied');
    });

    it('should unchecked all individual checkbox and mass applicant checkbox when page is moved to Suggested Talent tab', () => {
      cy.checkedMassApplicantCheckbox();
      cy.clickOnSuggestedTalentTab();
      cy.clickOnApplicantTab();
      applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
      applicantCheckbox.verifyAllCheckboxesUnchecked();
    });

    context(
      `Applicants - Verify mass checkbox and individual checkbox (${tabName} Job are unchecked when topmatch filters applied`,
      () => {
        before(() => {
          cy.visit('/');
          searchAndOpenJobOnTab(tabName, jobPostId);
        });

        it('should unchecked all individual checkbox and mass applicant checkbox when top match switch toggle is turned on', () => {
          cy.checkedMassApplicantCheckbox();
          applicationsFilter.applyTopMatchInFilter();
          applicantCheckbox.verifyMassApplicantCheckboxUnchecked();
          applicantCheckbox.verifyAllCheckboxesUnchecked();
        });
      },
    );
  });
});
