import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {
  verifyMinSkillsRequired,
  addOneMoreSkills,
  verifyMaxSkillsAdded,
  skillsNotFound,
  clearAllSkillsFromRecommendedSection,
  addMaxSkillsForTesting,
} from '../../../support/shared/job_post_skills_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Skills validation in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.fixture('JobPosting.json').as('jobPost');
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should have validation for skills', function () {
    fillJobDescriptionStep(this.jobPost.jobPostValidationMin);
    onClickNext();
    clearAllSkillsFromRecommendedSection();
    onClickNext();
    verifyMinSkillsRequired();
    addMaxSkillsForTesting();
    addOneMoreSkills();
    cy.verifyNotificationGrowl('You can only include up to 20 skills');
    onClickBack();
    fillJobDescriptionStep(this.jobPost.jobPostValidationMax);
    onClickNext();
    onClickNext();
    verifyMaxSkillsAdded();
  });
  it('should show no relevant skills found and verify relevant skills already added', function () {
    fillJobDescriptionStep(this.jobPost.jobPostValidationMin);
    onClickNext();
    skillsNotFound();
    // Below line is comment since it is comes to play another user story. need to enable once relevant user story done
    // relevantSkillsAddedAlready();
  });
});
