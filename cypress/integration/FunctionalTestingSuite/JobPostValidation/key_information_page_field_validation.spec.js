import {fillJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectAddMoreSkillSection} from '../../../support/shared/job_post_skills_page.functions';
import {
  verifyJobPostDurationValidations,
  verifyJobCategoriesValidations,
  verifyPositionLevelValidations,
  verifyQualificationValidations,
  verifyFieldOfStudyValidations,
  verifyNumberOfVacanciesValidations,
  verifyMinimumYearsExperienceValidations,
  verifyEmploymentTypeValidations,
  verifySalaryValidations,
  verifyScheme,
  verifySchemeNotExists,
  selectPCPScheme,
  verifyPCPSchemeValidations,
} from '../../../support/shared/job_post_key_information_page.functions';
import {onClickNext, onClickBack, suppressBeforeUnloadEvent} from '../../../support/shared/common.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Key information field validations in Job Posting Page', function () {
  before(() => {
    suppressBeforeUnloadEvent();
    cy.home();
    cy.login('Job Post Scheme Account', 'Job Admin');
    cy.clickNewJobPosting();
    cy.shouldHaveFormLoaderLoaded();
  });
  beforeEach(() => {
    cy.fixture('JobPosting.json').as('jobPost');
  });
  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });
  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('fill in job description and skills', function () {
    fillJobDescriptionStep(this.jobPost.jobPost1);
    onClickNext();
    searchAndSelectAddMoreSkillSection(this.jobPost.jobPost1.skillNotFromDropDown);
    onClickNext();
  });

  it('error bar should appear when trying to click next with current page is not valid', function () {
    onClickNext();
    cy.get('[data-cy=error-card]').contains('Please amend the highlighted fields to continue');
    onClickBack();
    onClickNext();
  });

  it('should have validation for fields in Key information tab in Job Posting Page', function () {
    verifyJobPostDurationValidations();
    verifyNumberOfVacanciesValidations();
    verifyJobCategoriesValidations(this.jobPost.jobPost1);
    verifyPositionLevelValidations();
    verifyMinimumYearsExperienceValidations();
    verifyEmploymentTypeValidations();
    verifyQualificationValidations();
    verifyFieldOfStudyValidations(this.jobPost.jobPost1);
    verifySalaryValidations();
  });

  it('should not have Career Trial scheme for selection', function () {
    verifySchemeNotExists('CareerTrial');
  });

  it('should have govt company scheme', function () {
    verifyScheme('PCP');
    verifyScheme('PMax');
    selectPCPScheme();
    onClickNext();
    verifyPCPSchemeValidations();
  });
});
