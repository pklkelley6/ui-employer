/* eslint-disable jest/no-identical-title */
import faker from 'faker';
import {mcf} from '@mcf/constants';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {onClickNext} from '../../../support/shared/common.functions';
import {inputJobDescriptionStep} from '../../../support/shared/job_post_description_page.functions';
import {searchAndSelectXSkills} from '../../../support/shared/job_post_skills_page.functions';
import {inputKeyInformation} from '../../../support/shared/job_post_key_information_page.functions';
import {screeningQuestions} from '../../../support/shared/job_post_manage_screening_questions.functions';
import {clickSubmitJobPost} from '../../../support/shared/job_post_preview_page.functions';
import {getJobPostId} from '../../../support/shared/job_post_success_page.functions';
import {removeSeededJobInDB} from '../../../support/manageData/job.seed.js';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {
  validateScreeningQuestionsTabForApplicantExistance,
  validateScreeningQuestionsAndApplicantResponses,
} from '../../../support/shared/manage_applicants_page.functions';
import {couchDBMockData} from '../../../fixtures/seedData/couchdb_applicant_seed_data';

describe('Job post create - Screening Questions and Applicant preview', () => {
  const loginEmployer = 'Test may update data';

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
  });

  beforeEach(() => {
    cy.on('window:confirm', (_) => {
      return true;
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Job post create without Screening questions - successful submission and validate no applicant screening questions tab', () => {
    context('create a job without any change in Screening question field', () => {
      let jobPostId;
      it('should unchecked the "Include screening questions" checkbox and clicking Next button without any input for questions', () => {
        openCreateJobPostingAndInputPreRequistiesDetails();
        screeningQuestions.validateIncludeScreeningQuestionsCheckboxIs('not.be.checked');
        screeningQuestions.validateScreeningQuestionsFieldsIs('not.exist');
        onClickNext();
      });

      it('should not display screening questions in preview page and should able post job successfully', () => {
        screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
        clickSubmitJobPost();
        getJobPostId().then((id) => {
          jobPostId = id;
        });
      });

      it('should not display screening questions message in review page', () => {
        cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
        screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
      });

      it('should not display screening questions tab for applicant', () => {
        cy.log('seed data - insert applicant details via couchDB');
        cy.task('jobDB', `Select uuid from jobs where job_post_id = '${jobPostId}'`).then((results) => {
          couchDBApi.seedBulkApplicants(1, results[0].uuid);
        });

        // Open applicants and validate screening questions tab
        cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
        cy.clickOnCandidate(0);
        validateScreeningQuestionsTabForApplicantExistance('not.exist');
      });

      after(() => {
        removeSeededJobInDB(jobPostId);
      });
    });

    context('create a job, input screening question and uncheck', () => {
      let jobPostId;
      it('should able to input screening questions and uncheck', () => {
        openCreateJobPostingAndInputPreRequistiesDetails();
        screeningQuestions.checkOrUncheckIncludeScreeningQuestions('check');
        screeningQuestions.validateScreeningQuestionsFieldsIs('exist');

        // input Questions and uncheck
        screeningQuestions.inputQuestionDetails({
          1: 'Are you willing to work in shift',
          2: 'Do you have experience leading a team',
        });
        screeningQuestions.checkOrUncheckIncludeScreeningQuestions('uncheck');

        onClickNext();
      });

      it('should not display screening questions in preview and should able post job successfully', () => {
        screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
        clickSubmitJobPost();
        getJobPostId().then((id) => {
          jobPostId = id;
        });
      });

      it('should display no screening questions message in review page', () => {
        cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
        screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
      });

      after(() => {
        removeSeededJobInDB(jobPostId);
      });
    });
  });

  describe('Job post create validate error messages for screening question fields', () => {
    const testData = [
      {
        questions: {
          1: '',
          2: '',
          3: '',
        },
        errorAssertion: 'exist',
        expectedErrorMessage: 'Please fill this in',
      },
      {
        questions: {
          1: 'te',
          2: 'a',
          3: 'add',
        },
        errorAssertion: 'exist',
        expectedErrorMessage: 'Please enter at least 5 characters',
      },
      {
        questions: {
          1: faker.lorem.paragraphs(150).substring(0, 122),
          2: faker.lorem.paragraphs(150).substring(0, 130),
          3: faker.lorem.paragraphs(150).substring(0, 150),
        },
        errorAssertion: 'exist',
        expectedErrorMessage: 'Please keep within 120 characters',
      },
    ];

    before(() => {
      openCreateJobPostingAndInputPreRequistiesDetails();
      screeningQuestions.checkOrUncheckIncludeScreeningQuestions('check');
    });

    testData.forEach(({questions, expectedErrorMessage}) => {
      it(`should show error message "${expectedErrorMessage}"`, () => {
        screeningQuestions.inputQuestionDetails(questions, {paste: true});
        Cypress._.each(questions, (_, num) => {
          screeningQuestions.validateErrorMessageForQuestion(num, expectedErrorMessage);
        });
        // remove all existing questions if exists
        screeningQuestions.removeAllExistingQuestionsExceptDefault();
      });
    });
  });

  describe('Job post create with Screening questions - successful submission and validate applicant screening question response', () => {
    let jobPostId;
    const questions = {
      1: 'Are you willing to work in shift?',
      2: 'Question with special character !@#$%^&*() {}|:"<>?',
      3: 'Test QUESTION 1324 24',
    };
    const questionsResponse = [
      mcf.SCREEN_QUESTION_RESPONSE.YES,
      mcf.SCREEN_QUESTION_RESPONSE.NO,
      mcf.SCREEN_QUESTION_RESPONSE.YES,
    ];

    it('should able to add screening questions successfully', () => {
      openCreateJobPostingAndInputPreRequistiesDetails();
      screeningQuestions.checkOrUncheckIncludeScreeningQuestions('check');
      screeningQuestions.inputQuestionDetails(questions);
      onClickNext();
    });

    it('should display screening questions in preview page and should able post job successfully', () => {
      screeningQuestions.validateQuestionsDetailsInPreviewReviewPage(questions);
      clickSubmitJobPost();
      getJobPostId().then((id) => {
        jobPostId = id;
      });
    });

    it('should display screening questions message in review page', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
      screeningQuestions.validateQuestionsDetailsInPreviewReviewPage(questions);
    });

    it('should display applicant screening questions response', () => {
      cy.log('seed data - insert applicant details via couchDB with screening question');
      cy.task('jobDB', `Select uuid from jobs where job_post_id = '${jobPostId}'`).then((results) => {
        const couchDBApplicantMockData = couchDBMockData.defaultApplicantData(results[0].uuid);
        couchDBApplicantMockData.jobScreeningQuestionResponses = questionsResponse;
        couchDBApi.createDocument(couchDBApplicantMockData);
      });

      // validate screening questions for applicant
      cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
      cy.clickOnCandidate(0);
      validateScreeningQuestionsTabForApplicantExistance('exist');
      validateScreeningQuestionsAndApplicantResponses(questions, questionsResponse);
    });
  });
});

const openCreateJobPostingAndInputPreRequistiesDetails = () => {
  // Input Step 1, 2, 3, 4 details in job post create
  cy.clickNewJobPosting();

  inputJobDescriptionStep({}, {paste: true});
  onClickNext();

  searchAndSelectXSkills('testing', 10);
  onClickNext();

  inputKeyInformation();
  onClickNext();
  onClickNext();
};
