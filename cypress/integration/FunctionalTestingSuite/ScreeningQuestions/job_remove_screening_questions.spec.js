import {mcf} from '@mcf/constants';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {couchDBApi} from '../../../support/api/couchDBApi';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {userInfos} from '../../../support/users';
import {applicationsFilter} from '../../../support/shared/applications_filter_functions';
import {validateScreeningQuestionsTabForApplicantExistance} from '../../../support/shared/manage_applicants_page.functions';
import {screeningQuestions} from '../../../support/shared/job_post_manage_screening_questions.functions';
import {mouseoverOnManageMenuButton} from '../../../support/shared/job_page.functions';
import {checkEditCountOfJobPost} from '../../../support/shared/job_details_page.functions';

describe('Job - Remove Screening Questions', () => {
  let jobPostId, jobUuid;
  const loginEmployer = 'Test may update data';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;

  const questions = {
    1: 'Are you willing to work in shift?',
    2: 'Do you have a driving license?',
    3: 'Can you work on weekends?',
  };

  const yesValue = mcf.SCREEN_QUESTION_RESPONSE.YES;
  const noValue = mcf.SCREEN_QUESTION_RESPONSE.NO;

  const applicationsSeedData = [
    {
      numberOfApplications: 5,
      statusId: 3, // Unsuccessful
      jobScreeningQuestionResponses: [yesValue, noValue, yesValue],
    },
    {
      numberOfApplications: 3,
      statusId: 2, // Hired
      jobScreeningQuestionResponses: [yesValue, yesValue, yesValue],
    },
  ];

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
    cy.log('seed data - job post with screening questions');
    const payload = {
      ...apiJobData.defaultJobPost,
      postedCompany: {
        uen: UEN,
      },
    };
    seedXNewJobsForUEN(1, payload, UEN).then((response) => {
      jobPostId = response[0].metadata.jobPostId;
      jobUuid = response[0].uuid;

      cy.log('seed data - insert applicant details via couchDB');
      couchDBApi.seedBulkApplicantsWithDifferentStatus(applicationsSeedData, jobUuid);
    });
  });

  beforeEach(() => {
    cy.on('window:confirm', (_) => {
      return true;
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Validate Remove Screening Questions menu display', () => {
    it('should not show Screening Questions menu for job which does not have screening questions', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
      mouseoverOnManageMenuButton();
      screeningQuestions.validateRemoveScreeningQuestionsMenuExistance('not.exist');
    });

    it('should show Screening Questions menu for job which have screening questions', () => {
      cy.log('seed data - insert screening question for the job');
      Cypress._.each(questions, (question, num) => {
        cy.task(
          'jobDB',
          `INSERT INTO job_screening_questions (job_uuid, sequence, question) VALUES ('${jobUuid}', '${
            num - 1
          }', '${question}');`,
        );
      });
      cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
      mouseoverOnManageMenuButton();
      screeningQuestions.validateRemoveScreeningQuestionsMenuExistance('exist');
    });
  });

  describe('Validate modal popup content and Cancel button', () => {
    it('should display modal with Question details', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
      mouseoverOnManageMenuButton();
      screeningQuestions.clickOnRemoveScreeningQuestionsOption();
      screeningQuestions.modalPopup.validateWarningModalPopupExistence('exist');
      screeningQuestions.modalPopup.validateWarningModalPopupShowsQuestions(questions);
    });

    it('should dismiss modal popup on clicking Cancel button', () => {
      screeningQuestions.modalPopup.clickOnButton('Cancel');
      screeningQuestions.modalPopup.validateWarningModalPopupExistence('not.exist');
    });
  });

  describe('Validate Removing screening questions flow', () => {
    it('should show warning and success modal up with question details', () => {
      mouseoverOnManageMenuButton();
      screeningQuestions.clickOnRemoveScreeningQuestionsOption();
      screeningQuestions.modalPopup.validateWarningModalPopupExistence('exist');
      screeningQuestions.modalPopup.clickOnButton('Yes, Remove');

      screeningQuestions.modalPopup.validateSuccessModalPopupExistenceAndClickOk();
    });

    it('should not show Screening question details in Filters', () => {
      cy.skipApplicantsOnboardingIfExist();
      applicationsFilter.clickOnButtonOrLink('Filter');
      applicationsFilter.verifyScreenQuestionSwitchExist('not.exist');
      applicationsFilter.clickOnButtonOrLink('Cancel');
    });

    it('should not show Screening questions details in Candidate Preview', () => {
      cy.clickOnCandidate(0);
      validateScreeningQuestionsTabForApplicantExistance('not.exist');
    });

    it('should show no screening questions in Job post View page', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
      screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
    });

    it('should created audit trails for remove screening question action in DB', () => {
      cy.task(
        'jobDB',
        `SELECT * FROM audit_trails where job_post_id = '${jobPostId}' and action_type = 'RMV_SCREEN_QNS';`,
      ).then((result) => {
        cy.wrap(result).should('not.be.empty');
      });
    });

    it('should not increased edit post count for the job', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'view'});
      checkEditCountOfJobPost('2');
    });
  });

  describe('Validate removing screening question error response', () => {
    before(() => {
      cy.log('seed data - insert screening question for the job');
      Cypress._.each(questions, (question, num) => {
        cy.task(
          'jobDB',
          `INSERT INTO job_screening_questions (job_uuid, sequence, question) VALUES ('${jobUuid}', '${
            num - 1
          }', '${question}');`,
        );
      });
      cy.intercept('DELETE', `**/screening-questions`, {
        statusCode: 504,
        body: {},
      }).as('DELETE_QUESTION_REQUEST');
    });
    it('should show error growl notification', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'applications'}).skipApplicantsOnboardingIfExist();
      mouseoverOnManageMenuButton();
      screeningQuestions.clickOnRemoveScreeningQuestionsOption();
      screeningQuestions.modalPopup.clickOnButton('Yes, Remove');
      cy.wait('@DELETE_QUESTION_REQUEST');
      cy.verifyNotificationGrowl('Temporarily unable to remove screening questions. Please try again.');
    });
  });
});
