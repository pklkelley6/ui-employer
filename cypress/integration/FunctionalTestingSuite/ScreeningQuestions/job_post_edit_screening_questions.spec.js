/* eslint-disable jest/no-identical-title */
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {onClickNext} from '../../../support/shared/common.functions';
import {seedXNewJobsForUEN, removeSeededJobInDB} from '../../../support/manageData/job.seed.js';
import {apiJobData} from '../../../fixtures/seedData/api_job_post_seed_data';
import {userInfos} from '../../../support/users';
import {screeningQuestions} from '../../../support/shared/job_post_manage_screening_questions.functions.js';

describe('Job post edit - Screening Questions and Preview', () => {
  const loginEmployer = 'Test may update data';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;
  const questions = [
    {
      question: 'Question 1 !@#$%^&*() 23423guyidsf lgukasdf gkasdf',
    },
    {
      question: 'Test 123 df lliuasd fjkgdv sckgysadlf',
    },
    {
      question: '23423j',
    },
  ];
  let jobPostId, jobUuid;

  before(() => {
    cy.home().login(loginEmployer, 'Job Admin');
  });

  beforeEach(() => {
    cy.on('window:confirm', (_) => {
      return true;
    });
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    removeSeededJobInDB(jobPostId);
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Job Post with Screening questions', () => {
    before(() => {
      cy.log('seed data - job post with screening questions');
      const payload = {
        ...apiJobData.defaultJobPost,
        postedCompany: {
          uen: UEN,
        },
        screeningQuestions: questions,
      };
      seedXNewJobsForUEN(1, payload, UEN).then((response) => {
        jobPostId = response[0].metadata.jobPostId;
        jobUuid = response[0].uuid;
      });
    });
    it('should show read-only screening questions and tooltip', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'edit#screening-questions'});
      questions.forEach((q, index) => {
        cy.get('div[class*=ScreeningQuestionsFields__question]').eq(index).should('have.text', q.question);
      });
      screeningQuestions.validateScreeningQuestionsTooltipExistance();
    });

    it('should not show edit link in preview section for screening questions', () => {
      onClickNext();
      screeningQuestions.validatePreviewEditLinkExistence('not.exist');
    });
  });

  describe('Job Post without Screening questions', () => {
    before(() => {
      cy.log('seed data - remove screening questions for the job').task(
        'jobDB',
        `DELETE FROM job_screening_questions where job_uuid = '${jobUuid}';`,
      );
    });

    it('should show no screening questions section and should show tooltip', () => {
      cy.openJobPageByJobPostId(jobPostId, {mode: 'edit#screening-questions'});
      screeningQuestions.validateNoQuestionsSectionInEditScreeningQuestionsTab();
      screeningQuestions.validateScreeningQuestionsTooltipExistance();
    });

    it('should not show edit link in preview section for screening questions', () => {
      onClickNext();
      screeningQuestions.validateNoScreeningQuestionsDetailsInPreviewReviewPage();
      screeningQuestions.validatePreviewEditLinkExistence('not.exist');
    });
  });
});
