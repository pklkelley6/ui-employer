import moment from 'moment';
import {talentSearch} from '../../../support/shared/talentSearch.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';
import {apiJobseeker} from '../../../support/api/jobseekerApi';
import {
  shouldHaveCandidateListItemInfo,
  shouldHaveCandidateRightTopPanelInfo,
  shouldHaveCandidateRightBottomPanelInfo,
} from '../../../support/shared/manage_applicants_page.functions';

describe('Verify talent search results', () => {
  const testDataSearchKeywords = [
    {keyword: 'Automation', assertion: 'exist', type: 'skill'},
    {keyword: '.net java c++ a/b', assertion: 'exist', type: 'skills'},
    {keyword: '"A/B Testing"', assertion: 'exist', type: 'skill with full search'},
    {keyword: 'MIT', assertion: 'exist', type: 'institution'},
    {keyword: 'mcf', assertion: 'exist', type: 'company name'},
    {keyword: '"Senior Software Architect"', assertion: 'exist', type: 'job title with full search'},
    {keyword: 'Aerospace Electronics', assertion: 'exist', type: 'field of study'},
    {keyword: '2001', assertion: 'not.exist', type: 'educations - year attained'},
    {keyword: 'Dayna95@hotmail.com', assertion: 'not.exist', type: 'email'},
    {keyword: 'Drake Olson', assertion: 'not.exist', type: 'name'},
  ];

  const talentName = 'Drake Olson';
  const talentNric = 'S4854022H';
  const keywordToReturnUniqueTalent = 'CYPRESS_TALENT_SEARCH'; // to return above talent based on Education qualification
  let talentDetails;

  before(() => {
    cy.home();
    cy.log('seed data - update candiate active status and enable resume')
      .task(
        'profileDB',
        `Update jobseekers Set last_login = '${moment().format(
          'YYYY-MM-DD HH:mm:ss',
        )}', is_show_profile = '1' where nric = '${talentNric}';`,
      )
      .task(
        'profileDB',
        `Update jobseeker_documents set is_deleted = '0' where is_default = '1' and individual_id = (Select individual_id from jobseekers where nric = '${talentNric}');`,
      );
    cy.login('Job Post Testing Account', 'Job Admin');
    talentSearch.openTalentSearchPage();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('verify search result for different keywords', () => {
    testDataSearchKeywords.forEach(({keyword, assertion, type}) => {
      it(`verify search result for ${type} - ${keyword}`, () => {
        talentSearch.inputAndClickSearchButton(keyword);
        talentSearch.validateTalentSearchResultCountIs(assertion);
        talentSearch.validateTalentSearchResultIs(assertion);
      });
    });
  });

  describe('verify talent LHS and RHS details', () => {
    it('verify LHS', () => {
      talentSearch.inputAndClickSearchButton(keywordToReturnUniqueTalent);
      apiJobseeker.getTalentProfileDetails(talentNric).then((details) => {
        talentDetails = details;
        talentSearch.selectTalentByName(talentName).then((talentItem) => {
          shouldHaveCandidateListItemInfo(talentDetails, 'searchTalent')(talentItem);
          talentSearch.validateTalentLabelOnLHS(talentItem);
        });
      });
    });

    it('verify RHS', () => {
      shouldHaveCandidateRightTopPanelInfo(talentDetails);
      talentSearch.validateTalentLabelOnRHS();
      shouldHaveCandidateRightBottomPanelInfo(talentDetails);
    });
  });

  describe('verify talent search results pagination', () => {
    it('should show pagination for search results contains more than 20 and should able to navigate', () => {
      talentSearch.inputAndClickSearchButton('testing');
      talentSearch.validateTalentSearchResultCountIs('exist');
      talentSearch.validateTalentSearchResultIs('exist');

      cy.clickOnPaginationBy('2');
      talentSearch.validateTalentSearchResultIs('exist');

      cy.clickOnPaginationBy('❮');
      talentSearch.validateTalentSearchResultIs('exist');

      cy.clickOnPaginationBy('❯');
      talentSearch.validateTalentSearchResultIs('exist');

      cy.clickOnPaginationBy('1');
      talentSearch.validateTalentSearchResultIs('exist');
    });
  });

  describe('verify talent search resume disable/enable and download in all tabs', () => {
    const resumeButtonStatus = [
      {status: 0, assertion: 'enabled'},
      {status: 1, assertion: 'disabled'},
    ];

    resumeButtonStatus.forEach(({status, assertion}) => {
      it(`should ${assertion} resume button and should ${!status ? '' : 'not'} able to the download`, () => {
        cy.log(`seed data - set is_deleted flag to ${assertion} resume for talent`)
          .task(
            'profileDB',
            `Update jobseeker_documents set is_deleted = '${status}' where is_default = '1' and individual_id = (Select individual_id from jobseekers where nric = '${talentNric}');`,
          )
          .then(() => {
            cy.log('Search a talent and select');
            talentSearch.inputAndClickSearchButton(keywordToReturnUniqueTalent);
            talentSearch.selectTalentByName(talentName);
          })
          .then(() => {
            talentSearch.validateResumeButtonStatusAndDownloadOnEnabledInAllRHSTabs(assertion);
          });
      });
    });
  });
});
