import moment from 'moment';
import {talentSearch} from '../../../support/shared/talentSearch.functions';
import {
  ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY,
  TALENT_SEARCH_INVITE_ALIAS,
  INVITATION_REQUEST_ALIAS,
} from '../../../support/commands/common.commands';
import {userInfos} from '../../../support/users';
import {seedXNewJobsForUEN, seedXCloseJobsForUEN, removeJobsByPostedUen} from '../../../support/manageData/job.seed';

describe('Verify talent search - Invite to apply - end to end flow', () => {
  const loginEmployer = 'Employer talent search';
  const UEN = Cypress._.find(userInfos, ['companyName', loginEmployer]).entityId;
  const talentsNric = ['S4034421G', 'S7407531B'];
  const keywordToSearchTalent = 'CYPRESS_INVITE_TALENT'; // to return some talents based on Education qualification
  let openJobDetails, closeJobDetails;

  before(() => {
    cy.log('seed data - update Education qualification description with unique keyword and active the profile');
    cy.wrap(talentsNric)
      .each((nric) => {
        cy.task(
          'profileDB',
          `Update jobseekers Set last_login = '${moment().format(
            'YYYY-MM-DD HH:mm:ss',
          )}', is_show_profile = '1' where nric = '${nric}';`,
        ).task(
          'profileDB',
          `UPDATE academic_qualifications SET name = '${keywordToSearchTalent}' ,description = '${keywordToSearchTalent}' WHERE individual_id = (SELECT individual_id FROM jobseekers where nric = '${nric}');
      `,
        );
      })
      .then(() => {
        removeJobsByPostedUen(UEN);
      });
    cy.home().login(loginEmployer);
    talentSearch.openTalentSearchPage();
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  describe('Verify Invite to Apply button is disabled and shows correct tooltip when no open jobs for the employer', () => {
    it('Close all existing open jobs for the employer and reload', () => {
      cy.task('jobDB', `UPDATE jobs SET job_status_id = '9' WHERE posted_uen = '${UEN}';`).reload();
    });

    it('should disabled Invite to Apply button and should show corresponding tooltip', () => {
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.clickOnCandidate(0);
      talentSearch.validateInviteToApplyButtonStatusIs('be.disabled');
      talentSearch.validateInviteToApplyToolTipIs(
        'Unable to invite this talent to apply as you have no jobs that are open at the moment.',
      );
    });
  });

  describe('Verify Invite to Apply button is enabled and shows correct tooltip when open jobs for the employer is available', () => {
    it('create two open job for the employer and reload', () => {
      seedXNewJobsForUEN(2, null, UEN)
        .then((response) => {
          openJobDetails = response;
          cy.log(response);
        })
        .then(() => cy.reload());
    });

    it('should enabled Invite to Apply button and should show corresponding tooltip', () => {
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.clickOnCandidate(0);
      talentSearch.validateInviteToApplyButtonStatusIs('be.enabled');
      talentSearch.validateInviteToApplyToolTipIs(
        'Click to send this talent an email with the job description and invitation to apply for the job.',
      );
    });
  });

  describe('Verify Invite to Apply modal popup features, should show open job details, related button status and View job link', () => {
    it('create a closed job for the employer and reload', () => {
      seedXCloseJobsForUEN(1, null, UEN)
        .then((response) => {
          closeJobDetails = response;
        })
        .then(() => cy.reload());
    });

    it('should show modal popup upon clicking Invite to Apply button, should show Open job details only, should not show closed job and View Jobs hyperlink', () => {
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.clickOnCandidate(0);
      talentSearch.clickOnButton('Invite to Apply');
      talentSearch.modalPopup.validateExistence('be.visible');

      // should show open job
      cy.wrap(openJobDetails).each((jobDetail) => {
        cy.log(`${jobDetail.uuid} - ${jobDetail.title} - ${jobDetail.metadata.jobPostId}`);
        talentSearch.modalPopup.shouldShowOpenJobDetailsAndViewJobsLink(
          {uuid: jobDetail.uuid, jobTitle: jobDetail.title, jobPostId: jobDetail.metadata.jobPostId},
          'exist',
        );
      });

      // should not show closed job
      talentSearch.modalPopup.shouldNotShowClosedJobDetails(closeJobDetails[0].metadata.jobPostId, 'not.exist');
    });

    it('should disabled send button, should enabled cancel button and upon clicking Cancel button should close modal popup', () => {
      talentSearch.validateSendButtonIsDisabled();
      talentSearch.clickOnButton('Cancel');
      talentSearch.modalPopup.validateExistence('not.exist');
    });
  });

  describe('Verify Invite to Apply for a Job - success', () => {
    it('should able to invite a talent for a job and should see success message with job details', () => {
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.clickOnCandidate(0);
      talentSearch.clickOnButton('Invite to Apply');
      talentSearch.modalPopup.selectAJob(openJobDetails[0].metadata.jobPostId);
      cy.captureTalentSearchInvite();
      talentSearch.clickOnButton('Send');
      cy.wait(`@${TALENT_SEARCH_INVITE_ALIAS}`);

      talentSearch.modalPopup.validateInvitedJobsInSuccessModalPopupAndClickOk({
        jobTitle: openJobDetails[0].title,
        jobPostId: openJobDetails[0].metadata.jobPostId,
      });

      talentSearch.clickOnButton('Ok');
      talentSearch.modalPopup.validateExistence('not.exist');
    });

    it('should show Invitation Sent message for the jobs already invited and should disabled', () => {
      talentSearch.clickOnButton('Invite to Apply');
      talentSearch.modalPopup.validateInvitationSentMessageShowsForInvitedJob(openJobDetails[0].metadata.jobPostId);
      talentSearch.clickOnButton('Cancel');
    });
  });

  describe('Verify Invite to Apply button is disabled and shows correct tooltip when all jobs are invited for a talent', () => {
    it('should able to invite more than one job for a talent', () => {
      cy.reload();
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.captureInvitationsRequest().clickOnCandidate(1);
      talentSearch.clickOnButton('Invite to Apply');
      cy.wait(`@${INVITATION_REQUEST_ALIAS}`);
      cy.wrap(openJobDetails).each((openJobDetail) => {
        talentSearch.modalPopup.selectAJob(openJobDetail.metadata.jobPostId);
      });
      cy.captureTalentSearchInvite();
      talentSearch.clickOnButton('Send');
      cy.wait(`@${TALENT_SEARCH_INVITE_ALIAS}`);

      cy.wrap(openJobDetails).each((openJobDetail) => {
        talentSearch.modalPopup.validateInvitedJobsInSuccessModalPopupAndClickOk({
          jobTitle: openJobDetail.title,
          jobPostId: openJobDetail.metadata.jobPostId,
        });
      });

      talentSearch.clickOnButton('Ok');
      talentSearch.modalPopup.validateExistence('not.exist');
    });

    it('should disabled Invite to Apply button and should show corresponding tooltip', () => {
      cy.reload();
      talentSearch.inputAndClickSearchButton(keywordToSearchTalent);
      cy.clickOnCandidate(1);
      talentSearch.validateInviteToApplyButtonStatusIs('be.disabled');
      talentSearch.validateInviteToApplyToolTipIs(
        'This talent has already been invited to all jobs that are open at the moment.',
      );
    });
  });

  describe('Verify Invite to Apply when backend returns error', () => {
    before(() => {
      cy.intercept('POST', `**/invitations/batch`, {
        statusCode: 500,
        body: {},
      });
    });

    it('should show error growl while sending an invite', () => {
      cy.clickOnCandidate(0);
      talentSearch.clickOnButton('Invite to Apply');
      talentSearch.modalPopup.selectAJob(openJobDetails[1].metadata.jobPostId);
      talentSearch.clickOnButton('Send');

      cy.verifyNotificationGrowl('Temporarily unable to send invitation to apply. Please try again.');
    });
  });
});
