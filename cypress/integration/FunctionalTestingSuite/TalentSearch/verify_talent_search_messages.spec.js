import {talentSearch} from '../../../support/shared/talentSearch.functions';
import {ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY} from '../../../support/commands/common.commands';

describe('Verify talent search messages', () => {
  before(() => {
    cy.home();
    cy.login('Job Post Testing Account', 'Job Admin');
  });

  afterEach(() => {
    cy.preserveLocalStorageOnce(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    Cypress.Cookies.preserveOnce('access-token');
  });

  after(() => {
    cy.clearLocalStorage(ACCOUNT_EXPIRY_LOCAL_STORAGE_KEY);
    cy.clearCookie('access-token');
  });

  it('should display "Search to view talents" message before search', () => {
    talentSearch.openTalentSearchPage();
    talentSearch.validateTalentSearchResultMessages(['Search to view talents']);
  });

  it('should display "No talents found" message when search returns no result', () => {
    talentSearch.inputAndClickSearchButton('auto1mation');
    talentSearch.validateTalentSearchResultMessages([
      'No talents found.',
      'You may wish to check your spelling or expand your search term.',
    ]);
  });

  it('should display "temporarily unavailable" message when search returns error', () => {
    cy.mockGraphQL('profile', {
      getSearchTalents: {
        response: {},
        statusCode: 504,
      },
    });
    talentSearch.inputAndClickSearchButton('auto1mation');
    talentSearch.validateTalentSearchResultMessages([
      'This information is temporarily unavailable.',
      'Please try again later.',
    ]);
  });
});
