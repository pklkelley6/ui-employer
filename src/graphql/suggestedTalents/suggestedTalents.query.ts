import gql from 'graphql-tag';

export const GET_SUGGESTED_TALENTS = gql`
  query getSuggestedTalents(
    $jobId: String!
    $limit: Int
    $threshold: Float
    $fromLastLogin: String
    $toLastLogin: String
    $page: Int
    $sortBy: [SortBy!]
  ) {
    suggestedTalentsForJob(
      jobId: $jobId
      limit: $limit
      threshold: $threshold
      fromLastLogin: $fromLastLogin
      toLastLogin: $toLastLogin
      page: $page
      sortBy: $sortBy
    ) {
      talents {
        talent {
          email
          name
          id
          lastLogin
          mobileNumber
          education {
            name
            isHighest
            isVerified
            institution
            yearAttained
            ssecEqaCode
            ssecEqaDescription
            ssecFosDescription
          }
          skills {
            uuid
            skill
          }
          workExperiences {
            jobTitle
            companyName
            startDate
            endDate
            jobDescription
          }
          resume {
            fileName
            filePath
          }
        }
        bookmarkedOn
      }
      total
    }
  }
`;

export const GET_SUGGESTED_TALENTS_COUNT = gql`
  query getSuggestedTalentsCount($jobId: String!, $threshold: Float, $fromLastLogin: String) {
    suggestedTalentsForJob(jobId: $jobId, threshold: $threshold, fromLastLogin: $fromLastLogin) {
      total
    }
  }
`;

export const SET_SUGGESTED_TALENT_BOOKMARK = gql`
  mutation setSuggestedTalentBookmark($individualId: String!, $jobId: String!, $isBookmark: Boolean!) {
    setSuggestedTalentBookmark(individualId: $individualId, jobId: $jobId, isBookmark: $isBookmark)
  }
`;
