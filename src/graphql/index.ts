import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import {ApolloLink, from} from 'apollo-link';
import {HttpLink} from 'apollo-link-http';
import {setContext} from 'apollo-link-context';
import {Store} from 'redux';
import {action} from 'typesafe-actions';
import {config} from '~/config';
import introspectionResult from '~/graphql/__generated__/types';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

export const API_PROFILE_VERSION = '2021-07-05';

export const setupClient = (...middleware: ApolloLink[]) => {
  const fragmentMatcher = new IntrospectionFragmentMatcher({introspectionQueryResultData: introspectionResult});
  const cache = new InMemoryCache({fragmentMatcher});

  return new ApolloClient({
    cache,
    link: from([...middleware, apiVersionLink, new HttpLink({uri: config.url.apiProfile, credentials: 'include'})]),
    connectToDevTools: config.environment === 'development',
  });
};

export const activityMiddleware = (store: Store) =>
  new ApolloLink((operation, forward) => {
    // dispatch an action so that redux-account-middleware know that there is still activity on-going and refresh auth token when necessary
    store.dispatch(action('GRAPHQL_ACTIVITY'));
    return forward ? forward(operation) : null;
  });

export const apiVersionLink = setContext((_, {headers}) => {
  return {
    headers: {
      [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
      ...headers,
    },
  };
});
