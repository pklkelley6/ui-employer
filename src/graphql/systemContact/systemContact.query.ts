import gql from 'graphql-tag';

export const GET_SYSTEM_CONTACT = gql`
  query getSystemContact {
    systemContact {
      contactNumber
      email
      designation
      name
      individualId
      termsAndConditionsAcceptedAt
    }
  }
`;

export const SET_SYSTEM_CONTACT = gql`
  mutation UpdateSystemContact($systemContact: SystemContactInput!) {
    updateSystemContact(systemContact: $systemContact) {
      contactNumber
      designation
      email
      name
    }
  }
`;

export const SET_TERMS_AND_CONDITIONS = gql`
  mutation AcceptTermsAndConditions {
    acceptTermsAndConditions
  }
`;

export const GET_JOB_SYSTEM_CONTACT = gql`
  query getJobSystemContact($jobUuid: ID!) {
    jobByUuid(uuid: $jobUuid) {
      emailRecipientContact {
        name
        email
      }
      createdByContact {
        name
        email
      }
    }
  }
`;
