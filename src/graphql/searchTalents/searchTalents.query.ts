import gql from 'graphql-tag';

export const GET_SEARCH_TALENTS = gql`
  query getSearchTalents($keywords: String!, $limit: Int, $skip: Int) {
    searchTalents(keywords: $keywords, limit: $limit, skip: $skip) {
      talents {
        email
        name
        id
        lastLogin
        mobileNumber
        education {
          name
          isHighest
          isVerified
          institution
          yearAttained
          ssecEqaCode
          ssecEqaDescription
          ssecFosDescription
        }
        skills {
          uuid
          skill
        }
        workExperiences {
          jobTitle
          companyName
          startDate
          endDate
          jobDescription
        }
        resume {
          fileName
          filePath
        }
      }
      total
    }
  }
`;
