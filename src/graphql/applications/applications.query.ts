import gql from 'graphql-tag';

export const GET_APPLICATIONS = gql`
  query getApplications(
    $jobId: ID!
    $skip: Int
    $limit: Int
    $bookmark: String
    $sortBy: [SortBy!]
    $filterBy: [FilterBy!]
  ) {
    applicationsForJob(
      jobId: $jobId
      skip: $skip
      limit: $limit
      bookmark: $bookmark
      sortBy: $sortBy
      filterBy: $filterBy
    ) {
      applications {
        job {
          uuid
        }
        applicant {
          email
          name
          id
          mobileNumber
          education {
            name
            isHighest
            isVerified
            institution
            yearAttained
            ssecEqaCode
            ssecEqaDescription
            ssecFosDescription
          }
          skills {
            uuid
            skill
          }
          workExperiences {
            jobTitle
            companyName
            startDate
            endDate
            jobDescription
          }
          resume {
            fileName
            filePath
            lastDownloadedAt
          }
        }
        createdOn
        id
        isViewed
        isShortlisted
        statusId
        bookmarkedOn
        rejectionReason
        rejectionReasonExtended
        scores {
          wcc
        }
        jobScreeningQuestionResponses {
          question
          answer
        }
      }
      total(filterBy: $filterBy)
    }
  }
`;

export const GET_APPLICATIONS_COUNT = gql`
  query getApplicationsCount($jobId: ID!) {
    applicationsForJob(jobId: $jobId) {
      total
      unviewedTotal
    }
  }
`;

export const GET_FILTERED_APPLICATIONS_COUNT = gql`
  query getFilteredApplicationsCount($jobId: ID!, $filterBy: [FilterBy!]) {
    applicationsForJob(jobId: $jobId, filterBy: $filterBy) {
      total(filterBy: $filterBy)
    }
  }
`;

export const SET_APPLICATION_AS_VIEWED = gql`
  mutation setAsViewed($applicationId: String!) {
    setApplicationAsViewed(applicationId: $applicationId) {
      id
      isViewed
    }
  }
`;

/**
 * Combined mutations for updating application parameters isViewed, lastDownloadedAt, isShortlisted, and statusId to 1 http call.
 * Having separate http calls for these mutations at the same time would cause problems in couchDB since it discards subsequent updates
 * to a document if any update is still ongoing.
 */
export const SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD = gql`
  mutation setApplicationAsViewedWithResumeDownload($applicationId: String!) {
    setApplicationAsViewed(applicationId: $applicationId) {
      id
      isViewed
    }
    setResumeLastDownloadedDate(applicationId: $applicationId) {
      id
      isViewed
      applicant {
        id
        resume {
          lastDownloadedAt
        }
      }
    }
  }
`;

export const SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE = gql`
  mutation setAsViewedWithResumeDownloadAndStatusUpdate($applicationId: String!, $statusId: Int!) {
    setApplicationAsViewed(applicationId: $applicationId) {
      id
      isViewed
    }
    setResumeLastDownloadedDate(applicationId: $applicationId) {
      id
      isViewed
      applicant {
        id
        resume {
          lastDownloadedAt
        }
      }
    }
    setApplicationStatus(applicationId: $applicationId, statusId: $statusId) {
      id
      statusId
      rejectionReason
      rejectionReasonExtended
    }
  }
`;

export const SET_RESUME_LAST_DOWNLOADED_DATE = gql`
  mutation setResumeLastDownloadedDate($applicationId: String!) {
    setResumeLastDownloadedDate(applicationId: $applicationId) {
      id
      isViewed
      applicant {
        id
        resume {
          lastDownloadedAt
        }
      }
    }
  }
`;

export const SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE = gql`
  mutation setResumeDownloadAndStatusUpdate($applicationId: String!, $statusId: Int!) {
    setResumeLastDownloadedDate(applicationId: $applicationId) {
      id
      isViewed
      applicant {
        id
        resume {
          lastDownloadedAt
        }
      }
    }
    setApplicationStatus(applicationId: $applicationId, statusId: $statusId) {
      id
      statusId
      rejectionReason
      rejectionReasonExtended
    }
  }
`;

export const SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE = gql`
  mutation setApplicationShortlistAndStatusUpdate($applicationId: String!, $statusId: Int!, $isShortlisted: Boolean!) {
    setApplicationShortlistState(applicationId: $applicationId, isShortlisted: $isShortlisted) {
      id
      isShortlisted
    }
    setApplicationStatus(applicationId: $applicationId, statusId: $statusId) {
      id
      statusId
      rejectionReason
      rejectionReasonExtended
    }
  }
`;

export const SET_APPLICATION_BOOKMARK = gql`
  mutation setApplicationBookmark($applicationId: String!, $isBookmark: Boolean!) {
    setApplicationBookmarkedOn(applicationId: $applicationId, isBookmark: $isBookmark) {
      id
      bookmarkedOn
    }
  }
`;

export const GET_APPLICATIONS_WITH_STATUS_COUNT_BY_JOB = gql`
  query getApplicationsWithStatusCountByJob($jobId: ID!) {
    applicationsWithStatusCountByJob(jobId: $jobId) {
      count
      statusId
    }
  }
`;

export const SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE = gql`
  mutation setMassApplicationShortlistAndStatusUpdate(
    $applicationIds: [String!]!
    $statusId: Int!
    $isShortlisted: Boolean!
  ) {
    setMassApplicationShortlistAndStatusUpdate(
      applicationIds: $applicationIds
      statusId: $statusId
      isShortlisted: $isShortlisted
    ) {
      id
      ok
    }
  }
`;

export const SET_APPLICATION_STATUS_BY_JOB = gql`
  mutation setApplicationStatusByJob($jobId: String!, $fromStatusIds: [Int!]!, $toStatusId: Int!) {
    setApplicationStatusByJob(jobId: $jobId, fromStatusIds: $fromStatusIds, toStatusId: $toStatusId) {
      id
      ok
    }
  }
`;

export const APPLICATION_STATUS_FRAGMENT = gql`
  fragment ApplicationStatusFragment on Application {
    statusId
  }
`;

export const SET_APPLICATION_REJECTION_REASON = gql`
  mutation setApplicationRejectionReason(
    $applicationId: String!
    $rejectionReason: String!
    $rejectionReasonExtended: String
  ) {
    setApplicationRejectionReason(
      applicationId: $applicationId
      rejectionReason: $rejectionReason
      rejectionReasonExtended: $rejectionReasonExtended
    ) {
      id
      rejectionReason
      rejectionReasonExtended
    }
  }
`;
