import gql from 'graphql-tag';

export const GET_BOOKMARKED_CANDIDATES_COUNT = gql`
  query getBookmarkedCandidatesCount($jobId: String!) {
    bookmarkedCandidatesForJob(jobId: $jobId) {
      total
    }
  }
`;

export const GET_BOOKMARKED_CANDIDATES = gql`
  query getBookmarkedCandidates(
    $jobId: String!
    $skip: Int
    $limit: Int
    $bookmark: String
    $candidateType: CandidateType
  ) {
    bookmarkedCandidatesForJob(
      jobId: $jobId
      skip: $skip
      limit: $limit
      bookmark: $bookmark
      candidateType: $candidateType
    ) {
      __typename
      total
      bookmark
      bookmarkedCandidates {
        __typename
        ... on Talent {
          __typename
          id
          bookmarkedOn
          talent {
            __typename
            email
            name
            id
            mobileNumber
            lastLogin
            education {
              __typename
              name
              isHighest
              isVerified
              institution
              yearAttained
              ssecEqaCode
              ssecEqaDescription
              ssecFosDescription
            }
            skills {
              __typename
              uuid
              skill
            }
            workExperiences {
              __typename
              jobTitle
              companyName
              startDate
              endDate
              jobDescription
            }
            resume {
              __typename
              fileName
              filePath
              lastDownloadedAt
            }
          }
        }
        ... on Application {
          __typename
          id
          bookmarkedOn
          isShortlisted
          statusId
          createdOn
          isViewed
          rejectionReason
          rejectionReasonExtended
          jobScreeningQuestionResponses {
            __typename
            question
            answer
          }
          applicant {
            __typename
            email
            name
            id
            mobileNumber
            education {
              __typename
              name
              isHighest
              isVerified
              institution
              yearAttained
              ssecEqaCode
              ssecEqaDescription
              ssecFosDescription
            }
            skills {
              __typename
              uuid
              skill
            }
            workExperiences {
              __typename
              jobTitle
              companyName
              startDate
              endDate
              jobDescription
            }
            resume {
              __typename
              fileName
              filePath
              lastDownloadedAt
            }
          }
        }
        ... on HiddenTalent {
          __typename
          id
          bookmarkedOn
          talent {
            __typename
            name
          }
        }
      }
    }
  }
`;
