import {
  GetApplicationsApplicant,
  GetApplicationsEducation,
  GetApplicationsResume,
  GetApplicationsSkills,
  GetApplicationsWorkExperiences,
  GetSuggestedTalentsEducation,
  GetSuggestedTalentsResume,
  GetSuggestedTalentsSkills,
  GetSuggestedTalentsTalent,
  GetSuggestedTalentsWorkExperiences,
  GetApplicationsJobScreeningQuestionResponses,
  Maybe,
} from '~/graphql/__generated__/types';
import {isNotNil} from '~/util/isNotNil';

export type ICandidate = GetSuggestedTalentsTalent | GetApplicationsApplicant;
export type IEducation = GetSuggestedTalentsEducation | GetApplicationsEducation;
export type ISkill = GetSuggestedTalentsSkills | GetApplicationsSkills;
export type IWorkExperience = GetSuggestedTalentsWorkExperiences | GetApplicationsWorkExperiences;
export type IResume = GetApplicationsResume | GetSuggestedTalentsResume;
export type IJobScreeningQuestionResponse = GetApplicationsJobScreeningQuestionResponses;

export const isApplicationResume = (resume: Maybe<IResume>): resume is GetApplicationsResume => {
  return isNotNil(resume) && isNotNil((resume as GetApplicationsResume).lastDownloadedAt);
};
