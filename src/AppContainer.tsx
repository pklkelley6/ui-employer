import {ConnectedRouter} from 'connected-react-router';
import React, {StrictMode} from 'react';
import {ApolloProvider} from 'react-apollo';
import {hot} from 'react-hot-loader/root';
import {Provider} from 'react-redux';
import {Route, Switch} from 'react-router';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Store} from 'redux';
import {PageUnavailable} from './pages/PageUnavailable/PageUnavailable';
import {ERROR_MSG_404} from './pages/PageUnavailable/PageUnavailableErrors.constants';
import {FeatureFlag} from './components/Core/ToggleFlag';
import {Footer} from '~/components/Layouts/Footer';
import {NotificationBannerContainer} from '~/components/NotificationBanner/NotificationBannerContainer';
import {MainNavigationContainer} from '~/components/Navigation/MainNavigation';
import {MainSideBar} from '~/components/Navigation/MainSideBar/MainSideBar';
import {PrivateRoute} from '~/components/Route';
import {IAppState} from '~/flux';
import {activityMiddleware, setupClient} from '~/graphql';
import {history} from '~/history';
import {MaintenanceBranchContainer} from '~/pages/Maintenance/MaintenanceBranchContainer';
import {privateRoutes, publicRoutes} from '~/router';
import cssVarPolyfill from '~/util/css/cssVarPolyfill';

interface IProps {
  store: Store<IAppState>;
}
class AppContainer extends React.Component<IProps> {
  public componentDidMount() {
    cssVarPolyfill.init();
  }

  public render() {
    const client = setupClient(activityMiddleware(this.props.store));
    toast.configure();
    return (
      <ApolloProvider client={client}>
        <Provider store={this.props.store}>
          <MaintenanceBranchContainer>
            <ConnectedRouter history={history}>
              <StrictMode>
                <Route component={MainNavigationContainer} />
                <Route component={NotificationBannerContainer} />
                <div className="flex">
                  <Route path="/:page(jobs|talent-search|company-profile|account-info)" component={MainSideBar} />
                  <Switch>
                    {publicRoutes.map((route, id) => (
                      <Route key={id} {...route} />
                    ))}
                    {privateRoutes.map((route, id) => (
                      <PrivateRoute key={id} {...route} />
                    ))}
                    <FeatureFlag
                      name="newErrorPage"
                      render={() => <Route render={() => <PageUnavailable subtitle={ERROR_MSG_404} />} />}
                    />
                  </Switch>
                </div>
                <Route
                  path="/:page(jobs|terms-of-use|terms-and-conditions|corppass-help)"
                  exact={true}
                  component={Footer}
                />
              </StrictMode>
            </ConnectedRouter>
          </MaintenanceBranchContainer>
        </Provider>
      </ApolloProvider>
    );
  }
}

export default hot(AppContainer);
