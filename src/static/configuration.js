window.urlConfig = {
  featureToggles: 'https://static.mycareersfuture.gov.sg/features/employer/latest.json',
  corppass: 'http://localhost:8001/cp',
  apiJob: {
    v1: 'http://localhost:9999',
    v2: 'http://localhost:9999/v2',
  },
  apiProfile: 'http://localhost:9999/profile',
  notification: 'https://static.mycareersfuture.gov.sg/notification/development.json',
  mcf: 'http://localhost:3000',
  apiVirusScanner: 'https://api-virus-scanner.ci.mcf.sh',
};

window.metaConfig = {
  environment: 'development',
  gaTrackingCode: 'UA-129908741-4',
  wogaaScriptUrl: 'https://assets.dcube.cloud/scripts/wogaa.js',
  gtmContainerId: '',
};
