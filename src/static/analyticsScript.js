/* eslint-disable */
// google analytics
ga('create', metaConfig.gaTrackingCode, 'auto');

// wogaa
(function(i, s, o, g, a, m) {
  (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
  a.src = g;
  m.parentNode.insertBefore(a, m);
})(window, document, 'script', metaConfig.wogaaScriptUrl);

// google tag manager
(function(w, d, s, l, i) {
  if (i) {
    w[l] = w[l] || [];
    w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
    var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl= l != 'dataLayer' ? '&l='+l : '';
    j.async = true;
    j.src = 'https://www.googletagmanager.com/gtm.js?id='+i+dl;
    f.parentNode.insertBefore(j, f);
  }
})(window, document, 'script', 'dataLayer', metaConfig.gtmContainerId);
