import {all, call, put, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {fetchJobTitlesFailed, fetchJobTitlesRequested, fetchJobTitlesSucceeded} from './jobTitles.actions';
import {JOB_TITLES_REQUESTED} from './jobTitles.constants';
import {getJobTitles} from '~/services/jobTitles/getJobTitles';

export function* fetchJobTitles({payload}: ActionType<typeof fetchJobTitlesRequested>) {
  try {
    const jobTitles: SagaReturnType<typeof getJobTitles> = yield call(getJobTitles, payload);
    yield put(fetchJobTitlesSucceeded(jobTitles));
  } catch (_) {
    yield put(fetchJobTitlesFailed());
  }
}

export function* jobTitlesSaga() {
  yield all([takeLatest(JOB_TITLES_REQUESTED, fetchJobTitles)]);
}
