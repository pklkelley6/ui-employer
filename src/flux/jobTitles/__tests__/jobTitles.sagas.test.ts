import {testSaga} from 'redux-saga-test-plan';
import {jobTitlesMock} from './../__mocks__/jobTitles.mock';
import {fetchJobTitlesRequested, fetchJobTitlesSucceeded} from '~/flux/jobTitles/jobTitles.actions';
import {fetchJobTitles} from '~/flux/jobTitles/jobTitles.sagas';
import {getJobTitles} from '~/services/jobTitles/getJobTitles';

describe('jobTitles sagas', () => {
  it('should fetchTitles', () => {
    const query = 'acc';
    testSaga(fetchJobTitles, fetchJobTitlesRequested(query))
      .next()
      .call(getJobTitles, query)
      .next(jobTitlesMock)
      .put(fetchJobTitlesSucceeded(jobTitlesMock))
      .next()
      .isDone();
  });
});
