import {IFeatureToggles} from './feature.reducer';
import {IAppState} from '~/flux';

export const getFeatureFlag = (name: keyof IFeatureToggles) => (state: IAppState) => state.features[name];
