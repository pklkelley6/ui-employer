import {action} from 'typesafe-actions';

export const UPDATE_FEATURE_TOGGLES = 'UPDATE_FEATURE_TOGGLES';
export const updateFeatureToggles = (featureToggles: Record<string, boolean>) =>
  action(UPDATE_FEATURE_TOGGLES, featureToggles);
