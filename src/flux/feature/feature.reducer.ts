import {ActionType} from 'typesafe-actions';
import * as actions from './feature.actions';

const initialState = {
  applicationStatusInFilterModal: false,
  screeningQuestions: false,
  x0paSuggestedTalents: false,
  x0paSuggestedSkills: false,
  newErrorPage: false,
  enhancedSkillPills: false,
};
export type IFeatureToggles = typeof initialState;

export type FeatureActionType = ActionType<typeof actions>;

export const features = (state: IFeatureToggles = initialState, action: FeatureActionType): IFeatureToggles => {
  switch (action.type) {
    case actions.UPDATE_FEATURE_TOGGLES:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
