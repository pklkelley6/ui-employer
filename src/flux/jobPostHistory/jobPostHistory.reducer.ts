import {ActionType} from 'typesafe-actions';
import * as actions from './jobPostHistory.actions';
import {
  JOB_POST_HISTORY_FETCH_FAILED,
  JOB_POST_HISTORY_FETCH_REQUESTED,
  JOB_POST_HISTORY_FETCH_SUCCEEDED,
} from './jobPostHistory.constants';
import {IJobPostHistoryItem} from '~/services/employer/jobs.types';

export interface IJobPostHistoryState {
  jobPostHistory: {
    [uuid: string]:
      | {
          fetchStatus?:
            | typeof JOB_POST_HISTORY_FETCH_REQUESTED
            | typeof JOB_POST_HISTORY_FETCH_SUCCEEDED
            | typeof JOB_POST_HISTORY_FETCH_FAILED;
          jobPostHistory?: Array<IJobPostHistoryItem>;
        }
      | undefined;
  };
}

const initialState: IJobPostHistoryState = {
  jobPostHistory: {},
};

export const jobPostHistory = (
  state: IJobPostHistoryState = initialState,
  action: JobPostHistoryActionType,
): IJobPostHistoryState => {
  switch (action.type) {
    case JOB_POST_HISTORY_FETCH_SUCCEEDED:
      return {
        ...state,
        jobPostHistory: {
          ...state.jobPostHistory,
          [action.payload.uuid]: {
            fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
            jobPostHistory: action.payload.history,
          },
        },
      };
    case JOB_POST_HISTORY_FETCH_REQUESTED:
    case JOB_POST_HISTORY_FETCH_FAILED:
      return {
        ...state,
        jobPostHistory: {
          ...state.jobPostHistory,
          [action.payload]: {
            fetchStatus: action.type,
          },
        },
      };
    default:
      return state;
  }
};

export type JobPostHistoryActionType = ActionType<typeof actions>;
