import {expectSaga, testSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {throwError} from 'redux-saga-test-plan/providers';
import {
  fetchJobPostHistoryRequested,
  fetchJobPostHistorySucceeded,
  fetchJobPostHistoryFailed,
} from '../jobPostHistory.actions';
import {fetchJobPostHistory} from '../jobPostHistory.sagas';
import {getJobPostHistory} from '~/services/employer/jobs';
import {ACTION_TYPES} from '~/services/employer/jobs.types';

describe('jobPostHistory sagas', () => {
  describe('fetchJobPostHistory', () => {
    it('calls getJobPostHistory service and yields the correct action', () => {
      const jobPostHistoryMock = [
        {
          id: '999',
          jobPostUpdatedAt: '2020-10-23T00:00:00.000Z',
          jobPostExpiryDate: '2020-10-30',
          actionType: ACTION_TYPES.CREATE,
        },
      ];
      testSaga(fetchJobPostHistory, fetchJobPostHistoryRequested('uuid'))
        .next()
        .call(getJobPostHistory, 'uuid')
        .next(jobPostHistoryMock)
        .put(fetchJobPostHistorySucceeded('uuid', jobPostHistoryMock))
        .next()
        .isDone();
    });

    it('should handle errors', () => {
      expectSaga(fetchJobPostHistory, fetchJobPostHistoryRequested('uuid'))
        .provide([[matchers.call.fn(getJobPostHistory), throwError(new Error('randomError'))]])
        .put(fetchJobPostHistoryFailed('uuid'))
        .run();
    });
  });
});
