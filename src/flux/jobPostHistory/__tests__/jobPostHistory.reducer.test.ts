import {
  fetchJobPostHistoryRequested,
  fetchJobPostHistorySucceeded,
  fetchJobPostHistoryFailed,
} from '../jobPostHistory.actions';
import {
  JOB_POST_HISTORY_FETCH_REQUESTED,
  JOB_POST_HISTORY_FETCH_SUCCEEDED,
  JOB_POST_HISTORY_FETCH_FAILED,
} from '../jobPostHistory.constants';
import {jobPostHistory, IJobPostHistoryState} from '../jobPostHistory.reducer';
import {ACTION_TYPES, IJobPostHistoryItem} from '~/services/employer/jobs.types';

describe('jobPostHistory reducer', () => {
  const initialState: IJobPostHistoryState = {jobPostHistory: {}};

  describe('JOB_POST_HISTORY_FETCH_REQUESTED', () => {
    it('should set jobPostHistory fetch status to JOB_POST_HISTORY_FETCH_REQUESTED for job uuid', () => {
      expect(jobPostHistory(initialState, fetchJobPostHistoryRequested('123'))).toEqual({
        ...initialState,
        jobPostHistory: {
          123: {
            fetchStatus: JOB_POST_HISTORY_FETCH_REQUESTED,
          },
        },
      });
    });
  });

  describe('JOB_POST_HISTORY_FETCH_SUCCEEDED', () => {
    it('should set job post history for job uuid', () => {
      const jobPostHistoryItems: Array<IJobPostHistoryItem> = [
        {
          jobPostUpdatedAt: '2020-10-23T00:00:00.000Z',
          jobPostExpiryDate: '2020-10-30',
          actionType: ACTION_TYPES.CREATE,
        },
      ];

      expect(jobPostHistory(initialState, fetchJobPostHistorySucceeded('123', jobPostHistoryItems))).toEqual({
        ...initialState,
        jobPostHistory: {
          123: {
            fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
            jobPostHistory: jobPostHistoryItems,
          },
        },
      });
    });
  });

  describe('JOB_POST_HISTORY_FETCH_FAILED', () => {
    it('should set job post history fetch status to JOB_POST_HISTORY_FETCH_FAILED for job uuid', () => {
      expect(jobPostHistory(initialState, fetchJobPostHistoryFailed('123'))).toEqual({
        ...initialState,
        jobPostHistory: {
          123: {
            fetchStatus: JOB_POST_HISTORY_FETCH_FAILED,
          },
        },
      });
    });
  });
});
