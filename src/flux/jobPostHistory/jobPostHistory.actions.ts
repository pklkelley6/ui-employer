import {action} from 'typesafe-actions';
import {
  JOB_POST_HISTORY_FETCH_SUCCEEDED,
  JOB_POST_HISTORY_FETCH_FAILED,
  JOB_POST_HISTORY_FETCH_REQUESTED,
} from '~/flux/jobPostHistory/jobPostHistory.constants';

import {IJobPostHistoryItem} from '~/services/employer/jobs.types';

export const fetchJobPostHistoryRequested = (uuid: string) => action(JOB_POST_HISTORY_FETCH_REQUESTED, uuid);
export const fetchJobPostHistorySucceeded = (uuid: string, history: Array<IJobPostHistoryItem>) =>
  action(JOB_POST_HISTORY_FETCH_SUCCEEDED, {uuid, history});
export const fetchJobPostHistoryFailed = (uuid: string) => action(JOB_POST_HISTORY_FETCH_FAILED, uuid);
