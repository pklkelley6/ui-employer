import {all, call, put, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {
  fetchJobPostHistoryRequested,
  fetchJobPostHistorySucceeded,
  fetchJobPostHistoryFailed,
} from './jobPostHistory.actions';
import {JOB_POST_HISTORY_FETCH_REQUESTED} from './jobPostHistory.constants';
import {getJobPostHistory} from '~/services/employer/jobs';

export function* fetchJobPostHistory({payload: uuid}: ActionType<typeof fetchJobPostHistoryRequested>) {
  try {
    const jobPostHistory: SagaReturnType<typeof getJobPostHistory> = yield call(getJobPostHistory, uuid);
    yield put(fetchJobPostHistorySucceeded(uuid, jobPostHistory));
  } catch (_) {
    yield put(fetchJobPostHistoryFailed(uuid));
  }
}

export function* jobPostHistorySaga() {
  yield all([takeLatest(JOB_POST_HISTORY_FETCH_REQUESTED, fetchJobPostHistory)]);
}
