import {
  accountReceived,
  accountReducer,
  accountRequestFailed,
  createAccountMiddleware,
  IAccountState,
} from '@govtechsg/redux-account-middleware';
import {connectRouter, routerMiddleware, RouterState} from 'connected-react-router';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {systemContactRequested} from './systemContact/systemContact.actions';
import {IJobPostHistoryState} from './jobPostHistory/jobPostHistory.reducer';
import {isCorppass, IUser} from '~/flux/account';
import {ICompanyState} from '~/flux/company';
import {IFeatureToggles, updateFeatureToggles} from '~/flux/feature';
import * as reducers from '~/flux/index.reducers';
import {rootSaga} from '~/flux/index.sagas';
import {IJobPostingState} from '~/flux/jobPosting';
import {IJobPostingsState} from '~/flux/jobPostings';
import {IJobTitlesResponse} from '~/flux/jobTitles/jobTitles.reducer';
import {INotificationState, notificationFetchRequested} from '~/flux/notification';
import {ISuggestedTalentsState} from '~/flux/suggestedTalents/suggestedTalents.reducer';
import {ISurveyState} from '~/flux/survey/survey.reducer';
import {ISystemContactState} from '~/flux/systemContact/systemContact.reducers';
import {IReleaseToggles, updateReleaseToggles} from '~/flux/releaseToggles';
import {history} from '~/history';
import {getAccount, logout, refreshAccountToken} from '~/services/account';

export interface IAppState {
  readonly features: IFeatureToggles;
  readonly router: RouterState;
  readonly jobPostings: IJobPostingsState;
  readonly jobPosting: IJobPostingState;
  readonly jobTitles: IJobTitlesResponse;
  readonly account: IAccountState<IUser>;
  readonly company: ICompanyState;
  readonly notification: INotificationState;
  readonly survey: ISurveyState;
  readonly suggestedTalents: ISuggestedTalentsState;
  readonly user: ISystemContactState;
  readonly releaseToggles: IReleaseToggles;
  readonly jobPostHistory: IJobPostHistoryState;
}

const appReducer = combineReducers<IAppState>({
  account: accountReducer,
  router: connectRouter(history),
  ...reducers,
});

const sagaMiddleware = createSagaMiddleware();
const accountMiddleware = createAccountMiddleware({
  fetchAccountRequest: getAccount,
  localStorageKey: 'mcf-employer-account-expiry',
  logoutRequest: logout,
  refreshTokenRequest: refreshAccountToken,
  tokenExpiryConfirmMessage:
    'You will soon be logged out because you have been inactive for too long. \n\nClick OK to stay logged in.',
});
const middleware = [routerMiddleware(history), sagaMiddleware, accountMiddleware];

// pass an optional param to rehydrate state on app start
function configureStore(initialState?: Record<string, unknown>) {
  // TypeScript definitions for devtools in /my-globals/index.d.ts
  // Redux devtools are still enabled in production!
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        actionsBlacklist: [],
      })
    : compose;
  // create store
  return createStore(appReducer, initialState, composeEnhancers(applyMiddleware(...middleware)));
}

export const createAndInitStore = async (
  featureToggles: Record<string, boolean>,
  releaseToggles: Record<string, boolean>,
) => {
  const store = configureStore();
  store.dispatch(updateFeatureToggles(featureToggles));
  store.dispatch(updateReleaseToggles(releaseToggles));
  // update feature flags before running saga as some sagas depends on feature flags
  sagaMiddleware.run(rootSaga);
  // fetch notifications after starting sagas
  store.dispatch(notificationFetchRequested());
  try {
    const account = await getAccount();
    store.dispatch(accountReceived(account));

    if (isCorppass(account)) {
      store.dispatch(systemContactRequested());
    }
  } catch (e) {
    if (e instanceof Error) {
      store.dispatch(accountRequestFailed(e));
    }
  }
  return store;
};
