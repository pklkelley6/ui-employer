import {action} from 'typesafe-actions';
import {
  JOB_POSTING_FETCH_FAILED,
  JOB_POSTING_FETCH_REQUESTED,
  JOB_POSTING_FETCH_SUCCEEDED,
  START_JOB_POSTING,
  COMPLETE_JOB_POSTING,
} from './jobPosting.constants';
import {IJobPost} from '~/services/employer/jobs.types';

export const jobPostingFetchRequested = (uuid: string) => action(JOB_POSTING_FETCH_REQUESTED, uuid);
export const jobPostingFetchSucceeded = (posting: IJobPost) => action(JOB_POSTING_FETCH_SUCCEEDED, posting);
export const jobPostingFetchFailed = (uuid: string) => action(JOB_POSTING_FETCH_FAILED, uuid);
export const startJobPosting = () => action(START_JOB_POSTING);
export const completeJobPosting = () => action(COMPLETE_JOB_POSTING);
