import {expectSaga, testSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {throwError} from 'redux-saga-test-plan/providers';
import {jobPostingFetchFailed, jobPostingFetchRequested, jobPostingFetchSucceeded} from '../jobPosting.actions';
import {fetchJobPosting} from '../jobPosting.sagas';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {getJobPosting} from '~/services/employer/jobs';

describe('jobPosting sagas', () => {
  describe('fetchJobPosting', () => {
    it('should fetch job postings by job uuid', () => {
      const uuidMock = 'uuid123';
      testSaga(fetchJobPosting, jobPostingFetchRequested(uuidMock))
        .next()
        .call(getJobPosting, uuidMock)
        .next(jobPostMock)
        .put(jobPostingFetchSucceeded(jobPostMock))
        .next()
        .isDone();
    });

    it('should handle error', () => {
      const uuidMock = 'uuid123';
      const error = new Error('error');

      return expectSaga(fetchJobPosting, jobPostingFetchRequested(uuidMock))
        .provide([[matchers.call.fn(getJobPosting), throwError(error)]])
        .put(jobPostingFetchFailed(uuidMock))
        .run();
    });
  });
});
