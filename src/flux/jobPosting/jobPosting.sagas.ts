import {all, call, put, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {jobPostingFetchFailed, jobPostingFetchRequested, jobPostingFetchSucceeded} from './jobPosting.actions';
import {JOB_POSTING_FETCH_REQUESTED} from './jobPosting.constants';
import {getJobPosting} from '~/services/employer/jobs';

export function* fetchJobPosting({payload: uuid}: ActionType<typeof jobPostingFetchRequested>) {
  try {
    const job: SagaReturnType<typeof getJobPosting> = yield call(getJobPosting, uuid);
    yield put(jobPostingFetchSucceeded(job));
  } catch (_) {
    yield put(jobPostingFetchFailed(uuid));
  }
}

export function* jobPostingSaga() {
  yield all([takeLatest(JOB_POSTING_FETCH_REQUESTED, fetchJobPosting)]);
}
