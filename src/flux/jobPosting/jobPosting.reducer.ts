import {ActionType} from 'typesafe-actions';
import * as actions from './jobPosting.actions';
import {
  JOB_POSTING_FETCH_FAILED,
  JOB_POSTING_FETCH_REQUESTED,
  JOB_POSTING_FETCH_SUCCEEDED,
} from './jobPosting.constants';
import {IJobPost} from '~/services/employer/jobs.types';

export interface IJobPostingState {
  jobPostings: {
    [uuid: string]:
      | {
          fetchStatus?:
            | typeof JOB_POSTING_FETCH_REQUESTED
            | typeof JOB_POSTING_FETCH_SUCCEEDED
            | typeof JOB_POSTING_FETCH_FAILED;
          jobPost?: IJobPost;
        }
      | undefined;
  };
}

const initialState: IJobPostingState = {
  jobPostings: {},
};

export const jobPosting = (state: IJobPostingState = initialState, action: JobPostingActionType): IJobPostingState => {
  switch (action.type) {
    case JOB_POSTING_FETCH_SUCCEEDED:
      return {
        ...state,
        jobPostings: {
          ...state.jobPostings,
          [action.payload.uuid]: {
            fetchStatus: action.type,
            jobPost: action.payload,
          },
        },
      };
    case JOB_POSTING_FETCH_REQUESTED:
    case JOB_POSTING_FETCH_FAILED:
      return {
        ...state,
        jobPostings: {
          ...state.jobPostings,
          [action.payload]: {
            fetchStatus: action.type,
          },
        },
      };
    default:
      return state;
  }
};

export type JobPostingActionType = ActionType<typeof actions>;
