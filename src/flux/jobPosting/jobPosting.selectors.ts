import {fromUnixTime} from 'date-fns';
import {formatToTimeZone} from 'date-fns-timezone';
import {IAppState} from '../index';

export const getMcfSystemDate = ({account: {data}}: IAppState) =>
  data &&
  formatToTimeZone(fromUnixTime(data?.exp), 'YYYY-MM-DD', {
    timeZone: 'Asia/Singapore',
  });
