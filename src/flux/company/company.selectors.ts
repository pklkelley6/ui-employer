import {IAccountState} from '@govtechsg/redux-account-middleware';
import {get} from 'lodash/fp';
import {ICompanyState} from './index';
import {IUser} from '~/flux/account';
import {CompanyAddressType} from '~/services/employer/company';

export const getCompanyName = (company: ICompanyState, account: IAccountState<IUser>) => {
  const user = account.data;
  const companyInfo = get('companyInfo', company);
  return companyInfo.name || (user && (user.userInfo.entityName || user.userInfo.entityId)) || '';
};

export const getCompanyLogo = (company: ICompanyState) => company.companyInfo.logoUploadPath || '';

export const getCompanyUen = (company: ICompanyState) => company.companyInfo.uen || '';

export const getCompanyDescription = (company: ICompanyState) => company.companyInfo.description || '';

export const getCompanyUrl = (company: ICompanyState) => company.companyInfo.companyUrl;

export const getCompanyEmployeeCount = (company: ICompanyState) => company.companyInfo.employeeCount || undefined;

export const getCompanyAddresses = (company: ICompanyState) => {
  return company.companyInfo.addresses;
};

export const getCompanyOperatingAddress = (company: ICompanyState) => {
  return getCompanyAddresses(company).find((address) => address.purpose === CompanyAddressType.OPERATING);
};

export const getCompanySsic = (company: ICompanyState) => company.companyInfo.ssicCode;
