import {expectSaga, testSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {throwError} from 'redux-saga-test-plan/providers';
import {getAccountUser} from '../../account';
import {companyFetchFailed, companyFetchSucceeded} from '../company.actions';
import {fetchCompanyInfo} from '../company.sagas';
import {getCompanyInfo} from '~/services/employer/getCompanyInfo';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

describe('company sagas', () => {
  describe('fetchCompanyInfo', () => {
    it('calls getCompanyInfo service and yields the correct action', () => {
      const companyMock = {
        addresses: [],
        description: '',
        logoFileName: '',
        logoUploadPath: '',
        name: '',
        uen: 'T08GB0046G',
      };
      testSaga(fetchCompanyInfo)
        .next()
        .select(getAccountUser)
        .next(uenUserMock)
        .call(getCompanyInfo, uenUserMock.userInfo.entityId)
        .next(companyMock)
        .put(companyFetchSucceeded(companyMock))
        .next()
        .isDone();
    });

    it('raise an error', () => {
      const error = new Error('error');

      return expectSaga(fetchCompanyInfo)
        .provide([[matchers.call.fn(getCompanyInfo), throwError(error)]])
        .put(companyFetchFailed())
        .run();
    });
  });
});
