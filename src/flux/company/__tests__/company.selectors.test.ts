import {getCompanyLogo, getCompanyName} from '../company.selectors';
import {nonUenUserMock, uenUserMock} from '~/__mocks__/account/account.mocks';
import {companyInfoMock} from '~/__mocks__/company/company.mocks';

describe('company selectors', () => {
  describe('getCompanyLogo()', () => {
    it('should return companyLogo', () => {
      const state = {
        company: {
          companyInfo: {
            ...companyInfoMock,
            logoUploadPath: 'http://image.com/image.png',
          },
          fetchStatus: '',
        },
      };
      const {company} = state;
      const companyLogo = getCompanyLogo(company);
      expect(companyLogo).toBe(state.company.companyInfo.logoUploadPath);
    });
  });

  describe('getCompanyName()', () => {
    it('should return company name if company name is available', () => {
      const state = {
        account: {
          data: uenUserMock,
        },
        company: {
          companyInfo: {
            ...companyInfoMock,
            name: 'CRAZY RICH ASIANS PTE LTD',
          },
          fetchStatus: '',
        },
      };
      const {company, account} = state;
      const companyName = getCompanyName(company, account);
      expect(companyName).toBe(state.company.companyInfo.name);
    });

    it('should return entity id if company name is unavailable', () => {
      const state = {
        account: {
          data: uenUserMock,
        },
        company: {
          companyInfo: {
            ...companyInfoMock,
            name: '',
          },
          fetchStatus: '',
        },
      };
      const {company, account} = state;
      const companyName = getCompanyName(company, account);
      expect(companyName).toBe(account.data.userInfo.entityId);
    });

    it('should return entityName if it is NON-UEN registered and name is available', () => {
      const state = {
        account: {
          data: nonUenUserMock,
        },
        company: {
          companyInfo: {
            ...companyInfoMock,
            name: '',
          },
          fetchStatus: '',
        },
      };
      const {company, account} = state;
      const companyName = getCompanyName(company, account);
      expect(companyName).toBe(account.data.userInfo.entityName);
    });
  });
});
