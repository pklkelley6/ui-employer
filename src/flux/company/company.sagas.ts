import {all, call, put, select, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {getAccountUser} from '~/flux/account';
import {COMPANY_FETCH_REQUESTED, companyFetchFailed, companyFetchSucceeded} from '~/flux/company';
import {getCompanyInfo} from '~/services/employer/getCompanyInfo';

export function* fetchCompanyInfo() {
  try {
    const user: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);
    const companyInfo: SagaReturnType<typeof getCompanyInfo> = yield call(getCompanyInfo, user?.userInfo.entityId);
    yield put(companyFetchSucceeded(companyInfo));
  } catch (e) {
    yield put(companyFetchFailed());
  }
}

export function* companySaga() {
  yield all([takeLatest(COMPANY_FETCH_REQUESTED, fetchCompanyInfo)]);
}
