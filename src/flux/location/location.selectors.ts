import {IAppState} from '~/flux';

export const getLocation = (state: IAppState) => state.router.location;
