import {action} from 'typesafe-actions';
import {
  ISystemContact,
  SYSTEM_CONTACT_FAILED,
  SYSTEM_CONTACT_REQUESTED,
  SYSTEM_CONTACT_SUCCEEDED,
  UPDATE_SYSTEM_CONTACT_FAILED,
  UPDATE_SYSTEM_CONTACT_REQUESTED,
  UPDATE_SYSTEM_CONTACT_SUCCEEDED,
  UPDATE_TERMS_AND_CONDITIONS_FAILED,
  UPDATE_TERMS_AND_CONDITIONS_REQUESTED,
  UPDATE_TERMS_AND_CONDITIONS_SUCCEEDED,
  UPDATE_SYSTEM_CONTACT_AND_TERMS_AND_CONDITIONS_REQUESTED,
} from '~/flux/systemContact/systemContact.constants';
import {AcceptTermsAndConditionsMutation, SystemContact} from '~/graphql/__generated__/types';

export const systemContactRequested = () => action(SYSTEM_CONTACT_REQUESTED);
export const systemContactSucceeded = (payload: Partial<SystemContact>) => action(SYSTEM_CONTACT_SUCCEEDED, payload);
export const updateSystemContactRequested = (payload: Partial<ISystemContact>) =>
  action(UPDATE_SYSTEM_CONTACT_REQUESTED, payload);
export const updateSystemContactSucceeded = (payload: Partial<SystemContact>) =>
  action(UPDATE_SYSTEM_CONTACT_SUCCEEDED, payload);
export const updateTermsAndConditionsRequested = () => action(UPDATE_TERMS_AND_CONDITIONS_REQUESTED);
export const updateTermsAndConditionsSucceeded = (validatedAt: AcceptTermsAndConditionsMutation) =>
  action(UPDATE_TERMS_AND_CONDITIONS_SUCCEEDED, validatedAt);
export const updateSystemContactFailed = () => action(UPDATE_SYSTEM_CONTACT_FAILED);
export const updateTermsAndConditionsFailed = () => action(UPDATE_TERMS_AND_CONDITIONS_FAILED);
export const systemContactFailed = () => action(SYSTEM_CONTACT_FAILED);
export const updateSystemContactAndTermsAndConditionsRequested = (payload: Partial<ISystemContact>) =>
  action(UPDATE_SYSTEM_CONTACT_AND_TERMS_AND_CONDITIONS_REQUESTED, payload);
