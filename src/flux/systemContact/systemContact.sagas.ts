import {all, call, put, take, takeEvery, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {refreshRequested} from '@govtechsg/redux-account-middleware';
import {ActionType} from 'typesafe-actions';
import {
  systemContactFailed,
  systemContactSucceeded,
  updateSystemContactRequested,
  updateSystemContactFailed,
  updateSystemContactSucceeded,
  updateTermsAndConditionsFailed,
  updateTermsAndConditionsRequested,
  updateTermsAndConditionsSucceeded,
  updateSystemContactAndTermsAndConditionsRequested,
} from '~/flux/systemContact/systemContact.actions';
import {
  SYSTEM_CONTACT_REQUESTED,
  UPDATE_SYSTEM_CONTACT_REQUESTED,
  UPDATE_SYSTEM_CONTACT_SUCCEEDED,
  UPDATE_TERMS_AND_CONDITIONS_REQUESTED,
  UPDATE_SYSTEM_CONTACT_AND_TERMS_AND_CONDITIONS_REQUESTED,
} from '~/flux/systemContact/systemContact.constants';
import {
  acceptTermsAndConditions,
  fetchSystemContact,
  mutateSystemContact,
} from '~/services/systemContact/systemContact';

export function* getSystemContact() {
  try {
    const response: SagaReturnType<typeof fetchSystemContact> = yield call(fetchSystemContact);
    if (!response.data.systemContact) {
      throw new Error('no data');
    }
    yield put(systemContactSucceeded(response.data.systemContact));
  } catch {
    yield put(systemContactFailed());
  }
}

export function* updateSystemContact({payload}: ActionType<typeof updateSystemContactRequested>) {
  try {
    const response: SagaReturnType<typeof mutateSystemContact> = yield call(mutateSystemContact, payload);
    if (!response.data?.updateSystemContact) {
      throw new Error('no data');
    }
    yield put(updateSystemContactSucceeded(response.data.updateSystemContact));
  } catch (error) {
    yield put(updateSystemContactFailed());
  }
}

export function* updateTermsAndConditions() {
  try {
    const response: SagaReturnType<typeof acceptTermsAndConditions> = yield call(acceptTermsAndConditions);
    if (!response.data) {
      throw new Error('no data');
    }
    yield put(updateTermsAndConditionsSucceeded(response.data));
    yield put(refreshRequested());
  } catch (error) {
    yield put(updateTermsAndConditionsFailed());
  }
}

export function* updateSystemContactAndTermsAndConditions({
  payload,
}: ActionType<typeof updateSystemContactAndTermsAndConditionsRequested>) {
  yield put(updateSystemContactRequested(payload));
  yield take(UPDATE_SYSTEM_CONTACT_SUCCEEDED);
  yield put(updateTermsAndConditionsRequested());
}

export function* systemContactSaga() {
  yield all([
    takeEvery(SYSTEM_CONTACT_REQUESTED, getSystemContact),
    takeLatest(UPDATE_SYSTEM_CONTACT_REQUESTED, updateSystemContact),
    takeLatest(UPDATE_TERMS_AND_CONDITIONS_REQUESTED, updateTermsAndConditions),
    takeLatest(UPDATE_SYSTEM_CONTACT_AND_TERMS_AND_CONDITIONS_REQUESTED, updateSystemContactAndTermsAndConditions),
  ]);
}
