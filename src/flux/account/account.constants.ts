export enum LoginActionTypes {
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
}
