import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {IAppState} from '~/flux';
import {getAccountUser} from '~/flux/account';

describe('account selectors', () => {
  describe('getAccountUser()', () => {
    it('should return account user', () => {
      const state: Partial<IAppState> = {
        account: {data: uenUserMock},
      };

      expect(getAccountUser(state as IAppState)).toEqual(uenUserMock);
    });

    it('should return undefined when account.data is not defined', () => {
      const state: Partial<IAppState> = {
        account: {},
      };

      expect(getAccountUser(state as IAppState)).toBeUndefined();
    });

    it('should return undefined when account issuer is not Corppass', () => {
      const state: Partial<IAppState> = {
        account: {data: {...uenUserMock, iss: 'Singpass'}},
      };

      expect(getAccountUser(state as IAppState)).toBeUndefined();
    });
  });
});
