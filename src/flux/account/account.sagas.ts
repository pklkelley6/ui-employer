import {SESSION_EXPIRED} from '@govtechsg/redux-account-middleware';
import {push, getLocation} from 'connected-react-router';
import {all, takeEvery, takeLatest, put, select, SagaReturnType} from 'redux-saga/effects';
import {LoginActionTypes} from './account.constants';
import {config} from '~/config';

function* loginRedirect() {
  const {pathname}: SagaReturnType<typeof getLocation> = yield select(getLocation);

  // if user login from logout or timeout page, after login redirect them to homepage instead of logout page else redirect them to where they were
  const returnUrl = ['/logout', '/session-timeout', '/unauthorised'].includes(pathname)
    ? window.location.origin
    : window.location.href;
  window.location.assign(`${config.url.corppass}/login?target=${encodeURIComponent(returnUrl)}`);
}

function* logoutRedirect() {
  yield put(push('/logout'));
}

function* sessionExpiredRedirect() {
  yield put(push('/session-timeout'));
}

export function* accountSaga() {
  yield all([
    takeEvery(LoginActionTypes.LOGIN, loginRedirect),
    takeEvery(LoginActionTypes.LOGOUT, logoutRedirect),
    takeLatest(SESSION_EXPIRED, sessionExpiredRedirect),
  ]);
}
