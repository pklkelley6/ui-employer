import {action} from 'typesafe-actions';
import {LoginActionTypes} from './account.constants';

export const login = () => action(LoginActionTypes.LOGIN);
export const logout = () => action(LoginActionTypes.LOGOUT);
