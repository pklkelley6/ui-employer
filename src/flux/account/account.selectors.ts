import {IAppState} from '../index';
import {IUser} from '~/flux/account';

export const getAccountUser = ({account: {data}}: IAppState): IUser | undefined => {
  return data && isCorppass(data) ? data : undefined;
};

export const getAccountName = ({account: {data}}: IAppState): string => {
  return data ? data.userInfo.userFullName : '';
};

export const getAccountUen = ({account: {data}}: IAppState) => data?.userInfo.entityId;

export const isCorppass = (account: IUser) => {
  return account.iss === 'Corppass';
};

export const getAccountSystemUnixTimeStamp = ({account: {data}}: IAppState) => data?.exp;
