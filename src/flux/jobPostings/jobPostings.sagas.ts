import {all, call, put, select, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {getAccountUser} from '../account';
import {
  fetchClosedJobsFailed,
  fetchClosedJobsRequested,
  fetchClosedJobsSucceeded,
  fetchOpenJobsFailed,
  fetchOpenJobsRequested,
  fetchOpenJobsSucceeded,
} from './jobPostings.actions';
import {CLOSED_JOBS_REQUESTED, OPEN_JOBS_REQUESTED} from './jobPostings.constants';
import {getClosedJobs, getOpenJobs} from '~/services/employer/jobs';

export function* fetchOpenJobs({payload}: ActionType<typeof fetchOpenJobsRequested>) {
  try {
    const user: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);
    if (!user) {
      throw new Error('user not login');
    }
    const openJobs: SagaReturnType<typeof getOpenJobs> = yield call(getOpenJobs, user.userInfo.entityId, payload);
    const hasSearchParams = !!(payload.jobPostId || payload.title);
    yield put(fetchOpenJobsSucceeded(openJobs, hasSearchParams));
  } catch (e) {
    yield put(fetchOpenJobsFailed());
  }
}

export function* fetchClosedJobs({payload}: ActionType<typeof fetchClosedJobsRequested>) {
  try {
    const user: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);
    if (!user) {
      throw new Error('user not login');
    }
    const closedJobs: SagaReturnType<typeof getClosedJobs> = yield call(getClosedJobs, user.userInfo.entityId, payload);
    const hasSearchParams = !!(payload.jobPostId || payload.title);
    yield put(fetchClosedJobsSucceeded(closedJobs, hasSearchParams));
  } catch (_) {
    yield put(fetchClosedJobsFailed());
  }
}

export function* jobsSaga() {
  yield all([takeLatest(OPEN_JOBS_REQUESTED, fetchOpenJobs), takeLatest(CLOSED_JOBS_REQUESTED, fetchClosedJobs)]);
}
