import {action} from 'typesafe-actions';
import {
  CLOSED_JOBS_RECEIVED,
  CLOSED_JOBS_REQUEST_FAILED,
  CLOSED_JOBS_REQUESTED,
  OPEN_JOBS_RECEIVED,
  OPEN_JOBS_REQUEST_FAILED,
  OPEN_JOBS_REQUESTED,
  RESET_JOB_POSTINGS_PARAMS,
} from './jobPostings.constants';

import {IFetchClosedJobs, IFetchOpenJobs} from './jobPostings.types';
import {IJobsResponse} from '~/flux/jobPostings';

export const fetchOpenJobsRequested = (query: IFetchOpenJobs) => action(OPEN_JOBS_REQUESTED, query);
export const fetchOpenJobsSucceeded = (jobs: IJobsResponse, hasSearchParams: boolean) =>
  action(OPEN_JOBS_RECEIVED, jobs, {hasSearchParams});
export const fetchOpenJobsFailed = () => action(OPEN_JOBS_REQUEST_FAILED);

export const fetchClosedJobsRequested = (query: IFetchClosedJobs) => action(CLOSED_JOBS_REQUESTED, query);
export const fetchClosedJobsSucceeded = (jobs: IJobsResponse, hasSearchParams: boolean) =>
  action(CLOSED_JOBS_RECEIVED, jobs, {hasSearchParams});
export const fetchClosedJobsFailed = () => action(CLOSED_JOBS_REQUEST_FAILED);

export const resetJobPostingsParams = () => action(RESET_JOB_POSTINGS_PARAMS);
