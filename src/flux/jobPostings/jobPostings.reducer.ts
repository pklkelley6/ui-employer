import {ActionType} from 'typesafe-actions';
import * as actions from './jobPostings.actions';
import {
  CLOSED_JOBS_RECEIVED,
  CLOSED_JOBS_REQUEST_FAILED,
  CLOSED_JOBS_REQUESTED,
  JobsSortCriteria,
  OPEN_JOBS_RECEIVED,
  OPEN_JOBS_REQUEST_FAILED,
  OPEN_JOBS_REQUESTED,
  RESET_JOB_POSTINGS_PARAMS,
} from './jobPostings.constants';
import {IFetchClosedJobs, IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {SortDirection} from '~/graphql/__generated__/types';
import {PAGE_SIZE} from '~/services/employer/jobs';
import {IJobPost} from '~/services/employer/jobs.types';

const initialState: IJobPostingsState = {
  closedJobs: {
    results: [],
    total: 0,
  },
  closedJobsParams: {
    limit: PAGE_SIZE,
    orderBy: JobsSortCriteria.DELETED_AT,
    orderDirection: SortDirection.Desc,
    page: 0,
  },
  closedJobsTotal: 0,
  openJobs: {
    results: [],
    total: 0,
  },
  openJobsParams: {
    limit: PAGE_SIZE,
    orderBy: JobsSortCriteria.EXPIRY_DATE,
    orderDirection: SortDirection.Asc,
    page: 0,
  },
  openJobsTotal: 0,
};

export const jobPostings = (
  state: IJobPostingsState = initialState,
  action: JobPostingsActionType,
): IJobPostingsState => {
  switch (action.type) {
    case OPEN_JOBS_RECEIVED:
      return {
        ...state,
        openJobs: action.payload,
        openJobsFetchStatus: action.type,
        openJobsTotal: action.meta.hasSearchParams ? state.openJobsTotal : action.payload.total,
      };
    case CLOSED_JOBS_RECEIVED:
      return {
        ...state,
        closedJobs: action.payload,
        closedJobsFetchStatus: action.type,
        closedJobsTotal: action.meta.hasSearchParams ? state.closedJobsTotal : action.payload.total,
      };
    case OPEN_JOBS_REQUESTED:
      return {
        ...state,
        openJobsFetchStatus: action.type,
        openJobsParams: action.payload,
      };
    case CLOSED_JOBS_REQUESTED:
      return {
        ...state,
        closedJobsFetchStatus: action.type,
        closedJobsParams: action.payload,
      };
    case OPEN_JOBS_REQUEST_FAILED:
      return {
        ...state,
        openJobsFetchStatus: action.type,
      };
    case CLOSED_JOBS_REQUEST_FAILED:
      return {
        ...state,
        closedJobsFetchStatus: action.type,
      };
    case RESET_JOB_POSTINGS_PARAMS:
      return {
        ...state,
        closedJobsParams: initialState.closedJobsParams,
        openJobsParams: initialState.openJobsParams,
      };
    default:
      return state;
  }
};

export interface IJobsResponse {
  results: IJobPost[];
  total: number;
  _links?: any;
}

/**
 * @property openJobsTotal  - to persist `total` of Jobs request results without params;
 *
 * Difference is to persist `total` of results in Tabs once you do
 * search; because right now we replace `openJobs` in state with any
 * request call.
 *
 * Example are, once you visit `/jobs` you will have total 10 results;
 * tabs shows `Opened (10)`. Once you started search for something and
 * you find `5` results; tab will remain with `Opened (10)`.
 */
export interface IJobPostingsState {
  closedJobs: IJobsResponse;
  closedJobsFetchStatus?: string;
  closedJobsParams: IFetchClosedJobs;
  closedJobsTotal: number;
  fetchStatus?: string;
  openJobs: IJobsResponse;
  openJobsFetchStatus?: string;
  openJobsParams: IFetchOpenJobs;
  openJobsTotal: number;
}

export type JobPostingsActionType = ActionType<typeof actions>;
