import {expectSaga, testSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {throwError} from 'redux-saga-test-plan/providers';
import {getAccountUser} from '../../account';
import {
  fetchClosedJobsFailed,
  fetchClosedJobsRequested,
  fetchClosedJobsSucceeded,
  fetchOpenJobsFailed,
  fetchOpenJobsRequested,
  fetchOpenJobsSucceeded,
} from '../jobPostings.actions';
import {fetchClosedJobs, fetchOpenJobs} from '../jobPostings.sagas';
import {getClosedJobs, getOpenJobs, PAGE_SIZE} from '~/services/employer/jobs';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

describe('jobPostings sagas', () => {
  describe('openJobsSaga', () => {
    it('calls getOpenJobs service and yields the correct action', () => {
      const openJobsResponseMock = {
        results: [],
        total: 0,
      };
      const openJobsParamsMock = {
        limit: PAGE_SIZE,
        page: 0,
      };
      const openJobsMetaMock = {
        hasSearchParams: false,
      };

      testSaga(fetchOpenJobs, fetchOpenJobsRequested(openJobsParamsMock))
        .next()
        .select(getAccountUser)
        .next(uenUserMock)
        .call(getOpenJobs, uenUserMock.userInfo.entityId, openJobsParamsMock)
        .next(openJobsResponseMock)
        .put(fetchOpenJobsSucceeded(openJobsResponseMock, openJobsMetaMock.hasSearchParams))
        .next()
        .isDone();
    });

    it('raise an error', () => {
      const error = new Error('error');

      const openJobsParamsMock = {
        limit: PAGE_SIZE,
        page: 0,
      };

      return expectSaga(fetchOpenJobs, fetchOpenJobsRequested(openJobsParamsMock))
        .provide([[matchers.call.fn(getOpenJobs), throwError(error)]])
        .put(fetchOpenJobsFailed())
        .run();
    });
  });

  describe('closedJobsSaga', () => {
    it('calls getClosedJobs service and yields the correct action', () => {
      const closedJobsParamsMock = {
        limit: PAGE_SIZE,
        page: 0,
      };
      const closedJobsResponseMock = {
        results: [],
        total: 0,
      };
      const closedJobsMetaMock = {
        hasSearchParams: false,
      };

      testSaga(fetchClosedJobs, fetchClosedJobsRequested(closedJobsParamsMock))
        .next()
        .select(getAccountUser)
        .next(uenUserMock)
        .call(getClosedJobs, uenUserMock.userInfo.entityId, closedJobsParamsMock)
        .next(closedJobsResponseMock)
        .put(fetchClosedJobsSucceeded(closedJobsResponseMock, closedJobsMetaMock.hasSearchParams))
        .next()
        .isDone();
    });

    it('raise an error', () => {
      const error = new Error('error');
      const closedJobsParamsMock = {
        limit: PAGE_SIZE,
        page: 0,
      };

      return expectSaga(fetchClosedJobs, fetchClosedJobsRequested(closedJobsParamsMock))
        .provide([[matchers.call.fn(getClosedJobs), throwError(error)]])
        .put(fetchClosedJobsFailed())
        .run();
    });
  });
});
