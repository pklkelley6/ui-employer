import {SortDirection} from '~/graphql/__generated__/types';

export const OPEN_JOBS_REQUESTED = 'OPEN_JOBS_REQUESTED';
export const OPEN_JOBS_RECEIVED = 'OPEN_JOBS_RECEIVED';
export const OPEN_JOBS_REQUEST_FAILED = 'OPEN_JOBS_REQUEST_FAILED';

export const CLOSED_JOBS_REQUESTED = 'CLOSED_JOBS_REQUESTED';
export const CLOSED_JOBS_RECEIVED = 'CLOSED_JOBS_RECEIVED';
export const CLOSED_JOBS_REQUEST_FAILED = 'CLOSED_JOBS_REQUEST_FAILED';

export const RESET_JOB_POSTINGS_PARAMS = 'RESET_JOB_POSTINGS_PARAMS';

export enum JobsSortCriteria {
  DELETED_AT = 'deleted_at',
  EXPIRY_DATE = 'expiry_date',
  POSTED_DATE = 'new_posting_date',
}

export const JOBS_SORT_CRITERIA_OPEN_JOBS = [
  {
    label: 'Auto-close Date',
    value: JobsSortCriteria.EXPIRY_DATE,
  },
  {
    label: 'Posted Date',
    value: JobsSortCriteria.POSTED_DATE,
  },
];

export const JOBS_SORT_CRITERIA_CLOSED_JOBS = [
  {
    label: 'Closed Date',
    value: JobsSortCriteria.DELETED_AT,
  },
  {
    label: 'Posted Date',
    value: JobsSortCriteria.POSTED_DATE,
  },
];

export enum JOBS_STATUS {
  CLOSED_JOBS = 'CLOSED_JOBS',
  OPEN_JOBS = 'OPEN_JOBS',
}

export const CRITERIA_ORDER_BY = {
  [JOBS_STATUS.CLOSED_JOBS]: {
    [JobsSortCriteria.DELETED_AT]: SortDirection.Desc,
    [JobsSortCriteria.EXPIRY_DATE]: undefined,
    [JobsSortCriteria.POSTED_DATE]: SortDirection.Desc,
  },
  [JOBS_STATUS.OPEN_JOBS]: {
    [JobsSortCriteria.DELETED_AT]: undefined,
    [JobsSortCriteria.EXPIRY_DATE]: SortDirection.Asc,
    [JobsSortCriteria.POSTED_DATE]: SortDirection.Desc,
  },
};
