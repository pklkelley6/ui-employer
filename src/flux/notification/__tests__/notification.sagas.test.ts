import {expectSaga, testSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {throwError} from 'redux-saga-test-plan/providers';
import {notificationFetchFailed, notificationFetchSucceeded} from '../notification.actions';
import {fetchNotifications} from '../notification.sagas';
import {maintenanceNotificationMock, featureNotificationMock} from '~/__mocks__/notification/notification.mocks';
import {getNotifications} from '~/services/notification/getNotifications';

describe('notification sagas', () => {
  describe('fetchNotifications', () => {
    it('should fetch maintenance and feature notifications', () => {
      const notificationsMock = [maintenanceNotificationMock, featureNotificationMock];
      testSaga(fetchNotifications)
        .next()
        .call(getNotifications)
        .next(notificationsMock)
        .put(notificationFetchSucceeded(notificationsMock))
        .next()
        .isDone();
    });

    it('should handle error', () => {
      const error = new Error('error');

      return expectSaga(fetchNotifications)
        .provide([[matchers.call.fn(getNotifications), throwError(error)]])
        .put(notificationFetchFailed())
        .run();
    });
  });
});
