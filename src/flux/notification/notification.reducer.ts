import {filter} from 'lodash/fp';
import {ActionType} from 'typesafe-actions';
import * as actions from './notification.actions';
import {NOTIFICATION_FETCH_SUCCEEDED} from './notification.constants';
import {
  isFeatureNotification,
  isMaintenanceNotification,
  IMaintenanceNotification,
  IFeatureNotification,
} from '~/services/notification/getNotifications';

export interface INotificationState {
  maintenances: IMaintenanceNotification[];
  features: IFeatureNotification[];
}
const initialState = {
  maintenances: [],
  features: [],
};

export const notification = (
  state: INotificationState = initialState,
  action: NotificationActionType,
): INotificationState => {
  switch (action.type) {
    case NOTIFICATION_FETCH_SUCCEEDED:
      return {
        ...state,
        features: filter(isFeatureNotification, action.payload),
        maintenances: filter(isMaintenanceNotification, action.payload),
      };
    default:
      return state;
  }
};

export type NotificationActionType = ActionType<typeof actions>;
