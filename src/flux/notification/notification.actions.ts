import {action} from 'typesafe-actions';
import {
  NOTIFICATION_FETCH_FAILED,
  NOTIFICATION_FETCH_REQUESTED,
  NOTIFICATION_FETCH_SUCCEEDED,
} from './notification.constants';
import {INotification} from '~/services/notification/getNotifications';

export const notificationFetchRequested = () => action(NOTIFICATION_FETCH_REQUESTED);
export const notificationFetchSucceeded = (notifications: INotification[]) =>
  action(NOTIFICATION_FETCH_SUCCEEDED, notifications);
export const notificationFetchFailed = () => action(NOTIFICATION_FETCH_FAILED);
