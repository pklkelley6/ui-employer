import {action} from 'typesafe-actions';

export const UPDATE_RELEASE_TOGGLES = 'UPDATE_RELEASE_TOGGLES';
export const updateReleaseToggles = (releaseToggles: Record<string, boolean>) => {
  return action(UPDATE_RELEASE_TOGGLES, releaseToggles);
};
