import {IReleaseToggles} from './releaseToggles.reducer';
import {IAppState} from '~/flux';

export const getReleaseFlag = (name: keyof IReleaseToggles) => (state: IAppState) => state.releaseToggles[name];
