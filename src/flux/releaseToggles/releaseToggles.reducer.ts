import {ActionType} from 'typesafe-actions';
import * as actions from './releaseToggles.actions';

const initialState = {
  '174262411TopMatchInFilterModal': true,
  '177657666filterSortEmptyScoresApplications': true,
  '179716621RepostJobDisplayEmailRecipient': true,
  '179646046ViewEditJobDisplayEmailRecipient': true,
  '179899438enhancedSkillPills': true,
};

export type IReleaseToggles = typeof initialState;

export type ReleaseActionType = ActionType<typeof actions>;

export const releaseToggles = (state: IReleaseToggles = initialState, action: ReleaseActionType): IReleaseToggles => {
  switch (action.type) {
    case actions.UPDATE_RELEASE_TOGGLES:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
