import {all, call, put, select, takeEvery, takeLatest, SagaReturnType} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {getAccountUser} from '~/flux/account';
import {onApplicationClicked} from '~/flux/analytics/analytics.actions';
import {ON_APPLICATION_CLICKED} from '~/flux/analytics/analytics.constants';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {ON_SUGGESTED_TALENT_CLICKED} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {makeSurveyHidden, makeSurveyVisible} from '~/flux/survey/survey.actions';
import {CLOSE_SURVEY, TAKE_SURVEY} from '~/flux/survey/survey.constants';
import {
  resetSurveyStorage,
  saveApplicationClick,
  saveTalentClick,
  shouldCount,
  shouldSurvey,
  stopCount,
} from '~/services/survey/survey';

export function* closeSurveyBar() {
  const accountUser: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);
  if (accountUser?.userInfo.userName) {
    yield call(resetSurveyStorage, accountUser.userInfo.userName);
  }
  yield put(makeSurveyHidden());
}

export function* stopSurveyCount() {
  const accountUser: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);
  if (accountUser?.userInfo.userName) {
    yield call(stopCount, accountUser.userInfo.userName);
  }
  yield put(makeSurveyHidden());
}

export function* countApplicationClicks({payload}: ActionType<typeof onApplicationClicked>) {
  const accountUser: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);

  const surveyStorageString = localStorage.getItem('survey-' + accountUser?.userInfo.userName);
  if (accountUser && shouldCount(surveyStorageString)) {
    const storage: SagaReturnType<typeof saveApplicationClick> = yield call(
      saveApplicationClick,
      accountUser.userInfo.userName,
      payload.id,
    );

    if (shouldSurvey(storage)) {
      yield put(makeSurveyVisible());
    }
  }
}

export function* countTalentClicks({payload}: ActionType<typeof onSuggestedTalentClicked>) {
  const accountUser: SagaReturnType<typeof getAccountUser> = yield select(getAccountUser);

  const surveyStorageString = localStorage.getItem('survey-' + accountUser?.userInfo.userName);
  if (accountUser && shouldCount(surveyStorageString)) {
    const storage: SagaReturnType<typeof saveTalentClick> = yield call(
      saveTalentClick,
      accountUser.userInfo.userName,
      payload.talentId,
    );

    if (shouldSurvey(storage)) {
      yield put(makeSurveyVisible());
    }
  }
}

export function* surveySaga() {
  yield all([
    takeEvery(ON_APPLICATION_CLICKED, countApplicationClicks),
    takeEvery(ON_SUGGESTED_TALENT_CLICKED, countTalentClicks),
    takeLatest(CLOSE_SURVEY, closeSurveyBar),
    takeLatest(TAKE_SURVEY, stopSurveyCount),
  ]);
}
