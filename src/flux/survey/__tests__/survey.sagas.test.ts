import {testSaga} from 'redux-saga-test-plan';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {getAccountUser} from '~/flux/account';
import {onApplicationClicked} from '~/flux/analytics/analytics.actions';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {makeSurveyHidden, makeSurveyVisible} from '~/flux/survey/survey.actions';
import {closeSurveyBar, countApplicationClicks, countTalentClicks, stopSurveyCount} from '~/flux/survey/survey.sagas';
import {resetSurveyStorage, saveApplicationClick, saveTalentClick, stopCount} from '~/services/survey/survey';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

describe('survey sagas', () => {
  it('should closeSurveyBar', () => {
    testSaga(closeSurveyBar)
      .next()
      .select(getAccountUser)
      .next(uenUserMock)
      .call(resetSurveyStorage, uenUserMock.userInfo.userName)
      .next()
      .put(makeSurveyHidden())
      .next()
      .isDone();
  });

  it('should stopSurveyCount', () => {
    testSaga(stopSurveyCount)
      .next()
      .select(getAccountUser)
      .next(uenUserMock)
      .call(stopCount, uenUserMock.userInfo.userName)
      .next()
      .put(makeSurveyHidden())
      .next()
      .isDone();
  });

  it('should countApplicationClicks', () => {
    const surveyStorageMock = {applications: ['ids', 'ids', 'ids', 'ids', 'ids'], talents: [], isCounting: true};
    const positionMock = 1;

    testSaga(countApplicationClicks, onApplicationClicked(applicationMock, positionMock))
      .next()
      .select(getAccountUser)
      .next(uenUserMock)
      .call(saveApplicationClick, uenUserMock.userInfo.userName, applicationMock.id)
      .next(surveyStorageMock)
      .put(makeSurveyVisible())
      .next()
      .isDone();
  });

  it('should countTalentClicks', () => {
    const surveyStorageMock = {applications: ['ids', 'ids', 'ids', 'ids', 'ids'], talents: [], isCounting: true};
    const talentIdMock = 'id';
    const positionMock = 1;
    const jobUuidMock = 'abcdefghi';

    testSaga(countTalentClicks, onSuggestedTalentClicked(talentIdMock, positionMock, jobUuidMock))
      .next()
      .select(getAccountUser)
      .next(uenUserMock)
      .call(saveTalentClick, uenUserMock.userInfo.userName, talentIdMock)
      .next(surveyStorageMock)
      .put(makeSurveyVisible())
      .next()
      .isDone();
  });
});
