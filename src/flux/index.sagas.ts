import {spawn} from 'redux-saga/effects';
import {accountSaga} from '~/flux/account';
import {analyticsSaga} from '~/flux/analytics/analytics.sagas';
import {companySaga} from '~/flux/company/company.sagas';
import {jobPostingSaga} from '~/flux/jobPosting/jobPosting.sagas';
import {jobsSaga} from '~/flux/jobPostings/jobPostings.sagas';
import {jobTitlesSaga} from '~/flux/jobTitles/jobTitles.sagas';
import {notificationSaga} from '~/flux/notification/notification.sagas';
import {surveySaga} from '~/flux/survey/survey.sagas';
import {systemContactSaga} from '~/flux/systemContact/systemContact.sagas';
import {jobPostHistorySaga} from '~/flux/jobPostHistory/jobPostHistory.sagas';

export function* rootSaga() {
  yield spawn(accountSaga);
  yield spawn(jobsSaga);
  yield spawn(jobPostingSaga);
  yield spawn(companySaga);
  yield spawn(notificationSaga);
  yield spawn(analyticsSaga);
  yield spawn(surveySaga);
  yield spawn(jobTitlesSaga);
  yield spawn(systemContactSaga);
  yield spawn(jobPostHistorySaga);
}
