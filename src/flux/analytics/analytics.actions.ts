import {action} from 'typesafe-actions';
import {mcf} from '@mcf/constants';
import {ApplicationSortCriteria} from '../applications';
import {
  APPLICATIONS_SORT_TYPE,
  APPLICATIONS_TOP_MATCH_LABEL,
  APPLICATIONS_TOP_MATCH_TOTAL,
  JOBS_SORTING_TYPE,
  ON_APPLICATION_CANDIDATE_RESUME_CLICKED,
  ON_APPLICATION_CANDIDATE_TAB_CLICKED,
  ON_APPLICATION_CLICKED,
  ON_APPLICATION_PAGE_CLICKED,
  ON_APPLICATIONS_TOP_MATCH_FILTERED,
  ON_APPLICATIONS_FILTERED,
  ON_CORPPASS_GET_STARTED_CLICKED,
  ON_JOB_SEARCH_QUERY,
  ON_NAVIGATION_MENU_LINK_CLICKED,
  ON_PAGE_CHANGE_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_INVITE_TO_APPLY_CLICKED,
  ON_SUGGESTED_TALENTS_PAGE_CLICKED,
  ON_SUGGESTED_TALENTS_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_TAB_CLICKED,
  ON_MASS_UPDATE_APPLICATIONS_SELECTED,
  ON_MASS_UPDATE_APPLICATIONS_STATUS_UPDATED,
  ON_TALENT_SEARCH_CANDIDATE_RESUME_CLICKED,
  ON_TALENT_SEARCH_CANDIDATES_PAGE_CLICKED,
  ON_TALENT_SEARCH_CANDIDATE_TAB_CLICKED,
  ON_TALENT_SEARCH_QUERY,
  ON_TALENT_SEARCH_CANDIDATE_INVITED,
  ON_JOB_POSTING_SCREENING_QUESTIONS_ADDED,
  ON_JOB_POSTING_SCREENING_QUESTIONS_REMOVED,
} from './analytics.constants';
import {CandidateTabs} from '~/components/CandidateInfo';
import {GetApplicationsApplications, GetBookmarkedCandidatesBookmarkedCandidates} from '~/graphql/__generated__/types';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {MAIN_NAVIGATION_MENU_LINK_TYPE} from '~/analytics/mainNavigationMenu';

export const onNavigationMenuLinkClicked = (title: MAIN_NAVIGATION_MENU_LINK_TYPE) =>
  action(ON_NAVIGATION_MENU_LINK_CLICKED, title);
export const onCorppassGetStartedClicked = () => action(ON_CORPPASS_GET_STARTED_CLICKED);

export const onApplicationClicked = (application: GetApplicationsApplications, position: number) =>
  action(ON_APPLICATION_CLICKED, {...application, position});
export const onApplicationCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_APPLICATION_CANDIDATE_TAB_CLICKED, tabIndex);
export const onApplicationPageClicked = (jobId: string, page: number) =>
  action(ON_APPLICATION_PAGE_CLICKED, {jobId, page});
export const onApplicationCandidateResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_APPLICATION_CANDIDATE_RESUME_CLICKED, label);
export const onApplicationSortingTypeClicked = (sortType: ApplicationSortCriteria) =>
  action(APPLICATIONS_SORT_TYPE, sortType);

export const onApplicationTopMatchFilter = (checked: boolean) => action(ON_APPLICATIONS_TOP_MATCH_FILTERED, checked);
export const onApplicationsFilter = ({
  isTopMatch,
  applicationStatus,
  screeningQuestions,
  jobId,
}: {
  isTopMatch: boolean;
  applicationStatus: number[];
  screeningQuestions: string[];
  jobId: string;
}) => {
  return action(ON_APPLICATIONS_FILTERED, {isTopMatch, applicationStatus, screeningQuestions, jobId});
};

export const getApplicationTopMatchTotal = (total: number, sortCriteria: ApplicationSortCriteria, jobId: string) =>
  action(APPLICATIONS_TOP_MATCH_TOTAL, {total, sortCriteria, jobId});
export const onApplicationTopMatchLabelClicked = () => action(APPLICATIONS_TOP_MATCH_LABEL);

export const onJobsSortingTypeClicked = (sortType: string) => action(JOBS_SORTING_TYPE, sortType);
export const onPageChangeClicked = ({jobStatus, page}: {jobStatus: string; page: number}) =>
  action(ON_PAGE_CHANGE_CLICKED, {jobStatus, page});
export const onJobSearchQueried = (query: string) => action(ON_JOB_SEARCH_QUERY, query);

export const onSuggestedTalentsPageClicked = (jobId: string, page: number) =>
  action(ON_SUGGESTED_TALENTS_PAGE_CLICKED, {jobId, page});
export const onSuggestedTalentsResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_SUGGESTED_TALENTS_RESUME_CLICKED, label);

export const onSuggestedTalentCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED, tabIndex);
export const onSuggestedTalentCandidateInviteToApplyClicked = (candidateId: string, jobId: string) =>
  action(ON_SUGGESTED_TALENT_CANDIDATE_INVITE_TO_APPLY_CLICKED, {candidateId, jobId});

export const onBookmarkedCandidatesPageClicked = (jobId: string, page: number) =>
  action(ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED, {jobId, page});
export const onBookmarkedCandidateResumeClicked = (label: DOWNLOAD_RESUME_GA_LABEL) =>
  action(ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED, label);
export const onBookmarkedCandidateClicked = (
  bookmarkedCandidate: GetBookmarkedCandidatesBookmarkedCandidates,
  position: number,
  jobUuid: string,
) => action(ON_BOOKMARKED_CANDIDATE_CLICKED, {bookmarkedCandidate, position, jobUuid});
export const onBookmarkedCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_BOOKMARKED_CANDIDATE_TAB_CLICKED, tabIndex);

export const onTalentSearchCandidateResumeClicked = (label = DOWNLOAD_RESUME_GA_LABEL.DETAIL) =>
  action(ON_TALENT_SEARCH_CANDIDATE_RESUME_CLICKED, label);
export const onTalentSearchCandidatesPageClicked = (page: number) =>
  action(ON_TALENT_SEARCH_CANDIDATES_PAGE_CLICKED, {page});
export const onTalentSearchCandidateTabClicked = (tabIndex: CandidateTabs) =>
  action(ON_TALENT_SEARCH_CANDIDATE_TAB_CLICKED, tabIndex);
export const onTalentSearchQueried = (query: string) => action(ON_TALENT_SEARCH_QUERY, query);
export const onTalentSearchCandidateInvited = (candidateId: string, jobId: string) =>
  action(ON_TALENT_SEARCH_CANDIDATE_INVITED, {candidateId, jobId});

export const onMassUpdateApplicationsSelected = (jobId: string, numberOfApplicationSelected: number) =>
  action(ON_MASS_UPDATE_APPLICATIONS_SELECTED, {jobId, numberOfApplicationSelected});
export const onMassUpdateApplicationsStatusUpdated = (jobId: string, applicationStatus: mcf.JOB_APPLICATION_STATUS) =>
  action(ON_MASS_UPDATE_APPLICATIONS_STATUS_UPDATED, {jobId, applicationStatus});

export const onJobPostingScreeningQuestionsAdded = (jobId: string, numberOfQuestions: number) =>
  action(ON_JOB_POSTING_SCREENING_QUESTIONS_ADDED, {jobId, numberOfQuestions});
export const onJobPostingScreeningQuestionsRemoved = (jobId: string, numberOfQuestions: number) =>
  action(ON_JOB_POSTING_SCREENING_QUESTIONS_REMOVED, {jobId, numberOfQuestions});
