import {ACCOUNT_RECEIVED, IAccountData} from '@govtechsg/redux-account-middleware';
import {LOCATION_CHANGE, LocationChangeAction} from 'connected-react-router';
import {all, call, select, spawn, takeEvery} from 'redux-saga/effects';
import {ActionType} from 'typesafe-actions';
import {onTalentSearchCandidateInvited} from './analytics.actions';
import {handleEmployerLogin, handleEmployerLogout, tagUserSession} from '~/analytics/account';
import {
  handleApplicationSortingType,
  handleApplicationsFilter,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
  handleApplicationMassUpdateSelectionClick,
  handleApplicationMassUpdateStatusClick,
} from '~/analytics/applications';
import {
  handleCandidateTabClick,
  handleCandidatesPageClick,
  handleCandidateResumeClick,
  handleCandidateClick,
} from '~/analytics/candidates';
import {handleAllJobsSortingType, handlePageChange} from '~/analytics/filters';
import {handleInviteToApplyClick, handleTalentSearchQuery} from '~/analytics/talents/talents';
import {handleCorpPassGetStartedClick} from '~/analytics/homeBanner';
import {handleJobsSearchQueryEvent} from '~/analytics/jobsSearchQuery';
import {mainNavigationMenuLinks} from '~/analytics/mainNavigationMenu';
import {handlePageView, startJobPosting, completeJobPosting} from '~/analytics/pages';
import {handleScreeningQuestionsChanged} from '~/analytics/jobPosting';
import {IUser, LoginActionTypes} from '~/flux/account';
import {
  APPLICATIONS_SORT_TYPE,
  APPLICATIONS_TOP_MATCH_LABEL,
  APPLICATIONS_TOP_MATCH_TOTAL,
  ON_APPLICATIONS_FILTERED,
  getApplicationTopMatchTotal,
  JOBS_SORTING_TYPE,
  ON_APPLICATION_CANDIDATE_RESUME_CLICKED,
  ON_APPLICATION_CANDIDATE_TAB_CLICKED,
  ON_APPLICATION_CLICKED,
  ON_APPLICATION_PAGE_CLICKED,
  ON_APPLICATIONS_TOP_MATCH_FILTERED,
  ON_CORPPASS_GET_STARTED_CLICKED,
  ON_JOB_SEARCH_QUERY,
  ON_NAVIGATION_MENU_LINK_CLICKED,
  ON_PAGE_CHANGE_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_INVITE_TO_APPLY_CLICKED,
  ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED,
  ON_SUGGESTED_TALENTS_PAGE_CLICKED,
  ON_SUGGESTED_TALENTS_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATE_CLICKED,
  ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED,
  ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED,
  ON_BOOKMARKED_CANDIDATE_TAB_CLICKED,
  ON_TALENT_SEARCH_CANDIDATE_RESUME_CLICKED,
  ON_TALENT_SEARCH_CANDIDATES_PAGE_CLICKED,
  ON_TALENT_SEARCH_CANDIDATE_TAB_CLICKED,
  ON_TALENT_SEARCH_QUERY,
  ON_MASS_UPDATE_APPLICATIONS_SELECTED,
  ON_MASS_UPDATE_APPLICATIONS_STATUS_UPDATED,
  ON_JOB_POSTING_SCREENING_QUESTIONS_ADDED,
  ON_JOB_POSTING_SCREENING_QUESTIONS_REMOVED,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onApplicationsFilter,
  onJobSearchQueried,
  onJobsSortingTypeClicked,
  onNavigationMenuLinkClicked,
  onPageChangeClicked,
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
  onApplicationCandidateResumeClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
  onBookmarkedCandidateClicked,
  onBookmarkedCandidateTabClicked,
  onTalentSearchCandidatesPageClicked,
  onTalentSearchCandidateTabClicked,
  onTalentSearchCandidateResumeClicked,
  onTalentSearchQueried,
  onMassUpdateApplicationsSelected,
  onMassUpdateApplicationsStatusUpdated,
  onSuggestedTalentCandidateInviteToApplyClicked,
  ON_TALENT_SEARCH_CANDIDATE_INVITED,
  onJobPostingScreeningQuestionsAdded,
  onJobPostingScreeningQuestionsRemoved,
} from '~/flux/analytics';
import {getLocation} from '~/flux/location';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {ON_SUGGESTED_TALENT_CLICKED} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {START_JOB_POSTING, COMPLETE_JOB_POSTING} from '~/flux/jobPosting/jobPosting.constants';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';
import {isApplication} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';

export function* sendPageView({payload: {location}}: LocationChangeAction) {
  yield spawn(handlePageView, location.pathname);
}

export function* mainNavigationMenuLinkEvent({payload}: ActionType<typeof onNavigationMenuLinkClicked>) {
  const {pathname} = yield select(getLocation);
  yield call(mainNavigationMenuLinks, payload, pathname);
}

export function* corpPassGetStartedEvent() {
  yield call(handleCorpPassGetStartedClick);
}

export function* employerLoginEvent() {
  yield call(handleEmployerLogin);
}

export function* sendApplicationClickEvent({payload}: ActionType<typeof onApplicationClicked>) {
  yield call(
    handleCandidateClick,
    {
      applicationId: payload.id,
      candidateId: payload.applicant.id,
      jobId: payload.job.uuid,
      position: payload.position,
    },
    EVENT_CATEGORY.JOB_APPLICANT,
  );
}

export function* sendApplicationSortingTypeEvent({payload}: ActionType<typeof onApplicationSortingTypeClicked>) {
  yield call(handleApplicationSortingType, payload);
}

export function* sendApplicationTopMatchFilterEvent({payload}: ActionType<typeof onApplicationTopMatchFilter>) {
  yield call(handleApplicationTopMatchFilter, payload);
}

export function* sendApplicationsFilterEvent({
  payload: {jobId, isTopMatch, applicationStatus, screeningQuestions},
}: ActionType<typeof onApplicationsFilter>) {
  yield call(handleApplicationsFilter, isTopMatch, applicationStatus, screeningQuestions, jobId);
}

export function* sendApplicationTopMatchTotal({
  payload: {total, sortCriteria, jobId},
}: ActionType<typeof getApplicationTopMatchTotal>) {
  yield call(handleApplicationTopMatchTotal, total, sortCriteria, jobId);
}

export function* sendApplicationMassUpdateSelectionEvent({
  payload,
}: ActionType<typeof onMassUpdateApplicationsSelected>) {
  yield call(handleApplicationMassUpdateSelectionClick, payload);
}

export function* sendApplicationMassUpdateStatusEvent({
  payload,
}: ActionType<typeof onMassUpdateApplicationsStatusUpdated>) {
  yield call(handleApplicationMassUpdateStatusClick, payload);
}

export function* sendAllJobsSortingTypeEvent({payload}: ActionType<typeof onJobsSortingTypeClicked>) {
  yield call(handleAllJobsSortingType, payload);
}

export function* sendPageChangeEvent({payload: {jobStatus, page}}: ActionType<typeof onPageChangeClicked>) {
  yield call(handlePageChange, jobStatus, page);
}

export function* sendLogoutEvent() {
  const {pathname} = yield select(getLocation);
  yield call(handleEmployerLogout, pathname);
}

export function* sendJobsSearchQueryEvent({payload}: ActionType<typeof onJobSearchQueried>) {
  yield call(handleJobsSearchQueryEvent, payload);
}

export function* sendSuggestedTalentClickEvent({
  payload: {talentId, jobUuid, position},
}: ActionType<typeof onSuggestedTalentClicked>) {
  yield call(
    handleCandidateClick,
    {
      jobId: jobUuid,
      position,
      candidateId: talentId,
    },
    EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
  );
}

export function* sendSuggestedTalentInviteToApplyClickEvent({
  payload,
}: ActionType<typeof onSuggestedTalentCandidateInviteToApplyClicked>) {
  yield call(handleInviteToApplyClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT);
}

export function* tagSessionWithEntityId({payload}: {type: typeof ACCOUNT_RECEIVED; payload: IUser & IAccountData}) {
  yield call(tagUserSession, payload.userInfo.entityId);
}

export function* sendApplicationTopMatchLabelEvent() {
  yield call(handleApplicationTopMatchLabel);
}

export function* sendStartJobPosting() {
  yield call(startJobPosting);
}

export function* sendCompleteJobPosting() {
  yield call(completeJobPosting);
}

export function* sendBookmarkedCandidateClickEvent({
  payload: {bookmarkedCandidate, jobUuid, position},
}: ActionType<typeof onBookmarkedCandidateClicked>) {
  yield call(
    handleCandidateClick,
    {
      jobId: jobUuid,
      position,
      candidateId: isApplication(bookmarkedCandidate) ? bookmarkedCandidate.applicant.id : bookmarkedCandidate.id,
      applicationId: bookmarkedCandidate.id,
    },
    EVENT_CATEGORY.JOB_SAVED_TAB,
  );
}

export function* sendTalentSearchQueryEvent({payload}: ActionType<typeof onTalentSearchQueried>) {
  yield call(handleTalentSearchQuery, payload);
}

export function* sendTalentSearchTalentInvitedEvent({payload}: ActionType<typeof onTalentSearchCandidateInvited>) {
  yield call(handleInviteToApplyClick, payload, EVENT_CATEGORY.JOB_TALENT_SEARCH);
}

export function* sendCandidateResumeEvent({
  type,
  payload,
}: ActionType<
  | typeof onTalentSearchCandidateResumeClicked
  | typeof onBookmarkedCandidateResumeClicked
  | typeof onSuggestedTalentsResumeClicked
  | typeof onApplicationCandidateResumeClicked
>) {
  const candidateTypes: Record<typeof type, Parameters<typeof handleCandidatesPageClick>[1]> = {
    [ON_APPLICATION_CANDIDATE_RESUME_CLICKED]: EVENT_CATEGORY.JOB_APPLICANT,
    [ON_SUGGESTED_TALENTS_RESUME_CLICKED]: EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
    [ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED]: EVENT_CATEGORY.JOB_SAVED_TAB,
    [ON_TALENT_SEARCH_CANDIDATE_RESUME_CLICKED]: EVENT_CATEGORY.JOB_TALENT_SEARCH,
  };
  yield call(handleCandidateResumeClick, payload, candidateTypes[type]);
}
export function* sendCandidatesPageEvent({
  type,
  payload,
}: ActionType<
  | typeof onTalentSearchCandidatesPageClicked
  | typeof onBookmarkedCandidatesPageClicked
  | typeof onSuggestedTalentsPageClicked
  | typeof onApplicationPageClicked
>) {
  const candidateTypes: Record<typeof type, Parameters<typeof handleCandidatesPageClick>[1]> = {
    [ON_APPLICATION_PAGE_CLICKED]: EVENT_CATEGORY.JOB_APPLICANT,
    [ON_SUGGESTED_TALENTS_PAGE_CLICKED]: EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
    [ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED]: EVENT_CATEGORY.JOB_SAVED_TAB,
    [ON_TALENT_SEARCH_CANDIDATES_PAGE_CLICKED]: EVENT_CATEGORY.JOB_TALENT_SEARCH,
  };
  yield call(handleCandidatesPageClick, {jobId: null, ...payload}, candidateTypes[type]);
}
export function* sendCandidateTabEvent({
  type,
  payload,
}: ActionType<
  | typeof onTalentSearchCandidateTabClicked
  | typeof onBookmarkedCandidateTabClicked
  | typeof onSuggestedTalentCandidateTabClicked
  | typeof onApplicationCandidateTabClicked
>) {
  const candidateTypes: Record<typeof type, Parameters<typeof handleCandidateTabClick>[1]> = {
    [ON_APPLICATION_CANDIDATE_TAB_CLICKED]: EVENT_CATEGORY.JOB_APPLICANT,
    [ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED]: EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
    [ON_BOOKMARKED_CANDIDATE_TAB_CLICKED]: EVENT_CATEGORY.JOB_SAVED_TAB,
    [ON_TALENT_SEARCH_CANDIDATE_TAB_CLICKED]: EVENT_CATEGORY.JOB_TALENT_SEARCH,
  };
  yield call(handleCandidateTabClick, payload, candidateTypes[type]);
}

export function* sendScreeningQuestionsChangeEvent({
  type,
  payload,
}: ActionType<typeof onJobPostingScreeningQuestionsAdded | typeof onJobPostingScreeningQuestionsRemoved>) {
  const eventActions: Record<typeof type, Parameters<typeof handleScreeningQuestionsChanged>[1]> = {
    [ON_JOB_POSTING_SCREENING_QUESTIONS_ADDED]: EVENT_ACTION.ADD_SCREENING_QUESTIONS,
    [ON_JOB_POSTING_SCREENING_QUESTIONS_REMOVED]: EVENT_ACTION.REMOVE_SCREENING_QUESTIONS,
  };
  yield call(handleScreeningQuestionsChanged, payload, eventActions[type]);
}

export function* analyticsSaga() {
  yield all([
    takeEvery(LOCATION_CHANGE, sendPageView),
    takeEvery(ON_NAVIGATION_MENU_LINK_CLICKED, mainNavigationMenuLinkEvent),
    takeEvery(ON_CORPPASS_GET_STARTED_CLICKED, corpPassGetStartedEvent),
    takeEvery(LoginActionTypes.LOGIN, employerLoginEvent),
    takeEvery(ON_APPLICATION_CLICKED, sendApplicationClickEvent),
    takeEvery(ON_MASS_UPDATE_APPLICATIONS_SELECTED, sendApplicationMassUpdateSelectionEvent),
    takeEvery(ON_MASS_UPDATE_APPLICATIONS_STATUS_UPDATED, sendApplicationMassUpdateStatusEvent),
    takeEvery(JOBS_SORTING_TYPE, sendAllJobsSortingTypeEvent),
    takeEvery(ON_PAGE_CHANGE_CLICKED, sendPageChangeEvent),
    takeEvery(LoginActionTypes.LOGOUT, sendLogoutEvent),
    takeEvery(ON_JOB_SEARCH_QUERY, sendJobsSearchQueryEvent),
    takeEvery(ACCOUNT_RECEIVED, tagSessionWithEntityId),
    takeEvery(ON_SUGGESTED_TALENT_CLICKED, sendSuggestedTalentClickEvent),
    takeEvery(APPLICATIONS_SORT_TYPE, sendApplicationSortingTypeEvent),
    takeEvery(ON_APPLICATIONS_FILTERED, sendApplicationsFilterEvent),
    takeEvery(ON_APPLICATIONS_TOP_MATCH_FILTERED, sendApplicationTopMatchFilterEvent),
    takeEvery(APPLICATIONS_TOP_MATCH_LABEL, sendApplicationTopMatchLabelEvent),
    takeEvery(APPLICATIONS_TOP_MATCH_TOTAL, sendApplicationTopMatchTotal),
    takeEvery(START_JOB_POSTING, sendStartJobPosting),
    takeEvery(COMPLETE_JOB_POSTING, sendCompleteJobPosting),
    takeEvery(ON_BOOKMARKED_CANDIDATE_CLICKED, sendBookmarkedCandidateClickEvent),
    takeEvery(ON_SUGGESTED_TALENT_CANDIDATE_INVITE_TO_APPLY_CLICKED, sendSuggestedTalentInviteToApplyClickEvent),
    takeEvery(ON_TALENT_SEARCH_QUERY, sendTalentSearchQueryEvent),
    takeEvery(ON_TALENT_SEARCH_CANDIDATE_INVITED, sendTalentSearchTalentInvitedEvent),

    takeEvery(
      [
        ON_APPLICATION_CANDIDATE_RESUME_CLICKED,
        ON_SUGGESTED_TALENTS_RESUME_CLICKED,
        ON_BOOKMARKED_CANDIDATE_RESUME_CLICKED,
        ON_TALENT_SEARCH_CANDIDATE_RESUME_CLICKED,
      ],
      sendCandidateResumeEvent,
    ),
    takeEvery(
      [
        ON_APPLICATION_PAGE_CLICKED,
        ON_SUGGESTED_TALENTS_PAGE_CLICKED,
        ON_BOOKMARKED_CANDIDATES_PAGE_CLICKED,
        ON_TALENT_SEARCH_CANDIDATES_PAGE_CLICKED,
      ],
      sendCandidatesPageEvent,
    ),
    takeEvery(
      [
        ON_APPLICATION_CANDIDATE_TAB_CLICKED,
        ON_SUGGESTED_TALENT_CANDIDATE_TAB_CLICKED,
        ON_BOOKMARKED_CANDIDATE_TAB_CLICKED,
        ON_TALENT_SEARCH_CANDIDATE_TAB_CLICKED,
      ],
      sendCandidateTabEvent,
    ),
    takeEvery(
      [ON_JOB_POSTING_SCREENING_QUESTIONS_ADDED, ON_JOB_POSTING_SCREENING_QUESTIONS_REMOVED],
      sendScreeningQuestionsChangeEvent,
    ),
  ]);
}
