import {accountReceived} from '@govtechsg/redux-account-middleware';
import {testSaga} from 'redux-saga-test-plan';
import {mcf} from '@mcf/constants';
import {
  getApplicationTopMatchTotal,
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationsFilter,
  onApplicationTopMatchFilter,
  onJobSearchQueried,
  onJobsSortingTypeClicked,
  onNavigationMenuLinkClicked,
  onPageChangeClicked,
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
  onBookmarkedCandidateTabClicked,
  onBookmarkedCandidateClicked,
  onTalentSearchCandidatesPageClicked,
  onTalentSearchCandidateTabClicked,
  onTalentSearchCandidateResumeClicked,
  onTalentSearchQueried,
  onMassUpdateApplicationsSelected,
  onMassUpdateApplicationsStatusUpdated,
  onTalentSearchCandidateInvited,
  onJobPostingScreeningQuestionsAdded,
  onJobPostingScreeningQuestionsRemoved,
} from '../analytics.actions';
import {
  corpPassGetStartedEvent,
  employerLoginEvent,
  mainNavigationMenuLinkEvent,
  sendAllJobsSortingTypeEvent,
  sendCandidateResumeEvent,
  sendCandidateTabEvent,
  sendCandidatesPageEvent,
  sendApplicationClickEvent,
  sendApplicationSortingTypeEvent,
  sendApplicationsFilterEvent,
  sendApplicationTopMatchFilterEvent,
  sendApplicationTopMatchLabelEvent,
  sendApplicationTopMatchTotal,
  sendJobsSearchQueryEvent,
  sendLogoutEvent,
  sendPageChangeEvent,
  sendSuggestedTalentClickEvent,
  tagSessionWithEntityId,
  sendStartJobPosting,
  sendCompleteJobPosting,
  sendBookmarkedCandidateClickEvent,
  sendApplicationMassUpdateSelectionEvent,
  sendApplicationMassUpdateStatusEvent,
  sendTalentSearchQueryEvent,
  sendTalentSearchTalentInvitedEvent,
  sendScreeningQuestionsChangeEvent,
} from '../analytics.sagas';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {handleEmployerLogin, handleEmployerLogout, tagUserSession} from '~/analytics/account';
import {
  handleApplicationSortingType,
  handleApplicationsFilter,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
  handleApplicationMassUpdateSelectionClick,
  handleApplicationMassUpdateStatusClick,
} from '~/analytics/applications';
import {
  handleCandidateTabClick,
  handleCandidatesPageClick,
  handleCandidateResumeClick,
  handleCandidateClick,
} from '~/analytics/candidates';
import {handleAllJobsSortingType, handlePageChange} from '~/analytics/filters';
import {handleCorpPassGetStartedClick} from '~/analytics/homeBanner';
import {handleJobsSearchQueryEvent} from '~/analytics/jobsSearchQuery';
import {mainNavigationMenuLinks, MAIN_NAVIGATION_MENU_LINK_TYPE} from '~/analytics/mainNavigationMenu';
import {startJobPosting, completeJobPosting} from '~/analytics/pages';
import {handleTalentSearchQuery, handleInviteToApplyClick} from '~/analytics/talents/talents';
import {handleScreeningQuestionsChanged} from '~/analytics/jobPosting';
import {ApplicationSortCriteria} from '~/flux/applications';
import {getLocation} from '~/flux/location';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';
import {DOWNLOAD_RESUME_GA_LABEL, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';
import {
  bookmarkedApplicationMock,
  bookmarkedTalentMock,
} from '~/__mocks__/bookmarkedCandidates/bookmarkedCandidates.mock';
import {CandidateTabs} from '~/components/CandidateInfo';

describe('analytics sagas', () => {
  describe('corpPassGetStartedEvent', () => {
    it('should send corpPassGetStartedEvent to GA', () => {
      testSaga(corpPassGetStartedEvent).next().call(handleCorpPassGetStartedClick).next().isDone();
    });
  });
  describe('mainNavigationMenuLinkEvent', () => {
    it('should send mainNavigationMenuLinkEvent and fires specific event to GA', () => {
      const payload = {
        title: MAIN_NAVIGATION_MENU_LINK_TYPE.WSG_LOGO,
      };
      const pathname = '/';
      testSaga(mainNavigationMenuLinkEvent, onNavigationMenuLinkClicked(payload.title))
        .next()
        .select(getLocation)
        .next({pathname})
        .call(mainNavigationMenuLinks, payload.title, pathname)
        .next()
        .isDone();
    });
  });
  describe('employerLoginEvent', () => {
    it('should send corpPassGetStartedEvent to GA', () => {
      testSaga(employerLoginEvent).next().call(handleEmployerLogin).next().isDone();
    });
  });
  describe('sendAllJobsSortingTypeEvent', () => {
    it('should sendAllJobsSortingTypeEvent and fires specific event to GA', () => {
      const payload = {
        sortType: 'descending',
      };
      testSaga(sendAllJobsSortingTypeEvent, onJobsSortingTypeClicked(payload.sortType))
        .next()
        .call(handleAllJobsSortingType, payload.sortType)
        .next()
        .isDone();
    });
  });
  describe('sendPageChangeEvent', () => {
    it('should sendPageChangeEvent to GA', () => {
      const page = 5;
      const jobStatus = 'ALL_JOBS_OPEN';
      testSaga(sendPageChangeEvent, onPageChangeClicked({jobStatus, page}))
        .next()
        .call(handlePageChange, jobStatus, page)
        .next()
        .isDone();
    });
  });
  describe('sendLogoutEvent', () => {
    it('should sendLogoutEvent to GA', () => {
      const pathname = '/jobs';
      testSaga(sendLogoutEvent)
        .next()
        .select(getLocation)
        .next({pathname})
        .call(handleEmployerLogout, pathname)
        .next()
        .isDone();
    });
  });
  describe('sendJobsSearchQueryEvent', () => {
    it('should sendJobsSearchQueryEvent to GA', () => {
      const payload = 'random query';
      testSaga(sendJobsSearchQueryEvent, onJobSearchQueried(payload))
        .next()
        .call(handleJobsSearchQueryEvent, payload)
        .next()
        .isDone();
    });
  });
  describe('sendCandidateTabEvent', () => {
    it.each`
      action                                  | payload                            | candidateType
      ${onApplicationCandidateTabClicked}     | ${CandidateTabs.EDUCATION_HISTORY} | ${EVENT_CATEGORY.JOB_APPLICANT}
      ${onSuggestedTalentCandidateTabClicked} | ${CandidateTabs.OVERVIEW}          | ${EVENT_CATEGORY.JOB_SUGGESTED_TALENT}
      ${onBookmarkedCandidateTabClicked}      | ${CandidateTabs.SKILLS}            | ${EVENT_CATEGORY.JOB_SAVED_TAB}
      ${onTalentSearchCandidateTabClicked}    | ${CandidateTabs.WORK_EXPERIENCES}  | ${EVENT_CATEGORY.JOB_TALENT_SEARCH}
    `('should sendCandidateTabEvent to GA', ({action, payload, candidateType}) => {
      testSaga(sendCandidateTabEvent, action(payload))
        .next()
        .call(handleCandidateTabClick, payload, candidateType)
        .next()
        .isDone();
    });
  });
  describe('sendCandidatesPageEvent', () => {
    it.each`
      action                                 | payload                                 | candidateType
      ${onApplicationPageClicked}            | ${{jobId: 'id1234', page: 2}}           | ${EVENT_CATEGORY.JOB_APPLICANT}
      ${onSuggestedTalentsPageClicked}       | ${{jobId: 'jobId-0987654321', page: 3}} | ${EVENT_CATEGORY.JOB_SUGGESTED_TALENT}
      ${onBookmarkedCandidatesPageClicked}   | ${{jobId: 'jobId-0987654321', page: 2}} | ${EVENT_CATEGORY.JOB_SAVED_TAB}
      ${onTalentSearchCandidatesPageClicked} | ${{page: 5}}                            | ${EVENT_CATEGORY.JOB_TALENT_SEARCH}
    `('should sendCandidatesPageEvent to GA', ({action, payload, candidateType}) => {
      testSaga(sendCandidatesPageEvent, action(...[payload.jobId, payload.page].filter(Boolean)))
        .next()
        .call(handleCandidatesPageClick, {jobId: null, ...payload}, candidateType)
        .next()
        .isDone();
    });
  });
  describe('sendCandidateResumeEvent', () => {
    it.each`
      action                                  | payload                            | candidateType
      ${onApplicationCandidateResumeClicked}  | ${DOWNLOAD_RESUME_GA_LABEL.DETAIL} | ${EVENT_CATEGORY.JOB_APPLICANT}
      ${onSuggestedTalentsResumeClicked}      | ${DOWNLOAD_RESUME_GA_LABEL.LIST}   | ${EVENT_CATEGORY.JOB_SUGGESTED_TALENT}
      ${onBookmarkedCandidateResumeClicked}   | ${DOWNLOAD_RESUME_GA_LABEL.LIST}   | ${EVENT_CATEGORY.JOB_SAVED_TAB}
      ${onTalentSearchCandidateResumeClicked} | ${DOWNLOAD_RESUME_GA_LABEL.DETAIL} | ${EVENT_CATEGORY.JOB_TALENT_SEARCH}
    `('should sendCandidateResumeEvent to GA', ({action, payload, candidateType}) => {
      testSaga(sendCandidateResumeEvent, action(payload))
        .next()
        .call(handleCandidateResumeClick, payload, candidateType)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationSortingTypeEvent', () => {
    it('should sendApplicationSortingTypeEvent to GA', () => {
      const payload = ApplicationSortCriteria.SCORES_WCC;
      testSaga(sendApplicationSortingTypeEvent, onApplicationSortingTypeClicked(payload))
        .next()
        .call(handleApplicationSortingType, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationsFilterEvent', () => {
    it('should sendApplicationsFilterEvent to GA', () => {
      const isTopMatch = true;
      const applicationStatus = [1, 2];
      const screeningQuestions = [mcf.SCREEN_QUESTION_RESPONSE.YES, mcf.SCREEN_QUESTION_RESPONSE.NO];
      const jobId = 'somejobid';

      testSaga(
        sendApplicationsFilterEvent,
        onApplicationsFilter({isTopMatch, applicationStatus, screeningQuestions, jobId}),
      )
        .next()
        .call(handleApplicationsFilter, isTopMatch, applicationStatus, screeningQuestions, jobId)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchFilterEvent', () => {
    it('should sendApplicationTopMatchFilterEvent to GA', () => {
      const payload = true;
      testSaga(sendApplicationTopMatchFilterEvent, onApplicationTopMatchFilter(payload))
        .next()
        .call(handleApplicationTopMatchFilter, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchTotal', () => {
    it('should sendApplicationTopMatchTotal to GA', () => {
      const payload = {
        jobId: 'abc1234xyz',
        sortCriteria: ApplicationSortCriteria.SCORES_WCC,
        total: 12,
      };
      testSaga(
        sendApplicationTopMatchTotal,
        getApplicationTopMatchTotal(payload.total, payload.sortCriteria, payload.jobId),
      )
        .next()
        .call(handleApplicationTopMatchTotal, payload.total, payload.sortCriteria, payload.jobId)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationTopMatchLabelEvent', () => {
    it('should sendApplicationTopMatchLabelEvent to GA', () => {
      testSaga(sendApplicationTopMatchLabelEvent).next().call(handleApplicationTopMatchLabel).next().isDone();
    });
  });
  describe('sendApplicationClickEvent', () => {
    it('should sendApplicationClickEvent to GA', () => {
      const mockApplication = applicationMock;
      const position = 2;

      const payload = {
        applicationId: applicationMock.id,
        candidateId: applicationMock.applicant.id,
        jobId: applicationMock.job.uuid,
        position,
      };
      testSaga(sendApplicationClickEvent, onApplicationClicked(mockApplication, position))
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_APPLICANT)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationMassUpdateSelectionEvent', () => {
    it('should sendApplicationMassUpdateSelectionEvent to GA', () => {
      const payload = {
        jobId: applicationMock.job.uuid,
        numberOfApplicationSelected: 12,
      };
      testSaga(
        sendApplicationMassUpdateSelectionEvent,
        onMassUpdateApplicationsSelected(payload.jobId, payload.numberOfApplicationSelected),
      )
        .next()
        .call(handleApplicationMassUpdateSelectionClick, payload)
        .next()
        .isDone();
    });
  });
  describe('sendApplicationMassUpdateStatusEvent', () => {
    it('should sendApplicationMassUpdateStatusEvent to GA', () => {
      const payload = {
        jobId: applicationMock.job.uuid,
        applicationStatus: applicationMock.statusId,
      };
      testSaga(
        sendApplicationMassUpdateStatusEvent,
        onMassUpdateApplicationsStatusUpdated(payload.jobId, payload.applicationStatus),
      )
        .next()
        .call(handleApplicationMassUpdateStatusClick, payload)
        .next()
        .isDone();
    });
  });
  describe('tagSessionWithEntityId', () => {
    it('should tagSessionWithEntityId to GA', () => {
      testSaga(tagSessionWithEntityId, accountReceived(uenUserMock))
        .next()
        .call(tagUserSession, uenUserMock.userInfo.entityId)
        .next()
        .isDone();
    });
  });
  describe('sendSuggestedTalentClickEvent', () => {
    it('should sendSuggestedTalentClickEvent to GA', () => {
      const talentIdMock = 'id';
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: talentIdMock,
      };
      testSaga(sendSuggestedTalentClickEvent, onSuggestedTalentClicked(talentIdMock, positionMock, jobUuidMock))
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SUGGESTED_TALENT)
        .next()
        .isDone();
    });
  });
  describe('sendStartJobPosting', () => {
    it('should send startTransactionalService to WOGAA', () => {
      testSaga(sendStartJobPosting).next().call(startJobPosting).next().isDone();
    });
  });
  describe('sendCompleteJobPosting', () => {
    it('should send completeTransactionalService to WOGAA', () => {
      testSaga(sendCompleteJobPosting).next().call(completeJobPosting).next().isDone();
    });
  });
  describe('sendBookmarkedCandidateClickEvent', () => {
    it('should sendBookmarkedCandidateClickEvent to GA for application', () => {
      const candidateMock = bookmarkedApplicationMock;
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: candidateMock.applicant.id,
        applicationId: candidateMock.id,
      };
      testSaga(
        sendBookmarkedCandidateClickEvent,
        onBookmarkedCandidateClicked(candidateMock, positionMock, jobUuidMock),
      )
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
    it('should sendBookmarkedCandidateClickEvent to GA for talent', () => {
      const candidateMock = bookmarkedTalentMock;
      const positionMock = 2;
      const jobUuidMock = 'abcdefghi';

      const payload = {
        jobId: jobUuidMock,
        position: positionMock,
        candidateId: candidateMock.id,
        applicationId: candidateMock.id,
      };
      testSaga(
        sendBookmarkedCandidateClickEvent,
        onBookmarkedCandidateClicked(candidateMock, positionMock, jobUuidMock),
      )
        .next()
        .call(handleCandidateClick, payload, EVENT_CATEGORY.JOB_SAVED_TAB)
        .next()
        .isDone();
    });
  });
  describe('sendTalentSearchQueryEvent', () => {
    it('should sendTalentSearchQueryEvent to GA', () => {
      const payload = 'search term';
      testSaga(sendTalentSearchQueryEvent, onTalentSearchQueried(payload))
        .next()
        .call(handleTalentSearchQuery, payload)
        .next()
        .isDone();
    });
  });
  describe('sendTalentSearchTalentInvitedEvent', () => {
    it('should sendTalentSearchTalentInvitedEvent to GA', () => {
      const candidateId = 'candidate-id';
      const jobId = 'job-id';
      const payload = {candidateId, jobId};
      testSaga(sendTalentSearchTalentInvitedEvent, onTalentSearchCandidateInvited(candidateId, jobId))
        .next()
        .call(handleInviteToApplyClick, payload, EVENT_CATEGORY.JOB_TALENT_SEARCH)
        .next()
        .isDone();
    });
  });
  describe('sendScreeningQuestionsChangeEvent', () => {
    const payload = {jobId: 'job-id', numberOfQuestions: 2};
    it.each`
      action                                   | payload    | eventAction
      ${onJobPostingScreeningQuestionsAdded}   | ${payload} | ${EVENT_ACTION.ADD_SCREENING_QUESTIONS}
      ${onJobPostingScreeningQuestionsRemoved} | ${payload} | ${EVENT_ACTION.REMOVE_SCREENING_QUESTIONS}
    `('should sendScreeningQuestionsChangeEvent to GA', ({action, payload, eventAction}) => {
      testSaga(sendScreeningQuestionsChangeEvent, action(payload.jobId, payload.numberOfQuestions))
        .next()
        .call(handleScreeningQuestionsChanged, payload, eventAction)
        .next()
        .isDone();
    });
  });
});
