import {orderBy} from 'lodash/fp';
import {ApplicationSortCriteria} from '~/flux/applications';
import {GetApplicationsApplications} from '~/graphql/__generated__/types';

export const getSortedApplications = (
  applications: GetApplicationsApplications[],
  sortCriteria: ApplicationSortCriteria,
): GetApplicationsApplications[] => {
  switch (sortCriteria) {
    case ApplicationSortCriteria.CREATED_ON:
      return orderBy<GetApplicationsApplications>(({createdOn}) => new Date(createdOn))('desc')(applications);
    default:
      return applications;
  }
};
