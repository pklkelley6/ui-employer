import {head, orderBy} from 'lodash/fp';
import {mcf} from '@mcf/constants';
import {ApplicationSortCriteria, TOP_MATCH_WCC_SCORE} from '~/flux/applications';
import {FilterBy} from '~/graphql/__generated__/types';
import {ICandidate, IEducation, IWorkExperience} from '~/graphql/candidates';
import {NULL_PLACEHOLDER} from '~/util/constants';
import {isNotNil} from '~/util/isNotNil';

export const getHighestEducation = (educationList: IEducation[]) =>
  educationList.find((education) => education.isHighest);

export const formatEducationTitle = (education: IEducation) => {
  if (!(education.ssecEqaDescription || education.ssecFosDescription)) {
    return NULL_PLACEHOLDER;
  }
  const eqaPart = education.ssecEqaDescription ? education.ssecEqaDescription : NULL_PLACEHOLDER;
  const fosPart =
    education.ssecEqaCode && mcf.ssecFosRequired(education.ssecEqaCode)
      ? education.ssecFosDescription
        ? education.ssecFosDescription
        : NULL_PLACEHOLDER
      : null;
  return fosPart ? `${eqaPart}, ${fosPart}` : eqaPart;
};

export const formatFullWorkExperience = (workExperience: IWorkExperience) =>
  workExperience.jobTitle
    ? `${workExperience.jobTitle}, ${workExperience.companyName ? workExperience.companyName : NULL_PLACEHOLDER}`
    : NULL_PLACEHOLDER;

export const getPosition = (candidate: ICandidate, full?: boolean) => {
  const {education, workExperiences} = candidate;
  const latestWorkExperience = head(sortWorkExperienceByLatest(workExperiences.filter(isNotNil)));
  if (latestWorkExperience) {
    return full ? formatFullWorkExperience(latestWorkExperience) : latestWorkExperience.jobTitle || NULL_PLACEHOLDER;
  }
  const highestEducation = getHighestEducation(education.filter(isNotNil));
  return highestEducation ? formatEducationTitle(highestEducation) : NULL_PLACEHOLDER;
};

export const getFullPosition = (candidate: ICandidate) => getPosition(candidate, true);

const MAX_YEAR = 9999;
const ZERO_YEAR = 0;

/**
 * Sort work experiences by endDate and put current work experience on top
 * Missing startDate will be placed at the bottom
 * The current work experience has endDate equal to null
 */
export const sortWorkExperienceByLatest = (workExperiences: IWorkExperience[]) =>
  orderBy<IWorkExperience>(({endDate, startDate}) => {
    const date = startDate && !endDate ? `${MAX_YEAR}` : !startDate ? ZERO_YEAR : endDate;
    return new Date(date);
  })('desc')(workExperiences);
/**
 * Sort education by yearAttained and put highest education on top
 */
export const sortEducationByLatest = (educationList: IEducation[]) =>
  orderBy<IEducation>(({isHighest, yearAttained}) => (isHighest ? MAX_YEAR : yearAttained || ZERO_YEAR))('desc')(
    educationList,
  );

export const getTopMatchFilter = (sortCriteria: ApplicationSortCriteria): FilterBy[] =>
  sortCriteria ? [{field: ApplicationSortCriteria.SCORES_WCC, floatValue: TOP_MATCH_WCC_SCORE}] : [];
