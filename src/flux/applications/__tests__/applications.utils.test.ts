import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {
  ApplicationSortCriteria,
  formatEducationTitle,
  formatFullWorkExperience,
  getHighestEducation,
  getPosition,
  getTopMatchFilter,
  sortEducationByLatest,
  sortWorkExperienceByLatest,
  TOP_MATCH_WCC_SCORE,
} from '~/flux/applications';
import {GetApplicationsEducation} from '~/graphql/__generated__/types';
import {ICandidate, IEducation, IWorkExperience} from '~/graphql/candidates';

describe('application utils', () => {
  describe('getHighestEducation()', () => {
    it('should return education with isHighest set to true', () => {
      const educationMock: IEducation = applicationMock.applicant.education[0]!;
      const educationList: IEducation[] = [
        {
          ...educationMock,
          isHighest: true,
        },
        {
          ...educationMock,
          isHighest: false,
        },
      ];

      expect(getHighestEducation(educationList)).toEqual(educationList[0]);
    });
  });

  describe('sortWorkExperienceByLatest()', () => {
    it('should return sorted workExperiences by end date and put the one with null endDate on top', () => {
      const workExperienceMock: IWorkExperience = applicationMock.applicant.workExperiences[0]!;
      const input: IWorkExperience[] = [
        {
          ...workExperienceMock,
          endDate: '2012-01-01T00:00:00.000',
        },
        {
          ...workExperienceMock,
          endDate: null,
        },
        {
          ...workExperienceMock,
          endDate: '2013-01-01T00:00:00.000',
        },
      ];

      const expectedOutput: IWorkExperience[] = [
        {
          ...workExperienceMock,
          endDate: null,
        },
        {
          ...workExperienceMock,
          endDate: '2013-01-01T00:00:00.000',
        },
        {
          ...workExperienceMock,
          endDate: '2012-01-01T00:00:00.000',
        },
      ];

      expect(sortWorkExperienceByLatest(input)).toEqual(expectedOutput);
    });
    it('should return sorted workExperiences by end date and put the one with null startDates at the bottom', () => {
      const workExperienceMock: IWorkExperience = applicationMock.applicant.workExperiences[0]!;
      const input: IWorkExperience[] = [
        {
          ...workExperienceMock,
          endDate: '2012-12-01T00:00:00.000',
          startDate: '2012-01-01T00:00:00.000',
        },
        {
          ...workExperienceMock,
          endDate: '2013-12-01T00:00:00.000',
          startDate: null,
        },
        {
          ...workExperienceMock,
          endDate: null,
          startDate: '2011-12-01T00:00:00.000',
        },
      ];

      const expectedOutput: IWorkExperience[] = [
        {
          ...workExperienceMock,
          endDate: null,
          startDate: '2011-12-01T00:00:00.000',
        },
        {
          ...workExperienceMock,
          endDate: '2012-12-01T00:00:00.000',
          startDate: '2012-01-01T00:00:00.000',
        },
        {
          ...workExperienceMock,
          endDate: '2013-12-01T00:00:00.000',
          startDate: null,
        },
      ];
      expect(sortWorkExperienceByLatest(input)).toEqual(expectedOutput);
    });
  });

  describe('getSortedEducation()', () => {
    it('should return sorted education by end date then put highest education on top', () => {
      const educationMock: IEducation = applicationMock.applicant.education[0]!;
      const input: IEducation[] = [
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2012,
        },
        {
          ...educationMock,
          isHighest: true,
          yearAttained: 2011,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2013,
        },
      ];

      const expectedOutput: IEducation[] = [
        {
          ...educationMock,
          isHighest: true,
          yearAttained: 2011,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2013,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2012,
        },
      ];

      expect(sortEducationByLatest(input)).toEqual(expectedOutput);
    });
    it('should return sorted education by end date then put highest education on top if yearAttained is optional', () => {
      const educationMock: IEducation = applicationMock.applicant.education[0]!;
      const input: IEducation[] = [
        {
          ...educationMock,
          isHighest: false,
          yearAttained: null,
        },
        {
          ...educationMock,
          isHighest: true,
          yearAttained: null,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2013,
        },
      ];

      const expectedOutput: IEducation[] = [
        {
          ...educationMock,
          isHighest: true,
          yearAttained: null,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: 2013,
        },
        {
          ...educationMock,
          isHighest: false,
          yearAttained: null,
        },
      ];

      expect(sortEducationByLatest(input)).toEqual(expectedOutput);
    });
  });

  describe('formatEducationTitle()', () => {
    const educationMock: IEducation = applicationMock.applicant.education[0]!;

    it.each`
      ssecEqaCode | ssecEqaDescription      | ssecFosDescription      | result
      ${'50'}     | ${'ssecEqaDescription'} | ${'ssecFosDescription'} | ${'ssecEqaDescription, ssecFosDescription'}
      ${'21'}     | ${'ssecEqaDescription'} | ${'ssecFosDescription'} | ${'ssecEqaDescription'}
      ${'60'}     | ${null}                 | ${null}                 | ${'(Data unavailable)'}
      ${'22'}     | ${null}                 | ${null}                 | ${'(Data unavailable)'}
      ${'70'}     | ${'ssecEqaDescription'} | ${null}                 | ${'ssecEqaDescription, (Data unavailable)'}
      ${'23'}     | ${'ssecEqaDescription'} | ${null}                 | ${'ssecEqaDescription'}
      ${'80'}     | ${null}                 | ${'ssecFosDescription'} | ${'(Data unavailable), ssecFosDescription'}
      ${'24'}     | ${null}                 | ${'ssecFosDescription'} | ${'(Data unavailable)'}
    `(
      `should render the education title correctly given ssecEqaCode($ssecEqaCode), ssecEqaDescription($ssecEqaDescription), ssecFosDescription($ssecFosDescription)`,
      ({ssecEqaCode, ssecEqaDescription, ssecFosDescription, result}) => {
        const education: GetApplicationsEducation = {
          ...educationMock,
          ssecEqaCode,
          ssecEqaDescription,
          ssecFosDescription,
        };
        expect(formatEducationTitle(education)).toEqual(result);
      },
    );
  });

  describe('formatFullWorkExperience()', () => {
    const workExperienceMock: IWorkExperience = applicationMock.applicant.workExperiences[0]!;
    it('should render job title and company name', () => {
      expect(formatFullWorkExperience(workExperienceMock)).toEqual('Some Job, Some Company');
    });
    it('should render null placeholder if jobTitle is empty', () => {
      const workExperience = {...workExperienceMock, jobTitle: ''};
      expect(formatFullWorkExperience(workExperience)).toEqual('(Data unavailable)');
    });

    it('should render null placeholder if both companyName is empty', () => {
      const workExperience = {...workExperienceMock, companyName: ''};
      expect(formatFullWorkExperience(workExperience)).toEqual('Some Job, (Data unavailable)');
    });
  });

  describe('getPosition()', () => {
    const applicantMock: ICandidate = {
      ...applicationMock.applicant,
      education: [
        {
          institution: 'School Name',
          isHighest: true,
          name: 'Optional name or description',
          ssecEqaCode: '60',
          ssecEqaDescription: "Bachelor's Degree or equivalent",
          ssecFosDescription: 'Accommodation Services',
          yearAttained: 2014,
        },
      ],
    };
    it('should render jobTitle if there is workExperience with jobTitle', () => {
      expect(getPosition(applicantMock)).toEqual('Some Job');
    });

    it('should render jobTitle and companyName if there is workExperience with jobTitle and companyName and full is true', () => {
      expect(getPosition(applicantMock, true)).toEqual('Some Job, Some Company');
    });

    it('should render null placeholder if there is workExperience with empty jobTitle', () => {
      const workExperience: IWorkExperience = {
        companyName: 'Some Company',
        endDate: '2018-01-01T00:00:00.000',
        jobDescription: 'description',
        jobTitle: '',
        startDate: '2017-01-01T00:00:00.000',
      };
      const applicant = {
        ...applicantMock,
        workExperiences: [workExperience],
      };
      expect(getPosition(applicant)).toEqual('(Data unavailable)');
    });

    it('should render education if there is no workExperience', () => {
      const applicant = {
        ...applicantMock,
        workExperiences: [],
      };
      expect(getPosition(applicant, true)).toEqual(`Bachelor's Degree or equivalent, Accommodation Services`);
    });

    it('should render null placeholder if there are no workExperiences and education', () => {
      const applicant = {
        ...applicantMock,
        education: [],
        workExperiences: [],
      };
      expect(getPosition(applicant)).toEqual('(Data unavailable)');
    });
  });

  describe('getTopMatchFilter()', () => {
    const wccFilter = {
      field: ApplicationSortCriteria.SCORES_WCC,
      floatValue: TOP_MATCH_WCC_SCORE,
    };

    it('should have both wcc filter if sort criteria is CREATED_ON', () => {
      expect(getTopMatchFilter(ApplicationSortCriteria.CREATED_ON)).toEqual([wccFilter]);
    });

    it('should have both wcc filter if sort criteria is SCORES_WCC', () => {
      expect(getTopMatchFilter(ApplicationSortCriteria.SCORES_WCC)).toEqual([wccFilter]);
    });
  });
});
