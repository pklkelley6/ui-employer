export const ON_APPLICATION_CLICKED = 'ON_APPLICATION_CLICKED';

export enum ApplicationsSelectOptions {
  SELECT_ALL,
  SELECT_NONE,
}

export const applicationsSelectOptionsData = [
  {
    label: 'Select All on this page',
    value: ApplicationsSelectOptions.SELECT_ALL,
  },
  {
    label: 'Select None',
    value: ApplicationsSelectOptions.SELECT_NONE,
  },
];

export enum ApplicationSortCriteria {
  CREATED_ON = 'createdOn',
  SCORES_WCC = 'scores.wcc',
}

export const TOP_MATCH_WCC_SCORE = 0.6;

export const CANDIDATES_SORT_CRITERIA = [
  {
    label: 'Date applied',
    value: ApplicationSortCriteria.CREATED_ON,
  },
  {
    label: 'Job match',
    value: ApplicationSortCriteria.SCORES_WCC,
  },
];
