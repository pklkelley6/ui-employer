import {mcf} from '@mcf/constants';
import {
  GetApplicationsApplications,
  GetApplicationsApplicationsForJob,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
  GetBookmarkedCandidatesBookmarkedCandidates,
  Application,
} from '~/graphql/__generated__/types';

type Applications =
  | GetApplicationsApplicationsForJob['applications']
  | GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates'];

export const isSelectedApplicationStatusUnsuccessful = (
  applications: Applications,
  selectedApplication: GetApplicationsApplications | GetBookmarkedCandidatesBookmarkedCandidates,
): boolean => {
  return (
    (applications as []).filter((application: Application) => {
      const isSelectedApplication = application.id === selectedApplication.id;
      const isSelectedApplicationStatusUnsuccessful = application.statusId === mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL;
      return isSelectedApplication && isSelectedApplicationStatusUnsuccessful;
    }).length > 0
  );
};
