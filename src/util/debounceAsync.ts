/**
 * Prevents wrapped async functions from executing if the previous async function is not yet finished
 */
export const debounceAsync = <T>(fn: (...args: T[]) => Promise<void>) => {
  let block = false;
  return async (...args: T[]) => {
    if (block) {
      return;
    }
    block = true;
    await fn(...args);
    block = false;
  };
};
