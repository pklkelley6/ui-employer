import {IJobPostAddress} from '~/services/employer/jobs.types';
import {ICompanyAddress} from '~/services/employer/company';

export const formatAddress = (address: IJobPostAddress | ICompanyAddress) => {
  let addressTokens;

  if (isJobPostAddress(address) && address.isOverseas && address.foreignAddress1) {
    addressTokens = [address.foreignAddress1, address.foreignAddress2, address.overseasCountry];
  } else {
    let floorAndUnit = '';
    if (address.floor && address.unit) {
      floorAndUnit = `#${address.floor}-${address.unit}`;
    } else if (address.floor) {
      floorAndUnit = `L${address.floor}`;
    } else if (address.unit) {
      floorAndUnit = `#${address.unit}`;
    } else {
      floorAndUnit = '';
    }

    addressTokens = [
      address.building ? `${address.building},` : null,
      address.block,
      address.street,
      floorAndUnit,
      address.postalCode,
    ];
  }

  const formatted = addressTokens.filter(Boolean).join(' ');

  return formatted;
};

const isJobPostAddress = (address: IJobPostAddress | ICompanyAddress): address is IJobPostAddress => {
  return ['isOverseas', 'foreignAddress1', 'foreignAddress2', 'overseasCountry'].every((key) =>
    Object.keys(address).includes(key),
  );
};
