/**
 *
 * CSS Polyfill - This function is only for the browser (IE) which does not support CSS variabels
 * The function below is to replace var() with css value
 * reference: https://stackoverflow.com/questions/46429937/ie11-does-a-polyfill-script-exist-for-css-variables/49203834#49203834
 */

interface ICSSVarPoly {
  init: () => boolean;
  findCSS: () => void;
  findSetters: (theCSS: string, counter: number) => void;
  updateCSS: () => void;
  ratifySetters: (varList: any[]) => void;
  replaceGetters: (curCSS: string, varList: any[]) => string;
  getLink: (url: string, counter: number, success: (counter: number, request: XMLHttpRequest) => any) => void;
  ratifiedVars?: any;
  varsByBlock?: any;
  oldCSS?: any;
}

declare let window: any;

const cssVarPoly: ICSSVarPoly = {
  init() {
    if (typeof window === 'undefined') {
      return false;
    }

    if (window.CSS && window.CSS.supports && window.CSS.supports('(--foo: red)')) {
      return false;
    }

    const body = document.querySelector('body');
    if (body) {
      body.classList.add('cssvars-polyfilled');
    }

    cssVarPoly.ratifiedVars = {};
    cssVarPoly.varsByBlock = {};
    cssVarPoly.oldCSS = {};

    cssVarPoly.findCSS();
    cssVarPoly.updateCSS();
    return true;
  },

  /**
   * Find all the css blocks, save off the content, and look for variables
   */
  findCSS() {
    const styleBlocks = document.querySelectorAll('style:not(.inserted),link[rel="stylesheet"]');

    let counter = 1;

    [].forEach.call(styleBlocks, (block: any) => {
      let theCSS;
      if (block.nodeName === 'STYLE') {
        theCSS = block.innerHTML;
        cssVarPoly.findSetters(theCSS, counter);
      } else if (block.nodeName === 'LINK') {
        cssVarPoly.getLink(block.getAttribute('href'), counter, (ctr, request) => {
          cssVarPoly.findSetters(request.responseText, ctr);
          cssVarPoly.oldCSS[ctr] = request.responseText;
          cssVarPoly.updateCSS();
        });
        theCSS = '';
      }
      cssVarPoly.oldCSS[counter] = theCSS;
      counter++;
    });
  },

  /**
   * Find all the "--variable: value" matches in a provided block of CSS and add them to the master list
   * @param {string} theCSS
   * @param {number} counter
   */
  findSetters(theCSS, counter) {
    cssVarPoly.varsByBlock[counter] = theCSS.match(/(--[a-zA-Z0-9-]+:[^;^}]+;)/g) || [];
  },

  updateCSS() {
    cssVarPoly.ratifySetters(cssVarPoly.varsByBlock);

    for (const curCSSID in cssVarPoly.oldCSS) {
      if (Object.prototype.hasOwnProperty.call(cssVarPoly.oldCSS, curCSSID)) {
        const newCSS = cssVarPoly.replaceGetters(cssVarPoly.oldCSS[curCSSID], cssVarPoly.ratifiedVars);
        const insertedElement = document.querySelector('#inserted' + curCSSID);
        if (insertedElement) {
          const newText = document.createTextNode(newCSS);
          insertedElement.appendChild(newText);
        } else {
          const style = document.createElement('style');
          style.type = 'text/css';
          const newText = document.createTextNode(newCSS);
          style.appendChild(newText);
          style.classList.add('inserted');
          style.id = 'inserted' + curCSSID;
          document.getElementsByTagName('head')[0].appendChild(style);
        }
      }
    }
  },

  /**
   * Determine the css variable name value pair and track the latest
   */
  ratifySetters(varList) {
    for (const curBlock in varList) {
      if (Object.prototype.hasOwnProperty.call(varList, curBlock)) {
        const curVars = varList[curBlock];
        curVars.forEach((theVar: any) => {
          const matches = theVar.split(/:\s*/);
          cssVarPoly.ratifiedVars[matches[0]] = matches[1].replace(/;/, '');
        });
      }
    }
    for (const theVar in cssVarPoly.ratifiedVars) {
      if (Object.prototype.hasOwnProperty.call(cssVarPoly.ratifiedVars, theVar)) {
        if (cssVarPoly.ratifiedVars[theVar].includes('var(--')) {
          cssVarPoly.ratifiedVars[theVar] = cssVarPoly.replaceGetters(
            cssVarPoly.ratifiedVars[theVar],
            cssVarPoly.ratifiedVars,
          );
        }
      }
    }
  },

  /**
   * Parse a provided block of CSS looking for a provided list of variables and replace the --var-name with the correct value
   */
  replaceGetters(curCSS, varList) {
    for (const theVar in varList) {
      if (Object.prototype.hasOwnProperty.call(varList, theVar)) {
        const getterRegex = new RegExp('var\\(\\s*' + theVar + '\\s*\\)', 'g');

        curCSS = curCSS.replace(getterRegex, varList[theVar]);

        const getterRegex2 = new RegExp('var\\(\\s*[^)]+\\s*,\\s*(.+)\\)', 'g');
        const matches = curCSS.match(getterRegex2);
        if (matches) {
          matches.forEach((match: string) => {
            curCSS = curCSS.replace(match, match.match(/var\(.+,\s*(.+)\)/)![1]);
          });
        }
      }
    }
    return curCSS;
  },

  /**
   * Get the CSS file (same domain)
   */
  getLink(url, counter, success) {
    const request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.overrideMimeType('text/css;');
    request.onload = () => {
      if (request.status >= 200 && request.status < 400) {
        if (typeof success === 'function') {
          success(counter, request);
        }
      }
    };
    request.send();
  },
};

export default cssVarPoly;
