export const convertNonBreakingSpaceToSpace = (word: string) => {
  return word.replace(/&nbsp;/g, ' ');
};
