import {decode} from 'he';
import {mapValues} from 'lodash/fp';
import {differenceInYears} from 'date-fns';
import {formatISODate} from './date';
import {IJobPost, JobStatus, JobStatusCodes} from '~/services/employer/jobs.types';
import {formatAddress} from '~/util/addresses';
import {jobToPath} from '~/util/url';

export interface IFormattedJobPost {
  address?: string;
  categories: string;
  employerName: string;
  employmentTypes: string;
  expiryDate: string;
  hiringEmployerName: string;
  id: string;
  uuid: string;
  jobApplications: string;
  jobStatus: JobStatus;
  linkUrl: string;
  title: string;
  monthlySalary: string;
  numberOfVacancies: string;
  positionLevels: string;
  originalPostedDate: string;
  postedDate: string;
  uen: string;
}

export const SALARY_UNDISCLOSED = 'Salary undisclosed';

export const transformJob = (job: IJobPost): IFormattedJobPost => {
  const employmentTypes = (job.employmentTypes || []).map((et) => et.employmentType).join(', ');
  const address = job.metadata.isHideCompanyAddress ? undefined : formatAddress(job.address);
  const categories = (job.categories || []).map((c) => c.category).join(', ');
  const positionLevels = (job.positionLevels || []).map((pos) => pos.position).join(', ');
  const originalPostedDate = formatISODate(job.metadata.originalPostingDate);
  const postedDate = formatISODate(job.metadata.newPostingDate);
  const expiryDate = formatISODate(job.metadata.expiryDate);
  const jobStatus = JobStatus[JobStatusCodes[job.status.id] as keyof typeof JobStatus] || JobStatus.Unknown;
  const monthlySalary = job.metadata.isHideSalary
    ? SALARY_UNDISCLOSED
    : `S$ ${job.salary.minimum.toLocaleString()} &ndash; ${job.salary.maximum.toLocaleString()}`;

  const companyName = job.hiringCompany ? job.hiringCompany.name : job.postedCompany.name;

  const linkUrl = jobToPath({
    company: companyName,
    jobTitle: job.title,
    uuid: job.uuid,
  });

  return {
    address,
    categories,
    employerName: job.postedCompany.name,
    employmentTypes,
    expiryDate,
    hiringEmployerName: companyName,
    id: job.metadata.jobPostId,
    jobApplications: job.metadata.totalNumberJobApplication.toString(),
    jobStatus,
    linkUrl,
    monthlySalary,
    numberOfVacancies: job.numberOfVacancies.toString(),
    positionLevels,
    originalPostedDate,
    postedDate,
    title: job.title,
    uen: job.postedCompany.uen,
    uuid: job.uuid,
  };
};

export const formatJob = (job: IJobPost): IFormattedJobPost =>
  mapValues((str) => str && decode(str), transformJob(job)) as IFormattedJobPost;

export const isSuggestedTalentUnavailable = (formattedJob: IFormattedJobPost) =>
  formattedJob.jobStatus === JobStatus.Closed &&
  differenceInYears(new Date(), new Date(formattedJob.originalPostedDate)) >= 1;

export const minJobDescriptionWordCount = (ssoc?: number) => {
  const minOneHundredWords = ['4', '5', '6', '7', '8', '9'];
  const firstDigit = ssoc && ssoc.toString()[0];

  return firstDigit && minOneHundredWords.includes(firstDigit) ? 100 : 300;
};
