declare const ga: (command: string, params: any) => any;
declare const wogaaCustom: {
  startTransactionalService: (trackingId: string) => any;
  completeTransactionalService: (trackingId: string) => any;
};

export const wogaaTrackingId = {
  jobPosting: 'employermycareersfuture-1011', // track posting of jobs by employers: https://www.pivotaltracker.com/story/show/171560901
};

export interface IPageViewFields {
  title: string;
  location?: string;
  page: string;
}

export enum EVENT_ACTION {
  LOGIN = 'login',
  LOGOUT = 'logout',
  ITEM_POSITION = 'item-position',
  PAGE_NUMBER = 'page-number',
  DOWNLOAD_RESUME_INDIVIDUAL = 'download-resume-individual',
  APPLICANTS_SORTING_TYPE = 'applicants-sorting-type',
  TOP_MATCH_FILTER_CLICKED = 'top-match-filter-clicked',
  APPLICATION_TOP_MATCH_LABEL = 'application-top-match-label',
  TOP_MATCH_TOTAL = 'top-match-total',
  APPLICANTS_FILTER = 'applicants-filter',
  CANDIDATE_TAB_CLICKED = 'candidate-tab-clicked',
  JOBS_SORTING_TYPE = 'jobs-sorting-type',
  CORPPASS_GET_STARTED = 'corppass-get-started',
  JOB_SEARCH_WITHOUT_KEYWORD = 'job-search-without-keyword',
  JOB_SEARCH_WITH_KEYWORD = 'job-search-with-keyword',
  JOB_SEARCH_WITH_JOB_ID = 'job-search-with-job-id',
  SWITCH_TO_JOBSEEKER = 'switch-to-jobseeker',
  WSG_LOGO = 'wsg-logo',
  MASS_UPDATE_SELECTION = 'mass-update-selection',
  MASS_UPDATE_STATUS = 'mass-update-status',
  INVITE_TO_APPLY = 'invite-to-apply',
  SEARCH_TERM = 'search-term',
  ADD_SCREENING_QUESTIONS = 'add-screening-qns',
  REMOVE_SCREENING_QUESTIONS = 'remove-screening-qns',
}

export enum EVENT_CATEGORY {
  ACCOUNT = 'Account',
  JOB_APPLICANT = 'Job-Applicant',
  JOB_SUGGESTED_TALENT = 'Job-SuggestedTalent',
  JOB_SAVED_TAB = 'Job-SavedTab',
  JOB_TALENT_SEARCH = 'Job-TalentSearch',
  JOB_POSTING = 'Job-Posting',
  ALL_JOBS_PAGE_SORTING = 'all-jobs-page-sorting',
  ALL_JOBS_OPEN = 'all-jobs-open',
  ALL_JOBS_CLOSED = 'all-jobs-close',
  HOME_BANNER = 'HomeBanner',
  MAIN_NAVIGATION_MENU = 'main-navigation-menu',
}

export interface IEventFields {
  eventAction: EVENT_ACTION;
  eventCategory: EVENT_CATEGORY;
  eventLabel?: string;
  eventValue?: number;
  nonInteraction?: boolean;
}

export enum CUSTOM_DIMENSIONS {
  JOB_HIT = 'dimension1',
  CANDIDATE_HIT = 'dimension2',
  ENTITY_ID_SESSION = 'dimension3',
}

export enum DOWNLOAD_RESUME_GA_LABEL {
  LIST,
  DETAIL,
}

export const googleAnalytics = {
  sendEvent: (eventFields: IEventFields) => ga('send', {...eventFields, hitType: 'event'}),
  sendPageView: (pageviewFields: IPageViewFields) => ga('send', {...pageviewFields, hitType: 'pageview'}),
  set: (fields: Record<string, any>) => ga('set', fields),
};

/*
 * Whole-of-Government Application Analytics docs:
 * Informational Services: https://docs.wogaa.sg/web-analytics/web-implement-and-verify/
 * Transactional Services: https://docs.wogaa.sg/web-analytics/web-implement-ts/
 **/
export const wogaa = (trackingId = wogaaTrackingId.jobPosting) => {
  return {
    startTransactionalService: () => {
      wogaaCustom.startTransactionalService(trackingId);
    },
    completeTransactionalService: () => {
      wogaaCustom.completeTransactionalService(trackingId);
    },
  };
};
