import {mcf} from '@mcf/constants';
import {compact} from 'lodash/fp';
import {differenceInDays, parseISO} from 'date-fns';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {IJobSkill, IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IJobPost, IJobPostAddress, IJobPostScheme, SalaryType, IJobPostInput} from '~/services/employer/jobs.types';
import {IUser} from '~/flux/account';
import {convertNonBreakingSpaceToSpace} from '~/util/convertNonBreakingSpaceToSpace';

export const transformJobPostingToFormState = (jobPosting: IJobPost): IJobPostingFormState => {
  const {
    address,
    title,
    description,
    ssocCode,
    hiringCompany,
    skills,
    metadata,
    numberOfVacancies,
    categories,
    positionLevels,
    minimumYearsExperience,
    employmentTypes,
    ssecEqa,
    ssecFos,
    salary,
    schemes,
    otherRequirements,
    screeningQuestions,
  } = jobPosting;

  const selectedSkills = skills.map((skill: IJobSkill) => ({
    ...skill,
    selected: true,
  }));

  const selectedSchemes: IJobPostingFormState['keyInformation']['schemes'] = schemes.map(
    ({scheme, subScheme}: IJobPostScheme) => {
      return {
        id: scheme.id,
        subSchemeId: subScheme?.id,
        selected: true,
      };
    },
  );

  const getLocationType = (jobPostAddress: IJobPostAddress) => {
    const {postalCode, block, street, isOverseas} = jobPostAddress;
    if (!postalCode && !block && !street && !isOverseas) {
      return LocationType.Multiple;
    } else {
      return LocationType.None;
    }
  };

  return {
    jobDescription: {
      description,
      occupation: ssocCode,
      otherRequirements,
      isThirdPartyEmployer: Boolean(hiringCompany?.uen),
      thirdPartyEmployer: hiringCompany
        ? {
            entityId: hiringCompany.uen,
            name: hiringCompany.name,
            industry: hiringCompany.ssicCode,
          }
        : {},
      title,
    },
    jobPostingSkills: {
      addedSkills: selectedSkills,
      recommendedSkills: [],
    },
    keyInformation: {
      newPostingDate: metadata.newPostingDate,
      employmentType: employmentTypes.map((type) => type.id),
      fieldOfStudy: ssecFos,
      jobCategories: categories.map((category) => category.id),
      jobPostDuration: differenceInDays(parseISO(metadata.expiryDate), parseISO(metadata.newPostingDate)),
      maximumSalary: salary.maximum,
      minimumSalary: salary.minimum,
      minimumYearsExperience,
      numberOfVacancies,
      positionLevel: positionLevels.map((position) => position.id)[0],
      qualification: ssecEqa,
      schemes: selectedSchemes,
    },
    workplaceDetails: {
      ...address,
      location: address.isOverseas ? Location.Overseas : Location.Local,
      locationType: getLocationType(address),
    },
    screeningQuestions: {
      includeScreeningQuestions: Boolean(screeningQuestions && screeningQuestions.length),
      questions:
        screeningQuestions && screeningQuestions.length ? screeningQuestions.map(({question}) => question) : [],
    },
  };
};

export const getSelectedSkillIds = (skills: IPillSkill[]) => {
  return skills.filter((skill) => skill.selected).map(({uuid}) => ({uuid}));
};

export const transformFormStateToJobPostInput = (formState: IJobPostingFormState, account: IUser): IJobPostInput => {
  const {jobDescription, keyInformation, jobPostingSkills, workplaceDetails, screeningQuestions} = formState;
  const {title, description, occupation, isThirdPartyEmployer, thirdPartyEmployer} = jobDescription;
  const {
    jobPostDuration,
    numberOfVacancies,
    jobCategories,
    positionLevel,
    minimumYearsExperience,
    employmentType,
    qualification,
    fieldOfStudy,
    minimumSalary,
    maximumSalary,
    schemes,
  } = keyInformation;
  const {location, locationType, ...address} = workplaceDetails;
  return {
    address:
      location === Location.Local && locationType === LocationType.Multiple
        ? undefined
        : {
            ...address,
            isOverseas: location === Location.Overseas,
          },
    categories: jobCategories.map((category) => ({id: category})),
    description: description && convertNonBreakingSpaceToSpace(description),
    employmentTypes: employmentType
      ? employmentType.map((employment) => {
          return {id: employment};
        })
      : [],
    hiringCompany: isThirdPartyEmployer && thirdPartyEmployer.entityId ? {uen: thirdPartyEmployer.entityId} : undefined,
    jobPostDuration,
    minimumYearsExperience,
    numberOfVacancies,
    positionLevels: positionLevel ? [{id: positionLevel}] : [],
    postedCompany: {uen: account.userInfo.entityId},
    salary: {
      maximum: maximumSalary,
      minimum: minimumSalary,
      type: {id: SalaryType.Monthly},
    },
    schemes: schemes.filter((scheme) => scheme.selected).map(({id, subSchemeId}) => ({id, subSchemeId})),
    skills: getSelectedSkillIds(jobPostingSkills.addedSkills).concat(
      getSelectedSkillIds(jobPostingSkills.recommendedSkills),
    ),
    ssecEqa: qualification,
    ssecFos: qualification && mcf.ssecFosRequired(qualification) ? fieldOfStudy : undefined,
    ssocCode: occupation,
    title,
    screeningQuestions:
      screeningQuestions.includeScreeningQuestions && compact(screeningQuestions.questions).length > 0
        ? compact(screeningQuestions.questions).map((question) => ({question}))
        : undefined,
  };
};
