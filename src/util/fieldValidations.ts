/*
 * Validation functions in this file are only intended to be used for validating react-final-form fields
 * https://github.com/final-form/react-final-form#validate-value-any-allvalues-object-meta-fieldstate--any
 */
import {get, includes, isArray, isString, isNil} from 'lodash/fp';
import {FieldState} from 'final-form';

export const composeValidators =
  (...validators: Array<(value: any, allValues?: any, meta?: FieldState<any>) => any>) =>
  (value: any, allValues?: any, meta?: FieldState<any>) =>
    validators.reduce((error, validator) => error || validator(value, allValues, meta), undefined);

export const lessThan = (max: number, message?: string) => (value?: number) =>
  isNil(value) || value < max ? undefined : message ? message : `Please enter a value less than ${max}`;

export const greaterThan = (min: number, message?: string) => (value?: number) =>
  isNil(value) || value > min ? undefined : message ? message : `This number should be more than ${min}`;

export const lessThanEqual = (max: number, message?: string) => (value?: number) =>
  isNil(value) || value <= max ? undefined : message ? message : `This number should not be more than ${max}`;

export const greaterThanEqual = (min: number, message?: string) => (value?: number) =>
  isNil(value) || value >= min ? undefined : message ? message : `This number should be more than or equal to ${min}`;

export const positive = (value: number) => (value >= 0 ? undefined : 'Please enter a non-negative value');

const isValidValue = (value: any) => typeof value === 'boolean' || value === 0 || value;

export const required = (message: any) => (value: any) => isValidValue(value) ? undefined : message;

export const isChecked = (message: any) => (value: boolean) => value ? undefined : message;

export const defaultRequired = required('Please fill this in');

export const searchableFieldRequired = required('Please type and select');

export const greaterThanEqualField = (pathToField: string, message?: string) => (value: any, allValues: any) => {
  const pathValue = get(pathToField, allValues);
  const errorMessage = message ? message : `This number should be more than ${pathValue}`;
  return pathValue || pathValue === 0 ? greaterThanEqual(pathValue, errorMessage)(value) : undefined;
};

export const lessThanEqualField = (pathToField: string, message?: string) => (value: any, allValues: any) => {
  const pathValue = get(pathToField, allValues);
  const errorMessage = message ? message : `Please enter a value less than ${get(pathToField, allValues)}`;
  return pathValue || pathValue === 0 ? lessThanEqual(pathValue, errorMessage)(value) : undefined;
};

export const requiredIfFieldHasInput = (pathToField: string) => (value: any, allValues: any) => {
  const pathValue = get(pathToField, allValues);
  return pathValue !== undefined ? defaultRequired(value) : undefined;
};

interface IOneOf {
  (valueList: string[]): (value: string) => string | undefined;
  (valueList: number[]): (value: number) => string | undefined;
}

export const oneOf: IOneOf = (valueList: Array<string | number>) => (value: string | number) => {
  return includes(value, valueList) ? undefined : 'Please select an option';
};

export const maxLength = (max: number, message?: string) => (value?: any) =>
  isNil(value) || ((isArray(value) || isString(value)) && value.length <= max)
    ? undefined
    : message
    ? message
    : `You can only add a maximum of ${max} items`;

export const minLength = (min: number, message?: string) => (value: any) =>
  (isArray(value) || isString(value)) && value.length >= min
    ? undefined
    : message
    ? message
    : `You must add a minimum of ${min} items`;

export const validateIf =
  (pathToField: any, predicate: (value: any) => boolean, validation: (value: any, allValues?: any) => any) =>
  (value: any, allValues: any) => {
    const pathValue = get(pathToField, allValues);
    return predicate(pathValue) ? validation(value) : undefined;
  };

export const exactLength = (length: number, message: string) => (value: string | number) =>
  `${value}`.length === length ? undefined : message;

export const regex = (message: string) => (pattern: RegExp) => (value?: string) => {
  const match = (value || '').match(pattern);
  return !match ? message : undefined;
};

const validEmailRegex =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const email = regex('Please check your email address')(validEmailRegex);

export const contactNumber = (str: string) => {
  if (!['6', '8', '9'].includes(str[0])) {
    return 'Please check that your phone number starts with 6, 8 or 9';
  }
  return exactLength(8, 'Please check that your phone number has 8 digits')(str);
};
