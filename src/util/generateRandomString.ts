const browserCrypto = window.crypto || window.msCrypto; // for IE 11

// NOTE: Via https://stackoverflow.com/a/27747377/995735
export const generateRandomString = (strLength = 10) => {
  const uint8 = new Uint8Array(strLength / 2);
  browserCrypto.getRandomValues(uint8);
  return Array.from(uint8, (val) => val.toString(16).padStart(2, '0')).join('');
};

// NOTE: Via https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
export const generateSha256FromString = async (message: string) => {
  const msgUint8 = new TextEncoder().encode(message);
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join('');
  return hashHex;
};

export const generateRandomSha256 = async () => {
  return generateSha256FromString(generateRandomString(32));
};
