export const getBlobObject = async (blobUrl: string) => {
  if (/^blob:.+/.test(blobUrl)) {
    const response = await fetch(blobUrl);
    return await response.blob();
  } else {
    return undefined;
  }
};
