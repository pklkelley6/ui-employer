import {flow} from 'lodash';
import {STOP_WORDS} from './stopwordList';

export const removeStopWords = (str = '') => {
  const strArr = str.toLowerCase().split(' ');
  const sanitisedArr = strArr.filter((word) => {
    return !STOP_WORDS.includes(word);
  });
  return sanitisedArr.join(' ');
};

export const removeWordsInBracket = (str = '') => {
  const re = /\(.*?\)/g;
  return str.replace(re, '');
};

export const removePunctuations = (str = '') => {
  // '-' is ignored in this case
  const re = /[~`!@#$%^&*(){}\[\];:"'<,.>?\/\\|_+=]/g;
  return str.replace(re, '');
};

export const removeExcessWhitespaces = (str = '') => {
  const re = /\s+/g;
  return str.replace(re, ' ');
};

export const removeRepeatedHyphens = (str = '') => {
  const re = /\-+/g;
  return str.replace(re, '-');
};

export const joinWords = (str = '') => {
  const re = /\s/g;
  return str.trim().replace(re, '-');
};

export const cleanWord = (str?: string) => {
  const currentStr = `${str || ''}`;
  return flow([
    removeWordsInBracket,
    removeStopWords,
    removePunctuations,
    removeExcessWhitespaces,
    joinWords,
    removeRepeatedHyphens,
    encodeURIComponent,
  ])(currentStr);
};

export const pathToJobUuid = (path: string): string => {
  const reJobURLTitleUUID = /(?:.*-)?(.*$)/i;
  const jobUuid = reJobURLTitleUUID.exec(path);
  return jobUuid?.[1] ?? '';
};

export const jobToPath = ({jobTitle, company, uuid}: {jobTitle: string; company: string; uuid: string}) => {
  const processedJobTitle = cleanWord(jobTitle);
  const processedCompany = cleanWord(company);
  const urlSegment = `${processedJobTitle && processedJobTitle.concat('-')}${
    processedCompany && processedCompany.concat('-')
  }${uuid}`;
  return `/jobs/${urlSegment}`;
};
