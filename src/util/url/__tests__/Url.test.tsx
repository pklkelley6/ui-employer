import {
  cleanWord,
  jobToPath,
  joinWords,
  pathToJobUuid,
  removeExcessWhitespaces,
  removePunctuations,
  removeRepeatedHyphens,
  removeStopWords,
  removeWordsInBracket,
} from '../Url';

describe('components/Utility/Url', () => {
  describe('pathToJobUuid', () => {
    it('works for just jobId', () => {
      const path = '1580bcedd5fafbcf04ee47e918cf25d2';
      const jobId = pathToJobUuid(path);
      expect(jobId).toEqual('1580bcedd5fafbcf04ee47e918cf25d2');
    });

    it('works for sample job title', () => {
      const path = 'java-software-developer-1580bcedd5fafbcf04ee47e918cf25d2';
      const jobId = pathToJobUuid(path);
      expect(jobId).toEqual('1580bcedd5fafbcf04ee47e918cf25d2');
    });

    it('works for path with special characters', () => {
      const path = '-?!@#$%^,as;ld,asjnakjsndkankjnskjdnk-akjsndkjan-&*()_-----1580bcedd5fafbcf04ee47e918cf25d2';
      const jobId = pathToJobUuid(path);
      expect(jobId).toEqual('1580bcedd5fafbcf04ee47e918cf25d2');
    });

    it('works for all official language and weird characters', () => {
      expect(pathToJobUuid('🤡软件开发人员–谷歌©-1580bcedd5fafbcf04ee47e918cf25d2')).toEqual(
        '1580bcedd5fafbcf04ee47e918cf25d2',
      );
      expect(pathToJobUuid('🤡सॉफ्टवेयर डेवलपर–गूगल©-1580bcedd5fafbcf04ee47e918cf25d2')).toEqual(
        '1580bcedd5fafbcf04ee47e918cf25d2',
      );
      expect(pathToJobUuid('🤡pemaju-perisian–google©-1580bcedd5fafbcf04ee47e918cf25d2')).toEqual(
        '1580bcedd5fafbcf04ee47e918cf25d2',
      );
    });
  });

  describe('removeStopWords', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(removeStopWords()).toEqual('');
      expect(removeStopWords(undefined)).toEqual('');
    });

    it('does nothing if there is no stop words', () => {
      const input = 'software developer needed';
      expect(removeStopWords(input)).toEqual('software developer needed');
    });

    it('removes word from the list', () => {
      const input = 'software developer needed to make coffee';
      expect(removeStopWords(input)).toEqual('software developer needed make coffee');
    });

    it('removes multiple words from the list', () => {
      const input = 'software developer needed to make coffee for directors';
      expect(removeStopWords(input)).toEqual('software developer needed make coffee directors');
    });
  });

  describe('removeWordsInBracket', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(removeWordsInBracket()).toEqual('');
      expect(removeWordsInBracket(undefined)).toEqual('');
    });

    it('does nothing if there is no words in bracket', () => {
      const input = 'software developer needed';
      expect(removeWordsInBracket(input)).toEqual('software developer needed');
    });

    it('removes empty bracket', () => {
      const input = 'software developer needed to make () coffee';
      expect(removeWordsInBracket(input)).toEqual('software developer needed to make  coffee');
    });

    it('removes word in bracket', () => {
      const input = '(software) developer needed to make coffee';
      expect(removeWordsInBracket(input)).toEqual(' developer needed to make coffee');
    });

    it('removes multiple words in bracket', () => {
      const input = '(software) developer needed (to make coffee)';
      expect(removeWordsInBracket(input)).toEqual(' developer needed ');
    });
  });

  describe('removePunctuations', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(removePunctuations()).toEqual('');
      expect(removePunctuations(undefined)).toEqual('');
    });

    it('does nothing if there is no punctuations', () => {
      const input = 'software developer needed';
      expect(removePunctuations(input)).toEqual('software developer needed');
    });

    it('removes multiple punctuations in different places', () => {
      const input = 'soft!w@are deve$lop^er n&eeded to make coffee';
      expect(removePunctuations(input)).toEqual('software developer needed to make coffee');
    });
  });

  describe('removeExcessWhitespaces', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(removeExcessWhitespaces()).toEqual('');
      expect(removeExcessWhitespaces(undefined)).toEqual('');
    });

    it('removes excess whitespace between words', () => {
      const input = 'software   developer            needed';
      expect(removeExcessWhitespaces(input)).toEqual('software developer needed');
    });

    it('removes excess whitespace before and after sentence, except one', () => {
      const input = '   test   ';
      expect(removeExcessWhitespaces(input)).toEqual(' test ');
    });
  });

  describe('removeRepeatedHyphens', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(removeRepeatedHyphens()).toEqual('');
      expect(removeRepeatedHyphens(undefined)).toEqual('');
    });

    it('removes excess whitespace between words', () => {
      const input = 'software---developer--------needed';
      expect(removeRepeatedHyphens(input)).toEqual('software-developer-needed');
    });

    it('does not remove single hyphens', () => {
      const input = 'software-developer-needed';
      expect(removeRepeatedHyphens(input)).toEqual('software-developer-needed');
    });
  });

  describe('joinWords', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(joinWords()).toEqual('');
      expect(joinWords(undefined)).toEqual('');
    });

    it('joins words with hypehen', () => {
      const input = 'software developer needed';
      expect(joinWords(input)).toEqual('software-developer-needed');
    });

    it('trims whitespace before joining', () => {
      const input = '   test engineer  ';
      expect(joinWords(input)).toEqual('test-engineer');
    });
  });

  describe('cleanWord', () => {
    it('returns empty string on empty, null or undefined string', () => {
      expect(cleanWord()).toEqual('');
      expect(cleanWord(undefined)).toEqual('');
    });

    it('cleans the input with all filters', () => {
      const input = 'AIRYA - CRESTAR (INTERNATIONAL) PTE. LTD.';
      expect(cleanWord(input)).toEqual('airya-crestar');
    });
  });

  describe('jobToPath', () => {
    it('works for just uuid', () => {
      const job = {
        company: '',
        jobTitle: '',
        uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
      };
      expect(jobToPath(job)).toEqual('/jobs/1580bcedd5fafbcf04ee47e918cf25d2');
    });

    it('works for uuid and job title', () => {
      const job = {
        company: '',
        jobTitle: 'DIRECTOR OF OPERATIONS',
        uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
      };
      expect(jobToPath(job)).toEqual('/jobs/director-operations-1580bcedd5fafbcf04ee47e918cf25d2');
    });

    it('works for uuid, job title and company name', () => {
      const job = {
        company: 'AIRYA CRESTAR INTERNATIONAL PTE. LTD.',
        jobTitle: 'DIRECTOR OF OPERATIONS',
        uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
      };
      expect(jobToPath(job)).toEqual(
        '/jobs/director-operations-airya-crestar-international-1580bcedd5fafbcf04ee47e918cf25d2',
      );
    });

    it('works for all official language and weird characters', () => {
      expect(
        jobToPath({
          company: '谷歌©',
          jobTitle: '软件开发人员🤡',
          uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
        }),
      ).toEqual(
        '/jobs/%E8%BD%AF%E4%BB%B6%E5%BC%80%E5%8F%91%E4%BA%BA%E5%91%98%F0%9F%A4%A1-%E8%B0%B7%E6%AD%8C%C2%A9-1580bcedd5fafbcf04ee47e918cf25d2',
      );

      expect(
        jobToPath({
          company: 'वलपर–गूगल©',
          jobTitle: '🤡सॉफ्टवेयर',
          uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
        }),
      ).toEqual(
        '/jobs/%F0%9F%A4%A1%E0%A4%B8%E0%A5%89%E0%A4%AB%E0%A5%8D%E0%A4%9F%E0%A4%B5%E0%A5%87%E0%A4%AF%E0%A4%B0-%E0%A4%B5%E0%A4%B2%E0%A4%AA%E0%A4%B0%E2%80%93%E0%A4%97%E0%A5%82%E0%A4%97%E0%A4%B2%C2%A9-1580bcedd5fafbcf04ee47e918cf25d2',
      );

      expect(
        jobToPath({
          company: 'google©',
          jobTitle: 'pemaju-perisian🤡',
          uuid: '1580bcedd5fafbcf04ee47e918cf25d2',
        }),
      ).toEqual('/jobs/pemaju-perisian%F0%9F%A4%A1-google%C2%A9-1580bcedd5fafbcf04ee47e918cf25d2');
    });
  });
});
