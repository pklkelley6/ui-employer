import {isObject} from 'lodash/fp';

export const getFromStorage = (prefix: string, id: string) => localStorage.getItem(`${prefix}-${id}`);

export const saveToStorage = (prefix: string, id: string, value: any) => {
  localStorage.setItem(`${prefix}-${id}`, isObject(value) ? JSON.stringify(value) : value);
};
