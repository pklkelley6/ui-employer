import {saveAs} from 'file-saver';

/**
 * This function makes it possible to wait for file downloads to finish before doing another async operation.
 * This is for behaviors in firefox and safari where fetch requests inside onClick are cancelled
 * and returns a network error when a file download happens at the same time.
 */
export const downloadFile = async (filePath: string, fileName: string) => {
  const response = await fetch(filePath, {
    credentials: 'include',
  });
  const blob = await response.blob();
  if (!response.ok) {
    throw new Error(await response.text());
  }
  saveAs(blob, fileName);
};

export const formatFileSize = (size: number) => {
  const k = 1024;
  const sizeUnits = ['bytes', 'KB', 'MB', 'GB'];
  if (size < k) {
    return size + sizeUnits[0];
  } else {
    const exponent = Math.floor(Math.log(size) / Math.log(k));
    const i = exponent < sizeUnits.length ? exponent : sizeUnits.length - 1;
    return (size / Math.pow(k, i)).toFixed(1) + sizeUnits[i];
  }
};
