import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {formatAddress} from '~/util/addresses';
import {IJobPostAddress} from '~/services/employer/jobs.types';
import {ICompanyAddress, CompanyAddressType} from '~/services/employer/company';

describe('util/addresses', () => {
  it('should format a job local address', () => {
    const okJob = jobPostMock;
    const address = formatAddress(okJob.address);
    expect(address).toBe('Sandcrawler, 9 WOODLANDS AVENUE 9 #8-77 738964');
  });

  it('should format an overseas address', () => {
    const overseasAddress: IJobPostAddress = {
      block: '9',
      foreignAddress1: 'foo',
      foreignAddress2: 'bar',
      isOverseas: true,
      overseasCountry: 'Malaysia',
      postalCode: '738964',
      street: 'WOODLANDS AVENUE 9',
    };
    const address = formatAddress(overseasAddress);
    expect(address).toBe('foo bar Malaysia');
  });

  it('should display the local address if overseas flag is true and foreign address is empty', () => {
    const overseasAddress: IJobPostAddress = {
      block: '9',
      foreignAddress1: '',
      foreignAddress2: '',
      isOverseas: true,
      overseasCountry: 'Australia',
      postalCode: '738964',
      street: 'WOODLANDS AVENUE 9',
    };
    const address = formatAddress(overseasAddress);
    expect(address).toBe('9 WOODLANDS AVENUE 9 738964');
  });

  it('should format a company address', () => {
    const companyAddress: ICompanyAddress = {
      block: '9',
      building: 'w building',
      floor: '10',
      postalCode: '738964',
      street: 'WOODLANDS AVENUE 9',
      purpose: CompanyAddressType.OPERATING,
    };
    const address = formatAddress(companyAddress);
    expect(address).toBe('w building, 9 WOODLANDS AVENUE 9 L10 738964');
  });
});
