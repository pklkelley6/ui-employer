import {differenceInDays, parseISO} from 'date-fns';
import {mcf} from '@mcf/constants';
import {jobPostMock, jobPostInputMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IJobPost, IJobPostInput} from '~/services/employer/jobs.types';
import {transformJobPostingToFormState, transformFormStateToJobPostInput} from '~/util/transformJobPosting';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

describe('transformJobPosting', () => {
  const newPostingDate = '2020-01-01';

  const mockedJobPost: IJobPost = {
    ...jobPostMock,
    address: {
      ...jobPostMock.address,
      isOverseas: false,
      block: '9',
      street: 'WOODLANDS AVENUE 9',
      building: 'Sandcrawler',
    },
    hiringCompany: {
      ...jobPostMock.hiringCompany,
      name: 'Republic Polytechnic',
      ssicCode: '79101',
      uen: 'T08GB0046G',
    },
    minimumYearsExperience: 1,
    schemes: [
      {
        scheme: mcf.SCHEMES[0],
      },
      {
        scheme: mcf.SCHEMES[1],
        subScheme: {
          id: 188,
          programme: 'some-program',
          schemeId: mcf.SCHEMES[1].id,
        },
      },
    ],
    ssecEqa: '42',
    ssecFos: '0521',
    metadata: {
      ...jobPostMock.metadata,
      newPostingDate: newPostingDate,
      expiryDate: '2020-01-31',
    },
    screeningQuestions: [{question: 'question'}],
  };

  const mockedFormState: IJobPostingFormState = {
    jobDescription: {
      description: mockedJobPost.description,
      occupation: mockedJobPost.ssocCode,
      isThirdPartyEmployer: Boolean(mockedJobPost.hiringCompany?.uen),
      thirdPartyEmployer: mockedJobPost.hiringCompany
        ? {
            entityId: mockedJobPost.hiringCompany.uen,
            name: mockedJobPost.hiringCompany.name,
            industry: '79101',
          }
        : {},
      title: mockedJobPost.title,
    },
    jobPostingSkills: {
      addedSkills: [
        ...mockedJobPost.skills.map((skill) => ({
          ...skill,
          selected: true,
        })),
      ],
      recommendedSkills: [],
    },
    keyInformation: {
      employmentType: mockedJobPost.employmentTypes ? mockedJobPost.employmentTypes.map((type) => type.id) : [],
      fieldOfStudy: mockedJobPost.ssecFos,
      jobCategories: mockedJobPost.categories ? mockedJobPost.categories.map((category) => category.id) : [],
      jobPostDuration: differenceInDays(
        parseISO(mockedJobPost.metadata.expiryDate),
        parseISO(mockedJobPost.metadata.newPostingDate),
      ),
      maximumSalary: mockedJobPost.salary.maximum,
      minimumSalary: mockedJobPost.salary.minimum,
      minimumYearsExperience: 1,
      numberOfVacancies: 1,
      positionLevel: mockedJobPost.positionLevels[0].id,
      qualification: mockedJobPost.ssecEqa,
      schemes: [
        {id: mcf.SCHEMES[0].id, selected: true},
        {id: mcf.SCHEMES[1].id, subSchemeId: 188, selected: true},
      ],
      newPostingDate: newPostingDate,
    },
    workplaceDetails: {
      ...mockedJobPost.address,
      location: Location.Local,
      locationType: LocationType.None,
    },
    screeningQuestions: {
      includeScreeningQuestions: true,
      questions: ['question'],
    },
  };

  const mockedJobPostInput: IJobPostInput = {
    ...jobPostInputMock,
    title: mockedJobPost.title,
    description: mockedJobPost.description,
    jobPostDuration: 30,
    minimumYearsExperience: mockedJobPost.minimumYearsExperience,
    ssocCode: mockedJobPost.ssocCode,
    numberOfVacancies: mockedJobPost.numberOfVacancies,
    categories: mockedJobPost.categories.map(({id}) => ({id})),
    employmentTypes: mockedJobPost.employmentTypes.map(({id}) => ({id})),
    positionLevels: mockedJobPost.positionLevels.map(({id}) => ({id})),
    skills: mockedJobPost.skills.map(({uuid}) => ({uuid})),
    schemes: mockedJobPost.schemes.map((scheme) => ({id: scheme.scheme.id, subSchemeId: scheme.subScheme?.id})),
    address: mockedJobPost.address,
    salary: {
      ...mockedJobPost.salary,
      type: {id: 4},
    },
    ssecEqa: mockedJobPost.ssecEqa,
    ssecFos: mockedJobPost.ssecFos,
    screeningQuestions: [
      {
        question: 'question',
      },
    ],
  };

  describe('transformJobPostingToFormState', () => {
    it('should transform job post to form state', () => {
      expect(transformJobPostingToFormState(mockedJobPost)).toEqual(mockedFormState);
    });

    it('should transform job post to form state when location is overseas', () => {
      const jobPost = {
        ...mockedJobPost,
        address: {
          isOverseas: true,
          overseasCountry: 'some country',
          foreignAddress1: 'some foreign address 1',
          foreignAddress2: 'some foreign address 2',
        },
      };
      const expectedFormState = {
        ...mockedFormState,
        workplaceDetails: {
          isOverseas: true,
          location: Location.Overseas,
          locationType: LocationType.None,
          overseasCountry: 'some country',
          foreignAddress1: 'some foreign address 1',
          foreignAddress2: 'some foreign address 2',
        },
      };
      expect(transformJobPostingToFormState(jobPost)).toEqual(expectedFormState);
    });
  });

  describe('transformFormStateToJobPostInput', () => {
    it('should transform form state to job post input', () => {
      expect(transformFormStateToJobPostInput(mockedFormState, uenUserMock)).toEqual(mockedJobPostInput);
    });

    it('should transform form state to job post input when location is overseas and locationType is multiple', () => {
      const formState = {
        ...mockedFormState,
        workplaceDetails: {
          isOverseas: true,
          location: Location.Overseas,
          locationType: LocationType.Multiple,
          overseasCountry: 'some country',
          foreignAddress1: 'some foreign address 1',
          foreignAddress2: 'some foreign address 2',
        },
      };

      const expectedJobPostInput = {
        ...mockedJobPostInput,
        address: {
          isOverseas: true,
          overseasCountry: 'some country',
          foreignAddress1: 'some foreign address 1',
          foreignAddress2: 'some foreign address 2',
        },
      };

      expect(transformFormStateToJobPostInput(formState, uenUserMock)).toEqual(expectedJobPostInput);
    });

    it('should remove screeningQuestions if includeScreeningQuestions is false', () => {
      const formState = {
        ...mockedFormState,
        screeningQuestions: {
          includeScreeningQuestions: false,
          questions: ['question'],
        },
      };

      const expectedJobPostInput = {
        ...mockedJobPostInput,
        screeningQuestions: undefined,
      };

      expect(transformFormStateToJobPostInput(formState, uenUserMock)).toEqual(expectedJobPostInput);
    });

    it('should remove screeningQuestions if there are no screening questions', () => {
      const formState = {
        ...mockedFormState,
        screeningQuestions: {
          includeScreeningQuestions: false,
          questions: ['question'],
        },
      };

      const expectedJobPostInput = {
        ...mockedJobPostInput,
        screeningQuestions: undefined,
      };

      expect(transformFormStateToJobPostInput(formState, uenUserMock)).toEqual(expectedJobPostInput);
    });
  });
});
