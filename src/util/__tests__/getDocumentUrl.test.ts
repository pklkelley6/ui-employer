import {getDocumentUrl} from '../getDocumentUrl';

jest.mock('~/config', () => ({
  config: {
    url: {
      apiProfile: '/profile',
    },
  },
}));

describe('util/getDocumentUrl', () => {
  it('should return document url', () => {
    expect(getDocumentUrl('/documents/123')).toEqual('/profile/documents/123');
  });
});
