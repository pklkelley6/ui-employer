import {convertDataToBase64} from '~/util/convertDataToBase64';

describe('convertDataToBase64 util', () => {
  it('should convert blob to base64', async () => {
    const blobInput = new Blob(['testing'], {type: 'image/png'});
    const result = await convertDataToBase64(blobInput);
    expect(result).toEqual('data:image/png;base64,dGVzdGluZw==');
  });
});
