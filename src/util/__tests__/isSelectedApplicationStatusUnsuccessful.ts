import {mcf} from '@mcf/constants';
import {isSelectedApplicationStatusUnsuccessful} from '~/util/isSelectedApplicationStatusUnsuccessful';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';

describe('util/isSelectedApplicationStatusUnsuccessful', () => {
  it('should return a selected application with status unsuccessful', () => {
    const applicationMockUnsuccessfulStatus = {
      ...applicationMock,
      statusId: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
    };

    const applications = [
      {
        ...applicationMockUnsuccessfulStatus,
      },
      {
        ...applicationMock,
      },
    ];
    const isApplicationUnsuccessful = isSelectedApplicationStatusUnsuccessful(
      applications,
      applicationMockUnsuccessfulStatus,
    );
    expect(isApplicationUnsuccessful).toBe(true);
  });
  it('should not return selected application if status is not unsuccessful', () => {
    const applicationMockUnsuccessfulStatus = {
      ...applicationMock,
      statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };

    const applications = [
      {
        ...applicationMockUnsuccessfulStatus,
      },
      {
        ...applicationMock,
      },
    ];
    const isApplicationUnsuccessful = isSelectedApplicationStatusUnsuccessful(
      applications,
      applicationMockUnsuccessfulStatus,
    );
    expect(isApplicationUnsuccessful).toBe(false);
  });
});
