import {getElapsedDays} from '../date';

describe('date util', () => {
  describe('getElapsedDays', () => {
    it('should return number of days ago if previous date is more than 1 day ago', () => {
      const currentDate = new Date('2018-09-30T00:00:00');
      const previousDate = '2018-09-28T00:00:00';
      expect(getElapsedDays(previousDate, currentDate)).toEqual('2 days ago');
    });

    it('should return yesterday if previous date is 1 day ago', () => {
      const currentDate = new Date('2018-09-30T00:00:00');
      const previousDate = '2018-09-29T00:00:00';
      expect(getElapsedDays(previousDate, currentDate)).toEqual('yesterday');
    });

    it('should return today if previous date is less than 1 day ago', () => {
      const currentDate = new Date('2018-09-30T01:00:00');
      const previousDate = '2018-09-30T00:00:00';
      expect(getElapsedDays(previousDate, currentDate)).toEqual('today');
    });

    it('should return date unavailable if previous date is missing', () => {
      const currentDate = new Date('2018-09-30T00:00:00');
      const previousDate = '';
      expect(getElapsedDays(previousDate, currentDate)).toEqual('date unavailable');
    });
  });
});
