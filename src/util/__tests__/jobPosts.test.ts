import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {JobStatus} from '~/services/employer/jobs.types';
import {formatJob, IFormattedJobPost, SALARY_UNDISCLOSED, minJobDescriptionWordCount} from '~/util/jobPosts';

describe('util/format', () => {
  it('formats a job', () => {
    const expected: IFormattedJobPost = {
      address: 'Sandcrawler, 9 WOODLANDS AVENUE 9 #8-77 738964',
      categories: 'Education and Training, Public / Civil Service',
      employerName: 'Republic Polytechnic Recruiter',
      employmentTypes: 'Full Time',
      // Native Date has platform inconsistencies, causes CI to break
      // This is not just a date-fn thing, it also breaks with moment
      expiryDate: '10 Oct 2020',
      hiringEmployerName: 'Republic Polytechnic',
      id: 'JOB-2018-0015396',
      jobApplications: '9999',
      jobStatus: JobStatus.Open,
      linkUrl: '/jobs/school-technology-arts-amp-manager-republic-polytechnic-afab75dafd1c63f8dc3a8d909be8ef4f',
      monthlySalary: 'S$ 0 – 10,000',
      numberOfVacancies: '1',
      positionLevels: 'Senior Management',
      originalPostedDate: '25 Jan 2018',
      postedDate: '25 Jan 2018',
      title: 'School of Technology for the Arts & Manager (Technical)',
      uen: 'T08GB0046G',
      uuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
    };

    expect(formatJob(jobPostMock)).toEqual(expected);
  });

  describe('employmentTypes', () => {
    it('display employmentTypes with comma delimiter when there is more than 1 employmentTypes', () => {
      const multiEmploymentTypesJob = {
        ...jobPostMock,
        employmentTypes: [...jobPostMock.employmentTypes, ...jobPostMock.employmentTypes],
      };
      expect(formatJob(multiEmploymentTypesJob).employmentTypes).toEqual('Full Time, Full Time');
    });

    it('display empty string when there is no employmentTypes', () => {
      const noEmploymentTypesJob = {
        ...jobPostMock,
        employmentTypes: [],
      };
      expect(formatJob(noEmploymentTypesJob).employmentTypes).toEqual('');
    });
  });

  describe('categories', () => {
    it('display categories with comma delimiter when there is more than 1 categories', () => {
      const multiCategoriesJob = {
        ...jobPostMock,
        categories: [...jobPostMock.categories, ...jobPostMock.categories],
      };
      expect(formatJob(multiCategoriesJob).categories).toEqual(
        'Education and Training, Public / Civil Service, Education and Training, Public / Civil Service',
      );
    });

    it('display empty string when there is no categories', () => {
      const noCategoriesJob = {
        ...jobPostMock,
        categories: [],
      };
      expect(formatJob(noCategoriesJob).categories).toEqual('');
    });
  });

  describe('positionLevels', () => {
    it('display positionLevels without comma delimiter when there is only 1 positionLevels', () => {
      const onePositionLevelsJob = {
        ...jobPostMock,
        positionLevels: [{id: 1, position: 'foo'}],
      };
      expect(formatJob(onePositionLevelsJob).positionLevels).toEqual('foo');
    });

    it('display positionLevels with comma delimiter when there is more than 1 positionLevels', () => {
      const multiPositionLevelsJob = {
        ...jobPostMock,
        positionLevels: [
          {id: 1, position: 'foo'},
          {id: 2, position: 'bar'},
        ],
      };
      expect(formatJob(multiPositionLevelsJob).positionLevels).toEqual('foo, bar');
    });
  });

  describe('jobStatus', () => {
    it('display Unkown when job status id is not found in JobStatusCodes enum', () => {
      const unKnownJobStatusJob = {
        ...jobPostMock,
        status: {id: 0, jobStatus: 'foobar'},
      };
      expect(formatJob(unKnownJobStatusJob).jobStatus).toEqual(JobStatus.Unknown);
    });
  });

  describe('monthlySalary', () => {
    it('display SALARY_UNDISCLOSED when metadata.isHideSalary is true', () => {
      const salaryUndisclosedJob = {
        ...jobPostMock,
        metadata: {...jobPostMock.metadata, isHideSalary: true},
      };
      expect(formatJob(salaryUndisclosedJob).monthlySalary).toEqual(SALARY_UNDISCLOSED);
    });
  });

  describe('hiringEmployerName', () => {
    it('display postedCompany name when there is no hiringCompany', () => {
      const noHiringCompanyJob = {
        ...jobPostMock,
        hiringCompany: undefined,
      };
      expect(formatJob(noHiringCompanyJob).hiringEmployerName).toEqual(jobPostMock.postedCompany.name);
    });
  });

  describe('minJobDescriptionWordCount', () => {
    it.each`
      ssoc         | firstDigit
      ${1000}      | ${'1'}
      ${2000}      | ${'2'}
      ${3000}      | ${'3'}
      ${'X000'}    | ${'X'}
      ${''}        | ${'an empty string'}
      ${undefined} | ${'undefined'}
    `('Should return 300 if firstDigit is $firstDigit', ({ssoc}) => {
      expect(minJobDescriptionWordCount(ssoc)).toEqual(300);
    });

    it.each`
      ssoc    | firstDigit
      ${4000} | ${'4'}
      ${5000} | ${'5'}
      ${6000} | ${'6'}
      ${7000} | ${'7'}
      ${8000} | ${'8'}
      ${9000} | ${'9'}
    `('Should return 100 if firstDigit is $firstDigit', ({ssoc}) => {
      expect(minJobDescriptionWordCount(ssoc)).toEqual(100);
    });
  });
});
