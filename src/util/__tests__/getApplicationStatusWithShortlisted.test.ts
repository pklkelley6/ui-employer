import {mcf} from '@mcf/constants';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';

describe('getApplicationStatusWithShortlisted', () => {
  it('should return undefined if application status is received, not sent, or withdrawn', () => {
    const isShortlisted = false;
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.RECEIVED, isShortlisted)).toEqual(undefined);
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.NOT_SENT, isShortlisted)).toEqual(undefined);
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.WITHDRAWN, isShortlisted)).toEqual(undefined);
  });
  it('should return shortlisted label if application status under review and shortlisted is true', () => {
    const isShortlisted = true;
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW, isShortlisted)).toEqual({
      label: 'To Interview',
      value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    });
  });
  it('should return undefined if application status is under review and shortlisted is false', () => {
    const isShortlisted = false;
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW, isShortlisted)).toEqual(
      undefined,
    );
  });
  it('should return hired label if application status is hired and shortlisted is false', () => {
    const isShortlisted = false;
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL, isShortlisted)).toEqual({
      label: 'Hired',
      value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    });
  });
  it('should return unsuccessful label if application status is unsuccessful and shortlisted is false', () => {
    const isShortlisted = false;
    expect(getApplicationStatusWithShortlisted(mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL, isShortlisted)).toEqual({
      label: 'Unsuccessful',
      value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
    });
  });
});
