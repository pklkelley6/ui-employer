import {getBlobObject} from '../getBlobObject';

describe('getBlobObject', () => {
  const blobOutput = new Blob(['testing'], {type: 'image/png'});
  beforeEach(function () {
    const mockBlobResponse = Promise.resolve(blobOutput);
    const mockFetchPromise = Promise.resolve({
      blob: () => mockBlobResponse,
    });
    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(mockFetchPromise));
  });
  it('should return blob object if its a blob url', async () => {
    const url = 'blob:http://someurl';
    const response = await getBlobObject(url);
    expect(response).toStrictEqual(blobOutput);
  });
  it('should return undefined if there is an existing S3 logo url instead of a blob url', async () => {
    const url = 'someurl';
    const result = await getBlobObject(url);
    expect(result).toEqual(undefined);
  });
});
