import {
  composeValidators,
  contactNumber,
  defaultRequired,
  email,
  exactLength,
  greaterThan,
  greaterThanEqual,
  greaterThanEqualField,
  lessThan,
  lessThanEqual,
  lessThanEqualField,
  maxLength,
  minLength,
  oneOf,
  positive,
  regex,
  required,
  searchableFieldRequired,
  validateIf,
} from '~/util/fieldValidations';

describe('fieldValidations', () => {
  const formValues = {
    section1: {
      field0: 0,
      field1: 1,
      field2: 2,
    },
  };

  describe('composeValidators', () => {
    const isMoreThan2 = (val: any) => (val > 2 ? undefined : 'not more than 2');
    const isLessThan4 = (val: any) => (val < 4 ? undefined : 'not less than 4');
    const receivedOptionalArgs = (val: any, optional: any) => (val && optional ? undefined : 'no args');

    it('should return the first error message if the first validation fails', () => {
      const result = composeValidators(isMoreThan2, isLessThan4, receivedOptionalArgs)(1);
      expect(result).toEqual('not more than 2');
    });

    it('should return the subsequent error message if the first validation passes and subsequent validation fails', () => {
      const result = composeValidators(isMoreThan2, isLessThan4, receivedOptionalArgs)(5);
      expect(result).toEqual('not less than 4');

      const result2 = composeValidators(isMoreThan2, isLessThan4, receivedOptionalArgs)(3);
      expect(result2).toEqual('no args');
    });

    it('should return undefined if all validations pass', () => {
      const result = composeValidators(isMoreThan2, isLessThan4, receivedOptionalArgs)(3, {opt: 'args'});
      expect(result).toEqual(undefined);
    });
  });

  describe('positive', () => {
    it('should return error message if value is a negative number', () => {
      expect(positive(-1)).toEqual('Please enter a non-negative value');
    });

    it.each`
      value
      ${0}
      ${1}
    `('should return undefined if value is $value', ({value}) => {
      expect(positive(value)).toEqual(undefined);
    });
  });

  describe('greaterThan', () => {
    it('should return error message if value is less than reference value', () => {
      expect(greaterThan(1)(0)).toEqual('This number should be more than 1');
    });

    it('should return error message if value is equal to reference value', () => {
      expect(greaterThan(1)(1)).toEqual('This number should be more than 1');
    });

    it('should return undefined if value is not greater than reference value', () => {
      expect(greaterThan(1)(2)).toEqual(undefined);
    });

    it('should return undefined if value is not defined', () => {
      expect(greaterThan(1)(undefined)).toEqual(undefined);
    });
  });

  describe('greaterThanEqual', () => {
    it('should return error message if value is less than reference value', () => {
      expect(greaterThanEqual(1)(0)).toEqual('This number should be more than or equal to 1');
    });

    it('should return undefined if value is equal to reference value', () => {
      expect(greaterThanEqual(1)(1)).toEqual(undefined);
    });

    it('should return undefined if value is not greater than reference value', () => {
      expect(greaterThanEqual(1)(2)).toEqual(undefined);
    });

    it('should return undefined if value is not defined', () => {
      expect(greaterThanEqual(1)(undefined)).toEqual(undefined);
    });
  });

  describe('lessThan', () => {
    it('should return error message if value is more than reference value', () => {
      expect(lessThan(1)(2)).toEqual('Please enter a value less than 1');
    });

    it('should return error message if value is equal to reference value', () => {
      expect(lessThan(1)(1)).toEqual('Please enter a value less than 1');
    });

    it('should return undefined if value is not less than reference value', () => {
      expect(lessThan(1)(0)).toEqual(undefined);
    });

    it('should return undefined if value is not defined', () => {
      expect(lessThan(1)(undefined)).toEqual(undefined);
    });
  });

  describe('lessThanEqual', () => {
    it('should return error message if value is more than reference value', () => {
      expect(lessThanEqual(1)(2)).toEqual('This number should not be more than 1');
    });

    it('should return error message if value is equal to reference value', () => {
      expect(lessThanEqual(1)(1)).toEqual(undefined);
    });

    it('should return undefined if value is not less than reference value', () => {
      expect(lessThanEqual(1)(0)).toEqual(undefined);
    });

    it('should return undefined if value is not defined', () => {
      expect(lessThanEqual(1)(undefined)).toEqual(undefined);
    });
  });

  describe('defaultRequired', () => {
    it.each`
      value
      ${undefined}
      ${null}
      ${''}
    `('should return error message if value is $value', ({value}) => {
      expect(defaultRequired(value)).toEqual('Please fill this in');
    });

    it.each`
      value
      ${'test'}
      ${0}
      ${110}
      ${false}
    `('should return undefined if value is not $value', ({value}) => {
      expect(defaultRequired(value)).toEqual(undefined);
    });
  });

  describe('required', () => {
    it.each`
      value
      ${'zoinks'}
      ${'we require more minerals'}
      ${'please do the needful'}
    `('should return custom error message if value is $value', ({value}) => {
      expect(required(value)(undefined)).toEqual(value);
    });

    it.each`
      value
      ${'test'}
      ${0}
      ${110}
      ${false}
    `('should return undefined if value is not $value', ({value}) => {
      expect(required('this is not expected')(value)).toEqual(undefined);
    });
  });

  describe('searchableFieldRequired', () => {
    it.each`
      value
      ${undefined}
      ${null}
      ${''}
    `('should return error message if value is $value', ({value}) => {
      expect(searchableFieldRequired(value)).toEqual('Please type and select');
    });

    it.each`
      value
      ${'test'}
      ${0}
      ${110}
      ${false}
    `('should return undefined if value is not $value', ({value}) => {
      expect(searchableFieldRequired(value)).toEqual(undefined);
    });
  });

  describe('greaterThanEqualField', () => {
    it('should return error message if value is not greater than value of the reference field value', () => {
      expect(greaterThanEqualField('section1.field1')(0, formValues)).toEqual('This number should be more than 1');
      expect(greaterThanEqualField('section1.field1', '💣')(0, formValues)).toEqual('💣');
    });

    it('should return undefined if value is greater than value of the reference field value', () => {
      expect(greaterThanEqualField('section1.field2')(3, formValues)).toEqual(undefined);
    });

    it('should return undefined if value is 0 and reference value is 0', () => {
      expect(greaterThanEqualField('section1.field0')(0, formValues)).toEqual(undefined);
    });
  });

  describe('lessThanEqualField', () => {
    it('should return error message if value is not less than value of the reference field value', () => {
      expect(lessThanEqualField('section1.field2')(3, formValues)).toEqual('Please enter a value less than 2');
      expect(lessThanEqualField('section1.field2', '💣')(3, formValues)).toEqual('💣');
    });

    it('should return undefined if value is less than value of the reference field value', () => {
      expect(lessThanEqualField('section1.field2')(1, formValues)).toEqual(undefined);
    });

    it('should return undefined if value is 0 and reference value is 0', () => {
      expect(lessThanEqualField('section1.field0')(0, formValues)).toEqual(undefined);
    });
  });

  describe('oneOf', () => {
    const list = [100, 200];

    it('should return error message when value is not in the value list', () => {
      expect(oneOf(list)(101)).toEqual('Please select an option');
    });

    it('should return undefined when value is in the value list', () => {
      expect(oneOf(list)(200)).toEqual(undefined);
    });
  });

  describe('maxLength', () => {
    const list = ['a', 'b', 'c'];
    const longString = 'this string is 27 char long';

    it('should return error message when the number of items in the array exceed the given amount', () => {
      expect(maxLength(2)(list)).toEqual('You can only add a maximum of 2 items');
      expect(maxLength(2, '2 many')(list)).toEqual('2 many');
    });

    it('should return error message when the number of characters in the string exceed the given amount', () => {
      expect(maxLength(26)(longString)).toEqual('You can only add a maximum of 26 items');
      expect(maxLength(2, '2 many characters')(list)).toEqual('2 many characters');
    });

    it.each`
      length
      ${3}
      ${4}
    `('should return undefined when the number of items in the array does not exceed the given amount', ({length}) => {
      expect(maxLength(length)(list)).toEqual(undefined);
    });

    it.each`
      length
      ${28}
      ${27}
    `(
      'should return undefined when the number of characters in the string does not exceed the given amount',
      ({length}) => {
        expect(maxLength(length)(longString)).toEqual(undefined);
      },
    );

    it('should return undefined when value is not defined', () => {
      expect(maxLength(100)(undefined)).toEqual(undefined);
    });
  });

  describe('minLength', () => {
    const list = ['a', 'b', 'c'];
    const shortString = '6 char';

    it('should return error message when the number of characters in the string is less than the given amount', () => {
      expect(minLength(7)(shortString)).toEqual('You must add a minimum of 7 items');
      expect(minLength(7, 'at least 7')(shortString)).toEqual('at least 7');
    });

    it('should return error message when the number of items in the array is less than the given amount', () => {
      expect(minLength(4)(list)).toEqual('You must add a minimum of 4 items');
      expect(minLength(4, 'at least 4')(list)).toEqual(`at least 4`);
    });

    it.each`
      length
      ${2}
      ${3}
    `('should return undefined when the number of items in the array is not less than the given amount', ({length}) => {
      expect(minLength(length)(list)).toEqual(undefined);
    });

    it.each`
      length
      ${5}
      ${6}
    `('should return undefined when the number of items in the array is not less than the given amount', ({length}) => {
      expect(minLength(length)(shortString)).toEqual(undefined);
    });
  });

  describe('validateIf', () => {
    const values = {
      section1: {
        item1: 'value1',
        item2: 'value2',
      },
    };
    const condition = (x: any) => x === 'value1';
    const validation = () => 'validated';

    it('should not run the validation if the path field value does not pass the condition', () => {
      expect(validateIf('section1.item2', condition, validation)('some value', values)).toEqual(undefined);
    });

    it('should run the validation if the path field value passes the condition', () => {
      expect(validateIf('section1.item1', condition, validation)('some value', values)).toEqual('validated');
    });
  });

  describe('exactLength', () => {
    it('should return undefined if value has the specified length', () => {
      expect(exactLength(6, 'Please enter correct length')('foobar')).toEqual(undefined);
    });

    it('should return the error if value does not have the specified length', () => {
      expect(exactLength(6, 'Please enter correct length')('fubar')).toEqual('Please enter correct length');
    });
  });

  describe('email validation', () => {
    it('Should return an error when email is not valid', () => {
      const invalidEmail = 'fake.com@cc';
      expect(email(invalidEmail)).toEqual('Please check your email address');
    });

    it('Should return undefined when email is valid', () => {
      const validEmail = 'YEO_Jun_Hao@wsg.gov.sg';
      expect(email(validEmail)).toBeUndefined();
    });

    it('Should return undefined when email is valid and caps', () => {
      const validEmail = 'YEO_JUN_HAO@wsg.gov.sg';
      expect(email(validEmail)).toBeUndefined();
    });
  });

  describe('contact number validation', () => {
    it.each`
      firstDigit
      ${0}
      ${1}
      ${2}
      ${3}
      ${4}
      ${5}
      ${7}
    `('Should return error when contact number start with $firstDigit and contains 8 digits', ({firstDigit}) => {
      expect(contactNumber(firstDigit + '1234567')).toEqual(
        'Please check that your phone number starts with 6, 8 or 9',
      );
    });

    it('Should invalidate size of contact number', () => {
      const invalidNumber = '9123912';
      expect(contactNumber(invalidNumber)).toEqual('Please check that your phone number has 8 digits');
    });

    it.each`
      firstDigit
      ${6}
      ${8}
      ${9}
    `('Should return undefined when contact number start with $firstDigit and contains 8 digits', ({firstDigit}) => {
      expect(contactNumber(firstDigit + '1234567')).toEqual(undefined);
    });
  });

  describe('regex', () => {
    it('should return error when pattern is not valid', () => {
      const patternExpectOnlyDigit = /^\d+$/;
      expect(regex('Invalid regex')(patternExpectOnlyDigit)('abcd')).toEqual('Invalid regex');
    });

    it('should return undefined when pattern is valid', () => {
      const patternExpectOnlyDigit = /^\d+$/;
      expect(regex('Invalid regex')(patternExpectOnlyDigit)('1234')).toEqual(undefined);
    });
  });
});
