import {convertNonBreakingSpaceToSpace} from '../convertNonBreakingSpaceToSpace';

describe('convertNonBreakingSpaceToSpace', () => {
  it('should convert all sentence`s non-breaking space into spaces', () => {
    expect(convertNonBreakingSpaceToSpace('non-breaking&nbsp;space')).toEqual('non-breaking space');
    expect(convertNonBreakingSpaceToSpace('non-breaking&nbsp;&nbsp;space')).toEqual('non-breaking  space');
    expect(convertNonBreakingSpaceToSpace('non-breaking&nbsp;space&nbsp;here')).toEqual('non-breaking space here');
  });
});
