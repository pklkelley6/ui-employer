import {format, differenceInCalendarDays, parseISO} from 'date-fns';
import {parseFromTimeZone} from 'date-fns-timezone';

import {Maybe} from '~/graphql/__generated__/types';

export const getElapsedDays = (previousDate?: Maybe<string>, currentDate: Date = new Date(Date.now())) => {
  if (!previousDate) {
    return 'date unavailable';
  }
  const daysApart = differenceInCalendarDays(currentDate, parseISO(previousDate));
  return daysApart > 1 ? `${daysApart} days ago` : daysApart ? 'yesterday' : 'today';
};

const SINGAPORE_TIMEZONE = 'Asia/Singapore';
/**
 * Adds singapore timezone to a date string without timezone information.
 * ex. 2019-07-02 becomes 2019-07-02T00:00:00.000+0800 or 2019-07-01T16:00:00.000Z
 */
export const addSingaporeTimeZone = (date: string) => parseFromTimeZone(date, {timeZone: SINGAPORE_TIMEZONE});

export const formatISODate = (date: string, displayFormat = 'd MMM yyyy') => format(parseISO(date), displayFormat);
