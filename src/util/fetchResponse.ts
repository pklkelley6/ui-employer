export const fetchResponse = async (response: Response) => {
  if (!response.ok) {
    let errorMessage: string;
    try {
      errorMessage = (await response.clone().json()).message;
    } catch (_) {
      errorMessage = await response.text();
    }
    throw new Error(errorMessage);
  }
  return response.json();
};
