import {IEmailInvitation} from './getEmailInvitations';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const postEmailInvitation = async (jobUuid: string, individualId: string): Promise<IEmailInvitation> => {
  const url = `${config.url.apiJob.v2}/jobs/${jobUuid}/invite-talents`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify({
        individualId,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    }),
  );
  return fetchResponse(response);
};
