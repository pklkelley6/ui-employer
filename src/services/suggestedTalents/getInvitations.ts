import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export interface IGetInvitationsResponse {
  data: IEmailInvitation[];
  total: number;
}

export const getInvitations = async (talent: string, uen: string): Promise<IGetInvitationsResponse> => {
  const url = `${config.url.apiJob.v2}/invitations?talent=${talent}&uen=${uen}`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'GET',
    }),
  );
  return fetchResponse(response);
};
