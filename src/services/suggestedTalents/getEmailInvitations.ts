import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export interface IEmailInvitation {
  jobUuid: string;
  individualId: string;
  updatedAt: Date;
  createdAt: Date;
  isEmailSent: boolean;
}

export const getEmailInvitations = async (jobUuid: string): Promise<IEmailInvitation[]> => {
  const url = `${config.url.apiJob.v2}/jobs/${jobUuid}/invite-talents`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'GET',
    }),
  );
  return fetchResponse(response);
};
