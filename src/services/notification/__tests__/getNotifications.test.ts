import {isMaintenanceNotification, isFeatureNotification} from '../getNotifications';
import {featureNotificationMock, maintenanceNotificationMock} from '~/__mocks__/notification/notification.mocks';

describe('getNotification', () => {
  describe('isMaintenanceNotification', () => {
    it('should return false when type is feature', () => {
      expect(isMaintenanceNotification(featureNotificationMock)).toBeFalsy();
    });

    it('should return false when downtime system not within MaintenanceSystem enum', () => {
      expect(
        isMaintenanceNotification({
          ...maintenanceNotificationMock,
          downtime: {
            ...maintenanceNotificationMock.downtime,
            system: 'foobar',
          },
        } as any),
      ).toBeFalsy();
    });

    it('should return true when type is maintenance and downtime system within MaintenanceSystem enum', () => {
      expect(isMaintenanceNotification(maintenanceNotificationMock)).toBeTruthy();
    });
  });

  describe('isFeatureNotifcation', () => {
    it('should return false when type is maintenance', () => {
      expect(isFeatureNotification(maintenanceNotificationMock)).toBeFalsy();
    });

    it('should return false if portal is not employer', () => {
      expect(
        isFeatureNotification({
          ...featureNotificationMock,
          portal: 'jobseeker',
        }),
      ).toBeFalsy();
    });

    it('should return true when type is feature and portal is employer', () => {
      expect(isMaintenanceNotification(maintenanceNotificationMock)).toBeTruthy();
    });
  });
});
