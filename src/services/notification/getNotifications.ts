import {config} from '~/config';

// cast the value to <any> so that enum will create reverse lookup table
export enum MaintenanceSystem {
  EMPLOYER = 'Employer' as any,
  CORPPASS = 'CorpPass' as any,
  SG_CORE = 'SG-CORE' as any,
}

export const EMPLOYER_PORTAL = 'employer';

export interface IMaintenanceNotification {
  type: 'maintenance';
  fromDate: string;
  toDate: string;
  message: string;
  title: string;
  downtime: {
    system: MaintenanceSystem;
    fromDate: string;
    toDate: string;
  };
}

export interface IFeatureNotification {
  type: 'feature';
  portal: string;
  fromDate: string;
  toDate: string;
  message: string;
  bannerColor: string;
  textColor: string;
  title: string;
}

export type INotification = IMaintenanceNotification | IFeatureNotification;

export const isMaintenanceNotification = (
  notification: IMaintenanceNotification | IFeatureNotification,
): notification is IMaintenanceNotification => {
  return notification.type === 'maintenance' && MaintenanceSystem[notification.downtime.system] !== undefined;
};

export const isFeatureNotification = (
  notification: IMaintenanceNotification | IFeatureNotification,
): notification is IFeatureNotification => {
  return notification.type === 'feature' && notification.portal === EMPLOYER_PORTAL;
};

export const getNotifications = (): Promise<INotification[]> => {
  return fetch(config.url.notification).then(async (res) => {
    const result = await res.json();
    return result.notifications;
  });
};
