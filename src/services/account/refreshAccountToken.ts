import {throttle} from 'lodash';
import {config} from '~/config';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const refreshAccountToken = throttle(async (): Promise<Response> => {
  const response = await fetch(
    `${config.url.apiJob.v2}/refresh-token`,
    getApiJobRequestInit({
      credentials: 'include',
    }),
  );
  if (!response.ok) {
    throw new Error();
  }
  return response;
}, 5000);
