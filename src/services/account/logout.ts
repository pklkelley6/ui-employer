import {config} from '~/config';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const logout = async (): Promise<Response> => {
  const response = await fetch(
    `${config.url.apiJob.v2}/logout`,
    getApiJobRequestInit({
      credentials: 'include',
    }),
  );
  if (!response.ok) {
    throw new Error();
  }
  return response;
};
