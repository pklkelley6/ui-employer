interface IOneMapGetAddressResponse {
  found: number;
}

export const getAddressByPostalCode = (postalCode: string): Promise<IOneMapGetAddressResponse> => {
  const url = `https://developers.onemap.sg/commonapi/search?searchVal=${postalCode}&returnGeom=N&getAddrDetails=N`;
  return fetch(url).then((res) => res.json());
};
