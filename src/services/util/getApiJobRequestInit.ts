import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

export const API_JOB_VERSION = '2021-07-09';

export const getApiJobRequestInit = (init?: RequestInit): RequestInit => ({
  ...init,
  headers: {
    [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
    ...init?.headers,
  },
});
