import {mcf} from '@mcf/constants';
import {useMutation, MutationFunctionOptions} from 'react-apollo';
import {
  Mutation,
  MutationSetApplicationStatusArgs,
  MutationSetResumeLastDownloadedDateArgs,
  GetApplicationsApplications,
} from '~/graphql/__generated__/types';
import {
  SET_RESUME_LAST_DOWNLOADED_DATE,
  SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD,
} from '~/graphql/applications';

export const useDownloadResumeMutation = () => {
  const [setResumeLastDownloadedDate] = useMutation<
    {
      setResumeLastDownloadedDate?: Mutation['setResumeLastDownloadedDate'];
    },
    MutationSetResumeLastDownloadedDateArgs
  >(SET_RESUME_LAST_DOWNLOADED_DATE);

  const [setResumeDownloadAndStatusUpdate] = useMutation<
    {
      setResumeLastDownloadedDate?: Mutation['setResumeLastDownloadedDate'];
      setApplicationStatus?: Mutation['setApplicationStatus'];
    },
    MutationSetApplicationStatusArgs
  >(SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE);

  const [setApplicationAsViewedWithResumeDownloadAndStatusUpdate] = useMutation<
    {
      setResumeLastDownloadedDate?: Mutation['setResumeLastDownloadedDate'];
      setApplicationStatus?: Mutation['setApplicationStatus'];
      setApplicationAsViewed?: Mutation['setApplicationAsViewed'];
    },
    MutationSetApplicationStatusArgs
  >(SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE);

  const [setApplicationAsViewedWithResumeDownload] = useMutation<
    {
      setResumeLastDownloadedDate?: Mutation['setResumeLastDownloadedDate'];
      setApplicationAsViewed?: Mutation['setApplicationAsViewed'];
    },
    MutationSetResumeLastDownloadedDateArgs
  >(SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD);

  return (
    {id, statusId, isViewed}: Pick<GetApplicationsApplications, 'id' | 'statusId' | 'isViewed'>,
    options?: MutationFunctionOptions<
      {
        setResumeLastDownloadedDate?: Mutation['setResumeLastDownloadedDate'];
        setApplicationStatus?: Mutation['setApplicationStatus'];
        setApplicationAsViewed?: Mutation['setApplicationAsViewed'];
      },
      MutationSetApplicationStatusArgs
    >,
  ) => {
    const isStatusReceived = [mcf.JOB_APPLICATION_STATUS.RECEIVED, mcf.JOB_APPLICATION_STATUS.NOT_SENT].includes(
      statusId,
    );
    if (isStatusReceived) {
      const variables = {
        applicationId: id,
        statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
      };
      if (isViewed) {
        return setResumeDownloadAndStatusUpdate({variables, ...options});
      } else {
        return setApplicationAsViewedWithResumeDownloadAndStatusUpdate({variables, ...options});
      }
    } else {
      const variables = {
        applicationId: id,
      };
      if (isViewed) {
        return setResumeLastDownloadedDate({variables, ...options});
      } else {
        return setApplicationAsViewedWithResumeDownload({variables, ...options});
      }
    }
  };
};
