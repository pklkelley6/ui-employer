import React from 'react';
import {mcf} from '@mcf/constants';
import {act} from 'react-dom/test-utils';
import {mount} from 'enzyme';
import {ApolloProvider} from 'react-apollo';
import {Matchers, ApolloGraphQLInteraction} from '@pact-foundation/pact';
import {print} from 'graphql';
import {useDownloadResumeMutation} from '../useDownloadResumeMutation';
import {PactBuilder} from '~/__mocks__/pact';
import {
  SET_RESUME_LAST_DOWNLOADED_DATE,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD,
  SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
  SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE,
} from '~/graphql/applications';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {GetApplicationsApplications} from '~/graphql/__generated__/types';
import {API_PROFILE_VERSION} from '~/graphql';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

describe('services/employer/useDownloadResumeMutation', () => {
  const ISO8601_DATETIME_FORMAT = '^\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d';
  const MockComponent = ({application}: {application: GetApplicationsApplications}) => {
    const setResumeLastDownloaded = useDownloadResumeMutation();

    return (
      <button id="resume-test-button" onClick={() => setResumeLastDownloaded(application)}>
        Click Here
      </button>
    );
  };

  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should setResumeLastDownloadedDate when application isViewed=true and application is not Received', async () => {
    const setResumeLastDownloadedDate = new ApolloGraphQLInteraction()
      .uponReceiving('set resume last downloaded date')
      .withQuery(print(SET_RESUME_LAST_DOWNLOADED_DATE))
      .withOperation('setResumeLastDownloadedDate')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables({applicationId: applicationMock.id})
      .willRespondWith({
        body: {
          data: {
            setResumeLastDownloadedDate: {
              id: applicationMock.id,
              applicant: {
                resume: {
                  lastDownloadedAt: Matchers.term({
                    generate: applicationMock.applicant.resume!.lastDownloadedAt!,
                    matcher: ISO8601_DATETIME_FORMAT,
                  }),
                },
              },
            },
          },
        },
        status: 200,
      });
    await pactBuilder.provider.addInteraction(setResumeLastDownloadedDate);

    const mockComponent = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <MockComponent
          application={{...applicationMock, isViewed: true, statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL}}
        />
      </ApolloProvider>,
    );

    const mockButton = mockComponent.find('#resume-test-button');
    mockButton.simulate('click');

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });

  it('should setApplicationAsViewedWithResumeDownload when application isViewed=false and application is not Received', async () => {
    const setApplicationAsViewedWithResumeDownload = new ApolloGraphQLInteraction()
      .uponReceiving('set application as viewed and resume last downloaded date')
      .withQuery(print(SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD))
      .withOperation('setApplicationAsViewedWithResumeDownload')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables({applicationId: applicationMock.id})
      .willRespondWith({
        body: {
          data: {
            setResumeLastDownloadedDate: {
              id: applicationMock.id,
              applicant: {
                resume: {
                  lastDownloadedAt: Matchers.term({
                    generate: applicationMock.applicant.resume!.lastDownloadedAt!,
                    matcher: ISO8601_DATETIME_FORMAT,
                  }),
                },
              },
            },
            setApplicationAsViewed: {
              id: applicationMock.id,
              isViewed: true,
            },
          },
        },
        status: 200,
      });
    await pactBuilder.provider.addInteraction(setApplicationAsViewedWithResumeDownload);

    const mockComponent = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <MockComponent
          application={{...applicationMock, isViewed: false, statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL}}
        />
      </ApolloProvider>,
    );

    const mockButton = mockComponent.find('#resume-test-button');
    mockButton.simulate('click');

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });

  it('should setResumeDownloadAndStatusUpdate when application isViewed=true and application is Received', async () => {
    const setResumeDownloadAndStatusUpdate = new ApolloGraphQLInteraction()
      .uponReceiving('set resume last downloaded date and status update to under review')
      .withQuery(print(SET_RESUME_DOWNLOAD_AND_STATUS_UPDATE))
      .withOperation('setResumeDownloadAndStatusUpdate')
      .withRequest({
        method: 'POST',
        path: '/profile',
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
      })
      .withVariables({applicationId: applicationMock.id, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW})
      .willRespondWith({
        body: {
          data: {
            setResumeLastDownloadedDate: {
              id: applicationMock.id,
              applicant: {
                resume: {
                  lastDownloadedAt: Matchers.term({
                    generate: applicationMock.applicant.resume!.lastDownloadedAt!,
                    matcher: ISO8601_DATETIME_FORMAT,
                  }),
                },
              },
            },
            setApplicationStatus: {
              id: applicationMock.id,
              statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
            },
          },
        },
        status: 200,
      });
    await pactBuilder.provider.addInteraction(setResumeDownloadAndStatusUpdate);

    const mockComponent = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <MockComponent
          application={{...applicationMock, isViewed: true, statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED}}
        />
      </ApolloProvider>,
    );

    const mockButton = mockComponent.find('#resume-test-button');
    mockButton.simulate('click');

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });

  it('should setApplicationAsViewedWithResumeDownloadAndStatusUpdate when application isViewed=false and application is not Not Sent', async () => {
    const setApplicationAsViewedWithResumeDownloadAndStatusUpdate = new ApolloGraphQLInteraction()
      .uponReceiving('set application as viewed and resume last downloaded date and status update to under review')
      .withQuery(print(SET_APPLICATION_AS_VIEWED_WITH_RESUME_DOWNLOAD_AND_STATUS_UPDATE))
      .withOperation('setAsViewedWithResumeDownloadAndStatusUpdate')
      .withRequest({
        method: 'POST',
        path: '/profile',
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
      })
      .withVariables({applicationId: applicationMock.id, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW})
      .willRespondWith({
        body: {
          data: {
            setResumeLastDownloadedDate: {
              id: applicationMock.id,
              applicant: {
                resume: {
                  lastDownloadedAt: Matchers.term({
                    generate: applicationMock.applicant.resume!.lastDownloadedAt!,
                    matcher: ISO8601_DATETIME_FORMAT,
                  }),
                },
              },
            },
            setApplicationAsViewed: {
              id: applicationMock.id,
              isViewed: true,
            },
            setApplicationStatus: {
              id: applicationMock.id,
              statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
            },
          },
        },
        status: 200,
      });
    await pactBuilder.provider.addInteraction(setApplicationAsViewedWithResumeDownloadAndStatusUpdate);

    const mockComponent = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <MockComponent
          application={{...applicationMock, isViewed: false, statusId: mcf.JOB_APPLICATION_STATUS.NOT_SENT}}
        />
      </ApolloProvider>,
    );

    const mockButton = mockComponent.find('#resume-test-button');
    mockButton.simulate('click');

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });
});
