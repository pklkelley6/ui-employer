import * as qs from 'qs';
import {FetchQuery, IFetchQuery} from '~/components/Core/FetchQuery';
import {ICompanyInfoWithAddresses} from '~/services/employer/company';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';
import {includeCredentials} from '~/services/employer/includeCredentials';

export const getCompanyInfo = async (uen?: string): Promise<ICompanyInfoWithAddresses> => {
  if (!uen) {
    throw new Error('uen required');
  }
  const url = `${config.url.apiJob.v2}/companies/${uen}`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      credentials: 'include',
      cache: 'no-cache',
    }),
  );
  return fetchResponse(response);
};

interface IGetCompanyInfoQuery extends Pick<IFetchQuery<ICompanyInfoWithAddresses>, 'children'> {
  uen: string;
}
export function GetCompanyInfoQuery({uen, children}: IGetCompanyInfoQuery) {
  if (uen === '') {
    return children({errorMessage: 'uen required', loading: false});
  }

  return FetchQuery<ICompanyInfoWithAddresses>({
    children,
    init: getApiJobRequestInit(includeCredentials),
    input: `${config.url.apiJob.v2}/companies/${uen}`,
  });
}

interface IGetCompanySuggestionQuery extends Pick<IFetchQuery<{results: ICompanySuggestion[]}>, 'children'> {
  name: string;
}
export function GetCompanySuggestionQuery({name, children}: IGetCompanySuggestionQuery) {
  if (name.length < 3) {
    return children({errorMessage: 'minimum length 3', loading: false});
  }
  return FetchQuery<{results: ICompanySuggestion[]}>({
    children,
    input: `${config.url.apiJob.v2}/companies/suggestions?${qs.stringify({name})}`,
    init: getApiJobRequestInit(),
  });
}

export interface ICompanySuggestion {
  name: string;
  uen: string;
}
