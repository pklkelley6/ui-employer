import {IJobSkill} from '~/components/JobPosting/Skills/skills.types';

export enum JobStatus {
  Closed = 'CLOSED',
  Draft = 'DRAFT',
  Open = 'OPEN',
  Reopened = 'REOPENED',
  Unknown = 'Unknown',
}

export enum JobStatusCodes {
  Closed = 9,
  Draft = 83,
  Open = 102,
  Reopened = 103,
}

export enum SalaryType {
  Monthly = 4,
  Annually = 5,
}

export interface ILinks {
  next?: {
    href: string;
  };
  self?: {
    href: string;
  };
  first?: {
    href: string;
  };
  last?: {
    href: string;
  };
}

export interface IJobPostCategory {
  id: number;
  category: string;
}

export interface IJobPostEmploymentType {
  id: number;
  employmentType: string;
}

export interface IJobPostPositionLevel {
  id: number;
  position: string;
}

export interface IJobPostCompany {
  _links?: ILinks;
  description?: string;
  logoFileName?: string;
  logoUploadPath?: string;
  name: string;
  uen: string;

  ssicCode?: string;
}

export interface IJobPostSalary {
  maximum: number;
  minimum: number;
  type: {
    id: number;
    salaryType: string;
  };
}

export interface IJobPostShiftPattern {
  id: number;
  shiftPattern: string;
}

export interface IJobPostStatus {
  id: number;
  jobStatus: string;
}

export interface IJobPostScheme {
  scheme: {
    id: number;
    scheme: string;
  };
  subScheme?: {
    id: number;
    programme: string;
    schemeId: number;
  };
}

export interface IJobPostAddress {
  overseasCountry?: string;
  foreignAddress1?: string;
  foreignAddress2?: string;
  block?: string;
  street?: string;
  floor?: string;
  unit?: string;
  building?: string;
  postalCode?: string;
  isOverseas: boolean;
}

export interface IJobScreeningQuestion {
  question: string;
}

export interface IJobPostMetadata {
  createdAt: string;
  editCount: number;
  expiryDate: string;
  isHideCompanyAddress: boolean;
  isHideEmployerName: boolean;
  isHideHiringEmployerName: boolean;
  isHideSalary: boolean;
  isPostedOnBehalf: boolean;
  jobPostId: string;
  newPostingDate: string;
  originalPostingDate: string;
  repostCount: number;
  totalNumberJobApplication: number;
  totalNumberOfView: number;
  updatedAt: string;
  deletedAt?: string;
  createdBy: string;
  emailRecipient?: string;
}

export interface IJobPost {
  _links?: ILinks;
  address: IJobPostAddress;
  categories: IJobPostCategory[];
  description: string;
  employmentTypes: IJobPostEmploymentType[];
  hiringCompany?: IJobPostCompany;
  metadata: IJobPostMetadata;
  minimumYearsExperience?: number;
  numberOfVacancies: number;
  otherRequirements?: string;
  positionLevels: IJobPostPositionLevel[];
  postedCompany: IJobPostCompany;
  psdUrl?: string;
  salary: IJobPostSalary;
  schemes: IJobPostScheme[];
  shiftPattern?: IJobPostShiftPattern;
  skills: IJobSkill[];
  sourceCode: string;
  ssocCode: number;
  ssecEqa?: string;
  ssecFos?: string;
  status: IJobPostStatus;
  title: string;
  uuid: string;
  workingHours?: string;
  screeningQuestions?: IJobScreeningQuestion[];
}

export interface IJobPostInput {
  address?: IJobPostAddress;
  categories: Array<Pick<IJobPostCategory, 'id'>>;
  description?: string;
  employmentTypes: Array<Pick<IJobPostEmploymentType, 'id'>>;
  hiringCompany?: Pick<IJobPostCompany, 'uen'>;
  jobPostDuration?: number;
  minimumYearsExperience?: number;
  numberOfVacancies?: number;
  positionLevels: Array<Pick<IJobPostPositionLevel, 'id'>>;
  postedCompany: Pick<IJobPostCompany, 'uen'>;
  salary: Partial<Pick<IJobPostSalary, Exclude<keyof IJobPostSalary, 'type'>>> & {
    type: Pick<IJobPostSalary['type'], 'id'>;
  };
  schemes?: Array<{id: number; subSchemeId?: number}>;
  skills: Array<Pick<IJobSkill, 'id' | 'uuid'>>;
  ssecEqa?: string;
  ssecFos?: string;
  ssocCode?: number;
  title?: string;
  screeningQuestions?: IJobScreeningQuestion[];
}

export enum ACTION_TYPES {
  CREATE = 'CREATE',
  REPOST = 'REPOST',
  CLOSE = 'CLOSE',
  EDIT = 'EDIT',
  EXPIRED = 'EXPIRED',
}
export interface IExtendJob {
  jobPostDuration: number;
}

export interface IJobPostHistoryItem {
  jobPostUpdatedAt: string;
  jobPostExpiryDate: string;
  actionType: ACTION_TYPES;
}
