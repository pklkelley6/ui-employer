export interface ICompanyInfo {
  uen: string;
  description: string;
  name: string;
  ssicCode?: string;
  companyUrl?: string;
  employeeCount?: number;
  logoFileName?: string;
  logoUploadPath?: string;
}

export interface ICompanyInfoWithAddresses extends ICompanyInfo {
  addresses: ICompanyAddress[];
}
export interface ICompanyAddress {
  block: string;
  street: string;
  postalCode: string;
  building?: string;
  floor?: string;
  unit?: string;
  purpose: CompanyAddressType;
}

export enum CompanyAddressType {
  OPERATING = 'operating',
  REGISTERED = 'registered',
  CORRESPONDENCE = 'correspondence',
}
