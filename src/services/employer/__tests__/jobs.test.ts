import crypto from 'crypto';
import {Interaction, Matchers} from '@pact-foundation/pact';
import {jobPostInputMock, jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {getJobs, postJobPosting} from '~/services/employer/jobs';
import {IJobPostInput} from '~/services/employer/jobs.types';
import {API_JOB_VERSION} from '~/services/util/getApiJobRequestInit';
import {API_VERSION_HEADER_KEY, IDEMPOTENCY_KEY_HEADER} from '~/services/util/constants';

const v2ApiMock = jest.fn();

jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('services/employer/jobs', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });
  const companyUen = '198701269D';
  afterAll(async () => pactBuilder.provider.finalize());

  describe('getJobs', () => {
    it('should GET /v2/companies/{uen}/jobs', async () => {
      const {schemes, ...jobWithoutScheme} = jobPostMock;
      const queryMockResult = {
        results: Matchers.eachLike(transformArrayToEachLikeMatcher(jobWithoutScheme)),
        total: Matchers.like(200),
      };

      const interaction = new Interaction()
        .uponReceiving('fetchJobs')
        .withRequest({
          headers: {
            [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
          },
          method: 'GET',
          path: `/v2/companies/${companyUen}/jobs`,
          query: {
            limit: '2',
            page: '4',
          },
        })
        .willRespondWith({
          body: queryMockResult,
          status: 200,
        });
      await pactBuilder.provider.addInteraction(interaction);

      const response = await getJobs(companyUen, '?limit=2&page=4');
      await pactBuilder.verifyInteractions();
      expect(response.results).toHaveLength(1);
      expect(response.total).toEqual(200);
    });
  });

  describe('postJobPosting', () => {
    it('should POST /v2/job', async () => {
      const jobInput: IJobPostInput = {...jobPostInputMock, postedCompany: {uen: companyUen}};
      const queryMockResult = transformArrayToEachLikeMatcher(jobPostMock);
      const idempotencyKey = crypto.randomBytes(64).toString('hex');

      const interaction = new Interaction()
        .uponReceiving('postJobPosting')
        .withRequest({
          body: jobInput,
          headers: {
            'Content-Type': 'application/json',
            [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
            [IDEMPOTENCY_KEY_HEADER]: idempotencyKey,
          },
          method: 'POST',
          path: '/v2/jobs',
        })
        .willRespondWith({
          body: Matchers.like(queryMockResult),
          status: 200,
        });
      await pactBuilder.provider.addInteraction(interaction);

      const response = await postJobPosting(jobInput, {idempotencyKey});
      await pactBuilder.verifyInteractions();
      expect(response).toEqual(
        transformArrayToEachLikeMatcher(jobPostMock, (val: any) => expect.arrayContaining([val])),
      );
    });
  });
});
