import {ICompanyInfo} from '~/services/employer/company';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

interface ICompanyInput {
  description: string;
  companyUrl?: string;
  employeeCount?: number;
  logoBase64?: string;
  virusScanResult?: string;
}

export const updateCompanyProfile = async (uen: string, companyProfile: ICompanyInput): Promise<ICompanyInfo> => {
  const url = `${config.url.apiJob.v2}/companies/${uen}`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify(companyProfile),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PATCH',
    }),
  );
  return fetchResponse(response);
};
