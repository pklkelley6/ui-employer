// fetch call with credentials include will result in `Network request failed` with message `Credentials forbidden`
export const includeCredentials: Pick<RequestInit, 'credentials'> =
  process.env.NODE_ENV === 'test'
    ? {}
    : {
        credentials: 'include',
      };
