import * as qs from 'qs';
import {config} from '~/config';
import {IJobsResponse} from '~/flux/jobPostings';
import {IFetchClosedJobs, IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {IExtendJob, IJobPost, IJobPostInput, JobStatusCodes, IJobPostHistoryItem} from '~/services/employer/jobs.types';
import {fetchResponse} from '~/util/fetchResponse';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {API_VERSION_HEADER_KEY, IDEMPOTENCY_KEY_HEADER} from '~/services/util/constants';
import {API_JOB_VERSION, getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const PAGE_SIZE = 20;

export const getJobPosting = async (uuid: string): Promise<IJobPost> => {
  const res = await fetch(`${config.url.apiJob.v2}/jobs/${uuid}`, getApiJobRequestInit(includeCredentials));
  return fetchResponse(res);
};

export const getJobs = async (uen: string, query = ''): Promise<IJobsResponse> => {
  const url = `${config.url.apiJob.v2}/companies/${uen}/jobs${query}`;
  const res = await fetch(url, getApiJobRequestInit(includeCredentials));
  return fetchResponse(res);
};

export const getOpenJobs = (uen: string, payload: IFetchOpenJobs): Promise<IJobsResponse> => {
  const query = `?jobStatuses=${JobStatusCodes.Open},${JobStatusCodes.Reopened}&${qs.stringify(payload)}`;
  return getJobs(uen, query);
};

export const getClosedJobs = (uen: string, payload: IFetchClosedJobs): Promise<IJobsResponse> => {
  const query = `?jobStatuses=${JobStatusCodes.Closed}&${qs.stringify(payload)}`;
  return getJobs(uen, query);
};

export const postJobPosting = async (
  newPosting: IJobPostInput,
  {apiVersion = API_JOB_VERSION, idempotencyKey}: {apiVersion?: string; idempotencyKey?: string} = {},
): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify(newPosting),
      headers: {
        'Content-Type': 'application/json',
        [API_VERSION_HEADER_KEY]: apiVersion,
        ...(idempotencyKey ? {[IDEMPOTENCY_KEY_HEADER]: idempotencyKey} : {}),
      },
      method: 'POST',
    }),
  );
  return fetchResponse(response);
};

export const closeJobPosting = async (uuid: string, isVacant: boolean): Promise<void> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}/close`;
  const res = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify({isVacant}),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PUT',
    }),
  );

  if (!res.ok) {
    throw new Error(await res.text());
  }
};

export const extendJobPost = async (uuid: string, jobPostDuration: IExtendJob): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}`;
  const res = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify(jobPostDuration),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PATCH',
    }),
  );
  return fetchResponse(res);
};

export const putJobPost = async (
  uuid: string,
  jobPosting: IJobPostInput,
  {apiVersion = API_JOB_VERSION}: {apiVersion?: string} = {},
): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify(jobPosting),
      headers: {
        'Content-Type': 'application/json',
        [API_VERSION_HEADER_KEY]: apiVersion,
      },
      method: 'PUT',
    }),
  );
  return fetchResponse(response);
};

export const repostJobPost = async (uuid: string, jobPostDuration: number): Promise<IJobPost> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}/repost`;
  const res = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body: JSON.stringify({jobPostDuration}),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PATCH',
    }),
  );
  return fetchResponse(res);
};

export const getJobPostHistory = async (uuid: string): Promise<Array<IJobPostHistoryItem>> => {
  const res = await fetch(`${config.url.apiJob.v2}/jobs/${uuid}/history`, getApiJobRequestInit(includeCredentials));
  return fetchResponse(res);
};

export const removeScreeningQuestions = async (uuid: string): Promise<void> => {
  const url = `${config.url.apiJob.v2}/jobs/${uuid}/screening-questions`;
  const res = await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'DELETE',
    }),
  );

  if (!res.ok) {
    throw new Error(await res.text());
  }
};
