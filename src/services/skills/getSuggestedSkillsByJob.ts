import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';
import {IJobSkill} from '~/components/JobPosting/Skills/skills.types';

export const getSuggestedSkillsByJob = async ({
  jobTitle,
  jobDescription = '',
  limit,
}: {
  jobTitle: string;
  jobDescription?: string;
  limit?: number;
}): Promise<IJobSkill[]> => {
  const url = `${config.url.apiJob.v2}/skills/suggestions`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      body: JSON.stringify({jobTitle, jobDescription, limit}),
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    }),
  );
  return fetchResponse(response)
    .then((result) => result.skills)
    .catch(() => []);
};
