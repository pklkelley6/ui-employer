import {uniqBy} from 'lodash/fp';
import {IJobSkill} from '~/components/JobPosting/Skills/skills.types';
import {getSkillsByJobTitle} from '~/services/skills/getSkillsByJobTitle';
import {getWccSkillsByJob} from '~/services/skills/getWccSkillsByJob';

const RECOMMENDED_SKILLS_LIMIT = 8;

export const fetchRecommendedSkills = async (jobTitle: string, description: string): Promise<IJobSkill[]> => {
  const skills = await Promise.all([
    getSkillsByJobTitle(jobTitle)
      .then((result) => result.skills.slice(0, RECOMMENDED_SKILLS_LIMIT))
      .catch(() => []),
    getWccSkillsByJob(description, RECOMMENDED_SKILLS_LIMIT)
      .then((result) => result.skills)
      .catch(() => []),
  ]);
  return uniqBy<IJobSkill>('uuid')([...skills[0], ...skills[1]]);
};
