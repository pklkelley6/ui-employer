import {escape} from 'lodash';
import {ISkillsByJobResponse} from '~/components/JobPosting/Skills/skills.types';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const getSkillsByJobTitle = async (jobTitle = ''): Promise<ISkillsByJobResponse> => {
  const url = `${config.url.apiJob.v1}/jobTitleToSkills?jobTitle=${escape(jobTitle)}`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      credentials: 'include',
    }),
  );
  return fetchResponse(response);
};
