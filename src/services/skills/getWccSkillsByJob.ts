import {IWCCSkillsByJobResponse} from '~/components/JobPosting/Skills/skills.types';
import {config} from '~/config';
import {fetchResponse} from '~/util/fetchResponse';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const getWccSkillsByJob = async (jobDescription = '', limit?: number): Promise<IWCCSkillsByJobResponse> => {
  const url = `${config.url.apiJob.v2}/wcc/skills`;
  const response = await fetch(
    url,
    getApiJobRequestInit({
      body: JSON.stringify({jobDescription, limit}),
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    }),
  );
  return fetchResponse(response);
};
