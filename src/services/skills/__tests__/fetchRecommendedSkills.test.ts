import {fetchRecommendedSkills} from '../fetchRecommendedSkills';
import * as jobKredServices from '~/services/skills/getSkillsByJobTitle';
import * as wccServices from '~/services/skills/getWccSkillsByJob';

describe('fetchRecommendedSkills', () => {
  const getSkillsByJobTitleMock = jest.spyOn(jobKredServices, 'getSkillsByJobTitle');
  const getWccSkillsByJob = jest.spyOn(wccServices, 'getWccSkillsByJob');

  afterAll(() => {
    getSkillsByJobTitleMock.mockRestore();
    getWccSkillsByJob.mockRestore();
  });

  const jobKredSkillsMock = {
    jobTitle: 'JobTitle',
    skills: [
      {uuid: '1', skill: 'jobKredOne'},
      {uuid: '2', skill: 'jobKredTwo'},
      {uuid: '3', skill: 'common'},
    ],
  };
  const wccSkillsMock = {
    skills: [
      {uuid: '4', skill: 'wccOne'},
      {uuid: '5', skill: 'wccTwo'},
      {uuid: '3', skill: 'common'},
    ],
  };

  it('should fetch wcc and jobKred skills then dedupe', async () => {
    const expectedSkills = [
      {uuid: '1', skill: 'jobKredOne'},
      {uuid: '2', skill: 'jobKredTwo'},
      {uuid: '3', skill: 'common'},
      {uuid: '4', skill: 'wccOne'},
      {uuid: '5', skill: 'wccTwo'},
    ];

    getSkillsByJobTitleMock.mockImplementation(() => Promise.resolve(jobKredSkillsMock));
    getWccSkillsByJob.mockImplementation(() => Promise.resolve(wccSkillsMock));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual(expectedSkills);
  });

  it('should return the other provider skills if one provder returns an error', async () => {
    const expectedSkills = [
      {uuid: '4', skill: 'wccOne'},
      {uuid: '5', skill: 'wccTwo'},
      {uuid: '3', skill: 'common'},
    ];

    getSkillsByJobTitleMock.mockImplementation(() => Promise.reject(new Error()));
    getWccSkillsByJob.mockImplementation(() => Promise.resolve(wccSkillsMock));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual(expectedSkills);
  });

  it('should return an empty array if all providers returned an error', async () => {
    getSkillsByJobTitleMock.mockImplementation(() => Promise.reject(new Error()));
    getWccSkillsByJob.mockImplementation(() => Promise.reject(new Error()));
    const result = await fetchRecommendedSkills('jobTitle', 'jobDescription');
    expect(result).toEqual([]);
  });
});
