import {FetchQuery, IFetchQuery} from '~/components/Core/FetchQuery';
import {config} from '~/config';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export interface IScheme {
  id: number;
  scheme: string;
}

export interface ISubScheme {
  id: number;
  schemeId: number;
  programme: string;
}

export interface ICompanyScheme {
  startDate: string;
  expiryDate: string;
  scheme: IScheme;
  subScheme?: ISubScheme;
}

export const getCompanySchemes = async (uen: string): Promise<ICompanyScheme[]> => {
  const url = `${config.url.apiJob.v2}/companies/${uen}/schemes`;
  return fetch(
    url,
    getApiJobRequestInit({
      credentials: 'include',
    }),
  ).then((res) => res.json());
};

interface IGetSubSchemeByIdQuery extends Pick<IFetchQuery<ISubScheme>, 'children'> {
  schemeId: number;
  subSchemeId: number;
}

export function GetSubSchemeById({schemeId, subSchemeId, children}: IGetSubSchemeByIdQuery) {
  return FetchQuery<ISubScheme>({
    children,
    input: `${config.url.apiJob.v2}/schemes/${schemeId}/subSchemes/${subSchemeId}`,
    init: getApiJobRequestInit(),
  });
}
