import {GetApplicationsScores} from '~/graphql/__generated__/types';
import {isTopMatch} from '~/services/scores/isTopMatch';

describe('TopMatcherBadge', () => {
  describe('showTopMatch()', () => {
    it('should return true when sortCriteria is job match by WCC and when wcc score >= 0.6', () => {
      const scores: GetApplicationsScores = {
        wcc: 0.876,
      };
      expect(isTopMatch(scores)).toBe(true);
    });

    it('should return false when wcc scores < 0.6', () => {
      const scores: GetApplicationsScores = {
        wcc: 0.01,
      };
      expect(isTopMatch(scores)).toBe(false);
    });
  });
});
