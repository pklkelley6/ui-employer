import {isNil} from 'lodash/fp';
import {GetApplicationsScores} from '~/graphql/__generated__/types';

enum TopMatcherScore {
  WCC = 0.6,
}

export const isTopMatch = (scores?: GetApplicationsScores) =>
  scores && !isNil(scores.wcc) && scores.wcc >= TopMatcherScore.WCC;
