import {mapValues} from 'lodash';
import {config} from '~/config';

export const loadFeatureToggles = () =>
  fetch(config.url.featureToggles).then(
    async (res) => {
      try {
        const text = await res.text();
        const allFeatureToggles = JSON.parse(text);
        const nodeEnv = config.environment || 'production';
        return mapValues(allFeatureToggles, nodeEnv);
      } catch (err) {
        console.error('failed to parse feature config', err);
        return {};
      }
    },
    (err) => {
      console.error('failed to get feature config', err);
      return {};
    },
  );
