import {config} from '~/config';
import {includeCredentials} from '~/services/employer/includeCredentials';
import {getApiJobRequestInit} from '~/services/util/getApiJobRequestInit';

export const postInvitationsBatch = async (jobs: string[], talent: string): Promise<Response> => {
  const url = `${config.url.apiJob.v2}/invitations/batch`;
  const body = JSON.stringify({
    talent,
    jobs,
  });

  return await fetch(
    url,
    getApiJobRequestInit({
      ...includeCredentials,
      body,
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    }),
  );
};
