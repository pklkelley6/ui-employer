import {mcf} from '@mcf/constants';
import {
  GetBookmarkedCandidatesApplicationInlineFragment,
  GetBookmarkedCandidatesTalentInlineFragment,
} from '~/graphql/__generated__/types';

export const bookmarkedApplicationMock: GetBookmarkedCandidatesApplicationInlineFragment = {
  __typename: 'Application',
  id: 'BMA-1',
  bookmarkedOn: '2018-08-20T00:00:00.000Z',
  isShortlisted: false, // NOTE: can be "isShortlisted":null
  statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
  createdOn: '2018-08-20T00:00:00.000',
  isViewed: true,
  rejectionReason: null,
  rejectionReasonExtended: null,
  jobScreeningQuestionResponses: null,
  applicant: {
    __typename: 'Candidate',
    email: 'email@example.com',
    name: 'Bookmarked Applicant',
    id: 'A-1',
    mobileNumber: '+65 9991 2345',
    education: [
      {
        __typename: 'Education',
        isHighest: false,
        institution: 'School Name',
        yearAttained: 2014,
        ssecEqaCode: '42',
        ssecEqaDescription: "Bachelor's Degree or equivalent",
        ssecFosDescription: 'Accommodation Services',
        name: 'Optional name or description',
        isVerified: null,
      },
    ],
    skills: [{__typename: 'Skill', uuid: 'uuid-1', skill: 'some skill'}],
    workExperiences: [
      {
        __typename: 'WorkExperience',
        companyName: 'Some Company',
        jobDescription: 'description',
        endDate: '2018-01-01T00:00:00.000',
        jobTitle: 'Some Job',
        startDate: '2017-01-01T00:00:00.000',
      },
    ],
    resume: {
      __typename: 'Resume',
      fileName: 'resume.pdf',
      filePath: 'path',
      lastDownloadedAt: '2018-09-24T16:04:50',
    },
  },
};

export const bookmarkedTalentMock: GetBookmarkedCandidatesTalentInlineFragment = {
  __typename: 'Talent',
  id: 'BMT-1',
  bookmarkedOn: '2018-08-20T00:00:00.000Z',
  talent: {
    __typename: 'Candidate',
    email: '2018-09-18T16:04:50',
    name: 'Bookmarked Talent',
    id: 'T-1',
    mobileNumber: '+65 9991 2345',
    lastLogin: '2018-09-24T16:04:50',
    education: [
      {
        __typename: 'Education',
        isHighest: false,
        institution: 'School Name',
        yearAttained: 2014,
        ssecEqaCode: '42',
        ssecEqaDescription: "Bachelor's Degree or equivalent",
        ssecFosDescription: 'Accommodation Services',
        name: 'Optional name or description',
        isVerified: null,
      },
    ],
    skills: [{__typename: 'Skill', uuid: 'uuid-1', skill: 'some skill'}],
    workExperiences: [
      {
        __typename: 'WorkExperience',
        companyName: 'Some Company',
        jobDescription: 'description',
        endDate: '2018-01-01T00:00:00.000',
        jobTitle: 'Some Job',
        startDate: '2017-01-01T00:00:00.000',
      },
    ],
    resume: {
      __typename: 'Resume',
      fileName: 'resume.pdf',
      filePath: 'path',
      lastDownloadedAt: '2018-09-24T16:04:50',
    },
  },
};
