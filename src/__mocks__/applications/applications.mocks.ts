/* tslint:disable:object-literal-sort-keys */
import {mcf} from '@mcf/constants';
import {GetApplicationsApplications} from '~/graphql/__generated__/types';

export const applicationMock: GetApplicationsApplications = {
  isViewed: false,
  isShortlisted: null,
  bookmarkedOn: null,
  applicant: {
    email: 'email@example.com',
    name: 'Tan Ah Kow',
    id: 'A-1',
    mobileNumber: '+65 9991 2345',
    education: [
      {
        isHighest: false,
        institution: 'School Name',
        yearAttained: 2014,
        ssecEqaCode: '42',
        ssecEqaDescription: "Bachelor's Degree or equivalent",
        ssecFosDescription: 'Accommodation Services',
        name: 'Optional name or description',
        isVerified: null,
      },
    ],
    skills: [{uuid: 'uuid-1', skill: 'some skill'}],
    workExperiences: [
      {
        companyName: 'Some Company',
        jobDescription: 'description',
        endDate: '2018-01-01T00:00:00.000',
        jobTitle: 'Some Job',
        startDate: '2017-01-01T00:00:00.000',
      },
    ],
    resume: {
      fileName: 'resume.pdf',
      filePath: 'path',
      lastDownloadedAt: '2018-09-24T16:04:50',
    },
  },
  createdOn: '2018-08-20T00:00:00.000',
  id: 'I-1',
  job: {
    uuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
  },
  rejectionReason: null,
  rejectionReasonExtended: null,
  statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
  scores: {
    wcc: 0.502,
  },
  jobScreeningQuestionResponses: null,
};

export interface IApplicationsByJobPostMock {
  [key: string]: GetApplicationsApplications[];
}

export const applicationsByJobPostsMock: IApplicationsByJobPostMock = {
  afab75dafd1c63f8dc3a8d909be8ef4f: [applicationMock],
};
