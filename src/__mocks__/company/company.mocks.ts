import {ICompanyAddress, CompanyAddressType, ICompanyInfoWithAddresses} from '~/services/employer/company';

export const companyInfoMock: ICompanyInfoWithAddresses = {
  addresses: [],
  description:
    'About Republic Polytechnic The first educational institution in Singapore to leverage the Problem-based Learning approach for all its diploma programmes, Republic Polytechnic (RP) has seven schools and one academic centre offering forty-two diplomas in Applied Science, Engineering, Management and Communication, Events and Hospitality, Infocomm, Sports, Health & Leisure, and Technology for the Arts.',
  logoFileName: '',
  logoUploadPath: 'https://static.ci.mcf.sh/images/company/logos/5d826b09c62e88ed3f7b92ccec5c4dfc.jpg',
  name: 'WE ARE THE CHAMPIONS SCHOOL OF JOBS (SINGAPORE EDITION)',
  uen: 'T08GB0046G',
  ssicCode: '85494',
};

export const companyAddressMock: ICompanyAddress = {
  block: '100',
  building: 'RAFFLES CITY TOWER',
  street: 'NORTH BRIDGE ROAD',
  floor: '01',
  unit: '02',
  postalCode: '520858',
  purpose: CompanyAddressType.OPERATING,
};
