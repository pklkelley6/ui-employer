import path from 'path';
import {Matchers, Pact, MockService} from '@pact-foundation/pact';
import {ApolloClient, HttpLink, InMemoryCache, IntrospectionFragmentMatcher, from} from 'apollo-boost';
import {isPlainObject, reduce} from 'lodash';
import {CommonWrapper} from 'enzyme';
import introspectionResult from '~/graphql/__generated__/types';
import {apiVersionLink} from '~/graphql';
import {nextTick} from '~/testUtil/enzyme';

const SERVICE_PORT = {
  'api-profile': 4444,
  'api-job': 4445,
};
export class PactBuilder {
  public port = 0;
  public host = '';
  public provider: Pact;

  constructor(providerName: 'api-profile' | 'api-job') {
    const logFilename = module.parent ? path.basename(module.parent.filename, '.tsx') : 'pact';
    this.provider = new Pact({
      consumer: 'ui-employer',
      cors: true,
      dir: './pacts',
      host: 'localhost',
      log: `./pacts/logs/${providerName}-${logFilename}.log`,
      logLevel: 'error',
      pactfileWriteMode: 'update',
      provider: providerName,
      spec: 2,
    });
  }

  public async setup() {
    if (process.env.PACT_CI) {
      this.host = `pact-mock-service-${this.provider.opts.provider}`;
      this.port = SERVICE_PORT[this.provider.opts.provider as keyof typeof SERVICE_PORT];
      const mockService = new MockService(undefined, undefined, this.port, this.host, false, 'overwrite');
      this.provider.mockService = mockService;
      this.provider.finalize = async () => {
        await mockService.writePact();
        await mockService.removeInteractions();
      };
    } else {
      const opts = await this.provider.setup();
      this.host = opts.host;
      this.port = opts.port;
    }
  }

  public getApolloClient() {
    const fragmentMatcher = new IntrospectionFragmentMatcher({introspectionQueryResultData: introspectionResult});
    const cache = new InMemoryCache({
      addTypename: false,
      fragmentMatcher,
    });

    return new ApolloClient({
      cache,
      link: from([apiVersionLink, new HttpLink({uri: `http://${this.host}:${this.port}/profile`})]),
    });
  }

  /**
   * This method is to retry the pact provider verify at least x times before failing verification check
   * It is introduced because on graphql query component mount, the request is not fired and registered by the provider immediately.
   * So a retry on the verification to cater for the delay, to prevent test flakiness
   * After pact provider verify, if wrapper component is provided update it
   */
  public async verifyInteractions({
    retry = 5,
    wrapperToUpdate,
  }: {retry?: number; wrapperToUpdate?: CommonWrapper} = {}): Promise<void> {
    try {
      await this.provider.mockService.verify();
      await this.provider.mockService.removeInteractions();
      if (wrapperToUpdate) {
        await nextTick(wrapperToUpdate, 100);
      }
    } catch (err) {
      if (retry > 0) {
        return this.verifyInteractions({retry: retry - 1, wrapperToUpdate});
      }
      throw err;
    }
  }
}

export const transformArrayToEachLikeMatcher = <T extends Record<string, any>>(
  mockData: T,
  eachLikeMatcher: any = Matchers.eachLike,
): Record<string, any> => {
  return isPlainObject(mockData)
    ? reduce(
        mockData,
        (prev, curr, key) => {
          if (isPlainObject(curr)) {
            return {...prev, [key]: transformArrayToEachLikeMatcher(curr, eachLikeMatcher)};
          } else if (Array.isArray(curr) && curr.length > 0) {
            return {...prev, [key]: eachLikeMatcher(transformArrayToEachLikeMatcher(curr[0], eachLikeMatcher))};
          }
          return {...prev, [key]: curr};
        },
        {},
      )
    : mockData;
};
