import {GetSearchTalentsSearchTalents} from '~/graphql/__generated__/types';

export const searchTalentsMock: GetSearchTalentsSearchTalents = {
  talents: [
    {
      email: 'FunctionalTest@email.com',
      name: '0 Name - Used for suggested talent list',
      id: 'TEST-INTEGRATION-SUGGESTED-TALENT-00',
      lastLogin: '2021-02-22T03:23:13.000Z',
      mobileNumber: null,
      education: [
        {
          name: 'Degree',
          isHighest: true,
          isVerified: null,
          institution: 'University of Western Australia',
          yearAttained: null,
          ssecEqaCode: '70',
          ssecEqaDescription: "Bachelor's Degree or equivalent",
          ssecFosDescription: 'Business Management',
        },
      ],
      skills: [
        {
          uuid: 'uuid-66',
          skill: 'Accounting',
        },
        {
          uuid: 'uuid-565',
          skill: 'Auditing',
        },
        {
          uuid: 'uuid-924',
          skill: 'Budget',
        },
        {
          uuid: 'uuid-3209',
          skill: 'External Audit',
        },
        {
          uuid: 'uuid-3361',
          skill: 'Financial Accounting',
        },
        {
          uuid: 'uuid-3364',
          skill: 'Financial Audits',
        },
      ],
      workExperiences: [
        {
          jobTitle: 'Internal Audit Manager',
          companyName: 'Mediacorp Pte Ltd',
          startDate: '2012-09-01',
          endDate: '2018-08-01',
          jobDescription: 'Internal Audit Manager',
          ssocCode: '1',
        },
        {
          jobTitle: 'Internal Audit',
          companyName: 'General Hotels Management Pte Ltd',
          startDate: '2009-11-01',
          endDate: '2012-08-01',
          jobDescription: 'Internal Audit',
          ssocCode: '1',
        },
        {
          jobTitle: 'Senior customer service coordinator',
          companyName: 'Barcode Marketing Pte. Ltd. ',
          startDate: '2007-07-01',
          endDate: '2012-04-01',
          jobDescription:
            'Communicate to the purchasing department unexpected increases or decreases in demand for product  Ensured all space in store was effectively utilised by organising a fast turnaround merchandising concept implementing weekly changes in the store  Provide first class service to customer meet targets and customers need  Deal with customer complaints returns and enquiries at the customer service desk  Handling daily paper work and monthly sales settlement  Assist to train new staffs.',
          ssocCode: '1',
        },
      ],
      resume: null,
    },
  ],
  total: 1,
};
