import {IConfiguration} from '~/config/index.d';

export const config: IConfiguration = {
  environment: 'development',
  meta: {
    version: 'v1.abc',
  },
  publicPath: '',
  url: {
    apiJob: {
      v1: '',
      v2: '',
    },
    apiProfile: '',
    corppass: '',
    featureToggles: '',
    mcf: '',
    notification: '',
    apiVirusScanner: '',
  },
};
