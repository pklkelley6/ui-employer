import {jobPostMock} from './jobs.mocks';
import {IJobPost} from '~/services/employer/jobs.types';

export const createJobPost = (options: Partial<IJobPost>): IJobPost => ({
  ...jobPostMock,
  ...options,
});
