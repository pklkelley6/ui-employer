import {IConfiguration, IUrlConfiguration, IAppConfiguration, Environment} from './index.d';

declare let __WEBPACK_DEFINE_CONFIG_JS_OBJ__: IAppConfiguration;

declare const urlConfig: IUrlConfiguration;
declare const metaConfig: {environment: Environment};

export const config: IConfiguration = {
  ...__WEBPACK_DEFINE_CONFIG_JS_OBJ__,
  url: urlConfig,
  environment: metaConfig.environment,
};
