const childProcess = require('child_process');
const path = require('path');
const fs = require('fs');
const verfile = path.join(process.cwd(), '/.version');

const latestCommitShortHash = childProcess.spawnSync('git', ['rev-parse', '--short', 'HEAD']).stdout.toString().trim();

const getVersion = () => {
  if (!fs.existsSync(verfile)) {
    const gitTags = childProcess.spawnSync('git', ['tag', '-l'], {
      stdio: 'pipe',
    }).stdout;

    const sortTags = childProcess.spawnSync('sort', ['-t.', '-k', '1,1n', '-k', '2,2n', '-k', '3,3n', '-k', '4,4n'], {
      input: gitTags,
    }).stdout;

    const sortTagsToDescending = sortTags.toString().trim().split('\n').reverse();

    const versionFormat = new RegExp('^[0-9]+.[0-9]+.[0-9]+$');

    const filteredTags = sortTagsToDescending.filter((gitTag) => gitTag.match(versionFormat));

    return filteredTags[0].concat(`-${latestCommitShortHash}`);
  }
  return fs.readFileSync(verfile).toString().trim();
};

const metaConfiguration = {
  version: `v1.${getVersion()}`,
};

module.exports = {
  metaConfiguration,
};
