// @ts-check

// This file is kept as JS so webpack and karma configuration files can access this

const {configuration} = require('./configuration');

// Global application configuration, containing both environment specific variables and generic shared variables
// Specific configuration is loaded though the specificConfiguration file and ALL environment specific MUST be loaded that way
// The goal is to make sure the configuration keeps simple
// The shape of the configuration is validated by typescript
// Typecheck config files with tsc:check or tsc:check-config

/**
 * @typedef {import('./index.d').IAppConfiguration} Config
 * @type Config
 */
const config = configuration();

module.exports = {
  config,
};
