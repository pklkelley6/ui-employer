// @ts-check

// Only add specific environment variables (like API_URL)
// For our own safety, please don't add logic here, just use plain objects (also no functions!)

const {metaConfiguration} = require('./meta');

const pathConfiguration = {
  publicPath: '/',
};

/**
 * @callback ConfigurationFunc
 * @return {import('./index.d').IAppConfiguration}
 */
/** @type {ConfigurationFunc} */
const configuration = () => {
  return {
    ...pathConfiguration,
    meta: {
      ...metaConfiguration,
    },
  };
};

module.exports = {
  configuration,
};
