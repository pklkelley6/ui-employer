import React from 'react';
import styles from './unauthorised.scss';
import {LoginLink} from '~/components/Login';

interface IUnauthorisedProps {
  login: () => any;
}

export const Unauthorised: React.FunctionComponent<IUnauthorisedProps> = ({login}) => {
  return (
    <div id="page-unauthorised" className="flex-auto flex flex-column justify-center content-center center tc pa3 pt5">
      <i className={`${styles.robotUnauthorised}`} />
      <h1 className="fw4 primary ma0 lh-title primary f3 mt4 pb2" id="title">
        You’ll need to log in to access this page
      </h1>
      <LoginLink onClick={login} />
    </div>
  );
};
