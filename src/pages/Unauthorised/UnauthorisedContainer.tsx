import {connect} from 'react-redux';
import {Unauthorised} from './Unauthorised';
import {login} from '~/flux/account';

export const UnauthorisedContainer = connect(null, {login})(Unauthorised);
