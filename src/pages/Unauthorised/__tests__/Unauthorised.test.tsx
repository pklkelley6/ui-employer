import {shallow} from 'enzyme';
import React from 'react';
import {Unauthorised} from '../Unauthorised';

describe('Unauthorised', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<Unauthorised login={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
  });
});
