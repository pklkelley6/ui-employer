import React, {useEffect} from 'react';
import {Form} from 'react-final-form';
import {Link} from 'react-router-dom';
import styles from './AccountInfo.scss';
import {HeaderContainer} from '~/components/Navigation/Header';
import {AccountInfoFields} from '~/components/AccountInfo/AccountInfoFields';
import {SystemContact} from '~/graphql/__generated__/types';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {Modal} from '~/components/Core/Modal';
import {ISystemContactState} from '~/flux/index.reducers';
import {
  UPDATE_SYSTEM_CONTACT_FAILED,
  UPDATE_SYSTEM_CONTACT_SUCCEEDED,
} from '~/flux/systemContact/systemContact.constants';

export interface IAccountInfo {
  systemContactStatus: ISystemContactState['status'];
  systemContact: {
    email: SystemContact['email'];
    contactNumber: SystemContact['contactNumber'];
    name: string;
    designation: SystemContact['designation'];
  };
  submitForm: (values: any) => void;
}

export const AccountInfo: React.FunctionComponent<IAccountInfo> = (props) => {
  const title = 'Account Info';

  useEffect(() => {
    document.title = 'Account Info | MyCareersFuture Employer';
  }, []);

  useEffect(() => {
    if (props.systemContactStatus === UPDATE_SYSTEM_CONTACT_FAILED) {
      growlNotification('Temporarily unable to save account info. Please try again later.', GrowlType.ERROR, {
        toastId: 'saveAccountInfoError',
      });
    }
  }, [props.systemContactStatus]);

  const displaySuccessConfirmation = () => {
    return (
      <Modal>
        <div data-cy="update-account-info-modal" className={`pa4 f4 black-80 bg-white ${styles.modal}`}>
          <p>Account Info Saved</p>
          <p>You have successfully saved your account info.</p>
          <div className="flex justify-end pt3">
            <Link
              to="/jobs"
              data-cy="ok-button-redirect-account-info"
              title="All Jobs"
              className={`pa3 db ml3 tc b--primary white bg-primary no-underline ${styles.redirectSuccessButton}`}
            >
              Ok
            </Link>
          </div>
        </div>
      </Modal>
    );
  };

  return (
    <section className="flex-auto-ie">
      <HeaderContainer data-cy="account-info-header" crumbs={[title]} />
      <main className={`flex pa3 ${styles.formContainer}`}>
        <Form
          onSubmit={props.submitForm}
          initialValues={props.systemContact}
          render={({handleSubmit, submitSucceeded}) => (
            <section className="flex-auto db">
              <form onSubmit={handleSubmit}>
                <div className={`bg-black-05 mb3 pa4 pt4 ${styles.formBase}`}>
                  <AccountInfoFields />
                </div>
                <div className="flex justify-end">
                  <Link
                    data-cy="account-info-cancel"
                    to="/jobs"
                    className={`${styles.button} pa3 db ba b--primary primary bg-white no-underline pointer dib v-mid tc`}
                  >
                    Cancel
                  </Link>
                  <button
                    data-cy="account-info-save"
                    className={`${styles.button} pa3 db ml3 b--primary white bg-primary`}
                    type="submit"
                  >
                    Save
                  </button>
                </div>
                {submitSucceeded && props.systemContactStatus === UPDATE_SYSTEM_CONTACT_SUCCEEDED
                  ? displaySuccessConfirmation()
                  : null}
              </form>
            </section>
          )}
        />
      </main>
    </section>
  );
};
