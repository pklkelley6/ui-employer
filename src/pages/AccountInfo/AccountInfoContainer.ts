import {connect} from 'react-redux';
import {IAppState} from '~/flux';
import {getAccountName} from '~/flux/account';
import {updateSystemContactRequested} from '~/flux/systemContact/systemContact.actions';
import {getSystemContact} from '~/flux/systemContact/systemContact.selectors';
import {AccountInfo} from '~/pages/AccountInfo/AccountInfo';

export const mapStateToProps = (state: IAppState) => {
  const systemContact = getSystemContact(state);
  const userName = getAccountName(state);
  return {
    systemContactStatus: systemContact.status,
    systemContact: {
      contactNumber: systemContact.contactNumber,
      designation: systemContact.designation,
      email: systemContact.email,
      name: userName,
    },
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    submitForm: (values: any) => {
      dispatch(updateSystemContactRequested(values));
    },
  };
};

export const AccountInfoContainer = connect(mapStateToProps, mapDispatchToProps)(AccountInfo);
