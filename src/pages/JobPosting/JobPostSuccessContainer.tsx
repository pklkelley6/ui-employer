import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {withRouter, RouteComponentProps} from 'react-router';
import {JobPostSuccess} from './JobPostSuccess';
import {completeJobPosting} from '~/flux/jobPosting/jobPosting.actions';

export interface IJobPostSuccessProps extends RouteComponentProps<{uuid: string}> {
  completeJobPosting: () => void;
}
export interface IJobPostSuccessDispatchProps {
  completeJobPosting: ActionType<typeof completeJobPosting>;
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    completeJobPosting: () => {
      dispatch(completeJobPosting());
    },
  };
};

export const JobPostSuccessContainer = connect(null, mapDispatchToProps)(withRouter(JobPostSuccess));
