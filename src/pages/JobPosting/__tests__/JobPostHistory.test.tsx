import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {JobPostHistory} from '../JobPostHistory';
import {JobPostHistoryType} from '../JobPostHistoryContainer';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {ACTION_TYPES} from '~/services/employer/jobs.types';
import {
  JOB_POST_HISTORY_FETCH_SUCCEEDED,
  JOB_POST_HISTORY_FETCH_FAILED,
} from '~/flux/jobPostHistory/jobPostHistory.constants';

const mockFetchJobPostHistory = jest.fn();

const defaultJobPostHistory: JobPostHistoryType = {
  mockuuid: {
    fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
    jobPostHistory: [
      {
        jobPostUpdatedAt: '2020-10-23T00:00:00.000Z',
        jobPostExpiryDate: '2020-10-30',
        actionType: ACTION_TYPES.CREATE,
      },
    ],
  },
};

describe('JobPosts/JobPostHistory', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('after successfully fetching job post history', () => {
    it('fetches job post history on mount', () => {
      mountComponent({});
      expect(mockFetchJobPostHistory).toBeCalledTimes(1);
    });

    it('renders correctly', () => {
      const wrapper = mountComponent({});
      expect(wrapper.find('[data-cy="job-post-history"]').text()).toContain('Created on:23 Oct 2020');
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('does not render if there are no history for the job', () => {
      const wrapper = mountComponent({
        jobPostHistory: {
          mockuuid: {
            fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
            jobPostHistory: [],
          },
        },
      });
      expect(wrapper.text()).toEqual('');
    });

    describe('layout', () => {
      it.each`
        history                                                                                                              | description
        ${{jobPostUpdatedAt: '2020-10-23T00:00:00.000Z', jobPostExpiryDate: '2020-10-30', actionType: ACTION_TYPES.CREATE}}  | ${'Created on:23 Oct 2020'}
        ${{jobPostUpdatedAt: '2020-10-22T20:00:00.000Z', jobPostExpiryDate: '2020-10-30', actionType: ACTION_TYPES.EDIT}}    | ${'Edited on:23 Oct 2020'}
        ${{jobPostUpdatedAt: '2020-10-23T15:59:00.000Z', jobPostExpiryDate: '2020-10-30', actionType: ACTION_TYPES.REPOST}}  | ${'Reposted on:23 Oct 2020'}
        ${{jobPostUpdatedAt: '2020-10-23T16:00:00.000Z', jobPostExpiryDate: '2020-10-30', actionType: ACTION_TYPES.CLOSE}}   | ${'Closed on:24 Oct 2020'}
        ${{jobPostUpdatedAt: '2020-10-31T00:00:00.000Z', jobPostExpiryDate: '2020-10-30', actionType: ACTION_TYPES.EXPIRED}} | ${'Closed on:30 Oct 2020'}
      `(`displays $description description for $actionType action type`, ({history, description}) => {
        const wrapper = mountComponent({
          jobPostHistory: {
            mockuuid: {
              fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
              jobPostHistory: [history],
            },
          },
        });
        expect(wrapper.find('[data-cy="job-post-history"]').text()).toContain(description);
      });

      it('has a divider after closed or expired actions', () => {
        const wrapper = mountComponent({
          jobPostHistory: {
            mockuuid: {
              fetchStatus: JOB_POST_HISTORY_FETCH_SUCCEEDED,
              jobPostHistory: [
                {
                  jobPostUpdatedAt: '2020-10-23T00:00:00.000Z',
                  jobPostExpiryDate: '2020-10-30',
                  actionType: ACTION_TYPES.CREATE,
                },
                {
                  jobPostUpdatedAt: '2020-10-24T00:00:00.000Z',
                  jobPostExpiryDate: '2020-10-23',
                  actionType: ACTION_TYPES.EXPIRED,
                },
                {
                  jobPostUpdatedAt: '2020-10-25T00:00:00.000Z',
                  jobPostExpiryDate: '2020-10-30',
                  actionType: ACTION_TYPES.REPOST,
                },
                {
                  jobPostUpdatedAt: '2020-10-26T00:00:00.000Z',
                  jobPostExpiryDate: '2020-10-30',
                  actionType: ACTION_TYPES.CLOSE,
                },
                {
                  jobPostUpdatedAt: '2020-10-27T00:00:00.000Z',
                  jobPostExpiryDate: '2020-10-30',
                  actionType: ACTION_TYPES.REPOST,
                },
              ],
            },
          },
        });

        expect(wrapper.find('[data-cy="job-post-history-divider"]')).toHaveLength(3);
      });
    });
  });

  describe('after failing to fetch job post history', () => {
    it('should show the error message', () => {
      const wrapper = mountComponent({
        jobPostHistory: {
          mockuuid: {
            fetchStatus: JOB_POST_HISTORY_FETCH_FAILED,
            jobPostHistory: [],
          },
        },
      });
      expect(wrapper.text()).toContain('This information is temporarily unavailable. Please try again later.');
    });
  });
});

const mountComponent = ({
  jobUuid = 'mockuuid',
  jobPostHistory = defaultJobPostHistory,
  fetchJobPostHistory = mockFetchJobPostHistory,
}) =>
  mount(<JobPostHistory jobUuid={jobUuid} jobPostHistory={jobPostHistory} fetchJobPostHistory={fetchJobPostHistory} />);
