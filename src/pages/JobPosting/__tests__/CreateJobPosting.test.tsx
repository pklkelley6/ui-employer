import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {CreateJobPosting} from '../CreateJobPosting';
import {jobPostingSteps} from '~/components/JobPosting/useJobPostingSteps';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import * as randomUtil from '~/util/generateRandomString';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

jest.mock('~/components/JobPosting/formInitialState');
jest.spyOn(randomUtil, 'generateRandomSha256').mockImplementation(() => Promise.resolve('randomString'));
jest.spyOn(randomUtil, 'generateSha256FromString').mockImplementation(() => Promise.resolve('randomString'));

describe('JobPosts/CreateJobPosting', () => {
  const mock: any = {};
  const routeComponentProps = {
    history: {
      ...mock,
      push: jest.fn(),
    },
    location: {
      ...mock,
      hash: jobPostingSteps[1].id,
      state: 'skywalker',
    },
    match: mock,
  };
  const dispatchProps = {
    onJobPostingScreeningQuestionsAdded: jest.fn(),
  };
  const store = configureStore()({
    jobTitles: {
      results: [],
    },
    account: {
      data: {
        exp: uenUserMock.exp,
      },
    },
    features: {
      screeningQuestions: true,
    },
  });
  it('should render correctly', async () => {
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router initialEntries={[{pathname: '/', key: 'key'}]}>
            <CreateJobPosting {...routeComponentProps} {...dispatchProps} postJobPosting={jest.fn()} />
          </Router>
        </Provider>
      </MockedProvider>,
    );

    await nextTick(wrapper);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render edit link in preview stage for screening questions', async () => {
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router initialEntries={[{pathname: '/', key: 'key', hash: '#preview'}]}>
            <CreateJobPosting {...routeComponentProps} {...dispatchProps} postJobPosting={jest.fn()} />
          </Router>
        </Provider>
      </MockedProvider>,
    );

    await nextTick(wrapper);

    expect(wrapper.find('Link[id="#screening-questions"]')).toHaveLength(1);
  });
});
