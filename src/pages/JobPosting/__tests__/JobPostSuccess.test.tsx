import {Interaction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {JobPostSuccess} from '../JobPostSuccess';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {PactBuilder} from '~/__mocks__/pact';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {API_JOB_VERSION} from '~/services/util/getApiJobRequestInit';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('JobPosts/JobPostSuccess', () => {
  const locationMock: any = {};
  const historyMock: any = {};
  const completeJobPosting = jest.fn();
  const routeComponentProps = {
    history: historyMock,
    location: locationMock,
    match: {
      isExact: true,
      params: {uuid: jobPostMock.uuid},
      path: '',
      url: '',
    },
  };
  const store = configureStore()();

  let jobPactBuilder: PactBuilder;
  beforeAll(async () => {
    jobPactBuilder = new PactBuilder('api-job');
    await jobPactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${jobPactBuilder.host}:${jobPactBuilder.port}/v2`);
  });
  afterEach(async () => {
    completeJobPosting.mockClear();
  });
  afterAll(async () => {
    await jobPactBuilder.provider.finalize();
  });
  describe('job posted successful page', () => {
    it('should render correctly', async () => {
      await addJobPostingInteraction();
      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...routeComponentProps} />
          </MemoryRouter>
        </Provider>,
      );
      await act(async () => {
        await jobPactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should call completeJobPosting on mount', async () => {
      await addJobPostingInteraction();
      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...routeComponentProps} />
          </MemoryRouter>
        </Provider>,
      );
      wrapper.update();
      expect(completeJobPosting).toBeCalledTimes(1);
    });

    it('should render from location state instead of fetch remotely when there is data in state', async () => {
      const routeComponentWithLocationStateProps = {
        ...routeComponentProps,
        location: {
          ...routeComponentProps.location,
          state: jobPostMock,
        },
      };
      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...routeComponentWithLocationStateProps} />
          </MemoryRouter>
        </Provider>,
      );
      await nextTick(wrapper);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should render warning message to confirm the expiryDate and its jobDuration from mcf job response when differs from the client system expiryDate', async () => {
      const routeComponentWithLocationStateProps = {
        ...routeComponentProps,
        location: {
          ...routeComponentProps.location,
          state: {...jobPostMock, clientExpiryDate: '2020-10-09'},
        },
      };

      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...routeComponentWithLocationStateProps} />
          </MemoryRouter>
        </Provider>,
      );

      await nextTick(wrapper);

      expect(wrapper.find('[data-cy="warning-success-job-expiry-date"]')).toHaveLength(1);
    });
  });

  describe('job edited successful page', () => {
    it('should have the correct header text', async () => {
      await addJobPostingInteraction({uuid: '478f7071c1aa46dbb35212500105a07e', editCount: 1});
      const editRouteComponentProps = {
        ...routeComponentProps,
        match: {
          ...routeComponentProps.match,
          params: {uuid: '478f7071c1aa46dbb35212500105a07e'},
        },
      };
      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...editRouteComponentProps} />
          </MemoryRouter>
        </Provider>,
      );
      await act(async () => {
        await jobPactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should render from location state instead of fetch remotely when there is data in state', async () => {
      const routeComponentWithLocationStateProps = {
        ...routeComponentProps,
        location: {
          ...routeComponentProps.location,
          state: {
            ...jobPostMock,
            metadata: {
              ...jobPostMock.metadata,
              editCount: 2,
            },
          },
        },
      };
      const wrapper = mount(
        <Provider store={store}>
          <MemoryRouter>
            <JobPostSuccess completeJobPosting={completeJobPosting} {...routeComponentWithLocationStateProps} />
          </MemoryRouter>
        </Provider>,
      );
      await nextTick(wrapper);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });
  });

  const addJobPostingInteraction = async ({uuid = jobPostMock.uuid, editCount = 0} = {}) => {
    const getJobPosting = new Interaction()
      .uponReceiving(`get job posting by uuid ${uuid}`)
      .withRequest({
        headers: {
          [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
        },
        method: 'GET',
        path: `/v2/jobs/${uuid}`,
      })
      .willRespondWith({
        body: Matchers.like({
          metadata: {
            editCount,
            expiryDate: jobPostMock.metadata.expiryDate,
            jobPostId: jobPostMock.metadata.jobPostId,
            updatedAt: jobPostMock.metadata.updatedAt,
          },
          title: jobPostMock.title,
        }),
        headers: {
          'access-control-allow-credentials': 'true',
        },
        status: 200,
      });
    await jobPactBuilder.provider.addInteraction(getJobPosting);
  };
});
