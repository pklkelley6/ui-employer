import {connect} from 'react-redux';
import {CreateJobPosting} from './CreateJobPosting';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {postJobPosting} from '~/services/employer/jobs';
import {onJobPostingScreeningQuestionsAdded} from '~/flux/analytics';

export interface ICreateJobPostingDispatchProps {
  onJobPostingScreeningQuestionsAdded: typeof onJobPostingScreeningQuestionsAdded;
}

const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state), postJobPosting});

export const CreateJobPostingContainer = connect(mapStateToProps, {onJobPostingScreeningQuestionsAdded})(
  CreateJobPosting,
);
