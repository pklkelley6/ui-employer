import {differenceInDays, parseISO, format} from 'date-fns';
import {IJobPost} from '~/services/employer/jobs.types';

export const newJobPostingExpiryDateValidation = (job: IJobPost) => {
  const {expiryDate, newPostingDate} = job.metadata;
  const jobDuration = differenceInDays(parseISO(expiryDate), parseISO(newPostingDate));
  return `Please note the job posting expires on ${format(
    parseISO(expiryDate),
    'dd MMM yyyy',
  )}, ${jobDuration} days from ${format(parseISO(newPostingDate), 'dd MMM yyyy')}.`;
};
