import {addDays, format} from 'date-fns';
import {FORM_ERROR} from 'final-form';
import React, {useEffect, useState} from 'react';
import {RouteComponentProps} from 'react-router';
import styles from './CreateJobPosting.scss';
import {ICreateJobPostingDispatchProps} from './CreateJobPostingContainer';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {JobPostingFormContainer} from '~/components/JobPosting/JobPostingFormContainer';
import {HeaderContainer} from '~/components/Navigation/Header';
import {IUser} from '~/flux/account';
import {StaticNudge} from '~/pages/JobPosting/StaticNudge';
import {postJobPosting} from '~/services/employer/jobs';
import {transformFormStateToJobPostInput} from '~/util/transformJobPosting';
import {generateRandomSha256, generateSha256FromString} from '~/util/generateRandomString';

interface ICreateJobPostingProps extends RouteComponentProps {
  postJobPosting: typeof postJobPosting;
  user?: IUser;
}

export const CreateJobPosting: React.FunctionComponent<ICreateJobPostingProps & ICreateJobPostingDispatchProps> = (
  props,
) => {
  const [jobTitle, setJobTitle] = useState<string | undefined>();
  const [formId, setFormId] = useState<string>('');
  const title = 'Create Job Posting';
  const crumbsTitle = jobTitle ? `${title} - ${jobTitle}` : title;

  useEffect(() => {
    document.title = 'Create Job Posting | MyCareersFuture Employer';
    (async () => {
      const sha256 = await generateRandomSha256();
      setFormId(sha256);
    })();
  }, []);

  const onSubmit = async (values: IJobPostingFormState) => {
    try {
      if (!props.user) {
        throw new Error('Invalid User');
      }
      const clientExpiryDate = format(addDays(new Date(), values.keyInformation.jobPostDuration!), 'yyyy-MM-dd');
      const payload = transformFormStateToJobPostInput(values, props.user);
      const idempotencyKey = await generateSha256FromString(JSON.stringify(payload) + formId);
      const response = await props.postJobPosting(payload, {idempotencyKey});
      if (response.screeningQuestions?.length) {
        props.onJobPostingScreeningQuestionsAdded(response.metadata.jobPostId, response.screeningQuestions.length);
      }
      props.history.push(`/jobs/${response.uuid}/success`, {
        ...response,
        clientExpiryDate, // check if client calculated expiry is different from server expiry
      });
    } catch (error) {
      if (error instanceof Error) {
        return {[FORM_ERROR]: error.message};
      }
    }
  };

  return (
    <section className="flex-auto-ie">
      <HeaderContainer data-cy="manage-applicants-all-jobs" crumbs={[crumbsTitle]} />
      <main className={`flex pa3 ${styles.formContainer}`}>
        <JobPostingFormContainer onTitleChange={setJobTitle} onSubmit={onSubmit} />
        <aside className={`pl3 ${styles.asideBar}`}>
          <StaticNudge />
        </aside>
      </main>
    </section>
  );
};
