import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {JobPostHistory} from './JobPostHistory';
import {IAppState} from '~/flux/index';
import {fetchJobPostHistoryRequested} from '~/flux/jobPostHistory/jobPostHistory.actions';
import {IJobPostHistoryState} from '~/flux/index.reducers';

type Pluck<T, K extends keyof T> = T[K];
export type JobPostHistoryType = Pluck<IJobPostHistoryState, 'jobPostHistory'>;

export interface IJobPostHistoryContainerStateProps {
  jobUuid: string;
  jobPostHistory?: JobPostHistoryType;
}

export interface IJobPostHistoryContainerDispatchProps {
  fetchJobPostHistory: (jobUuid: string) => ActionType<typeof fetchJobPostHistoryRequested>;
}

export type IJobPostHistoryProps = IJobPostHistoryContainerStateProps & IJobPostHistoryContainerDispatchProps;

export const mapStateToProps = ({jobPostHistory: {jobPostHistory}}: IAppState) => {
  return {
    jobPostHistory,
  };
};

export const JobPostHistoryContainer = connect(mapStateToProps, {
  fetchJobPostHistory: fetchJobPostHistoryRequested,
})(JobPostHistory);
