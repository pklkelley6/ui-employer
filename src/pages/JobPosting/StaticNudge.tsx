import React from 'react';

export const StaticNudge = () => {
  return (
    <>
      <div data-cy="static-nudge" className="bg-washed-yellow pa3 lh-title black-80 f5">
        <p className="ma0 fw6">Be sure to follow TAFEP fair advertisement guidelines</p>
        <p className="ma0 fw4 pt2">Write fair job advertisements. Avoid discriminatory phrases in your job posting.</p>
        <a
          className="fw4 pt3 blue underline dib"
          href="https://www.tal.sg/tafep/Employment-Practices/Recruitment/Job-Advertisements"
          target="_blank"
        >
          Learn More
        </a>
      </div>
    </>
  );
};
