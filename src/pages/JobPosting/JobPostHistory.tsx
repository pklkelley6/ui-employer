import React, {useEffect, Fragment} from 'react';
import {IJobPostHistoryProps} from './JobPostHistoryContainer';
import styles from './JobPostHistory.scss';
import {ACTION_TYPES} from '~/services/employer/jobs.types';
import {formatISODate} from '~/util/date';
import {
  JOB_POST_HISTORY_FETCH_FAILED,
  JOB_POST_HISTORY_FETCH_SUCCEEDED,
} from '~/flux/jobPostHistory/jobPostHistory.constants';

export const JobPostHistory: React.FunctionComponent<IJobPostHistoryProps> = ({
  jobUuid,
  jobPostHistory,
  fetchJobPostHistory,
}) => {
  useEffect(() => {
    fetchJobPostHistory(jobUuid);
  }, []);

  const divider = <hr data-cy="job-post-history-divider" className={`${styles.divider} w-100 center ma3`} />;

  const formatActionDescription = (actionType: ACTION_TYPES) => {
    switch (actionType) {
      case ACTION_TYPES.CREATE:
        return 'Created on:';
      case ACTION_TYPES.EDIT:
        return 'Edited on:';
      case ACTION_TYPES.REPOST:
        return 'Reposted on:';
      case ACTION_TYPES.CLOSE:
      case ACTION_TYPES.EXPIRED:
        return 'Closed on:';
      default:
        return '';
    }
  };

  const formattedJobPostHistoryList = (jobPostHistory?.[jobUuid]?.jobPostHistory ?? []).map((entry, idx, array) => {
    return (
      <Fragment key={idx}>
        <p className="w-60 ma0 f6 pv1 fw4">{formatActionDescription(entry.actionType)}</p>
        <p className="w-40 ma0 f6 pv1 fw6 tl">
          {formatISODate(entry.actionType === ACTION_TYPES.EXPIRED ? entry.jobPostExpiryDate : entry.jobPostUpdatedAt)}
        </p>
        {idx + 1 === array.length ||
        entry.actionType === ACTION_TYPES.CLOSE ||
        entry.actionType === ACTION_TYPES.EXPIRED
          ? divider
          : ''}
      </Fragment>
    );
  });

  if (jobPostHistory?.[jobUuid]?.fetchStatus === JOB_POST_HISTORY_FETCH_FAILED) {
    return (
      <div data-cy="job-post-history" className="bg-black-05 pa3 lh-title black-80 f5 mb4 flex flex-wrap">
        <p className="w-100 ma0 fw7 mb3">JOB POSTING HISTORY</p>
        <span className="i f6 black-50">This information is temporarily unavailable. Please try again later.</span>
      </div>
    );
  } else if (
    jobPostHistory &&
    jobPostHistory[jobUuid]?.fetchStatus === JOB_POST_HISTORY_FETCH_SUCCEEDED &&
    formattedJobPostHistoryList.length
  ) {
    return (
      <div data-cy="job-post-history" className="bg-black-05 pa3 lh-title black-80 f5 mb3 flex flex-wrap">
        <p className="w-100 ma0 fw7 mb3">JOB POSTING HISTORY</p>
        {formattedJobPostHistoryList}
        <p className="f6 lh-copy ma0">
          For EP and S Pass application, please note that the job post must be opened for at least 28 days from the
          create/repost/edit date (whichever is later).
        </p>
      </div>
    );
  }

  return <></>;
};
