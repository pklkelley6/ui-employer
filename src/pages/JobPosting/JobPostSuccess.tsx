import {parseISO, format} from 'date-fns';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {useAsync} from 'react-use';
import styles from './JobPostSuccess.scss';
import {StaticNudge} from './StaticNudge';
import {IJobPostSuccessProps} from './JobPostSuccessContainer';
import {newJobPostingExpiryDateValidation} from './newJobPostingExpiryDateValidation';
import {EditsRemainingCounter} from '~/components/EditsRemainingCounter/EditsRemainingCounter';
import {HeaderContainer} from '~/components/Navigation/Header';
import {config} from '~/config';
import {getJobPosting} from '~/services/employer/jobs';

export const JobPostSuccess: React.FunctionComponent<IJobPostSuccessProps> = ({
  match,
  location,
  completeJobPosting,
}) => {
  const [crumbTitle, setCrumbTitle] = useState('Job posted successfully');
  const [successMessage, setSuccessMessage] = useState('You have successfully posted the following job:');
  const jobUuid = match.params.uuid;

  const state = useAsync(async () => location.state || getJobPosting(jobUuid));
  const jobPosting = state.value;
  const hasExpiryDateChangedForClient =
    jobPosting && jobPosting.clientExpiryDate ? jobPosting.clientExpiryDate !== jobPosting.metadata.expiryDate : null;

  useEffect(() => {
    completeJobPosting();
  }, []);
  useEffect(() => {
    if (jobPosting && jobPosting.metadata.editCount > 0) {
      setCrumbTitle('Job edited successfully');
      setSuccessMessage('You have successfully edited the following job:');
    }
  }, [jobPosting]);

  return (
    <main className="flex-auto-ie">
      <HeaderContainer crumbs={[crumbTitle]} />
      <section data-cy="job-post-success-page" className={`flex pa3 ${styles.formContainer}`}>
        <div className="flex-column w-100">
          <div className="flex flex-column bg-white black-80 pa3 pv4">
            <i title="success" className={`${styles.successCheckIcon} ma3`} />
            <div data-cy="success-info" className="ma3">
              <div className="lh-copy f4 pt3 black-60">{successMessage}</div>
              {jobPosting ? (
                <>
                  <div className="f4 pt3 fw6 black-80 mt3" data-cy="success-job-title">
                    {jobPosting.title}
                  </div>
                  <div className="f4 pt2 fw4 black-60" data-cy="success-job-post-id">
                    {jobPosting.metadata.jobPostId}
                  </div>
                  <div className="f5 pt3 black-60" data-cy="success-job-expiry-date">{`Auto-closes on: ${format(
                    parseISO(jobPosting.metadata.expiryDate),
                    'dd MMM yyyy',
                  )}`}</div>
                  {jobPosting && jobPosting.clientExpiryDate && hasExpiryDateChangedForClient ? (
                    <div className="f5 pt3 red" data-cy="warning-success-job-expiry-date">
                      {newJobPostingExpiryDateValidation(jobPosting)}
                    </div>
                  ) : null}
                </>
              ) : null}
              <a
                data-cy="success-redirect-jobseeker"
                href={`${config.url.mcf}/job/${jobUuid}`}
                target="_blank"
                title="Posted Job"
                className="dib blue underline pt4"
              >
                View Posted Job
              </a>
            </div>
          </div>
          <div className="flex justify-end pt3">
            <Link
              to="/jobs"
              data-cy="success-redirect-employer"
              title="All Jobs"
              className={`${styles.button} db ml2 b--primary white bg-primary pa3 tc`}
            >
              Return to All Jobs
            </Link>
          </div>
        </div>
        <aside className={`pl3 ${styles.asideBar}`}>
          {jobPosting && jobPosting.metadata.editCount > 0 ? (
            <EditsRemainingCounter editsMade={jobPosting.metadata.editCount} />
          ) : null}
          <StaticNudge />
        </aside>
      </section>
    </main>
  );
};
