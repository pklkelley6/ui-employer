import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {IJobPostingStep, useJobPostingSteps} from '~/components/JobPosting/useJobPostingSteps';
import {JobDescriptionPreview} from '~/components/JobPosting/PreviewInfo/JobDescriptionPreview';
import {KeyInformationPreview} from '~/components/JobPosting/PreviewInfo/KeyInformationPreview';
import {MSFSkillsPreview} from '~/components/JobPosting/PreviewInfo/MSFSkillsPreview';
import {SkillsPreview} from '~/components/JobPosting/PreviewInfo/SkillsPreview';
import {WorkplaceDetailsPreview} from '~/components/JobPosting/PreviewInfo/WorkplaceDetailsPreview';
import {IJobPost} from '~/services/employer/jobs.types';
import {isMCFJob} from '~/util/isMCFJob';
import {transformJobPostingToFormState} from '~/util/transformJobPosting';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {ScreeningQuestionsPreview} from '~/components/JobPosting/PreviewInfo/ScreeningQuestionsPreview';

export const ViewJobPosting: React.FunctionComponent<{jobPosting: IJobPost}> = ({jobPosting}) => {
  const jobStatusId: JobStatusCodes = jobPosting.status.id;

  const renderPreviewComponent = (step: IJobPostingStep) => {
    switch (step.id) {
      case '#job-description':
        return <JobDescriptionPreview />;
      case '#skills':
        return isMCFJob(jobPosting.metadata.jobPostId) ? <SkillsPreview /> : <MSFSkillsPreview />;
      case '#key-information':
        return <KeyInformationPreview jobStatusId={jobStatusId} />;
      case '#workplace-details':
        return <WorkplaceDetailsPreview />;
      case '#screening-questions':
        return <ScreeningQuestionsPreview />;
      default:
        return <>Blank</>;
    }
  };

  const jobPostingSteps = useJobPostingSteps();
  const previewSteps = jobPostingSteps.slice(0, -1);

  return (
    <Form
      onSubmit={noop}
      initialValues={transformJobPostingToFormState(jobPosting)}
      render={() => (
        <section className="flex-auto db flex-column bg-black-05 mb3 pa4 pt4">
          {previewSteps.map((step, index) => (
            <section className="pa3" key={step.id} data-cy={`${step.id.slice(1)}_preview`}>
              <div className="flex justify-between" data-cy={step.id.slice(1)}>
                <h4 className="ma0 mb3 secondary">
                  {index + 1}. {step.description.toUpperCase()}
                </h4>
              </div>
              {renderPreviewComponent(step)}
              {previewSteps.length === index + 1 ? null : <div className="b--black-20 bt mt3 mb4 w-100" />}
            </section>
          ))}
        </section>
      )}
    />
  );
};
