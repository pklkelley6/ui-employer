import {FORM_ERROR} from 'final-form';
import {noop} from 'lodash/fp';
import React, {useRef, useState} from 'react';
import {RouteComponentProps} from 'react-router';
import {EditJobSubmitModal} from '~/components/EditJobPosting/EditJobSubmitModal';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {JobPostingFormContainer} from '~/components/JobPosting/JobPostingFormContainer';
import {ReadOnlyScreeningQuestionsFields} from '~/components/JobPosting/ScreeningQuestions/ReadOnlyScreeningQuestionsFields';
import {EditKeyInformationFields} from '~/components/JobPosting/KeyInformation/EditKeyInformationFields';
import {EditJobSkillsFields} from '~/components/JobPosting/Skills/EditJobSkillsFields';
import {EditWorkplaceDetailsFields} from '~/components/JobPosting/WorkplaceDetails/EditWorkplaceDetailsFields';
import {IUser} from '~/flux/account';
import Preview from '~/components/JobPosting/Preview';
import {putJobPost} from '~/services/employer/jobs';
import {transformJobPostingToFormState, transformFormStateToJobPostInput} from '~/util/transformJobPosting';
import {IJobPost} from '~/services/employer/jobs.types';

interface IEditJobPostingProps extends RouteComponentProps {
  jobPosting: IJobPost;
  user?: IUser;
  onTitleChange: (values: IJobPostingFormState['jobDescription']['title']) => void;
}

export const EditJobPosting: React.FunctionComponent<IEditJobPostingProps> = ({
  history,
  jobPosting,
  onTitleChange,
  user,
}) => {
  const [displaySubmitModal, setDisplaySubmitModal] = useState(false);
  const confirmationPromiseRef = useRef<{
    resolve: () => void;
    reject: () => void;
  }>({resolve: noop, reject: noop});

  const onSubmit = async (values: IJobPostingFormState) => {
    const confirmationPromise = new Promise((resolve, reject) => {
      confirmationPromiseRef.current = {resolve: () => resolve(undefined), reject};
    });

    setDisplaySubmitModal(true);
    try {
      await confirmationPromise;
    } catch {
      return setDisplaySubmitModal(false);
    }

    try {
      if (!user) {
        throw new Error('Invalid User');
      }
      const response = await putJobPost(jobPosting.uuid, transformFormStateToJobPostInput(values, user));
      history.push(`/jobs/${jobPosting.uuid}/success`, response);
    } catch (error) {
      setDisplaySubmitModal(false);
      if (error instanceof Error) {
        return {[FORM_ERROR]: error.message};
      }
    }
  };

  return (
    <>
      <JobPostingFormContainer
        initialValues={transformJobPostingToFormState(jobPosting)}
        jobSkills={<EditJobSkillsFields />}
        keyInformation={<EditKeyInformationFields />}
        screeningQuestions={<ReadOnlyScreeningQuestionsFields />}
        workplaceDetails={<EditWorkplaceDetailsFields />}
        onTitleChange={onTitleChange}
        onSubmit={onSubmit}
        preview={<Preview isScreeningQuestionsReadOnly={true} />}
      />
      {displaySubmitModal && (
        <EditJobSubmitModal
          editCount={jobPosting.metadata.editCount}
          onCancel={() => confirmationPromiseRef.current.reject()}
          onSubmit={() => confirmationPromiseRef.current.resolve()}
        />
      )}
    </>
  );
};
