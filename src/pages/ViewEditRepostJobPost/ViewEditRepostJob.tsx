import React, {useEffect, useState} from 'react';
import {Route, RouteComponentProps} from 'react-router';
import {Link} from 'react-router-dom';
import {useAsync} from 'react-use';
import {parseISO} from 'date-fns';
import {useSelector} from 'react-redux';
import {JobPostHistoryContainer} from '../JobPosting/JobPostHistoryContainer';
import {PAGE_UNAVAILABLE} from '../PageUnavailable/PageUnavailableErrors.constants';
import {EditJobPostingContainer} from './EditJobPostingContainer';
import styles from './ViewEditJobPosting.scss';
import {ViewJobPosting} from './ViewJobPosting';
import {MAX_JOB_POST_EDITS} from '~/components/EditJobPosting/EditJobPost.constants';
import {EditsRemainingCounter} from '~/components/EditsRemainingCounter/EditsRemainingCounter';
import {JobPostingContact} from '~/components/Jobs/JobPostingContact';
import {RepostJobPostModal} from '~/components/Jobs/RepostJobPostModal';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {HeaderContainer} from '~/components/Navigation/Header';
import {FeatureFlag} from '~/components/Core/ToggleFlag';
import {StaticNudge} from '~/pages/JobPosting/StaticNudge';
import {getJobPosting} from '~/services/employer/jobs';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {isMCFJob} from '~/util/isMCFJob';
import {pathToJobUuid} from '~/util/url';
import {RepostJobCounter, canRepost} from '~/components/RepostJobPosting/RepostJobCounter';
import {RedirectUnauthorizedJob} from '~/components/Jobs/RedirectUnauthorizedJob';
import {getAccountUen} from '~/flux/account';
import {IAppState} from '~/flux';

interface IMatchParams {
  titleId: string;
}

export const ViewEditRepostJob: React.FunctionComponent<RouteComponentProps<IMatchParams>> = ({match, history}) => {
  const titleIdParam = match.params.titleId;
  const jobUuid = pathToJobUuid(titleIdParam);
  const state = useAsync(() => getJobPosting(jobUuid));
  const jobPosting = state && state.value;

  const [displayRepostModal, setDisplayRepostModal] = useState(false);
  const [crumbTitle, setCrumbTitle] = useState('View Job Posting');
  const [actionText, setActionText] = useState('View');
  const [jobTitle, setJobTitle] = useState<string>();

  const jobStatusId = jobPosting?.status.id;
  const jobPostId = jobPosting?.metadata.jobPostId || '';

  const newErrorPageFeatureToggle = useSelector(({features}: IAppState) => features.newErrorPage);
  const accountUen = useSelector((state: IAppState) => getAccountUen(state));
  const validAccount = accountUen === jobPosting?.postedCompany.uen;

  const viewEditJobDisplayEmailRecipientReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['179646046ViewEditJobDisplayEmailRecipient'],
  );

  useEffect(() => {
    if (isMCFJob(jobPostId)) {
      setActionText(jobStatusId === JobStatusCodes.Closed ? 'View/Repost' : 'View/Edit');
    } else {
      setActionText('View');
    }

    if (jobPosting) {
      setJobTitle(jobPosting.title);
    }
  }, [state.loading]);

  useEffect(() => {
    const titlePrefix = newErrorPageFeatureToggle && !validAccount ? PAGE_UNAVAILABLE : `${actionText} Job Posting `;
    document.title = `${titlePrefix} | MyCareersFuture Employer`;
    setCrumbTitle(`${actionText} Job Posting${jobTitle ? ` - ${jobTitle}` : ''}`);
  }, [actionText, jobTitle]);

  const repostJobButton = () => {
    const jobPostingRepostCount = jobPosting?.metadata.repostCount ?? 0;
    const jobPostingOriginalPostingDate = jobPosting?.metadata.originalPostingDate ?? '';

    return (
      <>
        {canRepost(jobPostingRepostCount, parseISO(jobPostingOriginalPostingDate)) ? (
          <button
            data-cy="repost-job"
            className="bg-primary white no-underline mb3 tc pa3 fw6 w-100 pointer"
            onClick={() => setDisplayRepostModal(true)}
          >
            Repost Job
          </button>
        ) : (
          <div data-cy="disabled-repost-job-post" className="bg-primary white o-50 db mb3 tc pa3 fw6">
            Repost Job
          </div>
        )}
        <Link
          data-cy="back-to-job-applications"
          className="ba b--primary no-underline db mb3 tc pa3 fw6"
          to="applications"
        >
          Back to Applications
        </Link>

        <RepostJobCounter
          originalPostingDate={parseISO(jobPostingOriginalPostingDate)}
          repostCount={jobPostingRepostCount}
        />
        {displayRepostModal && jobPosting && (
          <RepostJobPostModal job={jobPosting} onCancel={() => setDisplayRepostModal(false)} />
        )}
      </>
    );
  };

  const editJobLink = () => {
    const jobPostingEditCount = jobPosting?.metadata.editCount ?? 0;

    return (
      <>
        <Route
          path="/jobs/:titleId/view"
          render={() => (
            <>
              {jobPostingEditCount < MAX_JOB_POST_EDITS ? (
                <Link data-cy="edit-job-post" className="bg-primary white no-underline db mb3 tc pa3 fw6" to="edit">
                  Edit Job Posting
                </Link>
              ) : (
                <div data-cy="disabled-edit-job-post" className="bg-primary white o-50 db mb3 tc pa3 fw6">
                  Edit Job Posting
                </div>
              )}
              <Link
                data-cy="back-to-job-applications"
                className="ba b--primary no-underline db mb3 tc pa3 fw6"
                to="applications"
              >
                Back to Applications
              </Link>
            </>
          )}
        />
        <EditsRemainingCounter editsMade={jobPostingEditCount} />
      </>
    );
  };

  return (
    <section className="flex-auto-ie">
      <FeatureFlag
        name="newErrorPage"
        render={() =>
          validAccount ? <HeaderContainer data-cy="manage-applicants-all-jobs" crumbs={[crumbTitle]} /> : <></>
        }
        fallback={() => <HeaderContainer data-cy="manage-applicants-all-jobs" crumbs={[crumbTitle]} />}
      />

      {state.loading ? (
        <FormLoader className="o-50" cardNumber={2} />
      ) : state.error ? (
        <TemporarilyUnavailable
          message={temporaryUnavailableMsg}
          link={{
            text: 'Go back to previous page',
            action: () => history.goBack(),
          }}
        />
      ) : jobPosting ? (
        <RedirectUnauthorizedJob job={jobPosting}>
          <main className={`flex pa3 ${styles.formContainer}`}>
            <Route path="/jobs/:titleId/view" render={() => <ViewJobPosting jobPosting={jobPosting} />} />
            <Route
              path="/jobs/:titleId/edit"
              render={() => <EditJobPostingContainer jobPosting={jobPosting} onTitleChange={setJobTitle} />}
            />
            <aside className={`pl3 ${styles.asideBar}`}>
              {jobStatusId !== JobStatusCodes.Closed && isMCFJob(jobPostId) && editJobLink()}
              {jobStatusId === JobStatusCodes.Closed && isMCFJob(jobPostId) && repostJobButton()}
              {viewEditJobDisplayEmailRecipientReleaseToggle ? <JobPostingContact jobUuid={jobUuid} /> : <></>}
              {isMCFJob(jobPostId) ? <JobPostHistoryContainer jobUuid={jobPosting.uuid} /> : <></>}
              <StaticNudge />
            </aside>
          </main>
        </RedirectUnauthorizedJob>
      ) : null}
    </section>
  );
};
