import {MockedProvider} from '@apollo/react-testing';
import {mount, ReactWrapper} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {formatISO} from 'date-fns';
import {EditJobPostingContainer} from '../EditJobPostingContainer';
import {ViewEditRepostJob} from '../ViewEditRepostJob';
import {ViewJobPosting} from '../ViewJobPosting';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {MAX_JOB_POST_EDITS} from '~/components/EditJobPosting/EditJobPost.constants';
import {EditsRemainingCounter} from '~/components/EditsRemainingCounter/EditsRemainingCounter';
import {GET_EDUCATION_LIST, GET_SSIC_LIST, GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {GET_JOB_SYSTEM_CONTACT} from '~/graphql/systemContact/systemContact.query';
import * as getJobPostServices from '~/services/employer/jobs';
import {nextTick} from '~/testUtil/enzyme';
import {JobPostHistoryContainer} from '~/pages/JobPosting/JobPostHistoryContainer';

describe('ViewEditRepostJob', () => {
  const mock: any = {};
  const mcfJobPostMock = {
    ...jobPostMock,
    metadata: {
      ...jobPostMock.metadata,
      jobPostId: 'MCF-2009-7654321',
      repostCount: 0,
      originalPostingDate: formatISO(new Date()),
    },
    uuid: 'afab75dafd1c63f8dc3a8d909be8ef4a',
  };

  const routeComponentProps = {
    history: {
      ...mock,
      push: jest.fn(),
    },
    jobUuid: `${mcfJobPostMock.uuid}`,
  };

  const matchMock = {
    ...mock,
    params: {titleId: `${mcfJobPostMock.uuid}`},
  };

  const store = configureStore()({
    jobPostHistory: {},
    account: {
      data: uenUserMock,
    },
    features: {
      screeningQuestions: true,
    },
    releaseToggles: {
      '179646046ViewEditJobDisplayEmailRecipient': true,
    },
  });

  const getJobPostingMock = jest.spyOn(getJobPostServices, 'getJobPosting');

  beforeEach(() => {
    getJobPostingMock.mockImplementation(() => Promise.resolve({...mcfJobPostMock, hiringCompany: undefined}));
  });

  const previewMocks = [
    {
      request: {
        query: GET_SSOC_LIST,
      },
      result: {
        data: {
          common: {
            ssocList: [
              {
                ssoc: 1,
                ssocTitle: 'some',
              },
            ],
          },
        },
      },
    },
    {
      request: {
        query: GET_SSIC_LIST,
      },
      result: {
        data: {
          common: {
            ssicList: [
              {
                code: '1',
                description: 'some',
              },
            ],
          },
        },
      },
    },
    {
      request: {
        query: GET_EDUCATION_LIST,
      },
      result: {
        data: {
          common: {
            ssecEqaList: [{actualCode: '1', description: 'JOHN DOE'}],
            ssecFosList: [{actualCode: '1', description: 'JANE DOE'}],
          },
        },
      },
    },
  ];

  const jobContactMocks = [
    {
      request: {
        query: GET_JOB_SYSTEM_CONTACT,
        variables: {
          jobUuid: mcfJobPostMock.uuid,
        },
      },
      result: {
        data: {
          jobByUuid: {
            emailRecipientContact: {
              name: 'emailRecipientContact name',
              email: 'emailRecipientContact email',
            },
            createdByContact: {
              name: 'createdByContact name',
              email: 'createdByContact email',
            },
          },
        },
      },
    },
  ];

  const queryMocks = [...previewMocks, ...jobContactMocks];

  describe('/view', () => {
    const pathname = `/jobs/${mcfJobPostMock.uuid}/view`;

    describe('jobStatusId is closed', () => {
      const closedMcfJobPostMock = {
        ...mcfJobPostMock,
        status: {id: 9, jobStatus: 'Closed'},
      };
      let wrapper: ReactWrapper;
      beforeAll(async () => {
        getJobPostingMock.mockImplementation(() => Promise.resolve(closedMcfJobPostMock));
        await act(async () => {
          wrapper = mount(
            <MockedProvider mocks={queryMocks} addTypename={false}>
              <Provider store={store}>
                <MemoryRouter initialEntries={[pathname]}>
                  <ViewEditRepostJob
                    {...routeComponentProps}
                    location={{
                      ...mock,
                      pathname,
                    }}
                    match={matchMock}
                  />
                </MemoryRouter>
              </Provider>
            </MockedProvider>,
          );
        });

        wrapper.update();
      });

      it('should render repost job button if it is a closed MCF job', async () => {
        expect(wrapper.find('[data-cy="repost-job"]')).toHaveLength(1);
      });

      it('should render "Back to Applications" link button if it is a closed MCF job', async () => {
        expect(wrapper.find('[data-cy="back-to-job-applications"]').find('Link')).toHaveLength(1);
      });

      it('should not render Edit Job Posting link and count', async () => {
        expect(wrapper.find('[data-cy="edit-job-post"]')).toHaveLength(0);
        expect(wrapper.find(EditsRemainingCounter)).toHaveLength(0);
      });
    });

    it('should render "Back to Applications" link button if path is /view', async () => {
      let wrapper!: ReactWrapper;
      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      await nextTick(wrapper);
      expect(wrapper.find('[data-cy="back-to-job-applications"]').find('Link')).toHaveLength(1);
    });

    it('should render ViewJobPosting if path is /view', async () => {
      let wrapper!: ReactWrapper;
      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      await nextTick(wrapper);
      expect(wrapper.find(ViewJobPosting)).toHaveLength(1);
    });

    it('should not render Edit Job Posting link count when the job is a non-MCF job', async () => {
      getJobPostingMock.mockImplementation(() => Promise.resolve(jobPostMock));

      let wrapper!: ReactWrapper;

      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      await nextTick(wrapper);
      expect(wrapper.find(EditsRemainingCounter)).toHaveLength(0);
      expect(wrapper.find('[data-cy="disabled-edit-job-post"]')).toHaveLength(0);
      expect(wrapper.find('[data-cy="edit-job-post"]')).toHaveLength(0);
    });

    it('should disable Edit Job Posting link and render "Back to Applications" link button when edit count has reached its maximum edits', async () => {
      const mcfJobPostMockWithMaxEdits = {
        ...mcfJobPostMock,
        hiringCompany: undefined,
        metadata: {
          ...mcfJobPostMock.metadata,
          editCount: MAX_JOB_POST_EDITS,
          jobPostId: 'MCF-2019-29139213',
        },
      };

      getJobPostingMock.mockImplementation(() => Promise.resolve(mcfJobPostMockWithMaxEdits));

      let wrapper!: ReactWrapper;

      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      await nextTick(wrapper);
      expect(wrapper.find('[data-cy="disabled-edit-job-post"]')).toHaveLength(1);
      expect(wrapper.find('[data-cy="edit-job-post"]')).toHaveLength(0);
      expect(wrapper.find('[data-cy="back-to-job-applications"]').find('Link')).toHaveLength(1);
    });

    it('should not render repost job link when the job is a non-MCF job', async () => {
      getJobPostingMock.mockImplementation(() => Promise.resolve(jobPostMock));
      let wrapper!: ReactWrapper;

      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      wrapper.update();
      expect(wrapper.find('[data-cy="repost-job"]')).toHaveLength(0);
    });

    it('should not render job post history when the job is a non-MCF job', async () => {
      getJobPostingMock.mockImplementation(() => Promise.resolve(jobPostMock));
      let wrapper!: ReactWrapper;

      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[pathname]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      wrapper.update();
      expect(wrapper.find(JobPostHistoryContainer)).toHaveLength(0);
    });
  });

  describe('/edit', () => {
    it('should render JobPostingForm if path is /edit', async () => {
      const pathname = `/jobs/${mcfJobPostMock.uuid}/edit`;

      let wrapper!: ReactWrapper;
      await act(async () => {
        wrapper = mount(
          <MockedProvider mocks={queryMocks} addTypename={false}>
            <Provider store={store}>
              <MemoryRouter initialEntries={[{pathname, key: 'key'}]}>
                <ViewEditRepostJob
                  {...routeComponentProps}
                  location={{
                    ...mock,
                    pathname,
                  }}
                  match={matchMock}
                />
              </MemoryRouter>
            </Provider>
          </MockedProvider>,
        );
      });

      await nextTick(wrapper);
      expect(wrapper.find(EditJobPostingContainer)).toHaveLength(1);
    });
  });
});
