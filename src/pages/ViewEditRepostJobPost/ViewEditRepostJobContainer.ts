import {withRouter} from 'react-router';
import {ViewEditRepostJob} from '~/pages/ViewEditRepostJobPost/ViewEditRepostJob';

export const ViewEditRepostJobContainer = withRouter(ViewEditRepostJob);
