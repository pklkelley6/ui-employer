import React, {useEffect, useState} from 'react';
import {useLazyQuery} from 'react-apollo';
import {useSelector} from 'react-redux';
import styles from './TalentSearchPage.scss';
import {
  ITalentSearchPageContainerDispatchProps,
  ITalentSearchPageContainerStateProps,
} from './TalentSearchPageContainer';
import {SearchBox, SearchBoxType} from '~/components/SearchBox/SearchBox';
import {HeaderContainer} from '~/components/Navigation/Header/HeaderContainer';
import {GET_SEARCH_TALENTS} from '~/graphql/searchTalents/searchTalents.query';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {EmptySearchResults} from '~/components/TalentsSearch/EmptySearchResults';
import {NoSearchResults} from '~/components/TalentsSearch/NoSearchResults';
import {EmptySuggestedTalent} from '~/components/SuggestedTalents';
import {Pagination} from '~/components/Navigation/Pagination';
import {CandidateInfo} from '~/components/CandidateInfo/CandidateInfo';
import {getElapsedDays} from '~/util/date';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';

import {
  GetSearchTalentsQuery,
  GetSearchTalentsQueryVariables,
  GetSearchTalentsTalents,
} from '~/graphql/__generated__/types';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {TalentSearchResultsList} from '~/components/TalentsSearch/TalentSearchResultsList';

import {InviteTalentButton, MESSAGE_TYPE} from '~/components/InviteTalent/InviteTalentButton';
import {getOpenJobs} from '~/services/employer/jobs';
import {getAccountUen} from '~/flux/account';
import {IAppState} from '~/flux';
import {IJobsResponse, JobsSortCriteria} from '~/flux/jobPostings';
import {SortDirection} from '~/graphql/__generated__/types';
import {getInvitations, IGetInvitationsResponse} from '~/services/suggestedTalents/getInvitations';
import {InviteToApplyModalContainer} from '~/components/InviteTalent/InviteToApplyModalContainer';

type ITalentSearchPageProps = ITalentSearchPageContainerStateProps & ITalentSearchPageContainerDispatchProps;

export const TalentSearchPage: React.FunctionComponent<ITalentSearchPageProps> = ({
  onTalentSearchCandidateResumeClicked,
  onTalentSearchCandidatesPageClicked,
  onTalentSearchCandidateTabClicked,
  onTalentSearchQueried,
}) => {
  useEffect(() => {
    document.title = 'Talent Search | MyCareersFuture Employer';
  });

  const [openJobs, setOpenJobs] = useState<IJobsResponse>();
  const [invitations, setInvitations] = useState<IGetInvitationsResponse>();
  const [isInviteToApplyModalDisplayed, setIsInviteToApplyModalDisplayed] = useState<boolean>(false);

  const accountUen = useSelector((state: IAppState) => getAccountUen(state));

  useEffect(() => {
    const fetchData = async (uen: string) => {
      const result = await getOpenJobs(uen, {
        page: 0,
        limit: 100,
        orderBy: JobsSortCriteria.EXPIRY_DATE,
        orderDirection: SortDirection.Asc,
      });

      setOpenJobs(result);
    };

    if (accountUen) {
      fetchData(accountUen);
    }
  }, []);

  const searchTalentsLimit = 20;
  const maxVisiblePageButtons = 5;

  const [page, setPage] = useState(0);
  const [keywords, setKeywords] = useState('');

  const [selectedTalent, setSelectedTalent] = useState<GetSearchTalentsTalents>();

  const [getSearchTalents, {loading, error, data}] = useLazyQuery<
    GetSearchTalentsQuery,
    GetSearchTalentsQueryVariables
  >(GET_SEARCH_TALENTS, {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
    notifyOnNetworkStatusChange: true,
  });

  useEffect(() => {
    setSelectedTalent(undefined);
  }, [loading]);

  const talentsFound = (total: number) => total + (total > 1 ? ' talents found' : ' talent found');

  const inviteTalentButton = openJobs ? (
    openJobs.total == 0 ? (
      <InviteTalentButton message={MESSAGE_TYPE.UNABLE} />
    ) : openJobs.total == invitations?.total ? (
      <InviteTalentButton message={MESSAGE_TYPE.INVITED} />
    ) : (
      <InviteTalentButton message={MESSAGE_TYPE.SEND} onClick={() => setIsInviteToApplyModalDisplayed(true)} />
    )
  ) : (
    <></>
  );

  const panes = {
    left: (resetRightScroll: () => void) => (
      <>
        <TalentSearchResultsList
          data={data}
          onSelectTalent={async (talent) => {
            setSelectedTalent(talent);
            resetRightScroll();

            if (accountUen) {
              const result = await getInvitations(talent.id, accountUen);
              setInvitations(result);
            }
          }}
          selectedTalent={selectedTalent}
        />
        <Pagination
          currentPage={page}
          totalItemsLength={data?.searchTalents?.total || 0}
          itemsPerPage={searchTalentsLimit}
          maxVisiblePageButtons={maxVisiblePageButtons}
          onPageChange={(page) => {
            setPage(page);
            getSearchTalents({variables: {keywords, limit: searchTalentsLimit, skip: searchTalentsLimit * page}});
            onTalentSearchCandidatesPageClicked(page + 1);
          }}
        />
      </>
    ),
    right: selectedTalent ? (
      <CandidateInfo
        key={selectedTalent.id}
        candidate={selectedTalent}
        candidateType={CandidateType.SuggestedTalent}
        isBookmarked={false}
        isBookmarkVisible={false}
        formattedDate={`Active ${getElapsedDays(selectedTalent.lastLogin)}`}
        onResumeClick={onTalentSearchCandidateResumeClicked}
        onCandidateBookmarkClick={() => {}}
        hasResumeButtonOnBottom={true}
        candidateTabClicked={onTalentSearchCandidateTabClicked}
        contactSuggestedTalent={inviteTalentButton}
      />
    ) : (
      <EmptySuggestedTalent />
    ),
  };

  let results;

  if (error?.networkError) {
    results = <TemporarilyUnavailable />;
  } else if (loading) {
    results = <CardLoader className="pa3 pt4 w-50" cardNumber={8} />;
  } else {
    if (data?.searchTalents) {
      if (data.searchTalents.total) {
        results = (
          <>
            <div className="fw6 f5 black-50 ml4 mb4 pt4" data-cy="talent-search-results-total">
              {talentsFound(data.searchTalents.total)}
            </div>
            <CandidatesPanes {...panes} />
          </>
        );
      } else {
        results = NoSearchResults;
      }
    } else {
      results = EmptySearchResults;
    }
  }

  return (
    <>
      <section className="flex-auto-ie">
        <HeaderContainer data-cy="talent-search-header" crumbs={['Talent Search']} />
        <div className={`pl4 pr4 bg-black-50 bb b--black-05 ${styles.topSection}`}>
          <div className="flex black-60 pt4 mb2">
            <span className="fw6">Search from our talent pool</span>
          </div>
          <div className="flex pt1 pb5">
            <div className="flex-auto">
              <SearchBox
                placeholder="Search talent by skills, job titles, or keywords"
                clearable
                onSearch={(keywords) => {
                  if (keywords.length) {
                    setKeywords(keywords);
                    getSearchTalents({variables: {keywords, limit: searchTalentsLimit, skip: 0}});
                    setPage(0);
                    onTalentSearchQueried(keywords);
                  }
                }}
                searchBoxType={SearchBoxType.Talent}
                opts={{
                  maxLength: 50,
                }}
              />
            </div>
          </div>
        </div>
        <div data-cy="talent-search-results">{results}</div>
      </section>
      {isInviteToApplyModalDisplayed && selectedTalent?.id ? (
        <InviteToApplyModalContainer
          talent={selectedTalent.id}
          jobs={openJobs?.results}
          invitations={invitations?.data}
          onCancel={() => {
            setIsInviteToApplyModalDisplayed(false);
          }}
          onOk={async () => {
            setIsInviteToApplyModalDisplayed(false);
            if (accountUen) {
              const result = await getInvitations(selectedTalent.id, accountUen);
              setInvitations(result);
            }
          }}
        />
      ) : null}
    </>
  );
};
