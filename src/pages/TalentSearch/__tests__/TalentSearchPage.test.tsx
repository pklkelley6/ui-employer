import configureStore from 'redux-mock-store';
import React from 'react';
import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {MemoryRouter as Router} from 'react-router-dom';
import {mount, ReactWrapper} from 'enzyme';
import {Provider} from 'react-redux';
import {Interaction, Matchers} from '@pact-foundation/pact';
import {act} from 'react-dom/test-utils';
import {TalentSearchPage} from '../TalentSearchPage';
import {nextTick} from '~/testUtil/enzyme';

import {GET_SEARCH_TALENTS} from '~/graphql/searchTalents/searchTalents.query';
import {searchTalentsMock} from '~/__mocks__/searchTalents.mock';
import {PactBuilder} from '~/__mocks__/pact';
import {jobsMock} from '~/__mocks__/jobs/jobs.mocks';
import {API_JOB_VERSION} from '~/services/util/getApiJobRequestInit';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

jest.mock('~/services/employer/jobs', () => ({
  getOpenJobs: () => ({
    results: jobsMock,
    total: 1,
  }),
}));

describe('pages/TalentSearchPage', () => {
  let pactBuilder: PactBuilder;

  describe('search results', () => {
    it('should show empty search results', () => {
      const page = mountSearchTalentsPage();
      const emptySearchResults = page.find('[data-testid="empty-search-results"]');

      expect(emptySearchResults).toMatchSnapshot();
    });

    it('should show no search results found', async () => {
      const keywords = 'Alia Atreides';
      const query = [
        {
          request: {
            query: GET_SEARCH_TALENTS,
            variables: {
              keywords,
              skip: 0,
              limit: 20,
            },
          },
          result: {
            data: {
              searchTalents: {
                talents: [],
                total: 0,
              },
            },
          },
        },
      ];

      const wrapper = mountSearchTalentsPage(query);

      const searchInput = wrapper.find('[data-cy="search-input"]');
      expect(searchInput).toHaveLength(1);
      searchInput.getDOMNode<HTMLInputElement>().value = keywords;
      searchInput.simulate('change');

      wrapper.find('[data-cy="search-button"]').simulate('click');

      await nextTick(wrapper, 100);

      const noSearchResults = wrapper.find('[data-cy="no-search-results"]');
      expect(noSearchResults).toHaveLength(1);
      expect(noSearchResults).toMatchSnapshot();
    });

    it('should show results total', async () => {
      const keywords = 'Lady Jessica';
      const query = [
        {
          request: {
            query: GET_SEARCH_TALENTS,
            variables: {
              keywords,
              skip: 0,
              limit: 20,
            },
          },
          result: {
            data: {
              searchTalents: {
                talents: searchTalentsMock.talents,
                total: searchTalentsMock.total,
              },
            },
          },
        },
      ];

      const wrapper = mountSearchTalentsPage(query);

      const searchInput = wrapper.find('[data-cy="search-input"]');
      expect(searchInput).toHaveLength(1);
      searchInput.getDOMNode<HTMLInputElement>().value = keywords;
      searchInput.simulate('change');

      wrapper.find('[data-cy="search-button"]').simulate('click');

      await nextTick(wrapper, 100);

      const searchResultsTotal = wrapper.find('[data-cy="talent-search-results-total"]');
      expect(searchResultsTotal).toHaveLength(1);
      expect(searchResultsTotal.text()).toEqual('1 talent found');
      expect(searchResultsTotal).toMatchSnapshot();
    });

    it('should show error message', async () => {
      const keywords = 'Moneo Atreides';
      const query = [
        {
          request: {
            query: GET_SEARCH_TALENTS,
            variables: {
              keywords,
              skip: 0,
              limit: 20,
            },
          },
          error: new Error('An error occurred'),
        },
      ];

      const wrapper = mountSearchTalentsPage(query);

      const searchInput = wrapper.find('[data-cy="search-input"]');
      expect(searchInput).toHaveLength(1);
      searchInput.getDOMNode<HTMLInputElement>().value = keywords;
      searchInput.simulate('change');

      wrapper.find('[data-cy="search-button"]').simulate('click');

      await nextTick(wrapper, 100);

      const errorMessage = wrapper.find('[data-cy="error-message"]');
      expect(errorMessage).toHaveLength(1);
      expect(errorMessage).toMatchSnapshot();
    });
  });

  describe('search results with talents', () => {
    beforeAll(async () => {
      pactBuilder = new PactBuilder('api-job');
      await pactBuilder.setup();
      v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
    });

    afterAll(async () => {
      await pactBuilder.provider.finalize();
    });

    it('should request open jobs and invitations for selected talents', async () => {
      const companyUen = 'R90SS0001A';

      const keywords = 'Lady Jessica';
      const query = [
        {
          request: {
            query: GET_SEARCH_TALENTS,
            variables: {
              keywords,
              skip: 0,
              limit: 20,
            },
          },
          result: {
            data: {
              searchTalents: {
                talents: searchTalentsMock.talents,
                total: searchTalentsMock.total,
              },
            },
          },
        },
      ];

      const wrapper = mountSearchTalentsPageWithUen(query);

      const searchInput = wrapper.find('[data-cy="search-input"]');
      expect(searchInput).toHaveLength(1);
      searchInput.getDOMNode<HTMLInputElement>().value = keywords;
      searchInput.simulate('change');

      wrapper.find('[data-cy="search-button"]').simulate('click');

      await nextTick(wrapper, 100);

      const getInvitationsInteraction = new Interaction()
        .uponReceiving('invitations')
        .withRequest({
          headers: {
            [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
          },
          method: 'GET',
          path: '/v2/invitations',
          query: {
            talent: searchTalentsMock.talents![0].id,
            uen: companyUen,
          },
        })
        .willRespondWith({
          body: {data: [], total: Matchers.like(0)},
          status: 200,
        });

      await pactBuilder.provider.addInteraction(getInvitationsInteraction);

      const candidateListItem = wrapper.find('[data-cy="candidate-info"]');
      expect(candidateListItem).toHaveLength(1);
      candidateListItem.simulate('click');

      await nextTick(wrapper);

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      const inviteButton = wrapper.find('[data-cy="invite-talent-apply-button"]');
      expect(inviteButton).toHaveLength(1);
    });
  });
});

const baseVariables = {
  keywords: 'Lady Jessica',
};

const queryResult = {
  data: {
    searchTalents: {
      talents: [],
      total: 1,
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_SEARCH_TALENTS,
      variables: baseVariables,
    },
    result: queryResult,
  },
];

const store = configureStore()({
  account: {
    data: {
      userInfo: {
        entityId: undefined,
      },
    },
  },
  features: {enhancedSkillPills: true},
  releaseToggles: {'179899438enhancedSkillPills': true},
});

const storeWithUen = configureStore()({
  account: {
    data: {
      userInfo: {
        entityId: 'R90SS0001A',
      },
    },
  },
  features: {
    screeningQuestions: true,
    enhancedSkillPills: true,
  },
  releaseToggles: {'179899438enhancedSkillPills': true},
});

const dispatchProps = {
  onTalentSearchCandidateResumeClicked: jest.fn(),
  onTalentSearchCandidatesPageClicked: jest.fn(),
  onTalentSearchCandidateTabClicked: jest.fn(),
  onTalentSearchQueried: jest.fn(),
};
const mountSearchTalentsPage = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  return mount(
    <MockedProvider mocks={queryMocks} addTypename={false}>
      <Provider store={store}>
        <Router>
          <TalentSearchPage {...dispatchProps} />
        </Router>
      </Provider>
    </MockedProvider>,
  );
};

const mountSearchTalentsPageWithUen = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  return mount(
    <MockedProvider mocks={queryMocks} addTypename={false}>
      <Provider store={storeWithUen}>
        <Router>
          <TalentSearchPage {...dispatchProps} />
        </Router>
      </Provider>
    </MockedProvider>,
  );
};
