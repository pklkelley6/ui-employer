import {connect} from 'react-redux';
import {TalentSearchPage} from './TalentSearchPage';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {
  onTalentSearchCandidateResumeClicked,
  onTalentSearchCandidatesPageClicked,
  onTalentSearchCandidateTabClicked,
  onTalentSearchQueried,
} from '~/flux/analytics';
import {IUser} from '~/flux/account';

export interface ITalentSearchPageContainerStateProps {
  user?: IUser;
}
export interface ITalentSearchPageContainerDispatchProps {
  onTalentSearchCandidateResumeClicked: typeof onTalentSearchCandidateResumeClicked;
  onTalentSearchCandidatesPageClicked: typeof onTalentSearchCandidatesPageClicked;
  onTalentSearchCandidateTabClicked: typeof onTalentSearchCandidateTabClicked;
  onTalentSearchQueried: typeof onTalentSearchQueried;
}

const mapStateToProps = (state: IAppState) => ({
  user: getAccountUser(state),
});

export const TalentSearchPageContainer = connect<
  ITalentSearchPageContainerStateProps,
  ITalentSearchPageContainerDispatchProps,
  {},
  IAppState
>(mapStateToProps, {
  onTalentSearchCandidateResumeClicked,
  onTalentSearchCandidatesPageClicked,
  onTalentSearchCandidateTabClicked,
  onTalentSearchQueried,
})(TalentSearchPage);
