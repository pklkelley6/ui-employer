import React from 'react';

export const SurveyPage: React.FunctionComponent = () => {
  return (
    <iframe
      style={{height: '2100px', overflowY: 'hidden'}}
      scrolling="no"
      src="https://form.gov.sg/5f7136000fffeb0011d8f5df"
      width="100%"
      frameBorder="0"
      sandbox="allow-scripts allow-same-origin"
    />
  );
};
