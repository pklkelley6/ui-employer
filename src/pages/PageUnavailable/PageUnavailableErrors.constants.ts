export const ERROR_MSG_404 = "We can't find the page you're looking for. Please check your URL.";
export const ERROR_MSG_403 = 'You are unable to view this page.';
export const PAGE_UNAVAILABLE = 'Page Unavailable';
