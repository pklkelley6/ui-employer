import React, {useEffect} from 'react';
import styles from './PageUnavailable.scss';
import {HomepageLink} from './HomepageLink';
import {ERROR_MSG_403, PAGE_UNAVAILABLE} from './PageUnavailableErrors.constants';

export interface IPageUnavailableProps {
  subtitle?: string;
}

export const PageUnavailable: React.FunctionComponent<IPageUnavailableProps> = ({subtitle = ERROR_MSG_403}) => {
  useEffect(() => {
    document.title = `${PAGE_UNAVAILABLE} | MyCareersFuture Employer`;
  }, []);

  return (
    <div id="page-unavailable" className="flex-auto flex flex-column content-center center tc pa3 pt5">
      <i className={`${styles.robotUnavailable}`} />
      <h1 className="fw4 primary ma0 lh-title primary f3 mt4 pb2" id="subtitle">
        {subtitle}
      </h1>
      <HomepageLink linkText="Go back to homepage" />
    </div>
  );
};
