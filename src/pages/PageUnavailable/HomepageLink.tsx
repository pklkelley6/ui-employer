import React from 'react';
import {Link} from 'react-router-dom';

interface IHomepageLinkProps {
  linkText: string;
}

export const HomepageLink: React.FunctionComponent<IHomepageLinkProps> = ({linkText}) => {
  return (
    <Link to="/" className="f4 underline blue" data-cy="homepage-link">
      {linkText}
    </Link>
  );
};
