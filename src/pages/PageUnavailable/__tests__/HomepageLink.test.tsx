import {shallow} from 'enzyme';
import React from 'react';
import {HomepageLink} from '../HomepageLink';

describe('Pages/HomepageLink', () => {
  const props = {
    linkText: 'Go back to homepage',
  };

  it('renders correctly', () => {
    const wrapper = shallow(<HomepageLink {...props} />);
    expect(wrapper.text()).toBe('Go back to homepage');
  });
});
