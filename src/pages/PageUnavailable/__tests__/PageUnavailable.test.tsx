import {shallow} from 'enzyme';
import React from 'react';
import {IPageUnavailableProps, PageUnavailable} from '../PageUnavailable';
import {HomepageLink} from '../HomepageLink';
import {ERROR_MSG_403} from '../PageUnavailableErrors.constants';

describe('Pages/PageUnavailable', () => {
  const shallowMountComponent = (props?: IPageUnavailableProps) => {
    return shallow(<PageUnavailable {...props} />);
  };

  it('renders correctly', () => {
    const wrapper = shallowMountComponent();
    const component = wrapper.find('#page-unavailable');
    expect(component.length).toBe(1);
    expect(wrapper).toMatchSnapshot();
  });

  it('should contain a subtitle and homepage link', () => {
    const wrapper = shallowMountComponent();
    const wrapperSubtitle = wrapper.find('#subtitle');
    expect(wrapperSubtitle.text()).toBe(ERROR_MSG_403);
    expect(wrapper.find(HomepageLink)).toHaveLength(1);
  });

  it('should render different subtitles when necessary', () => {
    const props = {
      subtitle: 'hello error',
    };
    const wrapper = shallowMountComponent(props);
    const wrapperSubtitle = wrapper.find('#subtitle');
    expect(wrapperSubtitle.text()).toBe('hello error');
  });
});
