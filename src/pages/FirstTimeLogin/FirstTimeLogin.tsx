import {CheckBox} from '@govtechsg/mcf-mcfui';
import React from 'react';
import {Field, Form} from 'react-final-form';
import {RouteComponentProps} from 'react-router';
import styles from './FirstTimeLogin.scss';
import {AccountInfoFields} from '~/components/AccountInfo/AccountInfoFields';
import {ErrorCard} from '~/components/Layouts/ErrorCard';
import {SystemContact} from '~/graphql/__generated__/types';
import {ERROR} from '~/pages/FirstTimeLogin/FirstTimeLoginValidation';
import {isChecked} from '~/util/fieldValidations';

const tachyonStyles = {
  block: 'flex-column mb5',
  button: 'db ml2 b--primary white bg-primary',
  header: 'dark-pink f6 fw6 ma0',
};

const renderFirstTimeDisclaimer = () => (
  <div data-cy="first-time-login-disclaimer" className="bg-light-yellow flex-column pa4 mb3 lh-copy">
    <div className="fw6">First time here ?</div>
    <div> Please take a moment to complete your account details.</div>
  </div>
);

const renderHeader = (companyName: string, id: string) => (
  <div data-cy="first-time-login-header" className={tachyonStyles.block}>
    <h5 className={tachyonStyles.header}>Organisation Information</h5>
    <p className="f3 fw6 dark-gray">
      {companyName}
      <span className="f4 silver fw4"> {id} </span>
    </p>
  </div>
);

const renderAccountDetail = () => (
  <div data-cy="first-time-login-fields" className={tachyonStyles.block}>
    <h5 className={tachyonStyles.header}>Your Account Details </h5>
    <AccountInfoFields />
    {/* <div className="mid-gray f6 fw6 pt4 mb3">For third party employers only</div>
      <div className="mb4">
        <Field name="isThirdParty">
          {({input}) => {
            return (
              <CheckBox
                label={
                  <div className="pl2 fw3 f5">
                    <div>
                      I acknowledge that I am third party employer (e.g. Employment agency, outsourced HR company,
                      centralised HR, etc). I am able to post jobs for my organisation and on behalf of other
                      organisation.
                    </div>
                  </div>
                }
                input={input}
                id="checkbox-third-party"
              />
            );
          }}
        </Field>
      </div> */}
    {/* {values.systemContact && values.systemContact.isThirdParty && (
        <Field name="licence" validate={validateLicense}>
          {({input, meta}) => {
            return (
              <div className="mw6">
                <TextInput
                  id="licence"
                  label="Employer Agency Licence Number (if applicable)"
                  placeholder="Enter Employer Agency Licence Number"
                  input={input}
                  meta={meta}
                />
              </div>
            );
          }}
        </Field>
      )} */}
  </div>
);

const renderTermsAndConditions = () => (
  <div data-cy="first-time-login-terms-and-conditions" className={tachyonStyles.block}>
    <h5 className={tachyonStyles.header}>Terms and Conditions</h5>
    <div className="mb3 mt3">
      <Field name="termsAndConditions" validate={(value: boolean) => isChecked(ERROR.TERMS_AND_CONDITIONS)(value)}>
        {({input, meta}) => {
          return (
            <div>
              <CheckBox
                label={
                  <div className="pl2 fw4">
                    I have read and accepted the{' '}
                    <a
                      href="https://employer.mycareersfuture.gov.sg/terms-of-use"
                      className="link blue underline"
                      target="_blank"
                    >
                      Terms of Use{' '}
                    </a>
                  </div>
                }
                input={input}
                id="terms-and-conditions"
              />
              {meta.error && meta.submitFailed && <span className="red f5 mv2 pb3 db">{meta.error}</span>}
            </div>
          );
        }}
      </Field>
    </div>
    <Field name="tripartite" validate={(value: boolean) => isChecked(ERROR.TRIPARTITE)(value)}>
      {({input, meta}) => {
        return (
          <div>
            <CheckBox
              label={
                <div className="pl2 lh-copy fw4">
                  I am aware that employers are required to comply with the{' '}
                  <a
                    href="https://www.tal.sg/tafep/getting-started/progressive/tripartite-standards"
                    className="link underline blue"
                    target="_blank"
                  >
                    {' '}
                    Tripartite Guideline on Fair Employment Practices{' '}
                  </a>
                  under the Fair Consideration Framework. For more information on the guidelines, please visit the
                  Tripartite Alliance for Fair and Progressive Employment Practices at{' '}
                  <a href="http://www.tafep.sg" className="link blue underline" target="_blank">
                    {/* Prettier do not accept that the href and content is the same link. This is why around {''} solves it. */}
                    {'http://www.tafep.sg'}
                  </a>
                  .
                </div>
              }
              input={input}
              id="checkbox-tripartite"
            />
            {meta.error && meta.submitFailed && <span className="red f5 mv2 db">{meta.error}</span>}
          </div>
        );
      }}
    </Field>
  </div>
);

export interface IFirstTimeLogin extends RouteComponentProps {
  company: {
    name: string;
    uen: string;
  };
  systemContactUpdated: boolean;
  termsAndConditionsAccepted: boolean;
  error: boolean;
  systemContact: {
    email: SystemContact['email'];
    contactNumber: SystemContact['contactNumber'];
    name: string;
    designation: SystemContact['designation'];
    termsAndConditions: boolean;
    tripartite: boolean;
  };
  submitForm: (values: any) => void;
}

export const renderErrorBanner = (submitFailed: boolean, serverError: boolean) =>
  submitFailed || serverError ? <ErrorCard message={submitFailed ? ERROR.INVALID_FORM : ERROR.SYSTEM_ERROR} /> : null;

export const FirstTimeLogin: React.FunctionComponent<IFirstTimeLogin> = (props) => {
  return (
    <section data-cy="first-time-login-form" className={`flex-auto-ie ${styles.formContainer}`}>
      <div className="flex flex-column pa4 mw8 center dark-gray">
        {renderFirstTimeDisclaimer()}
        <Form
          onSubmit={props.submitForm}
          initialValues={props.systemContact}
          render={({handleSubmit, submitFailed}) => (
            <form onSubmit={handleSubmit}>
              <div className="bg-black-05 mb3 pa4 pt4">
                {renderHeader(props.company.name, props.company.uen)}
                {renderAccountDetail()}
                {renderTermsAndConditions()}
              </div>
              {renderErrorBanner(submitFailed, props.error)}
              <div className="flex justify-end mt4">
                <button type="submit" className={`${styles.button}  ${tachyonStyles.button}`}>
                  Submit
                </button>
              </div>
            </form>
          )}
        />
      </div>
    </section>
  );
};
