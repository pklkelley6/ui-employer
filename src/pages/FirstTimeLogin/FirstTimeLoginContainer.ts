import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {IAppState} from '~/flux';
import {getAccountName} from '~/flux/account';
import {getCompanyName, getCompanyUen} from '~/flux/company';
import {updateSystemContactAndTermsAndConditionsRequested} from '~/flux/systemContact/systemContact.actions';
import {getSystemContact, isErrorOnSubmit} from '~/flux/systemContact/systemContact.selectors';
import {FirstTimeLogin} from '~/pages/FirstTimeLogin/FirstTimeLogin';

export const mapStateToProps = (state: IAppState) => {
  const companyName = getCompanyName(state.company, state.account);
  const companyUen = getCompanyUen(state.company);
  const systemContact = getSystemContact(state);
  const userName = getAccountName(state);
  const error = isErrorOnSubmit(state);
  return {
    company: {
      name: companyName,
      uen: companyUen,
    },
    error,
    systemContact: {
      contactNumber: systemContact.contactNumber,
      designation: systemContact.designation,
      email: systemContact.email,
      name: userName,
      termsAndConditions: !!systemContact.acceptedAt,
      tripartite: !!systemContact.acceptedAt,
    },
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    submitForm: (values: any) => {
      dispatch(updateSystemContactAndTermsAndConditionsRequested(values));
    },
  };
};

export const FirstTimeLoginContainer = connect(mapStateToProps, mapDispatchToProps)(withRouter(FirstTimeLogin));
