import {regex} from '~/util/fieldValidations';

export enum ERROR {
  INVALID_FORM = 'Please amend the highlighted fields to continue.',
  INVALID_LICENCE = 'Please check your licence number',
  SYSTEM_ERROR = 'We are temporarily unable to process your request. Please try again later.',
  TERMS_AND_CONDITIONS = 'Please acknowledge that you have read the terms of use',
  TRIPARTITE = 'Please acknowledge that you have read the guidelines',
}

// unused for the moment
export const licence = regex(ERROR.INVALID_LICENCE)(/^[a-zA-Z\d]{10}$/);
