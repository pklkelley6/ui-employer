import React from 'react';
import {MimeType} from 'file-type';
import FileType from 'file-type/browser';
import styles from './CompanyProfile.scss';
import {ILogotypePreview} from './CompanyProfile';
import {formatFileSize} from '~/util/files';

interface ILogoUploadedProps {
  input: {
    value: ILogotypePreview;
    onChange: (value: ILogotypePreview) => void;
    onBlur: () => void;
  };
  imageAlt: string;
  meta?: {
    error?: string;
    touched?: boolean;
  };
}

const RECOMMENDED_LOGO_WIDTH_HIEGHT = '100px';
const ACCEPTABLE_LOGO_FORMAT: MimeType[] = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp'];

export const LogoUploaded: React.FunctionComponent<ILogoUploadedProps> = ({input, imageAlt, meta}) => {
  const {onBlur, onChange, value} = input;

  const getLogotypePreview = async (files: FileList | null): Promise<ILogotypePreview | undefined> => {
    if (files) {
      for (const file of Array.from(files)) {
        const fileType = await FileType.fromBlob(file);
        return {
          src: URL.createObjectURL(file),
          name: file.name,
          size: file.size,
          validImageFormat: !!fileType && ACCEPTABLE_LOGO_FORMAT.includes(fileType?.mime),
        };
      }
    }
  };

  const revokePreviousLogoPreviewObjectURL = () => {
    if (/^blob:.+/.test(value.src)) {
      URL.revokeObjectURL(value.src);
    }
  };

  const updateLogotypePreview = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const logotypePreview = await getLogotypePreview(event.target.files);
    if (logotypePreview) {
      onBlur();
      revokePreviousLogoPreviewObjectURL();
      onChange(logotypePreview);
    }
  };

  const onImageDisplayError = () => {
    if (value.validImageFormat) {
      onChange({...value, validImageFormat: false});
    }
  };

  return (
    <>
      <div className="flex">
        <>
          {value.src && !meta?.error ? (
            <div className="ba b--solid mr3 h4 w4 dib bg-white flex items-center justify-center pa2 b--black-30">
              <img
                src={value.src}
                className="f6 black-70"
                alt={imageAlt}
                width={RECOMMENDED_LOGO_WIDTH_HIEGHT}
                height={RECOMMENDED_LOGO_WIDTH_HIEGHT}
                onError={onImageDisplayError}
              />
            </div>
          ) : (
            <div
              className={`ba b--dashed mr3 h4 w4 dib tc flex items-center br1 ${
                meta?.error && meta.touched ? 'b--red bg-washed-red' : 'b--black-20 bg-white-70'
              }`}
            >
              <label htmlFor="companyLogotypeUpload" className="f6 fw6 black-60 tc w-100 pa3 lh-copy">
                No image uploaded
              </label>
            </div>
          )}
        </>
        <div>
          {value.name && value.size ? (
            <small className="f6 db mb3 black-70">
              {value.name} ({formatFileSize(value.size)})
            </small>
          ) : null}
          <label
            htmlFor="companyLogotypeUpload"
            className={`${styles.logotypeUpload} pa2 dib ba b--primary primary bg-white br2`}
          >
            Upload image
          </label>
          <input
            className="o-0"
            id="companyLogotypeUpload"
            type="file"
            accept={ACCEPTABLE_LOGO_FORMAT.join(', ')}
            onChange={updateLogotypePreview}
          />

          <div className="black-80 f6 mt3 lh-copy">
            <p className="ma0">Acceptable formats: JPG, JPEG, BMP, GIF, PNG only</p>
            <p className="ma0">Maximum file size: 100 kB only</p>
            <p className="ma0">Recommended dimensions: 100 px by 100 px</p>
          </div>
        </div>
      </div>
      {meta?.error && meta.touched && (
        <span data-cy="logouploader-error" className="red">
          {meta.error}
        </span>
      )}
    </>
  );
};
