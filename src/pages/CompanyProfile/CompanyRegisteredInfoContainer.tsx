import {connect} from 'react-redux';
import {CompanyRegisteredInfo, ICompanyRegisteredInfoProps} from './CompanyRegisteredInfo';
import {getCompanyName, getCompanyUen, getCompanyOperatingAddress, getCompanySsic} from '~/flux/company';
import {IAppState} from '~/flux';

export const mapStateToProps = ({company, account}: IAppState) => ({
  name: getCompanyName(company, account),
  uen: getCompanyUen(company),
  address: getCompanyOperatingAddress(company),
  ssic: getCompanySsic(company),
});

export const CompanyRegisteredInfoContainer = connect<ICompanyRegisteredInfoProps, {}, {}, IAppState>(
  mapStateToProps,
  {},
)(CompanyRegisteredInfo);
