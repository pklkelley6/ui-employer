import {Interaction, Matchers, ApolloGraphQLInteraction} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {ApolloProvider} from 'react-apollo';
import toJson from 'enzyme-to-json';
import {print} from 'graphql';
import {CompanyProfileContainer} from '../CompanyProfileContainer';
import {CompanyRegisteredInfo} from '../CompanyRegisteredInfo';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {companyInfoMock, companyAddressMock} from '~/__mocks__/company/company.mocks';
import {PactBuilder} from '~/__mocks__/pact';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';
import {API_JOB_VERSION} from '~/services/util/getApiJobRequestInit';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

let jobPactBuilder: PactBuilder;

describe('CompanyProfile', () => {
  const store = configureStore()({
    account: {
      data: uenUserMock,
    },
    company: {
      companyInfo: {
        ...companyInfoMock,
        addresses: [companyAddressMock],
      },
    },
  });

  let profilePactBuilder: PactBuilder;

  beforeAll(async () => {
    jobPactBuilder = new PactBuilder('api-job');
    await jobPactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${jobPactBuilder.host}:${jobPactBuilder.port}/v2`);

    profilePactBuilder = new PactBuilder('api-profile');
    await profilePactBuilder.setup();
  });

  afterAll(async () => {
    await jobPactBuilder.provider.finalize();
    await profilePactBuilder.provider.finalize();
  });

  beforeEach(async () => {
    const getSsicQueryMockResult = {
      data: {
        common: {
          ssicList: Matchers.eachLike({
            code: companyInfoMock.ssicCode,
            description: 'Academic tutoring services (eg tuition centres, private tutoring services)',
          }),
        },
      },
    };
    const getSsicQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSICList with single code')
      .withQuery(print(GET_SSIC_LIST))
      .withVariables({code: companyInfoMock.ssicCode})
      .withOperation('getCommonSsic')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: getSsicQueryMockResult,
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(getSsicQuery);
  });

  it('should display the company registered info', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ApolloProvider client={profilePactBuilder.getApolloClient()}>
            <CompanyProfileContainer />
          </ApolloProvider>
        </MemoryRouter>
      </Provider>,
    );
    await act(async () => {
      await profilePactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    const companyRegisteredInfo = wrapper.find(CompanyRegisteredInfo);
    expect(toJson(companyRegisteredInfo, cleanSnapshot())).toMatchSnapshot();
  });

  it('should call PATCH /companies/{uen} api when saved button is clicked from the modal', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ApolloProvider client={profilePactBuilder.getApolloClient()}>
            <CompanyProfileContainer />
          </ApolloProvider>
        </MemoryRouter>
      </Provider>,
    );

    await act(async () => {
      await profilePactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    await addCompanyProfilePatchInteraction();

    wrapper.find('[data-cy="company-profile-save"]').simulate('submit');

    await act(async () => {
      await jobPactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('[data-cy="update-company-profile-modal"]')).toHaveLength(1);
  });
});

const addCompanyProfilePatchInteraction = async () => {
  const {addresses, ...companyResponse} = companyInfoMock;
  const companyUEN = 'T08GB0046G';
  const path = `/v2/companies/${companyUEN}`;

  const interaction = new Interaction()
    .uponReceiving(`a PATCH request for ${path}`)
    .withRequest({
      body: {
        description: companyInfoMock.description,
      },
      headers: {
        'Content-Type': 'application/json',
        [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
      },
      method: 'PATCH',
      path,
    })
    .willRespondWith({
      body: Matchers.like(companyResponse),
      status: 200,
    });

  await jobPactBuilder.provider.addInteraction(interaction);
};
