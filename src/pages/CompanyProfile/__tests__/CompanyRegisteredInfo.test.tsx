import {Matchers, ApolloGraphQLInteraction} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {ApolloProvider} from 'react-apollo';
import {print} from 'graphql';
import {CompanyRegisteredInfo, ICompanyRegisteredInfoProps} from '../CompanyRegisteredInfo';
import {companyInfoMock, companyAddressMock} from '~/__mocks__/company/company.mocks';
import {PactBuilder} from '~/__mocks__/pact';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

describe('CompanyRegisteredInfo', () => {
  const props: ICompanyRegisteredInfoProps = {
    name: companyInfoMock.name,
    uen: companyInfoMock.uen,
    ssic: companyInfoMock.ssicCode,
    address: companyAddressMock,
  };

  let profilePactBuilder: PactBuilder;
  beforeAll(async () => {
    profilePactBuilder = new PactBuilder('api-profile');
    await profilePactBuilder.setup();
  });

  afterAll(async () => {
    await profilePactBuilder.provider.finalize();
  });

  it('should display the company registered info', async () => {
    const getSsicQueryMockResult = {
      data: {
        common: {
          ssicList: Matchers.eachLike({
            code: props.ssic,
            description: 'Academic tutoring services (eg tuition centres, private tutoring services)',
          }),
        },
      },
    };
    const getSsicQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSICList with single code')
      .withQuery(print(GET_SSIC_LIST))
      .withVariables({code: props.ssic})
      .withOperation('getCommonSsic')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: getSsicQueryMockResult,
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(getSsicQuery);

    const wrapper = mount(
      <ApolloProvider client={profilePactBuilder.getApolloClient()}>
        <CompanyRegisteredInfo {...props} />
      </ApolloProvider>,
    );
    await act(async () => {
      await profilePactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('[data-cy="name"]').text()).toEqual(props.name);
    expect(wrapper.find('[data-cy="uen"] p').text()).toEqual(props.uen);
    expect(wrapper.find('[data-cy="industry"] p').text()).toEqual(
      `${props.ssic} - Academic tutoring services (eg tuition centres, private tutoring services)`,
    );
    expect(wrapper.find('[data-cy="address"] p').text()).toEqual(
      'RAFFLES CITY TOWER, 100 NORTH BRIDGE ROAD #01-02 520858',
    );
  });

  it('should display only the ssic code without description when not found', async () => {
    const propsWithNotFoundSsic = {...props, ssic: 'not-found'};
    const getSsicQueryMockResult = {
      data: {
        common: {
          ssicList: [],
        },
      },
    };
    const getSsicQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSICList with invalid ssic code')
      .withQuery(print(GET_SSIC_LIST))
      .withVariables({code: propsWithNotFoundSsic.ssic})
      .withOperation('getCommonSsic')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: getSsicQueryMockResult,
        status: 200,
      });
    await profilePactBuilder.provider.addInteraction(getSsicQuery);

    const wrapper = mount(
      <ApolloProvider client={profilePactBuilder.getApolloClient()}>
        <CompanyRegisteredInfo {...propsWithNotFoundSsic} />
      </ApolloProvider>,
    );
    await act(async () => {
      await profilePactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('[data-cy="industry"] p').text()).toEqual(propsWithNotFoundSsic.ssic);
  });

  it('should display dash(-) for value when ssic/address not specified', async () => {
    const propsWithoutOptionalProperties = {...props, ssic: undefined, address: undefined};

    const wrapper = mount(
      <ApolloProvider client={profilePactBuilder.getApolloClient()}>
        <CompanyRegisteredInfo {...propsWithoutOptionalProperties} />
      </ApolloProvider>,
    );

    expect(wrapper.find('[data-cy="industry"] p').text()).toEqual('-');
    expect(wrapper.find('[data-cy="address"] p').text()).toEqual('-');
  });
});
