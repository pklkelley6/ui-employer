import {TextInput} from '@govtechsg/mcf-mcfui';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {FORM_ERROR} from 'final-form';
import {Field, Form} from 'react-final-form';
import {stateFromHTML} from 'draft-js-import-html';
import {ActionType} from 'typesafe-actions';
import styles from './CompanyProfile.scss';
import {LogoUploaded} from './LogoUploader';
import {CompanyRegisteredInfoContainer} from './CompanyRegisteredInfoContainer';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {TextEditor} from '~/components/JobPosting/TextEditor';
import {Modal} from '~/components/Core/Modal';
import {HeaderContainer} from '~/components/Navigation/Header';
import {updateCompanyProfile} from '~/services/employer/updateCompanyProfile';
import {StaticNudge} from '~/pages/JobPosting/StaticNudge';
import {convertDataToBase64} from '~/util/convertDataToBase64';
import {getBlobObject} from '~/util/getBlobObject';
import {
  composeValidators,
  required,
  isChecked,
  lessThan,
  defaultRequired,
  maxLength,
  minLength,
  greaterThan,
} from '~/util/fieldValidations';
import {FormLoader} from '~/components/Layouts/FormLoader';
import CancelButtonWithConfirmation from '~/components/CompanyProfile/CancelButtonWithConfirmation';
import {scanForVirus, IScanForVirusResult} from '~/services/scanForVirus/scanForVirus';
import {parseJwt} from '~/util/parseJwt';

import {companyUpdated, companyFetchRequested} from '~/flux/company';
import {ICompanyInfo} from '~/services/employer/company';
interface ICompanyProfileProps {
  companyLogotypeSrc: string;
  companyName: string;
  companyUen: string;
  companyDescription: string;
  loading: boolean;
  website?: string;
  numberOfEmployees?: number;
  companyUpdated: (companyInfo: ICompanyInfo) => ActionType<typeof companyUpdated>;
  companyFetchRequested: () => ActionType<typeof companyFetchRequested>;
}

export interface ILogotypePreview {
  src: string;
  name?: string;
  size?: number;
  validImageFormat: boolean;
}

export interface ICompanyFormState {
  description: string;
  website?: string;
  numberOfEmployees?: number;
  logotype: ILogotypePreview;
}

const getValueFromHTML = (value: any) => stateFromHTML(value || '').getPlainText();

export const CompanyProfile: React.FunctionComponent<ICompanyProfileProps> = ({
  companyLogotypeSrc,
  companyName,
  companyUen,
  companyDescription,
  website,
  numberOfEmployees,
  loading,
  companyUpdated,
  companyFetchRequested,
}) => {
  const title = 'Company Profile';

  useEffect(() => {
    document.title = 'Company Profile | MyCareersFuture Employer';

    companyFetchRequested();
  }, []);

  const [virusScanResult, setVirusScanResult] = useState<string>();

  const displaySuccessConfirmation = () => {
    return (
      <Modal>
        <div data-cy="update-company-profile-modal" className={`pa4 f4 black-80 bg-white ${styles.modal}`}>
          <p>Company Profile Saved</p>
          <p>You have successfully saved your company profile</p>
          <div className="flex justify-end pt3">
            <Link
              to="/jobs"
              data-cy="ok-button-redirect-company-profile"
              title="All Jobs"
              className={`pa3 db ml3 tc b--primary white bg-primary no-underline ${styles.redirectSuccessButton}`}
            >
              Ok
            </Link>
          </div>
        </div>
      </Modal>
    );
  };

  const onSubmit = async (values: ICompanyFormState) => {
    const {logotype, description, website, numberOfEmployees} = values;
    try {
      const blob = await getBlobObject(logotype.src);
      const logotypeBase64 = blob && (await convertDataToBase64(blob));
      const companyProfile = await updateCompanyProfile(companyUen, {
        description,
        companyUrl: website,
        employeeCount: numberOfEmployees ? Number(numberOfEmployees) : undefined,
        logoBase64: logotypeBase64,
        virusScanResult,
      });
      companyUpdated(companyProfile);
    } catch (error) {
      growlNotification('Temporarily unable to save company profile. Please try again.', GrowlType.ERROR, {
        toastId: 'saveCompanyProfileError',
      });
      if (error instanceof Error) {
        return {[FORM_ERROR]: error.message};
      }
    }
  };

  const isVirusFree = async (binaryFile: string) => {
    const token = await scanForVirus(binaryFile);
    const {isInfected}: IScanForVirusResult = parseJwt(token);
    return {
      token,
      isInfected,
    };
  };

  return (
    <section className="flex-auto-ie">
      <HeaderContainer data-cy="company-profile-header" crumbs={[title]} />
      <main className={`flex pa3 ${styles.formContainer}`}>
        {loading ? (
          <FormLoader className="flex-auto o-50" cardNumber={2} />
        ) : (
          <Form
            keepDirtyOnReinitialize
            onSubmit={onSubmit}
            initialValues={{
              description: companyDescription,
              logotype: {
                src: companyLogotypeSrc,
                validImageFormat: true,
              },
              website,
              numberOfEmployees,
            }}
            render={({handleSubmit, submitSucceeded}) => {
              const DESCRIPTION_MIN_LENGTH = 100;
              const DESCRIPTION_MAX_LENGTH = 4000;
              const WEBSITE_MAX_LENGTH = 2000;
              return (
                <section className="flex-auto db">
                  <form onSubmit={handleSubmit}>
                    <div className={`bg-black-05 mb3 pa4 pt4 ${styles.formBase}`}>
                      <CompanyRegisteredInfoContainer />
                      <div className="b--black-10 pt4 bb mh2" />
                      <div className="pa2 pt4 w-100" data-cy="company-profile-logotype">
                        <label className="mb2 f5 fw6 black-60 db pb2">Company Logo</label>
                        <Field<ILogotypePreview>
                          name="logotype"
                          validate={composeValidators(
                            (value) => required('Please upload your company logo to continue')(value.src),
                            (value) =>
                              isChecked("We can't seem to read this file - please try uploading a different file")(
                                value.validImageFormat,
                              ),
                            (value) =>
                              lessThan(
                                100 * 1024,
                                'File exceeds 100kb - please try uploading a smaller file',
                              )(value.size),
                            async (value, _, meta) => {
                              if (meta?.touched) {
                                try {
                                  const blob = await getBlobObject(value.src);
                                  const fileBinaryString = blob && (await convertDataToBase64(blob));
                                  if (fileBinaryString) {
                                    const {token, isInfected} = await isVirusFree(fileBinaryString);
                                    if (isInfected) {
                                      return 'Potential virus detected - please try uploading a different file';
                                    } else {
                                      setVirusScanResult(token);
                                    }
                                  } else {
                                    throw new Error();
                                  }
                                } catch (error) {
                                  return "We can't seem to read this file - please try uploading a different file";
                                }
                              }
                            },
                          )}
                        >
                          {({input, meta}) => {
                            return <LogoUploaded input={input} imageAlt={companyName} meta={meta} />;
                          }}
                        </Field>
                      </div>
                      <div className="pa2 pt4 w-100" data-cy="company-profile-description">
                        <Field
                          name="description"
                          validate={(value: any) =>
                            composeValidators(
                              defaultRequired,
                              maxLength(
                                DESCRIPTION_MAX_LENGTH,
                                `Please keep within ${DESCRIPTION_MAX_LENGTH.toLocaleString()} characters`,
                              ),
                              minLength(
                                DESCRIPTION_MIN_LENGTH,
                                `Please enter at least ${DESCRIPTION_MIN_LENGTH.toLocaleString()} characters`,
                              ),
                            )(getValueFromHTML(value))
                          }
                        >
                          {({input, meta}) => {
                            return (
                              <TextEditor
                                input={input}
                                label="Company Description"
                                maxLength={DESCRIPTION_MAX_LENGTH}
                                meta={meta}
                                showToolbar={false}
                              />
                            );
                          }}
                        </Field>
                      </div>

                      <div className="flex">
                        <div className="pa2 w-50" data-cy="company-profile-number-of-employees">
                          <Field name="numberOfEmployees" allowNull={true} validate={greaterThan(0)}>
                            {({input, meta}) => {
                              return (
                                <TextInput
                                  id="number-of-employees"
                                  label="Number of Employees (optional)"
                                  placeholder="Enter number of employees"
                                  input={input}
                                  meta={meta}
                                  type="number"
                                />
                              );
                            }}
                          </Field>
                        </div>
                        <div className="pa2 w-50" data-cy="company-profile-website">
                          <Field
                            name="website"
                            validate={maxLength(
                              WEBSITE_MAX_LENGTH,
                              `Please keep within ${WEBSITE_MAX_LENGTH.toLocaleString()} characters`,
                            )}
                          >
                            {({input, meta}) => {
                              return (
                                <TextInput
                                  id="company-website"
                                  label="Company Website (optional)"
                                  placeholder="Enter company website URL"
                                  input={input}
                                  meta={meta}
                                />
                              );
                            }}
                          </Field>
                        </div>
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <CancelButtonWithConfirmation />
                      <button
                        data-cy="company-profile-save"
                        className={`${styles.button} pa3 db ml3 b--primary white bg-primary`}
                        type="submit"
                      >
                        Save
                      </button>
                    </div>
                    {submitSucceeded ? displaySuccessConfirmation() : null}
                  </form>
                </section>
              );
            }}
          />
        )}
        <aside className={`pl3 ${styles.asideBar}`}>
          <StaticNudge />
        </aside>
      </main>
    </section>
  );
};
