import React from 'react';

export const RefetchApplicationsWithStatusCountByJobContext = React.createContext<(() => Promise<any>) | undefined>(
  undefined,
);
