import {flowRight as compose} from 'lodash';
import {graphql} from 'react-apollo';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router-dom';
import {Candidates} from './Candidates';
import {IAppState} from '~/flux/index';
import {jobPostingFetchRequested} from '~/flux/jobPosting/jobPosting.actions';
import {getAccountUen} from '~/flux/account/';
import {
  GetApplicationsCountQuery,
  GetApplicationsCountQueryVariables,
  GetSuggestedTalentsCountQuery,
  GetSuggestedTalentsCountQueryVariables,
  GetBookmarkedCandidatesCountQuery,
  GetBookmarkedCandidatesCountQueryVariables,
  GetApplicationsWithStatusCountByJobQuery,
  GetApplicationsWithStatusCountByJobQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_APPLICATIONS_COUNT, GET_APPLICATIONS_WITH_STATUS_COUNT_BY_JOB} from '~/graphql/applications';
import {GET_SUGGESTED_TALENTS_COUNT} from '~/graphql/suggestedTalents';
import {IJobPost} from '~/services/employer/jobs.types';
import {pathToJobUuid} from '~/util/url';
import {GET_BOOKMARKED_CANDIDATES_COUNT} from '~/graphql/candidates/bookmarkedCandidates.query';
import {isSuggestedTalentUnavailable, formatJob} from '~/util/jobPosts';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

export interface ICandidatesContainerStateProps {
  fromLastLogin: string;
  jobUuid: string;
  threshold: number;
  job?: IJobPost;
  jobPostNotFound: boolean;
  x0paSuggestedTalentsFeatureToggle: boolean;
  newErrorPageFeatureToggle: boolean;
  validAccount: boolean;
}

export interface ICandidatesContainerDispatchProps {
  jobPostingFetchRequested: (uuid: string) => any;
}

export interface IApplicationsCountQueryProps {
  applicationsTotal: number;
}

export interface ISuggestedTalentsCountQueryProps {
  suggestedTalentsTotal?: number;
}

export interface IBookmarkedCandidatesCountQueryProps {
  bookmarkedCandidatesTotal?: number;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export interface IGetApplicationsWithStatusCountByJobQueryProps {
  applicationsWithStatusCountByJob?: any;
  refetchApplicationsWithStatusCountByJob?: () => Promise<any>;
}

export type ICandidatesContainerReduxProps = ICandidatesContainerStateProps & ICandidatesContainerDispatchProps;

export type ICandidatesProps = ICandidatesContainerReduxProps &
  IApplicationsCountQueryProps &
  ISuggestedTalentsCountQueryProps &
  IBookmarkedCandidatesCountQueryProps &
  IGetApplicationsWithStatusCountByJobQueryProps &
  RouteComponentProps<any>;

export const mapStateToProps = (
  state: IAppState,
  {match}: RouteComponentProps<any>,
): ICandidatesContainerStateProps => {
  const titleIdParam = match.params.titleId;
  const jobUuid = pathToJobUuid(titleIdParam);
  const posting = state.jobPosting.jobPostings[jobUuid]?.jobPost;

  const accountUen = state.account ? getAccountUen(state) : '';
  const validAccount = accountUen === posting?.postedCompany.uen;
  const jobPostNotFound = state.jobPosting.jobPostings[jobUuid]?.fetchStatus === 'JOB_POSTING_FETCH_FAILED';

  return {
    fromLastLogin: state.suggestedTalents.fromLastLogin,
    job: posting,
    jobPostNotFound,
    jobUuid,
    threshold: state.suggestedTalents.threshold,
    validAccount,
    x0paSuggestedTalentsFeatureToggle: state.features.x0paSuggestedTalents,
    newErrorPageFeatureToggle: state.features.newErrorPage,
  };
};

export const applicationsCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetApplicationsCountQuery,
  GetApplicationsCountQueryVariables,
  ICandidatesContainerReduxProps & IApplicationsCountQueryProps
>(GET_APPLICATIONS_COUNT, {
  options: ({jobUuid}) => ({
    variables: {
      jobId: jobUuid,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    applicationsTotal: data?.applicationsForJob?.total ?? 0,
  }),
});

export const suggestedTalentsCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetSuggestedTalentsCountQuery,
  GetSuggestedTalentsCountQueryVariables,
  ICandidatesContainerReduxProps & ISuggestedTalentsCountQueryProps
>(GET_SUGGESTED_TALENTS_COUNT, {
  options: ({fromLastLogin, jobUuid, threshold, x0paSuggestedTalentsFeatureToggle}) => ({
    variables: {
      fromLastLogin,
      jobId: jobUuid,
      threshold,
    },
    context: {
      headers: x0paSuggestedTalentsFeatureToggle ? {} : {[API_VERSION_HEADER_KEY]: '2021-05-24'},
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    suggestedTalentsTotal: data?.suggestedTalentsForJob?.total,
  }),
  skip: ({job}) => !job || isSuggestedTalentUnavailable(formatJob(job)),
});

export const bookmarkedCandidatesCountQuery = graphql<
  ICandidatesContainerReduxProps,
  GetBookmarkedCandidatesCountQuery,
  GetBookmarkedCandidatesCountQueryVariables,
  ICandidatesContainerReduxProps & IBookmarkedCandidatesCountQueryProps
>(GET_BOOKMARKED_CANDIDATES_COUNT, {
  options: ({jobUuid}) => ({
    variables: {
      jobId: jobUuid,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    bookmarkedCandidatesTotal: data?.bookmarkedCandidatesForJob?.total,
    refetchBookmarkedCandidatesTotal: data?.refetch,
  }),
});

export const applicationsByJobAndStatus = graphql<
  ICandidatesContainerReduxProps,
  GetApplicationsWithStatusCountByJobQuery,
  GetApplicationsWithStatusCountByJobQueryVariables,
  ICandidatesContainerReduxProps & IGetApplicationsWithStatusCountByJobQueryProps
>(GET_APPLICATIONS_WITH_STATUS_COUNT_BY_JOB, {
  options: ({jobUuid}) => ({
    variables: {
      jobId: jobUuid,
    },
  }),
  props: ({data, ownProps}) => ({
    ...ownProps,
    applicationsWithStatusCountByJob: data?.applicationsWithStatusCountByJob,
    refetchApplicationsWithStatusCountByJob: data?.refetch,
  }),
});

export const CandidatesContainer = compose([
  connect<ICandidatesContainerStateProps, ICandidatesContainerDispatchProps, RouteComponentProps<any>, IAppState>(
    mapStateToProps,
    {jobPostingFetchRequested},
  ),
  applicationsCountQuery,
  suggestedTalentsCountQuery,
  bookmarkedCandidatesCountQuery,
  applicationsByJobAndStatus,
])(Candidates);
