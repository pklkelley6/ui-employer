import {map, compact} from 'lodash/fp';
import React from 'react';
import {PageUnavailable} from '../PageUnavailable/PageUnavailable';
import {PAGE_UNAVAILABLE} from '../PageUnavailable/PageUnavailableErrors.constants';
import {ApplicationsContainer} from '~/components/Applications/ApplicationsContainer';
import {BookmarkedCandidatesContainer} from '~/components/BookmarkedCandidates/BookmarkedCandidatesContainer';
import {JobBar} from '~/components/JobBar/JobBar';
import JobPostMenu from '~/components/Jobs/JobPostMenu';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {HeaderContainer} from '~/components/Navigation/Header';
import {Tab, Tabs} from '~/components/Navigation/Tabs';
import {OnboardingProviderWithUser} from '~/components/Onboarding/OnboardingContext';
import {SuggestedTalentsContainer} from '~/components/SuggestedTalents/SuggestedTalentsContainer';
import {SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';
import {ICandidatesProps} from '~/pages/Candidates/CandidatesContainer';
import {formatJob, isSuggestedTalentUnavailable} from '~/util/jobPosts';
import {RedirectUnauthorizedJob} from '~/components/Jobs/RedirectUnauthorizedJob';
import {UpdateRemainingApplications} from '~/components/UpdateRemainingApplications/UpdateRemainingApplications';
import {RefetchApplicationsWithStatusCountByJobContext} from '~/pages/Candidates/RefetchApplicationsByJobAndStatusContext';
import {JobStatusCodes} from '~/services/employer/jobs.types';

interface ITabProp {
  tabId: CandidatesTabs;
  title: string;
  total?: number;
  children: React.ReactNode;
}

export class Candidates extends React.Component<ICandidatesProps> {
  constructor(props: ICandidatesProps) {
    super(props);
    this.handleTabSelect = this.handleTabSelect.bind(this);
  }

  public handleTabSelect(index: CandidatesTabs) {
    const {history, match} = this.props;
    history.push(`/jobs/${match.params.titleId}/${index}`);
  }

  public componentDidMount() {
    this.props.jobPostingFetchRequested(this.props.jobUuid);
  }

  public componentDidUpdate() {
    if (this.props.job) {
      const formattedJob = formatJob(this.props.job);
      const titlePrefix =
        this.props.newErrorPageFeatureToggle && !this.props.validAccount
          ? PAGE_UNAVAILABLE
          : `${formattedJob.title} - ${formattedJob.employerName}`;
      document.title = `${titlePrefix} | MyCareersFuture Employer`;
    }
  }

  public render() {
    const {
      job,
      jobUuid,
      jobPostNotFound,
      applicationsTotal,
      suggestedTalentsTotal,
      bookmarkedCandidatesTotal,
      refetchBookmarkedCandidatesTotal,
      match,
      validAccount,
      applicationsWithStatusCountByJob,
      refetchApplicationsWithStatusCountByJob,
      newErrorPageFeatureToggle,
    } = this.props;
    const formattedJob = job && formatJob(job);
    const title = formattedJob ? formattedJob.title : 'Unknown Job';
    const suggestedTalentsTotalWithLimit =
      suggestedTalentsTotal && suggestedTalentsTotal > SUGGESTED_TALENTS_TOTAL_LIMIT
        ? SUGGESTED_TALENTS_TOTAL_LIMIT
        : suggestedTalentsTotal;
    const displayTabs = map(
      (tabProp: ITabProp) => <Tab key={tabProp.title} {...tabProp} />,
      compact([
        {
          tabId: CandidatesTabs.Applications,
          title: 'Applicants',
          total: applicationsTotal,
          children: (
            <RefetchApplicationsWithStatusCountByJobContext.Provider value={refetchApplicationsWithStatusCountByJob}>
              <ApplicationsContainer
                job={job}
                jobUuid={jobUuid}
                refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
              />
            </RefetchApplicationsWithStatusCountByJobContext.Provider>
          ),
        },
        {
          tabId: CandidatesTabs.SuggestedTalents,
          title: 'Suggested Talents',
          total: suggestedTalentsTotalWithLimit,
          children: (
            <SuggestedTalentsContainer
              jobUuid={jobUuid}
              shouldDisplayContactButton={job?.status.id !== JobStatusCodes.Closed}
              unavailableSuggestedTalents={!!formattedJob && isSuggestedTalentUnavailable(formattedJob)}
              refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
            />
          ),
        },
        {
          tabId: CandidatesTabs.Saved,
          title: 'Saved',
          total: bookmarkedCandidatesTotal,
          children: (
            <RefetchApplicationsWithStatusCountByJobContext.Provider value={refetchApplicationsWithStatusCountByJob}>
              <BookmarkedCandidatesContainer
                job={job}
                jobUuid={jobUuid}
                shouldDisplayContactButton={job?.status.id !== JobStatusCodes.Closed}
                refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
              />
            </RefetchApplicationsWithStatusCountByJobContext.Provider>
          ),
        },
      ]),
    );

    return (
      <section className="flex-auto-ie">
        {newErrorPageFeatureToggle ? (
          <>
            {validAccount ? <HeaderContainer crumbs={['All Jobs', title]} /> : null}
            {job ? (
              <RedirectUnauthorizedJob job={job}>
                <div className="flex">
                  <JobBar job={job} />
                  <JobPostMenu job={job} />
                </div>
                <UpdateRemainingApplications
                  jobId={job.uuid}
                  vacancies={job.numberOfVacancies}
                  applicationsByJobAndStatus={applicationsWithStatusCountByJob}
                  applicationsTotal={applicationsTotal}
                  onUpdateCompleted={async () => {
                    if (refetchApplicationsWithStatusCountByJob) {
                      await refetchApplicationsWithStatusCountByJob();
                    }
                  }}
                />
                <OnboardingProviderWithUser>
                  <Tabs selectedTab={match.params.tab} onTabSelect={this.handleTabSelect}>
                    {displayTabs}
                  </Tabs>
                </OnboardingProviderWithUser>
              </RedirectUnauthorizedJob>
            ) : !job && jobPostNotFound ? (
              <PageUnavailable />
            ) : (
              <CardLoader className="flex w-100" cardNumber={3} />
            )}
          </>
        ) : (
          <>
            <HeaderContainer crumbs={['All Jobs', title]} />
            <div className="flex">
              {job ? (
                <RedirectUnauthorizedJob job={job}>
                  <JobBar job={job} />
                  <JobPostMenu job={job} />
                </RedirectUnauthorizedJob>
              ) : (
                <CardLoader className="flex w-100" cardNumber={3} />
              )}
            </div>
            {job ? (
              <UpdateRemainingApplications
                jobId={job.uuid}
                vacancies={job.numberOfVacancies}
                applicationsByJobAndStatus={applicationsWithStatusCountByJob}
                applicationsTotal={applicationsTotal}
                onUpdateCompleted={async () => {
                  if (refetchApplicationsWithStatusCountByJob) {
                    await refetchApplicationsWithStatusCountByJob();
                  }
                }}
              />
            ) : null}
            <OnboardingProviderWithUser>
              <Tabs selectedTab={match.params.tab} onTabSelect={this.handleTabSelect}>
                {displayTabs}
              </Tabs>
            </OnboardingProviderWithUser>
          </>
        )}
      </section>
    );
  }
}
