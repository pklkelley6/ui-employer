import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {format, subYears, subMonths} from 'date-fns';
import {Candidates} from '../Candidates';
import {CandidatesTabs} from '../Candidates.constants';
import {ICandidatesProps} from '../CandidatesContainer';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {Tab, Tabs} from '~/components/Navigation/Tabs';
import {SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {SuggestedTalentsContainer} from '~/components/SuggestedTalents/SuggestedTalentsContainer';
import {JobStatusCodes} from '~/services/employer/jobs.types';

describe('Pages/Candidates', () => {
  const mock: any = {};
  const routeComponentProps = {
    history: {
      ...mock,
      push: jest.fn(),
    },
    location: mock,
    match: {
      ...mock,
      params: {
        id: 'job123',
        tab: CandidatesTabs.Applications,
      },
    },
  };

  const candidatesProps: ICandidatesProps = {
    applicationsTotal: 12,
    fromLastLogin: '2019-01-01',
    job: jobPostMock,
    jobPostingFetchRequested: jest.fn(),
    jobUuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
    jobPostNotFound: false,
    suggestedTalentsTotal: 15,
    threshold: 1,
    x0paSuggestedTalentsFeatureToggle: true,
    newErrorPageFeatureToggle: true,
    validAccount: true,
    ...routeComponentProps,
  };

  describe('Application Tab', () => {
    it('should render CandidatesPage with applications tab selected', () => {
      const wrapper = shallow(<Candidates {...candidatesProps} />);
      const renderedTabs = wrapper.find(Tabs);

      expect(renderedTabs.props().selectedTab).toEqual(CandidatesTabs.Applications);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });
  });

  describe('Suggested Talent Tab', () => {
    it('should render CandidatesPage with suggested-talents tab selected', () => {
      const inputProps = {
        ...candidatesProps,
        match: {
          ...candidatesProps.match,
          params: {
            id: 'job123',
            tab: CandidatesTabs.SuggestedTalents,
          },
        },
      };
      const wrapper = shallow(<Candidates {...inputProps} />);
      const renderedTabs = wrapper.find(Tabs);

      expect(renderedTabs.props().selectedTab).toEqual(CandidatesTabs.SuggestedTalents);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should limit the suggestedTalents Total count with SUGGESTED_TALENTS_TOTAL_LIMIT', () => {
      const wrapper = shallow(
        <Candidates {...candidatesProps} suggestedTalentsTotal={SUGGESTED_TALENTS_TOTAL_LIMIT + 1} />,
      );
      const suggestedTalentsTabs = wrapper.findWhere(
        (component) => component.type() === Tab && component.prop('tabId') === CandidatesTabs.SuggestedTalents,
      );
      expect(suggestedTalentsTabs.prop('total')).toEqual(SUGGESTED_TALENTS_TOTAL_LIMIT);
    });

    describe('SuggestedTalentsContainer', () => {
      it('should have props unavailableSuggestedTalents = true when the job posted date is after 1 year', () => {
        const candidatesPropsWithJobPostingDatePastOneYear = {
          ...candidatesProps,
          job: {
            ...jobPostMock,
            status: {
              id: JobStatusCodes.Closed,
              jobStatus: 'Closed',
            },
            metadata: {...jobPostMock.metadata, originalPostingDate: format(subYears(new Date(), 1), 'yyyy-MM-dd')},
          },
        };
        const wrapper = shallow(<Candidates {...candidatesPropsWithJobPostingDatePastOneYear} />);
        const suggestedTalentsContainer = wrapper.find(SuggestedTalentsContainer);
        expect(suggestedTalentsContainer.prop('unavailableSuggestedTalents')).toEqual(true);
      });

      it('should have props unavailableSuggestedTalents = false when the job posted date is within 1 year', () => {
        const candidatesPropsWithJobPostingDateWithinOneYear = {
          ...candidatesProps,
          job: {
            ...jobPostMock,
            status: {
              id: JobStatusCodes.Closed,
              jobStatus: 'Closed',
            },
            metadata: {...jobPostMock.metadata, originalPostingDate: format(subMonths(new Date(), 11), 'yyyy-MM-dd')},
          },
        };
        const wrapper = shallow(<Candidates {...candidatesPropsWithJobPostingDateWithinOneYear} />);
        const suggestedTalentsContainer = wrapper.find(SuggestedTalentsContainer);
        expect(suggestedTalentsContainer.prop('unavailableSuggestedTalents')).toEqual(false);
      });
    });
  });
});
