import React from 'react';
import styles from './maintenance.scss';
import {IMaintenanceNotification} from '~/services/notification/getNotifications';

export interface IMaintenancePageProps {
  maintenance: IMaintenanceNotification;
}

export const MaintenancePage: React.FunctionComponent<IMaintenancePageProps> = ({maintenance}) => {
  const title = 'We will be back soon';

  return (
    <div data-cy="maintenance-page" className="flex justify-center items-center w-100 h-100 absolute">
      <section className="tc w-100 w-70-m w-50-l item pa4">
        <i className={styles.maintenanceTool} />

        <h1 className="f2 blue fw3 mv2 lh-solid pt4" id="title">
          {title}
        </h1>
        <p className="f5 fw5 lh-copy black-70 pb3" id="subtitle">
          {maintenance.message}
        </p>
        <i className={styles.ccLogo} />
      </section>
    </div>
  );
};
