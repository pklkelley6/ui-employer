import {addMinutes, format} from 'date-fns';
import {mount} from 'enzyme';
import React from 'react';
import {MaintenanceBranch} from '../MaintenanceBranch';
import {maintenanceNotificationMock} from '~/__mocks__/notification/notification.mocks';
import {IMaintenanceNotification, MaintenanceSystem} from '~/services/notification/getNotifications';

describe('Pages/MaintenanceBranch', () => {
  const props = {
    children: <div>children</div>,
    maintenances: [maintenanceNotificationMock],
  };

  it('renders children when no maintenance downtime within range', () => {
    const wrapper = mount(<MaintenanceBranch {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders MaintenacePage when there is maintenance downtime within range', () => {
    const maintenances: IMaintenanceNotification[] = [
      maintenanceNotificationMock,
      {
        ...maintenanceNotificationMock,
        downtime: {
          fromDate: format(new Date(), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
          system: MaintenanceSystem.CORPPASS,
          toDate: format(addMinutes(new Date(), 1), `yyyy-MM-dd'T'HH:mm:ss.SSSx`),
        },
      },
    ];
    const wrapper = mount(<MaintenanceBranch {...props} maintenances={maintenances} />);
    const maintenancePage = wrapper.find('[data-cy="maintenance-page"]');
    expect(maintenancePage).toHaveLength(1);
    expect(maintenancePage).toMatchSnapshot();
  });
});
