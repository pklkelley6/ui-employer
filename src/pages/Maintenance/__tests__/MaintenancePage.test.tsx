import {mount} from 'enzyme';
import React from 'react';
import {MaintenancePage} from '../MaintenancePage';
import {maintenanceNotificationMock} from '~/__mocks__/notification/notification.mocks';

describe('Pages/MaintenancePage', () => {
  const props = {
    maintenance: maintenanceNotificationMock,
  };

  it('renders', () => {
    const wrapper = mount(<MaintenancePage {...props} />);
    const maintenancePage = wrapper.find('[data-cy="maintenance-page"]');
    expect(maintenancePage).toHaveLength(1);
    expect(maintenancePage).toMatchSnapshot();
  });
});
