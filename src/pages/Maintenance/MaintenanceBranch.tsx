import {isWithinInterval, parseISO} from 'date-fns';
import React from 'react';
import {MaintenancePage} from './MaintenancePage';
import {IMaintenanceNotification} from '~/services/notification/getNotifications';

export interface IMaintenanceBranchProps {
  children: JSX.Element;
  maintenances: IMaintenanceNotification[];
}

export const MaintenanceBranch: React.FunctionComponent<IMaintenanceBranchProps> = ({children, maintenances}) => {
  const currentMaintenance = maintenances.find((maintenance) =>
    isWithinInterval(new Date(), {
      end: parseISO(maintenance.downtime.toDate),
      start: parseISO(maintenance.downtime.fromDate),
    }),
  );
  return currentMaintenance ? <MaintenancePage maintenance={currentMaintenance} /> : children;
};
