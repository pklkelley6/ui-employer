import {connect} from 'react-redux';
import {MaintenanceBranch} from './MaintenanceBranch';
import {IAppState} from '~/flux';

const mapStateToProps = ({notification: {maintenances}}: IAppState) => ({maintenances});

export const MaintenanceBranchContainer = connect(mapStateToProps)(MaintenanceBranch);
