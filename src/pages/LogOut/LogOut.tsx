import {LOGOUT_REQUEST_FAILED, LOGOUT_REQUESTED} from '@govtechsg/redux-account-middleware';
import React, {Fragment, ReactElement} from 'react';
import styles from './logOut.scss';

export interface ILogOutProps {
  title: string;
  subTitle: string;
  actionableElement: ReactElement<any>;
  accountStatus?: string;
  logOutRequested: () => any;
}

export class LogOut extends React.Component<ILogOutProps> {
  public componentDidMount() {
    this.props.logOutRequested();
  }

  public render() {
    let element: ReactElement<any>;
    switch (this.props.accountStatus) {
      case LOGOUT_REQUESTED:
        element = (
          <h1 className="fw4 primary ma0 lh-title f3" id="title">
            Logging Out...
          </h1>
        );
        break;
      case LOGOUT_REQUEST_FAILED:
        element = (
          <h1 className="fw4 primary ma0 lh-title f3" id="title">
            Logging Out Failed
          </h1>
        );
        break;
      default:
        element = (
          <Fragment>
            <i className={`${styles.robotLogout}`} />
            <h1 className="fw4 primary ma0 lh-title f3 mt4" id="title">
              {this.props.title}
            </h1>
            <p className="f5 black-60 mv2 pb4 lh-title" id="subtitle">
              {this.props.subTitle}
            </p>
            {this.props.actionableElement}
          </Fragment>
        );
    }
    return (
      <div id="page-logout" className="flex-auto">
        <div className="flex flex-column justify-center content-center center tc pa3 pt5">{element}</div>
      </div>
    );
  }
}
