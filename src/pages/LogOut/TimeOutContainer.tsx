import {logOutRequested} from '@govtechsg/redux-account-middleware';
import React from 'react';
import {connect} from 'react-redux';
import {login} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {LogOut} from '~/pages/LogOut/LogOut';
import {TimeOutLink} from '~/pages/LogOut/TimeOutLink';

const mapStateToProps = ({account: {status}}: IAppState) => ({
  accountStatus: status,
  subTitle: 'For your security, you were logged out after 20 minutes of inactivity.',
  title: 'Sorry, we had to log you out',
});

const mergeProps = (
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: {logOutRequested: typeof logOutRequested; login: typeof login},
) => ({
  ...stateProps,
  actionableElement: <TimeOutLink onClick={dispatchProps.login} />,
  logOutRequested: dispatchProps.logOutRequested,
});

export const TimeOutContainer = connect(mapStateToProps, {logOutRequested, login}, mergeProps)(LogOut);
