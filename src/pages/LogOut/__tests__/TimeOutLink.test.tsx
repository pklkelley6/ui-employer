import {shallow} from 'enzyme';
import React from 'react';
import {TimeOutLink} from '~/pages/LogOut/TimeOutLink';

describe('Pages/LogOut/TimeOutLink', () => {
  it('renders correctly', () => {
    const onClickMock = jest.fn();
    const wrapper = shallow(<TimeOutLink onClick={onClickMock} />);
    wrapper.find('a').simulate('click');

    expect(onClickMock).toHaveBeenCalledTimes(1);
    expect(wrapper).toMatchSnapshot();
  });
});
