import React from 'react';

export class TermsOfUse extends React.Component {
  public componentDidMount() {
    window.scrollTo(0, 0);
    document.title = 'TERMS OF USE | MyCareersFuture Employer';
  }

  public render() {
    return (
      <section id="term-of-use" className="pa4 flex center">
        <div className="bg-white mw9 pa5 black-80 lh-copy">
          <h1 className="f2-5 mv0 brand fw6 primary" id="title">
            TERMS OF USE
          </h1>
          <p>
            Welcome to MyCareersFuture, a career development and resources system with extensions to Jobs Bank for job
            applications, as well as related recommendation services (&quot;<strong>MyCareersFuture</strong>
            &quot;). The MyCareersFuture website and services are provided by Workforce Singapore Agency (&quot;
            <strong>WSG</strong>&quot;, &quot;<strong>we</strong>&quot; &quot;<strong>us</strong>&quot; or &quot;
            <strong>our</strong>&quot;). These terms of use (these &quot;<strong>Terms</strong>&quot;) govern the
            Visitors’ (as defined below, &quot;<strong>you</strong>&quot; or &quot;<strong>your</strong>&quot;) access
            to and use of MyCareersFuture, the web site and its subdomains (the &quot;<strong>Website</strong>&quot;)
            and all services provided by WSG and the subdomain operators in relation to MyCareersFuture.
          </p>
          <h2>1. Definitions</h2>

          <p>In these Terms, the following terms shall have the meanings set forth below:-</p>
          <p>
            &quot;<strong>Employer</strong>&quot; refers to an employer who lists job opportunities on Jobs Bank.
          </p>
          <p>
            &quot;<strong>Individual</strong>&quot; refers to an individual user who logs in and accesses the Website
            using his Singpass.
          </p>
          <p>
            &quot;<strong>Careers Toolkit</strong>&quot; refers to the reports, articles, videos, infographics and
            information listed on the Website under &quot;Careers Toolkit&quot;.
          </p>
          <p>
            &quot;<strong>Interaction</strong>&quot; refers to the collection and use of Personal Data by Employers from
            Individuals, by Individuals from Employers, and/or by third parties from Employers and/or Individuals.
          </p>
          <p>
            &quot;<strong>Jobs Bank</strong>&quot; refers to a platform, which is operated by WSG and which can be
            accessed through the Website, for Individuals seeking jobs to meet and exchange needs with Employers.
          </p>
          <p>
            &quot;<strong>Job Matching Services</strong>&quot; refers to the provision of job-to-skills match scores and
            job recommendations based on the technology offered by our service provider(s).
          </p>
          <p>
            &quot;<strong>MyCareersFuture Privacy Policy</strong>&quot; refers to the{' '}
            <a href="https://www.mycareersfuture.gov.sg/privacy-policy" target="_blank" className="blue">
              MyCareersFuture Privacy Policy
            </a>{' '}
            as amended from time to time.
          </p>
          <p>
            &quot;<strong>Personal Data</strong>&quot; refers to personally identifiable information about individuals
            including their name, phone number, email address and contact details, and other personal information
            incidental to such individual’s interaction with the Services, but excluding any business contact
            information.
          </p>
          <p>
            &quot;<strong>Services</strong>&quot; refers to the services available on or through the Website which
            include the Jobs Bank, Job Matching Services and Careers Toolkit.
          </p>
          <p>
            &quot;<strong>Singpass</strong>&quot; refers to the common user ID and password used to access the Singapore
            government’s online services.
          </p>
          <p>
            &quot;<strong>Users</strong>&quot; collectively refer to Employers, Individuals and Visitors, and each are
            referred to as &quot;<strong>User</strong>&quot;.
          </p>
          <p>
            &quot;<strong>User Content</strong>&quot; refers to information posted by Employers and Individuals in the
            form of text, images, photographs, video, audio and graphics that are available on the Website.
          </p>
          <p>
            &quot;<strong>Visitor</strong>&quot; refers to a user who browses the Website without logging in as an
            Individual or an Employer.
          </p>
          <p>
            &quot;<strong>Website Content</strong>&quot; refers to all information, text, images, data, links, or other
            material posted on the sections of the Website: (i) that are publicly available; and (ii) which you have the
            right to access, whether created by us or provided by a third party for display on the Website.
          </p>

          <h2>2. Acceptance of Terms</h2>

          <p>Please read these Terms carefully as they create a binding contract between you and WSG.</p>
          <p>
            If you are a corporate representative, by logging in to the Website via a CorpPass and using the Services,
            as the corporate representative, you represent and warrant that you have the authority to log in and access
            the Website and use its Services for and on behalf of your company and that you have read, understood and
            accepted these Terms for and on behalf of your company.
          </p>
          <p>
            Please review these Terms before using the Services. If you do not agree to these Terms, then you must
            immediately discontinue your use of the Website and the Services.
          </p>
          <p>
            We reserve the right to change these Terms from time to time for any reason. Any time changes are made to
            these Terms, you will be alerted of the revised Terms and will be required to read, understand and accept
            the revised Terms. Access to the Website and use of the Services are subject to these Terms, as they are
            amended from time to time and published on the Website.
          </p>

          <h2>3. Access to the Website</h2>

          <p>
            In order to log in and access the Website as an Employer, you will require a valid CorpPass. Upon the
            expiration or termination of your CorpPass, you will not be able to log in and access the Website. While any
            Visitor or Individual can browse and view the Website and use parts of the Services, you must be logged in
            with your CorpPass as an Employer in order to create a profile and view the contact information of an
            Individual. You are solely responsible for maintaining the confidentiality of your access and use of the
            Website using your CorpPass as an Employer. You agree that you will not divulge or share access information
            of your CorpPass and access to the Website using the same with any third party for any reason.
          </p>
          <p>
            During your first login to the Website, or any subsequent login, you may be prompted or required to enter
            additional information, including but not limited to your name, email address and contact number. You
            represent and warrant that all information provided by you is accurate, current and complete and undertake
            to maintain and update your information to keep it accurate, current and complete.
          </p>
          <p>
            You acknowledge that if any information by you is untrue, inaccurate, not current or incomplete, WSG may
            suspend your access to the Website, the Website Content and the Services without any liability to you.
          </p>

          <h2>4. Access to Services</h2>
          <p>
            WSG may alter, suspend, or discontinue the Services in whole or in part, at any time and for any reason,
            without notice. The Services may also periodically become unavailable due to maintenance or malfunction of
            computer equipment or for other reasons.
          </p>
          <p>
            WSG further reserves the right to remove any User Content uploaded by you that is in breach of these Terms,
            including but not limited to offensive, defamatory or libellous statements.
          </p>
          <p>
            You confirm that you are aware of and acknowledge that WSG shall have no liability in respect of such
            potential alteration, suspension, discontinuance, unavailability and/or removal.
          </p>
          <p>
            If you use our Website on your mobile device, your mobile carrier’s normal rates and charges apply. We are
            not responsible for any charges you incur from your mobile carrier as a result of use of the Services. You
            are responsible for ensuring that at all times while using the Services you are not in violation of your
            wireless data service agreement. You are solely responsible for all fees and costs associated with your
            access to and use of the Services.
          </p>

          <h2>5. Use of Services</h2>

          <p>You represent, warrant and covenant that:</p>

          <ol type="a">
            <li>
              <p>
                You will not upload, post or otherwise transmit any unsolicited or unauthorized advertising, promotional
                materials, junk mail, spam, chain letters, pyramid schemes or any other form of solicitation (commercial
                or otherwise) through the Website or the Services;
              </p>
            </li>
            <li>
              <p>
                You will not post any inappropriate, offensive, racist, hateful, sexist, pornographic, false,
                misleading, infringing, defamatory or libellous content;
              </p>
            </li>
            <li>
              <p>
                You will not reproduce, distribute, publicly display, publicly perform, communicate to the public,
                create derivative works from or otherwise use and exploit any Website Content or the Services except as
                permitted by these Terms;
              </p>
            </li>
            <li>
              <p>
                You will assume responsibility for controlling how your Personal Data is disclosed or used on the
                Website, including, without limitation, taking appropriate steps to protect such information; and
              </p>
            </li>
            <li>
              <p>You will not harass and/or solicit personal information from any Users.</p>
            </li>
          </ol>
          <h2>6. Release</h2>

          <p>
            The collection and use of the Personal Data by WSG will be subject to the MyCareersFuture Privacy Policy.
            Where there is Interaction, it is a direct transaction between Individuals and you and does not involve WSG
            in any way.
          </p>
          <p>
            WSG will not be responsible for the manner in which Personal Data is used or collected by Individuals and/or
            any third party. WSG will not manage or monitor the collection of Personal Data done by Individuals and/or
            any third party.
          </p>
          <p>
            WSG will not be responsible for the Interaction between Individuals and/or any third parties and you. WSG
            expressly disclaims any liability or claims that may arise from the Interaction. In the event that you have
            a dispute with any of the Individuals and/or any third party, you agree that WSG shall not have any
            liability in respect of such dispute and hereby release WSG (and its officers, directors, agents,
            subsidiaries, and employees) from any and all claims, demands, or damages (actual or consequential) of every
            kind, known and unknown arising out of or in any way related with such disputes.
          </p>

          <h2>7. Website Content</h2>

          <p>
            Subject to these Terms, WSG hereby grant to you a personal, non-transferable, non-exclusive,
            non-sublicensable right and licence to access and use the Services, and all Website Content therein, solely
            within the scope of your status as an Employer of the Services.
          </p>
          <p>
            WSG reserves the right to make changes to document names and content, descriptions or specifications of
            products or services, or other information without obligation to issue any notice of such changes.
          </p>
          <p>
            Nothing contained on this Website should be construed as granting, by implication, estoppel, or otherwise,
            any license or right to use this Website, Services or any Website Content, through the use of framing or
            otherwise, except: (a) as expressly permitted by these Terms; or (b) with our prior written permission or
            the permission of such third party that may own the trademark or copyright of material displayed on this
            Website.
          </p>

          <h2>8. User Content </h2>

          <ol type="a">
            <li>
              <p>
                <u>Accuracy.</u> If you choose to provide information as an Employer to register for or utilise any
                Services, event, or promotion on this Website, you agree that you will provide accurate, complete, and
                up to date information as requested on the screens that collect information from you. You agree that WSG
                may share the information provided by you (save for personally identifiable information which shall be
                handled in accordance with the MyCareersFuture Privacy Policy) with third parties. Such information may
                include WSG’s sharing of your listing of job opportunities with third parties. WSG may also permit third
                parties to publish your listing of job opportunities. Please refer to the MyCareersFuture Privacy Policy
                for information on our practices for handling personally identifiable information.
              </p>
            </li>
            <li>
              <p>
                <u>Liability.</u> WSG is not responsible or liable for the conduct of Users or for views, opinions and
                statements expressed in User Content submitted for display through the Services. Information submitted
                to the Website is not screened. With respect to such User Content posted on or through the Services, the
                Website acts as a passive conduit for such distribution and WSG is not responsible for such User
                Content. Any opinions, advice, statements, services, offers, or other information in the Services
                provided by Users are those of the respective author(s) or distributor(s) and not of WSG. We neither
                endorse nor guarantee the accuracy, completeness, or usefulness of any such User Content.
              </p>
              <p>
                You are responsible for ensuring that User Content submitted to or through the Services is not provided
                in violation of any applicable laws, including without limitation, laws relating to defamation, libel
                and slander, laws of confidentiality and non-disclosure, and laws relating to copyright, trade secret or
                other intellectual property rights of another person or entity or of any applicable law. You shall be
                solely liable for any damages resulting from any infringement or breach of any applicable laws,
                including without limitation, relating to defamation, libel and slander, and laws relating to
                copyrights, trade secret, or other intellectual property rights, or any other harm resulting from your
                uploading, posting or submission of User Content on this Website. WSG shall not be responsible for any
                breach of any contractual obligation or infringement of any third-party rights.
              </p>
            </li>
          </ol>

          <h2>9. Monitoring</h2>
          <p>
            WSG has the right, but not the obligation, to monitor User Content submitted through the Services or posted
            on the Website, to determine compliance with these Terms and any other applicable rules that may be
            established, from time to time. WSG has the right in our sole discretion to edit or remove any material
            submitted to or posted in any online discussion forum or chat room provided through the Services. Without
            limiting the foregoing, WSG has the right to remove any material that WSG, in its sole discretion, finds to
            be in violation of these Terms or otherwise objectionable, and you are solely responsible for the User
            Content that you post on or through the Services.
          </p>
          <h2>10. Links to External Websites</h2>

          <p>
            The Services may contain links to websites which are not operated or controlled by WSG, whether for
            marketing purposes or otherwise. These links are provided to you as a matter of convenience, and WSG makes
            no representation or warranty whatsoever in respect of any linked website and shall not be responsible for
            the content of any linked website. Any non-WSG website accessed from the Services is independent from WSG,
            and WSG has no control over the content of that website. In addition, a link to any non-WSG website from the
            Website does not imply that WSG endorses or accepts any responsibility for the content or use of such
            website. Use of any external website is subject to its terms of service and privacy policy. You are
            responsible for exercising caution and good judgment when using external websites.
          </p>

          <h2>11. Links from External Websites</h2>

          <p>
            Except as set forth below, caching and links to, and the framing of, this Website or any of the User Content
            and/or Website Content is strictly prohibited.
          </p>
          <p>
            You must secure written permission from WSG prior to hyper-linking to, or framing of, this Website or any of
            the User Content and/or Website Content, or engaging in similar activities. WSG reserves the right to impose
            conditions when permitting any hyper-linking to, or framing of, this Website or any of the User Content
            and/or Website Content.
          </p>
          <p>
            Under no circumstances shall WSG be considered to be associated or affiliated in whatever manner with any
            trade or service marks, logos, insignia or other devices used or appearing on websites that link to this
            website or any of the User Content and/or Website Content.
          </p>
          <p>
            WSG reserves all rights to disable or block any links to, or frames of any website containing inappropriate,
            profane, defamatory, infringing, obscene, indecent or unlawful topics, names, material or information, or
            material or information that violates any written law, any applicable intellectual property, proprietary,
            privacy or publicity rights.
          </p>
          <p>
            WSG reserves the right to disable or block any unauthorised links or frames and disclaims any responsibility
            for the content available on any other website reached by links to or from this Website or any of the User
            Content and/or Website Content.
          </p>

          <h2>12. Disclaimer</h2>

          <p>
            Under no circumstances will WSG be liable for any loss or damage caused by your reliance on any information
            communicated or provided on the Website or through the Services, or any information contained when you
            access the Website as an Employer.
          </p>
          <p>
            ALL WEBSITE CONTENT AND USER CONTENT AND INFORMATION ON OR ACCESSIBLE FROM THE SERVICES ARE PROVIDED “AS IS”
            WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMITTED BY LAW. WSG WILL
            NOT WARRANT AND HEREBY DISCLAIMS ANY WARRANTY:
          </p>
          <ol type="a">
            <li>
              <p>
                AS TO THE ACCURACY, CORRECTNESS, COMPLETENESS, RELIABILITY, TIMELINESS, NON-INFRINGEMENT, TITLE,
                MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OF THE WEBSITE CONTENT OR USER CONTENT OR THE
                SERVICES; AND
              </p>
            </li>

            <li>
              <p>
                THAT THE WEBSITE, THE USER CONTENT AND/OR THE WEBSITE CONTENT OR ANY FUNCTIONS ASSOCIATED THEREWITH WILL
                BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS WILL BE CORRECTED OR THAT THIS WEBSITE AND THE SERVER IS
                AND WILL BE FREE OF ALL VIRUSES, WORMS AND/OR OTHER HARMFUL OR INVASIVE ELEMENTS.
              </p>
            </li>
          </ol>
          <p>
            WSG SHALL NOT BE LIABLE FOR ANY INJURY, DAMAGE OR LOSS OF ANY KIND CAUSED AS A RESULT (DIRECT OR INDIRECT)
            OF THE USE OF THE WEBSITE, INCLUDING BUT NOT LIMITED TO ANY INJURY, DAMAGE OR LOSS SUFFERED AS A RESULT OF
            RELIANCE ON THE CONTENTS CONTAINED IN OR AVAILABLE FROM THE WEBSITE. NEITHER WSG NOR ANY OF ITS THIRD PARTY
            AGENTS SHALL HAVE ANY RESPONSIBILITY OR LIABILITY FOR ANY INJURY, DAMAGE OR LOSS CAUSED BY ANY NEGLIGENCE,
            OMISSION OR FAULT OF WSG, AND/OR ITS EMPLOYEES, AGENTS OR SUB-CONTRACTORS IN CONNECTION WITH THE WEBSITE.
          </p>
          <p>
            WSG SHALL NOT BE RESPONSIBLE FOR THE INTERACTION BETWEEN YOU AND/OR OTHER USERS AND/OR ANY THIRD PARTY. WSG
            IS NOT RESPONSIBLE FOR DISPUTES, CLAIMS, LOSSES, INJURIES, OR DAMAGE OF ANY KIND THAT MIGHT ARISE OUT OF OR
            RELATE TO CONDUCT OF OTHER USERS AND/OR ANY THIRD PARTY, INCLUDING, BUT NOT LIMITED TO, ANY USER’S RELIANCE
            UPON ANY INFORMATION PROVIDED BY SUCH OTHER USERS AND/OR THIRD PARTIES. WSG DOES NOT CONTROL CONTENT FROM
            OTHER USERS AND/OR ANY THIRD PARTY POSTED ON THE WEBSITE AND, AS SUCH, WSG DOES NOT GUARANTEE IN ANY MANNER
            THE RELIABILITY, VALIDITY, ACCURACY OR TRUTHFULNESS OF SUCH CONTENT. YOU ALSO UNDERSTAND THAT BY USING THE
            SERVICES MAY EXPOSE YOU TO CONTENT THAT YOU CONSIDER OFFENSIVE, INDECENT, OR OBJECTIONABLE. WSG DOES NOT
            HAVE ANY RESPONSIBILITY TO KEEP SUCH CONTENT FROM YOU OR BEARS ANY LIABILITY FOR YOUR ACCESS OR USE OF ANY
            SUBMITTED CONTENT, TO THE EXTENT PERMISSIBLE UNDER APPLICABLE LAW. WSG CANNOT AND DOES NOT GUARANTEE THAT
            ANY PERSONAL DATA SUPPLIED BY YOU WILL NOT BE MISAPPROPRIATED, INTERCEPTED, DELETED, DESTROYED OR USED BY
            OTHERS.
          </p>

          <h2>13. Indemnification</h2>

          <p>
            You hereby agree to indemnify, hold harmless and defend WSG, its affiliates, officers, directors, agents,
            partners, employees, licensors and representatives from any claims, damages, losses, liabilities, and all
            costs and expenses of defence, including but not limited to, legal fees, resulting directly or indirectly
            from a claim that arises in connection with your use of the Services.
          </p>

          <p>
            Notwithstanding the indemnification given by you, WSG shall have the right to assume the exclusive defence
            and control of any matter otherwise subject to indemnification by you under this Clause 13, and in such
            case, you agree to fully cooperate as reasonably required with such defence and in asserting any available
            defences.
          </p>

          <h2>14. Limitation of Liability</h2>

          <p>
            <u>Disclaimer</u> - In no event shall WSG, its affiliates, officers, directors, agents, partners, employees,
            licensors and/or representatives be liable to you or any other person or entity for any direct, indirect,
            special, incidental, consequential or exemplary damages (including, but not limited to, damages for loss of
            profits, loss of data, loss of use, or costs of obtaining substitute goods or services) arising out of the
            use, inability to use, unauthorized access to or use or misuse of the Services, questions &amp; comments,
            content or any information contained thereon, whether based upon warranty, contract, tort (including
            negligence), or otherwise, even if WSG has been advised of the possibility of such damages or losses.
          </p>

          <h2>15. Proprietary Rights</h2>

          <p>
            This Website, including the Website Content, is protected by copyright, trademarks and other forms of
            proprietary rights. All rights, title and interest in the Website Content are owned by, licensed to or
            controlled by WSG.
          </p>
          <p>
            Unless otherwise stated in the copyright attribution of any Website Content, WSG has the sole copyright to
            all works on the Website, Jobs Bank and in the Services.
          </p>
          <p>
            All rights reserved. No part of any works on the Website may be reproduced, stored in a retrieval system, or
            transmitted in any form or by any means, whether electronic, mechanical, photocopying, recording, or
            otherwise, without written permission from WSG.
          </p>

          <h2>16. Restrictions on Use</h2>

          <p>
            The Services, including this Website may not be used for any illegal purpose or in any manner inconsistent
            with these Terms. Except as otherwise provided, the Website Content shall not be reproduced, republished,
            uploaded, posted, transmitted, adapted, modified or otherwise displayed or distributed in any way, without
            the prior written permission of WSG.
          </p>
          <p>
            Modification of any of the Website Content or use of the Website Content for any other purpose will be a
            violation of WSG’s copyright and other intellectual property rights. Graphics, photographs, video, audio,
            software programs and images on this Website are protected by copyright and other proprietary rights and may
            not be reproduced, published, distributed, copied, modified, appropriated or exploited in any manner without
            the prior written permission of WSG.
          </p>

          <h2>17. Jurisdictional Issues</h2>

          <p>
            WSG makes no representation that information on this Website is appropriate or available for use outside
            Singapore. Those who choose to access this Website from outside Singapore do so on their own initiative and
            at their own risk and are responsible for compliance with applicable local laws. By using the Services, you
            consent to having your Personal Data transferred to and processed in Singapore, subject to the restrictions
            on such data applicable to WSG.
          </p>

          <h2>18. Termination</h2>

          <p>
            WSG reserves all rights to deny or restrict access to this Website by any particular person, or to block
            access from a particular IP address to this Website, at any time, without ascribing any reasons whatsoever.
          </p>
          <p>
            WSG may suspend your access as an Employer to the Website and its Services, in our sole discretion, for any
            reason and at any time, upon electronic notice to you at the e-mail address provided by you during your
            initial login to the Website. We may discontinue offering the Services and the Website Content at any time.
          </p>
          <p>
            We have no obligation to retain your data including Personal Data for any period of time beyond what may be
            required by applicable law. Upon expiration and/or termination of your CorpPass, your access to the Website
            and use of the Services as an Employer will automatically cease. All representations and warranties shall
            survive termination.
          </p>

          <h2>19. Governing Law</h2>
          <p>
            These Terms shall be governed by and construed according to the laws of Singapore and subject to the
            exclusive jurisdiction of the Singapore Courts. In the event of any dispute arising out of or in connection
            with these Terms including any question regarding its existence, validity or termination, the parties shall
            at first instance take reasonable efforts to settle and resolve such disputes in good faith and in an
            amicable manner by negotiation.
          </p>

          <p>
            None of the terms herein shall be enforceable by virtue of the Contracts (Rights of Third Parties) Act
            (Chapter 53B) or otherwise, by any person or entity who is not a party hereto.
          </p>

          <h2>20. For Additional Information</h2>
          <p>If you have any questions about these Terms, please contact us at: 6883 5885.</p>
          <h2>21. Careers Toolkit</h2>

          <p>
            The opinions expressed in the Careers Toolkit are those of the author(s) and do not reflect the opinions of
            WSG.
          </p>
          <p>
            Information contained in the Careers Toolkit has been either written or commissioned by WSG or obtained by
            WSG from third party sources. However, neither WSG nor the authors of the Careers Toolkit guarantee the
            accuracy or completeness of any information published herein and neither WSG nor the authors shall be
            responsible for any errors, omissions, or claims for damages, including exemplary damages, arising out of
            use, inability to use, or with regard to the accuracy or sufficiency of the information contained in the
            Careers Toolkit. WSG also makes no warranty that the information contained in the Careers Toolkit is free
            from any copyright infringement.
          </p>
          <p>
            WSG shall not be liable for any injury, damage or loss of any kind caused as a result (direct or indirect)
            of the use of the Careers Toolkit, including but not limited to any injury, damage or loss suffered as a
            result of reliance on the Careers Toolkit. WSG, its affiliates, officers, directors, agents, partners,
            employees, licensors and representatives shall have no responsibility or liability for any injury, damage or
            loss caused by any negligence, omission or fault of WSG, its employees, agents or sub-contractors in
            connection with the Careers Toolkit.
          </p>
        </div>
      </section>
    );
  }
}
