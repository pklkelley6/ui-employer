import React from 'react';
import corpPassHelpSelectUsers from '../../static/images/CPH-select-users.png';
import corpPassAssignEservice from '../../static/images/CPH-assign-eservice.png';
import corpPassInputEserviceDetails from '../../static/images/CPH-input-eservice-details.png';
import corpPassReviewAndSubmit from '../../static/images/CPH-review-and-submit.png';

export class CorpPassHelp extends React.Component {
  public componentDidMount() {
    window.scrollTo(0, 0);
    document.title = 'Corppass Help | MyCareersFuture Employer';
  }

  public render() {
    return (
      <section id="corppass-help" className="pa4 mw8 center lh-copy">
        <h1 className="f3 fw7 black-80 mb4 pb3" id="title">
          Setting up access for MyCareersFuture Employer Portal
        </h1>
        <h2 className="f5 fw7 black-80 mb0">Make sure you have both Singpass and Corppass accounts</h2>
        <p className="f5 black-80 mv3 pt2">
          Singapore Corporate Access (or Corppass) is a one-stop authentication and authorisation service, for corporate
          users to transact with government agencies online on behalf of their organisations. <br />
          <br />
          From 11 April 2021, the login process for Corppass will be changed to verify the user’s identity via Singpass
          first, before the user can proceed to access and transact with government digital services. <br />
          <br />
          While Singpass is used for logins, Corppass will continue to be the authorisation system for corporate
          transactions. The Corppass portal enables company administrators to specify the digital services that each
          employee can transact on the company’s behalf. <br />
          <br />
          <a href="https://go.gov.sg/corporate-login" target="_blank" className="underline blue">
            Find out more about the change in Corppass login process here
          </a>
        </p>
        <h2 className="f5 fw7 black-80 mb0 mt5">If you don’t have a Corppass user account yet:</h2>
        <p className="f5 black-80 mv3 pt2">
          Check that your organisation has registered for Corppass and created a user account for you. <br />
          <a href="https://www.corppass.gov.sg/corppass/common/findoutmore" target="_blank" className="underline blue">
            Find out more about Corppass here
          </a>
        </p>
        <h2 className="f5 fw7 black-80 mb0 mt5">
          If you have a Corppass user account but cannot access MyCareersFuture Employer Portal:{' '}
        </h2>
        <ol type="i" className="ph3">
          <li className="pl3 pt2 mv3">
            Identify your Corppass Admin and get them to follow the steps below. If you don’t know who your Corppass
            Admin is, you may search for your Corppass Admin{' '}
            <a
              href="https://www.corppass.gov.sg/corppass/enquirecp/enquire/singpassauth"
              target="_blank"
              className="underline blue"
            >
              here
            </a>
          </li>
          <li className="pl3 pt2 mv3 ">
            Get your Corppass Admin to log in to{' '}
            <a href="https://www.corppass.gov.sg" target="_blank" className="blue underline">
              Corppass
            </a>{' '}
            and select the users that need to access MyCareersFuture Employer Portal.
            <div className="w-100 pa4 bg-black-10 mv2 tc">
              <img src={corpPassHelpSelectUsers} className="lh-solid w-100" />
            </div>
          </li>
          <li className="pl3 pt2 mv3">
            Select MyCareersFuture Employer Portal as the e-Service.
            <div className="w-100 pa4 bg-black-10 mv2 tc">
              <img src={corpPassAssignEservice} className="lh-solid" style={{width: '86%'}} />
            </div>
          </li>
          <li className="pl3 pt2 mv3">
            Enter the relevant details for each user.
            <div className="w-100 pa4 bg-black-10 mv2 tc">
              <img src={corpPassInputEserviceDetails} className="lh-solid" style={{width: '84%'}} />
            </div>
          </li>
          <li className="pl3 pt2 mv3">
            Review the details before submitting.
            <div className="w-100 pa4 bg-black-10 mv2 tc">
              <img src={corpPassReviewAndSubmit} className="lh-solid" style={{width: '68%'}} />
            </div>
          </li>
        </ol>
      </section>
    );
  }
}
