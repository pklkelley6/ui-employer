import {connect} from 'react-redux';
import {JobsPage} from './JobsPage';
import {onJobSearchQueried, onPageChangeClicked} from '~/flux/analytics';
import {IAppState} from '~/flux/index';
import {fetchClosedJobsRequested, fetchOpenJobsRequested} from '~/flux/jobPostings/jobPostings.actions';

export const mapStateToProps = ({
  jobPostings: {
    openJobs,
    closedJobs,
    closedJobsParams,
    closedJobsTotal,
    openJobsParams,
    openJobsTotal,
    openJobsFetchStatus,
    closedJobsFetchStatus,
  },
}: IAppState) => ({
  closedJobs,
  closedJobsFetchStatus,
  closedJobsParams,
  closedJobsTotal,
  openJobs,
  openJobsFetchStatus,
  openJobsParams,
  openJobsTotal,
});

export const JobsPageContainer = connect(mapStateToProps, {
  fetchClosedJobs: fetchClosedJobsRequested,
  fetchOpenJobs: fetchOpenJobsRequested,
  jobSearchQueried: onJobSearchQueried,
  pageChangeClicked: onPageChangeClicked,
})(JobsPage);
