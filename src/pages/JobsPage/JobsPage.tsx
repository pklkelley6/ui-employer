import {isEmpty, isEqual, omit, trim} from 'lodash/fp';
import React, {Component} from 'react';
import {ActionType} from 'typesafe-actions';
import styles from './JobsPage.scss';
import {handlePageView} from '~/analytics/pages';
import {JobsSortSelectionContainer} from '~/components/Filters/JobsSortSelection/JobsSortSelectionContainer';
import {JobList} from '~/components/JobList/JobList';
import {JobsQuery} from '~/components/Jobs/JobsQuery';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {HeaderContainer} from '~/components/Navigation/Header';
import {CreateJobPostingButton} from '~/components/Navigation/NewJobPostButton/CreateJobPostingButton';
import {Pagination} from '~/components/Navigation/Pagination/Pagination';
import {Tab, Tabs} from '~/components/Navigation/Tabs';
import {ISearchBoxProps, SearchBox} from '~/components/SearchBox/SearchBox';
import {ALL_JOBS_CLOSED, ALL_JOBS_OPEN, onJobSearchQueried, onPageChangeClicked} from '~/flux/analytics';
import {JOBS_STATUS} from '~/flux/jobPostings';
import {fetchClosedJobsRequested, fetchOpenJobsRequested} from '~/flux/jobPostings/jobPostings.actions';
import {IJobsResponse} from '~/flux/jobPostings/jobPostings.reducer';
import {IFetchClosedJobs, IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {reJobPostId} from '~/pages/JobsPage/JobsPage.constants';
import {PAGE_SIZE} from '~/services/employer/jobs';

export interface IJobPageProps {
  openJobs: IJobsResponse;
  openJobsParams: IFetchOpenJobs;
  openJobsFetchStatus?: string;
  closedJobs: IJobsResponse;
  closedJobsFetchStatus?: string;
  fetchStatus?: string;
  fetchOpenJobs: (query: IFetchOpenJobs) => ActionType<typeof fetchOpenJobsRequested>;
  fetchClosedJobs: (query: IFetchClosedJobs) => ActionType<typeof fetchClosedJobsRequested>;
  pageChangeClicked: typeof onPageChangeClicked;
  jobSearchQueried: typeof onJobSearchQueried;
  closedJobsParams: IFetchClosedJobs;
  openJobsTotal: number;
  closedJobsTotal: number;
}

export enum JobsTabs {
  Open = 1,
  Closed,
}

export interface IJobPageState {
  selectedTab: JobsTabs;
}

export const getSearchParam = (searchQuery: string) => {
  if (isEmpty(searchQuery)) {
    return {};
  }
  return searchQuery.match(reJobPostId)
    ? {
        jobPostId: searchQuery,
      }
    : {
        title: searchQuery,
      };
};

export class JobsPage extends Component<IJobPageProps, IJobPageState> {
  constructor(props: IJobPageProps) {
    super(props);
    this.state = {
      selectedTab: JobsTabs.Open,
    };
    this.handleTabSelect = this.handleTabSelect.bind(this);
  }

  public handleTabSelect(index: JobsTabs) {
    this.setState({selectedTab: index});
  }

  public componentDidMount() {
    this.fetchAllJobs();
    handlePageView('jobs');
    document.title = `All Jobs | MyCareersFuture Employer`;
  }

  public componentDidUpdate(prevProps: IJobPageProps) {
    if (
      !isEqual(this.props.closedJobsParams, prevProps.closedJobsParams) ||
      !isEqual(this.props.openJobsParams, prevProps.openJobsParams)
    ) {
      this.fetchAllJobs();
    }
  }

  public fetchAllJobs() {
    const {openJobsParams, closedJobsParams, fetchOpenJobs, fetchClosedJobs} = this.props;
    fetchOpenJobs(openJobsParams);
    fetchClosedJobs(closedJobsParams);
  }

  public render() {
    const {
      openJobsParams,
      closedJobsParams,
      openJobs,
      openJobsFetchStatus,
      closedJobs,
      closedJobsFetchStatus,
      fetchOpenJobs,
      fetchClosedJobs,
      openJobsTotal,
      closedJobsTotal,
      pageChangeClicked,
    } = this.props;

    const openJobsTabTitle = 'Open';
    const closedJobsTabTitle = 'Closed';

    const openJobsSearchBoxProps: ISearchBoxProps = {
      clearable: true,
      initialValue: openJobsParams.title || openJobsParams.jobPostId,
      onSearch: (searchQuery) => {
        const trimmedSearchQuery = trim(searchQuery);
        fetchOpenJobs({
          ...omit(['title', 'jobPostId'], openJobsParams),
          ...getSearchParam(trimmedSearchQuery),
          page: 0,
        });
        this.props.jobSearchQueried(trimmedSearchQuery);
      },
      placeholder: 'Search by job title or JOB-ID',
    };

    const closedJobsSearchBoxProps: ISearchBoxProps = {
      clearable: true,
      initialValue: closedJobsParams.title || closedJobsParams.jobPostId,
      onSearch: (searchQuery) => {
        const trimmedSearchQuery = trim(searchQuery);
        fetchClosedJobs({
          ...omit(['title', 'jobPostId'], closedJobsParams),
          ...getSearchParam(trimmedSearchQuery),
          page: 0,
        });
        this.props.jobSearchQueried(trimmedSearchQuery);
      },
      placeholder: 'Search by job title or JOB-ID',
    };

    const showLockDescription = () => {
      return (
        <div className="flex">
          <i className={styles.iconLock} />
          <span className="black-70 pl3 lh-copy f4-5">
            Job postings created on external sources cannot be managed on this portal.
          </span>
        </div>
      );
    };

    const hasOpenJobsSearchParams = !!(openJobsParams.title || openJobsParams.jobPostId);
    const hasClosedJobsSearchParams = !!(closedJobsParams.title || closedJobsParams.jobPostId);

    return (
      <section className="flex-auto-ie">
        <HeaderContainer data-cy="manage-applicants-all-jobs" crumbs={['All Jobs']} />
        <div className={`relative ${styles.content}`}>
          <div className="absolute top-1 right-0 pr4 pt2">
            <CreateJobPostingButton />
          </div>
          <Tabs selectedTab={this.state.selectedTab} onTabSelect={this.handleTabSelect}>
            <Tab tabId={JobsTabs.Open} title={openJobsTabTitle} total={openJobsTotal}>
              <div className="flex tools center ph3-ns pv3 mv3 items-center justify-between flex-wrap">
                <div className="w-100 f4-5 pa3">
                  <div className="lh-copy black-70 pv4">
                    Here you’ll find your job postings that are open for applications. <br />
                    Click on a job below to review your applicants or discover other suggested talents.
                  </div>
                  {showLockDescription()}
                </div>
                <div className="w-50 pl3">
                  <SearchBox {...openJobsSearchBoxProps} />
                </div>
                <div className="pr3">
                  <JobsSortSelectionContainer
                    fetchJobs={({value, orderDirection}) => {
                      fetchOpenJobs({
                        ...openJobsParams,
                        orderBy: value,
                        orderDirection,
                        page: 0,
                      });
                    }}
                    jobsStatus={JOBS_STATUS.OPEN_JOBS}
                  />
                </div>
              </div>

              <JobsQuery status={openJobsFetchStatus} payload={openJobs}>
                {({loading, error, data}) => {
                  if (loading) {
                    return <CardLoader className="mh2 pa4 w-100" cardNumber={8} boxNumber={2} cardGap={3} />;
                  }

                  if (error) {
                    return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
                  }

                  return (
                    <>
                      <JobList
                        jobs={data.results}
                        total={data.total}
                        hasSearchResultMessage={hasOpenJobsSearchParams}
                      />
                      <Pagination
                        currentPage={openJobsParams.page}
                        totalItemsLength={data.total}
                        itemsPerPage={PAGE_SIZE}
                        maxVisiblePageButtons={9}
                        onPageChange={(page) => {
                          fetchOpenJobs({
                            ...openJobsParams,
                            page,
                          });
                          pageChangeClicked({jobStatus: ALL_JOBS_OPEN, page: page + 1});
                          window.scroll(0, 0);
                        }}
                      />
                    </>
                  );
                }}
              </JobsQuery>
            </Tab>
            <Tab tabId={JobsTabs.Closed} title={closedJobsTabTitle} total={closedJobsTotal}>
              <div className="flex tools center ph3-ns pv3 mv3 items-center justify-between flex-wrap">
                <div className="w-100 f4-5 pa3">
                  <div className="lh-copy black-70 pv4">
                    Closed jobs won't receive any new applications. <br />
                    You may continue to view the updated list of suggested talents.
                  </div>
                  {showLockDescription()}
                </div>
                <div className="w-50 pl3">
                  <SearchBox {...closedJobsSearchBoxProps} />
                </div>
                <div className="pr3">
                  <JobsSortSelectionContainer
                    fetchJobs={({value, orderDirection}) => {
                      fetchClosedJobs({
                        ...closedJobsParams,
                        orderBy: value,
                        orderDirection,
                        page: 0,
                      });
                    }}
                    jobsStatus={JOBS_STATUS.CLOSED_JOBS}
                  />
                </div>
              </div>
              <JobsQuery status={closedJobsFetchStatus} payload={closedJobs}>
                {({loading, error, data}) => {
                  if (loading) {
                    return <CardLoader className="pa4 w-100 mt4" cardNumber={8} boxNumber={2} cardGap={3} />;
                  }

                  if (error) {
                    return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
                  }

                  return (
                    <>
                      <JobList
                        jobs={data.results}
                        total={data.total}
                        hasSearchResultMessage={hasClosedJobsSearchParams}
                      />
                      <Pagination
                        currentPage={closedJobsParams.page}
                        totalItemsLength={closedJobs.total}
                        itemsPerPage={PAGE_SIZE}
                        maxVisiblePageButtons={9}
                        onPageChange={(page: number) => {
                          fetchClosedJobs({
                            ...closedJobsParams,
                            page,
                          });
                          pageChangeClicked({jobStatus: ALL_JOBS_CLOSED, page: page + 1});
                          window.scroll(0, 0);
                        }}
                      />
                    </>
                  );
                }}
              </JobsQuery>
            </Tab>
          </Tabs>
        </div>
      </section>
    );
  }
}
