import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {getSearchParam, JobsPage, JobsTabs} from '../JobsPage';
import {CreateJobPostingButton} from '~/components/Navigation/NewJobPostButton/CreateJobPostingButton';
import {Tab, Tabs} from '~/components/Navigation/Tabs';
import {JobsSortCriteria} from '~/flux/jobPostings';

jest.mock('~/analytics/pages', () => ({
  handlePageView: jest.fn(),
}));

describe('getSearchParam', () => {
  it('should return an object with jobPostId parameter if search query is a job Id', () => {
    const searchQuery = 'MCF-2020-1234567';
    expect(getSearchParam(searchQuery)).toEqual({
      jobPostId: searchQuery,
    });
  });

  it('should return an object with title parameter if search query is not a job Id', () => {
    const searchQuery = 'Some Job Title';
    expect(getSearchParam(searchQuery)).toEqual({
      title: searchQuery,
    });
  });

  it('should return an empty object if the search query is an empty string', () => {
    const searchQuery = '';
    expect(getSearchParam(searchQuery)).toEqual({});
  });
});

describe('Pages/JobsPage', () => {
  const fetchOpenJobsMock = jest.fn();
  const fetchClosedJobsMock = jest.fn();
  const onPageChangeClicked = jest.fn();
  const onJobSearchQueried = jest.fn();

  const openJobsMock = {
    results: [],
    total: 10,
  };

  const closedJobsMock = {
    results: [],
    total: 20,
  };

  const propsMock = {
    closedJobs: closedJobsMock,
    closedJobsParams: {
      limit: 20,
      page: 0,
    },
    closedJobsTotal: 0,
    fetchClosedJobs: fetchClosedJobsMock,
    fetchOpenJobs: fetchOpenJobsMock,
    jobSearchQueried: onJobSearchQueried,
    openJobs: openJobsMock,
    openJobsParams: {
      limit: 20,
      orderBy: JobsSortCriteria.EXPIRY_DATE,
      page: 0,
    },
    openJobsTotal: 0,
    pageChangeClicked: onPageChangeClicked,
  };

  const scrollToMock = jest.fn();
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    window.scroll = scrollToMock;
    wrapper = shallow(<JobsPage {...propsMock} />);
  });

  afterEach(() => {
    fetchOpenJobsMock.mockReset();
    fetchClosedJobsMock.mockReset();
    onPageChangeClicked.mockReset();
  });

  it('renders NewJobPostButton', () => {
    const button = wrapper.find(CreateJobPostingButton);
    expect(button).toHaveLength(1);
  });

  it('should call fetchOpenJobs and fetchClosedJobs actions', () => {
    expect(fetchOpenJobsMock).toHaveBeenCalledTimes(1);
    expect(fetchClosedJobsMock).toHaveBeenCalledTimes(1);
  });

  it('renders Open and Closed tabs', () => {
    expect(wrapper.find(Tabs)).toHaveLength(1);
    expect(wrapper.find(Tab).findWhere((n) => n.prop('tabId') === JobsTabs.Open)).toHaveLength(1);
    expect(wrapper.find(Tab).findWhere((n) => n.prop('tabId') === JobsTabs.Closed)).toHaveLength(1);
  });
});
