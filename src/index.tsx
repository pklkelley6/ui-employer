import '@govtechsg/mcf-mcfui/dist/main.css';
import React from 'react';
import ReactDOM from 'react-dom';

import 'tachyons/css/tachyons.min.css';
import './styles/main.scss';

import {loadFeatureToggles} from './services/featureToggles';
import AppContainer from '~/AppContainer';
import {createAndInitStore} from '~/flux';

const init = async () => {
  const featureToggles: Record<string, boolean> = await loadFeatureToggles();
  const releaseToggles = featureToggles; // release toggles is currently in the same list as feature toggles
  const store = await createAndInitStore(featureToggles, releaseToggles);

  ReactDOM.render(<AppContainer store={store} />, document.getElementById('root'));
};

init();
