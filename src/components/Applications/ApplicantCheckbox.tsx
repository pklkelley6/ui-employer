import React from 'react';
import styles from '~/components/Candidates/CandidatesList.scss';

export interface IApplicantCheckboxProps {
  onApplicantCheckboxChanged: (event: React.ChangeEvent<HTMLInputElement>) => void;
  isApplicantCheckboxChecked: boolean;
}

export const ApplicantCheckbox: React.FunctionComponent<IApplicantCheckboxProps> = ({
  onApplicantCheckboxChanged,
  isApplicantCheckboxChecked = false,
}) => {
  return (
    <div className="pr1">
      <label>
        <input
          data-cy="candidate-checkbox"
          className={styles.inputCheckbox}
          type="checkbox"
          onChange={onApplicantCheckboxChanged}
          checked={isApplicantCheckboxChecked}
        />
        <span className="f5 black-80 pointer" />
      </label>
    </div>
  );
};
