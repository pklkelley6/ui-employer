import React from 'react';
import {Link} from 'react-router-dom';
import {EmptyState} from '~/components/Layouts/EmptyState';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';

export const EmptyApplicationList: React.FunctionComponent = () => (
  <EmptyState className="black-50">
    <h1 className="f3">There are no applicants for this job yet.</h1>
    <h2 className="f4">
      Maximise your candidate pool by reaching out to{' '}
      <Link className="fw6 blue no-underline" to={CandidatesTabs.SuggestedTalents}>
        Suggested Talents
      </Link>
    </h2>
  </EmptyState>
);
