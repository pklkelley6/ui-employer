import {connect} from 'react-redux';
import {IJobPost} from 'src/services/employer/jobs.types';
import {Applications} from './Applications';
import {
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onApplicationsFilter,
  onApplicationTopMatchLabelClicked,
  onMassUpdateApplicationsSelected,
  onMassUpdateApplicationsStatusUpdated,
} from '~/flux/analytics';
import {IAppState} from '~/flux/index';

export interface IApplicationsContainerDispatchProps {
  onApplicationClicked: typeof onApplicationClicked;
  onApplicationTopMatchLabelClicked: typeof onApplicationTopMatchLabelClicked;
  onApplicationCandidateTabClicked: typeof onApplicationCandidateTabClicked;
  onApplicationPageClicked: typeof onApplicationPageClicked;
  onApplicationCandidateResumeClicked: typeof onApplicationCandidateResumeClicked;
  onApplicationSortingTypeClicked: typeof onApplicationSortingTypeClicked;
  onApplicationsFilter: typeof onApplicationsFilter;
  onApplicationTopMatchFilter: typeof onApplicationTopMatchFilter;
  onMassUpdateApplicationsSelected: typeof onMassUpdateApplicationsSelected;
  onMassUpdateApplicationsStatusUpdated: typeof onMassUpdateApplicationsStatusUpdated;
}

export interface IApplicationsContainerOwnProps {
  jobUuid: string;
  job?: IJobPost;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export type IApplicationsProps = IApplicationsContainerDispatchProps & IApplicationsContainerOwnProps;

export const ApplicationsContainer = connect<
  {},
  IApplicationsContainerDispatchProps,
  IApplicationsContainerOwnProps,
  IAppState
>(null, {
  onApplicationCandidateResumeClicked,
  onApplicationCandidateTabClicked,
  onApplicationClicked,
  onApplicationPageClicked,
  onApplicationSortingTypeClicked,
  onApplicationTopMatchFilter,
  onApplicationsFilter,
  onApplicationTopMatchLabelClicked,
  onMassUpdateApplicationsSelected,
  onMassUpdateApplicationsStatusUpdated,
})(Applications);
