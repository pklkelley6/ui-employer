import {DataProxy} from 'apollo-cache';
import React from 'react';
import {useMutation} from 'react-apollo';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {ApplicationSortCriteria} from '~/flux/applications';
import {
  GetApplicationsApplications,
  GetApplicationsCountQuery,
  Maybe,
  Mutation,
  MutationSetApplicationAsViewedArgs,
} from '~/graphql/__generated__/types';
import {GET_APPLICATIONS_COUNT, SET_APPLICATION_AS_VIEWED} from '~/graphql/applications/applications.query';
import {isTopMatch} from '~/services/scores/isTopMatch';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';
import {ApplicantCheckbox} from '~/components/Applications/ApplicantCheckbox';
import {useDownloadResumeMutation} from '~/services/applications/useDownloadResumeMutation';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {formatISODate} from '~/util/date';

export interface IApplicationListProps {
  applicationsIdChecked: Array<string>;
  applications: Array<Maybe<GetApplicationsApplications>>;
  invitedTalents: IEmailInvitation[];
  selectedApplication?: GetApplicationsApplications;
  onSelectApplication: (application: GetApplicationsApplications, positionIndex: number) => void;
  sortCriteria: ApplicationSortCriteria;
  onSelectTopMatchApplication: () => void;
  onApplicationCandidateResumeClicked: (gaLabel: DOWNLOAD_RESUME_GA_LABEL) => void;
  onApplicationCheckboxChanged: (checkboxChecked: boolean, candidateId: string) => void;
  isCheckboxChecked?: boolean;
}

export const ApplicationList: React.FunctionComponent<IApplicationListProps> = ({
  applicationsIdChecked,
  applications,
  selectedApplication,
  invitedTalents,
  onSelectApplication,
  onSelectTopMatchApplication,
  onApplicationCandidateResumeClicked,
  onApplicationCheckboxChanged,
}) => {
  const [setApplicationAsViewed] = useMutation<
    {setApplicationAsViewed: Mutation['setApplicationAsViewed']},
    MutationSetApplicationAsViewedArgs
  >(SET_APPLICATION_AS_VIEWED);
  const setResumeLastDownloaded = useDownloadResumeMutation();
  return (
    <ul className="ma0 pl3 pr1 list" data-cy="applications-list">
      {applications.map((application, index) => {
        const key = application?.id ?? index;
        if (!application) {
          return <MissingCandidate key={key} candidateType="application" />;
        }
        const {applicant, scores, isShortlisted, isViewed, statusId} = application;
        const isInvitedTalent = invitedTalents.some(
          (invitedTalent) => invitedTalent.individualId === application.applicant.id,
        );
        const status = getApplicationStatusWithShortlisted(statusId, isShortlisted);
        const isSelected = selectedApplication ? selectedApplication.id === application.id : false;
        const appliedOn = `Applied on ${formatISODate(application.createdOn)}`;
        const updateApplicationCountCache = (
          cache: DataProxy,
          mutationResult: {data?: Maybe<{setApplicationAsViewed?: Mutation['setApplicationAsViewed']}>},
        ) => {
          if (mutationResult.data?.setApplicationAsViewed?.isViewed) {
            const query = GET_APPLICATIONS_COUNT;
            const variables = {jobId: application.job.uuid};
            const data = cache.readQuery({
              query,
              variables,
            }) as GetApplicationsCountQuery;
            data!.applicationsForJob!.unviewedTotal -= 1;
            cache.writeQuery({
              data,
              query,
              variables,
            });
          }
        };
        return (
          <ErrorBoundary key={key} fallback={<MissingCandidate candidateType="application" />}>
            <li className="flex items-center">
              <ApplicantCheckbox
                onApplicantCheckboxChanged={(event) => {
                  onApplicationCheckboxChanged(event.target.checked, application.id);
                }}
                isApplicantCheckboxChecked={applicationsIdChecked.includes(application.id)}
              />
              <CandidatesListItem
                selected={isSelected}
                candidate={applicant}
                candidateType={CandidateType.Applicant}
                isViewed={isViewed}
                key={key}
                isBookmarked={!!application.bookmarkedOn}
                date={appliedOn}
                showTopMatch={isTopMatch(scores)}
                onClick={() => {
                  if (!isViewed) {
                    setApplicationAsViewed({
                      variables: {applicationId: application.id},
                      update: updateApplicationCountCache,
                    });
                  }
                  onSelectApplication(application, index);
                  if (isTopMatch(scores)) {
                    onSelectTopMatchApplication();
                  }
                }}
                onResumeClick={() => {
                  setResumeLastDownloaded(application, {update: updateApplicationCountCache});
                  onApplicationCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST);
                  onSelectApplication(application, index);
                }}
                applicationStatus={status}
                isInvitedTalent={isInvitedTalent}
              />
            </li>
          </ErrorBoundary>
        );
      })}
    </ul>
  );
};
