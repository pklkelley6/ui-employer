import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import React from 'react';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {ApplicationList} from '~/components/Applications/ApplicationList';
import {ApplicationSortCriteria} from '~/flux/applications';
import {SET_APPLICATION_AS_VIEWED} from '~/graphql/applications/applications.query';

const mutationMocks = [
  {
    request: {
      query: SET_APPLICATION_AS_VIEWED,
      variables: {
        applicationId: 'I-0',
      },
    },
    result: {
      data: {
        setApplicationAsViewed: {id: 'I-0', isViewed: true},
      },
    },
  },
];

const store = configureStore()({features: {}});

describe('Applications/ApplicationList', () => {
  it('should render applications in ApplicationList', () => {
    const applicationListProps = {
      applications: [applicationMock],
      applicationsIdChecked: [],
      invitedTalents: [],
      onApplicationCandidateResumeClicked: jest.fn(),
      onResumeClick: jest.fn(),
      onSelectApplication: jest.fn(),
      onSelectTopMatchApplication: jest.fn(),
      onCheckboxChanged: jest.fn(),
      sortCriteria: ApplicationSortCriteria.CREATED_ON,
      onApplicationCheckboxChanged: jest.fn(),
    };
    const wrapper = mount(
      <MockedProvider mocks={mutationMocks} addTypename={false}>
        <Router>
          <Provider store={store}>
            <ApplicationList {...applicationListProps} />
          </Provider>
        </Router>
      </MockedProvider>,
    );

    expect(wrapper.find('[data-cy="candidates-list-item"]')).toHaveLength(applicationListProps.applications.length);
  });
});
