import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount, ReactWrapper} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {mcf} from '@mcf/constants';
import {print} from 'graphql';
import {Applications} from '../Applications';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {Dropdown, IDefaultDropdownProps} from '~/components/Core/Dropdown';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {ApplicationsToolBar} from '~/components/ApplicationsToolBar/ApplicationsToolBar';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {EmptyApplicationList} from '~/components/Applications';
import {EmptyFilteredApplicationList} from '~/components/Applications/EmptyFilteredApplicationList';
import {Pagination} from '~/components/Navigation/Pagination';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';
import {ApplicationSortCriteria, CANDIDATES_SORT_CRITERIA} from '~/flux/applications';
import {SortDirection} from '~/graphql/__generated__/types';
import {APPLICATIONS_LIMIT, GET_APPLICATIONS} from '~/graphql/applications';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {IAppState} from '~/flux';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';

describe('Applications', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should render a loader when loading and then Applications view after data is received', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: Matchers.eachLike(transformArrayToEachLikeMatcher(applicationMock)),
          total: Matchers.like(APPLICATIONS_LIMIT * 2),
        },
      },
    };

    const getApplicationsQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getApplications')
      .withQuery(print(GET_APPLICATIONS))
      .withOperation('getApplications')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables(baseVariables)
      .willRespondWith({
        body: queryMockResult,
        status: 200,
      });
    await pactBuilder.provider.addInteraction(getApplicationsQuery);

    const wrapper = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <Provider store={store}>
          <Router>
            <OnboardingContext.Provider value={onboardingContextProps}>
              <Applications {...applicationsProps} />
            </OnboardingContext.Provider>
          </Router>
        </Provider>
      </ApolloProvider>,
    );

    expect(wrapper.find(CardLoader)).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(1);
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render CandidatesFetchError when a network error is received', async () => {
    const queryMocks = [
      {
        error: new Error(),
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const candidatesFetchError = wrapper.find(TemporarilyUnavailable);

    expect(candidatesFetchError).toHaveLength(1);
    expect(candidatesFetchError).toMatchSnapshot();
  });

  it('should render EmptyApplications when there are zero applications', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const emptyApplicationList = wrapper.find(EmptyApplicationList);

    expect(emptyApplicationList).toHaveLength(1);
    expect(emptyApplicationList).toMatchSnapshot();
  });

  it('should render EmptyFilteredApplicationList when there is > 0 filters active and there are zero application results', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: {...baseVariables, filterBy: [{field: 'statusId', intArrayValue: [1]}]},
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper, 100);

    wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

    act(() => {
      wrapper.find('#applicant-status-selection-checkbox-1').simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper, 100);

    act(() => {
      wrapper.find('[data-cy="filter-modal-show-results"]').simulate('click');
    });

    await nextTick(wrapper);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const emptyFilteredApplicationList = wrapper.find(EmptyFilteredApplicationList);

    expect(emptyFilteredApplicationList).toHaveLength(1);
    expect(emptyFilteredApplicationList).toMatchSnapshot();
  });

  it('should render errors for null values and render the rest of applications', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);
    await nextTick(wrapper, 100);

    const applicationsToolbar = wrapper.find(ApplicationsToolBar);
    expect(applicationsToolbar).toHaveLength(1);

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(2);
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();

    const missingApplications = wrapper.find(MissingCandidate).find('[candidateType="application"]');
    expect(missingApplications).toHaveLength(2);
    expect(toJson(missingApplications, cleanSnapshot())).toMatchSnapshot();
  });

  it('should fetch applications and navigate to another page when pagination is clicked', async () => {
    const wrapper = mountApplicationsComponent();
    await nextTick(wrapper);
    const pagination = wrapper.find(Pagination);
    expect(pagination.find('.bg-secondary').text()).toEqual('1');

    const secondPage = pagination.findWhere((node) => node.type() === 'span' && node.text() === '2');
    secondPage.simulate('click');

    await nextTick(wrapper);

    const pageChange = wrapper.find(Pagination);
    expect(pageChange.find('.bg-secondary').text()).toEqual('2');
  });

  it('should set all individual application checkbox to false when sortBy is changed', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper, 100);

    act(() => {
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(0)
        .simulate('change', {target: {checked: true}});
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(1)
        .simulate('change', {target: {checked: true}});
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(2)
        .simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    const dropdown = wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown);
    const dropdownInput = dropdown.prop('input')!;

    act(() => {
      dropdownInput.onChange!(ApplicationSortCriteria.CREATED_ON);
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(0).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(1).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(2).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="mass-application-status-select-all-text"]')).toHaveLength(1);
  });

  it('should check all applcation checkbox if the overall checkbox is checked', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="mass-application-selection-checkbox"]').simulate('click');
    });

    await nextTick(wrapper);

    Array.from(Array(20).keys()).forEach((key) =>
      expect(wrapper.find(`[data-cy="candidate-checkbox"]`).at(key).prop('checked')).toEqual(true),
    );
    expect(wrapper.find('[data-cy="mass-application-status-update-button"]')).toHaveLength(1);
  });

  it('should set all individual application checkbox to false after overall checkbox change to false', async () => {
    const queryMockResult = {
      data: {
        applicationsForJob: {
          applications: [...twentyOneApplicationsMock, null, null],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    };

    const queryMocks = [
      {
        request: {
          query: GET_APPLICATIONS,
          variables: baseVariables,
        },
        result: queryMockResult,
      },
    ];

    const wrapper = mountApplicationsComponent(queryMocks);

    await nextTick(wrapper);

    act(() => {
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(0)
        .simulate('change', {target: {checked: true}});
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(1)
        .simulate('change', {target: {checked: true}});
      wrapper
        .find('[data-cy="candidate-checkbox"]')
        .at(2)
        .simulate('change', {target: {checked: true}});
    });

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="mass-application-selection-checkbox"]').simulate('click');
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(0).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(1).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="candidate-checkbox"]').at(2).prop('checked')).toEqual(false);
    expect(wrapper.find('[data-cy="mass-application-status-select-all-text"]')).toHaveLength(1);
  });

  describe('deprecated topmatch checkbox test when release toggle is false', () => {
    const reduxState = {
      releaseToggles: {'174262411TopMatchInFilterModal': false},
      features: {
        applicationStatusInFilterModal: false,
      },
      router: {
        location: {
          pathname: 'R90SS0001A(4)/applications',
        },
      },
    };

    const store = configureStore()(reduxState);

    it('should fetch applications and set isTopMatchCheckedDeprecated to false when sorting change', async () => {
      const wrapper = mount(
        <MockedProvider mocks={defaultQueryMocks} addTypename={false}>
          <Provider store={store}>
            <Router>
              <OnboardingContext.Provider value={onboardingContextProps}>
                <Applications {...applicationsProps} />
              </OnboardingContext.Provider>
            </Router>
          </Provider>
        </MockedProvider>,
      );

      await nextTick(wrapper);

      const dropdown = wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown);
      const dropdownInput = dropdown.prop('input')!;
      act(() => {
        dropdownInput.onChange!(ApplicationSortCriteria.SCORES_WCC);
      });
      await nextTick(wrapper);

      expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);
    });

    it('should call fetchMoreApplications with filtered applications when sorting changes and isTopMatchCheckedDeprecated true', async () => {
      const wrapper = mount(
        <MockedProvider mocks={defaultQueryMocks} addTypename={false}>
          <Provider store={store}>
            <Router>
              <OnboardingContext.Provider value={onboardingContextProps}>
                <Applications {...applicationsProps} />
              </OnboardingContext.Provider>
            </Router>
          </Provider>
        </MockedProvider>,
      );

      await nextTick(wrapper);

      act(() => {
        wrapper.find('[data-cy="top-match-checkbox"]').simulate('change', {target: {checked: true}});
      });

      await nextTick(wrapper, 100);

      expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(true);

      expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
      expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();

      act(() => {
        wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown).prop('input')!.onChange!(
          ApplicationSortCriteria.SCORES_WCC,
        );
      });

      await nextTick(wrapper);

      expect(wrapper.find(Dropdown).prop('input')!.value).toEqual(ApplicationSortCriteria.SCORES_WCC);
      expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);

      act(() => {
        wrapper.find('[data-cy="top-match-checkbox"]').simulate('change', {target: {checked: true}});
      });

      await nextTick(wrapper);

      expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(true);
      expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
      expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();

      act(() => {
        wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown).prop('input')!.onChange!(
          ApplicationSortCriteria.SCORES_WCC,
        );
      });

      await nextTick(wrapper);

      expect(wrapper.find(Dropdown).prop('input')!.value).toEqual(ApplicationSortCriteria.SCORES_WCC);
      expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('checked')).toEqual(false);
      expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
      expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();
    });
  });

  describe('Filters', () => {
    describe('undo filters', () => {
      it('should toggle off topmatch switch toggle, uncheck all applicant status checkboxes and toggle off screeningQuestions toggle when reset all filters is clicked', async () => {
        const wrapper = mountApplicationsComponent();

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').simulate('change', {target: {checked: true}});

          wrapper
            .find('[data-cy="screening-questions-switch-toggle-checkbox"]')
            .simulate('change', {target: {checked: true}});

          wrapper
            .find('[data-cy="applicant-status-selection-checkbox"]')
            .at(0)
            .simulate('change', {target: {checked: true}});
          wrapper
            .find('[data-cy="applicant-status-selection-checkbox"]')
            .at(1)
            .simulate('change', {target: {checked: true}});
        });

        await act(async () => {
          wrapper.find('[data-cy="filter-modal-reset-all-filters"]').simulate('click');
        });

        await nextTick(wrapper);

        expect(wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').prop('checked')).toEqual(false);

        expect(wrapper.find('[data-cy="screening-questions-switch-toggle-checkbox"]').prop('checked')).toEqual(false);

        expect(wrapper.find('[data-cy="applicant-status-selection-checkbox"]').at(0).prop('checked')).toEqual(false);
        expect(wrapper.find('[data-cy="applicant-status-selection-checkbox"]').at(1).prop('checked')).toEqual(false);
      });

      it('should undo applicant status checkedboxes if checked, when cancel is clicked', async () => {
        const wrapper = mountApplicationsComponent();

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper
            .find('[data-cy="applicant-status-selection-checkbox"]')
            .at(0)
            .simulate('change', {target: {checked: true}});
          wrapper
            .find('[data-cy="applicant-status-selection-checkbox"]')
            .at(1)
            .simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-cancel"]').simulate('click');
        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        expect(wrapper.find('[data-cy="applicant-status-selection-checkbox"]').at(0).prop('checked')).toEqual(false);
        expect(wrapper.find('[data-cy="applicant-status-selection-checkbox"]').at(1).prop('checked')).toEqual(false);
      });

      it('should undo topmatch toggle state if changed, when cancel is clicked', async () => {
        const wrapper = mountApplicationsComponent();

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').prop('checked')).toBe(false);

        act(() => {
          wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-cancel"]').simulate('click');
        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        expect(wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').prop('checked')).toEqual(false);
      });
      it('should undo screening questions toggle if changed when cancel is clicked', async () => {
        const wrapper = mountApplicationsComponent();

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        expect(wrapper.find('[data-cy="screening-questions-switch-toggle-checkbox"]').prop('checked')).toBe(false);

        act(() => {
          wrapper
            .find('[data-cy="screening-questions-switch-toggle-checkbox"]')
            .simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-cancel"]').simulate('click');
        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        expect(wrapper.find('[data-cy="screening-questions-switch-toggle-checkbox"]').prop('checked')).toEqual(false);
      });

      it('should reset screening questions to default yes when reset is clicked', async () => {
        const wrapper = mountApplicationsComponent();

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper
            .find('[data-cy="screening-questions-switch-toggle-checkbox"]')
            .simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper);

        act(() => {
          wrapper.find('[data-cy="question-1-no"]').simulate('change', {target: {value: 'no'}});
        });

        await nextTick(wrapper);

        act(() => {
          wrapper.find('[data-cy="filter-modal-reset-all-filters"]').simulate('click');
        });

        await nextTick(wrapper);

        expect(wrapper.find('[data-cy="screening-questions-switch-toggle-checkbox"]').prop('checked')).toEqual(false);

        act(() => {
          wrapper
            .find('[data-cy="screening-questions-switch-toggle-checkbox"]')
            .simulate('change', {target: {checked: true}});
        });
        await nextTick(wrapper);

        expect(wrapper.find('[data-cy="question-1-yes"]').prop('checked')).toEqual(true);
      });
    });
    describe('fetch filtered list', () => {
      it('should fetch topmatch candidates when topmatch switch toggle is ON', async () => {
        const wrapper = mountApplicationsComponent(defaultQueryMocks);

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper, 100);

        act(() => {
          wrapper.find('[data-cy="filter-modal-show-results"]').simulate('click');
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').prop('checked')).toEqual(true);
        expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
        expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();
        expect(wrapper.find('[data-cy="filtered-applications-results-total"]').text()).toEqual(
          `${defaultQueryMockResult.data.applicationsForJob.total} results found`,
        );
      });
      it('should fetch candidates corresponding to selected statuses', async () => {
        const applicationsWithDifferentStatusIds = [
          {
            ...applicationMock,
            statusId: 3,
          },
        ];
        const filteredStatusQueryMockResult = {
          data: {
            applicationsForJob: {
              applications: applicationsWithDifferentStatusIds,
              total: APPLICATIONS_LIMIT * 2,
            },
          },
        };

        const queryMocks = [
          {
            request: {
              query: GET_APPLICATIONS,
              variables: baseVariables,
            },
            result: defaultQueryMockResult,
          },
          {
            request: {
              query: GET_APPLICATIONS,
              variables: {...baseVariables, filterBy: [{field: 'statusId', intArrayValue: [3]}]},
            },
            result: filteredStatusQueryMockResult,
          },
        ];

        const wrapper = mountApplicationsComponent(queryMocks);

        await nextTick(wrapper, 100);

        expect(wrapper.find(CandidatesListItem)).toHaveLength(2);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper.find('#applicant-status-selection-checkbox-3').simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper, 100);

        act(() => {
          wrapper.find('[data-cy="filter-modal-show-results"]').simulate('click');
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find('#applicant-status-selection-checkbox-3').prop('checked')).toEqual(true);
        expect(wrapper.find(CandidatesListItem)).toHaveLength(1);
        expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();
        expect(wrapper.find('[data-cy="filtered-applications-results-total"]').text()).toEqual(
          `${filteredStatusQueryMockResult.data.applicationsForJob.total} results found`,
        );
      });

      it('should fetch candidates corresponding to selected screening question responses', async () => {
        const applicationsWithScreeningQuestionResponses = [
          {
            ...applicationMock,
            jobScreeningQuestionResponses: [
              {question: 'This is first question', answer: mcf.SCREEN_QUESTION_RESPONSE.YES},
              {question: 'This is second question', answer: mcf.SCREEN_QUESTION_RESPONSE.YES},
            ],
          },
        ];

        const filteredScreenQuestionResponseQueryMockResult = {
          data: {
            applicationsForJob: {
              applications: applicationsWithScreeningQuestionResponses,
              total: APPLICATIONS_LIMIT * 2,
            },
          },
        };

        const queryMocks = [
          {
            request: {
              query: GET_APPLICATIONS,
              variables: baseVariables,
            },
            result: defaultQueryMockResult,
          },
          {
            request: {
              query: GET_APPLICATIONS,
              variables: {
                ...baseVariables,
                filterBy: [
                  {
                    field: 'jobScreeningQuestionResponses',
                    stringArrayValue: [mcf.SCREEN_QUESTION_RESPONSE.YES, mcf.SCREEN_QUESTION_RESPONSE.YES],
                  },
                ],
              },
            },
            result: filteredScreenQuestionResponseQueryMockResult,
          },
        ];

        const wrapper = mountApplicationsComponent(queryMocks);

        await nextTick(wrapper, 100);

        expect(wrapper.find(CandidatesListItem)).toHaveLength(2);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');

        act(() => {
          wrapper
            .find('[data-cy="screening-questions-switch-toggle-checkbox"]')
            .simulate('change', {target: {checked: true}});
        });

        await nextTick(wrapper, 100);

        act(() => {
          wrapper.find('[data-cy="filter-modal-show-results"]').simulate('click');
        });

        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find(CandidatesListItem)).toHaveLength(1);
      });

      it('should fetch applications when filters are applied and be able to sort the existing filtered list without resetting the filters', async () => {
        const applicationWithTopMatchScore = [
          {
            ...applicationMock,
            id: 'wcc-1',
            name: 'top-matcher',
            scores: {
              wcc: 0.9,
            },
          },
        ];

        const filteredQueryMockResult = {
          data: {
            applicationsForJob: {
              applications: [...applicationsMock, ...applicationWithTopMatchScore],
              total: APPLICATIONS_LIMIT * 2,
            },
          },
        };

        const queryMocks = [
          {
            request: {
              query: GET_APPLICATIONS,
              variables: baseVariables,
            },
            result: filteredQueryMockResult,
          },
          {
            request: {
              query: GET_APPLICATIONS,
              variables: {
                ...baseVariables,
                filterBy: [{field: 'scores.wcc', floatValue: 0.6}],
              },
            },
            result: filteredQueryMockResult,
          },
          {
            request: {
              query: GET_APPLICATIONS,
              variables: {
                ...baseVariables,
                filterBy: [{field: 'scores.wcc', floatValue: 0.6}],
                sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
              },
            },
            result: filteredQueryMockResult,
          },
        ];
        const wrapper = mountApplicationsComponent(queryMocks);
        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').simulate('change', {target: {checked: true}});

        act(() => {
          wrapper.find('[data-cy="filter-modal-show-results"]').simulate('click');
        });

        await nextTick(wrapper);

        const dropdown = wrapper.find<IDefaultDropdownProps<typeof CANDIDATES_SORT_CRITERIA[number]>>(Dropdown);
        const dropdownInput = dropdown.prop('input')!;
        act(() => {
          dropdownInput.onChange!(ApplicationSortCriteria.SCORES_WCC);
        });
        await nextTick(wrapper);

        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find('[data-cy="top-match-switch-toggle-checkbox"]').prop('checked')).toEqual(true);
        expect(wrapper.find(CandidatesListItem)).toHaveLength(3);
        expect(wrapper.find(CandidatesListItem)).toMatchSnapshot();
      });

      it('should not display screening questions toggle if there are no screening questions set for the job', async () => {
        const applicationPropsWithoutScreeningQuestions = {
          ...applicationsProps,
          job: {...applicationsProps.job, screeningQuestions: []},
        };
        const wrapper = mount(
          <MockedProvider mocks={defaultQueryMocks} addTypename={false}>
            <Provider store={store}>
              <Router>
                <OnboardingContext.Provider value={onboardingContextProps}>
                  <Applications {...applicationPropsWithoutScreeningQuestions} />
                </OnboardingContext.Provider>
              </Router>
            </Provider>
          </MockedProvider>,
        );

        await nextTick(wrapper, 100);
        wrapper.find('[data-cy="filter-modal-button"]').simulate('click');
        expect(wrapper.find('[data-cy="screeningQuestionResponse-switch-toggle"]')).toHaveLength(0);
      });
    });
  });
});

const applicationsProps = {
  job: {
    ...jobPostMock,
    uuid: 'R90SS0001A(4)',
    screeningQuestions: [{question: 'This is first question'}, {question: 'This is second question'}],
  },
  jobUuid: 'R90SS0001A(4)',
  onApplicationCandidateResumeClicked: jest.fn(),
  onApplicationCandidateTabClicked: jest.fn(),
  onApplicationClicked: jest.fn(),
  onApplicationPageClicked: jest.fn(),
  onApplicationSortingTypeClicked: jest.fn(),
  onApplicationStatusFilter: jest.fn(),
  onApplicationTopMatchFilter: jest.fn(),
  onApplicationsFilter: jest.fn(),
  onApplicationTopMatchLabelClicked: jest.fn(),
  onMassUpdateApplicationsSelected: jest.fn(),
  onMassUpdateApplicationsStatusUpdated: jest.fn(),
  removeCandidateIdSelection: jest.fn(),
  addCandidateIdSelection: jest.fn(),
  resetCandidateIdSelection: jest.fn(),
  getApplicationTopMatchTotal: jest.fn(),
};

const applicationsMock = [
  {
    ...applicationMock,
    id: 'I-1',
  },
  {
    ...applicationMock,
    id: 'I-2',
  },
];

const twentyOneApplicationsMock = Array.from(Array(20).keys()).map((keys) => {
  return {
    ...applicationMock,
    id: `I-${keys}`,
  };
});

const wccApplicationsMock = [
  {
    ...applicationMock,
    id: 'wcc-1',
  },
];

const baseVariables = {
  jobId: applicationsProps.jobUuid,
  limit: APPLICATIONS_LIMIT,
  skip: 0,
  sortBy: [{field: ApplicationSortCriteria.CREATED_ON, direction: SortDirection.Desc}],
};

const onboardingContextProps = {
  isApplicationViewed: true,
  setApplicationViewed: jest.fn(),
};

const defaultQueryMockResult = {
  data: {
    applicationsForJob: {
      applications: applicationsMock,
      total: APPLICATIONS_LIMIT * 2,
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_APPLICATIONS,
      variables: baseVariables,
    },
    result: defaultQueryMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        skip: APPLICATIONS_LIMIT,
      },
    },
    result: defaultQueryMockResult,
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [{field: 'scores.wcc', floatValue: 0.6}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [{field: 'scores.wcc', floatValue: 0.6}],
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
  {
    request: {
      query: GET_APPLICATIONS,
      variables: {
        ...baseVariables,
        filterBy: [{field: 'scores.wcc', floatValue: 0.6}],
        sortBy: [{field: ApplicationSortCriteria.SCORES_WCC, direction: SortDirection.Desc}],
      },
    },
    result: {
      data: {
        applicationsForJob: {
          applications: [...applicationsMock, ...wccApplicationsMock],
          total: APPLICATIONS_LIMIT * 2,
        },
      },
    },
  },
];

const reduxState: {router: {location: Pick<IAppState['router']['location'], 'pathname'>}} & {
  releaseToggles: Partial<IAppState['releaseToggles']>;
  features: Partial<IAppState['features']>;
} = {
  router: {
    location: {
      pathname: `${applicationsProps.jobUuid}/applications`,
    },
  },
  releaseToggles: {
    '174262411TopMatchInFilterModal': true,
    '177657666filterSortEmptyScoresApplications': true,
  },
  features: {
    applicationStatusInFilterModal: true,
  },
};
const store = configureStore()(reduxState);

const mountApplicationsComponent = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  return mount(
    <MockedProvider mocks={queryMocks} addTypename={false}>
      <Provider store={store}>
        <Router>
          <OnboardingContext.Provider value={onboardingContextProps}>
            <Applications {...applicationsProps} />
          </OnboardingContext.Provider>
        </Router>
      </Provider>
    </MockedProvider>,
  );
};
