import {formatISO} from 'date-fns';
import {useSelector} from 'react-redux';
import {useAsync} from 'react-use';
import {isEmpty, flow} from 'lodash/fp';
import React, {useState, useEffect} from 'react';
import {useQuery, useMutation} from 'react-apollo';
import {LoaderModal} from '../LoaderModal/LoaderModal';
import {ApplicationRejectionReason} from '../ApplicationRejectionReason/ApplicationRejectionReason';
import {IApplicationsProps} from './ApplicationsContainer';
import {EmptyFilteredApplicationList} from './EmptyFilteredApplicationList';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {ApplicationList, EmptyApplication, EmptyApplicationList} from '~/components/Applications';
import {ApplicationsToolBar} from '~/components/ApplicationsToolBar/ApplicationsToolBar';
import {ApplicationStatus} from '~/components/CandidateInfo/ApplicationStatus';
import {CandidateInfo} from '~/components/CandidateInfo/CandidateInfo';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {Dropdown} from '~/components/Core/Dropdown';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {TopMatchCheckboxDeprecatedContainer} from '~/components/Filters/TopMatchCheckbox/TopMatchCheckboxContainer';
import {FilterApplicationsModal} from '~/components/Filters/FilterApplicationsModal/FilterApplicationsModal';
import {FilterApplicationsButton} from '~/components/Filters/FilterApplicationsModal/FilterApplicationsButton';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {Pagination} from '~/components/Navigation/Pagination';
import {ApplicationsOnboarding} from '~/components/Onboarding/ApplicationsOnboarding';
import {MassCheckboxWithUpdateStatusButton} from '~/components/MassApplicationStatusUpdate/MassApplicationStatusUpdate';
import {ApplicationSortCriteria, CANDIDATES_SORT_CRITERIA, TOP_MATCH_WCC_SCORE} from '~/flux/applications';
import {
  GetApplicationsApplications,
  GetApplicationsApplicationsForJob,
  GetApplicationsQuery,
  GetApplicationsQueryVariables,
  SortDirection,
  Mutation,
  MutationSetApplicationBookmarkedOnArgs,
} from '~/graphql/__generated__/types';
import {APPLICATIONS_LIMIT, GET_APPLICATIONS, SET_APPLICATION_BOOKMARK} from '~/graphql/applications';
import {isTopMatch} from '~/services/scores/isTopMatch';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {MassApplicationStatusUpdateModal} from '~/components/MassApplicationStatusUpdate/MassApplicationStatusUpdateModal';
import {useDownloadResumeMutation} from '~/services/applications/useDownloadResumeMutation';
import {isSelectedApplicationStatusUnsuccessful} from '~/util/isSelectedApplicationStatusUnsuccessful';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {getEmailInvitations} from '~/services/suggestedTalents/getEmailInvitations';
import {formatISODate} from '~/util/date';
import {IAppState} from '~/flux/index';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

export interface IApplicationsState {
  page: number;
  selectedApplicationId?: string;
  sortCriteria: ApplicationSortCriteria;
  isTopMatchCheckedDeprecated: boolean;
}

export const Applications: React.FunctionComponent<IApplicationsProps> = ({
  jobUuid,
  job,
  onApplicationClicked,
  onApplicationTopMatchLabelClicked,
  onApplicationPageClicked,
  onApplicationCandidateResumeClicked,
  onApplicationTopMatchFilter,
  onApplicationsFilter,
  onApplicationSortingTypeClicked,
  onApplicationCandidateTabClicked,
  refetchBookmarkedCandidatesTotal,
  onMassUpdateApplicationsSelected,
  onMassUpdateApplicationsStatusUpdated,
}) => {
  const [applicationsIdChecked, setApplicationsIdChecked] = useState<Array<string>>([]);
  const [isTopMatchCheckedDeprecated, setIsTopMatchCheckedDeprecated] = useState<boolean>(false);
  const [page, setPage] = useState(0);
  const [sortCriteria, setSortCriteria] = useState(ApplicationSortCriteria.CREATED_ON);
  const [applications, setApplications] = useState<NonNullable<GetApplicationsApplicationsForJob['applications']>>([]);
  const [selectedApplication, setSelectedApplication] = useState<GetApplicationsApplications>();
  const [previousApplicationState, setPreviousApplicationState] = useState<GetApplicationsApplications>();
  const [displayMassUpdateModal, setDisplayMassUpdateModal] = useState(false);
  const [invitedTalents, setInvitedTalents] = useState<IEmailInvitation[]>([]);
  const [isTopMatchFiltered, setIsTopMatchFiltered] = useState<boolean>(false);
  const [applicantStatusFilters, setApplicantStatusFilters] = useState<Array<number>>([]);

  const [screeningQuestionsResponsesFilter, setScreeningQuestionsResponsesFilter] = useState<Array<string>>([]);

  const [isFilterModalDisplayed, setIsFilterModalDisplayed] = useState<boolean>(false);
  const [displayLoaderModal, setDisplayLoaderModal] = useState<boolean>(false);
  const [shouldFetchApplications, setShouldFetchApplications] = useState<boolean>(false);

  const numberOfActiveFilters = [
    isTopMatchFiltered,
    !isEmpty(applicantStatusFilters),
    !isEmpty(screeningQuestionsResponsesFilter),
  ].filter((x) => x).length;

  const [setApplicationBookmark] = useMutation<
    {setApplicationBookmark: Mutation['setApplicationBookmarkedOn']},
    MutationSetApplicationBookmarkedOnArgs
  >(SET_APPLICATION_BOOKMARK);

  const setResumeLastDownloaded = useDownloadResumeMutation();

  const filterSortEmptyScoresApplicationsReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['177657666filterSortEmptyScoresApplications'],
  );

  const topMatchFilterReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['174262411TopMatchInFilterModal'],
  );

  const applicantStatusFiltersReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['174262411TopMatchInFilterModal'],
  );
  const applicantStatusFiltersFeatureToggle = useSelector(
    ({features}: IAppState) => features.applicationStatusInFilterModal,
  );

  const deprecatedTopMatchFilter = isTopMatchCheckedDeprecated
    ? {filterBy: [{field: ApplicationSortCriteria.SCORES_WCC, floatValue: TOP_MATCH_WCC_SCORE}]}
    : {};

  const filterQueryVariables = ({
    isTopMatchFiltered,
    applicantStatusFilters,
    screeningQuestionsResponsesFilter,
  }: {
    isTopMatchFiltered: any;
    applicantStatusFilters: any;
    screeningQuestionsResponsesFilter: any;
  }) => {
    if (!isTopMatchFiltered && isEmpty(applicantStatusFilters) && isEmpty(screeningQuestionsResponsesFilter)) {
      return {};
    }

    return {
      filterBy: flow([
        (filters) => {
          return isTopMatchFiltered
            ? [...filters, {field: ApplicationSortCriteria.SCORES_WCC, floatValue: TOP_MATCH_WCC_SCORE}]
            : filters;
        },
        (filters) => {
          return !isEmpty(applicantStatusFilters)
            ? [...filters, {field: 'statusId', intArrayValue: applicantStatusFilters}]
            : filters;
        },
        (filters) => {
          return !isEmpty(screeningQuestionsResponsesFilter)
            ? [
                ...filters,
                {
                  field: 'jobScreeningQuestionResponses',
                  stringArrayValue: screeningQuestionsResponsesFilter,
                },
              ]
            : filters;
        },
      ])([]),
    };
  };

  const {
    loading,
    error,
    data,
    refetch: refetchApplications,
  } = useQuery<GetApplicationsQuery, GetApplicationsQueryVariables>(GET_APPLICATIONS, {
    variables: {
      jobId: jobUuid,
      limit: APPLICATIONS_LIMIT,
      skip: page * APPLICATIONS_LIMIT,
      sortBy: [{field: sortCriteria, direction: SortDirection.Desc}],
      ...(topMatchFilterReleaseToggle
        ? filterQueryVariables({isTopMatchFiltered, applicantStatusFilters, screeningQuestionsResponsesFilter})
        : deprecatedTopMatchFilter),
    },
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
    notifyOnNetworkStatusChange: true,
    context: {
      headers: filterSortEmptyScoresApplicationsReleaseToggle ? {} : {[API_VERSION_HEADER_KEY]: '2021-01-08'},
    },
    skip: !jobUuid,
  });

  // effect for when job posting number of screening questions changed e.g. removal of screening questions
  useEffect(() => {
    if (data) {
      // reset filters and go to first page
      setPage(0);
      setApplicantStatusFilters([]);
      setScreeningQuestionsResponsesFilter([]);
      setIsTopMatchFiltered(false);

      // fetch applications again
      setShouldFetchApplications(true);
    }
  }, [(job?.screeningQuestions ?? []).length]);

  useEffect(() => {
    // fetch applications again even if no filters changed https://www.pivotaltracker.com/story/show/174270844/comments/223218646
    if (shouldFetchApplications) {
      refetchApplications();
      setShouldFetchApplications(false);
    }
  }, [shouldFetchApplications]);

  useEffect(() => {
    setApplications(data?.applicationsForJob?.applications ?? []);
    // replace selected application with updated one if any mutation has changed it (e.g. status change)
    if (selectedApplication) {
      const updatedApplication = (data?.applicationsForJob?.applications ?? []).find(
        (application) => application?.id == selectedApplication.id,
      );
      if (updatedApplication) {
        setSelectedApplication(updatedApplication);
      }
    }
  }, [JSON.stringify(data)]);

  useEffect(() => {
    setSelectedApplication(undefined);
  }, [loading]);

  useEffect(() => {
    setApplications(
      applications.map((application) =>
        application?.id === selectedApplication?.id ? selectedApplication : application,
      ),
    );
  }, [JSON.stringify(selectedApplication)]);

  useEffect(() => {
    setApplications(
      applications.map((application) =>
        application?.id === previousApplicationState?.id ? previousApplicationState : application,
      ),
    );
    if (selectedApplication?.id === previousApplicationState?.id) {
      setSelectedApplication(previousApplicationState);
    }
  }, [JSON.stringify(previousApplicationState)]);

  const handleTopMatchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setApplicationsIdChecked([]);
    setIsTopMatchCheckedDeprecated(event.target.checked);
    onApplicationTopMatchFilter(event.target.checked);
    setPage(0);
  };

  const handleFilterChange = (
    isTopMatch: boolean,
    applicantStatusState: number[],
    screenQuestionResponses: string[],
  ) => {
    setApplicationsIdChecked([]);
    setIsTopMatchFiltered(isTopMatch);
    setApplicantStatusFilters(applicantStatusState);
    setPage(0);
    setShouldFetchApplications(true);
    setScreeningQuestionsResponsesFilter(screenQuestionResponses);
    if (isTopMatch || applicantStatusState.length || screenQuestionResponses.length) {
      onApplicationsFilter({
        isTopMatch,
        applicationStatus: applicantStatusState,
        screeningQuestions: screenQuestionResponses,
        jobId: jobUuid,
      });
    }
    setIsFilterModalDisplayed(false);
  };

  const handleSortCriteriaChange = (sortCriteria: ApplicationSortCriteria) => {
    setApplicationsIdChecked([]);
    setSortCriteria(sortCriteria);
    if (filterSortEmptyScoresApplicationsReleaseToggle) {
      setPage(0);
    } else {
      if (topMatchFilterReleaseToggle) {
        setIsTopMatchFiltered(false);
      } else {
        setIsTopMatchCheckedDeprecated(false);
      }
      if (applicantStatusFiltersFeatureToggle && applicantStatusFiltersReleaseToggle) {
        setApplicantStatusFilters([]);
      }
    }
    onApplicationSortingTypeClicked(sortCriteria);
  };

  const onApplicationCheckboxChanged = (isCheckboxChecked: boolean, candidateId: string) => {
    if (isCheckboxChecked) {
      setApplicationsIdChecked([...applicationsIdChecked, candidateId]);
    } else {
      const filteredApplicantsSelectionId = applicationsIdChecked.filter((id) => id !== candidateId);
      setApplicationsIdChecked(filteredApplicantsSelectionId);
    }
  };

  const applicationsTotal = data?.applicationsForJob?.total ?? 0;

  const rejectionComponent =
    selectedApplication && isSelectedApplicationStatusUnsuccessful(applications, selectedApplication) ? (
      <ApplicationRejectionReason
        applicationId={selectedApplication.id}
        applicationRejectReason={selectedApplication.rejectionReason ?? undefined}
        othersRejectReason={selectedApplication.rejectionReasonExtended ?? undefined}
      />
    ) : undefined;

  useAsync(async () => {
    const result = await getEmailInvitations(jobUuid);
    setInvitedTalents(result.filter((data) => data.isEmailSent === true));
  }, []);

  const applicationPanesProps = () => {
    const onCandidateBookmarkClick = async (checked: boolean) => {
      if (selectedApplication) {
        try {
          setSelectedApplication({
            ...selectedApplication,
            bookmarkedOn: checked ? formatISO(Date.now()) : null,
          });
          await setApplicationBookmark({
            variables: {
              applicationId: selectedApplication.id,
              isBookmark: checked,
            },
          });
          if (refetchBookmarkedCandidatesTotal) {
            await refetchBookmarkedCandidatesTotal();
          }
        } catch (error) {
          setPreviousApplicationState(selectedApplication);
          growlNotification(
            `Temporarily unable to ${checked ? 'save' : 'unsave'} candidate. Please try again.`,
            GrowlType.ERROR,
            {
              toastId: `setApplicationBookmarkId${selectedApplication.id}`,
            },
          );
        }
      }
    };
    return {
      left: (resetRightScroll: () => void) => (
        <>
          <ApplicationList
            applicationsIdChecked={applicationsIdChecked}
            applications={applications}
            invitedTalents={invitedTalents}
            onSelectApplication={(application, positionIndex) => {
              setSelectedApplication(application);
              onApplicationClicked(application, page * APPLICATIONS_LIMIT + positionIndex + 1);
              resetRightScroll();
            }}
            onSelectTopMatchApplication={onApplicationTopMatchLabelClicked}
            selectedApplication={selectedApplication}
            sortCriteria={sortCriteria}
            onApplicationCandidateResumeClicked={onApplicationCandidateResumeClicked}
            onApplicationCheckboxChanged={onApplicationCheckboxChanged}
          />
          <Pagination
            currentPage={page}
            totalItemsLength={applicationsTotal}
            itemsPerPage={APPLICATIONS_LIMIT}
            maxVisiblePageButtons={5}
            onPageChange={(page) => {
              setApplicationsIdChecked([]);
              setPage(page);
              onApplicationPageClicked(jobUuid, page + 1);
            }}
          />
        </>
      ),
      right: selectedApplication ? (
        // Added key prop to reset the ApplicationInfo selected tab on a different application
        <ErrorBoundary key={selectedApplication.id} fallback={<EmptyApplication />}>
          <CandidateInfo
            key={selectedApplication.id}
            candidate={selectedApplication.applicant}
            job={job}
            showTopMatch={isTopMatch(selectedApplication.scores)}
            formattedDate={`Applied on ${formatISODate(selectedApplication.createdOn)}`}
            isBookmarked={!!selectedApplication.bookmarkedOn}
            screeningQuestionResponses={selectedApplication.jobScreeningQuestionResponses}
            candidateType={CandidateType.Applicant}
            candidateTabClicked={onApplicationCandidateTabClicked}
            invitedTalents={invitedTalents}
            onResumeClick={() => {
              setResumeLastDownloaded(selectedApplication);
              onApplicationCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.DETAIL);
            }}
            statusSelector={
              <ApplicationStatus
                statusId={selectedApplication.statusId}
                isShortlisted={selectedApplication.isShortlisted}
                applicationId={selectedApplication.id}
                onLoading={() => {
                  setDisplayLoaderModal(true);
                }}
                onCompleted={() => {
                  setDisplayLoaderModal(false);
                }}
                onError={() => {
                  setDisplayLoaderModal(false);
                }}
              />
            }
            rejectionComponent={rejectionComponent}
            onCandidateBookmarkClick={onCandidateBookmarkClick}
          />
        </ErrorBoundary>
      ) : (
        <EmptyApplication />
      ),
    };
  };

  const displayApplicationsState = () => {
    if (loading) {
      return <CardLoader className="pa3 w-50 o-70" cardNumber={8} boxNumber={1} />;
    } else if (error?.networkError) {
      return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
    } else if (isEmpty(applications)) {
      if (numberOfActiveFilters > 0) {
        return (
          <EmptyFilteredApplicationList
            onFilterLinkClick={() => {
              setIsFilterModalDisplayed(true);
            }}
          />
        );
      }
      return <EmptyApplicationList />;
    } else {
      return (
        <>
          {numberOfActiveFilters > 0 ? (
            <div className="fw6 f5 black-50 ml4 mb4 pt3" data-cy="filtered-applications-results-total">
              {applicationsTotal} result{applicationsTotal === 1 ? '' : 's'} found
            </div>
          ) : null}
          <CandidatesPanes {...applicationPanesProps()} />
        </>
      );
    }
  };

  const massApplicationCheckboxClicked = () => {
    if (applicationsIdChecked.length === 0) {
      const applicationsIdSelected: string[] = [];
      applications.forEach((application) => {
        if (application) {
          applicationsIdSelected.push(application.id);
        }
      });
      setApplicationsIdChecked(applicationsIdSelected);
    } else {
      setApplicationsIdChecked([]);
    }
  };

  const hasApplications = applications.length > 0;

  const onMassApplicationStatusUpdateCompleted = () => {
    refetchApplications();
  };
  return (
    <>
      <ApplicationsToolBar
        massSelectionSlot={
          <MassCheckboxWithUpdateStatusButton
            onMassApplicationCheckboxClicked={massApplicationCheckboxClicked}
            isCheckboxDisabled={applications.length === 0}
            totalApplicationsChecked={applicationsIdChecked.length}
            isMassApplicationCheckboxChecked={hasApplications && applicationsIdChecked.length === applications.length}
            isMassApplicationCheckboxIndeterminate={
              hasApplications && applicationsIdChecked.length > 0 && applicationsIdChecked.length != applications.length
            }
            onMassApplicationButtonClicked={() => {
              onMassUpdateApplicationsSelected(jobUuid, applicationsIdChecked.length);
              setDisplayMassUpdateModal(true);
            }}
          />
        }
        sortSlot={
          <Dropdown
            data={CANDIDATES_SORT_CRITERIA}
            input={{
              onChange: handleSortCriteriaChange,
              value: sortCriteria,
            }}
          />
        }
        filterSlot={
          topMatchFilterReleaseToggle ? (
            <FilterApplicationsButton
              numberOfActiveFilters={numberOfActiveFilters}
              onClick={() => setIsFilterModalDisplayed(true)}
            />
          ) : (
            <TopMatchCheckboxDeprecatedContainer
              checked={isTopMatchCheckedDeprecated}
              sortCriteria={sortCriteria}
              onTopMatchChange={handleTopMatchChange}
            />
          )
        }
        onBoardingSlot={<ApplicationsOnboarding />}
      />
      <div data-cy="applications-view">{displayApplicationsState()}</div>
      {displayMassUpdateModal && (
        <MassApplicationStatusUpdateModal
          applicationIds={applicationsIdChecked}
          onCancel={() => setDisplayMassUpdateModal(false)}
          onSubmit={(applicationStatus) => onMassUpdateApplicationsStatusUpdated(jobUuid, applicationStatus)}
          onUpdateCompleted={onMassApplicationStatusUpdateCompleted}
        />
      )}
      {isFilterModalDisplayed && (
        <FilterApplicationsModal
          jobScreeningQuestions={job?.screeningQuestions}
          onCancel={() => setIsFilterModalDisplayed(false)}
          isTopMatchFiltered={isTopMatchFiltered}
          applicantStatusFilters={applicantStatusFilters}
          screeningQuestionsResponsesState={screeningQuestionsResponsesFilter}
          onHandleFilterChange={(topMatch, applicantStatus, screenQuestionResponse) => {
            handleFilterChange(topMatch, applicantStatus, screenQuestionResponse);
          }}
        />
      )}
      {displayLoaderModal && <LoaderModal />}
    </>
  );
};
