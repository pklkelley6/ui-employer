import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

interface IEmptyFilteredApplicationListProps {
  onFilterLinkClick: () => void;
}

export const EmptyFilteredApplicationList: React.FunctionComponent<IEmptyFilteredApplicationListProps> = ({
  onFilterLinkClick,
}) => (
  <EmptyState className="black-50">
    <h1 className="f3">0 results found based on your filters.</h1>
    <h2 className="f4">
      Please{' '}
      <a href="#" className="blue" onClick={onFilterLinkClick}>
        review the filters
      </a>{' '}
      applied.
    </h2>
  </EmptyState>
);
