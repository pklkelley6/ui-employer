import {mount, ReactWrapper} from 'enzyme';
import React from 'react';
import {mcf} from '@mcf/constants';
import {act} from 'react-dom/test-utils';
import {ApolloProvider} from 'react-apollo';
import {MockedProvider, MockedResponse} from '@apollo/react-testing';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {print} from 'graphql';
import {nextTick} from '../../../testUtil/enzyme';
import {MassApplicationStatusUpdateModal} from '../MassApplicationStatusUpdateModal';
import {Dropdown} from '~/components/Core/Dropdown';
import {PactBuilder} from '~/__mocks__/pact';
import {SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import {MutationSetMassApplicationShortlistAndStatusUpdateArgs} from '~/graphql/__generated__/types';
import {API_PROFILE_VERSION} from '~/graphql';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

let pactBuilder: PactBuilder;

const pactApplicationIds = ['R90SS0001A-128', 'R90SS0001A-129', 'R90SS0001A-130', 'R90SS0001A-131'];
const onCancelMock = jest.fn();
const onSubmitMock = jest.fn();
const onUpdateCompletedMock = jest.fn();

describe('MassApplicationStatusUpdateModal', () => {
  let wrapper: ReactWrapper;

  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });

  afterAll(async () => pactBuilder.provider.finalize());

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('successful flow', () => {
    beforeEach(() => {
      wrapper = mountComponentWithApolloProvider({});
    });

    it('should display the correct number of applicants selected', () => {
      expect(wrapper.find('[data-cy="dropdown-mass-application-status-update"] label').text()).toEqual(
        'Update application status for 4 applicants to',
      );
    });

    it('does not enable the Update button without selecting an application status', () => {
      const updateButton = wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]');
      expect(updateButton.prop('disabled')).toEqual(true);
    });

    it('select an application status to Shortlisted, and verify interaction and check that modal body updated', async () => {
      addInteraction({
        description: 'request setMassApplicationShortlistAndStatusUpdate for 4 applications with shortlisted status',
        variables: {
          applicationIds: pactApplicationIds,
          statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
          isShortlisted: true,
        },
        response: {
          body: {
            data: {
              setMassApplicationShortlistAndStatusUpdate: Matchers.eachLike({id: 'applicationId', ok: true}),
            },
          },
          status: 200,
        },
        headers: {[API_VERSION_HEADER_KEY]: API_PROFILE_VERSION},
      });
      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
      });
      await nextTick(wrapper);

      const updateButton = wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]');
      expect(updateButton.prop('disabled')).toBeUndefined();

      await act(async () => {
        wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]').simulate('click');
      });

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });
      expect(onSubmitMock).toHaveBeenNthCalledWith(1, mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);

      expect(wrapper.find('[data-cy="mass-application-status-update-success-header"]').text()).toEqual(
        'Status has been updated',
      );

      expect(wrapper.find('[data-cy="mass-application-status-update-success-body"]').text()).toEqual(
        '4 applicants updated to To Interview',
      );
    });

    it('triggers onCancel on Cancel click', () => {
      const cancelButton = wrapper.find('[data-cy="cancel-button-mass-application-status-update-modal"]');
      expect(cancelButton).toHaveLength(1);
      cancelButton.simulate('click');
      expect(onCancelMock).toHaveBeenCalledTimes(1);
    });
  });

  describe('unsuccessful flow', () => {
    it('call onCancel function when updating application that user does not have permission for an application that user does not have permission for', async () => {
      wrapper = mountComponentWithApolloProvider({applicationIds: [pactApplicationIds[0], 'fakeApplicationId']});
      addInteraction({
        description:
          'request setMassApplicationShortlistAndStatusUpdate for 1 existing application and 1 non-existing application with successful status',
        variables: {
          applicationIds: [pactApplicationIds[0], 'fakeApplicationId'],
          statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
          isShortlisted: false,
        },
        response: {
          body: {
            errors: [
              {
                message: 'Job application fakeApplicationId is not found',
                locations: [
                  {
                    line: 2,
                    column: 3,
                  },
                ],
                path: ['setMassApplicationShortlistAndStatusUpdate'],
                extensions: {
                  code: 'INTERNAL_SERVER_ERROR',
                  exception: {
                    code: 'JOB_APPLICATION_ID_NOT_FOUND_ERROR',
                    name: 'JobApplicationIdNotFoundError',
                  },
                },
              },
            ],
          },
          status: 200,
        },
        headers: {[API_VERSION_HEADER_KEY]: API_PROFILE_VERSION},
      });

      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
      });
      await nextTick(wrapper);

      const updateButton = wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]');
      expect(updateButton.prop('disabled')).toBeUndefined();

      await act(async () => {
        wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]').simulate('click');
      });
      await nextTick(wrapper);

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });
      expect(onSubmitMock).toHaveBeenNthCalledWith(1, mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
      expect(onCancelMock).toBeCalledTimes(1);
    });

    it('call onCancel and onUpdateCompleted function when updating application partially fails', async () => {
      wrapper = mountComponentWithMockProvider({
        applicationIds: [pactApplicationIds[0], pactApplicationIds[1]],
        mocks: [
          {
            request: {
              query: SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE,
              variables: {
                applicationIds: [pactApplicationIds[0], pactApplicationIds[1]],
                statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
                isShortlisted: false,
              },
            },
            result: {
              data: {
                setMassApplicationShortlistAndStatusUpdate: [
                  {
                    id: pactApplicationIds[0],
                    ok: true,
                  },
                  {
                    id: pactApplicationIds[1],
                    ok: false,
                  },
                ],
              },
            },
          },
        ],
      });

      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
      });
      await nextTick(wrapper);

      const updateButton = wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]');
      expect(updateButton.prop('disabled')).toBeUndefined();

      await act(async () => {
        wrapper.find('[data-cy="submit-button-mass-application-status-update-modal"]').simulate('click');
      });
      expect(onSubmitMock).toHaveBeenNthCalledWith(1, mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);

      await nextTick(wrapper);

      expect(onUpdateCompletedMock).toBeCalledTimes(1);
      expect(onCancelMock).toBeCalledTimes(1);
    });
  });
});

const addInteraction = async ({
  description,
  variables,
  response,
  headers = {[API_VERSION_HEADER_KEY]: API_PROFILE_VERSION},
}: {
  description: string;
  variables: MutationSetMassApplicationShortlistAndStatusUpdateArgs;
  response: any;
  headers?: Record<string, any>;
}) => {
  const interaction = new ApolloGraphQLInteraction()
    .uponReceiving(description)
    .withQuery(print(SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
    .withOperation('setMassApplicationShortlistAndStatusUpdate')
    .withRequest({
      headers: {
        'content-type': 'application/json',
        ...headers,
      },
      method: 'POST',
      path: '/profile',
    })
    .withVariables(variables)
    .willRespondWith(response);
  await pactBuilder.provider.addInteraction(interaction);
};

const mountComponentWithApolloProvider = ({
  applicationIds,
  onCancel,
  onUpdateCompleted,
}: {
  applicationIds?: string[];
  onCancel?: () => void;
  onUpdateCompleted?: () => void;
}) =>
  mount(
    <ApolloProvider client={pactBuilder.getApolloClient()}>
      <MassApplicationStatusUpdateModal
        applicationIds={applicationIds ?? pactApplicationIds}
        onCancel={onCancel ?? onCancelMock}
        onSubmit={onSubmitMock}
        onUpdateCompleted={onUpdateCompleted ?? onUpdateCompletedMock}
      />
    </ApolloProvider>,
  );

const mountComponentWithMockProvider = ({
  applicationIds,
  onCancel,
  onUpdateCompleted,
  mocks,
}: {
  mocks: ReadonlyArray<MockedResponse>;
  applicationIds?: string[];
  onCancel?: () => void;
  onUpdateCompleted?: () => void;
  store?: any;
}) =>
  mount(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MassApplicationStatusUpdateModal
        applicationIds={applicationIds ?? pactApplicationIds}
        onCancel={onCancel ?? onCancelMock}
        onSubmit={onSubmitMock}
        onUpdateCompleted={onUpdateCompleted ?? onUpdateCompletedMock}
      />
    </MockedProvider>,
  );
