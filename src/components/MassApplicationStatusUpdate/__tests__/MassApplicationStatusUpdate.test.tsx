import {mount} from 'enzyme';
import React from 'react';
import {MassCheckboxWithUpdateStatusButton} from '../MassApplicationStatusUpdate';

describe('MassCheckboxWithUpdateStatusButton', () => {
  const onMassApplicationCheckboxClickedMock = jest.fn();
  const onMassApplicationButtonClickedMock = jest.fn();

  it('should display mass-application-status-select-all-text-disabled if both isMassApplicationCheckboxChecked and isMassApplicationCheckboxIndeterminate is false', () => {
    const wrapper = mount(
      <MassCheckboxWithUpdateStatusButton
        onMassApplicationButtonClicked={onMassApplicationButtonClickedMock}
        onMassApplicationCheckboxClicked={onMassApplicationCheckboxClickedMock}
        isMassApplicationCheckboxChecked={false}
        isMassApplicationCheckboxIndeterminate={false}
        isCheckboxDisabled={true}
        totalApplicationsChecked={0}
      />,
    );

    expect(wrapper.find('[data-cy="mass-application-status-select-all-text-disabled"]')).toHaveLength(1);
  });

  it('should display updateStatus button with amount of applicantsChecked if either isMassApplicationCheckboxChecked or isMassApplicationCheckboxIndeterminate is true', () => {
    const wrapper = mount(
      <MassCheckboxWithUpdateStatusButton
        onMassApplicationButtonClicked={onMassApplicationButtonClickedMock}
        onMassApplicationCheckboxClicked={onMassApplicationCheckboxClickedMock}
        isMassApplicationCheckboxChecked={false}
        isMassApplicationCheckboxIndeterminate={true}
        isCheckboxDisabled={true}
        totalApplicationsChecked={4}
      />,
    );

    const updateButtonIndeterminate = wrapper.find('[data-cy="mass-application-status-update-button"]');

    expect(updateButtonIndeterminate).toHaveLength(1);
    expect(updateButtonIndeterminate.props().value).toEqual('Update status (4)');

    wrapper.setProps({
      isMassApplicationCheckboxChecked: true,
      isMassApplicationCheckboxIndeterminate: false,
      isCheckboxDisabled: true,
    });

    const updateButtonChecked = wrapper.find('[data-cy="mass-application-status-update-button"]');

    expect(updateButtonChecked).toHaveLength(1);
    expect(updateButtonChecked.props().value).toEqual('Update status (4)');
  });

  it('should disable text and button if isCheckboxDiabled is true', () => {
    const wrapper = mount(
      <MassCheckboxWithUpdateStatusButton
        onMassApplicationButtonClicked={onMassApplicationButtonClickedMock}
        onMassApplicationCheckboxClicked={onMassApplicationCheckboxClickedMock}
        isMassApplicationCheckboxChecked={false}
        isMassApplicationCheckboxIndeterminate={false}
        isCheckboxDisabled={true}
        totalApplicationsChecked={0}
      />,
    );

    expect(wrapper.find('[data-cy="mass-application-status-select-all-text-disabled"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="mass-application-selection-checkbox"]').props().disabled).toEqual(true);
  });
});
