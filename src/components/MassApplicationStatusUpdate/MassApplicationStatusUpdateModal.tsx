import {every} from 'lodash/fp';
import {mcf} from '@mcf/constants';
import React, {useState} from 'react';
import {useMutation} from 'react-apollo';
import {Modal} from '../Core/Modal';
import {growlNotification, GrowlType} from '../GrowlNotification/GrowlNotification';
import styles from './MassApplicationStatusUpdateModal.scss';
import {Dropdown} from '~/components/Core/Dropdown';
import {updateApplicationStatusOptions} from '~/components/CandidateInfo/ApplicationStatus';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {
  MutationSetMassApplicationShortlistAndStatusUpdateArgs,
  SetMassApplicationShortlistAndStatusUpdateMutation,
} from '~/graphql/__generated__/types';
import {SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';

interface IMassApplicationStatusUpdateModalProps {
  applicationIds: string[];
  onCancel: () => void;
  onSubmit: (applicationStatus: mcf.JOB_APPLICATION_STATUS) => void;
  onUpdateCompleted: () => void;
}

export const MassApplicationStatusUpdateModal: React.FunctionComponent<IMassApplicationStatusUpdateModalProps> = ({
  applicationIds,
  onCancel,
  onSubmit,
  onUpdateCompleted,
}) => {
  const label: JSX.Element = (
    <span>
      Update application status for{' '}
      <span className="primary fw6">
        {applicationIds.length} applicant{applicationIds.length === 1 ? '' : 's'}
      </span>{' '}
      to
    </span>
  );

  const [applicationStatus, setApplicationStatus] = useState<mcf.JOB_APPLICATION_STATUS | undefined>(undefined);
  const [isShortlisted, setIsShortlisted] = useState<boolean>(false);

  const [setMassApplicationShortlistAndStatusUpdate, {loading: mutationLoading, data}] = useMutation<
    SetMassApplicationShortlistAndStatusUpdateMutation,
    MutationSetMassApplicationShortlistAndStatusUpdateArgs
  >(SET_MASS_APPLICATION_SHORTLIST_AND_STATUS_UPDATE, {
    onCompleted: (data) => {
      if (applicationStatus && data.setMassApplicationShortlistAndStatusUpdate) {
        onUpdateCompleted();

        if (!every(['ok', true], data.setMassApplicationShortlistAndStatusUpdate)) {
          growlNotification(
            'Temporarily unable to update application status for some applicants. Please try again.',
            GrowlType.ERROR,
            {
              toastId: 'massApplicationStatusUpdateError',
              autoClose: 15000,
            },
          );
          onCancel();
        }
      }
    },
    onError: () => {
      growlNotification('Temporarily unable to update application status. Please try again.', GrowlType.ERROR, {
        toastId: 'massApplicationStatusUpdateError',
      });
      onCancel();
    },
  });

  const handleUpdateClicked = async () => {
    if (applicationStatus) {
      onSubmit(applicationStatus);
      await setMassApplicationShortlistAndStatusUpdate({
        variables: {
          applicationIds: applicationIds,
          statusId: applicationStatus,
          isShortlisted: isShortlisted,
        },
      });
    }
  };

  const showLoader = () => {
    return (
      <div className="bg-white tc ph5 pv4">
        <NumberLoader />
      </div>
    );
  };

  const showMassApplicationStatusUpdateFormSuccess = () => {
    return (
      <>
        <div
          data-cy="mass-application-status-update-success"
          className={`bg-white pa4 black-80 ${styles.standardModal}`}
        >
          <h3 data-cy="mass-application-status-update-success-header" className="f4 ma0 pb3 fw6">
            Status has been updated
          </h3>
          <div className="w-80 mb4" data-cy="mass-application-status-update-success-body">
            <p>
              <span className="primary fw6">
                {applicationIds.length} applicant{applicationIds.length === 1 ? '' : 's'}
              </span>{' '}
              updated to{' '}
              <span className="primary fw6">
                {getApplicationStatusWithShortlisted(applicationStatus, isShortlisted)?.label}
              </span>
            </p>
          </div>
        </div>
        <div className="pa2 tc bg-light-gray">
          <div className="tr">
            <button
              data-cy="success-button-mass-application-status-update-modal"
              className={`ma2 pa3 bg-primary ${styles.submitButton}`}
              onClick={onCancel}
            >
              Ok
            </button>
          </div>
        </div>
      </>
    );
  };
  const massApplicationStatusUpdateForm = () => {
    return (
      <div data-cy="mass-application-status-update" className="bg-white black-60">
        <div className="pa4">
          <h3 className="f4 ma0 pb3 fw6">Update Status</h3>
          <div className="w-80 mb4">
            <Dropdown
              innerProps={{
                'data-cy': 'dropdown-mass-application-status-update',
              }}
              placeholder="Not updated"
              label={label}
              data={updateApplicationStatusOptions}
              input={{
                onChange: (value) => {
                  setApplicationStatus(value);
                  setIsShortlisted(value == mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
                },
                value: applicationStatus,
              }}
            />
          </div>
        </div>
        <div className="pa2 tc bg-light-gray">
          <button
            data-cy="cancel-button-mass-application-status-update-modal"
            className={`ma2 pa3 primary ${styles.cancelButton}`}
            onClick={onCancel}
          >
            Cancel
          </button>
          {applicationStatus ? (
            <button
              data-cy="submit-button-mass-application-status-update-modal"
              className={`ma2 pa3 bg-primary ${styles.submitButton}`}
              onClick={() => handleUpdateClicked()}
            >
              Update
            </button>
          ) : (
            <button
              data-cy="submit-button-mass-application-status-update-modal"
              className={`ma2 pa3 bg-primary ${styles.submitButton}`}
              disabled={true}
            >
              Update
            </button>
          )}
        </div>
      </div>
    );
  };

  return (
    <Modal>
      <div>
        {mutationLoading
          ? showLoader()
          : data
          ? showMassApplicationStatusUpdateFormSuccess()
          : massApplicationStatusUpdateForm()}
      </div>
    </Modal>
  );
};
