import React from 'react';
import styles from '~/components/ApplicationsToolBar/ApplicationsToolBar.scss';

export interface IMassCheckboxWithUpdateStatusButton {
  onMassApplicationButtonClicked: () => void;
  onMassApplicationCheckboxClicked: () => void;
  isMassApplicationCheckboxChecked: boolean;
  isMassApplicationCheckboxIndeterminate: boolean;
  isCheckboxDisabled: boolean;
  totalApplicationsChecked: number;
}

export const MassCheckboxWithUpdateStatusButton: React.FunctionComponent<IMassCheckboxWithUpdateStatusButton> = ({
  isMassApplicationCheckboxChecked,
  onMassApplicationButtonClicked,
  onMassApplicationCheckboxClicked,
  isMassApplicationCheckboxIndeterminate = false,
  isCheckboxDisabled = false,
  totalApplicationsChecked,
}) => {
  const UpdateStatusButton: React.FunctionComponent = () => {
    return (
      <div className={`br b--moon-gray items-start pv2 ${styles.selectSegment}`}>
        <input
          className="f6 br2 dib white bg-blue pv2 ph3 pointer bn tc"
          data-cy="mass-application-status-update-button"
          type="button"
          onClick={onMassApplicationButtonClicked}
          value={`Update status (${totalApplicationsChecked})`}
        />
      </div>
    );
  };

  const UpdateStatusLabel: React.FunctionComponent = () => {
    return (
      <div className={`br b--moon-gray items-start pv2 ${styles.selectSegment}`}>
        {isCheckboxDisabled ? (
          <label
            data-cy="mass-application-status-select-all-text-disabled"
            className={`f4-5 fw6 black-50 ma0 lh-solid tl pr5 cursor-not-allowed ${styles.checkboxInactive}`}
          >
            Select all on <br /> this page
          </label>
        ) : (
          <label
            data-cy="mass-application-status-select-all-text"
            className="f4-5 fw6 black-50 ma0 lh-solid tl pr5 pointer"
            htmlFor="selectAll"
          >
            Select all on <br /> this page
          </label>
        )}
      </div>
    );
  };

  return (
    <>
      <label className="mr2">
        <input
          data-cy="mass-application-selection-checkbox"
          id="selectAll"
          onClick={onMassApplicationCheckboxClicked}
          checked={isMassApplicationCheckboxChecked}
          className={`${styles.selectionCheckbox}`}
          type="checkbox"
          disabled={isCheckboxDisabled}
          ref={(input) => {
            if (input) {
              input.checked = isMassApplicationCheckboxChecked;
              input.indeterminate = isMassApplicationCheckboxIndeterminate;
            }
          }}
        />
        <span className="f5 black-80 pointer" />
      </label>
      {isMassApplicationCheckboxChecked || isMassApplicationCheckboxIndeterminate ? (
        <UpdateStatusButton />
      ) : (
        <UpdateStatusLabel />
      )}
    </>
  );
};
