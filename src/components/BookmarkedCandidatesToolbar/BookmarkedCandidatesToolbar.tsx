import React from 'react';
import styles from '~/components/BookmarkedCandidatesToolbar/BookmarkedCandidatesToolbar.scss';

interface IBookmarkedCandidatesToolbar {
  filterSlot: JSX.Element | string;
}

export const BookmarkedCandidatesToolbar: React.FC<IBookmarkedCandidatesToolbar> = ({filterSlot}) => (
  <div className={`flex bg-black-50 bb b--black-05 ${styles.toolbar}`} data-cy="bookmarked-candidates-toolbar">
    <div className={`w-100 flex items-center justify-end ph3 ${styles.filterSlotBox}`}>
      <div className="flex items-center justify-around">
        <p data-cy="filter-slot-label" className="self-center f4-5 fw6 black-60 ma0 lh-solid tr pr3">
          Filter:
        </p>
        <div className={styles.filterSlotDropdown} data-cy="filter-slot-dropdown">
          {filterSlot}
        </div>
      </div>
    </div>
  </div>
);
