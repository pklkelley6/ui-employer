import {mount} from 'enzyme';
import React from 'react';
import {BookmarkedCandidatesToolbar} from './../BookmarkedCandidatesToolbar';

describe('BookmarkedCandidatesToolbar', () => {
  it('should render BookmarkedCandidatesToolbar with FilterSlot', () => {
    const props = {
      filterSlot: 'mockElement',
    };

    const wrapper = mount(<BookmarkedCandidatesToolbar {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
