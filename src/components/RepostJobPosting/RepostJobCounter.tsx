import React, {FunctionComponent} from 'react';
import {addMonths, isWithinInterval, format, subDays} from 'date-fns';
import {inRange} from 'lodash';
import styles from './RepostJobCounter.scss';
import {MAX_JOB_REPOSTS} from './repostJobCounter.constants';

interface IRepostJobCounterProps {
  originalPostingDate: Date;
  repostCount: number;
}

export const RepostJobCounter: FunctionComponent<IRepostJobCounterProps> = (props) => {
  const repostsLeft = <RepostsLeft repostCount={props.repostCount} />;
  const durationValidity = <DurationValidity originalPostingDate={props.originalPostingDate} />;

  if (canRepost(props.repostCount, props.originalPostingDate)) {
    return (
      <section data-cy="repost-job-counter" className="bg-black-05 pa3 black-80 mb3 lh-copy">
        {repostsLeft}
        <div className={`${styles.separator} mb3 f6`}>and</div>
        {durationValidity}
      </section>
    );
  } else {
    if (!(canRepostCount(props.repostCount) || canRepostDate(props.originalPostingDate, new Date()))) {
      return (
        <section data-cy="repost-job-counter" className="bg-red pa3 white mb3 lh-copy">
          {repostsLeft}
          <div className={`${styles.separator} ${styles.separatorError} mb3 f6`}>and</div>
          {durationValidity}
        </section>
      );
    } else if (!canRepostDate(props.originalPostingDate, new Date())) {
      return (
        <section data-cy="repost-job-counter" className="bg-red pa3 white mb3">
          {durationValidity}
        </section>
      );
    } else {
      return (
        <section data-cy="repost-job-counter" className="bg-red pa3 white mb3">
          {repostsLeft}
        </section>
      );
    }
  }
};

interface IRepostsLeftProps {
  repostCount: number;
}

const getReposts = (repostCount: number) => {
  const reposts = MAX_JOB_REPOSTS - repostCount;
  return reposts == MAX_JOB_REPOSTS || reposts == 0 ? `${reposts} Reposts left` : `${reposts} Repost left`;
};

const RepostsLeft: FunctionComponent<IRepostsLeftProps> = ({repostCount}) => {
  return (
    <div data-cy="reposts-left" className="flex">
      <div className={`flex-column ${styles.columnWithIcon}`}>
        <i
          className={`${styles.iconRepost} ${
            canRepostCount(repostCount) ? styles.iconRepostTick : styles.iconRepostError
          }`}
        ></i>
      </div>
      <div>
        <strong className="ma0 fw6">{getReposts(repostCount)}</strong>
        {canRepostCount(repostCount) ? (
          <p className="f6 ma0 pv2">Each job can only be reposted twice.</p>
        ) : (
          <p className="f6 ma0 pv2">Please note that each job can only be reposted twice.</p>
        )}
      </div>
    </div>
  );
};

interface IDurationValidityProps {
  originalPostingDate: Date;
}

const DurationValidity: FunctionComponent<IDurationValidityProps> = ({originalPostingDate}) => {
  // subtract 1 day since validity is 6 months inclusive of the start date.
  // ex. first day valid: Jan 1, last day valid: Jun 30
  const lastDayValid = format(subDays(addMonths(originalPostingDate, 6), 1), 'd MMM yyyy');
  return (
    <div data-cy="repost-duration-validity" className="flex">
      <div className={`flex-column ${styles.columnWithIcon}`}>
        <i
          className={`${styles.iconRepost} ${
            canRepostDate(originalPostingDate, new Date()) ? styles.iconRepostTick : styles.iconRepostError
          }`}
        ></i>
      </div>
      <div>
        {canRepostDate(originalPostingDate, new Date()) ? (
          <strong className="ma0 fw6">
            Repost available till <br /> {lastDayValid}
          </strong>
        ) : (
          <strong className="ma0 fw6">
            Repost unavailable after <br /> {lastDayValid}
          </strong>
        )}

        <p className="f6 ma0 pv2">Each job can only be reposted within 6 months of the original date posted.</p>
      </div>
    </div>
  );
};

export const canRepost = (repostCount: number, originalPostingDate: Date) => {
  return canRepostCount(repostCount) && canRepostDate(originalPostingDate, new Date());
};

export const canRepostCount = (repostCount: number) => {
  return inRange(repostCount, MAX_JOB_REPOSTS);
};

// NOTE: if dates are in the future
export const canRepostDate = (originalPostingDate: Date, now: Date) => {
  const sixMonths = addMonths(originalPostingDate, 6);
  return isWithinInterval(now, {start: originalPostingDate, end: sixMonths});
};
