import {mount} from 'enzyme';
import React from 'react';
import {GetResumeButton} from '~/components/GetResumeButton/GetResumeButton';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';

describe('GetResumeButton', () => {
  it('should render GetResumeButton correctly', () => {
    const resume = applicationMock.applicant.resume;
    const wrapperWithResume = mount(<GetResumeButton resume={resume} />);
    expect(wrapperWithResume.find(GetResumeButton)).toMatchSnapshot();

    const noResumePassed = mount(<GetResumeButton resume={undefined} />);
    expect(noResumePassed.find(GetResumeButton)).toMatchSnapshot();
  });
});
