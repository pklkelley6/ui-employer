import React from 'react';
import {debounceAsync} from '~/util/debounceAsync';
import {downloadFile} from '~/util/files';
import {getDocumentUrl} from '~/util/getDocumentUrl';
import styles from '~/components/GetResumeButton/GetResumeButton.scss';
import {IResume} from '~/graphql/candidates/';
import {Maybe} from '~/graphql/__generated__/types';

type GetResumeButtonProps = {
  resume: Maybe<IResume>;
  onResumeClick?: () => void;
};

export function GetResumeButton({resume, onResumeClick}: GetResumeButtonProps) {
  return (
    <a
      data-cy="applicant-resume-download-button"
      tabIndex={0}
      onClick={debounceAsync(async (event) => {
        event.preventDefault();
        if (resume) {
          await downloadFile(getDocumentUrl(resume.filePath), resume.fileName);

          if (onResumeClick) {
            onResumeClick();
          }
        }
      })}
      className={`no-underline dib f3 mt3 mr3 purple pa3 bg-white pointer ${styles.buttonWhiteBackground}`}
    >
      <div className="flex justify-center items-center">
        <i className={`db mr2 relative ${styles.iconDownloadPurple}`} />
        <span className="f5 fw6 db mr2">Get Resume</span>
      </div>
    </a>
  );
}
