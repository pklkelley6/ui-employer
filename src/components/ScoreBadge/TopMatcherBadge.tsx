import React from 'react';
import styles from './TopMatcherBadge.scss';

export const TopMatcherBadge: React.FunctionComponent = () => (
  <div
    data-cy="top-matcher"
    className={`${styles.topLabel} f6-5 green tl lh-title relative pl3 pr2`}
    title="topmatcher"
  >
    Among top matches
  </div>
);
