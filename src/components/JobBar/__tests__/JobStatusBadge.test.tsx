import {mount} from 'enzyme';
import React from 'react';
import {Badge} from './../JobStatusBadge';
import {JobStatus} from '~/services/employer/jobs.types';

describe('Jobs/JobBar', () => {
  it('renders an OPEN badge', () => {
    const wrapper = mount(<Badge status={JobStatus.Open} />);
    expect(wrapper.text()).toBe('OPEN');
  });

  it('renders a DRAFT badge', () => {
    const wrapper = mount(<Badge status={JobStatus.Draft} />);
    expect(wrapper.text()).toBe('DRAFT');
  });

  it('renders a CLOSED badge', () => {
    const wrapper = mount(<Badge status={JobStatus.Closed} />);
    expect(wrapper.text()).toBe('CLOSED');
  });

  it('renders a REOPENED badge', () => {
    const wrapper = mount(<Badge status={JobStatus.Reopened} />);
    expect(wrapper.text()).toBe('REOPENED');
  });

  it('defaults to an UNKNOWN badge', () => {
    const wrapper = mount(<Badge status={JobStatus.Unknown} />);
    expect(wrapper.text()).toBe('Unknown');
  });
});
