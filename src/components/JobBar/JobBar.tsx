import React from 'react';
import styles from './JobBar.scss';
import {Badge} from './JobStatusBadge';
import {IJobPost} from '~/services/employer/jobs.types';
import {isMCFJob} from '~/util/isMCFJob';
import {formatJob, SALARY_UNDISCLOSED} from '~/util/jobPosts';

export interface IJobBarProps {
  job: IJobPost;
}

export const JobBar: React.FunctionComponent<IJobBarProps> = ({job}) => {
  const formattedJob = formatJob(job);

  return (
    <section className={`bg-white flex items-center pa3 w-100 ${styles.jobBar}`}>
      <div className={styles.jobTitleBox}>
        <div className="flex items-center">
          {isMCFJob(formattedJob.id) ? null : <i className={`${styles.iconLock} mr2`} />}
          <div data-cy="job-post-id" className="mr2 f6-5 silver">
            {formattedJob.id}
          </div>
          <Badge status={formattedJob.jobStatus} />
        </div>

        <h2 data-cy="job-post-title" className="f3-5 ma0 mb1 fw4 primary-dark truncate lh-title">
          {formattedJob.title}
        </h2>
        <span className="lh-title f5-5 secondary">
          {formattedJob.positionLevels}
          {formattedJob.positionLevels !== '' ? <span className="ph2 f6 black-60">▸</span> : ''}
        </span>
        <span data-cy="job-post-vacancy" className="lh-title f5-5 black-60">
          {formattedJob.numberOfVacancies} {+formattedJob.numberOfVacancies > 1 ? ' Vacancies' : ' Vacancy'}
        </span>
      </div>

      <div className={`${styles.detailsBox} silver ph2`}>
        <div>
          {formattedJob.address && (
            <span data-cy="job-post-address" className="icon-bw-location black-60 f6 lh-copy">
              {formattedJob.address}
            </span>
          )}
        </div>
        <div>
          <span data-cy="job-post-employment-types" className="icon-bw-employment-type black-60 f6 lh-copy">
            {formattedJob.employmentTypes}
          </span>
          <span data-cy="job-post-categories" className="icon-bw-category black-60 f6 pl2 lh-copy">
            {formattedJob.categories}
          </span>
        </div>
      </div>

      <div className={`${styles.detailsBox} flex flex-column items-end justify-end`}>
        <div className="f4 lh-copy black-70">
          <span data-cy="job-post-monthly-salary">{formattedJob.monthlySalary}</span>
          {formattedJob.monthlySalary !== SALARY_UNDISCLOSED ? <span>/mth</span> : null}
        </div>

        <div className="f6 black-50 tr lh-copy">
          <span data-cy="job-post-posted-date">
            Posted <time dateTime={job.metadata.newPostingDate}>{formattedJob.postedDate}</time>
          </span>
          <span data-cy="job-post-expiry-date" className="dib ml2">
            Closing <time dateTime={job.metadata.expiryDate}>{formattedJob.expiryDate}</time>
          </span>
        </div>
      </div>
    </section>
  );
};
