export const hasRemainingApplications = ({
  vacancies,
  applicationsSuccessfulCount = 0,
  applicationsUnsuccessfulCount = 0,
  applicationsTotal,
}: {
  vacancies: number;
  applicationsSuccessfulCount?: number;
  applicationsUnsuccessfulCount?: number;
  applicationsTotal: number;
}) => {
  if (applicationsSuccessfulCount >= vacancies) {
    return applicationsSuccessfulCount + applicationsUnsuccessfulCount < applicationsTotal;
  }
  return false;
};
