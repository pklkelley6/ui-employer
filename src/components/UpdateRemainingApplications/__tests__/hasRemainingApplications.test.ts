import {hasRemainingApplications} from '../hasRemainingApplications';

describe.each([
  [1, 1, 0, 1, false],
  [1, 0, 0, 1, false],
  [2, 1, 0, 3, false],
  [2, 2, 0, 3, true],
  [2, 3, 0, 3, false],
  [2, 2, 1, 3, false],
])(
  'hasRemainingApplications',
  (vacancies, applicationsSuccessfulCount, applicationsUnsuccessfulCount, applicationsTotal, expected) => {
    test(`should returns ${expected} if vacancies = ${vacancies}, applications successful count = ${applicationsSuccessfulCount}, applications unsuccessful count = ${applicationsUnsuccessfulCount}, applications total = ${applicationsTotal}`, () => {
      expect(
        hasRemainingApplications({
          vacancies,
          applicationsSuccessfulCount,
          applicationsUnsuccessfulCount,
          applicationsTotal,
        }),
      ).toBe(expected);
    });
  },
);
