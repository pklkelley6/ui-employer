import {MockedProvider, MockedResponse} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import React from 'react';
import {print} from 'graphql';
import {UpdateRemainingApplications} from '../UpdateRemainingApplications';
import {nextTick} from '~/testUtil/enzyme';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {PactBuilder} from '~/__mocks__/pact';
import {SET_APPLICATION_STATUS_BY_JOB} from '~/graphql/applications';
import {MutationSetApplicationStatusByJobArgs} from '~/graphql/__generated__/types';
import * as GrowlNotification from '~/components/GrowlNotification/GrowlNotification';
import {API_PROFILE_VERSION} from '~/graphql';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

const {NOT_SENT, RECEIVED, UNDER_REVIEW, UNSUCCESSFUL, SUCCESSFUL} = mcf.JOB_APPLICATION_STATUS;

const onUpdateCompletedMock = jest.fn();
const growlNotificationMock = jest.spyOn(GrowlNotification, 'growlNotification');

const pactJobUuid = 'R90SS0001A_1200000_(0)';
let pactBuilder: PactBuilder;

describe('UpdateRemainingApplications', () => {
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });

  afterAll(async () => pactBuilder.provider.finalize());

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should update status of all non successful or unsuccessful applications in job to unsuccessful when update button is clicked', async () => {
    await addInteraction({
      givenState: `job ${pactJobUuid} has application with under review status`,
      description: 'request setApplicationStatusByJob to unsuccessful',
      variables: {
        jobId: pactJobUuid,
        fromStatusIds: [NOT_SENT, RECEIVED, UNDER_REVIEW],
        toStatusId: UNSUCCESSFUL,
      },
      response: {
        body: {
          data: {
            setApplicationStatusByJob: Matchers.eachLike({id: 'applicationId', ok: true}),
          },
        },
        status: 200,
      },
    });

    const wrapper = mountUpdateRemainingApplications(pactJobUuid);
    await act(async () => {
      wrapper.find('[data-cy="update-remaining-applications-link"]').simulate('click');
    });

    await nextTick(wrapper);

    expect(wrapper.find(NumberLoader)).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
    expect(onUpdateCompletedMock).toBeCalledTimes(1);
  });

  it('should show error growl when a full error has been received by updating a non existing job', async () => {
    const nonExistingJobId = 'nonExistingJobId';
    await addInteraction({
      description: 'request setApplicationStatusByJob with unauthorized jobId',
      variables: {
        jobId: nonExistingJobId,
        fromStatusIds: [NOT_SENT, RECEIVED, UNDER_REVIEW],
        toStatusId: UNSUCCESSFUL,
      },
      response: {
        body: {
          errors: [
            {
              message: Matchers.like(`Job id for ${nonExistingJobId} not found`),
              path: ['setApplicationStatusByJob'],
              extensions: {
                code: 'INTERNAL_SERVER_ERROR',
                exception: {
                  code: 'JOB_ID_NOT_FOUND',
                  name: 'JobIdNotFoundError',
                },
              },
            },
          ],
        },
        status: 200,
      },
    });
    const wrapper = mountUpdateRemainingApplications(nonExistingJobId);
    await act(async () => {
      wrapper.find('[data-cy="update-remaining-applications-link"]').simulate('click');
    });

    await nextTick(wrapper);

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });
    expect(onUpdateCompletedMock).toBeCalledTimes(0);
    expect(growlNotificationMock).toBeCalledWith(
      'Temporarily unable to update application status. Please try again.',
      GrowlNotification.GrowlType.ERROR,
      {
        toastId: 'massApplicationStatusUpdateError',
      },
    );
  });

  it('should show error growl for 15s when updating applications partially fails', async () => {
    const wrapper = mountComponentWithMockProvider({
      jobId: pactJobUuid,
      mocks: [
        {
          request: {
            query: SET_APPLICATION_STATUS_BY_JOB,
            variables: {
              jobId: pactJobUuid,
              fromStatusIds: [NOT_SENT, RECEIVED, UNDER_REVIEW],
              toStatusId: UNSUCCESSFUL,
            },
          },
          result: {
            data: {
              setApplicationStatusByJob: [
                {
                  id: 'application-1',
                  ok: true,
                },
                {
                  id: 'application-2',
                  ok: false,
                },
              ],
            },
          },
        },
      ],
    });

    await act(async () => {
      wrapper.find('[data-cy="update-remaining-applications-link"]').simulate('click');
    });
    await nextTick(wrapper);

    expect(onUpdateCompletedMock).toBeCalledTimes(1);
    expect(growlNotificationMock).toBeCalledWith(
      'Temporarily unable to update application status for some applicants. Please try again.',
      GrowlNotification.GrowlType.ERROR,
      {
        toastId: 'massApplicationStatusUpdateError',
        autoClose: 15000,
      },
    );
  });
});

const addInteraction = async ({
  description,
  variables,
  response,
  givenState,
  headers = {[API_VERSION_HEADER_KEY]: API_PROFILE_VERSION},
}: {
  description: string;
  variables: MutationSetApplicationStatusByJobArgs;
  response: any;
  givenState?: string;
  headers?: Record<string, any>;
}) => {
  const interaction = new ApolloGraphQLInteraction()
    .uponReceiving(description)
    .withQuery(print(SET_APPLICATION_STATUS_BY_JOB))
    .withOperation('setApplicationStatusByJob')
    .withRequest({
      method: 'POST',
      path: '/profile',
      headers: {
        'content-type': 'application/json',
        ...headers,
      },
    })
    .withVariables(variables)
    .willRespondWith(response);
  await pactBuilder.provider.addInteraction(givenState ? interaction.given(givenState) : interaction);
};

const mountUpdateRemainingApplications = (jobId: string) => {
  const props = {
    jobId,
    vacancies: 3,
    applicationsTotal: 4,
    applicationsByJobAndStatus: [{statusId: SUCCESSFUL, count: 3}],
    onUpdateCompleted: onUpdateCompletedMock,
  };
  return mount(
    <ApolloProvider client={pactBuilder.getApolloClient()}>
      <UpdateRemainingApplications {...props} />
    </ApolloProvider>,
  );
};

const mountComponentWithMockProvider = ({jobId, mocks}: {jobId: string; mocks: ReadonlyArray<MockedResponse>}) => {
  const props = {
    jobId,
    vacancies: 3,
    applicationsTotal: 4,
    applicationsByJobAndStatus: [{statusId: SUCCESSFUL, count: 3}],
    onUpdateCompleted: onUpdateCompletedMock,
  };
  return mount(
    <MockedProvider mocks={mocks} addTypename={false}>
      <UpdateRemainingApplications {...props} />
    </MockedProvider>,
  );
};
