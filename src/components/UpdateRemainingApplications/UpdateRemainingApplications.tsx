import {every} from 'lodash/fp';
import React from 'react';
import {mcf} from '@mcf/constants';
import {useMutation} from 'react-apollo';
import {hasRemainingApplications} from './hasRemainingApplications';
import styles from './UpdateRemainingApplications.scss';
import {ApplicationsStatus} from '~/graphql/__generated__/types';
import {LoaderModal} from '~/components/LoaderModal/LoaderModal';
import {MutationSetApplicationStatusByJobArgs, SetApplicationStatusByJobMutation} from '~/graphql/__generated__/types';
import {SET_APPLICATION_STATUS_BY_JOB, APPLICATION_STATUS_FRAGMENT} from '~/graphql/applications';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';

const {NOT_SENT, RECEIVED, UNDER_REVIEW, UNSUCCESSFUL} = mcf.JOB_APPLICATION_STATUS;

interface IUpdateRemainingCandidatesProps {
  jobId: string;
  vacancies: number;
  applicationsByJobAndStatus?: ApplicationsStatus[];
  applicationsTotal: number;
  onUpdateCompleted: () => void;
}

export const UpdateRemainingApplications: React.FC<IUpdateRemainingCandidatesProps> = ({
  jobId,
  vacancies,
  applicationsByJobAndStatus = [],
  applicationsTotal = 0,
  onUpdateCompleted,
}) => {
  const [setApplicationStatusByJob, {loading}] = useMutation<
    SetApplicationStatusByJobMutation,
    MutationSetApplicationStatusByJobArgs
  >(SET_APPLICATION_STATUS_BY_JOB, {
    onCompleted: (data) => {
      if (!every(['ok', true], data.setApplicationStatusByJob)) {
        growlNotification(
          'Temporarily unable to update application status for some applicants. Please try again.',
          GrowlType.ERROR,
          {
            toastId: 'massApplicationStatusUpdateError',
            autoClose: 15000,
          },
        );
      }
      onUpdateCompleted();
    },
    onError: () => {
      growlNotification('Temporarily unable to update application status. Please try again.', GrowlType.ERROR, {
        toastId: 'massApplicationStatusUpdateError',
      });
    },
    update: (cache, {data}) => {
      for (const updateResult of data?.setApplicationStatusByJob ?? []) {
        if (updateResult.ok) {
          const id = `Application:${updateResult.id}`;
          cache.writeFragment({
            id,
            fragment: APPLICATION_STATUS_FRAGMENT,
            data: {
              __typename: 'Application',
              statusId: UNSUCCESSFUL,
            },
          });
        }
      }
    },
  });

  const handleUpdateClicked = async () => {
    await setApplicationStatusByJob({
      variables: {
        jobId,
        fromStatusIds: [NOT_SENT, RECEIVED, UNDER_REVIEW],
        toStatusId: UNSUCCESSFUL,
      },
    });
  };
  return hasRemainingApplications({
    vacancies,
    applicationsSuccessfulCount: applicationsByJobAndStatus.find(
      ({statusId}) => statusId == mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    )?.count,
    applicationsUnsuccessfulCount: applicationsByJobAndStatus.find(
      ({statusId}) => statusId == mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
    )?.count,
    applicationsTotal: applicationsTotal,
  }) ? (
    <div
      className={`flex justify-between bl bw2 black-60 f6 lh-copy ${styles.updateRemainingApplications}`}
      data-cy="update-remaining-applications"
    >
      <div data-cy="update-remaining-applications-text">
        All vacancies filled. Do you want to update the remaining applicants as{' '}
        <strong className="fw6">Unsuccessful</strong>?
      </div>

      {loading ? (
        <LoaderModal />
      ) : (
        <a
          className="link underline fw6 pointer"
          data-cy="update-remaining-applications-link"
          onClick={handleUpdateClicked}
        >
          Yes, Update
        </a>
      )}
    </div>
  ) : null;
};
