import React from 'react';

export const InvitedTalentLabel: React.FunctionComponent = () => {
  return (
    <div
      data-cy="invited-talent-label"
      className="f8 bg-secondary br1 white fw6 tc mr2 lh-solid pv1 ph2 dib ttu tracked"
      title="INVITED TALENT"
    >
      INVITED TALENT
    </div>
  );
};
