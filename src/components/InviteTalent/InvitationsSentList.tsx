import React from 'react';
import {IJobPost} from '~/services/employer/jobs.types';
import styles from '~/components/InviteTalent/InvitationsSentList.scss';

type InvitationsSentListProps = {
  payload: string[];
  jobs: IJobPost[];
};

export function InvitationsSentList({payload, jobs}: InvitationsSentListProps) {
  return (
    <ul className={`list pl0 overflow-y-auto ${styles.list}`}>
      {payload.map((payloadJob) => {
        const jobFound = jobs.find((job) => job.uuid === payloadJob);

        return (
          <li className="pb2 flex" key={jobFound?.uuid}>
            <h3 className="fw6 ma0 f4-5 lh-title pr1 dib">–</h3>
            <div className="dib">
              <h3 className="fw6 ma0 f4-5 lh-title truncate">{jobFound?.title}</h3>
              <div className="db pt0 black-70 f6 lh-title">{jobFound?.metadata.jobPostId}</div>
            </div>
          </li>
        );
      })}
    </ul>
  );
}
