import React from 'react';
import Tooltip from 'rc-tooltip';
import styles from '~/components/InviteTalent/InviteTalentButton.scss';

export enum MESSAGE_TYPE {
  SEND,
  UNABLE,
  INVITED,
}

type InviteTalentButtonProps = {
  message: MESSAGE_TYPE;
  onClick?: () => void;
};

export function InviteTalentButton({message, onClick}: InviteTalentButtonProps) {
  const MESSAGES = [
    'Click to send this talent an email with the job description and invitation to apply for the job.',
    'Unable to invite this talent to apply as you have no jobs that are open at the moment.',
    'This talent has already been invited to all jobs that are open at the moment.',
  ];

  return (
    <div className="db">
      <Tooltip
        id="invite-talent-apply-tooltip"
        placement="top"
        trigger={['hover', 'focus']}
        align={{offset: [8, 2]}}
        overlayClassName={`z-5 bg-mid-gray white ${styles.customTooltip}`}
        destroyTooltipOnHide={true}
        overlay={<span className="db pa3 f7 lh-copy">{MESSAGES[message]}</span>}
      >
        {message == MESSAGE_TYPE.SEND ? (
          <button
            data-cy="invite-talent-apply-button"
            className={`no-underline dib f3 mt3 purple pa3 bg-white pointer ${styles.inviteTalentPurpleBackground}`}
            tabIndex={0}
            onClick={onClick}
          >
            <div className="flex justify-center items-center">
              <i className={`db relative mr2 ${styles.iconWhiteInviteTalent}`} />
              <span className={`f5 fw6 db mr2 white`}>Invite to Apply</span>
            </div>
          </button>
        ) : (
          <button
            data-cy="invite-talent-apply-button-sent"
            className={`no-underline dib f3 mt3 pa3 black-50 bg-light-gray ${styles.inviteTalentDisabled} cursor-not-allowed`}
            tabIndex={0}
            disabled={true}
          >
            <div className="flex justify-center items-center">
              <i className={`db relative mr2 ${styles.iconInviteTalentDisabled}`} />
              <span className="f5 fw6 db mr2">Invite to Apply</span>
            </div>
          </button>
        )}
      </Tooltip>
    </div>
  );
}
