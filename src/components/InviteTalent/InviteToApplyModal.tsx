import React, {useState} from 'react';
import {useAsyncFn} from 'react-use';
import {growlNotification, GrowlType} from '../GrowlNotification/GrowlNotification';
import {IInviteToApplyModalContainerDispatchProps} from './InviteToApplyModalContainer';
import {Modal} from '~/components/Core/Modal';
import {IJobPost} from '~/services/employer/jobs.types';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {postInvitationsBatch} from '~/services/invitations/postInvitationsBatch';
import {JobsToInviteList} from '~/components/InviteTalent/JobsToInviteList';
import {InvitationsSentList} from '~/components/InviteTalent/InvitationsSentList';

export type InviteToApplyModalProps = {
  talent: string;
  jobs?: IJobPost[];
  invitations?: IEmailInvitation[];
  onCancel: () => void;
  onOk: () => void;
};

export function InviteToApplyModal({
  talent,
  jobs = [],
  invitations = [],
  onCancel,
  onOk,
  onTalentSearchCandidateInvited,
}: InviteToApplyModalProps & IInviteToApplyModalContainerDispatchProps) {
  const [payload, setPayload] = useState<string[]>([]);
  const [isInvitationSent, setIsInvitationSent] = useState(false);

  const jobsToInviteList = JobsToInviteList({
    jobs,
    invitations,
    onClick: (checked, uuid) => {
      setPayload(checked ? [...payload, uuid] : payload.filter((item) => item !== uuid));
    },
  });

  const [state, doFetch] = useAsyncFn(async (jobsToInvite: string[]) => {
    const result = await postInvitationsBatch(jobsToInvite, talent);
    setIsInvitationSent(true);
    if (!result.ok) {
      growlNotification('Temporarily unable to send invitation to apply. Please try again.', GrowlType.ERROR, {
        toastId: 'inviteToApplyError',
      });
      onCancel();
    } else if (result.ok) {
      jobsToInvite.forEach((jobUuid) => {
        onTalentSearchCandidateInvited(talent, jobUuid);
      });
    }
  });

  const sendButtonDisabled = (
    <button
      data-cy="invite-to-apply-modal-send-button-disabled"
      className="w-50 ma2 pa3 ba b--primary bg-primary white o-20 cursor-not-allowed"
      disabled={false}
    >
      Send
    </button>
  );

  const invitationsToApplyContent = (
    <>
      <div className="bg-white black-70">
        <div className="pa4">
          <div className="flex justify-between items-center dib">
            <h2 className="f4 ma0 fw6">Invite to apply</h2>
          </div>
          <div className="b--black-20 bt mt3 mb4 w-100" />
          <div>
            <h4 className="ma0 mb1">Select the jobs you want to invite this talent for</h4>
            <p className="ma0 mb4">This list only shows jobs that are open at the moment.</p>
            {jobsToInviteList}
          </div>
        </div>
      </div>
      <div className="flex pa2 tc bg-light-gray">
        <button
          data-cy="invite-to-apply-modal-cancel"
          className="w-50 fw6 primary ma2 pa3 ba b--primary bg-white"
          onClick={() => {
            onCancel();
          }}
        >
          Cancel
        </button>

        {payload.length ? (
          state.loading ? (
            sendButtonDisabled
          ) : (
            <button
              data-cy="invite-to-apply-modal-send-button"
              className="w-50 ma2 pa3 ba b--primary bg-primary white"
              onClick={() => doFetch(payload)}
            >
              Send
            </button>
          )
        ) : (
          sendButtonDisabled
        )}
      </div>
    </>
  );

  const invitationsSentContent = (
    <>
      <div className="bg-white black-70">
        <div className="pa4">
          <div className="flex justify-between items-center dib">
            <h2 className="f4 ma0 fw6">Invitation sent successfully</h2>
          </div>
          <div className="b--black-20 bt mt3 mb4 w-100" />
          <div>
            <p>The talent has been invited for the following jobs:</p>
            <InvitationsSentList payload={payload} jobs={jobs} />
          </div>
        </div>
      </div>
      <div className="flex pa2 tc bg-light-gray justify-end">
        <button
          data-cy="invite-to-apply-modal-ok-button"
          className="w-50 ma2 pa3 ba b--primary bg-primary white"
          onClick={() => onOk()}
        >
          Ok
        </button>
      </div>
    </>
  );

  return (
    <Modal>
      <div data-cy="invite-to-apply-modal" className="flex flex-column w-50 center">
        {isInvitationSent ? invitationsSentContent : invitationsToApplyContent}
      </div>
    </Modal>
  );
}
