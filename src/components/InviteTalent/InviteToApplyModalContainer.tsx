import {connect} from 'react-redux';
import {InviteToApplyModal, InviteToApplyModalProps} from './InviteToApplyModal';
import {IAppState} from '~/flux/index';
import {onTalentSearchCandidateInvited} from '~/flux/analytics';

export interface IInviteToApplyModalContainerDispatchProps {
  onTalentSearchCandidateInvited: typeof onTalentSearchCandidateInvited;
}

export const InviteToApplyModalContainer = connect<
  {},
  IInviteToApplyModalContainerDispatchProps,
  InviteToApplyModalProps,
  IAppState
>(null, {onTalentSearchCandidateInvited})(InviteToApplyModal);
