import {mount} from 'enzyme';
import React from 'react';

import {JobsToInviteList} from '~/components/InviteTalent/JobsToInviteList';
import {jobsMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPost} from '~/services/employer/jobs.types';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';

describe('InviteTalent/JobsToInviteList', () => {
  it('should render JobsToInviteList with sent invites', () => {
    const jobs: IJobPost[] = [
      {...jobsMock[0], uuid: 'INVITATION_NOT_SENT'},
      {...jobsMock[0], uuid: 'INVITATION_SENT'},
    ];
    const invitations: IEmailInvitation[] = [
      {
        jobUuid: 'INVITATION_SENT',
        individualId: 'TALENT',
        updatedAt: new Date('2021-03-03'),
        createdAt: new Date('2021-03-03'),
        isEmailSent: false,
      },
    ];
    const wrapper = mount(<JobsToInviteList jobs={jobs} invitations={invitations} onClick={() => {}} />);
    expect(wrapper).toMatchSnapshot();
  });
});
