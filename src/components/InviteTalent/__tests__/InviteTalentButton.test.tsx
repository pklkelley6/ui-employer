import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';

import toJson from 'enzyme-to-json';
import {InviteTalentButton, MESSAGE_TYPE} from '~/components/InviteTalent/InviteTalentButton';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('InviteTalentButton', () => {
  const reduxState = {
    releaseToggles: {'178375045colourSchemeResumeButtonAndInviteTalent': true},
  };

  const store = configureStore()(reduxState);

  it('should render InviteTalentButton correctly', () => {
    const invitedButton = mount(
      <Provider store={store}>
        <InviteTalentButton message={MESSAGE_TYPE.INVITED} />
      </Provider>,
    );
    expect(toJson(invitedButton, cleanSnapshot())).toMatchSnapshot();

    const sendButton = mount(
      <Provider store={store}>
        <InviteTalentButton message={MESSAGE_TYPE.SEND} />
      </Provider>,
    );
    expect(toJson(sendButton, cleanSnapshot())).toMatchSnapshot();

    const unableButton = mount(
      <Provider store={store}>
        <InviteTalentButton message={MESSAGE_TYPE.UNABLE} />
      </Provider>,
    );
    expect(toJson(unableButton, cleanSnapshot())).toMatchSnapshot();
  });
});
