import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {InviteToApplyModal, InviteToApplyModalProps} from '../InviteToApplyModal';
import {IInviteToApplyModalContainerDispatchProps} from '../InviteToApplyModalContainer';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import * as postInvitationsBatch from '~/services/invitations/postInvitationsBatch';

describe('InviteTalent/InviteToApplyModal', () => {
  const onTalentSearchCandidateInvitedMock = jest.fn();
  const onCancelMock = jest.fn();
  const onOkMock = jest.fn();

  const postInvitationsBatchMock = jest.spyOn(postInvitationsBatch, 'postInvitationsBatch');

  const defaultProps: InviteToApplyModalProps & IInviteToApplyModalContainerDispatchProps = {
    talent: 'talentuuid',
    jobs: [jobPostMock],
    invitations: [],
    onCancel: onCancelMock,
    onOk: onOkMock,
    onTalentSearchCandidateInvited: onTalentSearchCandidateInvitedMock,
  };

  const mountComponent = () =>
    mount(
      <Provider store={configureStore()()}>
        <InviteToApplyModal {...defaultProps} />
      </Provider>,
    );

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should call onTalentSearchCandidateInvited when postInvitationsBatch succeeds', async () => {
    postInvitationsBatchMock.mockImplementation((_, x) => Promise.resolve(new Response(x)));
    const component = mountComponent();

    component.find('[data-cy="job-to-invite-selection-checkbox"]').simulate('change', {target: {checked: true}});
    await act(async () => {
      component.find('[data-cy="invite-to-apply-modal-send-button"]').simulate('click');
    });
    component.update();

    expect(onTalentSearchCandidateInvitedMock).toBeCalledTimes(1);
  });

  it('should not call onTalentSearchCandidateInvited when postInvitationsBatch fails', async () => {
    postInvitationsBatchMock.mockImplementation((_, x) => Promise.reject(new Response(x)));
    const component = mountComponent();

    component.find('[data-cy="job-to-invite-selection-checkbox"]').simulate('change', {target: {checked: true}});
    await act(async () => {
      component.find('[data-cy="invite-to-apply-modal-send-button"]').simulate('click');
    });
    component.update();

    expect(onTalentSearchCandidateInvitedMock).toBeCalledTimes(0);
  });
});
