import {mount} from 'enzyme';
import React from 'react';

import {InvitationsSentList} from '~/components/InviteTalent/InvitationsSentList';
import {jobsMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPost} from '~/services/employer/jobs.types';

describe('InviteTalent/InvitationSentList', () => {
  it('should render InvitationsSentList correctly', () => {
    const jobs: IJobPost[] = [
      {...jobsMock[0], uuid: 'INVITATION_NOT_SENT'},
      {...jobsMock[0], uuid: 'INVITATION_SENT'},
    ];

    const payload = ['INVITATION_SENT'];
    const wrapper = mount(<InvitationsSentList payload={payload} jobs={jobs} />);
    expect(wrapper).toMatchSnapshot();
  });
});
