import React, {useState} from 'react';
import styles from '~/components/InviteTalent/JobToInviteItem.scss';
import {config} from '~/config';

type JobToInviteItemProps = {
  title: string;
  uuid: string;
  jobPostId: string;
  isInvitationSent: boolean;
  onClick?: (checked: boolean, uuid: string) => void;
};

export function JobToInviteItem({title, uuid, jobPostId, isInvitationSent, onClick}: JobToInviteItemProps) {
  const [selected, setSelected] = useState<boolean>(false);
  const JobToInviteItemSent = (
    <li
      className="flex items-center flex-auto ba pa3 mb2 bw1 b--black-0125 bg-black-05"
      data-cy="job-to-invite-item-sent"
    >
      <input
        data-cy="job-to-invite-selection-checkbox"
        id={`job-to-invite-selection-checkbox-${jobPostId}`}
        className="mr3"
        checked={true}
        disabled={true}
        type="checkbox"
      />
      <div className="justify-between items-center flex-auto relative">
        <h3 className="fw6 ma0 f4-5 lh-title truncate black-20">{title}</h3>
        <div className="db pt0 black-70 f6 lh-title black-20">{jobPostId}</div>
      </div>
      <div className="ml2">
        <span className="b black-20 nowrap">Invitation sent</span>
      </div>
    </li>
  );
  return isInvitationSent ? (
    JobToInviteItemSent
  ) : (
    <li
      className={`flex items-center flex-auto ba pa3 mb2 bw1 ${selected ? styles.active + '' : 'b--black-10'}`}
      data-cy="job-to-invite-item"
    >
      <input
        data-cy="job-to-invite-selection-checkbox"
        id={`job-to-invite-selection-checkbox-${jobPostId}`}
        onChange={(event) => {
          setSelected(event.target.checked);
          if (onClick) {
            onClick(event.target.checked, uuid);
          }
        }}
        className="mr3"
        type="checkbox"
      />
      <label
        htmlFor={`job-to-invite-selection-checkbox-${jobPostId}`}
        className="justify-between items-center flex-auto pointer relative"
      >
        <h3 className="fw6 ma0 f4-5 lh-title truncate">{title}</h3>
        <div className="db pt0 black-70 f6 lh-title">{jobPostId}</div>
      </label>
      <div className="ml2">
        <a
          data-cy="job-to-invite-view-job"
          href={`${config.url.mcf}/job/${uuid}`}
          rel="noopener noreferrer"
          target="_blank"
          title="View job"
          className={`blue underline nowrap ${styles.newWindow}`}
        >
          View job
        </a>
      </div>
    </li>
  );
}
