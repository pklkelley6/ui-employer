import React from 'react';
import {JobToInviteItem} from './JobToInviteItem';
import {IJobPost} from '~/services/employer/jobs.types';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';

type JobsToInviteListProps = {
  jobs: IJobPost[];
  invitations: IEmailInvitation[];
  onClick: (checked: boolean, uuid: string) => void;
};
export function JobsToInviteList({jobs, invitations, onClick}: JobsToInviteListProps) {
  const jobsNotInvitedYet: JSX.Element[] = [];
  const jobsInvited: JSX.Element[] = [];

  jobs.forEach(({title, metadata, uuid}) => {
    const jobFound = invitations.find((invitation) => invitation.jobUuid === uuid);
    const jobItem = (
      <JobToInviteItem
        title={title}
        jobPostId={metadata.jobPostId}
        isInvitationSent={!!jobFound}
        uuid={uuid}
        key={uuid}
        onClick={onClick}
      />
    );
    if (jobFound) {
      jobsInvited.push(jobItem);
    } else {
      jobsNotInvitedYet.push(jobItem);
    }
  });

  return <ul className="list pl0 overflow-y-auto vh-50">{jobsNotInvitedYet.concat(jobsInvited)}</ul>;
}
