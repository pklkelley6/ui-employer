import React from 'react';
import Tooltip from 'rc-tooltip';
import styles from './BookmarkToggle.scss';

interface IBookmarkToggleProps {
  input: {
    value: boolean;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  };
}

export const BookmarkToggle: React.FunctionComponent<IBookmarkToggleProps> = ({input}) => {
  const {value, onChange} = input;
  const BOOKMARK_DESCRIPTION_TEXT = value ? 'Click to unsave' : 'Click to save';
  return (
    <div data-cy="bookmark-candidate" className="tr">
      {value && <span className="secondary dib v-mid pr2">Saved</span>}
      <Tooltip
        id="bookmark-toggle-tooltip"
        placement="topRight"
        trigger={['hover', 'focus']}
        overlayClassName={`db absolute`}
        overlay={
          <span className={`bg-mid-gray white pa2 db mr1 f7 shadow-1 ${styles.bookmarkToggleTooltip}`}>
            {BOOKMARK_DESCRIPTION_TEXT}
          </span>
        }
        arrowContent={<div className={styles.bookmarkToggleTooltipArrow} />}
        destroyTooltipOnHide={true}
      >
        <div data-cy="bookmark-toggle" className="dib v-mid">
          <input
            id="bookmark-toggle-checkbox"
            type="checkbox"
            className={styles.bookmarkToggleCheckbox}
            checked={value}
            aria-label={BOOKMARK_DESCRIPTION_TEXT}
            onChange={onChange}
          />
          <label className={styles.bookmarkToggleLabel} htmlFor="bookmark-toggle-checkbox" />
        </div>
      </Tooltip>
    </div>
  );
};
