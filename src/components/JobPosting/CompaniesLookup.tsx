import {debounce} from 'lodash/fp';
import React, {useState} from 'react';
import {FieldInputProps, FieldMetaState} from 'react-final-form';
import Creatable from 'react-select/creatable';
import {SearchIcon} from '../Icon/SearchIcon';
import styles from './CompaniesLookup.scss';
import {GetCompanySuggestionQuery} from '~/services/employer/getCompanyInfo';

export interface ICompaniesLookupProps {
  placeholder?: string;
  label?: string;
  subtitle?: string;
  id: string;
  input: FieldInputProps<string, HTMLElement>;
  inputLabel?: string;
  meta: FieldMetaState<string>;
}

interface ICompanySuggestionOption {
  value: string;
  label: string;
}

export const CompaniesLookup: React.FunctionComponent<ICompaniesLookupProps> = ({
  id,
  input,
  inputLabel,
  meta,
  placeholder,
  subtitle,
  label,
}) => {
  const [suggestionInputValue, setSuggestionInputValue] = useState('');
  const {error = '', touched = true} = meta || {};
  return (
    <>
      {label && <label className="mb2 f5 fw6 black-60 pl1 db">{label}</label>}
      {subtitle && <span className="mv2 f6 fw4 black-50 pl1 db">{subtitle}</span>}
      <GetCompanySuggestionQuery name={suggestionInputValue}>
        {({data, loading}) => (
          <Creatable<ICompanySuggestionOption>
            className={`lookup-selector ${error && touched ? styles.fieldError : ''}`}
            classNamePrefix="lookup"
            components={{
              DropdownIndicator: () => <SearchIcon />,
            }}
            id={id}
            isLoading={loading}
            loadingMessage={() => null}
            isMulti={false}
            formatCreateLabel={(value: string) => `UEN: ${value}`}
            isValidNewOption={(value) => value.trim().length > 8 && value.trim().length < 11}
            selectValue={input.value}
            options={data?.results.map(({uen, name}) => ({value: uen, label: `${name} (${uen})`}))}
            noOptionsMessage={() => null}
            onBlur={() => input.onBlur()}
            onChange={(option) => {
              const value = option && (Array.isArray(option) ? option[0].value : option.value).trim();
              input.onBlur();
              input.onChange(value);
            }}
            onInputChange={debounce(300)((value) => setSuggestionInputValue(value))}
            placeholder={placeholder}
            value={inputLabel ? [{value: input.value, label: inputLabel}] : []}
          />
        )}
      </GetCompanySuggestionQuery>
      {error && touched && (
        <span id={`${id}-error`} className={`pt2 db red f5-5`}>
          {error}
        </span>
      )}
    </>
  );
};
