import {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {IAppState} from '~/flux';

type Step =
  | '#job-description'
  | '#skills'
  | '#key-information'
  | '#workplace-details'
  | '#screening-questions'
  | '#preview';

export interface IJobPostingStep {
  id: Step;
  description: string;
  next?: Step;
  previous?: Step;
}

export const jobPostingStepsDeprecated: IJobPostingStep[] = [
  {
    description: 'Job Description',
    id: '#job-description',
    next: '#skills',
  },
  {
    description: 'Skills',
    id: '#skills',
    next: '#key-information',
    previous: '#job-description',
  },
  {
    description: 'Key Information',
    id: '#key-information',
    next: '#workplace-details',
    previous: '#skills',
  },
  {
    description: 'Workplace Details',
    id: '#workplace-details',
    next: '#preview',
    previous: '#key-information',
  },
  {
    description: 'Review',
    id: '#preview',
    previous: '#workplace-details',
  },
];

export const jobPostingSteps: IJobPostingStep[] = [
  {
    description: 'Job Description',
    id: '#job-description',
    next: '#skills',
  },
  {
    description: 'Skills',
    id: '#skills',
    next: '#key-information',
    previous: '#job-description',
  },
  {
    description: 'Key Information',
    id: '#key-information',
    next: '#workplace-details',
    previous: '#skills',
  },
  {
    description: 'Workplace Details',
    id: '#workplace-details',
    next: '#screening-questions',
    previous: '#key-information',
  },
  {
    description: 'Screening Questions',
    id: '#screening-questions',
    next: '#preview',
    previous: '#workplace-details',
  },
  {
    description: 'Review',
    id: '#preview',
    previous: '#screening-questions',
  },
];

export const useJobPostingSteps = () => {
  const featureFlags = useSelector((state: IAppState) => state.features);
  const [currentJobPostingSteps, setCurrentJobPostingSteps] = useState(jobPostingSteps);

  useEffect(() => {
    if (featureFlags.screeningQuestions) {
      setCurrentJobPostingSteps(jobPostingSteps);
    } else {
      setCurrentJobPostingSteps(jobPostingStepsDeprecated);
    }
  }, [featureFlags]);

  return currentJobPostingSteps;
};
