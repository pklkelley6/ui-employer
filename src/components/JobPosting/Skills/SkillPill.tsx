import React from 'react';
import styles from './SkillPill.scss';

export interface ISkillPillProps {
  label: string;
  selected?: boolean;
}

export const SkillPill: React.FunctionComponent<ISkillPillProps> = ({label: textLabel, selected}) => (
  <div
    data-cy="skill-pill"
    className={`dib f5 br-pill mh1 mv1 pv1 ph3 ba b--primary ${styles.skillButton} ${selected ? styles.selected : ''}`}
  >
    <span>{textLabel}</span>
  </div>
);
