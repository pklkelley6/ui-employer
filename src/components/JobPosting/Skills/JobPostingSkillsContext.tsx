import {concat, differenceBy, noop, sortBy, take} from 'lodash';
import React, {createContext, useReducer} from 'react';
import {ActionType, createAction, getType} from 'typesafe-actions';
import {IJobSkill, IPillSkill} from '~/components/JobPosting/Skills/skills.types';

export const addSkill = createAction('ADD_SKILL')<IJobSkill>();
export const toggleAddedSkill = createAction('TOGGLE_ADDED_SKILL')<string>();
export const unselectAddedSkills = createAction('UNSELECT_ADDED_SKILLS')();

export const setFoundSkills = createAction('SET_FOUND_SKILLS')<IJobSkill[]>();

export const setRecommendedSkills = createAction(
  'SET_RECOMMENDED_SKILLS',
  (skills: IJobSkill[], isSelected: boolean) => ({skills, isSelected}),
)();
export const toggleRecommendedSkill = createAction('TOGGLE_RECOMMENDED_SKILL')<string>();
export const unselectRecommendedSkills = createAction('UNSELECT_RECOMMENDED_SKILLS')();

type IJobPostingSkillsActions = ActionType<
  | typeof addSkill
  | typeof toggleAddedSkill
  | typeof unselectAddedSkills
  | typeof setFoundSkills
  | typeof setRecommendedSkills
  | typeof toggleRecommendedSkill
  | typeof unselectRecommendedSkills
>;

export interface IJobPostingSkillsState {
  addedSkills: IPillSkill[];
  foundSkills: IPillSkill[];
  recommendedSkills: IPillSkill[];
}

const unselectSkills = (skills: IPillSkill[]) =>
  skills.map((skill) => ({
    ...skill,
    selected: false,
  }));

const toggleSkill = (skills: IPillSkill[], uuid: string) =>
  skills.map((skill) =>
    skill.uuid === uuid
      ? {
          ...skill,
          selected: !skill.selected,
        }
      : skill,
  );

const MAX_FOUND_SKILLS = 20;

export const reducer = (state: IJobPostingSkillsState, action: IJobPostingSkillsActions): IJobPostingSkillsState => {
  switch (action.type) {
    case getType(addSkill): {
      const selectedSkill = {...action.payload, selected: true};
      return {
        addedSkills: sortBy(concat(state.addedSkills, [selectedSkill]), ['skill']),
        foundSkills: sortBy(differenceBy(state.foundSkills, [selectedSkill], 'uuid'), ['skill']),
        recommendedSkills: differenceBy(state.recommendedSkills, [selectedSkill], 'uuid'),
      };
    }
    case getType(toggleAddedSkill):
      return {
        ...state,
        addedSkills: toggleSkill(state.addedSkills, action.payload),
      };
    case getType(unselectAddedSkills):
      return {
        ...state,
        addedSkills: unselectSkills(state.addedSkills),
      };

    case getType(setFoundSkills): {
      const foundSkills = sortBy(take(differenceBy(action.payload, state.addedSkills, 'uuid'), MAX_FOUND_SKILLS), [
        'skill',
      ]);
      return {
        ...state,
        foundSkills: foundSkills.map((foundSkill) => ({...foundSkill, selected: false})),
      };
    }

    case getType(setRecommendedSkills): {
      const recommendedSkills = differenceBy(action.payload.skills, state.addedSkills, 'uuid');
      return {
        ...state,
        recommendedSkills: recommendedSkills.map((recommendedSkill) => ({
          ...recommendedSkill,
          selected: action.payload.isSelected,
        })),
      };
    }
    case getType(toggleRecommendedSkill):
      return {
        ...state,
        recommendedSkills: toggleSkill(state.recommendedSkills, action.payload),
      };
    case getType(unselectRecommendedSkills):
      return {
        ...state,
        recommendedSkills: unselectSkills(state.recommendedSkills),
      };
    default:
      return state;
  }
};

export interface IJobPostingSkillsContext {
  state: IJobPostingSkillsState;
  dispatch: (action: IJobPostingSkillsActions) => void;
}

const defaultState: IJobPostingSkillsState = {
  addedSkills: [],
  foundSkills: [],
  recommendedSkills: [],
};

const defaultValue: IJobPostingSkillsContext = {
  dispatch: noop,
  state: defaultState,
};

export const JobPostingSkillsContext = createContext(defaultValue);

interface IJobPostingSkillsProvider {
  initialState: IJobPostingSkillsState;
}

export const JobPostingSkillsProvider: React.FunctionComponent<IJobPostingSkillsProvider> = ({
  children,
  initialState,
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return <JobPostingSkillsContext.Provider value={{state, dispatch}}>{children}</JobPostingSkillsContext.Provider>;
};
