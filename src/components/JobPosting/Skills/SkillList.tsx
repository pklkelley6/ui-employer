import React from 'react';
import {SkillButton} from './SkillButton';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
export interface ISkillListProps {
  list: IPillSkill[];
  onClick: (skill: IPillSkill) => void;
}

export const SkillList: React.FunctionComponent<ISkillListProps> = ({list, onClick}) => {
  const skills = list.map((skillItem) => {
    return (
      <SkillButton
        key={skillItem.uuid}
        label={skillItem.skill}
        selected={skillItem.selected}
        onClick={() => {
          onClick(skillItem);
        }}
      />
    );
  });
  return <div className="center mv3">{skills}</div>;
};
