import {isEmpty} from 'lodash/fp';
import React, {useContext, useState} from 'react';
import {useSelector} from 'react-redux';
import {ActionMeta, ValueType} from 'react-select/src/types';
import {SkillList} from './SkillList';
import {ISkillsSearchSectionProps} from './SkillsSearchSectionContainer';
import {IAppState} from '~/flux';
import {maxSelectedSkillsGrowl} from '~/components/GrowlNotification/GrowlNotification';
import {IOption, JobTitlesLookup} from '~/components/JobPosting/JobTitlesLookup';
import {
  addSkill,
  JobPostingSkillsContext,
  setFoundSkills,
} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import {MAX_SELECTED_SKILLS} from '~/components/JobPosting/Skills/skillsValidations';
import {getSuggestedSkillsByJob} from '~/services/skills/getSuggestedSkillsByJob';
import {getSkillsByJobTitle} from '~/services/skills/getSkillsByJobTitle';
import {getSkillsByQuery} from '~/services/skills/getSkillsByQuery';

export const SkillsSearchSection: React.FunctionComponent<ISkillsSearchSectionProps> = ({
  jobTitles,
  fetchJobTitlesRequested,
  removeJobTitlesResults,
}) => {
  const SKILLS_LIMIT = 20;
  const {
    state: {addedSkills, foundSkills, recommendedSkills},
    dispatch,
  } = useContext(JobPostingSkillsContext);
  const x0paSuggestedSkillsFeatureToggle = useSelector(({features}: IAppState) => features.x0paSuggestedSkills);

  const [searchedJobTitle, setSearchedJobTitle] = useState('');

  const fetchSkillsByTitle = async (jobTitle: string) => {
    const result = x0paSuggestedSkillsFeatureToggle
      ? await getSuggestedSkillsByJob({jobTitle, limit: SKILLS_LIMIT})
      : await getSkillsByJobTitle(jobTitle).then((result) => result.skills);
    dispatch(setFoundSkills(result));
  };

  const fetchSkillsByQuery = async (jobTitle: string) => {
    const result = await getSkillsByQuery(jobTitle);
    dispatch(setFoundSkills(result));
  };

  return (
    <section data-cy="input-job-skills">
      <h1 className="f5-5 black-60 mt5">Find and add skills by keyword or job title</h1>
      <JobTitlesLookup
        jobTitles={jobTitles.results}
        onChange={(option: ValueType<IOption>, {action}: ActionMeta) => {
          if (action === 'clear') {
            dispatch(setFoundSkills([]));
            removeJobTitlesResults();
            setSearchedJobTitle('');
          } else if (option) {
            const jobTitle = Array.isArray(option) ? option[0].label : option.label;
            setSearchedJobTitle(jobTitle);

            if (action === 'create-option') {
              fetchSkillsByQuery(jobTitle);
            } else {
              fetchSkillsByTitle(jobTitle);
            }
          }
        }}
        onInputChange={(value) => {
          const MIN_INPUT_LENGTH = 2;
          if (value.length > MIN_INPUT_LENGTH) {
            fetchJobTitlesRequested(value);
          }
        }}
      />
      <div data-cy="found-skills">
        <SkillList
          list={foundSkills}
          onClick={(skill) => {
            if (
              !skill.selected &&
              [...recommendedSkills, ...addedSkills].filter((s) => s.selected).length >= MAX_SELECTED_SKILLS
            ) {
              return maxSelectedSkillsGrowl(MAX_SELECTED_SKILLS);
            }
            dispatch(addSkill(skill));
          }}
        />
      </div>
      {isEmpty(foundSkills) && searchedJobTitle ? (
        <div data-cy="found-skills-empty" className="mt3 f5 gray ma0 pb2 display-linebreak">
          No skills found. Check to see if relevant skills have already been displayed above, or try finding another
          skill.
        </div>
      ) : null}
    </section>
  );
};
