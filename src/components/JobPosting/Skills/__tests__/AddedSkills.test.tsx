import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {noop} from 'lodash';
import React from 'react';
import {AddedSkills} from '../AddedSkills';
import {SkillList} from '../SkillList';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {JobPostingSkillsProvider} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('AddedSkills', () => {
  const props = {
    header: 'skills header',
    input: {
      onChange: jest.fn(),
    },
  };

  const addedSkills = [
    {uuid: '1', skill: 'addedSkill1', selected: true},
    {uuid: '2', skill: 'addedSkill2', selected: false},
  ];

  const skillsInitialState = {
    addedSkills: [],
    foundSkills: [],
    recommendedSkills: [],
  };

  it('should render added skills', () => {
    const wrapper = mount(
      <JobPostingContext.Provider
        value={{
          jobDescriptionChanged: false,
          setFormLoading: noop,
          setShowErrorCard: noop,
          showErrorCard: false,
        }}
      >
        <JobPostingSkillsProvider initialState={{...skillsInitialState, addedSkills}}>
          <AddedSkills {...props} />
        </JobPostingSkillsProvider>
      </JobPostingContext.Provider>,
    );
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should render placeholder when added skills is empty', () => {
    const wrapper = mount(
      <JobPostingContext.Provider
        value={{
          jobDescriptionChanged: false,
          setFormLoading: noop,
          setShowErrorCard: noop,
          showErrorCard: false,
        }}
      >
        <JobPostingSkillsProvider initialState={skillsInitialState}>
          <AddedSkills {...props} />
        </JobPostingSkillsProvider>
      </JobPostingContext.Provider>,
    );

    expect(wrapper.find(SkillList)).toHaveLength(0);
    expect(wrapper.find('[data-cy="added-skills-empty"]')).toHaveLength(1);
  });
});
