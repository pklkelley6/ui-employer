import {mount} from 'enzyme';
import React from 'react';
import {
  addSkill,
  JobPostingSkillsContext,
  JobPostingSkillsProvider,
  reducer,
  setFoundSkills,
  setRecommendedSkills,
  toggleAddedSkill,
  toggleRecommendedSkill,
  unselectAddedSkills,
  unselectRecommendedSkills,
} from '../JobPostingSkillsContext';

describe('JobPostingSkillsContext', () => {
  const initialState = {
    addedSkills: [
      {uuid: '1', skill: 'addedOne', selected: true},
      {uuid: '2', skill: 'addedTwo', selected: true},
    ],
    foundSkills: [
      {uuid: '3', skill: 'foundOne', selected: false},
      {uuid: '4', skill: 'foundTwo', selected: false},
    ],
    recommendedSkills: [
      {uuid: '5', skill: 'recommendedOne', selected: true},
      {uuid: '6', skill: 'recommendedTwo', selected: true},
    ],
  };

  describe('JobPostingSkillsProvider', () => {
    it('should set state to initialState', () => {
      mount(
        <JobPostingSkillsProvider initialState={initialState}>
          <JobPostingSkillsContext.Consumer>
            {({state}) => {
              expect(state).toEqual(initialState);
              return null;
            }}
          </JobPostingSkillsContext.Consumer>
        </JobPostingSkillsProvider>,
      );
    });
  });

  describe('reducer', () => {
    describe('addedSkills', () => {
      it('should add skill from payload and remove found skill if same with payload when addSkill is received', () => {
        const payload = {uuid: '3', skill: 'foundOne'};
        const expectedState = {
          ...initialState,
          addedSkills: [
            {uuid: '1', skill: 'addedOne', selected: true},
            {uuid: '2', skill: 'addedTwo', selected: true},
            {uuid: '3', skill: 'foundOne', selected: true},
          ],
          foundSkills: [{uuid: '4', skill: 'foundTwo', selected: false}],
        };
        expect(reducer(initialState, addSkill(payload))).toEqual(expectedState);
      });
      it('should add skill from payload and remove recommended skill if same with payload when addSkill is received', () => {
        const payload = {uuid: '5', skill: 'recommendedOne'};
        const expectedState = {
          ...initialState,
          addedSkills: [
            {uuid: '1', skill: 'addedOne', selected: true},
            {uuid: '2', skill: 'addedTwo', selected: true},
            {uuid: '5', skill: 'recommendedOne', selected: true},
          ],
          recommendedSkills: [{uuid: '6', skill: 'recommendedTwo', selected: true}],
        };
        expect(reducer(initialState, addSkill(payload))).toEqual(expectedState);
      });
      it('should toggle selected prop of specified skill when toggleAddedSkill is received', () => {
        const expectedState = {
          ...initialState,
          addedSkills: [
            {uuid: '1', skill: 'addedOne', selected: false},
            {uuid: '2', skill: 'addedTwo', selected: true},
          ],
        };
        expect(reducer(initialState, toggleAddedSkill('1'))).toEqual(expectedState);
      });
      it('should set selected prop of all skills to false when unselectRecommendedSkills is received', () => {
        const expectedState = {
          ...initialState,
          addedSkills: [
            {uuid: '1', skill: 'addedOne', selected: false},
            {uuid: '2', skill: 'addedTwo', selected: false},
          ],
        };
        expect(reducer(initialState, unselectAddedSkills())).toEqual(expectedState);
      });
    });

    describe('foundSkills', () => {
      it('should set foundSkills to the one in payload, dedupe with addedSkills, and set selected to false when setFoundSkills is received', () => {
        const payload = [
          {uuid: '1', skill: 'addedOne'},
          {uuid: '10', skill: 'foundThree'},
        ];
        const expectedState = {
          ...initialState,
          foundSkills: [{uuid: '10', skill: 'foundThree', selected: false}],
        };
        expect(reducer(initialState, setFoundSkills(payload))).toEqual(expectedState);
      });
    });

    describe('recommendedSkills', () => {
      it('should set recommendedSkills to the one in payload, dedupe with addedSkills, and set selected to isSelected when setRecommendedSkills is received', () => {
        const payload = [
          {uuid: '1', skill: 'addedOne'},
          {uuid: '5', skill: 'recommendedOne'},
        ];
        const expectedState = {
          ...initialState,
          recommendedSkills: [{uuid: '5', skill: 'recommendedOne', selected: true}],
        };
        expect(reducer(initialState, setRecommendedSkills(payload, true))).toEqual(expectedState);
      });
      it('should toggle selected prop of specified skill when toggleRecommendedSkill is received', () => {
        const expectedState = {
          ...initialState,
          recommendedSkills: [
            {uuid: '5', skill: 'recommendedOne', selected: false},
            {uuid: '6', skill: 'recommendedTwo', selected: true},
          ],
        };
        expect(reducer(initialState, toggleRecommendedSkill('5'))).toEqual(expectedState);
      });
      it('should set selected prop of all skills to false when unselectRecommendedSkills is received', () => {
        const expectedState = {
          ...initialState,
          recommendedSkills: [
            {uuid: '5', skill: 'recommendedOne', selected: false},
            {uuid: '6', skill: 'recommendedTwo', selected: false},
          ],
        };
        expect(reducer(initialState, unselectRecommendedSkills())).toEqual(expectedState);
      });
    });
  });
});
