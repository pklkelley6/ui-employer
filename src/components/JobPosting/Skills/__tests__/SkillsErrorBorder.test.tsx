import {mount} from 'enzyme';
import React from 'react';
import {SkillsErrorBorder} from '../SkillsErrorBorder';
import {
  JobPostingSkillsErrors,
  MAX_SELECTED_SKILLS,
  MIN_SELECTED_SKILLS,
} from '~/components/JobPosting/Skills/skillsValidations';

describe('SkillsErrorBorder', () => {
  it('should render error if received error is lessThanMinSkillsSelected', () => {
    const wrapper = mount(
      <SkillsErrorBorder error={JobPostingSkillsErrors.LessThanMinSkillsSelected}>
        <div>child</div>
      </SkillsErrorBorder>,
    );

    expect(wrapper.find('[data-cy="skills-error-title"]').text()).toEqual(
      `Please add at least ${MIN_SELECTED_SKILLS} skills`,
    );
  });

  it('should render error if received error is greaterThanMaxSkillsSelected', () => {
    const wrapper = mount(
      <SkillsErrorBorder error={JobPostingSkillsErrors.GreaterThanMaxSkillsSelected}>
        <div>child</div>
      </SkillsErrorBorder>,
    );

    expect(wrapper.find('[data-cy="skills-error-title"]').text()).toEqual(
      `Please add no more than ${MAX_SELECTED_SKILLS} skills`,
    );
  });

  it('should render error as is if received error is not greaterThanMaxSkillsSelected or lessThanMinSkillsSelected', () => {
    const wrapper = mount(
      <SkillsErrorBorder error={'someError'}>
        <div>child</div>
      </SkillsErrorBorder>,
    );

    expect(wrapper.find('[data-cy="skills-error-title"]').text()).toEqual('someError');
  });

  it('should convert an error object to string and display the correct error', () => {
    // error format returned by IE11 for react final form meta.error
    const error = {
      0: 'l',
      1: 'e',
      2: 's',
      3: 's',
      4: 'T',
      5: 'h',
      6: 'a',
      7: 'n',
      8: 'M',
      9: 'i',
      10: 'n',
      11: 'S',
      12: 'k',
      13: 'i',
      14: 'l',
      15: 'l',
      16: 's',
      17: 'S',
      18: 'e',
      19: 'l',
      20: 'e',
      21: 'c',
      22: 't',
      23: 'e',
      24: 'd',
    };

    const wrapper = mount(
      <SkillsErrorBorder error={error}>
        <div>child</div>
      </SkillsErrorBorder>,
    );
    expect(wrapper.find('[data-cy="skills-error-title"]').text()).toEqual(
      `Please add at least ${MIN_SELECTED_SKILLS} skills`,
    );
  });
});
