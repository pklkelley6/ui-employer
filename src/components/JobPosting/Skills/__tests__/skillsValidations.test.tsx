import {range} from 'lodash';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {
  greaterThanMaxSkillsSelected,
  JobPostingSkillsErrors,
  lessThanMinSkillsSelected,
} from '~/components/JobPosting/Skills/skillsValidations';

const validMinSkills: IJobPostingFormState['jobPostingSkills'] = {
  addedSkills: range(4).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
  recommendedSkills: range(4, 10).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
};

const validMaxSkills: IJobPostingFormState['jobPostingSkills'] = {
  addedSkills: range(11).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
  recommendedSkills: range(11, 20).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
};

const validMinAddedSkills: IJobPostingFormState['jobPostingSkills'] = {
  addedSkills: range(0, 10).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
  recommendedSkills: [],
};

const validMinRecommendedSkills: IJobPostingFormState['jobPostingSkills'] = {
  addedSkills: [],
  recommendedSkills: range(0, 10).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
};

describe('skillsValidations', () => {
  describe('lessThanMinSkillsSelected', () => {
    it('should return lessThanMinSkillsSelected if there are no skills', () => {
      const lessThanMinSkills = {
        addedSkills: [],
        recommendedSkills: [],
      };
      expect(lessThanMinSkillsSelected(lessThanMinSkills)).toEqual(JobPostingSkillsErrors.LessThanMinSkillsSelected);
    });

    it('should return lessThanMinSkillsSelected if there are 9 added skills and 0 recommended skills', () => {
      const lessThanMinSkills = {
        addedSkills: range(9).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
        recommendedSkills: [],
      };
      expect(lessThanMinSkillsSelected(lessThanMinSkills)).toEqual(JobPostingSkillsErrors.LessThanMinSkillsSelected);
    });

    it('should return lessThanMinSkillsSelected if there are 0 added skills and 9 recommended skills', () => {
      const lessThanMinSkills = {
        addedSkills: [],
        recommendedSkills: range(9).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
      };
      expect(lessThanMinSkillsSelected(lessThanMinSkills)).toEqual(JobPostingSkillsErrors.LessThanMinSkillsSelected);
    });

    it('should return lessThanMinSkillsSelected if there are 5 added skills and 4 recommended skills', () => {
      const lessThanMinSkills = {
        addedSkills: range(5).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
        recommendedSkills: range(5, 9).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
      };
      expect(lessThanMinSkillsSelected(lessThanMinSkills)).toEqual(JobPostingSkillsErrors.LessThanMinSkillsSelected);
    });

    it('should return lessThanMinSkillsSelected if there are 5 added skills, 4 recommended skills, and 1 unselected skill', () => {
      const skills = {
        addedSkills: range(5).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
        recommendedSkills: [
          ...range(5, 9).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
          {uuid: '9', skill: '9', selected: false},
        ],
      };
      expect(lessThanMinSkillsSelected(skills)).toEqual(JobPostingSkillsErrors.LessThanMinSkillsSelected);
    });

    it('should return undefined if there are 10 added skills and 0 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinAddedSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 0 added skills and 10 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinRecommendedSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 4 added skills and 6 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 11 added skills and 9 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMaxSkills)).toEqual(undefined);
    });
  });

  describe('greaterThanMaxSkillsSelected', () => {
    it('should return greaterThanMaxSkillsSelected if there are more than maximum skills selected in both added and recommended skills', () => {
      const greaterThanMaxSkillsValue = {
        addedSkills: range(10, 21).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
        recommendedSkills: range(10).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
      };
      expect(greaterThanMaxSkillsSelected(greaterThanMaxSkillsValue)).toEqual(
        JobPostingSkillsErrors.GreaterThanMaxSkillsSelected,
      );
    });

    it('should return greaterThanMaxSkillsSelected if there are 21 added skills and 0 recommended skills', () => {
      const greaterThanMaxSkillsValue = {
        addedSkills: range(21).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
        recommendedSkills: [],
      };
      expect(greaterThanMaxSkillsSelected(greaterThanMaxSkillsValue)).toEqual(
        JobPostingSkillsErrors.GreaterThanMaxSkillsSelected,
      );
    });

    it('should return greaterThanMaxSkillsSelected if there are 0 added skills and 21 recommended skills', () => {
      const greaterThanMaxSkillsValue = {
        addedSkills: [],
        recommendedSkills: range(21).map((index) => ({uuid: `${index}`, skill: `${index}`, selected: true})),
      };
      expect(greaterThanMaxSkillsSelected(greaterThanMaxSkillsValue)).toEqual(
        JobPostingSkillsErrors.GreaterThanMaxSkillsSelected,
      );
    });

    it('should return undefined if there are 10 added skills and 0 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinAddedSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 0 added skills and 10 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinRecommendedSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 4 added skills and 6 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMinSkills)).toEqual(undefined);
    });

    it('should return undefined if there are 11 added skills and 9 recommended skills', () => {
      expect(lessThanMinSkillsSelected(validMaxSkills)).toEqual(undefined);
    });
  });
});
