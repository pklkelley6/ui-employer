export interface IJobSkill {
  id?: number;
  uuid: string;
  skill: string;
}

export interface IPillSkill extends IJobSkill {
  selected: boolean;
}

export interface ISkillsByJobResponse {
  jobTitle: string;
  skills: IJobSkill[];
}

export interface IWCCSkillsByJobResponse {
  skills: IJobSkill[];
}
