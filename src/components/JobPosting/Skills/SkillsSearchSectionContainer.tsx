import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {SkillsSearchSection} from './SkillsSearchSection';
import {IAppState} from '~/flux/index';
import {fetchJobTitlesRequested, removeJobTitlesResults} from '~/flux/jobTitles/jobTitles.actions';
import {IJobTitlesResponse} from '~/flux/jobTitles/jobTitles.reducer';

export interface ISkillsSearchSectionDispatchProps {
  fetchJobTitlesRequested: (query: string) => ActionType<typeof fetchJobTitlesRequested>;
  removeJobTitlesResults: () => ActionType<typeof removeJobTitlesResults>;
}

export interface ISkillsSearchSectionStateProps {
  jobTitles: IJobTitlesResponse;
}

export type ISkillsSearchSectionProps = ISkillsSearchSectionStateProps & ISkillsSearchSectionDispatchProps;

export const mapStateToProps = ({jobTitles}: IAppState) => ({
  jobTitles,
});

export const SkillsSearchSectionContainer = connect(mapStateToProps, {
  fetchJobTitlesRequested,
  removeJobTitlesResults,
})(SkillsSearchSection);
