import React from 'react';
import styles from './SkillButton.scss';

export interface ISkillButtonProps {
  label: string;
  selected?: boolean;
  onClick?: (event: React.MouseEvent<HTMLElement>) => any;
}

export const SkillButton: React.FunctionComponent<ISkillButtonProps> = ({label: textLabel, selected, onClick}) => {
  const color = selected ? 'bg-primary white' : 'bg-white primary';
  const icon = selected ? styles.skillIconTick : styles.skillIconPlus;
  return (
    <div
      data-cy="skill-icon"
      className={`dib f5 br-pill mr2 pv1 pl2 pr3 mb1 ba b--primary ${color} ${styles.skillButton} ${icon}`}
      onClick={onClick}
    >
      <span>{textLabel}</span>
    </div>
  );
};
