import React, {useContext, useEffect} from 'react';
import styles from './SkillButton.scss';
import {SkillList} from './SkillList';
import {maxSelectedSkillsGrowl} from '~/components/GrowlNotification/GrowlNotification';
import {
  JobPostingSkillsContext,
  toggleAddedSkill,
  unselectAddedSkills,
} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import {MAX_SELECTED_SKILLS} from '~/components/JobPosting/Skills/skillsValidations';

interface IAddedSkillsProps {
  input: any;
  header: string;
}

export const AddedSkills: React.FunctionComponent<IAddedSkillsProps> = ({input, header}) => {
  const {
    state: {addedSkills, recommendedSkills},
    dispatch,
  } = useContext(JobPostingSkillsContext);

  const isSkillsEmpty = addedSkills.length === 0;

  // NOTE: trigger final-form at the end of reducer cycle
  useEffect(() => {
    input.onChange(addedSkills);
  }, [addedSkills]);

  return (
    <section data-cy="add-more-skills">
      <div className={`mt3 lh-title f6 black-60 fw7 dib ${styles.skillsHeader}`}>{header}</div>

      {isSkillsEmpty ? (
        <p data-cy="added-skills-empty" className="f5 mb4 i black-30">
          no skills added
        </p>
      ) : (
        <>
          <span
            data-cy="clear-all-skills"
            className="underline f7 ml2 pointer dark-blue fw6"
            onClick={() => dispatch(unselectAddedSkills())}
          >
            Deselect all skills
          </span>
          <div data-cy="added-skills">
            <SkillList
              list={addedSkills}
              onClick={(skill) => {
                if (
                  !skill.selected &&
                  [...recommendedSkills, ...addedSkills].filter((s) => s.selected).length >= MAX_SELECTED_SKILLS
                ) {
                  return maxSelectedSkillsGrowl(MAX_SELECTED_SKILLS);
                }
                dispatch(toggleAddedSkill(skill.uuid));
              }}
            />
          </div>
        </>
      )}
    </section>
  );
};
