import React from 'react';
import styles from './SkillsErrorBorder.scss';
import {
  JobPostingSkillsErrors,
  MAX_SELECTED_SKILLS,
  MIN_SELECTED_SKILLS,
} from '~/components/JobPosting/Skills/skillsValidations';

const SkillsErrorTitle: React.FunctionComponent<{error: string}> = ({error}) => {
  switch (error) {
    case JobPostingSkillsErrors.LessThanMinSkillsSelected:
      return (
        <span>
          Please add <b>at least {MIN_SELECTED_SKILLS}</b> skills
        </span>
      );
    case JobPostingSkillsErrors.GreaterThanMaxSkillsSelected:
      return (
        <span>
          Please add <b>no more than {MAX_SELECTED_SKILLS}</b> skills
        </span>
      );
    default:
      return <span>{error}</span>;
  }
};

interface ISkillsErrorBorder {
  children: React.ReactNode;
  error?: string | {[key: number]: string};
}

export const SkillsErrorBorder: React.FunctionComponent<ISkillsErrorBorder> = ({children, error}) => {
  let errorType = '';
  if (typeof error !== 'string') {
    // For handling scenario in IE11 where the returned error from react final form meta is
    // an object in the form of {1: 'e', 2: 'r', 3: 'r', 4: 'o', 5: 'r'} instead of a string.
    errorType = (error && Object.values(error).join('')) ?? '';
  } else {
    errorType = error;
  }
  return (
    <>
      {error ? (
        <div data-cy="skills-error-title" className={`mt3 w5 white bg-dark-red h2 pa2 f6 ${styles.skillsErrorTitle}`}>
          <SkillsErrorTitle error={errorType} />
        </div>
      ) : null}
      <div className={error ? styles.skillsErrorBorder : ''}>{children}</div>
    </>
  );
};
