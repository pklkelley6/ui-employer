import React, {useContext, useEffect, useState} from 'react';
import {useField} from 'react-final-form';
import {useSelector} from 'react-redux';
import styles from './SkillButton.scss';
import {SkillList} from './SkillList';
import {IAppState} from '~/flux';
import {maxSelectedSkillsGrowl} from '~/components/GrowlNotification/GrowlNotification';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {
  JobPostingSkillsContext,
  setRecommendedSkills,
  toggleRecommendedSkill,
  unselectRecommendedSkills,
} from '~/components/JobPosting/Skills/JobPostingSkillsContext';
import {MAX_SELECTED_SKILLS} from '~/components/JobPosting/Skills/skillsValidations';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {fetchRecommendedSkills} from '~/services/skills/fetchRecommendedSkills';
import {getSuggestedSkillsByJob} from '~/services/skills/getSuggestedSkillsByJob';

export interface IRecommendedSkillsProps {
  input: any;
  selected: boolean;
  header: string;
}

export const RecommendedSkills: React.FunctionComponent<IRecommendedSkillsProps> = ({input, selected, header}) => {
  const SKILLS_LIMIT = 16;
  const x0paSuggestedSkillsFeatureToggle = useSelector(({features}: IAppState) => features.x0paSuggestedSkills);
  const {jobDescriptionChanged, setFormLoading} = useContext(JobPostingContext);
  const {
    state: {recommendedSkills, addedSkills},
    dispatch,
  } = useContext(JobPostingSkillsContext);
  const [isLoading, setIsLoading] = useState(false);

  const {
    input: {
      value: {description: jobDescription = '', title: jobTitle = ''},
    },
  } = useField<IJobPostingFormState['jobDescription']>('jobDescription', {subscription: {value: true}});

  const isSkillsEmpty = recommendedSkills.length === 0;

  useEffect(() => {
    const onMount = async () => {
      if (jobDescriptionChanged) {
        setIsLoading(true);
        setFormLoading(true);
        const fetchedSkills = x0paSuggestedSkillsFeatureToggle
          ? await getSuggestedSkillsByJob({jobTitle, jobDescription, limit: SKILLS_LIMIT})
          : await fetchRecommendedSkills(jobTitle, jobDescription);
        dispatch(setRecommendedSkills(fetchedSkills, selected));
        setIsLoading(false);
        setFormLoading(false);
      }
    };
    onMount();
  }, []);

  useEffect(() => {
    input.onChange(recommendedSkills);
  }, [recommendedSkills]);

  return (
    <section data-cy="recommended-skills" className="mt3">
      <div>
        <div className={`lh-title f6 black-60 fw7 dib mt3 ${styles.skillsHeader}`}>{header}</div>
        <span
          data-cy="clear-all-skills"
          className={`underline f7 ml2 pointer dark-blue fw6 ${isLoading || isSkillsEmpty ? 'dn' : ''}`}
          onClick={() => dispatch(unselectRecommendedSkills())}
        >
          Deselect all skills
        </span>
      </div>
      {isLoading ? (
        <FormLoader className="o-50" hasTextAreaLoader={false} />
      ) : isSkillsEmpty ? (
        <p data-cy="recommended-skills-empty" className="f5 mb4 i black-30">
          no skills identified
        </p>
      ) : (
        <SkillList
          list={recommendedSkills}
          onClick={(skill) => {
            if (
              !skill.selected &&
              [...recommendedSkills, ...addedSkills].filter((s) => s.selected).length >= MAX_SELECTED_SKILLS
            ) {
              return maxSelectedSkillsGrowl(MAX_SELECTED_SKILLS);
            }
            dispatch(toggleRecommendedSkill(skill.uuid));
          }}
        />
      )}
    </section>
  );
};
