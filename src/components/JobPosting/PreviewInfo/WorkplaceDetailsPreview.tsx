import {capitalize} from 'lodash';
import React from 'react';
import {FormSpy} from 'react-final-form';
import {PreviewField} from './PreviewField';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';

export const WorkplaceDetailsPreview: React.FunctionComponent = () => {
  return (
    <FormSpy
      render={({values}) => {
        const {
          location,
          locationType,
          postalCode,
          block,
          street,
          building,
          overseasCountry,
          foreignAddress1,
          foreignAddress2,
        }: IJobPostingFormState['workplaceDetails'] = values.workplaceDetails;

        return (
          <>
            <div className="pa3 flex flex-wrap w-100">
              <PreviewField containerClass="w-100 pb2 mb4" label="Workplace Address">
                {capitalize(location)}
              </PreviewField>
              {location === Location.Local ? (
                <>
                  {locationType === LocationType.Multiple ? (
                    <PreviewField containerClass="w-100 pb2 mb3" label="Postal Code">
                      "Not Applicable"
                    </PreviewField>
                  ) : (
                    <>
                      <PreviewField containerClass="w-100 pb2 mb3" label="Postal Code">
                        {postalCode}
                      </PreviewField>
                      <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Block/House No.">
                        {block}
                      </PreviewField>
                      <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Street Name">
                        {street}
                      </PreviewField>
                      <PreviewField containerClass="w-100 pb2 mb3" label="Building Name (optional)">
                        {building}
                      </PreviewField>
                    </>
                  )}
                </>
              ) : (
                <>
                  <PreviewField containerClass="w-100 pb2 mb3" label="Country">
                    {overseasCountry}
                  </PreviewField>
                  <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Overseas Address 1">
                    {foreignAddress1}
                  </PreviewField>
                  <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Overseas Address 2">
                    {foreignAddress2}
                  </PreviewField>
                </>
              )}
            </div>
          </>
        );
      }}
    />
  );
};
