import {addDays, format} from 'date-fns';
import React from 'react';

export const AutoCloseDateSubtitle: React.FunctionComponent<{jobPostingDate?: string; jobPostDuration?: number}> = ({
  jobPostingDate,
  jobPostDuration,
}) => {
  let formattedDate = '';
  if (jobPostDuration && jobPostingDate) {
    formattedDate = format(addDays(new Date(jobPostingDate), jobPostDuration), 'd MMM yyyy');
  } else if (jobPostDuration) {
    formattedDate = format(addDays(new Date(), jobPostDuration), 'd MMM yyyy');
  }
  return (
    <div data-cy="autoclose-subtitle" className="f6 black-80">
      Auto-closes on: {formattedDate}
    </div>
  );
};
