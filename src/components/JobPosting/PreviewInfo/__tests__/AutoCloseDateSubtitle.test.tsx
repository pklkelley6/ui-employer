import {mount} from 'enzyme';
import React from 'react';
import {addDays, format} from 'date-fns';
import {AutoCloseDateSubtitle} from '../AutoCloseDateSubtitle';

describe('AutoCloseDateSubtitle', () => {
  const currentDatePlus30 = format(addDays(new Date(), 30), 'd MMM yyyy');
  it.each`
    jobPostingDate  | jobPostDuration | result
    ${'2020-01-01'} | ${30}           | ${'Auto-closes on: 31 Jan 2020'}
    ${null}         | ${30}           | ${`Auto-closes on: ${currentDatePlus30}`}
  `(
    'should render $result given jobPostingDate $jobPostingDate, jobPostDuration $jobPostDuration',
    ({jobPostingDate, jobPostDuration, result}) => {
      const wrapper = mount(
        <AutoCloseDateSubtitle jobPostingDate={jobPostingDate} jobPostDuration={jobPostDuration} />,
      );
      const rendered = wrapper.find(AutoCloseDateSubtitle);

      expect(rendered.text()).toBe(result);
    },
  );
});
