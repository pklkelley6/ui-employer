import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {ScreeningQuestionsPreview} from '../ScreeningQuestionsPreview';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {PreviewField} from '~/components/JobPosting/PreviewInfo/PreviewField';

describe('ScreeningQuestionsPreview', () => {
  it('should render all screening questions', async () => {
    const questionsMock = ['question one', 'question two', 'question three'];
    const initialValues: Partial<IJobPostingFormState> = {
      screeningQuestions: {
        includeScreeningQuestions: true,
        questions: questionsMock,
      },
    };
    const wrapper = mount(
      <Form onSubmit={noop} initialValues={initialValues} render={() => <ScreeningQuestionsPreview />} />,
    );

    const previewFields = wrapper.find(PreviewField);
    expect(previewFields).toHaveLength(initialValues.screeningQuestions!.questions.length);

    previewFields.forEach((previewField, index) => {
      expect(previewField.find('span').first().text()).toEqual(questionsMock[index]);
    });
  });

  it('should render empty state if includeScreeningQuestions is false', async () => {
    const questionsMock = ['question one', 'question two', 'question three'];
    const initialValues: Partial<IJobPostingFormState> = {
      screeningQuestions: {
        includeScreeningQuestions: false,
        questions: questionsMock,
      },
    };
    const wrapper = mount(
      <Form onSubmit={noop} initialValues={initialValues} render={() => <ScreeningQuestionsPreview />} />,
    );

    expect(wrapper.find(PreviewField)).toHaveLength(0);
    expect(wrapper.find('[data-cy="screening-questions-preview-empty"]')).toHaveLength(1);
  });
});
