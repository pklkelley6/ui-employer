import React from 'react';
import {compact} from 'lodash/fp';
import {useFormState} from 'react-final-form';
import {PreviewField} from './PreviewField';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

export const ScreeningQuestionsPreview: React.FunctionComponent = () => {
  const {
    values: {screeningQuestions},
  } = useFormState<IJobPostingFormState>({subscription: {values: true}});
  const questions = screeningQuestions.includeScreeningQuestions ? compact(screeningQuestions.questions) : [];

  return (
    <div className="pa3 flex flex-wrap w-100">
      {questions.length > 0 ? (
        questions.map((question, index) => (
          <PreviewField key={index} containerClass="w-100 pb2 mb4 break-word" label={`Question ${index + 1}`}>
            <span>{question}</span>
            <br />
            <span className="black-40">(Yes/No)</span>
          </PreviewField>
        ))
      ) : (
        <div data-cy="screening-questions-preview-empty" className="black-40 center">
          There are no screening questions for this job posting.
        </div>
      )}
    </div>
  );
};
