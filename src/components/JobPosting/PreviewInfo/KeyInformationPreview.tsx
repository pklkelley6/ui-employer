import {mcf} from '@mcf/constants';
import React from 'react';
import {Query} from 'react-apollo';
import {FormSpy} from 'react-final-form';
import {PreviewField} from './PreviewField';
import {AutoCloseDateSubtitle} from './AutoCloseDateSubtitle';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {GetCommonEducationQuery, GetCommonEducationQueryVariables} from '~/graphql/__generated__/types';
import {GET_EDUCATION_LIST} from '~/graphql/jobs/jobs.query';
import {GetSubSchemeById} from '~/services/schemes/getCompanySchemes';
import {NULL_PLACEHOLDER} from '~/util/constants';
import {JobStatusCodes} from '~/services/employer/jobs.types';

interface IKeyInformationPreviewProps {
  jobStatusId?: JobStatusCodes;
}

export const KeyInformationPreview: React.FC<IKeyInformationPreviewProps> = ({jobStatusId}) => {
  return (
    <FormSpy
      render={({values}) => {
        const {
          jobPostDuration,
          numberOfVacancies,
          jobCategories,
          minimumYearsExperience,
          positionLevel,
          employmentType,
          qualification,
          fieldOfStudy,
          minimumSalary,
          maximumSalary,
          schemes,
          newPostingDate,
        }: IJobPostingFormState['keyInformation'] = values.keyInformation;

        const selectedSchemes = schemes.filter((scheme) => scheme.selected);

        return (
          <>
            <div className="flex flex-wrap w-100 pa3">
              <div className="pr2 w-50 pb2 mb4">
                <PreviewField label="Job Post Duration">{`${jobPostDuration} Calendar Days`}</PreviewField>
                {jobStatusId !== JobStatusCodes.Closed ? (
                  <AutoCloseDateSubtitle jobPostingDate={newPostingDate} jobPostDuration={jobPostDuration} />
                ) : null}
              </div>

              <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Number of Vacancies">
                {String(numberOfVacancies)}
              </PreviewField>
              <PreviewField containerClass="w-100 pb2 mb3" label="Job Function">
                {jobCategories
                  .map(
                    (jobCategoryId: number) =>
                      (
                        mcf.JOB_CATEGORIES.find((jobCategory) => jobCategory.id === jobCategoryId) || {
                          category: NULL_PLACEHOLDER,
                        }
                      ).category,
                  )
                  .join(', ')}
              </PreviewField>
              <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Position Level">
                {
                  (mcf.POSITION_LEVELS.find((level) => level.id === positionLevel) || {position: NULL_PLACEHOLDER})
                    .position
                }
              </PreviewField>
              <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Minimum Years of Experience">
                {String(minimumYearsExperience)}
              </PreviewField>
              <PreviewField containerClass="w-100 pb2 mb3" label="Employment Type">
                {employmentType
                  ? employmentType
                      .map(
                        (employmentTypeId: number) =>
                          (
                            mcf.EMPLOYMENT_TYPES.find((employment) => employment.id === employmentTypeId) || {
                              employmentType: NULL_PLACEHOLDER,
                            }
                          ).employmentType,
                      )
                      .join(', ')
                  : NULL_PLACEHOLDER}
              </PreviewField>
              <div className="b--black-10 bb mt3 mb4 w-100" />

              <Query<GetCommonEducationQuery, GetCommonEducationQueryVariables> query={GET_EDUCATION_LIST}>
                {({loading, error, data}) => {
                  if (loading || error || !data) {
                    return (
                      <div className="w-100">
                        <FormLoader className="o-50" cardNumber={1} />
                      </div>
                    );
                  }

                  return (
                    <>
                      <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Minimum Qualification Level">
                        {
                          (
                            data.common.ssecEqaList.find((item) => item.actualCode === qualification) || {
                              description: NULL_PLACEHOLDER,
                            }
                          ).description
                        }
                      </PreviewField>
                      {qualification && mcf.ssecFosRequired(qualification) && (
                        <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Field of Study">
                          {
                            (
                              data.common.ssecFosList.find((item) => item.actualCode === fieldOfStudy) || {
                                description: NULL_PLACEHOLDER,
                              }
                            ).description
                          }
                        </PreviewField>
                      )}
                    </>
                  );
                }}
              </Query>
              <div className="b--black-10 bb mt3 mb4 w-100" />
              <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Monthly Salary Range (SGD)">
                {`$${minimumSalary ? minimumSalary.toLocaleString() : minimumSalary} - ${
                  maximumSalary ? maximumSalary.toLocaleString() : maximumSalary
                }`}
              </PreviewField>

              <div className="b--black-10 bb mt3 mb4 w-100" />

              <div className="pa2 w-100">
                <h5 className="ma0 black-50 f6 lh-title normal">Government Support</h5>
                {selectedSchemes.length ? (
                  <ul>
                    {selectedSchemes
                      .filter((selectedScheme) => mcf.SCHEMES.map((scheme) => scheme.id).includes(selectedScheme.id))
                      .map((selectedScheme, index) => {
                        const schemeName = mcf.SCHEMES.find((scheme) => scheme.id === selectedScheme.id)!.scheme;
                        return (
                          <li key={index}>
                            {schemeName}
                            {selectedScheme.subSchemeId ? (
                              <GetSubSchemeById schemeId={selectedScheme.id} subSchemeId={selectedScheme.subSchemeId}>
                                {({data}) => <>{data ? ` (${data.programme})` : ''}</>}
                              </GetSubSchemeById>
                            ) : null}
                          </li>
                        );
                      })}
                  </ul>
                ) : (
                  <p className="i black-60">No schemes selected</p>
                )}
              </div>
            </div>
          </>
        );
      }}
    />
  );
};
