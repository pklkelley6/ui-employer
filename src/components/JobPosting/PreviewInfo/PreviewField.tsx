import React from 'react';

interface IPreviewFieldProps {
  containerClass?: string;
  label?: string;
  children?: React.ReactNode;
}

export const PreviewField: React.FunctionComponent<IPreviewFieldProps> = ({
  containerClass = '',
  label = '',
  children = '',
}) => {
  return (
    <div className={containerClass}>
      <label className="ma0 black-50 f5-5 lh-title normal">{label}</label>
      <p className="f4-5 mv2 black-80 lh-copy">{children}</p>
    </div>
  );
};
