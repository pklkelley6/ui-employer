import React from 'react';
import {FormSpy} from 'react-final-form';
import {SkillPill} from '../Skills/SkillPill';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

export const SkillsPreview: React.FunctionComponent = () => {
  return (
    <FormSpy
      render={({values}) => {
        const {addedSkills, recommendedSkills}: IJobPostingFormState['jobPostingSkills'] = values.jobPostingSkills;
        const skills = [...recommendedSkills, ...addedSkills];

        return skills.length ? (
          skills
            .filter((skill) => skill.selected)
            .map((item) => {
              return <SkillPill label={item.skill} selected={true} />;
            })
        ) : (
          <span>There are no selected skills</span>
        );
      }}
    />
  );
};
