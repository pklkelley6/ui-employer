import {find} from 'lodash';
import React from 'react';
import {Query} from 'react-apollo';
import {FormSpy} from 'react-final-form';
import {sanitize} from 'dompurify';
import {PreviewField} from './PreviewField';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {
  GetCommonQuery,
  GetCommonQueryVariables,
  GetCommonSsicQuery,
  GetCommonSsicQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_SSIC_LIST, GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {NULL_PLACEHOLDER} from '~/util/constants';

export interface ISsocProps {
  ssoc: number;
  ssocTitle: string;
}

export const JobDescriptionPreview: React.FunctionComponent = () => {
  return (
    <FormSpy
      render={({values}) => {
        const {
          title,
          occupation,
          description,
          otherRequirements,
          isThirdPartyEmployer,
          thirdPartyEmployer,
        }: IJobPostingFormState['jobDescription'] = values.jobDescription;
        const combinedDescription = `${description}${otherRequirements ?? ''}`;
        return (
          <>
            {isThirdPartyEmployer && thirdPartyEmployer.entityId ? (
              <div className="flex flex-wrap pa3">
                <h5 className="black-60 f4-5 fw6 lh-copy mt2 mb4 normal">Posting on behalf of another company</h5>
                <PreviewField containerClass=" w-100 pb2 mb3" label="Company Name / UEN">
                  {thirdPartyEmployer.name || thirdPartyEmployer.entityId}
                </PreviewField>
                <Query<GetCommonSsicQuery, GetCommonSsicQueryVariables>
                  query={GET_SSIC_LIST}
                  variables={{code: thirdPartyEmployer.industry}}
                  skip={!thirdPartyEmployer.industry}
                >
                  {({data: ssicList, loading: ssicListLoading}) => {
                    if (ssicListLoading) {
                      return <FormLoader className="o-50" />;
                    }
                    const ssic = ssicList?.common.ssicList.find(({code}) => code === thirdPartyEmployer.industry);
                    return (
                      <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Industry">
                        {(ssic ? ssic.description : thirdPartyEmployer.industry) || 'Not Applicable'}
                      </PreviewField>
                    );
                  }}
                </Query>
              </div>
            ) : null}
            <Query<GetCommonQuery, GetCommonQueryVariables> query={GET_SSOC_LIST}>
              {({loading, error, data}) => {
                if (loading || error || !data) {
                  return <FormLoader className="o-50" />;
                }
                return (
                  <div className="flex flex-wrap pa3">
                    <PreviewField containerClass="pr2 w-50 pb2 mb3" label="Job Title">
                      {title}
                    </PreviewField>
                    <PreviewField containerClass="pl2 w-50 pb2 mb3" label="Occupation">
                      {(
                        find(data.common.ssocList, (item) => item.ssoc === occupation) || {
                          ssocTitle: NULL_PLACEHOLDER,
                        }
                      ).ssocTitle || NULL_PLACEHOLDER}
                    </PreviewField>
                    <div className="w-100">
                      <h5 className="ma0 black-50 f6 lh-title normal">Job Description & Requirements</h5>
                      <div
                        className="mv2 break-word"
                        dangerouslySetInnerHTML={{__html: sanitize(combinedDescription) || NULL_PLACEHOLDER}}
                      />
                    </div>
                  </div>
                );
              }}
            </Query>
          </>
        );
      }}
    />
  );
};
