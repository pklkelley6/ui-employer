import React, {CSSProperties} from 'react';
import Creatable from 'react-select/creatable';
import {ThemeConfig} from 'react-select/src/theme';
import {ActionMeta, ValueType} from 'react-select/src/types';
import {SearchIcon} from '~/components/Icon/SearchIcon';
import {IJobTitle} from '~/flux/jobTitles/jobTitles.reducer';

export interface IOption {
  value: string;
  label: string;
  __isNew__?: boolean;
}

export interface IJobTitlesLookupProps {
  placeholder?: string;
  jobTitles: IJobTitle[];
  onInputChange: (newValue: string) => void;
  onChange: (value: ValueType<IOption>, action: ActionMeta) => void;
  defaultValue?: {value: string; label: string};
}

const reactSelectTheme: ThemeConfig = (theme) => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary25: '#e3d7f4',
    primary50: '#e3d7f4',
  },
});

const ClearIndicatorStyles = (base: CSSProperties) => ({
  ...base,
  color: '#777777',
  cursor: 'pointer',
  fontSize: '18px',
});

export const JobTitlesLookup: React.FunctionComponent<IJobTitlesLookupProps> = ({
  jobTitles,
  onInputChange,
  onChange,
  defaultValue,
  placeholder = "e.g. 'Accounting' or 'Accountant'",
}) => {
  return (
    <Creatable
      className="lookup-selector"
      classNamePrefix="lookup"
      createOptionPosition="first"
      styles={{clearIndicator: ClearIndicatorStyles}}
      components={{
        ClearIndicator: (props) => {
          const {
            children = '×',
            getStyles,
            innerProps: {ref, ...restInnerProps},
          } = props;
          return (
            <div {...restInnerProps} ref={ref} style={getStyles('clearIndicator', props)}>
              <div style={{padding: '0px 5px'}}>{children}</div>
            </div>
          );
        },
        DropdownIndicator: () => <SearchIcon />,
      }}
      formatCreateLabel={(input: string) => input}
      formatOptionLabel={(option) =>
        option.__isNew__ ? (
          <>
            Skills containing <strong>'{option.label}'</strong>
          </>
        ) : (
          <>
            <strong>{option.label}</strong> skills
          </>
        )
      }
      options={jobTitles.map((jobTitle) => {
        return {
          label: jobTitle.jobTitle,
          value: jobTitle.id + '',
        };
      })}
      onInputChange={onInputChange}
      onChange={onChange}
      defaultValue={defaultValue}
      placeholder={placeholder}
      noOptionsMessage={() => null}
      theme={reactSelectTheme}
      hideSelectedOptions={true}
      isClearable
    />
  );
};
