import React from 'react';
import {Field} from 'react-final-form';
import {CheckBox, TextInput} from '@govtechsg/mcf-mcfui';
import {useFieldArray} from 'react-final-form-arrays';
import styles from './ScreeningQuestionsFields.scss';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {Condition} from '~/components/Core/Condition';
import {composeValidators, maxLength, minLength, defaultRequired} from '~/util/fieldValidations';

const QUESTION_MAX_LENGTH = 120;
const QUESTION_MIN_LENGTH = 5;

const MIN_QUESTIONS = 1;
const MAX_QUESTIONS = 3;

export const ScreeningQuestionsFields: React.FunctionComponent = () => {
  const {fields} =
    useFieldArray<IJobPostingFormState['screeningQuestions']['questions'][0]>('screeningQuestions.questions');

  const isRemoveDisabled = fields.value.length <= MIN_QUESTIONS;
  const isAddDisabled = fields.value.length >= MAX_QUESTIONS;

  return (
    <section>
      <div className="flex w-100">
        <Field<
          IJobPostingFormState['screeningQuestions']['includeScreeningQuestions']
        > name="screeningQuestions.includeScreeningQuestions">
          {({input}) => {
            return (
              <CheckBox
                id="screening-questions-checkbox"
                label="Include screening questions for applicants to answer (optional)"
                input={input}
              />
            );
          }}
        </Field>
      </div>
      <div className={`${styles.checkboxLabel} black-60 pt2`}>
        This will allow you to include up to <b className="fw6">3 Yes/No</b> questions.
      </div>
      <Condition when="screeningQuestions.includeScreeningQuestions" is={true}>
        <>
          <ul className="list pl0 ma0">
            {fields.map((name, index) => (
              <li key={name} data-cy="screening-questions-field" className="flex justify-between pa3 bg-white br3 mv4">
                <div className={`pa2 ${styles.textField}`}>
                  <Field<IJobPostingFormState['screeningQuestions']['questions'][0]>
                    name={`${name}`}
                    validate={composeValidators(
                      defaultRequired,
                      minLength(
                        QUESTION_MIN_LENGTH,
                        `Please enter at least ${QUESTION_MIN_LENGTH.toLocaleString()} characters`,
                      ),
                      maxLength(
                        QUESTION_MAX_LENGTH,
                        `Please keep within ${QUESTION_MAX_LENGTH.toLocaleString()} characters`,
                      ),
                    )}
                  >
                    {({input, meta}) => (
                      <TextInput
                        id="screening-question"
                        label="Question"
                        placeholder="Please enter a question"
                        input={input}
                        meta={meta}
                        containerClassName={'ma0'}
                      />
                    )}
                  </Field>
                  <div className="pt1 black-40">Applicants will choose between Yes/No</div>
                </div>
                <div className={`${styles.removeButton} ph3`}>
                  <button
                    data-cy="screening-questions-remove"
                    className={`red fw6 bn bg-inherit ${isRemoveDisabled ? 'o-40' : 'pointer'}`}
                    type="button"
                    onClick={() => fields.remove(index)}
                    disabled={isRemoveDisabled}
                  >
                    Remove
                  </button>
                </div>
              </li>
            ))}
          </ul>
          <button
            data-cy="screening-questions-add"
            className={`blue fw6 bn bg-inherit ${isAddDisabled ? 'o-40' : 'pointer'}`}
            type="button"
            onClick={() => fields.push('')}
            disabled={isAddDisabled}
          >
            <i title="add-icon" className={`${styles.blueAddIcon} mr2`} />
            Add a question
          </button>
        </>
      </Condition>
    </section>
  );
};
