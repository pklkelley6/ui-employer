import React from 'react';
import {useFieldArray} from 'react-final-form-arrays';
import styles from './ScreeningQuestionsFields.scss';
import {ScreeningQuestionsToolTip} from './ScreeningQuestionToolTip';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {Condition} from '~/components/Core/Condition';

export const ReadOnlyScreeningQuestionsFields: React.FunctionComponent = () => {
  const {fields} =
    useFieldArray<IJobPostingFormState['screeningQuestions']['questions'][0]>('screeningQuestions.questions');

  return (
    <section>
      <Condition when="screeningQuestions.includeScreeningQuestions" is={true}>
        <>
          <ul className="list pl0 ma0">
            {fields.value.map((question, index) => (
              <li key={index} data-cy="screening-questions-field" className="flex justify-between pa3 bg-white br3 mv4">
                <div className={`pa2 ${styles.textField}`}>
                  <div className="pb3">Question {index + 1}</div>
                  <div className={`pa2 br2 bg-light-gray ${styles.questionBox}`}>
                    <div className="pa2">{question}</div>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </>
      </Condition>
      <Condition when="screeningQuestions.includeScreeningQuestions" is={false}>
        <>
          <div data-cy="screening-questions-preview-empty" className="black-40 flex flex-column pa6 items-center">
            <p className="f4 fw6">There are no screening questions for this job posting.</p>
          </div>
        </>
      </Condition>
      <ScreeningQuestionsToolTip />
    </section>
  );
};
