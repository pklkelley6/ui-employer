import Tooltip from 'rc-tooltip';
import React from 'react';
import styles from './ScreeningQuestionsToolTip.scss';

export const ScreeningQuestionsToolTip: React.FunctionComponent = () => {
  return (
    <Tooltip
      id="screening-questions-tooltip"
      placement="top"
      trigger={['hover']}
      overlayClassName={`db absolute z-5 bg-white black-60 shadow-1 ${styles.customOverlay}`}
      destroyTooltipOnHide={true}
      overlay={
        <div className="relative pa3 lh-copy db">
          <div className={styles.removeScreeningQuestionsImage} />
          <span className="f6 fw6">
            After a job posting has been submitted, screening questions can only be removed but not edited. To remove
            screening questions, go to the specific job posting, find Manage Job and click on "Remove Screening
            Questions".
          </span>
        </div>
      }
    >
      <span className="underline f6-5 black-80 db pt2 dib">Why can't I edit screening questions?</span>
    </Tooltip>
  );
};
