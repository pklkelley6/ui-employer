import Tooltip from 'rc-tooltip';
import React from 'react';
import styles from './JobPostDurationToolTip.scss';

export const JobPostDurationToolTip: React.FunctionComponent = () => {
  return (
    <Tooltip
      id="job-duration-tooltip"
      placement="top"
      trigger={['hover']}
      overlayClassName={`db absolute z-5 bg-white black-60 shadow-1 ${styles.customOverlay}`}
      destroyTooltipOnHide={true}
      overlay={
        <div className="relative pa3 lh-copy db">
          <div className={styles.extendJobDurationImage} />
          <span className="f6 fw6">
            Extending the duration does not count as an edit. To extend the duration, go to the job overview page, find
            Manage Job and click on Extend Posting Duration.
          </span>
        </div>
      }
    >
      <span className="underline f6-5 black-80 db pt2 dib">Why can't I change my job post duration?</span>
    </Tooltip>
  );
};
