import {mcf} from '@mcf/constants';
import {get} from 'lodash/fp';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {searchableFieldRequired, validateIf} from '~/util/fieldValidations';

export const requiredIfQualificationIsAboveALevels = validateIf(
  'keyInformation.qualification',
  mcf.ssecFosRequired,
  searchableFieldRequired,
);

export const minNotMoreThanMax = (value: IJobPostingFormState['keyInformation']) => {
  const minimumSalary = get('minimumSalary', value);
  const maximumSalary = get('maximumSalary', value);
  return minimumSalary && maximumSalary && minimumSalary > maximumSalary
    ? {
        maximumSalary: ' ', // need a non-empty value here for red border
        minMaxRange: 'Please check that the Min is not greater than the Max',
        minimumSalary: undefined,
      }
    : undefined;
};
