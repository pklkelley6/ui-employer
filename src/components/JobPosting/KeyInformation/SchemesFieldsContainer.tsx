import {connect} from 'react-redux';
import {SchemesFields} from './SchemesFields';
import {COMPANY_FETCH_REQUESTED} from '~/flux/company/company.constants';
import {IAppState} from '~/flux/index';

const mapStateToProps = ({company}: IAppState) => ({
  isLoading: company.fetchStatus === COMPANY_FETCH_REQUESTED,
  uen: company && company.companyInfo.uen,
});

export default connect(mapStateToProps)(SchemesFields);
