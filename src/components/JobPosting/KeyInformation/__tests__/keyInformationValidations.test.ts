import {minNotMoreThanMax, requiredIfQualificationIsAboveALevels} from '../keyInformationValidations';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

describe('keyInformationValidations', () => {
  const initialKeyInfoState: IJobPostingFormState['keyInformation'] = {
    newPostingDate: '2021-10-01',
    jobCategories: [],
    maximumSalary: 456,
    minimumSalary: 123,
    schemes: [],
  };
  describe('minNotMoreThanMax', () => {
    it.each`
      minimumSalary | maximumSalary
      ${undefined}  | ${undefined}
      ${1}          | ${undefined}
      ${undefined}  | ${1}
      ${0}          | ${0}
      ${1}          | ${1}
      ${1}          | ${0}
      ${0}          | ${1}
      ${1}          | ${2}
    `(
      'should return undefined if minimumSalary is $minimumSalary and maximumSalary is $maximumSalary',
      ({minimumSalary, maximumSalary}) => {
        expect(minNotMoreThanMax({...initialKeyInfoState, minimumSalary, maximumSalary})).toEqual(undefined);
      },
    );

    it('should return object with error message if minimumSalary is more than maximumSalary', () => {
      expect(minNotMoreThanMax({...initialKeyInfoState, minimumSalary: 2, maximumSalary: 1})).toEqual({
        maximumSalary: ' ',
        minMaxRange: 'Please check that the Min is not greater than the Max',
        minimumSalary: undefined,
      });
    });
  });

  describe('requiredIfQualificationIsAboveALevels', () => {
    it.each`
      qualification
      ${'42'}
      ${'N'}
      ${'N9'}
      ${'05'}
      ${'51'}
    `(
      'should return object with error message if qualification is diploma and above ($qualification)',
      ({qualification}) => {
        const state = {keyInformation: {qualification}};
        expect(requiredIfQualificationIsAboveALevels(null, state)).toEqual('Please type and select');
      },
    );

    it.each`
      qualification
      ${'41'}
      ${'03'}
    `('should return undefined if qualification is below diploma ($qualification)', ({qualification}) => {
      const state = {keyInformation: {qualification}};
      expect(requiredIfQualificationIsAboveALevels(null, state)).toEqual(undefined);
    });
  });
});
