import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Form, FormSpy} from 'react-final-form';
import {SchemesFields} from '../SchemesFields';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import * as getCompanySchemes from '~/services/schemes/getCompanySchemes';

const getCompanySchemesMock = jest.spyOn(getCompanySchemes, 'getCompanySchemes');

const schemesUen = 'T08GB0046G';
const schemesMock: getCompanySchemes.ICompanyScheme[] = [
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.P_MAX,
      scheme: 'P-Max',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 1,
      programme: 'PCP for Public Transport Bus (Service Controller/ Assistant Service Controller)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 2,
      programme: 'PCP for Public Transport Rail Sector (Executive Engineer/ Assistant Engineer)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 3,
      programme: 'PCP for Public Transport Rail Sector (Station Manager/ Assistant Station Manager)',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.PCP,
      scheme: 'Professional Conversion Programme (PCP)',
    },
    startDate: '2019-01-01',
    subScheme: {
      id: 4,
      programme: 'PCP for Regional Operations Manager',
      schemeId: mcf.SCHEME_ID.PCP,
    },
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.CAREER_TRIAL,
      scheme: 'Career Trial',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.SG_UNITED_TRAINEESHIPS,
      scheme: 'SGUnited Traineeships',
    },
    startDate: '2019-01-01',
  },
  {
    expiryDate: '2050-01-01',
    scheme: {
      id: mcf.SCHEME_ID.SG_UNITED_MID_CAREER_PATHWAYS,
      scheme: 'SGUnited Mid-Career Pathways Programme',
    },
    startDate: '2019-01-01',
  },
];

describe('SchemeFields', () => {
  it('should render subschemes dropdown if a selected scheme has sub-schemes', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [{id: mcf.SCHEME_ID.PCP, selected: true}],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mountSchemesFields(initialValues);
    });
    wrapper.update();
    expect(wrapper.find('[data-cy="subschemes-dropdown"]')).toHaveLength(1);
  });

  it('should keep previous scheme selection if it is present in the fetched company schemes', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [
          {id: mcf.SCHEME_ID.CAREER_SUPPORT, selected: true},
          {id: mcf.SCHEME_ID.CAREER_TRIAL, selected: true},
        ],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mountSchemesFields(initialValues);
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    expectSchemesCheckBoxToBeCorrect(wrapper, schemesMock, [mcf.SCHEME_ID.CAREER_SUPPORT, mcf.SCHEME_ID.CAREER_TRIAL]);
  });

  it('should fetch schemes using third party uen if third party is selected', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve(schemesMock));
    const thirdPartyUen = 'THIRDPARTYUEN';
    const initialValues: Partial<IJobPostingFormState> = {
      jobDescription: {
        isThirdPartyEmployer: true,
        thirdPartyEmployer: {
          entityId: thirdPartyUen,
        },
      },
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [{id: mcf.SCHEME_ID.PCP, selected: true}],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mountSchemesFields(initialValues);
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(thirdPartyUen);
    expectSchemesCheckBoxToBeCorrect(wrapper, schemesMock, [mcf.SCHEME_ID.PCP]);
  });

  it('should remove scheme selection that is no longer present in the fetched company schemes', async () => {
    getCompanySchemesMock.mockImplementation(() => Promise.resolve([]));
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [
          {id: mcf.SCHEME_ID.CAREER_SUPPORT, selected: true},
          {id: mcf.SCHEME_ID.PCP, selected: true},
        ],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mount(
        <Form
          onSubmit={noop}
          initialValues={initialValues}
          render={() => (
            <>
              <SchemesFields isLoading={false} uen={schemesUen} />
              <FormSpy subscription={{values: true}}>
                {({values}) => <pre id="formState">{JSON.stringify(values)}</pre>}
              </FormSpy>
            </>
          )}
        />,
      );
    });
    wrapper.update();
    expect(getCompanySchemesMock).toBeCalledWith(schemesUen);
    const formState = JSON.parse(wrapper.find('#formState').text());
    expect(formState.keyInformation.schemes).toEqual([]);
  });

  it('should not render SchemesFields component when there is only career trial scheme (not for user selection) eligible', async () => {
    getCompanySchemesMock.mockImplementation(() =>
      Promise.resolve([
        {
          expiryDate: '2050-01-01',
          scheme: {
            id: mcf.SCHEME_ID.CAREER_TRIAL,
            scheme: 'Career Trial',
          },
          startDate: '2019-01-01',
        },
      ]),
    );
    const initialValues: Partial<IJobPostingFormState> = {
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        maximumSalary: 4000,
        schemes: [{id: mcf.SCHEME_ID.CAREER_TRIAL, selected: true}],
      },
    };

    let wrapper: any;
    await act(async () => {
      wrapper = mountSchemesFields(initialValues);
    });
    wrapper.update();

    expect(wrapper.find(SchemesFields).isEmptyRender()).toBe(true);
  });
});

const mountSchemesFields = (initialValues: Partial<IJobPostingFormState>) =>
  mount(
    <Form
      onSubmit={noop}
      initialValues={initialValues}
      render={() => <SchemesFields isLoading={false} uen={schemesUen} />}
    />,
  );

const expectSchemesCheckBoxToBeCorrect = (
  wrapper: any,
  schemesMock: getCompanySchemes.ICompanyScheme[],
  checkedSchemes: mcf.SCHEME_ID[],
) => {
  // should not display career trial scheme
  const careerTrialSchemeCheckBox = wrapper.find(`input#schemes-checkbox-${mcf.SCHEME_ID.CAREER_TRIAL}`);
  expect(careerTrialSchemeCheckBox).toHaveLength(0);

  schemesMock
    .filter(({scheme: {id}}) => id !== mcf.SCHEME_ID.CAREER_TRIAL)
    .forEach((schemeMock) => {
      const schemeCheckBox = wrapper.find(`input#schemes-checkbox-${schemeMock.scheme.id}`);
      expect(schemeCheckBox).toHaveLength(1);
      expect(schemeCheckBox.find('input').props().checked).toEqual(checkedSchemes.includes(schemeMock.scheme.id));
    });
};
