import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {print} from 'graphql';
import {PactBuilder} from '~/__mocks__/pact';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {CreateKeyInformationFields} from '~/components/JobPosting/KeyInformation/CreateKeyInformationFields';
import {GET_EDUCATION_LIST} from '~/graphql/jobs/jobs.query';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

describe('JobPosting/CreateKeyInformationFields', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  beforeEach(async () => {
    const queryMockResult = {
      data: {
        common: {
          ssecEqaList: Matchers.eachLike({actualCode: '1', description: 'JOHN DOE'}),
          ssecFosList: Matchers.eachLike({actualCode: '1', description: 'JANE DOE'}),
        },
      },
    };
    const graphqlQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getCommonEducation')
      .withQuery(print(GET_EDUCATION_LIST))
      .withOperation('getCommonEducation')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: queryMockResult,
        status: 200,
      });
    return pactBuilder.provider.addInteraction(graphqlQuery);
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should be able to interact with api-profile', async () => {
    const store = configureStore()({
      company: {companyInfo: {}, fetchStatus: ''},
    });
    mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <Provider store={store}>
          <Form
            onSubmit={noop}
            render={() => (
              <JobPostingContext.Provider
                value={{
                  jobDescriptionChanged: false,
                  setFormLoading: noop,
                  setShowErrorCard: noop,
                  showErrorCard: false,
                }}
              >
                <CreateKeyInformationFields />
              </JobPostingContext.Provider>
            )}
          />
        </Provider>
      </ApolloProvider>,
    );
    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });
});
