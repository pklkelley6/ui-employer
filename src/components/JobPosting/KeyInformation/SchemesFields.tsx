import {mcf} from '@mcf/constants';
import {compact, find, groupBy, includes} from 'lodash/fp';
import React, {useEffect, useState} from 'react';
import {Field, useField} from 'react-final-form';
import {Scheme} from './Scheme';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {getCompanySchemes, ICompanyScheme} from '~/services/schemes/getCompanySchemes';

// NOTE: manual ordering using ids
const schemesOrder = [
  mcf.SCHEME_ID.CAREER_SUPPORT,
  mcf.SCHEME_ID.SG_UNITED_TRAINEESHIPS,
  mcf.SCHEME_ID.PCP,
  mcf.SCHEME_ID.CAREER_TRIAL,
  mcf.SCHEME_ID.P_MAX,
  mcf.SCHEME_ID.SG_UNITED_MID_CAREER_PATHWAYS,
];

const schemesNotForUserSelection = [mcf.SCHEME_ID.CAREER_TRIAL];

interface ISchemesFieldsFieldsProps {
  uen?: string;
  isLoading: boolean;
}

export const SchemesFields: React.FunctionComponent<ISchemesFieldsFieldsProps> = ({uen}) => {
  const {
    input: {
      value: {isThirdPartyEmployer, thirdPartyEmployer},
    },
  }: {input: {value: IJobPostingFormState['jobDescription']}} = useField('jobDescription', {
    subscription: {value: true},
  });
  // Need to check if thirdPartyEmployer is true since thirdPartyEmployerEntityId is not cleared when thirdPartyEmployer is set to false
  const companyUen = isThirdPartyEmployer && thirdPartyEmployer.entityId ? thirdPartyEmployer.entityId : uen;
  const [companySchemes, setCompanySchemes] = useState<ICompanyScheme[]>();

  useEffect(() => {
    const run = async () => {
      if (companyUen) {
        setCompanySchemes(await getCompanySchemes(companyUen));
      }
    };
    run();
  }, [companyUen]);
  const groupedCompanySchemes = groupBy<ICompanyScheme>('scheme.id')(companySchemes);
  const orderedCompanySchemes = compact(
    schemesOrder.map(
      (orderedSchemeId) =>
        groupedCompanySchemes[orderedSchemeId] && {
          schemeId: orderedSchemeId,
          subSchemes: compact(groupedCompanySchemes[orderedSchemeId].map((scheme) => scheme.subScheme)),
        },
    ),
  );

  return companySchemes ? (
    <Field name="keyInformation.schemes">
      {({input}) => {
        const formSchemes: IJobPostingFormState['keyInformation']['schemes'] = input.value;

        useEffect(() => {
          const initialState: IJobPostingFormState['keyInformation']['schemes'] = orderedCompanySchemes.map(
            (companyScheme) => {
              const companySubSchemeIds = companyScheme.subSchemes.map((subScheme) => subScheme.id);
              const prevScheme = find((formScheme) => formScheme.id === companyScheme.schemeId, formSchemes);
              const prevSubScheme = prevScheme ? prevScheme.subSchemeId : undefined;
              // keep previous scheme and subscheme selection if it is present in the fetched company schemes
              return {
                id: companyScheme.schemeId,
                selected: prevScheme ? prevScheme.selected : false,
                subSchemeId: includes(prevSubScheme)(companySubSchemeIds) ? prevSubScheme : undefined,
              };
            },
          );
          input.onChange(initialState);
        }, []);

        const isSchemeAvailableForUserSelection =
          formSchemes.filter(({id}) => !schemesNotForUserSelection.includes(id)).length > 0;
        return isSchemeAvailableForUserSelection ? (
          <div>
            <div className="f4-5 fw6 black-60 mb4 lh-copy" data-cy="job-govt-optional-section">
              Government support (Optional)
              <p className="ma0 f5 black-50">Select the programme that is applicable for the job post.</p>
            </div>
            {formSchemes.map((formScheme, index) => {
              const companyScheme = find((cs) => cs.schemeId === formScheme.id, orderedCompanySchemes);
              const subSchemes = companyScheme ? companyScheme.subSchemes : [];
              return schemesNotForUserSelection.includes(formScheme.id) ? null : (
                <Scheme key={formScheme.id} id={formScheme.id} index={index} subSchemes={subSchemes} />
              );
            })}
            <div className="black-60 f6"> ⃰ ­­PMETs stands for Professionals, Managers, Executives and Technicians</div>
          </div>
        ) : null;
      }}
    </Field>
  ) : null;
};
