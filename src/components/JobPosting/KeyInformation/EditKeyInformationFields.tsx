import {TextInput} from '@govtechsg/mcf-mcfui';
import {mcf} from '@mcf/constants';
import {addDays, format} from 'date-fns';
import React, {useContext} from 'react';
import {Query} from 'react-apollo';
import {Field, FormSpy} from 'react-final-form';

import {JobPostingContext} from '../JobPostingContext';
import {MinMaxSalaryFields} from './MinMaxSalaryFields';
import SchemesFieldsContainer from './SchemesFieldsContainer';
import {Dropdown} from '~/components/Core/Dropdown';
import {JobPostDurationToolTip} from '~/components/JobPosting/KeyInformation/JobPostDurationToolTip';
import styles from '~/components/JobPosting/KeyInformation/JobPostDurationToolTip.scss';
import {requiredIfQualificationIsAboveALevels} from '~/components/JobPosting/KeyInformation/keyInformationValidations';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {GetCommonEducationQuery, GetCommonEducationQueryVariables} from '~/graphql/__generated__/types';
import {GET_EDUCATION_LIST} from '~/graphql/jobs/jobs.query';
import {
  composeValidators,
  defaultRequired,
  greaterThan,
  lessThanEqual,
  maxLength,
  minLength,
  oneOf,
  positive,
  required,
} from '~/util/fieldValidations';

export function EditKeyInformationFields() {
  const {setFormLoading} = useContext(JobPostingContext);
  return (
    <Query<GetCommonEducationQuery, GetCommonEducationQueryVariables> query={GET_EDUCATION_LIST}>
      {({loading, error, data}) => {
        if (loading || error || !data) {
          setFormLoading(true);
          return <FormLoader className="o-50" cardNumber={2} />;
        }
        const officialSsecEqaCodes = mcf.SSEC_EQA_LIST.map(({code}) => code);
        const ssecFilteredList = data.common.ssecEqaList.filter(
          // only allow users to select ssec codes from the offcial list
          ({actualCode}) => actualCode && officialSsecEqaCodes.includes(actualCode),
        );
        setFormLoading(false);
        return (
          <>
            <div className="flex">
              <div className={`pa2 pl0 w-50 ${styles.readonly}`}>
                <Field name="keyInformation.jobPostDuration" format={(value) => `${value} days`}>
                  {({input}) => {
                    return (
                      <>
                        <TextInput
                          id="job-post-duration-readonly"
                          label="Job Post Duration"
                          placeholder=""
                          input={input}
                          readOnly
                        />
                        <div className="relative top--1 pt2">
                          <FormSpy
                            render={({values}) => {
                              const {newPostingDate} = values.keyInformation;
                              return (
                                <span className="f6 black-60 db">
                                  Auto-closes on:{' '}
                                  {input.value
                                    ? format(
                                        addDays(new Date(newPostingDate), input.value.replace(/[^\d]/g, '')),
                                        'd MMM yyyy',
                                      )
                                    : ''}
                                </span>
                              );
                            }}
                          />
                          <JobPostDurationToolTip />
                        </div>
                      </>
                    );
                  }}
                </Field>
              </div>
              <div className="pa2 w-50">
                <Field
                  name="keyInformation.numberOfVacancies"
                  parse={(value) => (value === '' ? value : Number(value))}
                  validate={composeValidators(defaultRequired, greaterThan(0), lessThanEqual(999))}
                >
                  {({input, meta}) => {
                    return (
                      <TextInput
                        id="number-of-vacancies"
                        label="Number of Vacancies"
                        placeholder="Enter number of vacancies"
                        input={input}
                        meta={meta}
                        type="number"
                      />
                    );
                  }}
                </Field>
              </div>
            </div>
            <div className="pa2 pl0 w-100">
              <Field
                name="keyInformation.jobCategories"
                validate={composeValidators(
                  minLength(1, 'Please type and select'),
                  maxLength(5, 'You can only add a maximum of 5 functions'),
                )}
              >
                {({input, meta}) => {
                  return (
                    <Dropdown
                      id="job-categories"
                      label="Job Function (Max 5 functions)"
                      placeholder="Search..."
                      input={input}
                      meta={meta}
                      data={mcf.JOB_CATEGORIES}
                      labelKey="category"
                      valueKey="id"
                      isMulti
                      isSearchable
                      classNamePrefix="job-categories-search"
                      noOptionsMessage={() => 'No results found. Please try again.'}
                    />
                  );
                }}
              </Field>
            </div>
            <div className="flex">
              <div className="pa2 pl0 w-50">
                <Field name="keyInformation.positionLevel" validate={oneOf(mcf.POSITION_LEVELS.map((x) => x.id))}>
                  {({input, meta}) => {
                    return (
                      <Dropdown
                        id="job-position-level"
                        label="Position Level"
                        placeholder="Select..."
                        meta={meta}
                        input={input}
                        labelKey="position"
                        valueKey="id"
                        data={mcf.POSITION_LEVELS}
                      />
                    );
                  }}
                </Field>
              </div>
              <div className="pa2 w-50">
                <Field
                  name="keyInformation.minimumYearsExperience"
                  parse={(value) => (value === '' ? value : Number(value))}
                  validate={composeValidators(defaultRequired, positive, lessThanEqual(99))}
                >
                  {({input, meta}) => {
                    return (
                      <TextInput
                        id="job-minimum-years-of-experience"
                        label="Minimum Years of Experience"
                        placeholder="Enter number of years"
                        input={input}
                        meta={meta}
                        type="number"
                      />
                    );
                  }}
                </Field>
              </div>
            </div>
            <div className="flex">
              <div className="pa2 pl0 w-50">
                <Field name="keyInformation.employmentType" validate={minLength(1, 'Please select an option')}>
                  {({input, meta}) => {
                    return (
                      <Dropdown
                        id="job-employment-type"
                        label="Employment Type"
                        placeholder="Select..."
                        meta={meta}
                        input={input}
                        data={mcf.EMPLOYMENT_TYPES}
                        labelKey="employmentType"
                        valueKey="id"
                        classNamePrefix="job-employment-type-select"
                        isMulti
                      />
                    );
                  }}
                </Field>
              </div>
            </div>
            <hr className="b--black-10 bb mv4" />
            <div className="flex">
              <div className="pa2 pl0 w-50">
                <Field name="keyInformation.qualification" validate={required('Please select an option')}>
                  {({input, meta}) => {
                    return (
                      <Dropdown
                        id="job-qualification"
                        label="Minimum Qualification Level"
                        placeholder="Select..."
                        meta={meta}
                        input={input}
                        labelKey="description"
                        valueKey="actualCode"
                        data={ssecFilteredList}
                      />
                    );
                  }}
                </Field>
              </div>
              <div className="pa2 w-50" data-cy="field-of-study">
                <Field name="keyInformation.qualification" subscription={{value: true}}>
                  {({input: {value}}) =>
                    mcf.ssecFosRequired(value) ? (
                      <Field name="keyInformation.fieldOfStudy" validate={requiredIfQualificationIsAboveALevels}>
                        {({input, meta}) => {
                          return (
                            <Dropdown
                              id="job-field-of-study"
                              label="Field of Study"
                              placeholder="Search..."
                              input={input}
                              meta={meta}
                              data={data.common.ssecFosList}
                              labelKey="description"
                              valueKey="actualCode"
                              isSearchable
                              noOptionsMessage={() => 'No results found. Please try again.'}
                            />
                          );
                        }}
                      </Field>
                    ) : null
                  }
                </Field>
              </div>
            </div>

            <hr className="b--black-10 bb mv4" />
            <MinMaxSalaryFields />
            <hr className="b--black-10 bb mv4" />
            <SchemesFieldsContainer />
          </>
        );
      }}
    </Query>
  );
}
