import {CheckBox} from '@govtechsg/mcf-mcfui';
import {mcf} from '@mcf/constants';
import React from 'react';
import {Field} from 'react-final-form';
import {sanitize} from 'dompurify';
import {Dropdown} from '~/components/Core/Dropdown';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {ISubScheme} from '~/services/schemes/getCompanySchemes';
import {defaultRequired} from '~/util/fieldValidations';

const descriptionMap = [
  {
    description:
      'Receive funding support when you hire Singapore Citizens PMETs<sup>*</sup> who are unemployed or retrenched, for jobs paying at least $3,600 (SME) or $4,000 (non-SME).',
    id: mcf.SCHEME_ID.CAREER_SUPPORT,
  },
  {
    description:
      'Receive funding support when you hire and re-skill mid-career Singapore Citizen or Permanent Resident PMETs<sup>*</sup>.',
    id: mcf.SCHEME_ID.PCP,
  },
  {
    description:
      'Receive funding support when you hire Singapore Citizen or Permanent Resident PMETs<sup>*</sup> to help them transition to your SME.',
    id: mcf.SCHEME_ID.P_MAX,
  },
  {
    description:
      'Receive funding support when you hire Singapore Citizens for short-term trials for jobs paying $1,500 or more.',
    id: mcf.SCHEME_ID.CAREER_TRIAL,
  },
  {
    description:
      'Receive funding support for training allowance when you take on recently graduated Singapore Citizens or Permanent Residents that who have recently graduated as trainees for your company.',
    id: mcf.SCHEME_ID.SG_UNITED_TRAINEESHIPS,
  },
  {
    description:
      'Receive funding support for training allowance when you take mid-career Singapore Citizens or Permanent Residents on attachment for your organisation.',
    id: mcf.SCHEME_ID.SG_UNITED_MID_CAREER_PATHWAYS,
  },
];

interface ISchemesProps {
  id: number;
  disabled?: boolean;
  subSchemes: ISubScheme[];
  index: number;
}

export const Scheme: React.FunctionComponent<ISchemesProps> = ({id, subSchemes, index, disabled}) => {
  const description = descriptionMap.find((mapValue) => mapValue.id === id)!.description;
  const {scheme: name, link} = mcf.SCHEMES.find((scheme) => scheme.id === id)!;
  return (
    <div className="mb4" data-cy={`scheme-${id}`}>
      <Field name={`keyInformation.schemes[${index}]`}>
        {({input}) => {
          const scheme: IJobPostingFormState['keyInformation']['schemes'][0] = input.value;
          // NOTE: uncheck scheme when disabled
          if (disabled && scheme.selected) {
            input.onChange({...scheme, selected: false});
          }

          return (
            <div>
              <CheckBox
                label={
                  <div className="pl2">
                    <div className="fw6 mb2" data-cy={`scheme-name-${id}`}>
                      {name}
                    </div>
                    <div
                      className="lh-copy fw4"
                      dangerouslySetInnerHTML={{__html: sanitize(description)}}
                      data-cy={`scheme-description-${id}`}
                    />
                  </div>
                }
                disabled={disabled}
                input={{
                  onChange: (checked) => input.onChange({...scheme, selected: checked}),
                  value: scheme.selected,
                }}
                id={`schemes-checkbox-${id}`}
              />
              {subSchemes.length && scheme.selected ? (
                <div data-cy="subschemes-dropdown" className="ml4 pa2 pl0">
                  <Field name={`keyInformation.schemes[${index}].subSchemeId`} validate={defaultRequired}>
                    {({input: subSchemeInput, meta}) => (
                      <Dropdown
                        id="subschemes-dropdown"
                        label="Programme Name"
                        placeholder="Select..."
                        meta={meta}
                        input={subSchemeInput}
                        data={subSchemes}
                        labelKey="programme"
                        valueKey="id"
                      />
                    )}
                  </Field>
                </div>
              ) : null}
              <a className="relative blue pl4 pt2 dib" data-cy={`scheme-link-${id}`} target="_blank" href={link}>
                Learn more
              </a>
            </div>
          );
        }}
      </Field>
    </div>
  );
};
