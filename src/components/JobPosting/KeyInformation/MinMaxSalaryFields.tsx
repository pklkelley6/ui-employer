import {get} from 'lodash/fp';
import React from 'react';
import {Field} from 'react-final-form';
import {getNumberFromLocaleString, LocaleNumberInput} from '../LocaleNumberInput/LocaleNumberInput';
import {minNotMoreThanMax} from '~/components/JobPosting/KeyInformation/keyInformationValidations';
import {composeValidators, defaultRequired, greaterThan, lessThan} from '~/util/fieldValidations';

const dashSize = 30; // size of dash container
const pa2 = 0.5; // padding used around all Field in this form, value comes from tachyons
const dashStyle = {flexBasis: `${dashSize}px`};
const minMaxStyle = {flexBasis: `calc(50% - ${dashSize / 2}px - ${pa2 / 2}rem)`};
const errorStyle = {
  color: 'var(--red)',
  display: 'block',
  fontSize: 'var(--font-size-5-5)',
  fontWeight: 400,
};

export const MinMaxSalaryFields: React.FunctionComponent = () => {
  return (
    <>
      <div className="pt2">
        <span data-cy="job-month-salary-label" className="f5 fw6 black-60 pl1">
          Monthly Salary Range (SGD)
        </span>
      </div>
      <div className="flex">
        <div className="w-50">
          <Field name="keyInformation" validate={minNotMoreThanMax}>
            {(renderProps) => (
              <>
                <div className="flex">
                  <span className="f4 black-60 db pr1 pt4">$</span>
                  <div style={minMaxStyle}>
                    <Field
                      name="keyInformation.minimumSalary"
                      parse={(value) => getNumberFromLocaleString(value)}
                      validate={composeValidators(
                        defaultRequired,
                        greaterThan(0),
                        lessThan(100000000, 'This number should not be more than 8 digits'),
                      )}
                    >
                      {({input, meta}) => (
                        <LocaleNumberInput id="job-minimum-salary" placeholder="Min" input={input} meta={meta} />
                      )}
                    </Field>
                  </div>
                  <div className="tc pt4" style={dashStyle}>
                    -
                  </div>
                  <div style={minMaxStyle}>
                    <Field
                      name="keyInformation.maximumSalary"
                      parse={(value) => getNumberFromLocaleString(value)}
                      validate={composeValidators(
                        defaultRequired,
                        greaterThan(0),
                        lessThan(100000000, 'This number should not be more than 8 digits'),
                      )}
                    >
                      {({input, meta}) => (
                        <LocaleNumberInput id="job-maximum-salary" placeholder="Max" input={input} meta={meta} />
                      )}
                    </Field>
                  </div>
                </div>
                <div id="job-salary-error" style={errorStyle}>
                  {get('meta.error.minMaxRange', renderProps)}
                </div>
              </>
            )}
          </Field>
        </div>
      </div>
    </>
  );
};
