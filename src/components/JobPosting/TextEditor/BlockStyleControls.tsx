import {DraftBlockType, EditorState} from 'draft-js';
import React from 'react';
import {TextEditorButton} from './TextEditorButton';

const BLOCK_TYPES: Array<{label: string; style: DraftBlockType}> = [
  {label: 'H1', style: 'header-one'},
  {label: 'H2', style: 'header-two'},
  {label: 'H3', style: 'header-three'},
  {label: '', style: 'unordered-list-item'},
  {label: '', style: 'ordered-list-item'},
];

interface IBlockStyleControlsProps {
  editorState: EditorState;
  onClick: (type: DraftBlockType) => void;
}
export const BlockStyleControls: React.FunctionComponent<IBlockStyleControlsProps> = ({editorState, onClick}) => {
  const selection = editorState.getSelection();
  const blockType = editorState.getCurrentContent().getBlockForKey(selection.getStartKey()).getType();

  return (
    <>
      {BLOCK_TYPES.map((type) => (
        <TextEditorButton
          key={type.style}
          active={type.style === blockType}
          label={type.label}
          onClick={() => onClick(type.style)}
          className={type.style}
        />
      ))}
    </>
  );
};
