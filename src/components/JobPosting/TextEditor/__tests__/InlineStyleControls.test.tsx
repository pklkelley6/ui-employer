import {EditorState} from 'draft-js';
import {stateFromHTML} from 'draft-js-import-html';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {InlineStyleControls} from '~/components/JobPosting/TextEditor/InlineStyleControls';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('InlineStyleControls', () => {
  it('should render all buttons as inactive', () => {
    const editorState = EditorState.createEmpty();
    const controls = mount(<InlineStyleControls editorState={editorState} onClick={() => void 0} />);
    expect(toJson(controls, cleanSnapshot())).toMatchSnapshot();
  });
  it('should render B as active', () => {
    const editorState = EditorState.createWithContent(stateFromHTML('<strong>some text</strong>'));
    const controls = mount(<InlineStyleControls editorState={editorState} onClick={() => void 0} />);
    expect(toJson(controls, cleanSnapshot())).toMatchSnapshot();
  });
  it('should render all buttons as active', () => {
    const editorState = EditorState.createWithContent(stateFromHTML('<u><em><strong>some text</strong></em></u>'));
    const controls = mount(<InlineStyleControls editorState={editorState} onClick={() => void 0} />);
    expect(toJson(controls, cleanSnapshot())).toMatchSnapshot();
  });
});
