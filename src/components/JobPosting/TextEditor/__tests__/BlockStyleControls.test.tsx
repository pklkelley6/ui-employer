import {EditorState} from 'draft-js';
import {stateFromHTML} from 'draft-js-import-html';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {BlockStyleControls} from '../BlockStyleControls';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('BlockStyleControls', () => {
  it('should render all buttons as inactive', () => {
    const editorState = EditorState.createEmpty();
    const controls = mount(<BlockStyleControls editorState={editorState} onClick={() => void 0} />);
    expect(toJson(controls, cleanSnapshot())).toMatchSnapshot();
  });
  it('should render h1 as active', () => {
    const editorState = EditorState.createWithContent(stateFromHTML('<h1>header text</h1>'));
    const controls = mount(<BlockStyleControls editorState={editorState} onClick={() => void 0} />);
    expect(toJson(controls, cleanSnapshot())).toMatchSnapshot();
  });
});
