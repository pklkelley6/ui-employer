import {mount} from 'enzyme';
import React from 'react';
import {TextEditorButton} from '~/components/JobPosting/TextEditor/TextEditorButton';

describe('TextEditorButton', () => {
  it('should render with label as B and without active className', () => {
    expect(mount(<TextEditorButton label="B" onClick={() => void 0} />)).toMatchSnapshot();
  });
  it('should render with label as I and with active className', () => {
    expect(mount(<TextEditorButton label="I" onClick={() => void 0} active />)).toMatchSnapshot();
  });
  it('should render with label as U and with extra className', () => {
    expect(mount(<TextEditorButton label="U" onClick={() => void 0} className="custom-classname" />)).toMatchSnapshot();
  });
  it('should call onClick when clicking on the button', () => {
    const onClick = jest.fn();
    const wrapper = mount(<TextEditorButton label="U" onClick={onClick} className="custom-classname" />);
    wrapper.find('button').simulate('mouseDown');

    expect(onClick).toHaveBeenCalled();
  });
});
