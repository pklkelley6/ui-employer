import {Editor} from 'draft-js';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {BlockStyleControls} from '../BlockStyleControls';
import {InlineStyleControls} from '../InlineStyleControls';
import {TextEditor} from '~/components/JobPosting/TextEditor/TextEditor';

describe('TextEditor', () => {
  it('should render all control buttons, text editor', () => {
    const input = {value: '<p>some value</P>', onBlur: noop};
    const textEditor = mount(<Form onSubmit={noop} render={() => <TextEditor input={input} />} />);

    expect(textEditor.find(InlineStyleControls)).toHaveLength(1);
    expect(textEditor.find(BlockStyleControls)).toHaveLength(1);
    expect(textEditor.find(Editor)).toHaveLength(1);
  });

  it('should render character count when maxLength prop is specified', () => {
    const input = {value: '<p>some value</P>', onBlur: noop};
    const textEditor = mount(<Form onSubmit={noop} render={() => <TextEditor input={input} maxLength={50} />} />);

    const characterCountDisplayComponent = textEditor.find('[data-cy="text-editor-character-count"]');
    expect(characterCountDisplayComponent).toHaveLength(1);
    expect(characterCountDisplayComponent).toMatchSnapshot();
  });

  it('should render character count in red when contentLength > maxLength', () => {
    const input = {value: '<p>some value</P>', onBlur: noop};
    const textEditor = mount(<Form onSubmit={noop} render={() => <TextEditor input={input} maxLength={5} />} />);

    const characterCountDisplayComponent = textEditor.find('[data-cy="text-editor-character-count"]');
    expect(characterCountDisplayComponent).toHaveLength(1);
    expect(characterCountDisplayComponent).toMatchSnapshot();
  });
});
