import React from 'react';
import styles from './TextEditorButton.scss';

interface IStyleButtonProps {
  active?: boolean;
  label: string;
  className?: string;
  onClick: () => void;
}
export const TextEditorButton: React.FunctionComponent<IStyleButtonProps> = ({
  label,
  onClick,
  active,
  className = '',
}) => {
  return (
    <button
      onClick={(event: React.MouseEvent) => event.preventDefault()}
      onMouseDown={(event: React.MouseEvent) => {
        event.preventDefault();
        onClick();
      }}
      className={`mh2 ${className} ${styles.button} ${active ? styles.active : ''}`}
    >
      {label}
    </button>
  );
};
