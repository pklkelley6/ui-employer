import {Editor, EditorState, RichUtils} from 'draft-js';
import {stateToHTML} from 'draft-js-export-html';
import {stateFromHTML} from 'draft-js-import-html';
import {noop} from 'lodash';
import React, {useState} from 'react';
import styles from './TextEditor.scss';
import {BlockStyleControls} from '~/components/JobPosting/TextEditor/BlockStyleControls';
import {InlineStyleControls} from '~/components/JobPosting/TextEditor/InlineStyleControls';

interface IMyEditorProps {
  label?: string;
  subtitle?: string;
  input: {
    value?: string;
    onChange?: (value: string) => void;
    onBlur: () => void;
  };
  showToolbar?: boolean;
  maxLength?: number;
  meta?: {
    error?: string;
    touched?: boolean;
  };
}

export const TextEditor: React.FunctionComponent<IMyEditorProps> = ({
  label,
  subtitle,
  input,
  maxLength,
  meta,
  showToolbar = true,
}) => {
  const onChange = input.onChange || noop;
  const [editorState, setEditorState] = useState(EditorState.createWithContent(stateFromHTML(input.value || '')));
  const currentContentLength = editorState.getCurrentContent().getPlainText().length;

  const controls = (
    <div className={`${styles.toolbar}`} data-cy="text-editor-toolbar">
      <InlineStyleControls
        editorState={editorState}
        onClick={(type) => {
          setEditorState(RichUtils.toggleInlineStyle(editorState, type));
        }}
      />
      <BlockStyleControls
        editorState={editorState}
        onClick={(type) => {
          setEditorState(RichUtils.toggleBlockType(editorState, type));
        }}
      />
    </div>
  );
  return (
    <>
      {label && <label className="mb2 f5 fw6 black-60 db">{label}</label>}
      {subtitle && <span className="mv2 f6 fw4 black-50 db">{subtitle}</span>}
      <div className={`${styles.editorWrap} ${meta?.error && meta.touched && styles.editorError}`}>
        {showToolbar ? controls : null}

        <Editor
          data-cy="text-editor"
          editorState={editorState}
          onChange={(state) => {
            setEditorState(state);
            onChange(stateToHTML(state.getCurrentContent()));
          }}
          onBlur={() => {
            input.onBlur();
          }}
        />
      </div>
      <div className="mv2">
        {maxLength && (
          <span data-cy="text-editor-character-count" className={currentContentLength > maxLength ? 'red' : 'black-80'}>
            {currentContentLength.toLocaleString()} / {maxLength.toLocaleString()} characters
          </span>
        )}
        {meta?.error && meta.touched && (
          <span>
            {' '}
            -{' '}
            <span data-cy="text-editor-error" className="red">
              {meta.error}
            </span>
          </span>
        )}
      </div>
    </>
  );
};
