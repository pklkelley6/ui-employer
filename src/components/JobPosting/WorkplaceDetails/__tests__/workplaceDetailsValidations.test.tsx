import {validPostalCode} from '../workplaceDetailsValidations';
import * as oneMap from '~/services/oneMap/oneMap';

const getAddressByPostalCodeMock = jest.spyOn(oneMap, 'getAddressByPostalCode');

describe('workplaceDetailsValidations', () => {
  describe('validPostalCode', () => {
    it('should return undefined if there are addresses found for the given postalCode in onemap', async () => {
      const getAddressByPostalCodeResponse = {
        found: 1,
      };
      getAddressByPostalCodeMock.mockImplementation(() => Promise.resolve(getAddressByPostalCodeResponse));
      const result = await validPostalCode('123456');
      expect(result).toEqual(undefined);
    });

    it('should return error message if there are no addresses found for the given postalCode in onemap', async () => {
      const getAddressByPostalCodeResponse = {
        found: 0,
      };
      getAddressByPostalCodeMock.mockImplementation(() => Promise.resolve(getAddressByPostalCodeResponse));
      const result = await validPostalCode('993456');
      expect(result).toEqual('Please check your postal code');
    });

    it('should check if first 2 digits are valid if onemap returned an error', async () => {
      getAddressByPostalCodeMock.mockImplementation(() => Promise.reject());
      // '12' is valid sector
      const result = await validPostalCode('123456');
      expect(result).toEqual(undefined);
    });

    it('should return error message if onemap returns an error and first 2 digits are invalid', async () => {
      getAddressByPostalCodeMock.mockImplementation(() => Promise.reject());
      // 99 is invalid sector
      const result = await validPostalCode('993456');
      expect(result).toEqual('Please check your postal code');
    });
  });
});
