import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import configureStore, {MockStore} from 'redux-mock-store';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {formInitialState} from '~/components/JobPosting/formInitialState';
import LocalAddressFieldsContainer from '~/components/JobPosting/WorkplaceDetails/LocalAddressFieldsContainer';
import {COMPANY_REQUEST_SUCCEEDED} from '~/flux/company/company.constants';
import {CompanyAddressType, ICompanyAddress} from '~/services/employer/company';

describe('JobPosting/LocalAddressFields', () => {
  const mockAddress: ICompanyAddress = {
    block: 'the block',
    building: 'the building',
    postalCode: '111222',
    purpose: CompanyAddressType.OPERATING,
    street: 'the street',
  };

  const mockStore = configureStore()({
    company: {companyInfo: {addresses: [mockAddress], uen: 'uen123'}, fetchStatus: COMPANY_REQUEST_SUCCEEDED},
  });
  it('should autopopulate address fields when locationType is SameAsCompany', async () => {
    const wrapper = mountLocalAddressFields();

    const localAddressFields = wrapper.find('[data-cy="local-address-fields"]');
    expect(localAddressFields.find('input#postal-code').props().value).toEqual(mockAddress.postalCode);
    expect(localAddressFields.find('input#street-name').props().value).toEqual(mockAddress.street);
    expect(localAddressFields.find('input#block-house-num').props().value).toEqual(mockAddress.block);
    expect(localAddressFields.find('input#building-name').props().value).toEqual(mockAddress.building);
  });

  it('should populate address from third party employer when isThirdPartyEmployer = true', async () => {
    const mockThirdPartyAddress: ICompanyAddress = {
      block: 'third party',
      building: 'the other building',
      postalCode: '666888',
      purpose: CompanyAddressType.OPERATING,
      street: 'the next street',
    };

    const stateWithThirdParty = {
      ...formInitialState,
      jobDescription: {
        isThirdPartyEmployer: true,
        thirdPartyEmployer: {
          address: mockThirdPartyAddress,
        },
      },
    };
    const wrapper = mountLocalAddressFields({state: stateWithThirdParty});

    const localAddressFields = wrapper.find('[data-cy="local-address-fields"]');
    expect(localAddressFields.find('input#postal-code').props().value).toEqual(mockThirdPartyAddress.postalCode);
    expect(localAddressFields.find('input#street-name').props().value).toEqual(mockThirdPartyAddress.street);
    expect(localAddressFields.find('input#block-house-num').props().value).toEqual(mockThirdPartyAddress.block);
    expect(localAddressFields.find('input#building-name').props().value).toEqual(mockThirdPartyAddress.building);
  });

  it('should disable checkbox-sameLocation if there is no address', async () => {
    const storeWithoutCompanyAddress = configureStore()({
      company: {companyInfo: {addresses: [], uen: 'uen123'}, fetchStatus: COMPANY_REQUEST_SUCCEEDED},
    });
    const wrapper = mountLocalAddressFields({store: storeWithoutCompanyAddress});

    expect(wrapper.find('input#checkbox-sameLocation').props().disabled).toEqual(true);
  });

  it('should clear all address fields when locationType is Multiple', async () => {
    const wrapper = mountLocalAddressFields();
    wrapper.find('input#checkbox-multipleLocation').simulate('change', {target: {checked: true}});

    const localAddressFields = wrapper.find('[data-cy="local-address-fields"]');
    expect(localAddressFields.find('input#postal-code').props().value).toEqual('');
    expect(localAddressFields.find('input#street-name').props().value).toEqual('');
    expect(localAddressFields.find('input#block-house-num').props().value).toEqual('');
    expect(localAddressFields.find('input#building-name').props().value).toEqual('');
  });

  const mountLocalAddressFields = ({
    store = mockStore,
    state = formInitialState,
  }: {
    store?: MockStore;
    state?: IJobPostingFormState;
  } = {}) =>
    mount(
      <Provider store={store}>
        <JobPostingContext.Provider
          value={{
            jobDescriptionChanged: false,
            setFormLoading: noop,
            setShowErrorCard: noop,
            showErrorCard: false,
          }}
        >
          <Form initialValues={state} onSubmit={noop} render={() => <LocalAddressFieldsContainer />} />
        </JobPostingContext.Provider>
      </Provider>,
    );
});
