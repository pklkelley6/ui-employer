import {CheckBox, TextInput} from '@govtechsg/mcf-mcfui';
import React, {useContext, useEffect} from 'react';
import {Field, useField} from 'react-final-form';
import styles from './LocalAddressFields.scss';
import {LocationType} from './workplaceDetails.types';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {validPostalCodeMemo} from '~/components/JobPosting/WorkplaceDetails/workplaceDetailsValidations';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {ICompanyAddress} from '~/services/employer/company';
import {composeValidators, defaultRequired, exactLength, validateIf, maxLength} from '~/util/fieldValidations';

const MAX_LENGTH_BLOCK = 10;
const MAX_LENGTH_STREET = 32;
const MAX_LENGTH_BUILDING = 66;
const MAX_LENGTH_POSTAL_CODE = 6;

export interface ILocalAddressFieldsProps {
  postingCompanyAddress?: ICompanyAddress;
  isLoading: boolean;
}

export const LocalAddressFields: React.FunctionComponent<ILocalAddressFieldsProps> = ({
  postingCompanyAddress,
  isLoading,
}) => {
  const {setFormLoading, setShowErrorCard} = useContext(JobPostingContext);
  const workplaceDetails = useField<IJobPostingFormState['workplaceDetails']>('workplaceDetails', {
    subscription: {value: true},
  });
  const jobDescription = useField<IJobPostingFormState['jobDescription']>('jobDescription', {
    subscription: {value: true},
  });
  useEffect(() => {
    switch (workplaceDetails.input.value.locationType) {
      case LocationType.SameAsCompany:
        if (companyAddress) {
          workplaceDetails.input.onChange(sameAsCompanyAddressState);
        } else {
          workplaceDetails.input.onChange({
            ...workplaceDetails.input.value,
            locationType: LocationType.None,
          });
        }
        break;
      case LocationType.Multiple:
        workplaceDetails.input.onChange(multipleLocationState);
        break;
    }
  }, [workplaceDetails.input.value.locationType]);

  const readOnly = workplaceDetails.input.value.locationType !== LocationType.None;

  if (isLoading) {
    setFormLoading(true);
    return <FormLoader className="o-50" cardNumber={2} />;
  }
  setFormLoading(false);

  const companyAddress = jobDescription.input.value.isThirdPartyEmployer
    ? jobDescription.input.value.thirdPartyEmployer.address
    : postingCompanyAddress;
  const sameAsCompanyAddressState: IJobPostingFormState['workplaceDetails'] = {
    ...workplaceDetails.input.value,
    block: companyAddress?.block ?? '',
    building: companyAddress?.building ?? '',
    postalCode: companyAddress?.postalCode ?? '',
    street: companyAddress?.street ?? '',
  };

  const multipleLocationState: IJobPostingFormState['workplaceDetails'] = {
    ...workplaceDetails.input.value,
    block: '',
    building: '',
    postalCode: '',
    street: '',
  };

  return (
    <div data-cy="local-address-fields" className="flex-column pt3">
      <div className="mb3">
        <Field name="workplaceDetails.locationType">
          {({input}) => {
            return (
              <CheckBox
                label={<span>The workplace address is the same as the company address</span>}
                input={{
                  onChange: () => {
                    input.onChange(
                      input.value === LocationType.SameAsCompany ? LocationType.None : LocationType.SameAsCompany,
                    );
                    setShowErrorCard(false);
                  },
                  value: input.value === LocationType.SameAsCompany,
                }}
                id="checkbox-sameLocation"
                disabled={companyAddress ? false : true}
              />
            );
          }}
        </Field>
      </div>
      <div>
        <Field name="workplaceDetails.locationType">
          {({input}) => {
            return (
              <CheckBox
                label={<span>This position involves multiple workplace locations in Singapore</span>}
                input={{
                  onChange: () => {
                    input.onChange(input.value === LocationType.Multiple ? LocationType.None : LocationType.Multiple);
                    setShowErrorCard(false);
                  },
                  value: input.value === LocationType.Multiple,
                }}
                id="checkbox-multipleLocation"
              />
            );
          }}
        </Field>
      </div>
      <hr className="b--black-10 bb mv4" />
      <div className={`flex-column w-50 pa2 ${styles.readonly}`}>
        <Field
          name="workplaceDetails.postalCode"
          validate={validateIf(
            'workplaceDetails.locationType',
            (value) => value !== LocationType.Multiple,
            composeValidators(
              defaultRequired,
              exactLength(
                MAX_LENGTH_POSTAL_CODE,
                `This should be a ${MAX_LENGTH_POSTAL_CODE.toLocaleString()}-digit number`,
              ),
              validPostalCodeMemo,
            ),
          )}
        >
          {({input, meta}) => {
            return (
              <TextInput
                id="postal-code"
                label="Postal Code"
                placeholder={readOnly ? '' : 'Enter postal code'}
                input={input}
                maxLength={MAX_LENGTH_POSTAL_CODE}
                pattern="[0-9]*"
                meta={meta}
                readOnly={readOnly}
              />
            );
          }}
        </Field>
      </div>
      <div className={`flex ${styles.readonly}`}>
        <div className="w-50 pa2">
          <Field
            name="workplaceDetails.block"
            validate={validateIf(
              'workplaceDetails.locationType',
              (value) => value !== LocationType.Multiple,
              composeValidators(
                defaultRequired,
                maxLength(MAX_LENGTH_BLOCK, `Please keep within ${MAX_LENGTH_BLOCK.toLocaleString()} characters`),
              ),
            )}
          >
            {({input, meta}) => {
              return (
                <TextInput
                  id="block-house-num"
                  label="Block/House No."
                  placeholder={readOnly ? '' : 'Enter block/house number'}
                  input={input}
                  meta={meta}
                  readOnly={readOnly}
                />
              );
            }}
          </Field>
        </div>
        <div className="w-50 pa2">
          <Field
            name="workplaceDetails.street"
            validate={validateIf(
              'workplaceDetails.locationType',
              (value) => value !== LocationType.Multiple,
              composeValidators(
                defaultRequired,
                maxLength(MAX_LENGTH_STREET, `Please keep within ${MAX_LENGTH_STREET.toLocaleString()} characters`),
              ),
            )}
          >
            {({input, meta}) => {
              return (
                <TextInput
                  id="street-name"
                  label="Street Name"
                  placeholder={readOnly ? '' : 'Enter street name'}
                  input={input}
                  meta={meta}
                  readOnly={readOnly}
                />
              );
            }}
          </Field>
        </div>
      </div>
      <div className={`flex-column w-50 pa2 ${styles.readonly}`}>
        <Field
          name="workplaceDetails.building"
          validate={validateIf(
            'workplaceDetails.locationType',
            (value) => value !== LocationType.Multiple,
            maxLength(MAX_LENGTH_BUILDING, `Please keep within ${MAX_LENGTH_BUILDING.toLocaleString()} characters`),
          )}
        >
          {({input, meta}) => {
            return (
              <TextInput
                id="building-name"
                label="Building Name (optional)"
                placeholder={readOnly ? '' : 'Enter building name'}
                input={input}
                meta={meta}
                readOnly={readOnly}
              />
            );
          }}
        </Field>
      </div>
    </div>
  );
};
