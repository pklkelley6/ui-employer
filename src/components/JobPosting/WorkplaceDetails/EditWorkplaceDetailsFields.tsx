import React, {useContext} from 'react';
import {Field} from 'react-final-form';
import LocalAddressFieldsContainer from './LocalAddressFieldsContainer';
import {OverseasAddressFields} from './OverseasAddressFields';
import {Location, LocationType} from './workplaceDetails.types';
import {Condition} from '~/components/Core/Condition';
import {WhenFieldChanges} from '~/components/Core/WhenFieldChanges';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';

export const EditWorkplaceDetailsFields: React.FunctionComponent = () => {
  const {setShowErrorCard} = useContext(JobPostingContext);
  return (
    <section className="pa1">
      <div className="f5 fw6 black-60" data-cy="workplace-label">
        Workplace Address
      </div>
      <div className="flex-column">
        <div className="flex pb2">
          <div className="pv2 flex mr4">
            <WhenFieldChanges
              field="workplaceDetails.location"
              becomes={Location.Local}
              set="workplaceDetails"
              to={{location: Location.Local, locationType: LocationType.None}}
            />
            <Field
              className="pointer"
              id="local"
              name="workplaceDetails.location"
              component="input"
              type="radio"
              value={Location.Local}
              onClick={() => setShowErrorCard(false)} // Will remove the error card since fields should be reset when changing location
            />
            <label className="pl2 pointer black-60" htmlFor="local">
              Local
            </label>
          </div>
          <div className="pv2 flex">
            <WhenFieldChanges
              field="workplaceDetails.location"
              becomes={Location.Overseas}
              set="workplaceDetails"
              to={{location: Location.Overseas, locationType: LocationType.None}}
            />
            <Field
              className="pointer"
              id="overseas"
              name="workplaceDetails.location"
              component="input"
              type="radio"
              value={Location.Overseas}
              onClick={() => setShowErrorCard(false)}
            />
            <label className="pl2 pointer black-60" htmlFor="overseas">
              Overseas
            </label>
          </div>
        </div>
        <Condition when="workplaceDetails.location" is={Location.Local}>
          <LocalAddressFieldsContainer />
        </Condition>
        <Condition when="workplaceDetails.location" is={Location.Overseas}>
          <OverseasAddressFields />
        </Condition>
      </div>
    </section>
  );
};
