import {connect} from 'react-redux';
import {LocalAddressFields} from './LocalAddressFields';
import {COMPANY_FETCH_REQUESTED} from '~/flux/company/company.constants';
import {IAppState} from '~/flux/index';
import {getCompanyOperatingAddress} from '~/flux/company';

const mapStateToProps = ({company}: IAppState) => ({
  postingCompanyAddress: getCompanyOperatingAddress(company),
  isLoading: !company || !company.fetchStatus || company.fetchStatus === COMPANY_FETCH_REQUESTED,
});

export default connect(mapStateToProps)(LocalAddressFields);
