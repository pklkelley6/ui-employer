export const formInitialState = {
  jobDescription: {
    isThirdPartyEmployer: false,
    thirdPartyEmployer: {},
  },
  jobPostingSkills: {
    addedSkills: [],
    recommendedSkills: [],
  },
  keyInformation: {
    newPostingDate: '2021-10-01',
    jobCategories: [],
    schemes: [],
  },
  workplaceDetails: {
    location: 'local',
    locationType: 1,
  },
  screeningQuestions: {
    includeScreeningQuestions: false,
    questions: [''],
  },
};
