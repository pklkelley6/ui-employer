import {CheckBox, TextInput} from '@govtechsg/mcf-mcfui';
import React, {useContext} from 'react';
import {Query} from 'react-apollo';
import {Field, useField} from 'react-final-form';
import {CompaniesLookup} from './CompaniesLookup';
import {JobPostingContext} from './JobPostingContext';
import styles from './ThirdPartyEmployerFields.scss';
import {IJobPostingFormState} from './JobPostingForm';
import {Condition} from '~/components/Core/Condition';
import {GetCommonSsicQuery, GetCommonSsicQueryVariables} from '~/graphql/__generated__/types';
import {GET_SSIC_LIST} from '~/graphql/jobs/jobs.query';
import {GetCompanyInfoQuery} from '~/services/employer/getCompanyInfo';
import {composeValidators, defaultRequired} from '~/util/fieldValidations';
import {CompanyAddressType} from '~/services/employer/company';

type ThirdPartyEmployer = IJobPostingFormState['jobDescription']['thirdPartyEmployer'];

export const ThirdPartyEmployerFields: React.FunctionComponent = () => {
  const {setFormLoading} = useContext(JobPostingContext);
  const thirdPartyEmployerEntityId = useField<ThirdPartyEmployer['entityId']>(
    'jobDescription.thirdPartyEmployer.entityId',
    {
      subscription: {value: true},
    },
  );
  const thirdPartyEmployerName = useField<ThirdPartyEmployer['name']>('jobDescription.thirdPartyEmployer.name');
  const thirdPartyEmployerIndustry = useField<ThirdPartyEmployer['industry']>(
    'jobDescription.thirdPartyEmployer.industry',
  );
  const thirdPartyEmployerAddress = useField<ThirdPartyEmployer['address']>(
    'jobDescription.thirdPartyEmployer.address',
  );

  return (
    <>
      <div className="flex pa2 w-100">
        <Field<
          IJobPostingFormState['jobDescription']['isThirdPartyEmployer']
        > name="jobDescription.isThirdPartyEmployer">
          {({input}) => {
            return (
              <CheckBox
                id="third-party-employer-checkbox"
                label="You're posting on behalf of another company"
                input={input}
              />
            );
          }}
        </Field>
      </div>
      <Condition when="jobDescription.isThirdPartyEmployer" is={true}>
        <GetCompanyInfoQuery uen={thirdPartyEmployerEntityId?.input?.value ?? ''}>
          {({data, errorMessage, loading}) => {
            if (loading) {
              setFormLoading(true);
            } else {
              setFormLoading(false);
              if (data) {
                thirdPartyEmployerName.input.onChange(data.name);

                thirdPartyEmployerIndustry.input.onChange(data.ssicCode);
                thirdPartyEmployerAddress.input.onChange(
                  data.addresses.find((address) => address.purpose === CompanyAddressType.OPERATING),
                );
              }
              if (errorMessage) {
                thirdPartyEmployerName.input.onChange(undefined);

                thirdPartyEmployerIndustry.input.onChange(undefined);
                thirdPartyEmployerAddress.input.onChange(undefined);
              }
            }
            return (
              <div className={`flex-column mb2 ${styles.expandAnimate}`}>
                <div className="pa2 w-100 pt4" data-cy="company-uen">
                  <Field<ThirdPartyEmployer['entityId']>
                    name="jobDescription.thirdPartyEmployer.entityId"
                    validate={composeValidators(defaultRequired, () => errorMessage && 'Please check the UEN')}
                    /**
                     * In order to rerender my Field with a new validation function when the `errorMessage` props is ready.
                     * We will need to update the Field key prop and force a rerender. For more info, refer to below link
                     * https://github.com/final-form/react-final-form#validate-value-any-allvalues-object-meta-fieldstate--any
                     */
                    key={`third-party-employer-entity-id${loading ? '-loading' : ''}`}
                  >
                    {({input, meta}) => {
                      return (
                        <CompaniesLookup
                          id="entity-id"
                          input={input}
                          meta={meta}
                          placeholder="Search..."
                          subtitle="Type to search for company name or enter full UEN"
                          label="Company Name / UEN"
                          inputLabel={data && `${data.name} (${data.uen})`}
                        />
                      );
                    }}
                  </Field>
                </div>
                <div className="flex">
                  <div className={`pa2 w-50 ${styles.readonly}`}>
                    <Field<ThirdPartyEmployer['industry']> name="jobDescription.thirdPartyEmployer.industry">
                      {({input, meta}) => {
                        return (
                          <Query<GetCommonSsicQuery, GetCommonSsicQueryVariables>
                            query={GET_SSIC_LIST}
                            variables={{code: input.value}}
                            skip={!input.value}
                          >
                            {({data: ssicList, loading: ssicListLoading}) => {
                              const ssic =
                                !ssicListLoading && ssicList
                                  ? ssicList.common.ssicList.find(({code}) => code === input.value)
                                  : undefined;
                              return (
                                <TextInput
                                  id="industry"
                                  label="Industry"
                                  placeholder=""
                                  input={{...input, value: ssic?.description ?? input.value}}
                                  meta={meta}
                                  readOnly
                                />
                              );
                            }}
                          </Query>
                        );
                      }}
                    </Field>
                  </div>
                </div>
                <hr className="b--black-10 bb mv4" />
              </div>
            );
          }}
        </GetCompanyInfoQuery>
      </Condition>
    </>
  );
};
