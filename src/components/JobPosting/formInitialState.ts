import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';

export const formInitialState: IJobPostingFormState = {
  jobDescription: {
    isThirdPartyEmployer: false,
    thirdPartyEmployer: {},
  },
  jobPostingSkills: {
    addedSkills: [],
    recommendedSkills: [],
  },
  keyInformation: {
    newPostingDate: '',
    jobCategories: [],
    schemes: [],
  },
  workplaceDetails: {
    location: Location.Local,
    locationType: LocationType.SameAsCompany,
  },
  screeningQuestions: {
    includeScreeningQuestions: false,
    questions: [''],
  },
};
