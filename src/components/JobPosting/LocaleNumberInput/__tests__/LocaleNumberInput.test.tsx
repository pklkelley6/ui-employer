import React from 'react';
import {mount} from 'enzyme';
import {FieldInputProps} from 'react-final-form';
import {LocaleNumberInput, getNumberFromLocaleString} from '../LocaleNumberInput';

describe('LocaleNumberInput', () => {
  const inputProps: FieldInputProps<number | undefined> = {
    name: 'some-field',
    onBlur: jest.fn(),
    onChange: jest.fn(),
    onFocus: jest.fn(),
    value: 12345678,
  };
  it('should render the input value correctly', async () => {
    const localeNumberInput = mount(<LocaleNumberInput id="id" placeholder="" input={inputProps} />);
    expect(localeNumberInput.find('input').prop('value')).toEqual('12,345,678');
  });

  describe('getNumberFromLocaleString', () => {
    it.each`
      localeString    | result
      ${'12,345,678'} | ${12345678}
      ${'12.345.678'} | ${12345678}
      ${'12a,345'}    | ${12345}
      ${'a12,345b'}   | ${12345}
      ${'12345'}      | ${12345}
    `('should return $result from $localeString', ({localeString, result}) => {
      expect(getNumberFromLocaleString(localeString)).toEqual(result);
    });
  });
});
