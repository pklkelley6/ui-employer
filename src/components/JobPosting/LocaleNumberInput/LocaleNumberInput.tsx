import {TextInput} from '@govtechsg/mcf-mcfui';
import React from 'react';
import {FieldInputProps, FieldMetaState} from 'react-final-form';

export const getNumberFromLocaleString = (value: string) =>
  value === '' ? value : Number(value.replace(/[^\d]/g, ''));

interface ILocaleNumberInput {
  id: string;
  placeholder: string;
  label?: string;
  input: FieldInputProps<number | undefined>;
  meta?: FieldMetaState<number | undefined>;
}
export const LocaleNumberInput: React.FunctionComponent<ILocaleNumberInput> = ({
  id,
  placeholder,
  label,
  input,
  meta,
}) => {
  return (
    <TextInput
      id={id}
      label={label}
      placeholder={placeholder}
      input={{
        ...input,
        onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
          const valueAfterFormat = getNumberFromLocaleString(e.target.value).toLocaleString();
          const differenceInLengthBetweenBeforeAndAfterFormat = valueAfterFormat.length - e.target.value.length;
          const cursorPos = Math.max((e.target.selectionStart ?? 0) + differenceInLengthBetweenBeforeAndAfterFormat, 0);
          // set the value with after format, so that the cursor doesn't jump after final form setState
          e.target.value = valueAfterFormat;
          // set the cursor position
          e.target.setSelectionRange(cursorPos, cursorPos);
          input.onChange(e);
        },
        value: input.value ? input.value.toLocaleString() : '',
      }}
      meta={meta}
    />
  );
};
