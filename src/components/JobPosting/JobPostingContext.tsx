import {noop} from 'lodash';
import {createContext, Dispatch, SetStateAction} from 'react';

interface IJobPostingContext {
  jobDescriptionChanged: boolean;
  setFormLoading: Dispatch<SetStateAction<boolean>>;
  setShowErrorCard: Dispatch<SetStateAction<boolean>>;
  showErrorCard: boolean;
}

const defaultValue: IJobPostingContext = {
  jobDescriptionChanged: false,
  setFormLoading: noop,
  setShowErrorCard: noop,
  showErrorCard: false,
};

export const JobPostingContext = createContext<IJobPostingContext>(defaultValue);
