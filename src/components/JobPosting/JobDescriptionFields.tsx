import {TextInput, TypeAhead} from '@govtechsg/mcf-mcfui';
import {stateFromHTML} from 'draft-js-import-html';
import React, {useContext} from 'react';
import {Query} from 'react-apollo';
import {Field} from 'react-final-form';
import {JobPostingContext} from './JobPostingContext';
import {ThirdPartyEmployerFields} from './ThirdPartyEmployerFields';
import {IJobPostingFormState} from './JobPostingForm';
import {TextEditor} from '~/components/JobPosting/TextEditor';
import {FormLoader} from '~/components/Layouts/FormLoader';
import {GetCommonQuery, GetCommonQueryVariables} from '~/graphql/__generated__/types';
import {GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {composeValidators, defaultRequired, maxLength, minLength, required} from '~/util/fieldValidations';
import {minJobDescriptionWordCount} from '~/util/jobPosts';

const JOB_DESCRIPTION_MAX_LENGTH = 20000;
const JOB_TITLE_MIN_LENGTH = 3;
const JOB_TITLE_MAX_LENGTH = 200;

const getValueFromHTML = (value?: string) => stateFromHTML(value || '').getPlainText();

export const JobDescriptionFields: React.FunctionComponent = () => {
  const {setFormLoading} = useContext(JobPostingContext);
  return (
    <Query<GetCommonQuery, GetCommonQueryVariables> query={GET_SSOC_LIST}>
      {({loading, error, data}) => {
        if (loading || error) {
          setFormLoading(true);
          return <FormLoader className="o-50" cardNumber={2} />;
        }
        setFormLoading(false);
        return (
          <>
            <ThirdPartyEmployerFields />
            <div className="flex">
              <div className="pa2 w-50" data-cy="jobtitle">
                <Field
                  name="jobDescription.title"
                  validate={composeValidators(
                    defaultRequired,
                    maxLength(
                      JOB_TITLE_MAX_LENGTH,
                      `Please keep within ${JOB_TITLE_MAX_LENGTH.toLocaleString()} characters`,
                    ),
                    minLength(
                      JOB_TITLE_MIN_LENGTH,
                      `Please enter at least ${JOB_TITLE_MIN_LENGTH.toLocaleString()} characters`,
                    ),
                  )}
                >
                  {({input, meta}) => {
                    return (
                      <TextInput
                        id="job-title"
                        label="Job Title"
                        placeholder="Enter Job Title"
                        input={input}
                        subtitle="Enter the job title to be displayed in the job post"
                        meta={meta}
                      />
                    );
                  }}
                </Field>
              </div>
              <div className="pa2 w-50 typeahead-field" data-cy="joboccupation">
                <Field name="jobDescription.occupation" validate={required('Please type and select')}>
                  {({input, meta}) => {
                    return (
                      <TypeAhead
                        id="job-occupation"
                        label="Occupation"
                        placeholder="Search..."
                        input={{...input, className: ''}}
                        data={data!.common.ssocList}
                        targetKey="ssocTitle"
                        valueKey="ssoc"
                        subtitle="Search for an occupation that fits the job title"
                        meta={meta}
                        resultsLength={100}
                      />
                    );
                  }}
                </Field>
              </div>
            </div>
            <div className="pa2 pt4 w-100" data-cy="jobdescription">
              <Field<IJobPostingFormState['jobDescription']['description']>
                name="jobDescription.description"
                validate={(value, allValues) => {
                  const ssoc = (allValues as Partial<IJobPostingFormState>).jobDescription?.occupation;
                  const jobDescriptionMinLength = minJobDescriptionWordCount(ssoc);

                  return composeValidators(
                    defaultRequired,
                    maxLength(
                      JOB_DESCRIPTION_MAX_LENGTH,
                      `Please keep within ${JOB_DESCRIPTION_MAX_LENGTH.toLocaleString()} characters`,
                    ),
                    minLength(
                      jobDescriptionMinLength,
                      `Please enter at least ${jobDescriptionMinLength.toLocaleString()} characters`,
                    ),
                  )(getValueFromHTML(value));
                }}
              >
                {({input, meta}) => {
                  return (
                    <TextEditor
                      input={input}
                      label="Job Description & Requirements"
                      subtitle="Help applicants understand the roles and responsibilities for the job"
                      maxLength={JOB_DESCRIPTION_MAX_LENGTH}
                      meta={meta}
                    />
                  );
                }}
              </Field>
            </div>
          </>
        );
      }}
    </Query>
  );
};
