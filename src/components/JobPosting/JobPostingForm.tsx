import {isEqual, noop} from 'lodash/fp';
import {format} from 'date-fns';
import React, {useEffect, useState} from 'react';
import {Form} from 'react-final-form';
import {OnChange} from 'react-final-form-listeners';
import {useSelector} from 'react-redux';
import {RouteComponentProps} from 'react-router';
import {Link} from 'react-router-dom';
import arrayMutators from 'final-form-arrays';
import styles from './JobPostingForm.scss';
import {JobDescriptionFields} from '~/components/JobPosting/JobDescriptionFields';
import {JobPostingContext} from '~/components/JobPosting/JobPostingContext';
import {CreateKeyInformationFields} from '~/components/JobPosting/KeyInformation/CreateKeyInformationFields';
import Preview from '~/components/JobPosting/Preview';
import {CreateJobSkillsFields} from '~/components/JobPosting/Skills/CreateJobSkillsFields';
import {CreateWorkplaceDetailsFields} from '~/components/JobPosting/WorkplaceDetails/CreateWorkplaceDetailsFields';
import {ScreeningQuestionsFields} from '~/components/JobPosting/ScreeningQuestions/ScreeningQuestionsFields';
import {ErrorCard} from '~/components/Layouts/ErrorCard';
import {useJobPostingSteps, IJobPostingStep} from '~/components/JobPosting/useJobPostingSteps';
import {formInitialState} from '~/components/JobPosting/formInitialState';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {IPillSkill} from '~/components/JobPosting/Skills/skills.types';
import {getMcfSystemDate} from '~/flux/jobPosting/jobPosting.selectors';
import {IAppState} from '~/flux';
import {ICompanyAddress} from '~/services/employer/company';

let isFormPristine: boolean;

const askBeforeUnload = (event: BeforeUnloadEvent) => {
  if (!isFormPristine) {
    event.returnValue = 'You have unfinished changes!';
  }
};

export interface IJobPostingFormState {
  jobDescription: {
    description?: string;
    title?: string;
    occupation?: number;
    otherRequirements?: string;
    isThirdPartyEmployer: boolean;
    thirdPartyEmployer: {
      entityId?: string;
      name?: string;
      industry?: string;
      address?: ICompanyAddress;
    };
  };
  jobPostingSkills: {
    addedSkills: IPillSkill[];
    recommendedSkills: IPillSkill[];
  };
  keyInformation: {
    newPostingDate: string;
    jobPostDuration?: number;
    numberOfVacancies?: number;
    jobCategories: number[];
    positionLevel?: number;
    minimumYearsExperience?: number;
    employmentType?: number[];
    qualification?: string;
    fieldOfStudy?: string;
    minimumSalary?: number;
    maximumSalary?: number;
    schemes: Array<{id: number; subSchemeId?: number; selected: boolean}>;
  };
  workplaceDetails: {
    location: Location;
    locationType: LocationType;
    postalCode?: string;
    block?: string;
    street?: string;
    building?: string;
    overseasCountry?: string;
    foreignAddress1?: string;
    foreignAddress2?: string;
  };
  screeningQuestions: {
    includeScreeningQuestions: boolean;
    questions: string[];
  };
}

interface IJobPostingFormProps extends RouteComponentProps {
  initialValues?: IJobPostingFormState;
  jobDescription?: React.ReactElement;
  jobSkills?: React.ReactElement;
  keyInformation?: React.ReactElement;
  workplaceDetails?: React.ReactElement;
  screeningQuestions?: React.ReactElement;
  preview?: React.ReactElement;
  onTitleChange?: (values: IJobPostingFormState['jobDescription']['title']) => void;
  onSubmit: (values: IJobPostingFormState) => void;
  startJobPosting: () => void;
}

export const JobPostingForm: React.FunctionComponent<IJobPostingFormProps> = ({
  initialValues,
  jobDescription,
  jobSkills,
  keyInformation,
  screeningQuestions,
  workplaceDetails,
  history,
  location,
  preview,
  onTitleChange = noop,
  onSubmit,
  startJobPosting,
}) => {
  const mcfSystemDate = useSelector((state: IAppState) => getMcfSystemDate(state));

  const intialiseFormState = {
    ...formInitialState,
    keyInformation: {
      ...formInitialState.keyInformation,
      newPostingDate: mcfSystemDate ? mcfSystemDate : format(new Date(), 'yyyy-MM-dd'),
    },
  };

  const initialState = initialValues ? initialValues : intialiseFormState;
  const [jobDescriptionChanged, setJobDescriptionChanged] = useState(true);
  const [showErrorCard, setShowErrorCard] = useState(false);
  const [formLoading, setFormLoading] = useState(false);
  const jobPostingSteps = useJobPostingSteps();

  // only gets updated everytime next is clicked
  const [formState, setFormState] = useState(intialiseFormState);

  useEffect(() => {
    window.addEventListener('beforeunload', askBeforeUnload);
    return () => {
      window.removeEventListener('beforeunload', askBeforeUnload);
    };
  }, []);

  useEffect(() => {
    startJobPosting();
  }, []);

  let currentStepIndex = jobPostingSteps.findIndex((step) => step.id === location.hash);
  currentStepIndex = currentStepIndex === -1 ? 0 : currentStepIndex;
  const currentStep = jobPostingSteps[currentStepIndex];

  const stepIndicator = jobPostingSteps.map((step, index) => (
    <Link
      to={{
        hash: step.id,
        state: location.state,
      }}
      key={step.id}
      className={`no-underline pa3 f6 ${
        step.id === currentStep.id
          ? `${styles.selected} fw7 black-80 disabled-link`
          : `${index < currentStepIndex ? 'blue fw6' : 'black-30 fw6 disabled-link'}`
      }`}
      data-cy={`${step.description.replace(/\s+/g, '-')}-link`}
    >
      <div className="tc ml1">{index + 1}.</div>
      <div className="tc">{step.description}</div>
    </Link>
  ));

  const onNext = (values: IJobPostingFormState, nextStep: string) => {
    setJobDescriptionChanged(
      !isEqual(formState.jobDescription.title)(values.jobDescription.title) ||
        !isEqual(formState.jobDescription.description)(values.jobDescription.description),
    );
    setFormState(values);

    history.push(nextStep, location.state);
  };

  const onPrevious = (previousStep?: string) => {
    if (previousStep) {
      setShowErrorCard(false);
      history.push(previousStep, location.state);
    }
  };

  const renderFormForStep = (step: IJobPostingStep) => {
    switch (step.id) {
      case '#job-description':
        return jobDescription || <JobDescriptionFields />;
      case '#skills':
        return jobSkills || <CreateJobSkillsFields />;
      case '#key-information':
        return keyInformation || <CreateKeyInformationFields />;
      case '#workplace-details':
        return workplaceDetails || <CreateWorkplaceDetailsFields />;
      case '#screening-questions':
        return screeningQuestions || <ScreeningQuestionsFields />;
      case '#preview':
        return preview || <Preview />;

      default:
        return <>Blank</>;
    }
  };

  return (
    <Form<IJobPostingFormState>
      onSubmit={(values) => {
        if (currentStep.next) {
          return onNext(values, currentStep.next);
        }
        return onSubmit(values);
      }}
      initialValues={initialState}
      initialValuesEqual={isEqual}
      mutators={{...arrayMutators}}
      render={({
        handleSubmit,
        hasValidationErrors,
        submitting,
        pristine,
        values,
        submitError,
        submitFailed,
        dirtySinceLastSubmit,
      }) => {
        // NOTE: have to calc ourselves since final-form calc `pristine` only for visible fields
        isFormPristine = pristine && isEqual(values, initialState);
        return (
          <section className="flex-auto db">
            <div data-cy="job-posting-steps" className="flex justify-around bg-white">
              {stepIndicator}
            </div>

            <form onSubmit={handleSubmit}>
              <OnChange name={'jobDescription.title'}>
                {(title: IJobPostingFormState['jobDescription']['title']) => onTitleChange(title)}
              </OnChange>
              <JobPostingContext.Provider
                value={{
                  jobDescriptionChanged,
                  setFormLoading,
                  setShowErrorCard,
                  showErrorCard,
                }}
              >
                <div className={`bg-black-05 mb3 pa4 pt4`}>{renderFormForStep(currentStep)}</div>
              </JobPostingContext.Provider>
              {submitFailed && hasValidationErrors && showErrorCard ? (
                <div className="pb3">
                  <ErrorCard message="Please amend the highlighted fields to continue" />
                </div>
              ) : null}
              {!dirtySinceLastSubmit && submitError && <div className="bg-red white pa3 mb3">{submitError}</div>}
              <div className="flex justify-end">
                {currentStep.previous && (
                  <button
                    data-cy="new-post-previous"
                    className={`${styles.button} pa3 db ba b--primary primary bg-white`}
                    onClick={() => onPrevious(currentStep.previous)}
                    type="button"
                  >
                    Back
                  </button>
                )}
                <button
                  data-cy="new-post-next"
                  className={`${styles.button} pa3 db ml3 b--primary white bg-primary`}
                  onClick={() => setShowErrorCard(true)}
                  disabled={submitting || formLoading}
                  type="submit"
                >
                  {currentStep.next ? 'Next' : 'Submit Job Post'}
                </button>
              </div>
            </form>
          </section>
        );
      }}
    />
  );
};
