import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import {ScreeningQuestionsFields} from '../ScreeningQuestions/ScreeningQuestionsFields';
import {formInitialState} from '~/components/JobPosting/formInitialState';
import {nextTick} from '~/testUtil/enzyme';

jest.mock('~/components/JobPosting/formInitialState');

describe('JobPosting/ScreeningQuestionsFields', () => {
  it('should render ScreeningQuestionsFields initial state checkbox unchecked', async () => {
    const wrapper = mount(
      <Form
        initialValues={formInitialState}
        onSubmit={noop}
        mutators={{...arrayMutators}}
        render={() => <ScreeningQuestionsFields />}
      />,
    );

    expect(wrapper.find('input#screening-questions-checkbox').props().checked).toEqual(false);
    expect(wrapper.find('[data-cy="screening-questions-field"]')).toHaveLength(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('should disable remove button if theres only one question field', async () => {
    const wrapper = mount(
      <Form
        initialValues={formInitialState}
        onSubmit={noop}
        mutators={{...arrayMutators}}
        render={() => <ScreeningQuestionsFields />}
      />,
    );

    act(() => {
      wrapper.find('input#screening-questions-checkbox').simulate('change', {target: {checked: true}});
    });
    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="screening-questions-field"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="screening-questions-remove"]').first().props().disabled).toEqual(true);

    expect(wrapper).toMatchSnapshot();
  });

  it('should render disable add button if there are 3 questions', async () => {
    const wrapper = mount(
      <Form
        initialValues={{
          ...formInitialState,
          screeningQuestions: {
            includeScreeningQuestions: true,
            questions: ['', '', ''],
          },
        }}
        onSubmit={noop}
        mutators={{...arrayMutators}}
        render={() => <ScreeningQuestionsFields />}
      />,
    );

    expect(wrapper.find('input#screening-questions-checkbox').props().checked).toEqual(true);
    expect(wrapper.find('[data-cy="screening-questions-field"]')).toHaveLength(3);
    expect(wrapper.find('[data-cy="screening-questions-add"]').props().disabled).toEqual(true);

    expect(wrapper).toMatchSnapshot();
  });

  it('should not render fields and add button if includeScreeningQuestions is false', async () => {
    const wrapper = mount(
      <Form
        initialValues={{
          ...formInitialState,
          screeningQuestions: {
            includeScreeningQuestions: false,
            questions: ['', '', ''],
          },
        }}
        onSubmit={noop}
        mutators={{...arrayMutators}}
        render={() => <ScreeningQuestionsFields />}
      />,
    );

    expect(wrapper.find('input#screening-questions-checkbox').props().checked).toEqual(false);
    expect(wrapper.find('[data-cy="screening-questions-field"]')).toHaveLength(0);
    expect(wrapper.find('[data-cy="screening-questions-add"]')).toHaveLength(0);

    expect(wrapper).toMatchSnapshot();
  });
});
