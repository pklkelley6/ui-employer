import {MockedProvider} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import Preview from '../Preview';
import {IJobPostingFormState} from '~/components/JobPosting/JobPostingForm';
import {jobPostingSteps, jobPostingStepsDeprecated} from '~/components/JobPosting/useJobPostingSteps';
import {Location, LocationType} from '~/components/JobPosting/WorkplaceDetails/workplaceDetails.types';
import {GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {nextTick} from '~/testUtil/enzyme';

describe('JobPosting/Preview', () => {
  const getSsocQueryMock = [
    {
      request: {
        query: GET_SSOC_LIST,
      },
      result: {
        data: {
          common: {
            ssocList: [
              {
                ssoc: 1,
                ssocTitle: 'some',
              },
            ],
          },
        },
      },
    },
  ];
  const initialValues: IJobPostingFormState = {
    jobDescription: {
      description: '',
      occupation: 123,
      isThirdPartyEmployer: false,
      thirdPartyEmployer: {},
      title: '',
    },
    jobPostingSkills: {
      addedSkills: [],
      recommendedSkills: [],
    },
    keyInformation: {
      newPostingDate: '2021-10-01',
      employmentType: [7],
      fieldOfStudy: '0521',
      jobCategories: [1, 4, 7, 8],
      jobPostDuration: 14,
      maximumSalary: 1212,
      minimumSalary: 12,
      minimumYearsExperience: 12,
      numberOfVacancies: 12,
      positionLevel: 1,
      qualification: '91',
      schemes: [
        {id: mcf.SCHEME_ID.P_MAX, selected: true},
        {id: mcf.SCHEME_ID.CAREER_TRIAL, selected: true},
      ],
    },
    workplaceDetails: {
      block: '',
      building: '',
      foreignAddress1: '',
      foreignAddress2: '',
      location: Location.Local,
      locationType: LocationType.None,
      overseasCountry: '',
      postalCode: '',
      street: '',
    },
    screeningQuestions: {
      includeScreeningQuestions: true,
      questions: ['question'],
    },
  };

  const reduxState = {
    company: {
      companyInfo: {
        uen: '',
      },
    },
    features: {
      screeningQuestions: true,
    },
  };

  it('should render all edit links', async () => {
    const store = configureStore()(reduxState);
    const component = mount(
      <MockedProvider mocks={getSsocQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <Form initialValues={initialValues} onSubmit={noop} render={() => <Preview />} />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(component);

    const links = component.find(Link);

    expect(links).toHaveLength(jobPostingSteps.length - 1);
    links.forEach((link, index) => {
      expect(link.prop('to')).toStrictEqual({hash: jobPostingSteps[index].id, state: undefined});
    });
  });

  it('should render all edit links without screening questions if screeningQuestions toggle is off', async () => {
    const store = configureStore()({...reduxState, features: {}});
    const component = mount(
      <MockedProvider mocks={getSsocQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <Form initialValues={initialValues} onSubmit={noop} render={() => <Preview />} />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(component);

    const links = component.find(Link);

    expect(links).toHaveLength(jobPostingStepsDeprecated.length - 1);
    links.forEach((link, index) => {
      expect(link.prop('to')).toStrictEqual({hash: jobPostingStepsDeprecated[index].id, state: undefined});
    });
  });

  it('should not render edit links in screening questions if screeningQuestions feature toggle is on, isScreeningQuestionsReadOnly is true', async () => {
    const store = configureStore()(reduxState);
    const component = mount(
      <MockedProvider mocks={getSsocQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <Form
              initialValues={initialValues}
              onSubmit={noop}
              render={() => <Preview isScreeningQuestionsReadOnly={true} />}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(component);

    expect(component.find('Link[id="#screening-questions"]')).toHaveLength(0);

    const links = component.find(Link);

    links.forEach((link, index) => {
      expect(link.prop('to')).toStrictEqual({hash: jobPostingSteps[index].id, state: undefined});
    });
  });

  it('should render edit links in screening questions if screeningQuestions feature toggle is on, isScreeningQuestionsReadOnly is false', async () => {
    const store = configureStore()(reduxState);
    const component = mount(
      <MockedProvider mocks={getSsocQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <Form
              initialValues={initialValues}
              onSubmit={noop}
              render={() => <Preview isScreeningQuestionsReadOnly={false} />}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(component);

    expect(component.find('Link[id="#screening-questions"]')).toHaveLength(1);
  });
});
