import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import {noop} from 'lodash';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {Form} from 'react-final-form';
import {print} from 'graphql';
import {JobDescriptionFields} from '../JobDescriptionFields';
import {PactBuilder} from '~/__mocks__/pact';
import {GET_SSOC_LIST} from '~/graphql/jobs/jobs.query';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

describe('JobPosting/JobDescriptionFields', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  beforeEach(async () => {
    const queryMockResult = {
      data: {
        common: {
          ssocList: Matchers.eachLike({ssoc: 1, ssocTitle: 'ABLE SEAMAN'}),
        },
      },
    };
    const graphqlQuery = new ApolloGraphQLInteraction()
      .uponReceiving('getSSOCList')
      .withQuery(print(GET_SSOC_LIST))
      .withOperation('getCommon')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .willRespondWith({
        body: queryMockResult,
        status: 200,
      });
    return pactBuilder.provider.addInteraction(graphqlQuery);
  });
  afterAll(async () => pactBuilder.provider.finalize());

  it('should render JobDescriptionFields correctly', async () => {
    mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <Form onSubmit={noop} render={() => <JobDescriptionFields />} />
      </ApolloProvider>,
    );
    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });
});
