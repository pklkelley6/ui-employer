import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {noop} from 'lodash/fp';
import React from 'react';
import {Form} from 'react-final-form';
import {Provider} from 'react-redux';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {JobDescriptionFields} from '../JobDescriptionFields';
import {JobPostingForm} from '../JobPostingForm';
import {formInitialState} from '../formInitialState';
import {CreateKeyInformationFields} from '../KeyInformation/CreateKeyInformationFields';
import {CreateJobSkillsFields} from '../Skills/CreateJobSkillsFields';
import {CreateWorkplaceDetailsFields} from '../WorkplaceDetails/CreateWorkplaceDetailsFields';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {jobPostingSteps, jobPostingStepsDeprecated} from '~/components/JobPosting/useJobPostingSteps';
import {companyInfoMock} from '~/__mocks__/company/company.mocks';

jest.mock('~/components/JobPosting/formInitialState');

describe('JobPostingForm', () => {
  const mock: any = {};
  const routeComponentProps = {
    history: {
      ...mock,
      push: jest.fn(),
    },
    match: mock,
  };
  const startJobPosting = jest.fn();
  const reduxState = {
    company: {companyInfo: companyInfoMock},
    jobTitles: {
      results: [],
    },
    account: {
      data: {exp: '1633061459'}, // 2021-10-01
    },
    features: {
      screeningQuestions: true,
    },
  };
  const store = configureStore()(reduxState);
  const SpecifiedComponent: React.FunctionComponent<{text: string}> = ({text}) => <div>{text}</div>;

  afterEach(() => {
    startJobPosting.mockReset();
  });

  it('should render form with formInitialState if initialValues prop is undefined', async () => {
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={mock}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);

    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    expect(wrapper.find(Form).prop('initialValues')).toEqual(formInitialState);

    const stepLinks = wrapper.find('[data-cy="job-posting-steps"] Link');
    expect(stepLinks).toHaveLength(jobPostingSteps.length);
    stepLinks.forEach((stepLink, index) => {
      expect(stepLink.prop('to')).toEqual({hash: jobPostingSteps[index].id});
    });
  });

  it('should render form with 5 steps if screeningQuestions toggle is off', async () => {
    const wrapper = mount(
      <MockedProvider>
        <Provider store={configureStore()({...reduxState, features: {}})}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={mock}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    expect(wrapper.find(Form).prop('initialValues')).toEqual(formInitialState);

    const stepLinks = wrapper.find('[data-cy="job-posting-steps"] Link');
    expect(stepLinks).toHaveLength(jobPostingStepsDeprecated.length);
    stepLinks.forEach((stepLink, index) => {
      expect(stepLink.prop('to')).toEqual({hash: jobPostingStepsDeprecated[index].id});
    });
  });

  it('should render form with the initialValues prop if it is set', async () => {
    const initialValues = {
      ...formInitialState,
      keyInformation: {
        newPostingDate: '2021-10-01',
        jobCategories: [],
        schemes: [],
      },
      jobDescription: {
        ...formInitialState.jobDescription,
        description: 'Job Description',
        occupation: 3012,
        title: 'Accountant',
      },
    };
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={mock}
              onSubmit={noop}
              initialValues={initialValues}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    expect(wrapper.find(Form).prop('initialValues')).toEqual(initialValues);
  });

  it('should render the form with the jobDescription component specified in props', async () => {
    const currentStep = 0;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              jobDescription={<SpecifiedComponent text="This is a job description component" />}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(JobDescriptionFields)).toHaveLength(0);
    expect(wrapper.find(SpecifiedComponent)).toHaveLength(1);
    expect(wrapper.find(SpecifiedComponent).prop('text')).toEqual('This is a job description component');
  });

  it('should render the form with the default job description component if jobDescription prop is not defined', async () => {
    const currentStep = 0;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(JobDescriptionFields)).toHaveLength(1);
  });

  it('should call startJobPosting on mount', async () => {
    const currentStep = 0;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(startJobPosting).toBeCalledTimes(1);
  });

  it('should render the form with the job skills component specified in props', async () => {
    const currentStep = 1;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              jobSkills={<SpecifiedComponent text="This is a job skills component" />}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateJobSkillsFields)).toHaveLength(0);
    expect(wrapper.find(SpecifiedComponent)).toHaveLength(1);
    expect(wrapper.find(SpecifiedComponent).prop('text')).toEqual('This is a job skills component');
  });

  it('should render the form with the default skills component if jobSkills prop is not defined', async () => {
    const currentStep = 1;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateJobSkillsFields)).toHaveLength(1);
  });

  it('should render the form with the key information component specified in props', async () => {
    const currentStep = 2;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              keyInformation={<SpecifiedComponent text="This is a key information component" />}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateKeyInformationFields)).toHaveLength(0);
    expect(wrapper.find(SpecifiedComponent)).toHaveLength(1);
    expect(wrapper.find(SpecifiedComponent).prop('text')).toEqual('This is a key information component');
  });

  it('should render the form with the default key information component if keyInformation prop is not defined', async () => {
    const currentStep = 2;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateKeyInformationFields)).toHaveLength(1);
  });

  it('should render the form with the workplace details component specified in props', async () => {
    const currentStep = 3;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              workplaceDetails={<SpecifiedComponent text="This is a workplace details component" />}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateWorkplaceDetailsFields)).toHaveLength(0);
    expect(wrapper.find(SpecifiedComponent)).toHaveLength(1);
    expect(wrapper.find(SpecifiedComponent).prop('text')).toEqual('This is a workplace details component');
  });

  it('should render the form with the default workplace details component if workplaceDetails prop is not defined', async () => {
    const currentStep = 3;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find(CreateWorkplaceDetailsFields)).toHaveLength(1);
  });

  it('should disable navigation for current and next steps', async () => {
    const currentStep = 2;
    const wrapper = mount(
      <MockedProvider>
        <Provider store={store}>
          <Router>
            <JobPostingForm
              {...routeComponentProps}
              startJobPosting={startJobPosting}
              location={{
                ...mock,
                hash: jobPostingSteps[currentStep].id,
              }}
              onSubmit={noop}
            />
          </Router>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);

    // can navigate to step 1-2
    expect(wrapper.find(Link).at(0).prop('className')).not.toContain('disabled-link');
    expect(wrapper.find(Link).at(1).prop('className')).not.toContain('disabled-link');

    // cant' navigate to step 3-5
    expect(wrapper.find(Link).at(2).prop('className')).toContain('disabled-link');
    expect(wrapper.find(Link).at(3).prop('className')).toContain('disabled-link');
    expect(wrapper.find(Link).at(4).prop('className')).toContain('disabled-link');
  });
});
