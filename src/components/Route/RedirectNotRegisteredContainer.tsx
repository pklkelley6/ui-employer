import {connect} from 'react-redux';
import {RedirectNotRegistered} from '~/components/Route/RedirectNotRegistered';
import {IAppState} from '~/flux/index';
import {
  areSystemContactUpdated,
  areTermsAndConditionsAccepted,
  isLoadingFinished,
} from '~/flux/systemContact/systemContact.selectors';

const mapStateToProps = (state: IAppState) => ({
  hasRegistered: areSystemContactUpdated(state) && areTermsAndConditionsAccepted(state),
  loaded: isLoadingFinished(state),
});
export const RedirectNotRegisteredContainer = connect(mapStateToProps)(RedirectNotRegistered);
