import {isNil} from 'lodash/fp';
import {connect} from 'react-redux';
import {RedirectUnauthorised} from './RedirectUnauthorised';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';

const mapStateToProps = (state: IAppState) => ({
  hasRedirectCondition: isNil(getAccountUser(state)),
});
export const RedirectUnauthorisedContainer = connect(mapStateToProps)(RedirectUnauthorised);
