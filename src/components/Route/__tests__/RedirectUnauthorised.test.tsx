import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Route, Switch} from 'react-router';
import {MemoryRouter} from 'react-router-dom';
import {RedirectUnauthorised} from '../RedirectUnauthorised';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('RedirectUnauthorised', () => {
  it('renders correctly when should redirect', () => {
    const wrapper = mount(
      <MemoryRouter>
        <Switch>
          <Route path="/unauthorised" render={() => <div>Unauthorized</div>} />
          <RedirectUnauthorised hasRedirectCondition={true}>
            <div> foo </div>
          </RedirectUnauthorised>
        </Switch>
      </MemoryRouter>,
    );
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('renders correctly when should not redirect', () => {
    const wrapper = mount(
      <RedirectUnauthorised hasRedirectCondition={false}>
        <div> foo </div>
      </RedirectUnauthorised>,
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders correctly when should not redirect because user has not registered', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={['/']} keyLength={0}>
        <Switch>
          <Route path="/terms-and-conditions" render={() => <div>Unauthorized</div>} />
          <RedirectUnauthorised hasRedirectCondition={true}>
            <div> foo </div>
          </RedirectUnauthorised>
        </Switch>
      </MemoryRouter>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
