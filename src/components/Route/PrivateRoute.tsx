import React from 'react';
import {Route, RouteProps} from 'react-router-dom';
import {RedirectUnauthorisedContainer} from './RedirectUnauthorisedContainer';
import {RedirectNotRegisteredContainer} from '~/components/Route/RedirectNotRegisteredContainer';

export const PrivateRoute = ({component: BaseComponent, render, path, ...rest}: RouteProps) => (
  <Route
    {...rest}
    path={path}
    render={(props) => {
      const toRender = BaseComponent ? (
        <BaseComponent
          {...props}
          key={props.location.state?.key} // trigger a remount on BaseComponent if key is set
        />
      ) : render ? (
        render(props)
      ) : undefined;
      return (
        <RedirectUnauthorisedContainer>
          <RedirectNotRegisteredContainer path={path} privateElement={toRender} />
        </RedirectUnauthorisedContainer>
      );
    }}
  />
);
