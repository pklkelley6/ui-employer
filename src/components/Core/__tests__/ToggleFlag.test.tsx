import {mount} from 'enzyme';
import React from 'react';
import {ToggleFlag} from '~/components/Core/ToggleFlag';

describe('ToggleFlag', () => {
  const flags = {
    jobPost: false,
    systemContact: true,
  };
  test('should render component when feature flag is set to true', () => {
    const wrapper = mount(
      <ToggleFlag
        flags={flags}
        name="systemContact"
        render={() => <div>System contact is active</div>}
        fallback={() => <div>This feature is not available</div>}
      />,
    );
    expect(wrapper.find('div').text()).toEqual('System contact is active');
  });
  test('should render fallback component when feature flag is set to false', () => {
    const wrapper = mount(
      <ToggleFlag
        flags={flags}
        name="jobPost"
        render={() => <div>Job post is active</div>}
        fallback={() => <div>This feature is not available</div>}
      />,
    );
    expect(wrapper.find('div').text()).toEqual('This feature is not available');
  });
  test('should not render anything when there is no render function and feature flag is true', () => {
    const wrapper = mount(<ToggleFlag flags={flags} name="systemContact" />);
    expect(wrapper.find('div').length).toEqual(0);
  });
  test('should not render anything when there is no fallback function and feature flag is false', () => {
    const wrapper = mount(<ToggleFlag flags={flags} name="jobPost" />);
    expect(wrapper.find('div').length).toEqual(0);
  });
});
