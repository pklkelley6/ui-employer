import {mount} from 'enzyme';
import React from 'react';
import {Dropdown} from '../Dropdown';
const data = [
  {
    label: 'AA',
    value: 11,
  },
  {
    label: 'BB',
    value: 22,
  },
];

describe('Dropdown', () => {
  it.each`
    touched  | error
    ${false} | ${'present'}
    ${true}  | ${undefined}
    ${false} | ${undefined}
  `(
    'should not display error message when props.meta.touched is $touched and props.meta.error is $error',
    ({touched, error}) => {
      const meta = {
        error,
        touched,
      };
      const wrapper = mount(<Dropdown id="dropdownId" data={data} classNamePrefix="dropdown" meta={meta} />);
      expect(wrapper.find('#dropdownId-error').length).toEqual(0);
    },
  );

  it('should display error message if props.meta.touched is true and props.meta.error is present', () => {
    const meta = {
      error: 'test error message',
      touched: true,
    };
    const wrapper = mount(<Dropdown id="dropdownId" data={data} classNamePrefix="dropdown" meta={meta} />);
    expect(wrapper.find('#dropdownId-error').length).toEqual(1);
    expect(wrapper.find('#dropdownId-error').text()).toEqual('test error message');
  });

  describe('Single Dropwdown', () => {
    it('should display no label when there is no value', () => {
      const wrapper = mount(<Dropdown data={data} classNamePrefix="dropdown" />);
      expect(wrapper.find('.dropdown__single-value').length).toEqual(0);
      expect(wrapper.find('.dropdown__placeholder').hostNodes().text()).toEqual('Select...');
    });
    it('should display AA label when value is 11', () => {
      const wrapper = mount(<Dropdown data={data} input={{value: 11}} classNamePrefix="dropdown" />);
      expect(wrapper.find('.dropdown__single-value').hostNodes().text()).toEqual('AA');
      expect(wrapper.find('.dropdown__placeholder').length).toEqual(0);
    });
    it('should display CC label when value is 33 by mapping label and value', () => {
      const dataWithDifferentLabelAndValueKey = [
        {
          bar: 33,
          foo: 'CC',
        },
        {
          bar: 44,
          foo: 'DD',
        },
      ];
      const wrapper = mount(
        <Dropdown
          data={dataWithDifferentLabelAndValueKey}
          input={{value: 33}}
          classNamePrefix="dropdown"
          labelKey="foo"
          valueKey="bar"
        />,
      );
      expect(wrapper.find('.dropdown__single-value').hostNodes().text()).toEqual('CC');
      expect(wrapper.find('.dropdown__placeholder').length).toEqual(0);
    });
  });
  describe('Multi Dropwdown', () => {
    it('should display no label when there is no value', () => {
      const wrapper = mount(<Dropdown data={data} classNamePrefix="dropdown" isMulti />);
      expect(wrapper.find('.dropdown__single-value').length).toEqual(0);
      expect(wrapper.find('.dropdown__placeholder').hostNodes().text()).toEqual('Select...');
    });
    it('should display AA label when value is [11]', () => {
      const wrapper = mount(<Dropdown data={data} input={{value: [11]}} classNamePrefix="dropdown" isMulti />);
      expect(wrapper.find('.dropdown__multi-value__label').text()).toEqual('AA');
      expect(wrapper.find('.dropdown__placeholder').length).toEqual(0);
    });
    it('should display AA and BB label when value is [11, 22]', () => {
      const wrapper = mount(<Dropdown data={data} input={{value: [11, 22]}} classNamePrefix="dropdown" isMulti />);
      expect(wrapper.find('.dropdown__multi-value__label').length).toEqual(2);
      expect(wrapper.find('.dropdown__multi-value__label').at(0).text()).toEqual('AA');
      expect(wrapper.find('.dropdown__multi-value__label').at(1).text()).toEqual('BB');
      expect(wrapper.find('.dropdown__placeholder').length).toEqual(0);
    });
    it('should display CC label when value is 33 by mapping label and value', () => {
      const dataWithDifferentLabelAndValueKey = [
        {
          bar: 33,
          foo: 'CC',
        },
        {
          bar: 44,
          foo: 'DD',
        },
      ];
      const wrapper = mount(
        <Dropdown
          data={dataWithDifferentLabelAndValueKey}
          input={{value: [33]}}
          classNamePrefix="dropdown"
          labelKey="foo"
          valueKey="bar"
          isMulti
        />,
      );
      expect(wrapper.find('.dropdown__multi-value__label').text()).toEqual('CC');
      expect(wrapper.find('.dropdown__placeholder').length).toEqual(0);
    });
  });
});
