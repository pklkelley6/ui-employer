import {Interaction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {FetchQuery} from '../FetchQuery';
import {PactBuilder} from '~/__mocks__/pact';
import {config} from '~/config';
import {ICompanySuggestion} from '~/services/employer/getCompanyInfo';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('FetchQuery', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });
  afterAll(async () => {
    await pactBuilder.provider.finalize();
  });
  it('should render fetched data', async () => {
    const interaction = new Interaction()
      .uponReceiving('companies suggestion')
      .withRequest({
        method: 'GET',
        path: '/v2/companies/suggestions',
        query: {name: 'Republic'},
      })
      .willRespondWith({
        body: {results: Matchers.eachLike({name: 'Republic Poly', uen: 'poly-uen'})},
        status: 200,
      });
    await pactBuilder.provider.addInteraction(interaction);

    const wrapper = mount(
      <FetchQuery<{results: ICompanySuggestion[]}>
        input={`${config.url.apiJob.v2}/companies/suggestions?name=Republic`}
      >
        {({data, errorMessage, loading}) => {
          return <div>{JSON.stringify({data, errorMessage, loading})}</div>;
        }}
      </FetchQuery>,
    );
    expect(wrapper.find('div').text()).toEqual(JSON.stringify({loading: true}));
    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('div').text()).toEqual(
      JSON.stringify({data: {results: [{name: 'Republic Poly', uen: 'poly-uen'}]}, loading: false}),
    );
  });
  it('should render error when fetch fail', async () => {
    const interaction = new Interaction()
      .uponReceiving('companies suggestion with error')
      .withRequest({
        method: 'GET',
        path: '/v2/companies/suggestions',
        query: {name: 're'},
      })
      .willRespondWith({
        body: {
          message: Matchers.like('Validation errors'),
        },
        status: 400,
      });
    await pactBuilder.provider.addInteraction(interaction);

    const wrapper = mount(
      <FetchQuery<{results: ICompanySuggestion[]}> input={`${config.url.apiJob.v2}/companies/suggestions?name=re`}>
        {({data, errorMessage, loading}) => {
          return <div>{JSON.stringify({data, errorMessage, loading})}</div>;
        }}
      </FetchQuery>,
    );
    expect(wrapper.find('div').text()).toEqual(JSON.stringify({loading: true}));
    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });
    expect(wrapper.find('div').text()).toEqual(JSON.stringify({errorMessage: 'Validation errors', loading: false}));
  });
});
