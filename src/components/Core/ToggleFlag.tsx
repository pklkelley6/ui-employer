import React, {ReactElement} from 'react';
import {useSelector} from 'react-redux';
import {IAppState} from '~/flux';
import {IFeatureToggles} from '~/flux/feature';
import {IReleaseToggles} from '~/flux/index.reducers';

interface IToggleFlagProps<T extends Record<string, boolean>> {
  name: keyof T;
  flags?: T;
  render?: () => ReactElement;
  fallback?: () => ReactElement;
}

export const ToggleFlag: <T extends Record<string, boolean>>(props: IToggleFlagProps<T>) => React.ReactElement | null =
  ({name, flags, render, fallback}) => {
    const featureFlag: boolean = flags ? flags[name] : false;
    if (featureFlag && render) {
      return render();
    }
    if (!featureFlag && fallback) {
      return fallback();
    }
    return null;
  };

type IFeatureFlagProps = Omit<IToggleFlagProps<IFeatureToggles>, 'flags'>;

export const FeatureFlag: React.FunctionComponent<IFeatureFlagProps> = (props) => {
  const featureFlags = useSelector((state: IAppState) => state.features);
  return <ToggleFlag<IFeatureToggles> {...props} flags={featureFlags} />;
};

type IReleaseFlagProps = Omit<IToggleFlagProps<IReleaseToggles>, 'flags'>;

export const ReleaseFlag: React.FunctionComponent<IReleaseFlagProps> = (props) => {
  const releaseFlags = useSelector((state: IAppState) => state.releaseToggles);
  return <ToggleFlag<IReleaseToggles> {...props} flags={releaseFlags} />;
};
