import {noop} from 'lodash';
import React from 'react';
import Select from 'react-select';
import {Props} from 'react-select/base';
import {StylesConfig} from 'react-select/src/styles';
import {ThemeConfig} from 'react-select/src/theme';
import {SearchIcon} from '~/components/Icon/SearchIcon';
import {cssStyleVars} from '~/styles/variables';

// https://github.com/microsoft/TypeScript/issues/17002
// https://github.com/DefinitelyTyped/DefinitelyTyped/issues/35708
declare global {
  /* eslint-disable @typescript-eslint/naming-convention */
  interface ArrayConstructor {
    isArray(arg: ReadonlyArray<any> | any): arg is ReadonlyArray<any>;
  }
}

interface IBaseDropdownProps<
  Type extends Record<string, any>,
  LabelKey extends keyof Type,
  ValueKey extends keyof Type,
> {
  classNamePrefix?: string;
  data: Type[];
  innerProps?: {
    [key: string]: any;
  };
  id?: string;
  label?: string | JSX.Element;
  labelKey?: LabelKey;
  meta?: {
    error?: string;
    touched?: boolean;
  };
  isClearable?: boolean;
  isMulti?: boolean | null;
  isSearchable?: boolean;
  noOptionsMessage?: () => string;
  placeholder?: string;
  valueKey?: ValueKey;
  isDisabled?: boolean;
}

export interface IDefaultDropdownProps<
  Type extends Record<string, any>,
  LabelKey extends keyof Type = 'label',
  ValueKey extends keyof Type = 'value',
> extends IBaseDropdownProps<Type, LabelKey, ValueKey> {
  input?: {
    value?: Type[ValueKey];
    onChange?: (value: Type[ValueKey]) => void;
    onBlur?: () => void;
  };
  isMulti?: false | null;
}

interface IDefaultMultiDropDownProps<
  Type extends Record<string, any>,
  LabelKey extends keyof Type = 'label',
  ValueKey extends keyof Type = 'value',
> extends IBaseDropdownProps<Type, LabelKey, ValueKey> {
  input?: {
    value?: Array<Type[ValueKey]>;
    onChange?: (value: Array<Type[ValueKey]>) => void;
    onBlur?: () => void;
  };
  isMulti: true;
}

interface IValuedDropdownProps<Type extends Record<string, any>, ValueKey extends keyof Type>
  extends IDefaultDropdownProps<Type, 'label', ValueKey> {
  valueKey: ValueKey;
}
interface IValuedMultiDropdownProps<Type extends Record<string, any>, ValueKey extends keyof Type>
  extends IDefaultMultiDropDownProps<Type, 'label', ValueKey> {
  valueKey: ValueKey;
}
interface ILabelledDropdownProps<Type extends Record<string, any>, LabelKey extends keyof Type>
  extends IDefaultDropdownProps<Type, LabelKey, 'value'> {
  labelKey: LabelKey;
}
interface ILabelledMultiDropdownProps<Type extends Record<string, any>, LabelKey extends keyof Type>
  extends IDefaultMultiDropDownProps<Type, LabelKey, 'value'> {
  labelKey: LabelKey;
}
interface IValuedLabelledDropdownProps<
  Type extends Record<string, any>,
  LabelKey extends keyof Type,
  ValueKey extends keyof Type,
> extends IDefaultDropdownProps<Type, LabelKey, ValueKey> {
  labelKey: LabelKey;
  valueKey: ValueKey;
}
interface IValuedLabelledMultiDropdownProps<
  Type extends Record<string, any>,
  LabelKey extends keyof Type,
  ValueKey extends keyof Type,
> extends IDefaultMultiDropDownProps<Type, LabelKey, ValueKey> {
  labelKey: LabelKey;
  valueKey: ValueKey;
}

interface IOptionType {
  value: any;
  label: any;
}

const SearchableDropdownIndicator = () => {
  return <SearchIcon />;
};

const errorControlStyles = (provided: any, state: any) => {
  return {
    ...provided,
    '&:hover': {
      border: '1px solid red',
    },
    border: '1px solid red',
    boxShadow: state.isFocused ? '0 0 20px 5px rgba(255, 255, 255, 0.6)' : 'none',
    marginTop: '6px',
  };
};

const reactSelectStyles: StylesConfig = {
  container: (provided) => ({
    ...provided,
    marginBottom: '3px',
    marginTop: '6px',
  }),
  control: (provided, state) => {
    return {
      ...provided,
      '&:hover': {
        border: state.isFocused ? `1px solid ${cssStyleVars.lightSilver}` : cssStyleVars.borderStyle,
      },
      border: state.isFocused ? `1px solid ${cssStyleVars.lightSilver}` : cssStyleVars.borderStyle,
      boxShadow: state.isFocused ? '0 0 20px 5px rgba(255, 255, 255, 0.6)' : 'none',
      marginTop: '6px',
    };
  },
  indicatorSeparator: (provided) => ({
    ...provided,
    display: 'none',
  }),
  multiValue: (provided) => ({
    ...provided,
    backgroundColor: cssStyleVars.primary,
    borderRadius: '14px',
    color: 'white',
    padding: '0.25em',
  }),
  multiValueLabel: (provided) => ({
    ...provided,
    color: 'white',
    fontSize: cssStyleVars.fontSize6,
    fontWeight: 400,
    lineHeight: '1',
    padding: '0.25em',
  }),
  multiValueRemove: (provided) => ({
    ...provided,
    '&:hover': {
      backgroundColor: 'transparent',
      color: 'white',
      cursor: 'pointer',
    },
    color: cssStyleVars.white60,
    lineHeight: '1',
  }),
  placeholder: (provided) => ({
    ...provided,
    color: '#aaa',
    fontSize: cssStyleVars.fontSize45,
    fontWeight: 400,
  }),
  valueContainer: (provided) => ({
    ...provided,
    minHeight: '48px',
    padding: '0.5em',
  }),
};

const reactSelectTheme: ThemeConfig = (theme) => ({
  ...theme,
  borderRadius: 0,
  colors: {
    ...theme.colors,
    primary: cssStyleVars.primary,
    primary25: cssStyleVars.lightPrimary,
    primary50: cssStyleVars.lightPrimary, // when clicking on element
  },
});

const hasLabelKey = (props: any): props is {labelKey: any} => props.labelKey;
const hasValueKey = (props: any): props is {valueKey: any} => props.valueKey;
// overloading function to handle for case
// 1. data having both `value` and `label` fields (both single and multi dropdown)
// 2. data having only `value` field (both single and multi dropdown)
// 3. data having only `label` field (both single and multi dropdown)
// 4. data having neither `value` and `label` field (both single and multi dropdown)
export function Dropdown<Type extends IOptionType>(props: IDefaultDropdownProps<Type>): JSX.Element;
export function Dropdown<Type extends Pick<IOptionType, 'label'>, ValueKey extends keyof Type>(
  props: IValuedDropdownProps<Type, ValueKey>,
): JSX.Element;
export function Dropdown<Type extends Pick<IOptionType, 'value'>, LabelKey extends keyof Type>(
  props: ILabelledDropdownProps<Type, LabelKey>,
): JSX.Element;
export function Dropdown<Type extends Record<string, any>, LabelKey extends keyof Type, ValueKey extends keyof Type>(
  props: IValuedLabelledDropdownProps<Type, LabelKey, ValueKey>,
): JSX.Element;
// unified-signatures between single and multi dropdown will cause input.onChange value to have type any when isMulti is not defined
export function Dropdown<Type extends IOptionType>(props: IDefaultMultiDropDownProps<Type>): JSX.Element;
export function Dropdown<Type extends Pick<IOptionType, 'label'>, ValueKey extends keyof Type>(
  props: IValuedMultiDropdownProps<Type, ValueKey>,
): JSX.Element;
export function Dropdown<Type extends Pick<IOptionType, 'value'>, LabelKey extends keyof Type>(
  props: ILabelledMultiDropdownProps<Type, LabelKey>,
): JSX.Element;
export function Dropdown<Type extends Record<string, any>, LabelKey extends keyof Type, ValueKey extends keyof Type>(
  props: IValuedLabelledMultiDropdownProps<Type, LabelKey, ValueKey>,
): JSX.Element;

// concrete implementation :)
export function Dropdown<Type extends Record<string, any>, LabelKey extends keyof Type, ValueKey extends keyof Type>(
  props:
    | IValuedLabelledDropdownProps<Type, LabelKey, ValueKey>
    | IValuedLabelledMultiDropdownProps<Type, LabelKey, ValueKey>
    | IDefaultDropdownProps<Type>
    | IDefaultMultiDropDownProps<Type>
    | IValuedDropdownProps<Type, ValueKey>
    | IValuedMultiDropdownProps<Type, ValueKey>
    | ILabelledDropdownProps<Type, LabelKey>
    | ILabelledMultiDropdownProps<Type, LabelKey>,
) {
  // convert label and value from option, by default option has {label:string; value:string} shape
  // however using getOptionLabel and getOptionValue the shape can be different and those helpers functions
  // help to find label and value within options.
  const getSingleLabel = (key: LabelKey | 'label') => (option: Type) => option[key];
  const getSingleValue = (key: ValueKey | 'value') => (option: Type) => option[key];
  const getMultiValue = (key: ValueKey | 'value') => (options: readonly Type[]) => options.map(getSingleValue(key));
  const getLabelKey = (): LabelKey | 'label' => (hasLabelKey(props) ? props.labelKey : 'label');
  const getValueKey = (): ValueKey | 'value' => (hasValueKey(props) ? props.valueKey : 'value');

  const {id = '', innerProps = {}} = props;
  const reactSelectProps: Props<Type> = {
    classNamePrefix: props.classNamePrefix,
    getOptionLabel: getSingleLabel(getLabelKey()),
    getOptionValue: getSingleValue(getValueKey()),
    isClearable: props.isClearable || false,
    isDisabled: props.isDisabled || false,
    isMulti: props.isMulti || false,
    isSearchable: props.isSearchable || false,
    noOptionsMessage: props.noOptionsMessage || (() => 'No Options'),
    onBlur: props.input?.onBlur,
    // call onChange with option value instead of option object
    onChange: (option) => {
      if (props.isMulti) {
        const onChange = props.input?.onChange || noop;
        onChange(getMultiValue(getValueKey())(Array.isArray(option) ? option : []));
      } else if (!props.isMulti && option && !Array.isArray(option)) {
        const onChange = props.input?.onChange || noop;
        onChange(getSingleValue(getValueKey())(option));
      }
    },
    options: props.data,
    placeholder: props.placeholder,
    // map values (from input.value) to object entry (from data) as react-select expect an option
    value: props.isMulti
      ? props.data.filter((option) =>
          props.input?.value && Array.isArray(props.input.value)
            ? props.input.value.includes(getSingleValue(getValueKey())(option))
            : false,
        )
      : props.data.find((option) =>
          props.input ? props.input.value === getSingleValue(getValueKey())(option) : false,
        ),
  };
  const {
    error = '',
    // set to true by default so that dont need to handle if dont want to
    touched = true,
  } = props.meta || {};

  const errorStyle = {
    color: cssStyleVars.red,
    display: 'block',
    fontSize: cssStyleVars.fontSize55,
    fontWeight: 400,
  };

  return (
    <div id={id} className={`mv3`} {...innerProps}>
      {props.label && <label className="mb2 f5 fw6 black-60 pl1 db">{props.label}</label>}
      <Select<Type>
        components={props.isSearchable ? {DropdownIndicator: SearchableDropdownIndicator} : {}}
        id={`${id}-select`}
        {...reactSelectProps}
        styles={error && touched ? {...reactSelectStyles, ...{control: errorControlStyles}} : reactSelectStyles}
        theme={reactSelectTheme}
      />
      {error && touched && (
        <span id={`${id}-error`} style={errorStyle}>
          {error}
        </span>
      )}
    </div>
  );
}
