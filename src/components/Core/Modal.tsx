import React from 'react';
import {createPortal} from 'react-dom';

const modalRoot = document.getElementById('modal-root');
export interface IModalState {
  isMounted: boolean;
}
export class Modal extends React.Component<{}, IModalState> {
  private el: HTMLElement = document.createElement('div');

  constructor(props: {}) {
    super(props);
    this.state = {
      isMounted: false,
    };
  }

  public componentDidMount() {
    if (modalRoot) {
      modalRoot.appendChild(this.el);
      window.scroll(0, 0);
    }
    this.setState({isMounted: true});
  }

  public componentWillUnmount() {
    if (modalRoot) {
      modalRoot.removeChild(this.el);
    }
  }

  public render() {
    return this.state.isMounted
      ? createPortal(
          <>
            <div className="z-999 bg-black-40 w-100 h-100 fixed left-0 top-0" />
            <div className="absolute z-999 w-100 flex items-center justify-center pb2" style={{top: '160px'}}>
              {this.props.children}
            </div>
          </>,
          this.el,
        )
      : null;
  }
}
