import React from 'react';
import {Field} from 'react-final-form';

export const Condition = ({
  when,
  is,
  children,
}: {
  when: string;
  is: string | boolean | number;
  children: React.ReactElement;
}) => (
  <Field name={when} subscription={{value: true}}>
    {({input: {value}}) => (value === is ? children : null)}
  </Field>
);
