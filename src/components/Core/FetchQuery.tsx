import {isEqual} from 'lodash/fp';
import React, {useEffect, useRef, useState} from 'react';
import {fetchResponse} from '~/util/fetchResponse';

export interface IFetchQuery<T> {
  input: string; // use string instead of RequestInfo since useDeepCompareMemoize does not work with Request type
  init?: RequestInit;
  children: (result: {loading: boolean; errorMessage?: string; data?: T}) => React.ReactElement<any> | null;
}

export function FetchQuery<T>({input, init, children}: IFetchQuery<T>) {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>();

  const useDeepCompareMemoize = (value: [RequestInfo, RequestInit?]) => {
    const ref = useRef<[RequestInfo, RequestInit?]>();

    if (!isEqual(value)(ref.current)) {
      ref.current = value;
    }

    return ref.current;
  };

  useEffect(() => {
    const controller = new AbortController();
    const fetchData = async () => {
      setLoading(true);

      try {
        const response = await fetch(input, {...init, signal: controller.signal});
        setData(await fetchResponse(response));
      } catch (error) {
        if (error instanceof Error) {
          setErrorMessage(error.message);
        }
      }

      setLoading(false);
    };

    fetchData();
    return () => {
      controller.abort();
      setData(undefined);
      setErrorMessage(undefined);
    };
  }, useDeepCompareMemoize([input, init]));

  const result = () =>
    children({
      data,
      errorMessage,
      loading,
    });

  return result();
}
