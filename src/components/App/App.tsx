import React from 'react';
import {Redirect} from 'react-router-dom';
import {Footer} from './../Layouts/Footer';
import styles from './app.scss';
import {IUser} from '~/flux/account';

import {BannerContainer} from '~/components/Banner/BannerContainer';

interface IAppProps {
  user?: IUser;
}

export const App: React.FunctionComponent<IAppProps> = ({user}) => {
  return user ? (
    <Redirect to="/jobs" />
  ) : (
    <div className={`app w-100 flex flex-column justify-between ${styles.appContainer}`} id="app">
      <BannerContainer />

      <section className="w-100 mw9 center flex justify-between ph3 pv5">
        <div data-cy="employer-content" className="w-third ph5-l ph4-m ph3">
          <div className={styles.iconUspShortlist} />
          <p className="db fw6 f3-5 ma0 tc pt4 pb2 center primary">Find candidates for your jobs</p>
          <span className="db fw3 f4-5 tc center lh-copy black-70">
            After posting your jobs, you can review applicants and find suggested talents.
          </span>
        </div>

        <div data-cy="employer-content" className="w-third ph5-l ph4-m ph3">
          <div className={styles.iconUspScore} />
          <p className="db fw6 f3-5 ma0 tc pt4 pb2 center primary">Easily assess candidates</p>
          <span className="db fw3 f4-5 tc center lh-copy black-70">
            Our job matching partners scan profiles for relevant skills, work experience and education.
          </span>
        </div>

        <div data-cy="employer-content" className="w-third ph5-l ph4-m ph3">
          <div className={styles.iconUspTime} />
          <p className="db fw6 f3-5 ma0 tc pt4 pb2 center primary">Save time screening</p>
          <span className="db fw3 f4-5 tc center lh-copy black-70">
            Screen suitable candidates first when they’re sorted by how well they match your job.
          </span>
        </div>
      </section>

      <Footer />
    </div>
  );
};
