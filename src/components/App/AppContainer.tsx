import {connect} from 'react-redux';
import {App} from './App';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';

export const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state)});

export const AppContainer = connect(mapStateToProps)(App);
