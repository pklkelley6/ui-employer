import React from 'react';
import {useSelector} from 'react-redux';
import styles from '~/components/ApplicationsToolBar/ApplicationsToolBar.scss';
import {IAppState} from '~/flux/index';

export interface IApplicationToolBarProps {
  massSelectionSlot: JSX.Element;
  sortSlot: JSX.Element;
  onBoardingSlot: JSX.Element;
  filterSlot: JSX.Element;
}

export const ApplicationsToolBar: React.FunctionComponent<IApplicationToolBarProps> = ({
  sortSlot,
  onBoardingSlot,
  massSelectionSlot,
  filterSlot,
}) => {
  const topMatchFilterReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['174262411TopMatchInFilterModal'],
  );
  return (
    <div className="pb3">
      <div className="w-100 pa3 tr">{onBoardingSlot}</div>
      <div className={`flex bg-black-50 bb b--black-05 w-100 items-stretch ${styles.applicationToolbar}`}>
        <div className={`flex self-center items-center h-100`}>{massSelectionSlot}</div>
        {topMatchFilterReleaseToggle ? (
          <div className="flex">{filterSlot}</div>
        ) : (
          <div className={`flex self-center ${styles.flexEndRow}`} data-cy="applications-top-match">
            {filterSlot}
          </div>
        )}
        <div className={`flex ${styles.flexEndRow} ${styles.sortBox}`}>
          <p data-cy="sort-by" className="self-center f4-5 fw6 black-60 ma0 lh-solid tr pr3">
            Sort by:
          </p>
          <div className={`pr3 ${styles.sortDropdown}`} data-cy="applications-sort-criteria-dropdown">
            {sortSlot}
          </div>
        </div>
      </div>
    </div>
  );
};
