import React from 'react';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {GetSearchTalentsQuery, GetSearchTalentsTalents} from '~/graphql/__generated__/types';
import {getElapsedDays} from '~/util/date';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';

interface ITalentSearchResultsList {
  data?: GetSearchTalentsQuery;
  selectedTalent?: GetSearchTalentsTalents;
  onSelectTalent: (talent: GetSearchTalentsTalents, positionIndex: number) => void;
}
export const TalentSearchResultsList: React.FC<ITalentSearchResultsList> = ({data, selectedTalent, onSelectTalent}) => {
  return data?.searchTalents?.talents ? (
    <ul className="ma0 pl3 pr1 list" data-cy="talent-search-results-list">
      {data.searchTalents.talents.map((talent, index) => {
        const key = talent.id || index;
        const {id, lastLogin} = talent;
        const isSelected = selectedTalent ? selectedTalent.id === id : false;
        const lastLoginInfo = `Active ${getElapsedDays(lastLogin)}`;

        return (
          <CandidatesListItem
            key={key}
            selected={isSelected}
            isViewed={false}
            isBookmarked={false}
            candidate={talent}
            candidateType={CandidateType.SuggestedTalent}
            date={lastLoginInfo}
            onClick={() => onSelectTalent(talent, index)}
            onResumeClick={() => {}}
            isGetResumeVisible={false}
          />
        );
      })}
    </ul>
  ) : null;
};
