import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const NoSearchResults = (
  <EmptyState className="black-30" data-cy="no-search-results">
    <h1 className="f3">
      No talents found.
      <br />
      You may wish to check your spelling or expand your search term.
    </h1>
  </EmptyState>
);
