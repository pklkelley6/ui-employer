import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const EmptySearchResults = (
  <EmptyState className="black-30" data-testid="empty-search-results">
    <h1 className="f3">Search to view talents</h1>
  </EmptyState>
);
