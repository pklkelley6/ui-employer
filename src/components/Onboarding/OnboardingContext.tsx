import React from 'react';
import {connect} from 'react-redux';
import {getAccountUser} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {getFromStorage, saveToStorage} from '~/util/localStorage';

interface IOnboardingProviderProps {
  userId?: string;
  children: React.ReactNode;
}

interface IOnboardingProviderState {
  isApplicationViewed: boolean;
  isSuggestedTalentsViewed: boolean;
}

export const OnboardingContext = React.createContext({});

const defaultState: IOnboardingProviderState = {
  isApplicationViewed: false,
  isSuggestedTalentsViewed: false,
};

const ONBOARDING_PREFIX = 'onboarding';

export class OnboardingProvider extends React.Component<IOnboardingProviderProps, IOnboardingProviderState> {
  constructor(props: IOnboardingProviderProps) {
    super(props);
    const onboardingsStorage: IOnboardingProviderState = JSON.parse(this.getOnboardingsFromStorage() || '{}');
    this.state = {
      ...defaultState,
      ...onboardingsStorage,
    };
  }

  public getOnboardingsFromStorage = () => this.props.userId && getFromStorage(ONBOARDING_PREFIX, this.props.userId);
  public saveOnboardingsToStorage = () =>
    this.props.userId && saveToStorage(ONBOARDING_PREFIX, this.props.userId, this.state);

  public setApplicationViewed = () => {
    if (!this.state.isApplicationViewed) {
      this.setState({isApplicationViewed: true}, this.saveOnboardingsToStorage);
    }
  };

  public setSuggestedTalentsViewed = () => {
    if (!this.state.isSuggestedTalentsViewed) {
      this.setState({isSuggestedTalentsViewed: true}, this.saveOnboardingsToStorage);
    }
  };

  public render() {
    return (
      <OnboardingContext.Provider
        value={{
          ...this.state,
          setApplicationViewed: this.setApplicationViewed,
          setSuggestedTalentsViewed: this.setSuggestedTalentsViewed,
        }}
      >
        {this.props.children}
      </OnboardingContext.Provider>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  const user = getAccountUser(state);
  const userId = user?.userInfo.userId;
  return {userId};
};

export const OnboardingProviderWithUser = connect(mapStateToProps)(OnboardingProvider);
