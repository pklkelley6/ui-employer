import React from 'react';
import styles from './Onboarding.scss';
import {CandidatesOnboarding} from '~/components/Onboarding/CandidatesOnboarding';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';

export class ApplicationsOnboarding extends React.Component {
  public static contextType = OnboardingContext;

  public render() {
    return (
      <CandidatesOnboarding
        title={'How do I sort applicants by suitability for my job?'}
        isViewed={this.context.isApplicationViewed}
        setAsViewed={this.context.setApplicationViewed}
      >
        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.applicationStep1} />
          <p data-cy="applicant-onboarding-string-1" className="pt4 pb3 lh-copy f5">
            You may sort applicants by job match, which ranks applicants based on their skills, work experience and
            education against your job description.
            <br />
            <br />
            The job matching engine is powered by WCC.
          </p>
        </div>
        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.applicationStep2} />
          <p data-cy="applicant-onboarding-string-2" className="pt4 pb3 lh-copy f5">
            Applicants bearing the ‘Among top matches’ label have been assessed to be a close match to the job by the
            job matching engines.
          </p>
        </div>
      </CandidatesOnboarding>
    );
  }
}
