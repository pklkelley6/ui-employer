import React from 'react';
import styles from './Onboarding.scss';
import {CandidatesOnboarding} from '~/components/Onboarding/CandidatesOnboarding';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';

export class SuggestedTalentsOnboarding extends React.Component {
  public static contextType = OnboardingContext;

  public render() {
    return (
      <CandidatesOnboarding
        title={'How are suggested talents identified for my job?'}
        isViewed={this.context.isSuggestedTalentsViewed}
        setAsViewed={this.context.setSuggestedTalentsViewed}
      >
        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.suggestedTalentStep1} />
          <p data-cy="talent-onboarding-string-1" className="pt4 pb3 lh-copy f5">
            Suggested talents did not apply for the job. However, they have relevant skills, work experience and
            education to match your job posting.
          </p>
        </div>

        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.suggestedTalentStep2} />
          <p data-cy="talent-onboarding-string-2" className="pt4 pb3 lh-copy f5">
            These suggested talents are candidates who are open to being contacted for job opportunities. You may choose
            to download their resumes and contact them directly.
          </p>
        </div>

        <div className={`${styles.onboarderContentBox} pa3`}>
          <div className={styles.suggestedTalentStep3} />
          <p data-cy="talent-onboarding-string-3" className="pt4 pb3 lh-copy f5">
            The suggested talents list is dynamic and refreshed regularly. New talents are constantly added as our
            talent pool grows, while existing talents may be removed if their profile is no longer relevant.
            <br /> <br />
            You are encouraged to download the resumes of suggested talents you are keen to contact.
          </p>
        </div>
      </CandidatesOnboarding>
    );
  }
}
