import {isArray} from 'lodash/fp';
import React from 'react';
import Slider from 'react-slick';
import styles from './Onboarding.scss';
import {Modal} from '~/components/Core/Modal';

interface ICandidatesOnboardingProps {
  isViewed: boolean;
  setAsViewed: () => void;
  title: string;
  children: React.ReactNode;
}
interface ICandidatesOnboardingState {
  isVisible: boolean;
  currentPage: number;
}

export class CandidatesOnboarding extends React.Component<ICandidatesOnboardingProps, ICandidatesOnboardingState> {
  public slider: Slider | null = null;

  constructor(props: ICandidatesOnboardingProps) {
    super(props);
    this.state = {
      currentPage: 0,
      isVisible: false,
    };
  }

  public close = () => {
    this.setState({isVisible: false});
  };

  public open = () => {
    this.setState({isVisible: true, currentPage: 0});
  };

  public onPageChange = (index: number) => {
    this.setState({currentPage: index});
  };

  public nextPage = () => {
    if (this.slider) {
      this.slider.slickNext();
    }
  };

  public renderDot = (page: number) => {
    const style = `${styles.onboardingDot} ${page === this.state.currentPage ? styles.onboardingDotActive : ''}`;
    return <div className={style} />;
  };

  public componentDidMount() {
    this.setState(
      (prev) => ({
        isVisible: prev.isVisible || !this.props.isViewed,
      }),
      () => this.props.setAsViewed(),
    );
  }

  public render() {
    const sliderSettings = {
      afterChange: this.onPageChange,
      appendDots: (dots: React.ReactNode) => <ul>{dots}</ul>,
      arrows: false,
      customPaging: this.renderDot,
      dots: true,
      dotsClass: styles.onboardingDots,
      draggable: false,
      speed: 0,
    };
    const lastPage = isArray(this.props.children) ? this.props.children.length - 1 : 0;
    const isLastPage = this.state.currentPage === lastPage;
    return (
      <>
        <a
          href="#"
          data-cy="onboarding-link"
          className={`no-underline dim blue fw6 f4-5 dib pa2 pb2 ${styles.onboardingLink}`}
          onClick={this.open}
        >
          {this.props.title}
        </a>
        {this.state.isVisible && (
          <Modal>
            <div data-cy="onboarding-module" className="bg-white pa4 black-60">
              <div className="flex justify-between pb3">
                <h3 className="mv0">{this.props.title}</h3>
                <button
                  data-cy="onboarding-module-close"
                  className={`${styles.onboardingCloseButton}`}
                  onClick={this.close}
                />
              </div>
              <div className={`pv2 ph4 ${styles.onboardingBox}`}>
                <Slider ref={(instance: Slider | null) => (this.slider = instance)} {...sliderSettings}>
                  {this.props.children}
                </Slider>
              </div>
              <div className="flex justify-between">
                <button data-cy="skip-button" onClick={this.close} className="blue bn pointer bg-inherit">
                  skip
                </button>
                {isLastPage ? (
                  <button data-cy="gotit-button" onClick={this.close} className={styles.onboardingNextButton}>
                    Got it
                  </button>
                ) : (
                  <button data-cy="next-button" onClick={this.nextPage} className={styles.onboardingNextButton}>
                    Next
                  </button>
                )}
              </div>
            </div>
          </Modal>
        )}
      </>
    );
  }
}
