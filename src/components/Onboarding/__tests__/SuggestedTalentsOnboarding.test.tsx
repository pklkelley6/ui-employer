import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {SuggestedTalentsOnboarding} from '../SuggestedTalentsOnboarding';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {OnboardingProviderWithUser} from '~/components/Onboarding/OnboardingContext';

describe('SuggestedTalentsOnboarding', () => {
  it('should render modal when localStorage value for onboardings is null then close modal if close button is clicked', () => {
    const reduxState = {
      account: {
        data: uenUserMock,
      },
    };
    const store = configureStore()(reduxState);

    const localStorageKey = `onboarding-${uenUserMock.userInfo.userId}`;
    const localStorageValue = null;

    const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
    localStorageGetItemSpy.mockReturnValueOnce(localStorageValue);

    const localStorageSetItemSpy = jest.spyOn(Storage.prototype, 'setItem');
    const suggestedTalentsViewedState = {
      isApplicationViewed: false,
      isSuggestedTalentsViewed: true,
    };

    const wrapper = mount(
      <Provider store={store}>
        <OnboardingProviderWithUser>
          <SuggestedTalentsOnboarding />
        </OnboardingProviderWithUser>
      </Provider>,
    );

    expect(localStorageGetItemSpy).toHaveBeenCalledWith(localStorageKey);
    expect(localStorageSetItemSpy).toHaveBeenCalledWith(localStorageKey, JSON.stringify(suggestedTalentsViewedState));

    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(1);
    expect(wrapper.find(SuggestedTalentsOnboarding)).toMatchSnapshot();

    wrapper.find('[data-cy="onboarding-module-close"]').simulate('click');

    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(0);
    expect(wrapper.find(SuggestedTalentsOnboarding)).toMatchSnapshot();
  });

  it('should not render modal when isSuggestedTalentsViewed from localStorage is true', () => {
    const reduxState = {
      account: {
        data: uenUserMock,
      },
    };
    const store = configureStore()(reduxState);

    const localStorageKey = `onboarding-${uenUserMock.userInfo.userId}`;
    const localStorageValue = {
      isApplicationViewed: false,
      isSuggestedTalentsViewed: true,
    };

    const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
    localStorageGetItemSpy.mockReturnValueOnce(JSON.stringify(localStorageValue));

    const wrapper = mount(
      <Provider store={store}>
        <OnboardingProviderWithUser>
          <SuggestedTalentsOnboarding />
        </OnboardingProviderWithUser>
      </Provider>,
    );

    expect(localStorageGetItemSpy).toHaveBeenCalledWith(localStorageKey);
    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(0);
    expect(wrapper.find(SuggestedTalentsOnboarding)).toMatchSnapshot();
  });
});
