import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {ApplicationsOnboarding} from '../ApplicationsOnboarding';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {OnboardingProviderWithUser} from '~/components/Onboarding/OnboardingContext';

describe('ApplicationsOnboarding', () => {
  it('should render modal when localStorage value for onboardings is null then close modal if close button is clicked', () => {
    const reduxState = {
      account: {
        data: uenUserMock,
      },
    };
    const store = configureStore()(reduxState);

    const localStorageKey = `onboarding-${uenUserMock.userInfo.userId}`;
    const localStorageValue = null;

    const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
    localStorageGetItemSpy.mockReturnValueOnce(localStorageValue);

    const localStorageSetItemSpy = jest.spyOn(Storage.prototype, 'setItem');
    const applicationViewedState = {
      isApplicationViewed: true,
      isSuggestedTalentsViewed: false,
    };

    const wrapper = mount(
      <Provider store={store}>
        <OnboardingProviderWithUser>
          <ApplicationsOnboarding />
        </OnboardingProviderWithUser>
      </Provider>,
    );

    expect(localStorageGetItemSpy).toHaveBeenCalledWith(localStorageKey);
    expect(localStorageSetItemSpy).toHaveBeenCalledWith(localStorageKey, JSON.stringify(applicationViewedState));

    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(1);
    expect(wrapper.find(ApplicationsOnboarding)).toMatchSnapshot();

    wrapper.find('[data-cy="onboarding-module-close"]').simulate('click');

    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(0);
    expect(wrapper.find(ApplicationsOnboarding)).toMatchSnapshot();
  });

  it('should not render modal when isApplicationViewed from localStorage is true', () => {
    const reduxState = {
      account: {
        data: uenUserMock,
      },
    };
    const store = configureStore()(reduxState);

    const localStorageKey = `onboarding-${uenUserMock.userInfo.userId}`;
    const localStorageValue = {
      isApplicationViewed: true,
      isSuggestedTalentsViewed: false,
    };

    const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
    localStorageGetItemSpy.mockReturnValueOnce(JSON.stringify(localStorageValue));

    const wrapper = mount(
      <Provider store={store}>
        <OnboardingProviderWithUser>
          <ApplicationsOnboarding />
        </OnboardingProviderWithUser>
      </Provider>,
    );

    expect(localStorageGetItemSpy).toHaveBeenCalledWith(localStorageKey);
    expect(wrapper.find('[data-cy="onboarding-module"]')).toHaveLength(0);
    expect(wrapper.find(ApplicationsOnboarding)).toMatchSnapshot();
  });
});
