import {mount} from 'enzyme';
import React from 'react';
import {CandidateLabel, CandidateType} from '../CandidateLabel';

describe('CandidateLabel', () => {
  it('should render Applicant label with the right color correctly', () => {
    const candidateLabelProps = {
      label: CandidateType.Applicant,
    };
    const component = mount(<CandidateLabel {...candidateLabelProps} />);

    expect(component).toMatchSnapshot();
  });
  it('should render Suggested Talent label with the right color correctly', () => {
    const talentLabel = {
      label: CandidateType.SuggestedTalent,
    };
    const component = mount(<CandidateLabel {...talentLabel} />);

    expect(component).toMatchSnapshot();
  });
});
