import React from 'react';

export enum CandidateType {
  Applicant = 'Applicant',
  SuggestedTalent = 'Talent',
}

export interface ICandidateLabel {
  label: CandidateType;
}

export const CandidateLabel: React.FunctionComponent<ICandidateLabel> = ({label}) => {
  return (
    <div
      data-cy={`${label}-label`}
      className={`f8 ${
        label === CandidateType.Applicant ? 'bg-green' : 'bg-secondary'
      } br1 white fw6 tc mr2 lh-solid pv1 ph2 dib ttu tracked`}
      title={label}
    >
      {label}
    </div>
  );
};
