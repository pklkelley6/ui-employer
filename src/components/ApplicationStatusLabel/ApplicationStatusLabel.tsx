import {mcf} from '@mcf/constants';
import React from 'react';

export interface IApplicationStatus {
  value: mcf.JOB_APPLICATION_STATUS;
  label: string;
}
interface IApplicationStatusLabelProps {
  applicationStatus: IApplicationStatus;
}

const unsuccessful = 'dark-red bg-washed-red';
const hired = 'dark-green bg-washed-green';
const shortlisted = 'black-60 bg-black-10';

export const ApplicationStatusLabel: React.FunctionComponent<IApplicationStatusLabelProps> = ({applicationStatus}) => {
  const {value, label} = applicationStatus;

  const statusColorClassName =
    value === mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW
      ? shortlisted
      : value === mcf.JOB_APPLICATION_STATUS.SUCCESSFUL
      ? hired
      : unsuccessful;

  return (
    <div
      data-cy="application-status-label"
      className={`f8 ${statusColorClassName} fw6 tc lh-solid pv1 ph2 dib ml2 ttu tracked br-pill flex-shrink-0`}
      title={`${label}`}
    >
      {label}
    </div>
  );
};
