import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import React from 'react';
import {ApplicationStatusLabel} from '../ApplicationStatusLabel';

describe('ApplicationStatus', () => {
  it('should display Hired label when application status id is successful', () => {
    const hiredProp = {
      label: 'Hired',
      value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const component = mount(<ApplicationStatusLabel applicationStatus={hiredProp} />);
    expect(component.find('[data-cy="application-status-label"]')).toMatchSnapshot();
  });

  it('should display Unsuccessful label when application status id is unsuccessful', () => {
    const unsuccessfulProp = {
      label: 'Unsuccessful',
      value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
    };

    const component = mount(<ApplicationStatusLabel applicationStatus={unsuccessfulProp} />);
    expect(component.find('[data-cy="application-status-label"]')).toMatchSnapshot();
  });

  it('should display To Interview label when application status id is under review', () => {
    const shortlistedProp = {
      label: 'To Interview',
      value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };

    const component = mount(<ApplicationStatusLabel applicationStatus={shortlistedProp} />);
    expect(component.find('[data-cy="application-status-label"]')).toMatchSnapshot();
  });
});
