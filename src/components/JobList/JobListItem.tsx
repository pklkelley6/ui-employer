import {addDays, formatDistanceStrict, isBefore, parseISO} from 'date-fns';
import {isNil} from 'lodash/fp';
import React, {Fragment} from 'react';
import {Query} from 'react-apollo';
import {Link} from 'react-router-dom';
import styles from './JobList.scss';
import {UnviewedBadge} from '~/components/Badges/UnviewedBadge';
import {TotalCount} from '~/components/JobList/TotalCount';
import SuggestedTalentsCount from '~/components/SuggestedTalents/SuggestedTalentsCountCountainer';
import {GetApplicationsCountQuery, GetApplicationsCountQueryVariables} from '~/graphql/__generated__/types';
import {GET_APPLICATIONS_COUNT} from '~/graphql/applications/applications.query';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';
import {IJobPost, JobStatusCodes} from '~/services/employer/jobs.types';
import {addSingaporeTimeZone, formatISODate} from '~/util/date';
import {isMCFJob} from '~/util/isMCFJob';
import {formatJob} from '~/util/jobPosts';

export interface IJobListItemProps {
  job: IJobPost;
}

export const JobListItem: React.FunctionComponent<IJobListItemProps> = ({job}) => {
  const formatted = formatJob(job);
  // This uses a different date format
  const postingDateFormatted = formatISODate(job.metadata.newPostingDate);
  const expiryDateFormatted = formatISODate(job.metadata.expiryDate);

  /**
   * job.metadata.expiryDate is a simple date (yyyy-MM-dd) that's implied to be in singapore time
   * Job is expired at the end of the expiry day in singapore timezone
   */
  const expiryDate = addDays(addSingaporeTimeZone(job.metadata.expiryDate), 1);

  const isExpired = isBefore(expiryDate, Date.now());
  const distanceToExpiredDate = formatDistanceStrict(expiryDate, Date.now(), {
    addSuffix: true,
    roundingMethod: 'floor',
  });

  /**
   * deletedAt is either the date a job is manually closed or the date a job expires.
   * More info: https://www.pivotaltracker.com/story/show/167084844/comments/204841326
   */
  const closedDate = job.metadata.deletedAt;
  const isClosed = isExpired || job.status.id === JobStatusCodes.Closed;

  const distanceToClosedDate = closedDate
    ? formatDistanceStrict(parseISO(closedDate), Date.now(), {addSuffix: true, roundingMethod: 'floor'})
    : '';

  const queryVariables = {
    jobId: formatted.uuid,
  };

  return (
    <li className="flex items-stretch lh-copy mb3 pa1" data-cy="job-list-item">
      <div className="flex-auto-ie w-60 pa3 bg-white flex items-center" data-cy="job-list-item-info">
        <Link className="no-underline flex-auto-ie pa2" to={`${formatted.linkUrl}/${CandidatesTabs.Applications}`}>
          <div className="flex">
            {isMCFJob(formatted.id) ? null : <i className={`${styles.iconLock} mr2`} />}
            <span data-cy="job-list-item_job-id" className="db pt0 black-40 f6-5 lh-solid">
              {formatted.id}
            </span>
          </div>
          <h3
            data-cy="job-list-item_job-title"
            className="primary ma0 pt1 f4-5 fw6 lh-title pr2 truncate underline-hover"
            title={formatted.title}
          >
            {formatted.title}
          </h3>
        </Link>
        <div className={`db lh-solid f6 fw4 bg-white tr lh-copy ${styles.date}`}>
          <span data-cy="job-list-item_job-post-date" className="black-50 db">
            Posted on {postingDateFormatted}
          </span>
          <span data-cy="job-list-item_job-expiry-date" className="black-50 db" title={expiryDateFormatted}>
            {isClosed ? `Closed ${distanceToClosedDate}` : `Auto-closes ${distanceToExpiredDate}`}
          </span>
        </div>
      </div>

      <Link
        to={`${formatted.linkUrl}/${CandidatesTabs.Applications}`}
        className={`tc relative mid-gray bl b--light-gray bg-white flex flex-column justify-center no-underline ${styles.score}`}
        data-cy="job-list-item-applicant-count"
      >
        <Query<GetApplicationsCountQuery, GetApplicationsCountQueryVariables>
          query={GET_APPLICATIONS_COUNT}
          variables={queryVariables}
        >
          {({loading, error, data}) => {
            if (loading || error || isNil(data) || isNil(data.applicationsForJob)) {
              return <TotalCount title={'Applicants'} loading={loading} />;
            }

            const {unviewedTotal, total} = data.applicationsForJob;
            return (
              <Fragment>
                {unviewedTotal ? <UnviewedBadge count={unviewedTotal} /> : null}
                <TotalCount count={total} title={'Applicants'} loading={loading} />
              </Fragment>
            );
          }}
        </Query>
      </Link>
      <Link
        to={`${formatted.linkUrl}/${CandidatesTabs.SuggestedTalents}`}
        className={`tc dark-blue bg-white flex flex-column justify-center ml1 no-underline ${styles.score}`}
        data-cy="job-list-item-suggested-talent-count"
      >
        <SuggestedTalentsCount formattedJob={formatted} />
      </Link>
    </li>
  );
};
