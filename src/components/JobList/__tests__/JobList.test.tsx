import {MockedProvider} from '@apollo/react-testing';
import {addDays, format, parseISO} from 'date-fns/fp';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {flow} from 'lodash';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {JobList} from '~/components/JobList/JobList';
import {JobsSortCriteria} from '~/flux/jobPostings';
import {GET_APPLICATIONS_COUNT} from '~/graphql/applications/applications.query';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {IAppState} from '~/flux';

const mocks = [
  {
    request: {
      query: GET_APPLICATIONS_COUNT,
      variables: {
        jobPostId: '1',
      },
    },
    result: {
      data: {
        applicationsForJobPost: {total: 1, unviewedTotal: 1},
      },
    },
  },
];

describe('Jobs/JobList', () => {
  const jobsMock = [
    {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        expiryDate: flow([parseISO, addDays(21), format(`yyyy-MM-dd`)])(jobPostMock.metadata.newPostingDate || ''),
      },
    },
  ];

  const reduxState: Pick<IAppState, 'suggestedTalents'> & {
    jobPostings: Partial<IAppState['jobPostings']>;
    features: Partial<IAppState['features']>;
  } = {
    jobPostings: {
      closedJobsParams: {
        limit: 20,
        page: 0,
      },
      openJobsParams: {
        limit: 20,
        orderBy: JobsSortCriteria.EXPIRY_DATE,
        page: 0,
      },
    },
    suggestedTalents: {fromLastLogin: '2019-01-01', threshold: 1},
    features: {x0paSuggestedTalents: true},
  };
  const store = configureStore()(reduxState);
  it('should render correctly', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobList jobs={jobsMock} total={jobsMock.length} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(toJson(wrapper.find(JobList), cleanSnapshot())).toMatchSnapshot();
  });

  it('should render correctly with search result message', async () => {
    const jobListProps = {
      hasSearchResultMessage: true,
      jobs: jobsMock,
      total: jobsMock.length,
    };

    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobList {...jobListProps} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(toJson(wrapper.find(JobList), cleanSnapshot())).toMatchSnapshot();
  });
});
