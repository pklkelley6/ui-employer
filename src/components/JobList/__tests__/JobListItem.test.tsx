import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {JobListItem} from '~/components/JobList/JobListItem';
import {JobsSortCriteria} from '~/flux/jobPostings';
import {GET_APPLICATIONS_COUNT} from '~/graphql/applications/applications.query';
import {CandidatesTabs} from '~/pages/Candidates/Candidates.constants';
import {IJobPost} from '~/services/employer/jobs.types';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {formatJob} from '~/util/jobPosts';
import {IAppState} from '~/flux';

const currentDateMock = '2019-07-15T00:00:00.000+0800';
jest.spyOn(Date, 'now').mockImplementation(() => new Date(currentDateMock).valueOf());

const mocks = [
  {
    request: {
      query: GET_APPLICATIONS_COUNT,
      variables: {
        jobPostId: '1',
      },
    },
    result: {
      data: {
        applicationsForJobPost: {total: 1, unviewedTotal: 1},
      },
    },
  },
];

describe('Jobs/JobListItem', () => {
  const reduxState: Pick<IAppState, 'suggestedTalents'> & {
    jobPostings: Partial<IAppState['jobPostings']>;
    features: Partial<IAppState['features']>;
  } = {
    jobPostings: {
      closedJobsParams: {
        limit: 20,
        page: 0,
      },
      openJobsParams: {
        limit: 20,
        orderBy: JobsSortCriteria.EXPIRY_DATE,
        page: 0,
      },
    },
    suggestedTalents: {fromLastLogin: '2019-01-01', threshold: 1},
    features: {x0paSuggestedTalents: true},
  };
  const store = configureStore()(reduxState);
  it('has job title that link to applicants tab of the job url', async () => {
    const formatted = formatJob(jobPostMock);
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter>
            <JobListItem job={jobPostMock} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const rightJobCard = wrapper.find('[data-cy="job-list-item-info"]');
    expect(rightJobCard.find(`Link[to="${formatted.linkUrl}/${CandidatesTabs.Applications}"]`)).toHaveLength(1);
  });

  it('has applicants count that link to applicants tab of the job url', async () => {
    const formatted = formatJob(jobPostMock);
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter>
            <JobListItem job={jobPostMock} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const applicantCount = wrapper.find('[data-cy="job-list-item-applicant-count"]');
    expect(applicantCount.find(`Link[to="${formatted.linkUrl}/${CandidatesTabs.Applications}"]`)).toHaveLength(1);
  });

  it('has suggested talents count that link to suggested talents tab of the job url', async () => {
    const formatted = formatJob(jobPostMock);
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter>
            <JobListItem job={jobPostMock} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const suggestedTalentCount = wrapper.find('[data-cy="job-list-item-suggested-talent-count"]');
    expect(
      suggestedTalentCount.find(`Link[to="${formatted.linkUrl}/${CandidatesTabs.SuggestedTalents}"]`),
    ).toHaveLength(1);
  });

  it('renders correctly', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={jobPostMock} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(toJson(wrapper.find(JobListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it('should display distance to expiry date end of day if job is open', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        expiryDate: '2019-07-20',
      },
      status: {
        id: 102,
        jobStatus: 'Open',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const distanceToClosed = wrapper.find('[data-cy="job-list-item_job-expiry-date"]');
    expect(distanceToClosed.text()).toEqual('Auto-closes in 6 days');
  });

  it('should display distance to deletedAt date if job is closed', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        deletedAt: '2019-07-10T00:00:00.000Z', // 2019-07-10T08:00:00.000+0800
        expiryDate: '2019-07-20',
      },
      status: {
        id: 9,
        jobStatus: 'Closed',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const distanceToClosed = wrapper.find('[data-cy="job-list-item_job-expiry-date"]');
    expect(distanceToClosed.text()).toEqual('Closed 4 days ago');
  });

  it('should display closed without date if a job is closed and has no deletedAt date', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        deletedAt: undefined,
        expiryDate: '2019-07-20',
      },
      status: {
        id: 9,
        jobStatus: 'Closed',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const distanceToClosed = wrapper.find('[data-cy="job-list-item_job-expiry-date"]');
    expect(distanceToClosed.text()).toEqual('Closed ');
  });

  it('should display closed without date if a job is still open but is already expired', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        deletedAt: undefined,
        expiryDate: '2019-07-10',
      },
      status: {
        id: 102,
        jobStatus: 'Open',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    const distanceToClosed = wrapper.find('[data-cy="job-list-item_job-expiry-date"]');
    expect(distanceToClosed.text()).toEqual('Closed ');
  });
  it('should display lock icon if a job is posted as a non-mcf job', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        jobPostId: 'JOB-12345678',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find('i.iconLock')).toHaveLength(1);
  });
  it('should not display lock icon if a job is posted from mcf jobs', async () => {
    const inputJob: IJobPost = {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        jobPostId: 'MCF-12131415',
      },
    };
    const wrapper = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Provider store={store}>
          <MemoryRouter initialEntries={[{pathname: '/', key: 'testKey'}]}>
            <JobListItem job={inputJob} />
          </MemoryRouter>
        </Provider>
      </MockedProvider>,
    );
    await nextTick(wrapper);
    expect(wrapper.find('i.iconLock')).toHaveLength(0);
  });
});
