import {getOr} from 'lodash/fp';
import {connect} from 'react-redux';
import {JobsSortSelection} from './JobsSortSelection';
import {IAppState} from '~/flux';
import {onJobsSortingTypeClicked} from '~/flux/analytics';
import {CRITERIA_ORDER_BY, JOBS_STATUS, JobsSortCriteria} from '~/flux/jobPostings';
import {SortDirection} from '~/graphql/__generated__/types';

interface IFetchJobsParams {
  value: JobsSortCriteria;
  orderDirection?: SortDirection;
}

interface IJobsSortSelectionPassedProps {
  fetchJobs: (props: IFetchJobsParams) => void;
  jobsStatus: JOBS_STATUS;
}

const mapStateToProps = (state: IAppState) => {
  return {
    sortCriteriaClosedJobs: getOr('', 'jobPostings.closedJobsParams.orderBy', state),
    sortCriteriaOpenJobs: getOr('', 'jobPostings.openJobsParams.orderBy', state),
  };
};

const mapDispatchToProps = (dispatch: any, {fetchJobs, jobsStatus}: IJobsSortSelectionPassedProps) => {
  return {
    setSortingCriteria: (criteria: JobsSortCriteria): void => {
      dispatch(onJobsSortingTypeClicked(criteria));
      const orderDirection = CRITERIA_ORDER_BY[jobsStatus][criteria];
      fetchJobs({value: criteria, orderDirection});
    },
  };
};

export const JobsSortSelectionContainer = connect(mapStateToProps, mapDispatchToProps)(JobsSortSelection);
