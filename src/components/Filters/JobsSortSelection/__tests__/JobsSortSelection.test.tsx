import {shallow} from 'enzyme';
import React from 'react';
import {JobsSortSelection} from '../JobsSortSelection';
import {Dropdown} from '~/components/Core/Dropdown';
import {
  JOBS_SORT_CRITERIA_CLOSED_JOBS,
  JOBS_SORT_CRITERIA_OPEN_JOBS,
  JOBS_STATUS,
  JobsSortCriteria,
} from '~/flux/jobPostings';

describe('JobsSortSelection', () => {
  const props = {
    setSortingCriteria: jest.fn(),
    sortCriteriaClosedJobs: JobsSortCriteria.DELETED_AT,
    sortCriteriaOpenJobs: JobsSortCriteria.POSTED_DATE,
  };

  it('should display Dropdown with sort selection for open jobs', () => {
    const inputProps = {...props, jobsStatus: JOBS_STATUS.OPEN_JOBS};
    const wrapper = shallow(<JobsSortSelection {...inputProps} />);
    const dropdown = wrapper.find(Dropdown);

    expect(dropdown).toHaveLength(1);
    expect(dropdown.prop('input').value).toEqual(JobsSortCriteria.POSTED_DATE);
    expect(dropdown.prop('data')).toEqual(JOBS_SORT_CRITERIA_OPEN_JOBS);
  });

  it('should display Dropdown with sort selection for closed jobs', () => {
    const inputProps = {...props, jobsStatus: JOBS_STATUS.CLOSED_JOBS};
    const wrapper = shallow(<JobsSortSelection {...inputProps} />);
    const dropdown = wrapper.find(Dropdown);

    expect(dropdown).toHaveLength(1);
    expect(dropdown.prop('input').value).toEqual(JobsSortCriteria.DELETED_AT);
    expect(dropdown.prop('data')).toEqual(JOBS_SORT_CRITERIA_CLOSED_JOBS);
  });
});
