import React from 'react';
import {Dropdown} from '~/components/Core/Dropdown';
import {
  JOBS_SORT_CRITERIA_CLOSED_JOBS,
  JOBS_SORT_CRITERIA_OPEN_JOBS,
  JOBS_STATUS,
  JobsSortCriteria,
} from '~/flux/jobPostings';

export interface IJobsSortSelectionProps {
  sortCriteriaOpenJobs: JobsSortCriteria;
  sortCriteriaClosedJobs: JobsSortCriteria;
  setSortingCriteria: (criteria: JobsSortCriteria) => void;
  jobsStatus: string;
}

export const JobsSortSelection: React.FunctionComponent<IJobsSortSelectionProps> = ({
  sortCriteriaOpenJobs,
  sortCriteriaClosedJobs,
  setSortingCriteria,
  jobsStatus,
}) => {
  return (
    <div className="flex">
      <p data-cy="sort-by" className="flex flex-column justify-center f5-5 fw6 pl3 lh-solid tr pr3">
        Sort by:
      </p>
      <div data-cy="jobs-sort-selection-dropdown" className="w5">
        <Dropdown
          data={jobsStatus === JOBS_STATUS.CLOSED_JOBS ? JOBS_SORT_CRITERIA_CLOSED_JOBS : JOBS_SORT_CRITERIA_OPEN_JOBS}
          input={{
            onChange: (value) => setSortingCriteria(value),
            value: jobsStatus === JOBS_STATUS.CLOSED_JOBS ? sortCriteriaClosedJobs : sortCriteriaOpenJobs,
          }}
        />
      </div>
    </div>
  );
};
