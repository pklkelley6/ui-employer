import React from 'react';
import {mcf} from '@mcf/constants';
import {IJobScreeningQuestion} from '~/services/employer/jobs.types';

export interface IScreeningQuestionResponse extends IJobScreeningQuestion {
  answer: string;
}
interface IScreenQuestionResponseSelectionProps {
  input: {
    value: IScreeningQuestionResponse[];
    onChange: (value: IScreeningQuestionResponse[]) => void;
  };
}

export const ScreenQuestionResponseSelection = ({input}: IScreenQuestionResponseSelectionProps) => {
  const {value, onChange} = input;

  const yesResponse = mcf.getScreeningQuestionResponse(mcf.SCREEN_QUESTION_RESPONSE.YES);
  const noResponse = mcf.getScreeningQuestionResponse(mcf.SCREEN_QUESTION_RESPONSE.NO);

  const updateResponseSelection = (event: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const responses = [...value];
    responses?.splice(index, 1, {
      question: value[index]['question'],
      answer: event.target.value,
    });
    onChange(responses);
  };

  return (
    <div>
      <ul className="list pl0 ma0">
        {value.map((item, index) => {
          return (
            <li data-cy={`screening-question-answer-${index + 1}`} key={index}>
              <div className="b--black-20 f4-5 mt2 mb2 flex lh-copy">
                <div>{`Q${index + 1}.`}</div>
                <div className="pl3">
                  <div>{item.question}</div>
                  <div className="flex pv2">
                    <div className="mr4 mb2">
                      <input
                        type="radio"
                        data-cy={`question-${index + 1}-yes`}
                        id={`question-${index + 1}-yes`}
                        name={`question-${index + 1}`}
                        value={yesResponse.value}
                        checked={item.answer === yesResponse.value}
                        onChange={(event) => updateResponseSelection(event, index)}
                      />
                      <label className="ml2" htmlFor={`question-${index + 1}-yes`}>
                        {yesResponse.label}
                      </label>
                    </div>
                    <div className="mr2 mb2">
                      <input
                        type="radio"
                        data-cy={`question-${index + 1}-no`}
                        id={`question-${index + 1}-no`}
                        name={`question-${index + 1}`}
                        value={noResponse.value}
                        checked={item.answer === noResponse.value}
                        onChange={(event) => updateResponseSelection(event, index)}
                      />
                      <label className="ml2" htmlFor={`question-${index + 1}-no`}>
                        {noResponse.label}
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
