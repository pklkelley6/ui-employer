import {connect} from 'react-redux';
import {TopMatchCheckboxQuery} from './TopMatchCheckboxQuery';
import {getApplicationTopMatchTotal} from '~/flux/analytics';
import {ApplicationSortCriteria} from '~/flux/applications';
import {IAppState} from '~/flux/index';
import {pathToJobUuid} from '~/util/url';

export interface ITopMatchCheckboxContainerOwnProps {
  onTopMatchChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  sortCriteria: ApplicationSortCriteria;
  checked: boolean;
}
export interface ITopMatchCheckboxContainerStateProps {
  jobUuid: string;
}

export interface ITopMatchCheckboxContainerDispatchProps {
  getApplicationTopMatchTotal: typeof getApplicationTopMatchTotal;
}

export type ITopMatchCheckboxKeyProps = ITopMatchCheckboxContainerStateProps &
  ITopMatchCheckboxContainerDispatchProps &
  ITopMatchCheckboxContainerOwnProps;

const mapStateToProps = ({router}: IAppState) => {
  const jobUuid = pathToJobUuid(router.location.pathname).split('/')[0];
  return {
    jobUuid,
  };
};

export const TopMatchCheckboxDeprecatedContainer = connect(mapStateToProps, {getApplicationTopMatchTotal})(
  TopMatchCheckboxQuery,
);
