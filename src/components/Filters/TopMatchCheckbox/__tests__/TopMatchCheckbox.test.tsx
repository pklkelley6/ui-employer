import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {act} from 'react-dom/test-utils';
import {TopMatchCheckboxQuery} from '../TopMatchCheckboxQuery';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';
import {ApplicationSortCriteria} from '~/flux/applications';
import {GET_FILTERED_APPLICATIONS_COUNT} from '~/graphql/applications';
import {nextTick} from '~/testUtil/enzyme';

describe('TopMatchCheckboxQuery', () => {
  const props = {
    checked: false,
    getApplicationTopMatchTotal: jest.fn(),
    jobUuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
    onTopMatchChange: jest.fn(),
    sortCriteria: ApplicationSortCriteria.CREATED_ON,
  };

  const tutorialContextProps = {
    isApplicationViewed: true,
    setApplicationViewed: jest.fn(),
  };

  it('should disable checkbox when topmatch count is zero', async () => {
    const toolbarQueryMock = [
      {
        request: {
          query: GET_FILTERED_APPLICATIONS_COUNT,
          variables: {
            filterBy: [{field: 'scores.wcc', value: 0.6}],
            jobId: 'afab75dafd1c63f8dc3a8d909be8ef4f',
          },
        },
        result: {
          data: {
            applicationsForJob: {
              total: 0,
            },
          },
        },
      },
    ];

    const store = configureStore()();
    const wrapper = mount(
      <MockedProvider mocks={toolbarQueryMock} addTypename={false}>
        <Provider store={store}>
          <Router>
            <OnboardingContext.Provider value={tutorialContextProps}>
              <TopMatchCheckboxQuery {...props} />
            </OnboardingContext.Provider>
          </Router>
        </Provider>
      </MockedProvider>,
    );

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="applications-top-match"]').find('span');
    });

    expect(wrapper.text()).toEqual('Show "Among top matches" (0)');
    expect(wrapper.find('[data-cy="top-match-checkbox"]').prop('disabled')).toEqual(true);
  });
});
