import React from 'react';
import {Query} from 'react-apollo';
import {TopMatchCheckboxDeprecated} from './TopMatchCheckbox';
import {ITopMatchCheckboxKeyProps} from './TopMatchCheckboxContainer';
import {getTopMatchFilter} from '~/flux/applications';
import {
  GetFilteredApplicationsCountQuery,
  GetFilteredApplicationsCountQueryVariables,
} from '~/graphql/__generated__/types';
import {GET_FILTERED_APPLICATIONS_COUNT} from '~/graphql/applications/applications.query';

export const TopMatchCheckboxQuery: React.FunctionComponent<ITopMatchCheckboxKeyProps> = (props) => {
  const {sortCriteria, jobUuid} = props;
  return (
    <Query<GetFilteredApplicationsCountQuery, GetFilteredApplicationsCountQueryVariables>
      query={GET_FILTERED_APPLICATIONS_COUNT}
      variables={{
        filterBy: getTopMatchFilter(sortCriteria),
        jobId: jobUuid,
      }}
    >
      {({data}) => {
        const total = data?.applicationsForJob?.total;
        return <TopMatchCheckboxDeprecated {...props} total={total} />;
      }}
    </Query>
  );
};
