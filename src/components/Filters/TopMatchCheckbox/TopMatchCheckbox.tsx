import {isNil} from 'lodash/fp';
import React, {useEffect} from 'react';
import {ITopMatchCheckboxKeyProps} from './TopMatchCheckboxContainer';
import styles from '~/components/ApplicationsToolBar/ApplicationsToolBar.scss';

export type ITopMatchCheckboxComponentProps = ITopMatchCheckboxKeyProps & ITopMatchCheckboxComponentOwnProps;

export interface ITopMatchCheckboxComponentOwnProps {
  total?: number | null;
}

export const TopMatchCheckboxDeprecated = ({
  onTopMatchChange,
  checked,
  total,
  sortCriteria,
  jobUuid,
  getApplicationTopMatchTotal,
}: ITopMatchCheckboxComponentProps) => {
  useEffect(() => {
    if (!isNil(total)) {
      getApplicationTopMatchTotal(total, sortCriteria, jobUuid);
    }
  }, [sortCriteria, total]);
  return (
    <label>
      <input
        data-cy="top-match-checkbox"
        className={styles.inputCheckbox}
        type="checkbox"
        onChange={onTopMatchChange}
        checked={checked}
        disabled={!total}
      />
      {total ? (
        <span className="f5 pr3 black-80 pointer">{`Show "Among top matches" (${total})`}</span>
      ) : (
        <span
          className={`f5 pr3 black-80 cursor-not-allowed ${styles.checkboxInactive}`}
        >{`Show "Among top matches" (0)`}</span>
      )}
    </label>
  );
};
