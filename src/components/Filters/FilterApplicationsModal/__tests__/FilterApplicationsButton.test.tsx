import {mount} from 'enzyme';
import {noop} from 'lodash/fp';
import React from 'react';
import toJson from 'enzyme-to-json';
import {FilterApplicationsButton} from '../FilterApplicationsButton';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('FilterApplicationsButton', () => {
  it.each`
    numberOfActiveFilters | expectation
    ${0}                  | ${'Filter'}
    ${1}                  | ${'1 Filter applied'}
    ${10}                 | ${'10 Filters applied'}
  `(
    'should display the correct filter button text if numberOfActiveFilters is $numberOfActiveFilters',
    ({numberOfActiveFilters, expectation}) => {
      const component = mount(
        <FilterApplicationsButton numberOfActiveFilters={numberOfActiveFilters} onClick={noop} />,
      );

      expect(component.find('[data-cy="filter-modal-button"]').text()).toEqual(expectation);
      expect(toJson(component, cleanSnapshot())).toMatchSnapshot();
    },
  );

  it('triggers onClick function when clicked', () => {
    const onClickStub = jest.fn();
    const component = mount(<FilterApplicationsButton numberOfActiveFilters={0} onClick={onClickStub} />);

    component.find('[data-cy="filter-modal-button"]').simulate('click');

    expect(onClickStub).toBeCalledTimes(1);
  });
});
