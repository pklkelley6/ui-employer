import React from 'react';
import styles from '~/components/Filters/FilterApplicationsModal/FilterApplicationsModal.scss';

interface IFilterApplicationsButtonProps {
  numberOfActiveFilters?: number;
  onClick: () => void;
}

export const FilterApplicationsButton: React.FunctionComponent<IFilterApplicationsButtonProps> = ({
  onClick,
  numberOfActiveFilters,
}) =>
  numberOfActiveFilters ? (
    <a
      data-cy="filter-modal-button"
      onClick={onClick}
      className={`br1 flex no-underline pa2 ma3 white bg-primary primary ba b--primary pointer ${styles.filterModalButton}`}
    >
      <div className="flex w-100 justify-around items-center">
        <i className={`db mr2 w-20 relative white ${styles.iconFilterFunnelActive}`} />
        <span className="f6 fw6 tl w-80 db mr2 white">
          {numberOfActiveFilters} Filter{numberOfActiveFilters === 1 ? '' : 's'} applied
        </span>
      </div>
    </a>
  ) : (
    <a
      data-cy="filter-modal-button"
      onClick={onClick}
      className={`br1 flex no-underline pa2 ma3 white bg-white primary ba b--primary pointer ${styles.filterModalButton}`}
    >
      <div className="flex w-100 justify-around items-center">
        <i className={`db mr2 w-50 relative ${styles.iconFilterFunnel}`} />
        <span className="f6 fw6 tl w-50 db mr2">Filter</span>
      </div>
    </a>
  );
