import React, {useState} from 'react';
import {mcf} from '@mcf/constants';
import {Modal} from '~/components/Core/Modal';
import {FeatureFlag} from '~/components/Core/ToggleFlag';
import {ApplicantStatusCheckbox} from '~/components/Filters/ApplicantStatusCheckbox/ApplicantStatusCheckbox';
import {SwitchToggle} from '~/components/SwitchToggle/SwitchToggle';
import {
  ScreenQuestionResponseSelection,
  IScreeningQuestionResponse,
} from '~/components/Filters/ScreenQuestionResponse/ScreenQuestionResponseSelection';
import {IJobScreeningQuestion} from '~/services/employer/jobs.types';
import styles from '~/components/Filters/FilterApplicationsModal/FilterApplicationsModal.scss';
interface IFilterApplicationsModalProps {
  jobScreeningQuestions?: IJobScreeningQuestion[];
  onCancel: () => void;
  isTopMatchFiltered: boolean;
  applicantStatusFilters: number[];
  screeningQuestionsResponsesState: string[];
  onHandleFilterChange: (
    topMatchState: boolean,
    applicantStatusState: number[],
    screeningQuestionsResponsesState: string[],
  ) => void;
}

export const FilterApplicationsModal: React.FunctionComponent<IFilterApplicationsModalProps> = ({
  jobScreeningQuestions,
  onCancel,
  isTopMatchFiltered,
  applicantStatusFilters,
  screeningQuestionsResponsesState,
  onHandleFilterChange,
}) => {
  const [topMatchState, setTopMatchState] = useState<boolean>(isTopMatchFiltered);
  const [applicantStatusesChecked, setApplicantStatusesChecked] = useState<Array<number>>(applicantStatusFilters);
  const [screeningQuestionsToggledState, setScreeningQuestionsToggledState] = useState<boolean>(
    !!screeningQuestionsResponsesState.length,
  );

  const mapScreeningQuestionsToResponses =
    screeningQuestionsResponsesState &&
    jobScreeningQuestions?.map(({question}, index) => ({
      question,
      answer:
        screeningQuestionsResponsesState[index] ??
        mcf.getScreeningQuestionResponse(mcf.SCREEN_QUESTION_RESPONSE.YES).value,
    }));

  const [screeningQuestionsResponses, setScreeningQuestionsResponses] = useState<
    IScreeningQuestionResponse[] | undefined
  >(mapScreeningQuestionsToResponses);

  const handleApplicantStatusSelection = (isCheckboxChecked: boolean, applicantStatus: number) => {
    const state = isCheckboxChecked
      ? [...applicantStatusesChecked, applicantStatus]
      : applicantStatusesChecked.filter((status) => status !== applicantStatus);
    setApplicantStatusesChecked(state);
  };

  return (
    <Modal>
      <div data-cy="filter-modal" className={`flex flex-column ${styles.filterModalContainer}`}>
        <div className="bg-white black-70">
          <div className="pb3 pt4 pl4 pr4">
            <div className="flex justify-between items-center">
              <h2 className="f4 ma0 fw6">Filter</h2>
              <FeatureFlag
                name="applicationStatusInFilterModal"
                render={() => (
                  <div
                    data-cy="filter-modal-reset-all-filters"
                    className="underline blue pointer"
                    role="button"
                    tabIndex={0}
                    onClick={() => {
                      setTopMatchState(false);
                      setApplicantStatusesChecked([]);
                      setScreeningQuestionsToggledState(false);
                      setScreeningQuestionsResponses(
                        screeningQuestionsResponses?.map(({question}) => ({
                          question,
                          answer: mcf.getScreeningQuestionResponse(mcf.SCREEN_QUESTION_RESPONSE.YES).value,
                        })),
                      );
                    }}
                  >
                    Reset all filters
                  </div>
                )}
              />
            </div>
            <div className="b--black-20 bt mt3 mb4 w-100" />
            <h4 className="ma0 pb3 fw6 secondary">Top Matches</h4>
            <div className="flex justify-between">
              <div className="w-70 fw6">Show applicants that are 'Among top matches' for your job posting</div>
              <div data-cy="top-match-toggle">
                <SwitchToggle
                  id="top-match"
                  input={{
                    value: topMatchState,
                    onChange: (e: React.ChangeEvent<HTMLInputElement>) => setTopMatchState(e.target.checked),
                  }}
                />
              </div>
            </div>
          </div>
          <FeatureFlag
            name="applicationStatusInFilterModal"
            render={() => (
              <div className="pb3 pl4 pr4">
                <div className="b--black-20 bt mt3 mb3 w-100" />
                <h4 className="ma0 pb3 fw6 secondary">Application Status</h4>
                <div className="flex flex-column">
                  <div className="w-100 fw6">Show applicants with any of the selected status</div>
                  <ApplicantStatusCheckbox
                    onApplicantStatusCheckboxChange={(event, statusId) => {
                      handleApplicantStatusSelection(event, statusId);
                    }}
                    applicantStatusesChecked={applicantStatusesChecked}
                  />
                </div>
              </div>
            )}
          />
          {jobScreeningQuestions && jobScreeningQuestions.length > 0 && (
            <div className="pb3 pl4 pr4">
              <div className="b--black-20 bt mt3 mb3 w-100" />
              <h4 className="ma0 pb3 fw6 secondary">Screening Questions</h4>
              <div className="flex justify-between pb3">
                <div className="fw6">Show applicants based on their responses</div>
                <div data-cy="screening-question-response-toggle">
                  <SwitchToggle
                    id="screening-questions"
                    input={{
                      value: screeningQuestionsToggledState,
                      onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
                        setScreeningQuestionsToggledState(e.target.checked),
                    }}
                  />
                </div>
              </div>
              {screeningQuestionsToggledState ? (
                <ScreenQuestionResponseSelection
                  input={{
                    value: screeningQuestionsResponses!,
                    onChange: (val) => setScreeningQuestionsResponses(val),
                  }}
                />
              ) : null}
            </div>
          )}
        </div>
        <div className="flex pa2 tc bg-light-gray">
          <button
            data-cy="filter-modal-cancel"
            className="w-50 fw6 primary ma2 pa3 ba b--primary bg-white"
            onClick={() => {
              onCancel();
            }}
          >
            Cancel
          </button>
          <button
            data-cy="filter-modal-show-results"
            className="w-50 ma2 pa3 ba b--primary bg-primary white"
            onClick={() => {
              const updatedScreenQuestionResponses =
                (screeningQuestionsToggledState && screeningQuestionsResponses?.map(({answer}) => answer)) || [];
              onHandleFilterChange(topMatchState, applicantStatusesChecked, updatedScreenQuestionResponses);
            }}
          >
            Show Results
          </button>
        </div>
      </div>
    </Modal>
  );
};
