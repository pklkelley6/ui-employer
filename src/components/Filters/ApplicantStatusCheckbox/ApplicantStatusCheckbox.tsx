import React from 'react';
import {mcf} from '@mcf/constants';
import styles from '~/components/Filters/ApplicantStatusCheckbox/ApplicantStatusCheckbox.scss';

export const ApplicantStatusCheckboxOptions = [
  {
    label: 'Not updated', // application received for a job but not updated to a definite status
    value: mcf.JOB_APPLICATION_STATUS.RECEIVED,
  },
  {
    label: 'To Interview',
    value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
  },
  {
    label: 'Hired',
    value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
  },
  {
    label: 'Unsuccessful',
    value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
  },
];

export interface IApplicantStatusCheckboxProps {
  onApplicantStatusCheckboxChange: (event: boolean, status: number) => void;
  applicantStatusesChecked: Array<number>;
}

export const ApplicantStatusCheckbox = ({
  onApplicantStatusCheckboxChange,
  applicantStatusesChecked,
}: IApplicantStatusCheckboxProps) => {
  return (
    <ul className="pl0 pt2 pb2 flex justify-between" data-cy="applicant-statuses-checkbox-filter">
      {ApplicantStatusCheckboxOptions.map((statusOption, index) => {
        return (
          <li key={index} className="flex v-mid dib items-center">
            <input
              data-cy="applicant-status-selection-checkbox"
              id={`applicant-status-selection-checkbox-${statusOption.value}`}
              className={`${styles.applicantStatusCheckbox}`}
              onChange={(event) => onApplicantStatusCheckboxChange(event.target.checked, statusOption.value)}
              checked={applicantStatusesChecked.includes(statusOption.value)}
              type="checkbox"
            />
            <label className="pl2 f6 fw6" htmlFor={`applicant-status-selection-checkbox-${statusOption.value}`}>
              {statusOption.label}
            </label>
          </li>
        );
      })}
    </ul>
  );
};
