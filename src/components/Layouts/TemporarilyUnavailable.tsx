import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const temporaryUnavailableMsg = 'This information is temporarily unavailable. \n Please try again later.';
export const noLongerAvailableMsg = 'This information is no longer available for this job.';
interface ITemporarilyUnavailableProps {
  message?: string;
  link?: {
    text: string;
    action: () => void;
  };
}

export const TemporarilyUnavailable: React.FunctionComponent<ITemporarilyUnavailableProps> = ({
  message = temporaryUnavailableMsg,
  link,
}) => (
  <EmptyState className="black-50">
    <h1 className="f3 pre overflow-auto" data-cy="error-message">
      {message}
    </h1>
    {link && (
      <a className="f3 blue" href="#" onClick={link.action} data-cy="previous-page">
        {link.text}
      </a>
    )}
  </EmptyState>
);
