import React from 'react';
import {Link} from 'react-router-dom';
import styles from './Footer.scss';
import {config} from '~/config';

export const Footer: React.FunctionComponent = () => {
  const year = new Date().getFullYear();
  return (
    <footer className={`bg-white bt b--black-10 ${styles.footer}`}>
      <section className="flex pa3 w-100 center justify-between items-center">
        <div className="w-50 dib v-mid flex items-start">
          <div className="pt2">
            <p className="fw3 f6 gray pb2 w-100 mv0">A collaboration between</p>
            <i className={`dib mv3 ml3 mr2 v-mid ${styles.wsgLogo}`} />
            <i className={`dib mv3 ml3 v-mid ${styles.govtechLogo}`} />
          </div>

          <div className="pt2 ml5">
            <p className="fw3 f6 gray pb2 mv0 db">Powered by</p>
            <i title="X0PA" className={`dib mv3 v-mid ${styles.xopaLogo}`} />
          </div>
        </div>

        <div className="w-50 pl4 pr0">
          <div className="pb1">
            <a
              href="https://www.mycareersfuture.gov.sg/docs/mycareersfuture_sg_user_faqs.pdf"
              title="FAQ"
              target="_blank"
              className="pr3 blue dib lh-copy"
            >
              FAQ
            </a>
            <Link to="/terms-of-use" title="Terms of Use" className="pr3 blue dib lh-copy">
              Terms of Use
            </Link>
            <Link to="/survey" title="Survey" className="pr3 blue dib lh-copy">
              Survey
            </Link>
            <a href="https://portal.ssg-wsg.gov.sg/" title="Feedback" target="_blank" className="pr3 blue dib lh-copy">
              Feedback
            </a>
            <a
              href="https://www.tal.sg/tafep/getting-started/fair/employers-pledge"
              title="Pledge for Fair Employment"
              target="_blank"
              className="pr3 blue dib lh-copy"
            >
              Pledge for Fair Employment
            </a>
            <a
              href="https://www.mom.gov.sg/covid-19/second-job-arrangements"
              title="Second Job Arrangements"
              target="_blank"
              className="pr3 blue dib lh-copy"
            >
              Second Job Arrangements
            </a>
            <a
              href="https://programmes.mycareersfuture.gov.sg/CareerTrialEmployers/ProgrammeDetails.aspx"
              title="Career Trial"
              target="_blank"
              className="pr3 blue dib lh-copy"
            >
              Career Trial
            </a>
          </div>
          <div className="tl" />
          <div>
            <small className="mt1 f6 black-60 db tl">
              <span className="dib pr2">© {year} Government of Singapore</span>
            </small>
          </div>
          <small id="version" title="Epic.Major.Minor.Build" className="mt1 f6 black-30 db">
            {config.meta.version}
          </small>
          <a
            href="https://www.tech.gov.sg/report_vulnerability"
            target="_blank"
            className="blue pt3 f6 db underline"
            title="Report Vulnerability"
          >
            Report Vulnerability
          </a>
        </div>
      </section>
    </footer>
  );
};
