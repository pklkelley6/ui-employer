import React from 'react';

export interface IRowProps {
  className: string;
  children?: JSX.Element;
}

export const Row: React.FunctionComponent<IRowProps> = ({className, children}) => {
  return <div className={`layouts row ${className}`}>{children}</div>;
};
