import {connect} from 'react-redux';
import {ContactSuggestedTalent} from './ContactSuggestedTalent';
import {onSuggestedTalentCandidateInviteToApplyClicked} from '~/flux/analytics';

export interface IContactSuggestedTalentContainerDispatchProps {
  onSuggestedTalentCandidateInviteToApplyClicked: typeof onSuggestedTalentCandidateInviteToApplyClicked;
}

export const ContactSuggestedTalentContainer = connect<{}, IContactSuggestedTalentContainerDispatchProps>(null, {
  onSuggestedTalentCandidateInviteToApplyClicked,
})(ContactSuggestedTalent);
