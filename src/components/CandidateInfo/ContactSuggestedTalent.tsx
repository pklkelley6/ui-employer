import React, {useState} from 'react';
import {useAsync, useAsyncFn} from 'react-use';
import Tooltip from 'rc-tooltip';
import {growlNotification, GrowlType} from '../GrowlNotification/GrowlNotification';
import {IContactSuggestedTalentContainerDispatchProps} from './ContactSuggestedTalentContainer';
import styles from '~/components/InviteTalent/InviteTalentButton.scss';
import {postEmailInvitation} from '~/services/suggestedTalents/postEmailInvitation';
import {getEmailInvitations} from '~/services/suggestedTalents/getEmailInvitations';
interface IContactSuggestedTalentProps extends IContactSuggestedTalentContainerDispatchProps {
  jobUuid: string;
  individualId: string;
}

export const ContactSuggestedTalent: React.FunctionComponent<IContactSuggestedTalentProps> = ({
  jobUuid,
  individualId,
  onSuggestedTalentCandidateInviteToApplyClicked,
}) => {
  const TALENT_APPLY_INVITATION_DESC =
    'Click to send this talent an email with the job description and invitation to apply for the job.';

  const [isInvitationSent, setIsInvitationSent] = useState(false);

  const [_, fetch] = useAsyncFn(async () => {
    try {
      const result = await postEmailInvitation(jobUuid, individualId);

      if (result) {
        setIsInvitationSent(true);
        onSuggestedTalentCandidateInviteToApplyClicked(individualId, jobUuid);
      }
    } catch (error) {
      growlNotification('Temporarily unable to send invitation to apply. Please try again.', GrowlType.ERROR, {
        toastId: 'contactSuggestedTalentError',
        autoClose: 5000,
      });
    }
  });

  const getEmailInvitationsState = useAsync(async () => {
    const result = await getEmailInvitations(jobUuid);
    if (result.find((data) => data.individualId == individualId)) {
      setIsInvitationSent(true);
    }
  }, []);

  const InvitationSent = (
    <div className="db">
      <button
        data-cy="invite-talent-apply-button-sent"
        className={`no-underline dib f3 mr3 mt3 pa3 black-50 bg-light-gray mr3
         ${styles.inviteTalentDisabled} cursor-not-allowed`}
        tabIndex={0}
        disabled={true}
      >
        <div className="flex justify-center items-center">
          <i className={`db relative mr2 ${styles.iconInviteTalentDisabled}`} />
          <span className="f5 fw6 db mr2">Invitation sent</span>
        </div>
      </button>
    </div>
  );

  const InviteToApplyButton = (
    <div className="db">
      <Tooltip
        id="invite-talent-apply-tooltip"
        placement="top"
        trigger={['hover', 'focus']}
        align={{offset: [8, 2]}}
        overlayClassName={`z-5 bg-mid-gray white ${styles.customTooltip}`}
        destroyTooltipOnHide={true}
        overlay={<span className={`db pa3 f7 lh-copy`}>{TALENT_APPLY_INVITATION_DESC}</span>}
      >
        <button
          data-cy="invite-talent-apply-button"
          className={`no-underline dib f3 mt3 purple pa3 bg-white pointer mr3 ${styles.inviteTalentPurpleBackground}`}
          tabIndex={0}
          onClick={fetch}
        >
          <div className="flex justify-center items-center">
            <i className={`db relative mr2 ${styles.iconWhiteInviteTalent}`} />
            <span className={`f5 fw6 db mr2 white`}>Invite to Apply</span>
          </div>
        </button>
      </Tooltip>
    </div>
  );

  return !getEmailInvitationsState.loading && isInvitationSent ? InvitationSent : InviteToApplyButton;
};

export default ContactSuggestedTalent;
