import {mcf} from '@mcf/constants';
import React, {useState, useContext, useLayoutEffect} from 'react';
import {Mutation, MutationFunction, MutationResult} from 'react-apollo';
import styles from './CandidateInfo.scss';
import {Dropdown} from '~/components/Core/Dropdown';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {Maybe} from '~/graphql/__generated__/types';
import {SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import {RefetchApplicationsWithStatusCountByJobContext} from '~/pages/Candidates/RefetchApplicationsByJobAndStatusContext';

export const updateApplicationStatusOptions = [
  {
    label: 'To Interview',
    value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
  },
  {
    label: 'Unsuccessful',
    value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
  },
  {
    label: 'Hired',
    value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
  },
];

interface IApplicationStatusProps {
  statusId?: number;
  isShortlisted?: Maybe<boolean>;
  applicationId: string;
  onLoading?: () => void;
  onCompleted?: () => void;
  onError?: () => void;
}

export const ApplicationStatus: React.FunctionComponent<IApplicationStatusProps> = ({
  statusId,
  isShortlisted,
  applicationId,
  onLoading,
  onCompleted,
  onError,
}) => {
  /*
    If the status id is UNDER_REVIEW and isShortlisted is false, there would be no selection showed in the dropdown. This scenario
    would happen if the user downloaded the resume but has not yet selected to shortlist it from this dropdown.
  */
  const isStatusUnderReview = statusId === mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW;
  const initialStatus = !isShortlisted && isStatusUnderReview ? undefined : statusId;

  useLayoutEffect(() => {
    setSelectedStatus(initialStatus);
  }, [statusId]);

  const [selectedStatus, setSelectedStatus] = useState<mcf.JOB_APPLICATION_STATUS | undefined>();
  const [previousStatus, setPreviousStatus] = useState<mcf.JOB_APPLICATION_STATUS | undefined>();

  const onMutationError = () => {
    growlNotification('Temporarily unable to update application status. Please try again.', GrowlType.ERROR, {
      toastId: 'updateStatusError',
    });
    setSelectedStatus(previousStatus);

    if (onError) {
      onError();
    }
  };

  const refetch = useContext<(() => Promise<any>) | undefined>(RefetchApplicationsWithStatusCountByJobContext);

  return (
    <div data-cy="applicant-update-status" className={styles.applicantStatus}>
      <Mutation
        mutation={SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE}
        onError={onMutationError}
        onCompleted={async () => {
          if (refetch) {
            await refetch();
          }

          if (onCompleted) {
            onCompleted();
          }
        }}
      >
        {(setApplicationShortlistAndStatusUpdate: MutationFunction, {loading}: MutationResult) => {
          if (loading) {
            if (onLoading) {
              onLoading();
            }
          }
          return (
            <Dropdown
              label={'Application Status'}
              id="update-application-status-dropdown"
              key={selectedStatus}
              placeholder="Not updated"
              data={updateApplicationStatusOptions}
              input={{
                onChange: (value) => {
                  const isValueUnderReview = value === mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW;
                  setApplicationShortlistAndStatusUpdate({
                    variables: {
                      applicationId,
                      isShortlisted: isValueUnderReview,
                      statusId: value,
                    },
                  });
                  setPreviousStatus(selectedStatus);
                  setSelectedStatus(value);
                },
                value: selectedStatus,
              }}
            />
          );
        }}
      </Mutation>
    </div>
  );
};
