import React from 'react';
import {FeatureFlag, ReleaseFlag} from '../Core/ToggleFlag';
import {SkillPill} from '../JobPosting/Skills/SkillPill';
import {ISkill} from '~/graphql/candidates';
import styles from '~/components/CandidateInfo/CandidateInfo.scss';

export interface ISkillsProps {
  matchedSkills: ISkill[];
  nonMatchedSkills?: ISkill[];
  headerMessage?: string;
  limit?: number;
}

export const Skills: React.FunctionComponent<ISkillsProps> = ({
  matchedSkills,
  nonMatchedSkills,
  headerMessage,
  limit,
}) => {
  const newRender = (
    <>
      <h4 data-cy="overview-skills" className="f5-5 fw7 ma0 mb3 secondary">
        {headerMessage || 'Skills'}
      </h4>
      {matchedSkills.length > 0 ? (
        <ul className="flex list pl0 ma0 flex-wrap">
          {matchedSkills.map((skill) => <SkillPill label={skill.skill} selected={true} />).slice(0, limit)}
          {nonMatchedSkills?.map((skill) => <SkillPill label={skill.skill} selected={false} />).slice(0, limit)}
        </ul>
      ) : (
        <div className="f5 black-50 lh-copy tc">This candidate has not added any skills matched to the job.</div>
      )}
    </>
  );

  const deprecatedRender = matchedSkills.length ? (
    <ul className="flex list pl0 ma0 flex-wrap">
      {matchedSkills
        .map((skill) => (
          <li data-cy="skill-icon" key={skill.uuid} className="flex f6-5 br-pill bg-primary white mr2 pv1 pl2 pr3 mb1">
            <i className={`${styles.skillIcon}`} />
            <span>{skill.skill}</span>
          </li>
        ))
        .slice(0, limit)}
    </ul>
  ) : (
    <div className="f5 black-50 lh-copy tc">
      This candidate has not added any skills. <br />
      You may want to view the resume instead.
    </div>
  );

  return (
    <ReleaseFlag
      name="179899438enhancedSkillPills"
      render={() => (
        <FeatureFlag name="enhancedSkillPills" render={() => newRender} fallback={() => deprecatedRender} />
      )}
      fallback={() => deprecatedRender}
    />
  );
};

export default Skills;
