import {noop} from 'lodash/fp';
import {throttle} from 'lodash';
import React, {ReactElement, useCallback} from 'react';
import styles from '~/components/CandidateInfo/CandidateInfo.scss';
import {TopMatcherBadge} from '~/components/ScoreBadge/TopMatcherBadge';
import {CandidateLabel, CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {ICandidate} from '~/graphql/candidates/';
import {BookmarkToggle} from '~/components/BookmarkToggle/BookmarkToggle';
import {InvitedTalentLabel} from '~/components/InvitedTalentLabel/InvitedTalentLabel';
import {GetResumeButton} from '~/components/GetResumeButton/GetResumeButton';
export interface ICandidateInfoPanelProps {
  candidate: ICandidate;
  position: string;
  showTopMatch?: boolean;
  isBookmarked: boolean;
  isInvitedTalent: boolean;
  formattedDate: string;
  onResumeClick?: () => void;
  candidateType: CandidateType;
  statusSelector?: ReactElement;
  onCandidateBookmarkClick: (checked: boolean) => void;
  contactSuggestedTalent?: ReactElement;
  isBookmarkVisible?: boolean;
  isResumeButtonVisible?: boolean;
}

export const CandidateInfoPanel: React.FunctionComponent<ICandidateInfoPanelProps> = ({
  candidate,
  position,
  formattedDate,
  onResumeClick = noop,
  showTopMatch,
  isBookmarked,
  statusSelector,
  candidateType,
  onCandidateBookmarkClick,
  contactSuggestedTalent,
  isInvitedTalent,
  isBookmarkVisible = true,
  isResumeButtonVisible = true,
}) => {
  const {resume} = candidate;
  const throttledOnBookmarkClick = useCallback(
    throttle((checked: boolean) => onCandidateBookmarkClick(checked), 500, {trailing: false}),
    [],
  );

  return (
    <section data-cy="candidate-right-panel-1" className="pa4 flex-shrink-0">
      <div className="flex justify-between">
        <div data-cy="candidate-right-panel-1-applicant-details">
          <h1 data-cy="candidate-right-panel-1-applicant-name" className="f3 fw6 mt0 mb1 ws-normal primary">
            {candidate.name}
          </h1>
          <h2 data-cy="candidate-right-panel-1-position" className="f5 fw4 ma0 mb2 black-70 lh-title">
            {position}
          </h2>
          <div className="flex mt2 flex-wrap items-center">
            <CandidateLabel label={candidateType} />
            {isInvitedTalent ? <InvitedTalentLabel /> : undefined}
          </div>
          <div className="mt2">
            <span data-cy="candidate-right-panel-1-applied-date" className="f6 black-40 lh-title pv1">
              {formattedDate}
            </span>
          </div>
        </div>
        <div className={`pl2 ${styles.candidateInfoPanelRight}`}>
          {isBookmarkVisible ? (
            <BookmarkToggle
              input={{
                value: isBookmarked,
                onChange: (event) => throttledOnBookmarkClick(event.target.checked),
              }}
            />
          ) : null}

          {statusSelector}
        </div>
      </div>
      <div className="flex items-center mt3 w-100 justify-between">
        <div className="flex-column">
          {candidate.email && (
            <a
              data-cy="candidate-right-panel-1-email"
              href={`mailto: ${candidate.email}`}
              className={`blue no-underline underline-hover ws-normal pr3 ${styles.email}`}
            >
              {candidate.email}
            </a>
          )}
          {candidate.mobileNumber && (
            <a
              data-cy="candidate-right-panel-1-mobile"
              href={`tel: ${candidate.mobileNumber}`}
              className={`blue no-underline underline-hover lh-copy ma0 mt1 ${styles.mobileNumber}`}
            >
              {candidate.mobileNumber}
            </a>
          )}
          <div className="flex items-center">
            {contactSuggestedTalent}
            {isResumeButtonVisible && resume ? <GetResumeButton resume={resume} onResumeClick={onResumeClick} /> : null}
          </div>
        </div>
        <div className={styles.topMatchLabel}>{showTopMatch ? <TopMatcherBadge /> : null}</div>
      </div>
    </section>
  );
};
