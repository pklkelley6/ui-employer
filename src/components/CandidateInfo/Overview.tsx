import React, {ReactElement} from 'react';
import {FeatureFlag, ReleaseFlag} from '../Core/ToggleFlag';
import {EducationHistory, Skills, WorkExperiences} from '~/components/CandidateInfo';
import {IEducation, ISkill, IWorkExperience} from '~/graphql/candidates';

const SKILLS_LIMIT = 20;
const DEPRECATED_SKILLS_LIMIT = 10;
const WORK_EXPERIENCES_LIMIT = 3;
const EDUCATION_HISTORY_LIMIT = 3;

export interface IOverviewProps {
  educationList: IEducation[];
  matchedSkills: ISkill[];
  nonMatchedSkills?: ISkill[];
  workExperiences: IWorkExperience[];
  onViewSkills: () => void;
  applicationRejectionReason?: ReactElement;
}

const ViewSkillsButton = ({onViewSkills}: {onViewSkills: () => void}) => (
  <div className="mt2 tr">
    <a
      href="#"
      onClick={(event) => {
        event.preventDefault();
        onViewSkills();
      }}
    >
      View all skills
    </a>
  </div>
);

export const Overview: React.FunctionComponent<IOverviewProps> = ({
  educationList,
  onViewSkills,
  matchedSkills,
  nonMatchedSkills,
  workExperiences,
  applicationRejectionReason,
}) => (
  <>
    {applicationRejectionReason}
    {matchedSkills.length || workExperiences.length || educationList.length ? (
      <div>
        <ReleaseFlag
          name="179899438enhancedSkillPills"
          render={() => (
            <FeatureFlag
              name="enhancedSkillPills"
              render={() => (
                <section className="pb4">
                  <Skills
                    matchedSkills={matchedSkills}
                    nonMatchedSkills={nonMatchedSkills}
                    headerMessage={
                      matchedSkills && nonMatchedSkills
                        ? `Skills Matched To Job (${matchedSkills.length}/${
                            matchedSkills.length + nonMatchedSkills.length
                          })`
                        : 'Skills'
                    }
                    limit={SKILLS_LIMIT}
                  />
                  <ViewSkillsButton onViewSkills={onViewSkills} />
                </section>
              )}
              fallback={() => (
                <>
                  {!!matchedSkills.length && (
                    <section className="pb4">
                      <h4 data-cy="overview-skills" className="f5-5 fw7 ma0 mb3 secondary">
                        Skills
                      </h4>
                      <Skills matchedSkills={matchedSkills} limit={DEPRECATED_SKILLS_LIMIT} />
                      {matchedSkills.length > DEPRECATED_SKILLS_LIMIT && (
                        <ViewSkillsButton onViewSkills={onViewSkills} />
                      )}
                    </section>
                  )}
                </>
              )}
            />
          )}
          fallback={() => (
            <>
              {!!matchedSkills.length && (
                <section className="pb4">
                  <h4 data-cy="overview-skills" className="f5-5 fw7 ma0 mb3 secondary">
                    Skills
                  </h4>
                  <Skills matchedSkills={matchedSkills} limit={DEPRECATED_SKILLS_LIMIT} />
                  {matchedSkills.length > DEPRECATED_SKILLS_LIMIT && <ViewSkillsButton onViewSkills={onViewSkills} />}
                </section>
              )}
            </>
          )}
        />

        {workExperiences.length > 0 && (
          <section className="pb4">
            <h4 data-cy="overview-work-experience" className="f5-5 fw7 ma0 mb3 secondary">
              Work Experience
            </h4>
            <WorkExperiences workExperiences={workExperiences} limit={WORK_EXPERIENCES_LIMIT} />
          </section>
        )}

        {educationList.length > 0 && (
          <section className="pb4">
            <h4 data-cy="overview-education-history" className="f5-5 fw7 ma0 mb3 secondary">
              Education History
            </h4>
            <EducationHistory educationList={educationList} limit={EDUCATION_HISTORY_LIMIT} />
          </section>
        )}
      </div>
    ) : (
      <div data-cy="overview-no-info" className="f5 black-50 lh-copy tc">
        This candidate has not added any information. <br />
        You may want to view the resume instead.{' '}
      </div>
    )}
  </>
);
