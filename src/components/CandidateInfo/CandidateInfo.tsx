import {sortBy} from 'lodash/fp';
import React, {ReactElement, useState} from 'react';
import {useSelector} from 'react-redux';
import {FeatureFlag, ReleaseFlag} from '../Core/ToggleFlag';
import {EducationHistory, Overview, Skills, WorkExperiences, ScreeningQuestions} from '~/components/CandidateInfo';
import styles from '~/components/CandidateInfo/CandidateInfo.scss';
import {CandidateInfoPanel} from '~/components/CandidateInfo/CandidateInfoPanel';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {Tab, Tabs, TabsUse} from '~/components/Navigation/Tabs';
import {
  onApplicationCandidateTabClicked,
  onSuggestedTalentCandidateTabClicked,
  onBookmarkedCandidateTabClicked,
  onTalentSearchCandidateTabClicked,
} from '~/flux/analytics';
import {getFullPosition, sortEducationByLatest, sortWorkExperienceByLatest} from '~/flux/applications';
import {ICandidate, IJobScreeningQuestionResponse} from '~/graphql/candidates';
import {isNotNil} from '~/util/isNotNil';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {GetResumeButton} from '~/components/GetResumeButton/GetResumeButton';
import {IAppState} from '~/flux';
import {IJobPost} from '~/services/employer/jobs.types';
import {Skill} from '~/graphql/__generated__/types';

export enum CandidateTabs {
  OVERVIEW,
  EDUCATION_HISTORY,
  SKILLS,
  WORK_EXPERIENCES,
  SCREENING_QUESTIONS,
}

export interface ICandidateInfoProps {
  candidate: ICandidate;
  candidateType: CandidateType;
  job?: IJobPost;
  showTopMatch?: boolean;
  isBookmarked: boolean;
  formattedDate: string;
  candidateTabClicked?:
    | typeof onApplicationCandidateTabClicked
    | typeof onSuggestedTalentCandidateTabClicked
    | typeof onBookmarkedCandidateTabClicked
    | typeof onTalentSearchCandidateTabClicked;
  onResumeClick?: () => void;
  statusSelector?: ReactElement;
  onCandidateBookmarkClick: (checked: boolean) => void;
  rejectionComponent?: ReactElement;
  contactSuggestedTalent?: ReactElement;
  invitedTalents?: IEmailInvitation[];
  isBookmarkVisible?: boolean;
  hasResumeButtonOnBottom?: boolean;
  screeningQuestionResponses?: IJobScreeningQuestionResponse[] | null;
}

export const CandidateInfo: React.FunctionComponent<ICandidateInfoProps> = (props) => {
  const [selectedTab, setSelectedTab] = useState(CandidateTabs.OVERVIEW);
  const screeningQuestionsFeatureToggle = useSelector(({features}: IAppState) => features['screeningQuestions']);
  const enhancedSkillPillsFeatureToggle = useSelector(({features}: IAppState) => features['enhancedSkillPills']);
  const enhancedSkillPillsReleaseToggle = useSelector(
    ({releaseToggles}: IAppState) => releaseToggles['179899438enhancedSkillPills'],
  );

  const handleTabSelect = (index: CandidateTabs) => {
    setSelectedTab(index);
    if (props.candidateTabClicked) {
      props.candidateTabClicked(index);
    }
  };

  const {
    job,
    candidate,
    candidateType,
    formattedDate,
    onResumeClick,
    showTopMatch,
    isBookmarked,
    statusSelector,
    onCandidateBookmarkClick,
    rejectionComponent,
    contactSuggestedTalent,
    invitedTalents = [],
    isBookmarkVisible = true,
    hasResumeButtonOnBottom = false,
    screeningQuestionResponses,
  } = props;

  const {education, skills, workExperiences} = props.candidate;
  const nonNilSkills =
    enhancedSkillPillsReleaseToggle && enhancedSkillPillsFeatureToggle
      ? sortBy('skill', skills.filter(isNotNil))
      : skills.filter(isNotNil);
  const nonNilJobSkills = job
    ? enhancedSkillPillsReleaseToggle && enhancedSkillPillsFeatureToggle
      ? sortBy('skill', job.skills.filter(isNotNil))
      : job.skills.filter(isNotNil)
    : [];
  const sortedEducation = sortEducationByLatest(education.filter(isNotNil));
  const sortedWorkExperiences = sortWorkExperienceByLatest(workExperiences.filter(isNotNil));
  const position = getFullPosition(candidate);
  const skillArrayToObjectFn = (map: Record<string, string>, skill: Pick<Skill, 'uuid' | 'skill'>) => {
    if (skill.uuid) {
      map[skill.uuid] = skill.skill;
    }
    return map;
  };

  const jobSkillMap = nonNilJobSkills.reduce(skillArrayToObjectFn, {});
  const userSkillMap = nonNilSkills.reduce(skillArrayToObjectFn, {});
  const matchedSkills =
    nonNilJobSkills
      ?.filter(({uuid}: {uuid: string}) => userSkillMap[uuid])
      .map(({uuid, skill}) => {
        return {uuid, skill};
      }) ?? [];
  const excludedJobSkills = nonNilJobSkills.filter(({uuid}: {uuid: string}) => !userSkillMap[uuid]);
  const excludedUserSkills = job ? nonNilSkills.filter(({uuid}) => uuid && jobSkillMap && !jobSkillMap[uuid]) : [];

  const deprecatedOverviewComponent = (
    <Overview
      educationList={sortedEducation}
      matchedSkills={nonNilSkills}
      workExperiences={sortedWorkExperiences}
      onViewSkills={() => handleTabSelect(CandidateTabs.SKILLS)}
      applicationRejectionReason={rejectionComponent}
    />
  );

  return (
    <div className="flex flex-column bg-white w-100">
      <CandidateInfoPanel
        candidate={candidate}
        candidateType={candidateType}
        isInvitedTalent={invitedTalents.some((data) => data.individualId === candidate.id)}
        position={position}
        formattedDate={formattedDate}
        showTopMatch={showTopMatch}
        isBookmarked={isBookmarked}
        isBookmarkVisible={isBookmarkVisible}
        onResumeClick={onResumeClick}
        statusSelector={statusSelector}
        onCandidateBookmarkClick={onCandidateBookmarkClick}
        contactSuggestedTalent={contactSuggestedTalent}
        isResumeButtonVisible={!hasResumeButtonOnBottom}
      />
      <section className={`${styles.infoContainer} bg-white flex-shrink-0`}>
        <Tabs use={TabsUse.PANEL} selectedTab={selectedTab} onTabSelect={handleTabSelect}>
          <Tab tabId={CandidateTabs.OVERVIEW} title={'Overview'}>
            <section data-cy="overview-tab" className="pa4">
              {job ? (
                <ReleaseFlag
                  name="179899438enhancedSkillPills"
                  render={() => (
                    <FeatureFlag
                      name="enhancedSkillPills"
                      render={() => {
                        if (job) {
                          return (
                            <Overview
                              educationList={sortedEducation}
                              matchedSkills={matchedSkills}
                              nonMatchedSkills={excludedJobSkills}
                              workExperiences={sortedWorkExperiences}
                              onViewSkills={() => handleTabSelect(CandidateTabs.SKILLS)}
                              applicationRejectionReason={rejectionComponent}
                            />
                          );
                        }
                        return deprecatedOverviewComponent;
                      }}
                      fallback={() => deprecatedOverviewComponent}
                    />
                  )}
                  fallback={() => deprecatedOverviewComponent}
                />
              ) : (
                deprecatedOverviewComponent
              )}
            </section>
          </Tab>
          <Tab tabId={CandidateTabs.SKILLS} title={'Skills'}>
            <section data-cy="skills-tab" className="pa4">
              {job ? (
                <ReleaseFlag
                  name="179899438enhancedSkillPills"
                  render={() => (
                    <FeatureFlag
                      name="enhancedSkillPills"
                      render={() => {
                        if (job && matchedSkills && excludedJobSkills && excludedUserSkills) {
                          return (
                            <>
                              <Skills
                                matchedSkills={matchedSkills}
                                nonMatchedSkills={excludedJobSkills}
                                headerMessage={`Skills Matched To Job (${matchedSkills.length}/${
                                  matchedSkills.length + excludedJobSkills.length
                                })`}
                              />
                              {!!excludedUserSkills.length && (
                                <span className="pa4">
                                  <Skills matchedSkills={excludedUserSkills} headerMessage="Other Skills" />
                                </span>
                              )}
                            </>
                          );
                        }
                        return <Skills matchedSkills={nonNilSkills} />;
                      }}
                      fallback={() => <Skills matchedSkills={nonNilSkills} />}
                    />
                  )}
                  fallback={() => <Skills matchedSkills={nonNilSkills} />}
                />
              ) : (
                <Skills matchedSkills={nonNilSkills} />
              )}
            </section>
          </Tab>
          <Tab tabId={CandidateTabs.WORK_EXPERIENCES} title={'Work Experience'}>
            <section data-cy="work-experiences-tab" className="pa4">
              <WorkExperiences workExperiences={sortedWorkExperiences} />
            </section>
          </Tab>
          <Tab tabId={CandidateTabs.EDUCATION_HISTORY} title={'Education History'}>
            <section data-cy="education-history-tab" className="pa4">
              <EducationHistory educationList={sortedEducation} />
            </section>
          </Tab>
          <Tab
            tabId={CandidateTabs.SCREENING_QUESTIONS}
            title={'Screening Questions'}
            hidden={!screeningQuestionsFeatureToggle || !screeningQuestionResponses}
          >
            <section data-cy="screening-questions-tab" className="pa4">
              <ScreeningQuestions screeningQuestionResponses={screeningQuestionResponses ?? []} />
            </section>
          </Tab>
        </Tabs>
      </section>
      {hasResumeButtonOnBottom ? (
        <section className={`${styles.getResumeSection} bg-white pl4 pr4 bw1 bt b--light-gray`}>
          {!candidate.resume ? (
            <span
              data-cy="candidates-info-no-resume-text"
              className={`f6-5 fw4 db h-100 black-40 ${styles.noResumeAvailableText}`}
            >
              No Resume Available
            </span>
          ) : (
            <GetResumeButton resume={candidate.resume} onResumeClick={onResumeClick} />
          )}
        </section>
      ) : null}
    </div>
  );
};
