import React from 'react';
import {mcf} from '@mcf/constants';
import {IJobScreeningQuestionResponse} from '~/graphql/candidates';

interface IScreeningQuestionsProps {
  screeningQuestionResponses: IJobScreeningQuestionResponse[];
}

export const ScreeningQuestions: React.VFC<IScreeningQuestionsProps> = ({screeningQuestionResponses}) => (
  <dl className="ma0 lh-copy" data-cy="screening-questions">
    {screeningQuestionResponses.map(({question, answer}, index) => {
      return (
        <>
          <dt
            data-cy={`screening-question-question-${index}`}
            key={`screening-question-question-${index}`}
            className="fw6 black-60 f4-5 mr3 dib"
          >
            {`Q${index + 1}. `}
            {question}
          </dt>
          <dd
            data-cy={`screening-question-answer-${index}`}
            key={`screening-question-answer-${index}`}
            className="ma0 mt2 mb4 black-60"
          >
            {mcf.getScreeningQuestionResponse(answer).label}
          </dd>
        </>
      );
    })}
  </dl>
);
