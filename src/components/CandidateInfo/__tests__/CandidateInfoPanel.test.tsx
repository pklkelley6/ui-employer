import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {suggestedTalentsMock} from '~/__mocks__/suggestedTalents/suggestedTalents.mocks';
import {CandidateInfoPanel} from '~/components/CandidateInfo/CandidateInfoPanel';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('Candidates/CandidateInfoPanel', () => {
  describe('applicant', () => {
    const candidateInfoPanelProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.Applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isBookmarked: false,
      isInvitedTalent: false,
    };
    it('should render CandidateInfo', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} />);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });
  });

  describe('suggested talent', () => {
    const candidateInfoPanelProps = {
      candidate: suggestedTalentsMock[0].talent,
      candidateType: CandidateType.SuggestedTalent,
      formattedDate: 'Active 10 days ago',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isBookmarked: false,
      isInvitedTalent: false,
    };
    it('should render CandidateInfo', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} />);
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should not render email and mobile when its not present', () => {
      const wrapper = mount(
        <CandidateInfoPanel
          {...candidateInfoPanelProps}
          candidate={{...suggestedTalentsMock[0].talent, email: undefined, mobileNumber: undefined}}
        />,
      );

      const emailDisplay = wrapper.find('[data-cy="candidate-right-panel-1-email"]');
      expect(emailDisplay).toHaveLength(0);
      const mobileDisplay = wrapper.find('[data-cy="candidate-right-panel-1-mobile"]');
      expect(mobileDisplay).toHaveLength(0);
    });
  });

  describe('bookmark candidate toggle', () => {
    const candidateInfoPanelProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.Applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isInvitedTalent: false,
    };
    it('should render bookmark toggle as toggled on when isBookmarked is true', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} isBookmarked={true} />);
      const bookmarkToggle = wrapper.find('[data-cy="bookmark-candidate"]');
      expect(toJson(bookmarkToggle, cleanSnapshot())).toMatchSnapshot();
    });
    it('should not render bookmark toggle as toggled off when isBookmarked is false', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} isBookmarked={false} />);
      const bookmarkToggle = wrapper.find('[data-cy="bookmark-candidate"]');
      expect(toJson(bookmarkToggle, cleanSnapshot())).toMatchSnapshot();
    });
  });

  describe('invited talent tag toggle', () => {
    const candidateInfoPanelProps = {
      candidate: applicationMock.applicant,
      candidateType: CandidateType.Applicant,
      formattedDate: 'Applied on 10 Jan 2018',
      onResumeClick: jest.fn(),
      onCandidateBookmarkClick: jest.fn(),
      position: '',
      isBookmarked: false,
    };
    it('should render invited talent tag toggle as toggled on when isInvitedTalent is true', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} isInvitedTalent={true} />);

      expect(wrapper.find('[data-cy="invited-talent-label"]').exists()).toBe(true);
    });
    it('should not render invited talent tag toggle as toggled off when isInvitedTalent is false', () => {
      const wrapper = mount(<CandidateInfoPanel {...candidateInfoPanelProps} isInvitedTalent={false} />);

      expect(wrapper.find('[data-cy="invited-talent-label"]').exists()).toBe(false);
    });
  });
});
