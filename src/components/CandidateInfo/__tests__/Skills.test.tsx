import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import Skills, {ISkillsProps} from '../Skills';
import {IAppState} from '~/flux';
import {SkillPill} from '~/components/JobPosting/Skills/SkillPill';

describe('Skills', () => {
  const defaultSkills = [
    {uuid: '123', skill: 'skillA'},
    {uuid: '456', skill: 'skillB'},
    {uuid: '789', skill: 'skillC'},
  ];

  const defaultProps: ISkillsProps = {
    matchedSkills: defaultSkills,
  };

  describe('with enhanced skill pills feature on', () => {
    const mountWithProps = (props: Partial<ISkillsProps>) => mountComponent(true)({...defaultProps, ...props});

    it('should display the appropriate header if headerMessage is defined', () => {
      const headerMessage = 'test header';
      const wrapper = mountWithProps({headerMessage});

      expect(wrapper.find('h4').text()).toEqual(headerMessage);
    });

    it('should display the default header if headerMessage is not defined', () => {
      const wrapper = mountWithProps({});

      expect(wrapper.find('h4').text()).toEqual('Skills');
    });

    it('should display a message if matchedSkills is empty', () => {
      const wrapper = mountWithProps({matchedSkills: []});

      expect(wrapper.find(SkillPill)).toHaveLength(0);
      expect(wrapper.text()).toContain('This candidate has not added any skills matched to the job.');
    });

    it('should display matching number of SkillPills if matchedSkills length > 0', () => {
      const wrapper = mountWithProps({});
      const skillPills = wrapper.find(SkillPill);

      const expectedSkillNames = defaultSkills.map(({skill}) => skill);
      expect(skillPills).toHaveLength(defaultSkills.length);
      skillPills.forEach((pill) => {
        expect(pill.props().selected).toBeTruthy();
        expect(expectedSkillNames).toContain(pill.props().label);
      });
    });

    it('should not display SkillPills with selected=false if nonMatchedSkills is not defined and if matchedSkills length > 0', () => {
      const wrapper = mountWithProps({});
      const skillPills = wrapper.find(SkillPill);

      expect(skillPills).toHaveLength(defaultSkills.length);
      skillPills.forEach((pill) => {
        expect(pill.props().selected).not.toBeFalsy();
      });
    });

    it('should display SkillPills with selected=false if nonMatchedSkills defined and if matchedSkills length > 0', () => {
      const nonMatchedSkills = [
        {uuid: '321', skill: 'notSkillA'},
        {uuid: '654', skill: 'notSkillB'},
      ];
      const wrapper = mountWithProps({nonMatchedSkills});

      const selectedSkillPills = wrapper.find(SkillPill).findWhere((pill) => pill.prop('selected') === true);
      expect(selectedSkillPills).toHaveLength(defaultSkills.length);
      const nonSelectedSkillPills = wrapper.find(SkillPill).findWhere((pill) => pill.prop('selected') === false);
      expect(nonSelectedSkillPills).toHaveLength(nonMatchedSkills.length);
    });
  });

  describe('with enhanced skill pills feature off', () => {
    const mountWithProps = (props: Partial<ISkillsProps>) => mountComponent(false)({...defaultProps, ...props});

    it('should display matching number of skills if matchedSkills is not empty', () => {
      const wrapper = mountWithProps({});
      expect(wrapper.find('[data-cy="skill-icon"]')).toHaveLength(defaultSkills.length);

      defaultSkills.forEach(({skill}) => {
        expect(wrapper.text()).toContain(skill);
      });
    });

    it('should display a message if matchedSkills is empty', () => {
      const wrapper = mountWithProps({matchedSkills: []});
      expect(wrapper.text()).toEqual(
        'This candidate has not added any skills. You may want to view the resume instead.',
      );
    });
  });
});

const mountComponent = (enhancedSkillPillsFlag: boolean) => (props: ISkillsProps) => {
  const store = configureStore<{
    features: Partial<IAppState['features']>;
    releaseToggles: Partial<IAppState['releaseToggles']>;
  }>()({
    features: {
      enhancedSkillPills: enhancedSkillPillsFlag,
    },
    releaseToggles: {
      '179899438enhancedSkillPills': enhancedSkillPillsFlag,
    },
  });

  return mount(
    <Provider store={store}>
      <Skills {...props} />
    </Provider>,
  );
};
