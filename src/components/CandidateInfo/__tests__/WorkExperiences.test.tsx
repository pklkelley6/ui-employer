import {mount} from 'enzyme';
import React from 'react';
import {WorkExperiences} from '../WorkExperiences';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {IWorkExperience} from '~/graphql/candidates';

describe('CandidateInfo/WorkExperiences', () => {
  const workExperienceMock: IWorkExperience = applicationMock.applicant.workExperiences[0]!;

  it('should render WorkExperiences not more than the specified limit', () => {
    const workExperiencesMock: IWorkExperience[] = [workExperienceMock, workExperienceMock];
    const wrapper = mount(<WorkExperiences workExperiences={workExperiencesMock} limit={1} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render endDate as Present if endDate is undefined and startDate is defined', () => {
    const workExperiencesMock: IWorkExperience[] = [
      {
        ...workExperienceMock,
        endDate: null,
      },
    ];
    const wrapper = mount(<WorkExperiences workExperiences={workExperiencesMock} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render when optional values are not defined', () => {
    const workExperiencesMock: IWorkExperience[] = [
      {
        companyName: 'Some Company',
        endDate: null,
        jobDescription: null,
        jobTitle: 'Some Job',
        startDate: null,
      },
    ];
    const wrapper = mount(<WorkExperiences workExperiences={workExperiencesMock} />);
    expect(wrapper).toMatchSnapshot();
  });
});
