import React from 'react';
import {mount, ReactWrapper} from 'enzyme';
import {act} from 'react-dom/test-utils';
import toJson from 'enzyme-to-json';
import {nextTick, cleanSnapshot} from '~/testUtil/enzyme';
import * as getEmailInvitations from '~/services/suggestedTalents/getEmailInvitations';
import * as postEmailInvitation from '~/services/suggestedTalents/postEmailInvitation';
import {ContactSuggestedTalent} from '~/components/CandidateInfo/ContactSuggestedTalent';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import * as GrowlNotification from '~/components/GrowlNotification/GrowlNotification';

const getEmailInvitationMock = jest.spyOn(getEmailInvitations, 'getEmailInvitations');
const postEmailInvitationMock = jest.spyOn(postEmailInvitation, 'postEmailInvitation');
const growlNotificationMock = jest.spyOn(GrowlNotification, 'growlNotification');

describe('CandidateInfo/ContactSuggestedTalent', () => {
  it('should show disabled button if getIndividualIdFunction API already has the individualId', async () => {
    const emailInvitationMock: IEmailInvitation[] = [
      {
        jobUuid: '10000000-1000000',
        individualId: '1000000000',
        updatedAt: new Date(),
        createdAt: new Date(),
        isEmailSent: false,
      },
    ];

    getEmailInvitationMock.mockImplementation(() => Promise.resolve(emailInvitationMock));

    const wrapper = mountContactSuggestedTalent();

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="invite-talent-apply-button-sent"]')).toHaveLength(1);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should show button if getIndividualIdFunction API did not find individualId', async () => {
    const emailInvitationMock: IEmailInvitation[] = [
      {
        jobUuid: '10000000-1000000',
        individualId: '2000000000',
        updatedAt: new Date(),
        createdAt: new Date(),
        isEmailSent: false,
      },
    ];

    getEmailInvitationMock.mockImplementation(() => Promise.resolve(emailInvitationMock));

    const wrapper = mountContactSuggestedTalent();

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="invite-talent-apply-button"]')).toHaveLength(1);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should change to a disabled button when invitation button has been clicked', async () => {
    const getEmailInvitationDataMock: IEmailInvitation[] = [
      {
        jobUuid: '10000000-1000000',
        individualId: '2000000000',
        updatedAt: new Date(),
        createdAt: new Date(),
        isEmailSent: false,
      },
    ];

    const postEmailInvitationDataMock: IEmailInvitation = {
      jobUuid: '10000000-1000000',
      individualId: '1000000000',
      updatedAt: new Date(),
      createdAt: new Date(),
      isEmailSent: false,
    };

    getEmailInvitationMock.mockImplementation(() => Promise.resolve(getEmailInvitationDataMock));
    postEmailInvitationMock.mockImplementation(() => Promise.resolve(postEmailInvitationDataMock));

    const wrapper = mountContactSuggestedTalent();

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="invite-talent-apply-button"]').simulate('click');
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="invite-talent-apply-button-sent"]')).toHaveLength(1);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('should growl an error if postEmailInvitation API has error', async () => {
    const getEmailInvitationDataMock: IEmailInvitation[] = [
      {
        jobUuid: '10000000-1000000',
        individualId: '2000000000',
        updatedAt: new Date(),
        createdAt: new Date(),
        isEmailSent: false,
      },
    ];

    const errorDataMock = 'DATABASE_ERROR';

    getEmailInvitationMock.mockImplementation(() => Promise.resolve(getEmailInvitationDataMock));
    postEmailInvitationMock.mockImplementation(() => Promise.reject(errorDataMock));

    const wrapper = mountContactSuggestedTalent();

    await nextTick(wrapper);

    act(() => {
      wrapper.find('[data-cy="invite-talent-apply-button"]').simulate('click');
    });

    await nextTick(wrapper);

    expect(wrapper.find('[data-cy="invite-talent-apply-button"]')).toHaveLength(1);
    expect(growlNotificationMock).toBeCalledWith(
      'Temporarily unable to send invitation to apply. Please try again.',
      GrowlNotification.GrowlType.ERROR,
      {
        toastId: 'contactSuggestedTalentError',
        autoClose: 5000,
      },
    );
  });
});

const dispatchProps = {
  onSuggestedTalentCandidateInviteToApplyClicked: jest.fn(),
};

const mountContactSuggestedTalent = (): ReactWrapper => {
  return mount(<ContactSuggestedTalent {...dispatchProps} individualId={'1000000000'} jobUuid={'10000000-1000000'} />);
};
