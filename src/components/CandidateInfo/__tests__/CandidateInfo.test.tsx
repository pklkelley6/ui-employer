import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {range} from 'lodash/fp';
import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {mcf} from '@mcf/constants';
import {CandidateInfo, ICandidateInfoProps} from '../CandidateInfo';
import {IAppState} from '~/flux';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('CandidateInfo/CandidateInfo', () => {
  const store = configureStore<{
    features: Partial<IAppState['features']>;
    releaseToggles: Partial<IAppState['releaseToggles']>;
  }>()({
    features: {
      screeningQuestions: true,
      enhancedSkillPills: false,
    },
    releaseToggles: {
      '179899438enhancedSkillPills': false,
    },
  });
  const candidateInfoProps = {
    candidate: applicationMock.applicant,
    candidateType: CandidateType.Applicant,
    candidateTabClicked: jest.fn(),
    onCandidateBookmarkClick: jest.fn(),
    formattedDate: 'Applied on 10 Jan 2018',
    scores: {
      wcc: 5.0,
    },
    isBookmarked: false,
    screeningQuestionResponses: [
      {question: 'question 1', answer: mcf.SCREEN_QUESTION_RESPONSE.YES},
      {question: 'question 2', answer: mcf.SCREEN_QUESTION_RESPONSE.NO},
    ],
  };

  it('should render candidate info with tab contents', () => {
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoProps} />
      </Provider>,
    );
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();

    const skillsTabTitle = wrapper.find('[title="Skills"]');
    skillsTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="skills-tab"]')).toMatchSnapshot();

    const workExperiencesTabTitle = wrapper.find('[title="Work Experience"]');
    workExperiencesTabTitle.simulate('click');
    expect(toJson(wrapper.find('[data-cy="work-experiences-tab"]'), cleanSnapshot())).toMatchSnapshot();

    const educationHistoryTabTitle = wrapper.find('[title="Education History"]');
    educationHistoryTabTitle.simulate('click');
    expect(toJson(wrapper.find('[data-cy="education-history-tab"]'), cleanSnapshot())).toMatchSnapshot();

    const screeningQuestionsTabTitle = wrapper.find('[title="Screening Questions"]');
    screeningQuestionsTabTitle.simulate('click');
    expect(toJson(wrapper.find('[data-cy="screening-questions-tab"]'), cleanSnapshot())).toMatchSnapshot();
  });

  it('should should sort skills by uuid when enhancedSkillPills feature is off', () => {
    const skillList = [
      {uuid: '1', skill: 'Z'},
      {uuid: '2', skill: 'A'},
    ];

    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo
          {...{
            ...candidateInfoProps,
            candidate: {...applicationMock.applicant, skills: skillList},
          }}
        />
      </Provider>,
    );

    const skills = wrapper.find('Overview').prop('matchedSkills');
    expect(skills).toEqual([
      {uuid: '1', skill: 'Z'},
      {uuid: '2', skill: 'A'},
    ]);
  });

  it('should should sort skills by skill name when enhancedSkillPills feature is on', () => {
    const skillList = [
      {uuid: '1', skill: 'Z'},
      {uuid: '2', skill: 'A'},
    ];
    const store = configureStore<{
      features: Partial<IAppState['features']>;
      releaseToggles: Partial<IAppState['releaseToggles']>;
    }>()({
      features: {
        enhancedSkillPills: true,
      },
      releaseToggles: {
        '179899438enhancedSkillPills': true,
      },
    });
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo
          {...{
            ...candidateInfoProps,
            candidate: {...applicationMock.applicant, skills: skillList},
          }}
        />
      </Provider>,
    );

    const skills = wrapper.find('Overview').prop('matchedSkills');
    expect(skills).toEqual([
      {uuid: '2', skill: 'A'},
      {uuid: '1', skill: 'Z'},
    ]);
  });

  it('should render skills tab when there are more than 10 skills and viewSkillsButton is clicked', () => {
    const candidateInfoPropsWithMoreSkills = {
      ...candidateInfoProps,
      candidate: {
        ...applicationMock.applicant,
        skills: range(0)(11).map((id) => ({id, skill: `${id}`})),
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoPropsWithMoreSkills} />
      </Provider>,
    );
    const viewSkillsButton = wrapper.find('ViewSkillsButton a');
    viewSkillsButton.simulate('click');
    expect(toJson(wrapper.find('[data-cy="skills-tab"]'), cleanSnapshot())).toMatchSnapshot();
  });

  it('should render candidate info tab contents for empty state', () => {
    const candidateInfoPropsWithEmptyState = {
      ...candidateInfoProps,
      candidate: {
        ...applicationMock.applicant,
        education: [],
        skills: [],
        workExperiences: [],
      },
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoPropsWithEmptyState} />
      </Provider>,
    );
    expect(wrapper.find('[data-cy="overview-tab"]')).toMatchSnapshot();

    const skillsTabTitle = wrapper.find('[title="Skills"]');
    skillsTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="skills-tab"]')).toMatchSnapshot();

    const workExperiencesTabTitle = wrapper.find('[title="Work Experience"]');
    workExperiencesTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="work-experiences-tab"]')).toMatchSnapshot();

    const educationHistoryTabTitle = wrapper.find('[title="Education History"]');
    educationHistoryTabTitle.simulate('click');
    expect(wrapper.find('[data-cy="education-history-tab"]')).toMatchSnapshot();
  });

  it('should show `No Resume Available` when there is no resume and hasResumeButtonOnBottom is true', () => {
    const candidateInfoPropsWithoutResume: ICandidateInfoProps = {
      ...candidateInfoProps,
      candidate: {
        ...applicationMock.applicant,
        education: [],
        resume: null,
        skills: [],
        workExperiences: [],
      },
      hasResumeButtonOnBottom: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoPropsWithoutResume} />
      </Provider>,
    );
    const noResumeText = wrapper.find('[data-cy="candidates-info-no-resume-text"]');
    expect(noResumeText).toHaveLength(1);
  });

  it('should render the reject component if it is provided', () => {
    const mockRejectionComponent = <div id="rejection-component">hi</div>;
    const candidateInfoPropsWithRejectionComponent: ICandidateInfoProps = {
      ...candidateInfoProps,
      rejectionComponent: mockRejectionComponent,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidateInfo {...candidateInfoPropsWithRejectionComponent} />
      </Provider>,
    );
    expect(wrapper.find('#rejection-component')).toHaveLength(1);
  });

  describe('screening questions tab', () => {
    it('should not render screening questions tab when there is no screeningQuestions responses', () => {
      const candidateInfoPropsWithoutScreeningQuestionResponses: ICandidateInfoProps = {
        ...candidateInfoProps,
        screeningQuestionResponses: null,
      };
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfo {...candidateInfoPropsWithoutScreeningQuestionResponses} />
        </Provider>,
      );

      expect(wrapper.find('[title="Screening Questions"]')).toHaveLength(0);
    });

    it('should not render screening questions tab when screeningQuestions feature is off', () => {
      const store = configureStore<{
        features: Partial<IAppState['features']>;
        releaseToggles: Partial<IAppState['releaseToggles']>;
      }>()({
        features: {
          screeningQuestions: false,
        },
        releaseToggles: {'179899438enhancedSkillPills': true},
      });
      const wrapper = mount(
        <Provider store={store}>
          <CandidateInfo {...candidateInfoProps} />
        </Provider>,
      );

      expect(wrapper.find('[title="Screening Questions"]')).toHaveLength(0);
    });
  });
});
