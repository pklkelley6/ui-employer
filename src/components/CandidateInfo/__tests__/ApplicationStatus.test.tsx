import {MockedProvider} from '@apollo/react-testing';
import {mcf} from '@mcf/constants';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {print} from 'graphql';
import {ApplicationStatus} from '../ApplicationStatus';
import {PactBuilder} from '~/__mocks__/pact';
import {Dropdown} from '~/components/Core/Dropdown';
import {SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE} from '~/graphql/applications';
import {nextTick} from '~/testUtil/enzyme';
import {RefetchApplicationsWithStatusCountByJobContext} from '~/pages/Candidates/RefetchApplicationsByJobAndStatusContext';
import {API_PROFILE_VERSION} from '~/graphql';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

const applicationIdMock = 'R90SS0001A-90';

describe('ApplicationStatus', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });

  afterAll(async () => pactBuilder.provider.finalize());

  it('should display the correct statusId in the dropdown selection', () => {
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );
    const dropdown = component.find(Dropdown);
    expect(dropdown.prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
  });

  it('should display no selection if the application has under review status but isShortlisted is false', () => {
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );
    const dropdown = component.find(Dropdown);
    expect(dropdown.prop('input').value).toEqual(undefined);
  });

  it('should update the application statusId and set isShortlisted to true if under review status is selected', async () => {
    const variables = {
      applicationId: applicationIdMock,
      isShortlisted: true,
      statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
    };
    const mockResult = {
      data: {
        setApplicationShortlistState: {id: applicationIdMock, isShortlisted: true},
        setApplicationStatus: {id: applicationIdMock, statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW},
      },
    };
    const graphqlInteraction = new ApolloGraphQLInteraction()
      .uponReceiving('getApplicationShortlistAndStatusUpdateUnderReview')
      .withQuery(print(SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
      .withOperation('setApplicationShortlistAndStatusUpdate')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables)
      .willRespondWith({
        body: Matchers.like(mockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(graphqlInteraction);

    const onLoadingMock = jest.fn();

    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
      onLoading: onLoadingMock,
    };

    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );

    act(() => {
      component.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
    });
    component.update();
    expect(onLoadingMock).toBeCalled();
    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: component});
    });

    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
  });

  it('should update the application statusId and set isShortlisted to false if hired status is selected', async () => {
    const variables = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const mockResult = {
      data: {
        setApplicationShortlistState: {id: applicationIdMock, isShortlisted: false},
        setApplicationStatus: {id: applicationIdMock, statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL},
      },
    };
    const graphqlInteraction = new ApolloGraphQLInteraction()
      .uponReceiving('getApplicationShortlistAndStatusUpdateSuccessful')
      .withQuery(print(SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
      .withOperation('setApplicationShortlistAndStatusUpdate')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables)
      .willRespondWith({
        body: Matchers.like(mockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(graphqlInteraction);

    const onLoadingMock = jest.fn();

    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
      onLoading: onLoadingMock,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );

    act(() => {
      component.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
    });
    component.update();
    expect(onLoadingMock).toBeCalled();

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: component});
    });

    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
  });

  it('should update the application statusId and remove rejection reason when status change from rejected', async () => {
    const variables = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
    };
    const mockResult = {
      data: {
        setApplicationStatus: {
          id: applicationIdMock,
          statusId: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
          rejectionReason: null,
          rejectionReasonExtended: null,
        },
      },
    };
    const graphqlInteraction = new ApolloGraphQLInteraction()
      .given(`application ${applicationIdMock} has application with UNSUCCESSFUL status`)
      .uponReceiving('getApplicationStatusUpdateFromUnsuccessful')
      .withQuery(print(SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE))
      .withOperation('setApplicationShortlistAndStatusUpdate')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables)
      .willRespondWith({
        body: Matchers.like(mockResult),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(graphqlInteraction);

    const onLoadingMock = jest.fn();

    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
      onLoading: onLoadingMock,
    };
    const component = mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <ApplicationStatus {...props} />
      </ApolloProvider>,
    );

    act(() => {
      component.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.SUCCESSFUL);
    });
    component.update();
    expect(onLoadingMock).toBeCalled();

    await act(async () => {
      await pactBuilder.verifyInteractions();
    });
  });

  it('should revert to previous selection if updating the status failed', async () => {
    const mutationMockError = [
      {
        error: new Error('Error'),
        request: {
          query: SET_APPLICATION_SHORTLIST_AND_STATUS_UPDATE,
          variables: {
            applicationId: applicationIdMock,
            isShortlisted: true,
            statusId: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
          },
        },
      },
    ];
    const onErrorMock = jest.fn();
    const props = {
      applicationId: applicationIdMock,
      isShortlisted: false,
      statusId: mcf.JOB_APPLICATION_STATUS.RECEIVED,
      onError: onErrorMock,
    };
    const component = mount(
      <RefetchApplicationsWithStatusCountByJobContext.Provider value={jest.fn()}>
        <MockedProvider mocks={mutationMockError} addTypename={false}>
          <ApplicationStatus {...props} />
        </MockedProvider>
      </RefetchApplicationsWithStatusCountByJobContext.Provider>,
    );

    act(() => {
      component.find(Dropdown).prop('input').onChange(mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW);
    });
    await nextTick(component);
    await nextTick(component);
    expect(component.find(Dropdown).prop('input').value).toEqual(mcf.JOB_APPLICATION_STATUS.RECEIVED);
    expect(onErrorMock).toBeCalledTimes(1);
  });
});
