import {mount} from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {Overview, IOverviewProps} from '../Overview';
import {Skills} from '../Skills';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {ISkill} from '~/graphql/candidates';
import {isNotNil} from '~/util/isNotNil';
import {IAppState} from '~/flux';

describe('CandidateInfo/Overview', () => {
  const skills: ISkill[] = applicationMock.applicant.skills.filter(isNotNil);
  const defaultProps: IOverviewProps = {
    matchedSkills: skills,
    nonMatchedSkills: [],
    educationList: [],
    workExperiences: [],
    onViewSkills: jest.fn(),
  };

  describe('with enhancedSkillPills feature flag on', () => {
    const mountWithProps = (props: Partial<IOverviewProps>) => mountComponent(true)({...defaultProps, ...props});

    it('should render Skills component with matchedSkills, nonMatchedSkills, headerMessage and limit props', () => {
      const wrapper = mountWithProps({});

      const skillsComponent = wrapper.find(Skills);

      const expectedProps = ['matchedSkills', 'nonMatchedSkills', 'headerMessage', 'limit'];
      expectedProps.forEach((propName) => {
        expect(skillsComponent.prop(propName)).not.toBeUndefined();
      });
      const viewSkillsButton = wrapper.find('a');
      expect(viewSkillsButton).toHaveLength(1);
      expect(viewSkillsButton.text()).toEqual('View all skills');
    });

    it('should pass down formatted skills header message if matchedSkills and nonMatchedSkills are defined', () => {
      const wrapper = mountWithProps({});

      const skillsComponent = wrapper.find(Skills);
      expect(skillsComponent.prop('headerMessage')).toEqual('Skills Matched To Job (1/1)');
    });

    it('should pass down unformatted skills header message if nonMatchedSkills is undefined', () => {
      const wrapper = mountWithProps({nonMatchedSkills: undefined});

      const skillsComponent = wrapper.find(Skills);
      expect(skillsComponent.prop('headerMessage')).toEqual('Skills');
    });
  });

  describe('with enhancedSkillPills feature flag off', () => {
    const mountWithProps = (props: Partial<IOverviewProps>) => mountComponent(false)({...defaultProps, ...props});

    it('should not render the candidate info partially when some are missing', () => {
      const wrapper = mountWithProps({});

      expect(wrapper).toMatchSnapshot();
    });

    it('should render Skills component with matchedSkills and limit props', () => {
      const wrapper = mountWithProps({});

      const skillsComponent = wrapper.find(Skills);

      const expectedProps = ['matchedSkills', 'limit'];
      const expectedUndefinedProps = ['nonMatchedSkills', 'headerMessage'];
      expectedProps.forEach((propName) => {
        expect(skillsComponent.prop(propName)).not.toBeUndefined();
      });
      expectedUndefinedProps.forEach((propName) => {
        expect(skillsComponent.prop(propName)).toBeUndefined();
      });
    });

    it('should show ViewSkillsButton when there are more than 10 matchedSkills', () => {
      const elevenSkills = Array.from(Array(11).keys()).map((index) => {
        return {uuid: `uuid${index}`, skill: `skill${index}`};
      });

      const wrapper = mountWithProps({matchedSkills: elevenSkills});

      const viewSkillsButton = wrapper.find('a');
      expect(viewSkillsButton).toHaveLength(1);
      expect(viewSkillsButton.text()).toEqual('View all skills');
    });

    it('should not show ViewSkillsButton when there are more than 10 matchedSkills', () => {
      const nineSkills = Array.from(Array(9).keys()).map((index) => {
        return {uuid: `uuid${index}`, skill: `skill${index}`};
      });

      const wrapper = mountWithProps({matchedSkills: nineSkills});

      const viewSkillsButton = wrapper.find('a');
      expect(viewSkillsButton).toHaveLength(0);
    });
  });
});

const mountComponent = (enhancedSkillPillsFlag: boolean) => (props: IOverviewProps) => {
  const store = configureStore<{
    features: Partial<IAppState['features']>;
    releaseToggles: Partial<IAppState['releaseToggles']>;
  }>()({
    features: {
      enhancedSkillPills: enhancedSkillPillsFlag,
    },
    releaseToggles: {
      '179899438enhancedSkillPills': enhancedSkillPillsFlag,
    },
  });

  return mount(
    <Provider store={store}>
      <Overview {...props} />
    </Provider>,
  );
};
