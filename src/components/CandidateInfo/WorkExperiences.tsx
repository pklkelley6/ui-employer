import {format, parseISO} from 'date-fns';
import React from 'react';
import {Experience} from './Experience';
import {IWorkExperience} from '~/graphql/candidates';
import {NULL_PLACEHOLDER} from '~/util/constants';

interface IWorkExperiencesProps {
  workExperiences: IWorkExperience[];
  limit?: number;
}

export const WorkExperiences: React.FunctionComponent<IWorkExperiencesProps> = ({workExperiences, limit}) => {
  return workExperiences.length ? (
    <div>
      <ul className="list pl0 ma0">
        {workExperiences
          .map((workExperience, index) => {
            const startDate = workExperience.startDate && format(parseISO(workExperience.startDate), 'yyyy');
            const endDate = workExperience.endDate ? format(parseISO(workExperience.endDate), 'yyyy') : 'Present';
            const duration = startDate ? `${startDate} - ${endDate}` : NULL_PLACEHOLDER;

            return (
              <li data-cy="work-experience-field" key={index}>
                <Experience
                  title={workExperience.jobTitle || NULL_PLACEHOLDER}
                  location={workExperience.companyName || NULL_PLACEHOLDER}
                  time={duration}
                  description={workExperience.jobDescription || undefined}
                />
              </li>
            );
          })
          .slice(0, limit)}
      </ul>
    </div>
  ) : (
    <div className="f5 black-50 lh-copy tc">
      This candidate has not added any work experience. <br /> You may want to view the resume instead.
    </div>
  );
};
