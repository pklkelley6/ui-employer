import React, {useCallback} from 'react';
import {times} from 'lodash/fp';
import {throttle} from 'lodash';
import styles from './HiddenCandidateInfo.scss';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {HiddenCandidate} from '~/graphql/__generated__/types';
import {BookmarkToggle} from '~/components/BookmarkToggle/BookmarkToggle';

export interface IHiddenCandidateInfoProps {
  candidate: HiddenCandidate;
  candidateType: CandidateType;
  isBookmarked: boolean;
  onCandidateBookmarkClick: (checked: boolean) => void;
}

const greyBarGroup = (num: number, padTop = 0, padBottom = 0) => {
  return times(
    () => (
      <div className={`bg-white w-100 pb${padBottom} pt${padTop} o-50 flex`}>
        <div className="flex-auto mr3">
          <div className={`w-40 h1 ${styles.staticGradient}`} />
          <div className={`w-100 h1 mt2 ${styles.staticGradient}`} />
          <div className={`w-90 h1 mt2 ${styles.staticGradient}`} />
        </div>
      </div>
    ),
    num,
  );
};

export const HiddenCandidateInfo: React.FunctionComponent<IHiddenCandidateInfoProps> = ({
  candidate,
  candidateType,
  isBookmarked,
  onCandidateBookmarkClick,
}) => {
  const throttledOnBookmarkClick = useCallback(
    throttle((checked: boolean) => onCandidateBookmarkClick(checked), 500, {trailing: false}),
    [],
  );
  return (
    <div data-cy="hidden-candidate-info" className="flex flex-column bg-white w-100">
      <section className="pa4 flex-shrink-0">
        <div className="flex justify-between">
          <div>
            <h1 data-cy="hidden-candidate-info-name" className="f3 fw6 mt0 mb1 ws-normal primary">
              {candidate.name}
            </h1>
            <p className="f5 mt3 mb1 black-70 lh-title">This {candidateType.toLowerCase()} is no longer available</p>
            {isBookmarked && (
              <p data-cy="hidden-candidate-unsave-text" className="f5 mt0 mb4 black-70 lh-title">
                You may want to unsave this {candidateType.toLowerCase()}
              </p>
            )}
          </div>
          <div className={`pl2 ${styles.candidateInfoPanelRight}`}>
            {isBookmarked && (
              <BookmarkToggle
                input={{
                  value: true,
                  onChange: () => throttledOnBookmarkClick(false),
                }}
              />
            )}
          </div>
        </div>
        <div className="flex-auto justify-between">{greyBarGroup(1)}</div>
      </section>
      <section className={`${styles.infoContainer} bg-white pa4 flex-shrink-0 relative`}>
        <p className={`ma0 relative tc f5 w-100 ${styles.top50}`}>
          This {candidateType.toLowerCase()} is no longer available
        </p>
        <div className="flex-auto justify-between">
          {greyBarGroup(1, 0, 3)}
          {greyBarGroup(4, 3, 3)}
        </div>
      </section>
    </div>
  );
};
