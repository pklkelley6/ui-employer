import React from 'react';

export const SearchIcon = () => <span className="icon-magnify black-60 f6" />;
