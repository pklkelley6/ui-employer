import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {MainNavigation} from './../MainNavigation';

import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {PostLoginMenu, PreLoginMenuContainer} from '~/components/Navigation/MainNavigationMenu';
import {IUser} from '~/flux/account';

describe('Navigation/MainNavigation', () => {
  let wrapper: ShallowWrapper;
  const user: IUser = uenUserMock;

  beforeEach(() => {
    wrapper = shallow(
      <MainNavigation
        user={user}
        onLogoutClicked={jest.fn()}
        onNavigationMenuLinkClicked={jest.fn()}
        survey={{hasShown: true}}
      />,
    );
  });

  describe('renders Links', () => {
    it('renders PostLoginMenu and not PreLoginMenuContainer if user is available', () => {
      expect(wrapper.find(PostLoginMenu).findWhere((x) => x.prop('user') === user)).toHaveLength(1);
      expect(wrapper.find(PreLoginMenuContainer)).toHaveLength(0);
    });

    it('renders PreLoginMenuContainer and not PostLoginMenu if user is not available', () => {
      wrapper = shallow(
        <MainNavigation
          onLogoutClicked={jest.fn()}
          onNavigationMenuLinkClicked={jest.fn()}
          survey={{hasShown: true}}
        />,
      );
      expect(wrapper.find(PostLoginMenu)).toHaveLength(0);
      expect(wrapper.find(PreLoginMenuContainer)).toHaveLength(1);
    });
  });

  it('renders i elements', () => {
    expect(wrapper.find('i')).toHaveLength(1);
  });
});
