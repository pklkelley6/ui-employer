import React from 'react';
import {RouteComponentProps} from 'react-router';
import styles from './MainNavigation.scss';

export const PostJobsLink: React.FunctionComponent<RouteComponentProps> = ({history, location}) => (
  <a
    href="/jobs"
    className={`no-underline pointer dib pv3 ph3 v-mid bb bw2 b--dark-pink ${styles.postJobsLink}`}
    data-cy="post-jobs-link"
    onClick={(event) => {
      event.preventDefault();

      const confirmMessage = 'Are you sure you want to leave this page? Your changes will not be saved.';
      if (location.pathname.includes('jobs/new')) {
        if (window.confirm(confirmMessage)) {
          history.push('/');
        }
      } else {
        history.push('/');
      }
    }}
  >
    <span className="pl3 pr2 fw6 f5 lh-solid dark-pink">Post Jobs</span>
  </a>
);
