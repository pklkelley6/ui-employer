import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {PostJobsLink} from './PostJobsLink';

export const PostJobsLinkContainer = connect()(withRouter(PostJobsLink));
