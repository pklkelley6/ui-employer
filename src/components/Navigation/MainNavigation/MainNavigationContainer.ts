import {connect} from 'react-redux';
import {MainNavigation} from './MainNavigation';
import {getAccountUser, IUser} from '~/flux/account';
import {onNavigationMenuLinkClicked} from '~/flux/analytics';
import {logout} from '~/flux/account';
import {IAppState} from '~/flux/index';
import {ISurveyState} from '~/flux/survey/survey.reducer';

export interface IMainNavigationProps extends IMainNavigationDispatchProps {
  user?: IUser;
  survey: ISurveyState;
}

export interface IMainNavigationDispatchProps {
  onNavigationMenuLinkClicked: typeof onNavigationMenuLinkClicked;
  onLogoutClicked: typeof logout;
}

export const mapStateToProps = (state: IAppState) => ({user: getAccountUser(state), survey: state.survey});

export const MainNavigationContainer = connect(mapStateToProps, {onLogoutClicked: logout, onNavigationMenuLinkClicked})(
  MainNavigation,
);
