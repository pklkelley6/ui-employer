import {shallow} from 'enzyme';
import React from 'react';
import {Pagination} from '../Pagination';

describe('Pagination/Pagination', () => {
  let component: any = null;

  const mockCurrentPage = 1;
  const mockTotalItems = 80;
  const mockItemsPerPage = 20;
  const props = {
    onPageChange: jest.fn(),
  };

  beforeEach(() => {
    component = shallow(
      <Pagination
        currentPage={mockCurrentPage}
        totalItemsLength={mockTotalItems}
        itemsPerPage={mockItemsPerPage}
        {...props}
      />,
    );
  });

  describe('.render()', () => {
    it('renders 2 divs', () => {
      const rendered = component.find('div.pv3');
      expect(rendered).toHaveLength(1);
      const spacing = component.find('div .pb4');
      expect(spacing).toHaveLength(1);
    });

    it('renders the correct amount of span buttons', () => {
      const rendered = component.find('span');
      // '<', '1', '2', '3', '4', '>'
      expect(rendered).toHaveLength(6);
    });

    it('has onClick props for non-active pages only', () => {
      const spans = component.find('span');
      expect(spans).toHaveLength(6);
      const onClicks = spans.findWhere((n: any) => n.prop('onClick') !== undefined);
      expect(onClicks).toHaveLength(5);
    });
  });

  describe('.onPageChange', () => {
    it('calls onPageChange when onPageChange is executed', () => {
      // add node.html() guard cause when a node is empty, node.text() returns the previous value
      component.findWhere((node: any) => node.html() !== '' && node.text() === '4').simulate('click');
      expect(props.onPageChange).toHaveBeenCalledTimes(1);
      expect(props.onPageChange).toHaveBeenCalledWith(3, 20);
    });
  });

  describe('.getPageNumbers', () => {
    it('returns correct element for single page', () => {
      expect(component.instance().getPageNumbers(1, 10, 1, 0)).toEqual([{pageIndex: 0, label: '1', active: true}]);
    });

    it('returns correct element for many pages, with current page as first', () => {
      expect(component.instance().getPageNumbers(10, 5, 21, 0)).toEqual([
        {pageIndex: 0, label: '1', active: true},
        {pageIndex: 1, label: '2', active: false},
        {pageIndex: 2, label: '3', active: false},
        {pageIndex: 1, label: '❯', active: false},
      ]);
    });

    it('returns correct element for many pages, current page at middle', () => {
      expect(component.instance().getPageNumbers(10, 5, 100, 6)).toEqual([
        {pageIndex: 5, label: '❮', active: false},
        {pageIndex: 4, label: '5', active: false},
        {pageIndex: 5, label: '6', active: false},
        {pageIndex: 6, label: '7', active: true},
        {pageIndex: 7, label: '8', active: false},
        {pageIndex: 8, label: '9', active: false},
        {pageIndex: 7, label: '❯', active: false},
      ]);
    });

    it('returns correct element for many pages, current page at last', () => {
      expect(component.instance().getPageNumbers(5, 5, 25, 4)).toEqual([
        {pageIndex: 3, label: '❮', active: false},
        {pageIndex: 2, label: '3', active: false},
        {pageIndex: 3, label: '4', active: false},
        {pageIndex: 4, label: '5', active: true},
      ]);
    });
  });
});
