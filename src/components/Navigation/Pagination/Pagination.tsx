import {range} from 'lodash/fp';
import React from 'react';

export interface IPaginationProps {
  onPageChange: (page: number, pageSize: number) => void;
  currentPage: number;
  totalItemsLength: number;
  itemsPerPage: number;
  maxVisiblePageButtons?: number;
}

export interface IPageNumberItem {
  active: boolean;
  label: string;
  pageIndex: number;
}

export class Pagination extends React.Component<IPaginationProps> {
  public render() {
    const renderButtons = this.renderButtons();
    return (
      <div className="tc pv3">
        {renderButtons}
        <div className="bw0 w-100 ma0 pb4" />
      </div>
    );
  }

  private getPageNumbers(
    itemsPerPage = 20,
    maxVisiblePageButtons = 5,
    totalItemsLength = 0,
    currentPage = 0,
  ): IPageNumberItem[] {
    const delta = Math.ceil((maxVisiblePageButtons - 1) / 2);
    const numberOfPages = Math.ceil(totalItemsLength / itemsPerPage);

    const min = Math.max(currentPage - delta, 0);
    const max = Math.min(currentPage + delta + 1, numberOfPages);

    const pages = range(min, max).map((index: number) => ({
      active: index === currentPage,
      label: `${index + 1}`,
      pageIndex: index,
    }));
    const prev = currentPage > 0 ? [{active: false, label: '❮', pageIndex: currentPage - 1}] : [];
    const next = currentPage < numberOfPages - 1 ? [{active: false, label: '❯', pageIndex: currentPage + 1}] : [];

    return [...prev, ...pages, ...next];
  }

  private renderButtons() {
    const {currentPage, totalItemsLength, itemsPerPage, maxVisiblePageButtons} = this.props;
    const pageNumbers = this.getPageNumbers(itemsPerPage, maxVisiblePageButtons, totalItemsLength, currentPage);

    return pageNumbers.map((page: any) => {
      const spanProps = page.active
        ? {
            className: 'f5-5 pv2 ph3 mh1 dib white bg-secondary',
          }
        : {
            className: 'f5-5 pv2 ph3 mh1 pointer dib black-70 hover-bg-white',
            onClick: () => this.props.onPageChange(page.pageIndex, itemsPerPage),
          };
      return (
        <span data-cy="pagination-number" key={page.label} {...spanProps}>
          {page.label}
        </span>
      );
    });
  }
}
