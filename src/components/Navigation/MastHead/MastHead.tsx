import React from 'react';
import styles from './MastHead.scss';
import {Row} from '~/components/Layouts/Row';

export const MastHead: React.FunctionComponent = () => {
  return (
    <Row className={`${styles['masthead-container']} ph4-ns ph3`}>
      <a href="https://www.gov.sg" target="_blank" className={`db pv1 no-underline ${styles['masthead']}`}>
        <span className={`v-mid ${styles['masthead-crest']}`} />
        <span className="ml1 lh-copy v-mid ">A Singapore Government Agency Website</span>
      </a>
    </Row>
  );
};
