import {connect} from 'react-redux';
import {CompanyDetails} from './CompanyDetails';
import {companyFetchRequested, getCompanyLogo, getCompanyName} from '~/flux/company';
import {IAppState} from '~/flux/index';

export const mapStateToProps = ({company, account}: IAppState) => ({
  companyLogo: getCompanyLogo(company),
  companyName: getCompanyName(company, account),
});

export const CompanyDetailsContainer = connect(mapStateToProps, {
  fetchCompanyInfo: companyFetchRequested,
})(CompanyDetails);
