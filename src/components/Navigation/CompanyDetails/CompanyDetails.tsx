import React, {useEffect} from 'react';
import {get} from 'lodash/fp';
import {Link} from 'react-router-dom';
import {ActionType} from 'typesafe-actions';
import DEFAULT_LOGO_URL from '../../../static/images/default-company-logo.png';
import styles from './CompanyDetails.scss';
import {companyFetchRequested} from '~/flux/company';

interface ICompanyDetails {
  companyName: string;
  companyLogo: string;
  fetchCompanyInfo: () => ActionType<typeof companyFetchRequested>;
}

export const CompanyDetails: React.FC<ICompanyDetails> = ({companyName, companyLogo, fetchCompanyInfo}) => {
  useEffect(() => {
    fetchCompanyInfo();
  }, []);

  const addDefaultLogoSrc = (e: React.ChangeEvent<HTMLImageElement>) => {
    const targetSrc = get('target.src', e);
    if (targetSrc !== DEFAULT_LOGO_URL && targetSrc.indexOf(DEFAULT_LOGO_URL) === -1) {
      e.target.src = DEFAULT_LOGO_URL;
    }
  };

  return (
    <div className="tc pv4" title={companyName} data-cy="company-details">
      <img
        src={companyLogo}
        onError={addDefaultLogoSrc}
        className={`${styles.logo} ba b--moon-gray`}
        data-cy="company-logo"
        alt={companyName}
      />
      <div className={`mt3 black-70 f6 fw7 lh-title ${styles.companyName}`} data-cy="company-name">
        {companyName}
      </div>
      <Link
        data-cy="to-company-profile"
        to="/company-profile"
        className={'blue dib f6 mt2'}
        title={'Edit company profile'}
      >
        Edit company profile
      </Link>
    </div>
  );
};
