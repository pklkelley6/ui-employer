import {mount} from 'enzyme';
import React from 'react';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {CompanyDetails} from '../CompanyDetails';

describe('CompanyDetails', () => {
  describe('CompanyDetails without companyProfile feature toggle', () => {
    it('should render CompanyDetails correctly', () => {
      const store = configureStore()();

      const propsMock = {
        companyName: 'DBS Bank Ltd',
        companyLogo: 'dbs-logo.png',
        fetchCompanyInfo: jest.fn(),
      };

      const wrapper = mount(
        <Provider store={store}>
          <Router keyLength={0}>
            <CompanyDetails {...propsMock} />
          </Router>
        </Provider>,
      );

      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('CompanyDetails with companyProfile feature toggle', () => {
    it('should render CompanyDetails correctly', () => {
      const store = configureStore()();

      const propsMock = {
        companyName: 'DBS Bank Ltd',
        companyLogo: 'dbs-logo.png',
        fetchCompanyInfo: jest.fn(),
      };

      const wrapper = mount(
        <Provider store={store}>
          <Router keyLength={0}>
            <CompanyDetails {...propsMock} />
          </Router>
        </Provider>,
      );
      const linkToCompanyProfile = wrapper.find(Link);
      expect(linkToCompanyProfile.props().to).toEqual('/company-profile');

      expect(wrapper).toMatchSnapshot();
    });
  });
});
