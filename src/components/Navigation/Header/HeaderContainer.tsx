import {ReactChild} from 'react';
import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {Header} from './Header';
import {resetJobPostingsParams} from '~/flux/jobPostings';

export interface IHeaderDispatchProps {
  resetJobPostingsParams: () => ActionType<typeof resetJobPostingsParams>;
}
export interface IHeaderOwnProps {
  crumbs?: ReactChild[];
}

export const HeaderContainer = connect<void, IHeaderDispatchProps, IHeaderOwnProps>(null, {resetJobPostingsParams})(
  Header,
);
