import {mount} from 'enzyme';
import React from 'react';
import {Link, MemoryRouter as Router} from 'react-router-dom';
import {Header} from '../Header';

describe('Header', () => {
  const propsMock = {
    crumbs: ['foo', 'bar', 'baz', 'All jobs'],
    resetJobPostingsParams: jest.fn(),
  };

  it('should render Header correctly', () => {
    const wrapper = mount(
      <Router>
        <Header {...propsMock} />
      </Router>,
    );
    expect(wrapper.find(Header)).toMatchSnapshot();

    const jobLink = wrapper.find(Link).findWhere((x) => x.prop('to') === '/jobs');
    expect(jobLink).toHaveLength(1);
    jobLink.simulate('click');
    expect(propsMock.resetJobPostingsParams).toHaveBeenCalledTimes(1);
  });
});
