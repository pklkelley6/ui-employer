import {mount} from 'enzyme';
import React from 'react';
import {NavLink, MemoryRouter as Router} from 'react-router-dom';
import toJson from 'enzyme-to-json';
import configureStore from 'redux-mock-store';
import {MockedProvider} from '@apollo/react-testing';
import {Provider} from 'react-redux';
import {MainSideBar} from '../MainSideBar';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {CreateJobPostingButton} from '~/components/Navigation/NewJobPostButton/CreateJobPostingButton';
import * as randomUtil from '~/util/generateRandomString';
import * as jobPostingsMock from '~/flux/jobPostings';

jest.spyOn(randomUtil, 'generateRandomString').mockImplementation(() => 'randomString');

jest.mock('~/components/Navigation/CompanyDetails/CompanyDetailsContainer', () => {
  return {
    CompanyDetailsContainer: () => 'CompanyDetailsContainer',
  };
});

jest.mock('~/flux/jobPostings');

describe('MainSideBar', () => {
  it('should render MainSideBar correctly', () => {
    const wrapper = mountComponent();

    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });

  it('renders NewJobPostButton', () => {
    const wrapper = mountComponent();

    const button = wrapper.find(CreateJobPostingButton);
    expect(button).toHaveLength(1);
  });

  it('renders Link to homepage', () => {
    const wrapper = mountComponent();

    const jobLink = wrapper.find(NavLink).findWhere((x) => x.prop('to') === '/jobs');
    expect(jobLink).toHaveLength(1);
    jobLink.simulate('click');
    expect(jobPostingsMock.resetJobPostingsParams).toHaveBeenCalledTimes(1);
  });

  it('renders Link to talent search page', () => {
    const wrapper = mountComponent();

    const jobLink = wrapper.find(NavLink).findWhere((x) => x.prop('to') === '/talent-search');
    expect(jobLink).toHaveLength(1);
  });
});

const mountComponent = () => {
  const store = configureStore()();
  return mount(
    <MockedProvider mocks={undefined}>
      <Provider store={store}>
        <Router keyLength={0}>
          <MainSideBar />
        </Router>
      </Provider>
    </MockedProvider>,
  );
};
