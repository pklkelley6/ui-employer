import React from 'react';
import {NavLink} from 'react-router-dom';
import styles from './MainSideBar.scss';
import {resetJobPostingsParams} from '~/flux/jobPostings';
import {CreateJobPostingButton} from '~/components/Navigation/NewJobPostButton/CreateJobPostingButton';
import {CompanyDetailsContainer} from '~/components/Navigation/CompanyDetails/CompanyDetailsContainer';

export const MainSideBar: React.FC = () => {
  return (
    <>
      <div id="main-side-bar" className={`pa3 fixed ${styles.sideBar}`}>
        <div className="flex-column">
          <CompanyDetailsContainer />
          <div className="pb3">
            <CreateJobPostingButton dataCy="create-job-posting-button-sidebar" />
          </div>
          <hr className={`${styles.divider} w-90 center mb1`} />
          <nav className="relative pt3">
            <div className="pb3">
              <NavLink
                activeClassName={`${styles.selected}`}
                to="/jobs"
                onClick={resetJobPostingsParams}
                className="no-underline relative gray"
                isActive={(_, {pathname}) =>
                  pathname.includes('jobs') ? !(pathname.includes('new') || pathname.includes('success')) : false
                }
              >
                <div data-cy="all-jobs-label" className={`ph3 pv1 pr5 fw6 f5 ${styles.allJobs}`}>
                  All Jobs
                </div>
              </NavLink>
            </div>
            <div className="pb3">
              <NavLink
                activeClassName={`${styles.selected}`}
                to="/talent-search"
                className="no-underline relative gray"
                isActive={(_, {pathname}) => pathname.includes('talent-search')}
              >
                <div data-cy="talent-search-label" className={`ph3 pv1 pr5 fw6 f5 ${styles.talentSearch}`}>
                  Talent Search
                </div>
              </NavLink>
            </div>
          </nav>
        </div>
      </div>
      <div className={`pa3 ${styles.sideBarDummy}`} />
    </>
  );
};
