import React from 'react';
import {IPreLoginMenuProps} from './PreLoginMenuContainer';
import {NavLoginLink} from '~/components/Login';

export const PreLoginMenu: React.FunctionComponent<IPreLoginMenuProps> = ({login}) => {
  return <NavLoginLink onClick={login} />;
};
