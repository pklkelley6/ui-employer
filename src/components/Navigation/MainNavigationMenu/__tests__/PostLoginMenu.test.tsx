import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {PostLoginMenu} from '../PostLoginMenu';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('PostLoginMenu', () => {
  it('renders correctly', () => {
    const wrapper = mount(<PostLoginMenu logoutClicked={jest.fn()} user={uenUserMock} />);
    const postLoginMenu = wrapper.find(PostLoginMenu);
    expect(toJson(postLoginMenu, cleanSnapshot())).toMatchSnapshot();
  });
});
