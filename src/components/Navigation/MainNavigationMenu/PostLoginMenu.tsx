import Tooltip from 'rc-tooltip';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import styles from './MainNavigationMenu.scss';
import {IUser} from '~/flux/account';
import {logout} from '~/flux/account';

interface IPostLoginMenuProps {
  user: IUser;
  logoutClicked: typeof logout;
}

export const PostLoginMenu: React.FunctionComponent<IPostLoginMenuProps> = ({user, logoutClicked}) => {
  const [isMenuVisible, setIsMenuVisible] = useState<boolean | undefined>();

  return (
    <Tooltip
      placement="bottom"
      trigger={['hover']}
      overlayClassName={`absolute z-999 bg-white shadow-1 tl`}
      destroyTooltipOnHide={true}
      visible={isMenuVisible}
      onVisibleChange={setIsMenuVisible}
      overlay={
        <>
          <Link
            id="account-info-button"
            to="/account-info"
            className={`pa3 w5 no-underline fw6 f5 db bb b--black-20 ${styles.submenuUser}`}
            onClick={() => {
              setIsMenuVisible(false);
            }}
          >
            Account Info
          </Link>
          <a
            id="logout-button"
            href="#"
            onClick={() => {
              setIsMenuVisible(false);
              logoutClicked();
            }}
            className={`pa3 w5 no-underline fw6 f5 db ${styles.submenuUser}`}
          >
            Logout
          </a>
        </>
      }
    >
      <a
        href="#"
        className={`relative no-underline fw6 truncate pv1 v-mid dib ${styles.menuUser}`}
        title={user.userInfo.userFullName}
        data-cy="user-fullname"
      >
        {user.userInfo.userFullName}
      </a>
    </Tooltip>
  );
};
