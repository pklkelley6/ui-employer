import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {SurveyBar} from './SurveyBar';
import {closeSurvey, takeSurvey} from '~/flux/survey/survey.actions';

export interface ISurveyBarDispatchProps {
  closeSurvey: () => ActionType<typeof closeSurvey>;
  takeSurvey: () => ActionType<typeof takeSurvey>;
}

export const SurveyBarContainer = connect(null, {
  closeSurvey,
  takeSurvey,
})(SurveyBar);
