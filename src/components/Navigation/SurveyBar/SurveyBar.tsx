import React from 'react';
import {Link} from 'react-router-dom';
import {ISurveyBarDispatchProps} from './SurveyBarContainer';

export const SurveyBar: React.FunctionComponent<ISurveyBarDispatchProps> = ({closeSurvey, takeSurvey}) => {
  return (
    <aside className="bg-yellow pt2" data-cy="surveybar">
      <div className="ph3-ns pv0 center">
        <div className="flex-ns justify-between pv0 ph3 items-center">
          <p className="f6 ma0 pv0 pr3 fw4 black v-mid lh-copy">
            Let us know how we're doing so we can improve your future visits.
          </p>
          <div className="flex">
            <a
              href=""
              data-cy="surveybar-close-button"
              className="f6 link br2 ba ph3 pv2 mb2 dib purple mr2 bg-white"
              onClick={(evt) => {
                evt.preventDefault();
                closeSurvey();
              }}
            >
              Close
            </a>

            <Link
              to="/survey"
              onClick={takeSurvey}
              className="f6 link br2 ph3 pv2 mb2 dib white bg-purple"
              data-cy="surveybar-take-survey-button"
            >
              Take survey
            </Link>
          </div>
        </div>
      </div>
    </aside>
  );
};
