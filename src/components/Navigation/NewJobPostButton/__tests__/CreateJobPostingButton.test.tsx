import {shallow} from 'enzyme';
import React from 'react';
import {CreateJobPostingButton} from '../CreateJobPostingButton';
import * as randomUtil from '~/util/generateRandomString';

jest.spyOn(randomUtil, 'generateRandomString').mockImplementation(() => 'randomString');

describe('CreateJobPostingButton', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<CreateJobPostingButton />);
    expect(wrapper).toMatchSnapshot();
  });
});
