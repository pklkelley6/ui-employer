import React from 'react';
import {Link} from 'react-router-dom';
import {generateRandomString} from '~/util/generateRandomString';

interface ICreateJobPostingLinkProps {
  className?: string;
  dataCy?: string;
}

export const CreateJobPostingLink: React.FunctionComponent<ICreateJobPostingLinkProps> = ({
  className = 'blue db mt2 fw6',
  dataCy = 'create-job-posting-link',
}) => {
  return (
    <Link
      to={{
        pathname: '/jobs/new',
        state: {
          key: generateRandomString(),
        },
      }}
      className={className}
      data-cy={dataCy}
    >
      Create Job Posting
    </Link>
  );
};
