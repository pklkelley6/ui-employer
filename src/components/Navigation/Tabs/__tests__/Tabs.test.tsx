import {mount} from 'enzyme';
import React from 'react';
import {Tab, Tabs, TabsUse} from '~/components/Navigation/Tabs/';

describe('Components/Tabs', () => {
  const props = {
    children: [
      <Tab tabId={1} title="1" key="1">
        1
      </Tab>,
      <Tab tabId={2} title="2" key="2">
        2
      </Tab>,
    ],
    onTabSelect: jest.fn(),
    selectedTab: 1,
  };

  it('should render with default styles when use prop is not available', () => {
    const wrapper = mount(<Tabs {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with styles corresponding to the use prop', () => {
    const inputProps = {
      ...props,
      use: TabsUse.PANEL,
    };
    const wrapper = mount(<Tabs {...inputProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should call onTabSelect with tab ID once clicked on tab', () => {
    const component = mount(<Tabs {...props} />);

    component.find('#tab-2').simulate('click');
    expect(props.onTabSelect).toBeCalledWith(2);
    component.find('#tab-1').simulate('click');
    expect(props.onTabSelect).toBeCalledWith(1);
  });

  it('should not render hidden Tab', () => {
    const propsWithHiddenTab = {
      ...props,
      children: [
        ...props.children,
        <Tab tabId={3} title="3" key="3" hidden={true}>
          3
        </Tab>,
      ],
    };
    const wrapper = mount(<Tabs {...propsWithHiddenTab} />);
    expect(wrapper.find('#tab-3')).toHaveLength(0);
    expect(wrapper).toMatchSnapshot();
  });
});
