import {find, isFinite} from 'lodash/fp';
import React from 'react';
import {ITab} from './Tab';

interface ITabsStyles {
  nav: string;
  title: string;
  activeTitle: string;
  inactiveTitle: string;
}

export enum TabsUse {
  PANEL,
}

const getTabsStyle = (tabsStyleName?: TabsUse): ITabsStyles => {
  const DEFAULT_STYLE: ITabsStyles = {
    activeTitle: 'black-70 bb b--black-70',
    inactiveTitle: 'blue pointer dim',
    nav: 'center b--black-10 mt2',
    title: 'f4 dib ph4 pt4 pb3 no-underline fw6 cursor-default bw1 ',
  };

  switch (tabsStyleName) {
    case TabsUse.PANEL:
      return {
        activeTitle: 'bw1 b--black-50 z-1 black-70 flex items-center ',
        inactiveTitle: 'blue pointer dim flex items-center b--transparent',
        nav: 'flex justify-around items-stretch db bb b--light-gray pt1',
        title: `f5-5 fw6 lh-solid dib pv3 ph2 ph2-m ph3-l fw4 cursor-default tc bb`,
      };
    default:
      return DEFAULT_STYLE;
  }
};

export interface ITabsProps<T> {
  selectedTab: T;
  children: Array<React.ReactElement<ITab<T>>>;
  onTabSelect: (index: T) => any;
  use?: TabsUse;
}

export class Tabs<T> extends React.Component<ITabsProps<T>> {
  public render() {
    return (
      <>
        {this.renderTitles()}
        {this.renderContent()}
      </>
    );
  }

  private renderTitles() {
    const tabsStyle = getTabsStyle(this.props.use);
    const titles = this.props.children.map((child: React.ReactElement<ITab<T>>) => {
      const activeClass =
        this.props.selectedTab === child.props.tabId ? tabsStyle.activeTitle : tabsStyle.inactiveTitle;
      return !child.props.hidden ? (
        <div
          id={`tab-${child.props.tabId}`}
          key={`${child.props.tabId}`}
          className={`${tabsStyle.title} ${activeClass}`}
          title={child.props.title}
          onClick={(event) => {
            event.preventDefault();
            this.props.onTabSelect(child.props.tabId);
          }}
        >
          <span>{child.props.title} </span>
          {isFinite(child.props.total) ? (
            <div className="br-pill bg-black-10 f5 pv1 ph2 dib ml2">{child.props.total} </div>
          ) : null}
        </div>
      ) : null;
    });

    return <nav className={tabsStyle.nav}>{titles}</nav>;
  }

  private renderContent() {
    return find<React.ReactElement<ITab<T>>>(
      (child) => !child.props.hidden && child.props.tabId === this.props.selectedTab,
    )(this.props.children);
  }
}
