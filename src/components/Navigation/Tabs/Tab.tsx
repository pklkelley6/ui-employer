import React from 'react';

export interface ITab<T> {
  tabId: T;
  title: string;
  total?: number;
  hidden?: boolean;
  children: React.ReactNode;
}

export class Tab<T> extends React.Component<ITab<T>> {
  public render() {
    return this.props.children;
  }
}
