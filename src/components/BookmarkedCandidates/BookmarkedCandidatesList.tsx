import React from 'react';
import {getElapsedDays, formatISODate} from '~/util/date';
import {HiddenCandidateListItem} from '~/components/Candidates/HiddenCandidateListItem';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {CandidateType as CandidateLabelType} from '~/components/CandidateLabel/CandidateLabel';
import {getApplicationStatusWithShortlisted} from '~/util/getApplicationStatusWithShortlisted';
import {
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
} from '~/graphql/__generated__/types';
import {isApplication, isTalent, isHiddenTalent} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';
import {IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';

interface IBookmarkedCandidateListProps {
  bookmarkedCandidates: GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates'];
  selectedCandidate?: GetBookmarkedCandidatesBookmarkedCandidates;
  invitedTalents: IEmailInvitation[];
  onCandidateClick: (candidate: GetBookmarkedCandidatesBookmarkedCandidates, positionIndex: number) => void;
  onResumeClick: (candidate: GetBookmarkedCandidatesBookmarkedCandidates) => void;
}

export const BookmarkedCandidateList: React.FunctionComponent<IBookmarkedCandidateListProps> = ({
  bookmarkedCandidates,
  selectedCandidate,
  invitedTalents,
  onCandidateClick,
  onResumeClick,
}) => {
  return (
    <ul className="ma0 pl3 pr1 list" data-cy="bookmarked-candidate-list">
      {bookmarkedCandidates?.map((bookmarkedCandidate, index) => {
        const key = bookmarkedCandidate?.id ?? index;
        if (!bookmarkedCandidate) {
          return <MissingCandidate key={key} candidateType={'candidate'} />;
        }
        const isSelected = selectedCandidate ? bookmarkedCandidate.id === selectedCandidate.id : false;
        if (isApplication(bookmarkedCandidate)) {
          const {applicant, bookmarkedOn, statusId, isShortlisted, createdOn} = bookmarkedCandidate;
          const isInvitedTalent = invitedTalents.some((invitedTalent) => invitedTalent.individualId === applicant.id);
          const status = getApplicationStatusWithShortlisted(statusId, isShortlisted);
          return (
            <div data-cy="bookmarked-application-item" key={key}>
              <ErrorBoundary fallback={<MissingCandidate candidateType={'application'} />}>
                <li>
                  <CandidatesListItem
                    selected={isSelected}
                    isViewed={false}
                    isBookmarked={!!bookmarkedOn}
                    candidate={applicant}
                    candidateType={CandidateLabelType.Applicant}
                    date={`Applied on ${formatISODate(createdOn)}`}
                    onClick={() => onCandidateClick(bookmarkedCandidate, index)}
                    onResumeClick={() => onResumeClick(bookmarkedCandidate)}
                    applicationStatus={status}
                    isInvitedTalent={isInvitedTalent}
                  />
                </li>
              </ErrorBoundary>
            </div>
          );
        } else if (isTalent(bookmarkedCandidate)) {
          const {talent, bookmarkedOn} = bookmarkedCandidate;
          return (
            <div data-cy="bookmarked-talent-item" key={key}>
              <ErrorBoundary fallback={<MissingCandidate candidateType={'suggested talent'} />}>
                <li>
                  <CandidatesListItem
                    selected={isSelected}
                    isViewed={false}
                    isBookmarked={!!bookmarkedOn}
                    candidate={talent}
                    candidateType={CandidateLabelType.SuggestedTalent}
                    date={`Active ${getElapsedDays(talent.lastLogin)}`}
                    onClick={() => onCandidateClick(bookmarkedCandidate, index)}
                    onResumeClick={() => onResumeClick(bookmarkedCandidate)}
                  />
                </li>
              </ErrorBoundary>
            </div>
          );
        } else if (isHiddenTalent(bookmarkedCandidate)) {
          const {talent, bookmarkedOn} = bookmarkedCandidate;
          return (
            <li>
              <HiddenCandidateListItem
                selected={isSelected}
                isViewed={false}
                isBookmarked={!!bookmarkedOn}
                candidate={talent}
                candidateType={CandidateLabelType.SuggestedTalent}
                onClick={() => onCandidateClick(bookmarkedCandidate, index)}
              />
            </li>
          );
        } else {
          return <MissingCandidate key={key} candidateType={'candidate'} />;
        }
      })}
    </ul>
  );
};
