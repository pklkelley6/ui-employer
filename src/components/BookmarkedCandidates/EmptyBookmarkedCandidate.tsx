import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const EmptyBookmarkedCandidate: React.FunctionComponent = () => (
  <EmptyState className="ph5 black-30">
    <h1 data-cy="empty-bookmarked-candidate" className="f4 ph3">
      Pick a candidate from the left to view profile details.
    </h1>
  </EmptyState>
);
