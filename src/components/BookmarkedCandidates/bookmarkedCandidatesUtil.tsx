import {
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesApplicationInlineFragment,
  GetBookmarkedCandidatesTalentInlineFragment,
  GetBookmarkedCandidatesHiddenTalentInlineFragment,
} from '~/graphql/__generated__/types';

export const isApplication = (
  candidate: GetBookmarkedCandidatesBookmarkedCandidates,
): candidate is GetBookmarkedCandidatesApplicationInlineFragment => candidate.__typename === 'Application';

export const isTalent = (
  candidate: GetBookmarkedCandidatesBookmarkedCandidates,
): candidate is GetBookmarkedCandidatesTalentInlineFragment => candidate.__typename === 'Talent';
export const isHiddenTalent = (
  candidate: GetBookmarkedCandidatesBookmarkedCandidates,
): candidate is GetBookmarkedCandidatesHiddenTalentInlineFragment => candidate.__typename === 'HiddenTalent';
