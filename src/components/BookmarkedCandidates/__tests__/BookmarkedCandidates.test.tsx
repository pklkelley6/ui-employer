import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import {mount, ReactWrapper} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {print} from 'graphql';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {BookmarkedCandidates, BOOKMARKED_CANDIDATES_LIMIT} from '../BookmarkedCandidates';
import {BookmarkedCandidatesInfo} from '../BookmarkedCandidatesInfo';
import {
  bookmarkedApplicationMock,
  bookmarkedTalentMock,
} from '~/__mocks__/bookmarkedCandidates/bookmarkedCandidates.mock';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {Pagination} from '~/components/Navigation/Pagination';
import {cleanSnapshot, nextTick} from '~/testUtil/enzyme';
import {GET_BOOKMARKED_CANDIDATES} from '~/graphql/candidates/bookmarkedCandidates.query';
import introspectionResult, {GetBookmarkedCandidatesQuery, CandidateType} from '~/graphql/__generated__/types';
import * as dateUtil from '~/util/date';
import {ApplicationStatus} from '~/components/CandidateInfo/ApplicationStatus';
import {EmptyBookmarkedCandidatesList} from '~/components/BookmarkedCandidates/EmptyBookmarkedCandidatesList';
import {Dropdown} from '~/components/Core/Dropdown';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import * as getEmailInvitations from '~/services/suggestedTalents/getEmailInvitations';
import {IAppState} from '~/flux';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

jest.spyOn(dateUtil, 'getElapsedDays').mockImplementation(() => '3 days ago');
jest.spyOn(getEmailInvitations, 'getEmailInvitations').mockImplementation(() => Promise.resolve([]));

const pactJobUuid = 'R90SS0001A_1200000_(0)';

let pactBuilder: PactBuilder;

describe('BookmarkedCandidates', () => {
  it('should render loader then display both bookmarked applications and talents', async () => {
    const wrapper = mountBookmarkedCandidates();

    expect(wrapper.find(CardLoader)).toHaveLength(1);

    await nextTick(wrapper);

    const candidateListItems = wrapper.find(CandidatesListItem);
    expect(candidateListItems).toHaveLength(3);
    expect(toJson(candidateListItems, cleanSnapshot())).toMatchSnapshot();

    const bookmarkedApplications = wrapper.find('[data-cy="bookmarked-application-item"]');
    expect(bookmarkedApplications).toHaveLength(2);
    bookmarkedApplications.first().find('[data-cy="candidate-info"]').simulate('click');
    const bookmarkedApplicationInfo = wrapper.find(BookmarkedCandidatesInfo);
    expect(toJson(bookmarkedApplicationInfo, cleanSnapshot())).toMatchSnapshot();
    expect(bookmarkedApplicationInfo.find(ApplicationStatus)).toHaveLength(1);

    const bookmarkedTalents = wrapper.find('[data-cy="bookmarked-talent-item"]');
    expect(bookmarkedTalents).toHaveLength(1);
    bookmarkedTalents.first().find('[data-cy="candidate-info"]').simulate('click');
    const bookmarkedTalentInfo = wrapper.find(BookmarkedCandidatesInfo);
    expect(toJson(bookmarkedTalentInfo, cleanSnapshot())).toMatchSnapshot();
    expect(bookmarkedTalentInfo.find(ApplicationStatus)).toHaveLength(0);
  });

  it('should render CandidatesFetchError when a network error is received', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        error: new Error(),
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);
    expect(wrapper.find(TemporarilyUnavailable)).toHaveLength(1);
  });

  it('should render EmptyBookmarkedCandidatesList when there are zero candidates', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        result: {
          data: {
            __typename: 'BookmarkedCandidates',
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [],
              total: 0,
              bookmark: 'pageOneBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper);
    expect(wrapper.find(EmptyBookmarkedCandidatesList)).toHaveLength(1);
  });

  it('should fetch candidates and navigate to another page when pagination is clicked', async () => {
    const queryMocks = [
      ...defaultQueryMocks,
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: {
            jobId: jobIdMock,
            limit: BOOKMARKED_CANDIDATES_LIMIT,
            skip: BOOKMARKED_CANDIDATES_LIMIT,
            bookmark: 'pageOneBookmark',
          },
        },
        result: {
          data: {
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [bookmarkedApplicationMock, bookmarkedTalentMock],
              total: BOOKMARKED_CANDIDATES_LIMIT * 2,
              bookmark: 'pageTwoBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper, 100);

    expect(wrapper.find(CandidatesListItem)).toHaveLength(3);

    const pagination = wrapper.find(Pagination);
    expect(pagination.find('.bg-secondary').text()).toEqual('1');
    const secondPage = pagination.findWhere((node) => node.type() === 'span' && node.text() === '2');
    secondPage.simulate('click');

    await nextTick(wrapper);

    const pageChange = wrapper.find(Pagination);
    expect(pageChange.find('.bg-secondary').text()).toEqual('2');

    wrapper.update();
    expect(wrapper.find(CandidatesListItem)).toHaveLength(2);
  });

  it('should render errors for null values and display the rest', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_BOOKMARKED_CANDIDATES,
          variables: baseVariables,
        },
        result: {
          data: {
            bookmarkedCandidatesForJob: {
              __typename: 'BookmarkedCandidates',
              bookmarkedCandidates: [bookmarkedApplicationMock, bookmarkedTalentMock, null, null],
              total: BOOKMARKED_CANDIDATES_LIMIT * 2,
              bookmark: 'pageOneBookmark',
            },
          },
        },
      },
    ];
    const wrapper = mountBookmarkedCandidates(queryMocks);
    await nextTick(wrapper, 100);

    const missingCandidates = wrapper.find(MissingCandidate).find('[candidateType="candidate"]');
    expect(missingCandidates).toHaveLength(2);
  });

  describe('BookmarkedCandidates with BookmarkedCandidatesToolbar', () => {
    beforeAll(async () => {
      pactBuilder = new PactBuilder('api-profile');
      await pactBuilder.setup();
    });

    afterAll(async () => pactBuilder.provider.finalize());

    it('should verify interactions based on dropdown options change', async () => {
      await addGetBookmarkedCandidatesInteraction();
      const wrapper = mountBookmarkedCandidatesWithApolloProvider();

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(wrapper.find(CandidatesListItem)).toHaveLength(1);

      await addGetBookmarkedCandidatesInteractionWithApplicants();

      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange(CandidateType.Applicant);
      });

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(wrapper.find(CandidatesListItem)).toHaveLength(1);

      await addGetBookmarkedCandidatesInteractionWithSuggestedTalents();

      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange(CandidateType.Talent);
      });

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(wrapper.find(CandidatesListItem)).toHaveLength(0);
    });
  });
});

const jobIdMock = 'job123';
const baseVariables = {
  jobId: jobIdMock,
  limit: BOOKMARKED_CANDIDATES_LIMIT,
  skip: 0,
};

const defaultQueryMockResult: {data: GetBookmarkedCandidatesQuery} = {
  data: {
    bookmarkedCandidatesForJob: {
      __typename: 'BookmarkedCandidates',
      bookmarkedCandidates: [
        bookmarkedApplicationMock,
        bookmarkedTalentMock,
        {...bookmarkedApplicationMock, id: 'BMA-2', applicant: {...bookmarkedApplicationMock.applicant, id: 'A-2'}},
      ],
      total: BOOKMARKED_CANDIDATES_LIMIT * 2,
      bookmark: 'pageOneBookmark',
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_BOOKMARKED_CANDIDATES,
      variables: baseVariables,
    },
    result: defaultQueryMockResult,
  },
];

const reduxState: {
  features: Partial<IAppState['features']>;
  releaseToggles: Partial<IAppState['releaseToggles']>;
} = {
  features: {screeningQuestions: true, enhancedSkillPills: true},
  releaseToggles: {'179899438enhancedSkillPills': true},
};

const store = configureStore()(reduxState);
const dispatchProps = {
  onBookmarkedCandidateClicked: jest.fn(),
  onBookmarkedCandidatesPageClicked: jest.fn(),
  onBookmarkedCandidateResumeClicked: jest.fn(),
};

const mountBookmarkedCandidates = (queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks): ReactWrapper => {
  const fragmentMatcher = new IntrospectionFragmentMatcher({introspectionQueryResultData: introspectionResult});
  const cache = new InMemoryCache({fragmentMatcher});
  return mount(
    <MockedProvider mocks={queryMocks} cache={cache} addTypename={false}>
      <Provider store={store}>
        <BookmarkedCandidates {...dispatchProps} jobUuid={jobIdMock} shouldDisplayContactButton={false} />
      </Provider>
    </MockedProvider>,
  );
};

const mountBookmarkedCandidatesWithApolloProvider = () => {
  return mount(
    <Provider store={store}>
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <BookmarkedCandidates {...dispatchProps} jobUuid={pactJobUuid} shouldDisplayContactButton={false} />
      </ApolloProvider>
    </Provider>,
  );
};

const addGetBookmarkedCandidatesInteraction = async () => {
  await addInteraction('Get Bookmarked Candidates', 1, [
    transformArrayToEachLikeMatcher({...bookmarkedApplicationMock, id: 'appl-1'}),
  ]);
};

const addGetBookmarkedCandidatesInteractionWithApplicants = async () => {
  await addInteraction(
    'Get Bookmarked Candidates with Applicant',
    1,
    [transformArrayToEachLikeMatcher({...bookmarkedApplicationMock, id: 'appl-1'})],
    CandidateType.Applicant,
  );
};

const addGetBookmarkedCandidatesInteractionWithSuggestedTalents = async () => {
  await addInteraction('Get Bookmarked Candidates with Suggested Talents', 1, [], CandidateType.Talent);
};

const addInteraction = async (
  description: string,
  total: number,
  listOfCandidates: any[],
  candidateType?: CandidateType,
) => {
  const body = getBookmarkedCandidatesResponseMock(total, listOfCandidates);
  const interaction = new ApolloGraphQLInteraction()
    .uponReceiving(description)
    .withQuery(print(GET_BOOKMARKED_CANDIDATES))
    .withOperation('getBookmarkedCandidates')
    .withRequest({
      headers: {
        'content-type': 'application/json',
        [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
      },
      method: 'POST',
      path: '/profile',
    })
    .withVariables({
      jobId: pactJobUuid,
      limit: BOOKMARKED_CANDIDATES_LIMIT,
      skip: 0,
      ...(candidateType ? {candidateType} : {}),
    })
    .willRespondWith({
      body,
      status: 200,
    });
  await pactBuilder.provider.addInteraction(interaction);
};

const getBookmarkedCandidatesResponseMock = (total: number, candidates: any[]) => {
  return {
    data: {
      bookmarkedCandidatesForJob: {
        __typename: 'BookmarkedCandidates',
        total: total,
        bookmark: Matchers.like('3bde0528a2ec6c6fc484b9ccaa37554f0b90d1ed2bc0b2990740d8e7582865d5'),
        bookmarkedCandidates: Matchers.like(candidates),
      },
    },
  };
};
