import React from 'react';
import {EmptyState} from '~/components/Layouts/EmptyState';

export const EmptyBookmarkedCandidatesList: React.FunctionComponent = () => (
  <EmptyState className="black-30">
    <h1 className="f3">There are no saved candidates for this job yet.</h1>
  </EmptyState>
);
