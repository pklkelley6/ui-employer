import {isEmpty} from 'lodash/fp';
import React, {useState, useEffect} from 'react';
import {useQuery} from 'react-apollo';
import {useAsync} from 'react-use';
import {IJobPost} from 'src/services/employer/jobs.types';
import {BookmarkedCandidatesToolbar} from '../BookmarkedCandidatesToolbar/BookmarkedCandidatesToolbar';
import {ApplicationRejectionReason} from '../ApplicationRejectionReason/ApplicationRejectionReason';
import {IBookmarkedCandidatesContainerDispatchProps} from './BookmarkedCandidatesContainer';
import {GET_BOOKMARKED_CANDIDATES} from '~/graphql/candidates/bookmarkedCandidates.query';
import {Pagination} from '~/components/Navigation/Pagination';
import {
  GetBookmarkedCandidatesQuery,
  GetBookmarkedCandidatesQueryVariables,
  GetBookmarkedCandidatesBookmarkedCandidates,
  GetBookmarkedCandidatesBookmarkedCandidatesForJob,
  CandidateType,
  Maybe,
} from '~/graphql/__generated__/types';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {TemporarilyUnavailable, temporaryUnavailableMsg} from '~/components/Layouts/TemporarilyUnavailable';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {EmptyBookmarkedCandidatesList} from '~/components/BookmarkedCandidates/EmptyBookmarkedCandidatesList';
import {BookmarkedCandidateList} from '~/components/BookmarkedCandidates/BookmarkedCandidatesList';
import {BookmarkedCandidatesInfoContainer} from '~/components/BookmarkedCandidates/BookmarkedCandidatesInfoContainer';
import {isApplication} from '~/components/BookmarkedCandidates/bookmarkedCandidatesUtil';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {Dropdown} from '~/components/Core/Dropdown';
import {useDownloadResumeMutation} from '~/services/applications/useDownloadResumeMutation';
import {isSelectedApplicationStatusUnsuccessful} from '~/util/isSelectedApplicationStatusUnsuccessful';
import {getEmailInvitations, IEmailInvitation} from '~/services/suggestedTalents/getEmailInvitations';
import {ContactSuggestedTalentContainer} from '~/components/CandidateInfo/ContactSuggestedTalentContainer';

export const BOOKMARKED_CANDIDATES_LIMIT = 20;

export const FILTER_BY = [
  {
    label: 'Show all',
    value: 'all',
  },
  {
    label: 'Show applicants only',
    value: CandidateType.Applicant,
  },
  {
    label: 'Show talents only',
    value: CandidateType.Talent,
  },
];

interface IBookmarkedCandidatesProps extends IBookmarkedCandidatesContainerDispatchProps {
  job?: IJobPost;
  jobUuid: string;
  shouldDisplayContactButton: boolean;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export const BookmarkedCandidates: React.FunctionComponent<IBookmarkedCandidatesProps> = ({
  job,
  jobUuid,
  shouldDisplayContactButton,
  refetchBookmarkedCandidatesTotal,
  onBookmarkedCandidateClicked,
  onBookmarkedCandidatesPageClicked,
  onBookmarkedCandidateResumeClicked,
}) => {
  const [bookmarkedCandidates, setBookmarkedCandidates] = useState<
    GetBookmarkedCandidatesBookmarkedCandidatesForJob['bookmarkedCandidates']
  >([]);
  const [selectedCandidate, setSelectedCandidate] = useState<GetBookmarkedCandidatesBookmarkedCandidates>();
  const [bookmark, setBookmark] = useState<string>();
  const [page, setPage] = useState(0);
  const [matchCriteria, setMatchCriteria] = useState<Maybe<CandidateType>>(null);
  const [invitedTalents, setInvitedTalents] = useState<IEmailInvitation[]>([]);

  const {
    loading,
    error,
    data,
    refetch: refetchApplications,
  } = useQuery<GetBookmarkedCandidatesQuery, GetBookmarkedCandidatesQueryVariables>(GET_BOOKMARKED_CANDIDATES, {
    variables: {
      jobId: jobUuid,
      limit: BOOKMARKED_CANDIDATES_LIMIT,
      skip: page * BOOKMARKED_CANDIDATES_LIMIT,
      bookmark,
      ...(matchCriteria ? {candidateType: matchCriteria} : {}),
    },
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
    notifyOnNetworkStatusChange: true,
    skip: !jobUuid,
  });

  // effect for when job posting number of screening questions changed e.g. removal of screening questions
  useEffect(() => {
    if (data) {
      refetchApplications();
    }
  }, [(job?.screeningQuestions ?? []).length]);

  useEffect(() => {
    setBookmarkedCandidates(data?.bookmarkedCandidatesForJob?.bookmarkedCandidates ?? []);
    if (selectedCandidate) {
      const updatedCandidate = (data?.bookmarkedCandidatesForJob?.bookmarkedCandidates ?? []).find(
        (candidate) => candidate?.id == selectedCandidate.id,
      );
      if (updatedCandidate) {
        setSelectedCandidate(updatedCandidate);
      }
    }
  }, [JSON.stringify(data)]);

  useEffect(() => {
    setSelectedCandidate(undefined);
  }, [loading]);

  useEffect(() => {
    setBookmarkedCandidates(
      bookmarkedCandidates.map((bookmarkedCandidate) =>
        bookmarkedCandidate?.id === selectedCandidate?.id ? selectedCandidate : bookmarkedCandidate,
      ),
    );
  }, [JSON.stringify(selectedCandidate)]);

  const setResumeLastDownloaded = useDownloadResumeMutation();

  const onResumeClick = async (candidate: GetBookmarkedCandidatesBookmarkedCandidates) => {
    if (isApplication(candidate)) {
      await setResumeLastDownloaded(candidate);
    }
  };

  const rejectionComponent =
    selectedCandidate?.__typename === 'Application' &&
    isSelectedApplicationStatusUnsuccessful(bookmarkedCandidates, selectedCandidate) ? (
      <ApplicationRejectionReason
        applicationId={selectedCandidate.id}
        applicationRejectReason={selectedCandidate.rejectionReason ?? undefined}
        othersRejectReason={selectedCandidate.rejectionReasonExtended ?? undefined}
      />
    ) : undefined;

  const contactSuggestedTalent = shouldDisplayContactButton ? (
    <ContactSuggestedTalentContainer jobUuid={jobUuid} individualId={selectedCandidate?.id || ''} />
  ) : undefined;

  useAsync(async () => {
    const result = await getEmailInvitations(jobUuid);
    setInvitedTalents(result.filter((data) => data.isEmailSent === true));
  }, []);

  const bookmarkedCandidatesPanesProps = {
    left: (resetRightScroll: () => void) => (
      <>
        <BookmarkedCandidateList
          selectedCandidate={selectedCandidate}
          bookmarkedCandidates={bookmarkedCandidates}
          onCandidateClick={(candidate, positionIndex) => {
            setSelectedCandidate(candidate);
            onBookmarkedCandidateClicked(candidate, page * BOOKMARKED_CANDIDATES_LIMIT + positionIndex + 1, jobUuid);
            resetRightScroll();
          }}
          onResumeClick={(candidate) => {
            onResumeClick(candidate);
            onBookmarkedCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.LIST);
          }}
          invitedTalents={invitedTalents}
        />
        <Pagination
          currentPage={page}
          totalItemsLength={data?.bookmarkedCandidatesForJob?.total ?? 0}
          itemsPerPage={BOOKMARKED_CANDIDATES_LIMIT}
          maxVisiblePageButtons={5}
          onPageChange={(newPage) => {
            setPage(newPage);
            // set bookmark along with page to prevent extra fetching due to bookmark change
            setBookmark(data?.bookmarkedCandidatesForJob?.bookmark);
            onBookmarkedCandidatesPageClicked(jobUuid, newPage + 1);
          }}
        />
      </>
    ),
    right: (
      <BookmarkedCandidatesInfoContainer
        job={job}
        selectedCandidate={selectedCandidate}
        setSelectedCandidate={setSelectedCandidate}
        bookmarkedCandidates={bookmarkedCandidates}
        setBookmarkedCandidates={setBookmarkedCandidates}
        refetchBookmarkedCandidatesTotal={refetchBookmarkedCandidatesTotal}
        onResumeClick={(candidate) => {
          onResumeClick(candidate);
          onBookmarkedCandidateResumeClicked(DOWNLOAD_RESUME_GA_LABEL.DETAIL);
        }}
        jobId={jobUuid}
        rejectionComponent={rejectionComponent}
        contactSuggestedTalent={contactSuggestedTalent}
        invitedTalents={invitedTalents}
      />
    ),
  };

  const renderPanes = () => {
    if (loading) {
      return <CardLoader className="pa3 w-50 o-70" cardNumber={8} boxNumber={1} />;
    } else if (error?.networkError) {
      return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
    } else if (isEmpty(bookmarkedCandidates)) {
      return <EmptyBookmarkedCandidatesList />;
    }

    return <CandidatesPanes {...bookmarkedCandidatesPanesProps} />;
  };

  return (
    <>
      <BookmarkedCandidatesToolbar
        filterSlot={
          <Dropdown
            innerProps={{
              'data-cy': 'filter-slot-dropdown-saved-tab',
            }}
            data={FILTER_BY}
            input={{
              onChange: (value) => {
                const filterBy = value === 'all' ? null : value;
                setMatchCriteria(filterBy as Maybe<CandidateType>);
              },
              value: matchCriteria ?? 'all',
            }}
          />
        }
      />

      {renderPanes()}
    </>
  );
};
