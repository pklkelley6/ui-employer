import {connect} from 'react-redux';
import {BookmarkedCandidatesInfo} from './BookmarkedCandidatesInfo';
import {onBookmarkedCandidateTabClicked} from '~/flux/analytics';

export interface IBookmarkedCandidatesInfoContainerDispatchProps {
  onBookmarkedCandidateTabClicked: typeof onBookmarkedCandidateTabClicked;
}

export const BookmarkedCandidatesInfoContainer = connect<{}, IBookmarkedCandidatesInfoContainerDispatchProps>(null, {
  onBookmarkedCandidateTabClicked,
})(BookmarkedCandidatesInfo);
