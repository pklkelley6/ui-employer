import {eq} from 'lodash/fp';
import React, {Component} from 'react';
import {Cross} from './Cross';
import styles from './SearchBox.scss';
import {KeyCodes} from '~/util/keyCodes';

export enum SearchBoxType {
  Talent,
  Job,
}

export interface ISearchBoxProps {
  initialValue?: string;
  placeholder?: string;
  clearable?: boolean;
  onSearch: (value: string) => any;
  searchBoxType?: SearchBoxType;
  opts?: Record<string, any>;
}

interface ISearchBoxState {
  previousInitialValue?: string;
  value: string;
}

export class SearchBox extends Component<ISearchBoxProps, ISearchBoxState> {
  public static getDerivedStateFromProps(props: ISearchBoxProps, state: ISearchBoxState) {
    if (props.initialValue !== state.previousInitialValue) {
      return {
        previousInitialValue: props.initialValue,
        value: props.initialValue || '',
      };
    }

    return null;
  }

  public state = {
    value: '',
  };

  public handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({value: event.currentTarget.value});
  };

  public handleSubmit = () => {
    this.props.onSearch(this.state.value);
  };

  public clearValue = () => {
    this.setState({value: ''});
  };

  public onKeyPress = (event: React.KeyboardEvent) => {
    if (eq(event.which, KeyCodes.Enter)) {
      this.props.onSearch(this.state.value);
    }
  };

  public render() {
    const {placeholder, clearable, searchBoxType, opts = {}} = this.props;

    const button = () => {
      switch (searchBoxType) {
        case SearchBoxType.Talent:
          return (
            <input
              data-cy="search-button"
              type="button"
              value="Search"
              onClick={this.handleSubmit}
              className={`${styles.button} white button-reset bg-primary`}
            />
          );
        default:
          return (
            <input
              data-cy="search-button"
              type="button"
              onClick={this.handleSubmit}
              className={`${styles.button} ${styles.job} button-reset`}
            />
          );
      }
    };

    return (
      <div className={`${styles.searchBox} search-box z-1 flex`}>
        <div className="search-input-container relative f4-5 w-100">
          {clearable && this.state.value ? <Cross onClick={this.clearValue} /> : null}
          <input
            data-cy="search-input"
            type="text"
            placeholder={placeholder}
            value={this.state.value}
            onChange={this.handleChange}
            onKeyPress={this.onKeyPress}
            className={`${styles.searchInput} input-reset w-100 ph2 black-80 ba b--near-black`}
            {...opts}
          />
        </div>

        {button()}
      </div>
    );
  }
}
