import {mount} from 'enzyme';
import React from 'react';
import {Cross} from '~/components/SearchBox/Cross';

describe('Cross', () => {
  it('should render Cross', () => {
    const onClickMock = jest.fn();
    const crossProps = {
      onClick: onClickMock,
    };
    const wrapper = mount(<Cross {...crossProps} />);
    expect(wrapper).toMatchSnapshot();
  });
});
