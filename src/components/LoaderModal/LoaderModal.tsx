import React from 'react';
import {Modal} from '../Core/Modal';
import {NumberLoader} from '~/components/JobList/NumberLoader';

export const LoaderModal: React.FC = () => (
  <Modal data-cy="loader-modal">
    <div className="bg-white tc ph5 pv4">
      <NumberLoader />
    </div>
  </Modal>
);
