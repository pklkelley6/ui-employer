import React from 'react';
import styles from './UnviewedBadge.scss';

export interface IUnviewedBadgeProps {
  count: number;
}

export const UnviewedBadge: React.FunctionComponent<IUnviewedBadgeProps> = ({count}) => {
  return (
    <span
      data-cy="unviewed-badge"
      className={`f7 bg-light-red br-pill b--red white absolute ${styles.notificationDot}`}
    >
      {count}
    </span>
  );
};
