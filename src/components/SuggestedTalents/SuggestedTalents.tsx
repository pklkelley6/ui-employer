import {isEmpty, isNil} from 'lodash/fp';
import React, {useState, useEffect} from 'react';
import {useQuery, useMutation} from 'react-apollo';
import {formatISO} from 'date-fns';
import {useSelector} from 'react-redux';
import {growlNotification, GrowlType} from '../GrowlNotification/GrowlNotification';
import {ISuggestedTalentsProps} from './SuggestedTalentsContainer';
import {CandidateInfo} from '~/components/CandidateInfo/CandidateInfo';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {CandidatesPanes} from '~/components/Candidates/CandidatesPanes';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {
  noLongerAvailableMsg,
  TemporarilyUnavailable,
  temporaryUnavailableMsg,
} from '~/components/Layouts/TemporarilyUnavailable';
import {Pagination} from '~/components/Navigation/Pagination';
import {SuggestedTalentsOnboarding} from '~/components/Onboarding/SuggestedTalentsOnboarding';
import {EmptySuggestedTalent, EmptySuggestedTalentList, SuggestedTalentList} from '~/components/SuggestedTalents';
import {
  GetSuggestedTalentsQuery,
  GetSuggestedTalentsQueryVariables,
  GetSuggestedTalentsTalents,
  GetSuggestedTalentsSuggestedTalentsForJob,
  MutationSetSuggestedTalentBookmarkArgs,
  Mutation,
} from '~/graphql/__generated__/types';
import {
  GET_SUGGESTED_TALENTS,
  SUGGESTED_TALENTS_LIMIT,
  SUGGESTED_TALENTS_TOTAL_LIMIT,
  SET_SUGGESTED_TALENT_BOOKMARK,
} from '~/graphql/suggestedTalents';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {getElapsedDays} from '~/util/date';
import {ContactSuggestedTalentContainer} from '~/components/CandidateInfo/ContactSuggestedTalentContainer';
import {IAppState} from '~/flux';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

export const SuggestedTalents: React.FunctionComponent<ISuggestedTalentsProps> = ({
  fromLastLogin,
  jobUuid,
  shouldDisplayContactButton,
  onSuggestedTalentClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
  onSuggestedTalentCandidateTabClicked,
  refetchBookmarkedCandidatesTotal,
  threshold,
  unavailableSuggestedTalents,
}) => {
  const x0paSuggestedTalentsFeatureToggle = useSelector(({features}: IAppState) => features.x0paSuggestedTalents);
  const [page, setPage] = useState(0);
  const [selectedSuggestedTalent, setSelectedSuggestedTalent] = useState<GetSuggestedTalentsTalents>();
  const [suggestedTalents, setSuggestedTalents] = useState<
    NonNullable<GetSuggestedTalentsSuggestedTalentsForJob['talents']>
  >([]);
  const [suggestedTalentForRevert, setSuggestedTalentForRevert] = useState<GetSuggestedTalentsTalents>();
  const {loading, error, data} = useQuery<GetSuggestedTalentsQuery, GetSuggestedTalentsQueryVariables>(
    GET_SUGGESTED_TALENTS,
    {
      variables: {jobId: jobUuid, limit: SUGGESTED_TALENTS_LIMIT, page: page, threshold, fromLastLogin},
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
      notifyOnNetworkStatusChange: true,
      skip: !jobUuid,
      context: {
        headers: x0paSuggestedTalentsFeatureToggle ? {} : {[API_VERSION_HEADER_KEY]: '2021-05-24'},
      },
    },
  );
  const [setSuggestedTalentBookmark] = useMutation<
    {setSuggestedTalentBookmark: Mutation['setSuggestedTalentBookmark']},
    MutationSetSuggestedTalentBookmarkArgs
  >(SET_SUGGESTED_TALENT_BOOKMARK);
  useEffect(() => {
    setSuggestedTalents(data?.suggestedTalentsForJob?.talents ?? []);
  }, [JSON.stringify(data)]);

  useEffect(() => {
    setSelectedSuggestedTalent(undefined);
  }, [loading]);

  useEffect(() => {
    setSuggestedTalents(
      suggestedTalents.map((suggestedTalent) =>
        suggestedTalent?.talent.id === selectedSuggestedTalent?.talent.id ? selectedSuggestedTalent : suggestedTalent,
      ),
    );
  }, [JSON.stringify(selectedSuggestedTalent)]);

  useEffect(() => {
    setSuggestedTalents(
      suggestedTalents.map((suggestedTalent) =>
        suggestedTalent?.talent.id === suggestedTalentForRevert?.talent.id ? suggestedTalentForRevert : suggestedTalent,
      ),
    );
    if (selectedSuggestedTalent?.talent.id === suggestedTalentForRevert?.talent.id) {
      setSelectedSuggestedTalent(suggestedTalentForRevert);
    }
  }, [JSON.stringify(suggestedTalentForRevert)]);

  const contactSuggestedTalent = shouldDisplayContactButton ? (
    <ContactSuggestedTalentContainer jobUuid={jobUuid} individualId={selectedSuggestedTalent?.talent.id || ''} />
  ) : undefined;

  const onCandidateBookmarkClick = async (checked: boolean) => {
    if (selectedSuggestedTalent) {
      try {
        setSelectedSuggestedTalent({
          ...selectedSuggestedTalent,
          bookmarkedOn: checked ? formatISO(Date.now()) : null,
        });
        await setSuggestedTalentBookmark({
          variables: {individualId: selectedSuggestedTalent.talent.id, jobId: jobUuid, isBookmark: checked},
        });
        if (refetchBookmarkedCandidatesTotal) {
          await refetchBookmarkedCandidatesTotal();
        }
      } catch (_) {
        setSuggestedTalentForRevert(selectedSuggestedTalent);
        growlNotification(
          `Temporarily unable to ${checked ? 'save' : 'unsave'} candidate. Please try again.`,
          GrowlType.ERROR,
          {
            toastId: `setSuggestedTalentBookmark${selectedSuggestedTalent.talent.id}`,
          },
        );
      }
    }
  };

  if (loading) {
    return <CardLoader className="pa3 pt4 w-50" cardNumber={8} />;
  }

  if (unavailableSuggestedTalents) {
    return <TemporarilyUnavailable message={noLongerAvailableMsg} />;
  }

  if (error?.networkError || isNil(data?.suggestedTalentsForJob)) {
    return <TemporarilyUnavailable message={temporaryUnavailableMsg} />;
  }

  const suggestedTalentsTotal = data?.suggestedTalentsForJob.total || 0;
  const suggestedTalentsTotalWithLimit =
    suggestedTalentsTotal > SUGGESTED_TALENTS_TOTAL_LIMIT ? SUGGESTED_TALENTS_TOTAL_LIMIT : suggestedTalentsTotal;

  if (isEmpty(suggestedTalents)) {
    return <EmptySuggestedTalentList />;
  }

  const suggestedTalentsPanesProps = {
    left: (resetRightScroll: () => void) => (
      <>
        <SuggestedTalentList
          onSelectSuggestedTalent={(suggestedTalent, positionIndex) => {
            setSelectedSuggestedTalent(suggestedTalent);
            onSuggestedTalentClicked(
              suggestedTalent.talent.id,
              page * SUGGESTED_TALENTS_LIMIT + positionIndex + 1,
              jobUuid,
            );
            resetRightScroll();
          }}
          selectedSuggestedTalent={selectedSuggestedTalent}
          suggestedTalents={suggestedTalents}
          onResumeClick={(gaLabel) => onSuggestedTalentsResumeClicked(gaLabel)}
        />
        <span className="black-50 i f6 pa3 ma0 dib">
          Suggested talents powered by {x0paSuggestedTalentsFeatureToggle ? 'X0PA AI' : 'WCC'}
        </span>
        <Pagination
          currentPage={page}
          totalItemsLength={suggestedTalentsTotalWithLimit}
          itemsPerPage={SUGGESTED_TALENTS_LIMIT}
          maxVisiblePageButtons={5}
          onPageChange={(page) => {
            setPage(page);
            onSuggestedTalentsPageClicked(jobUuid, page + 1);
          }}
        />
      </>
    ),
    right: selectedSuggestedTalent ? (
      <ErrorBoundary key={selectedSuggestedTalent.talent.id} fallback={<EmptySuggestedTalent />}>
        <CandidateInfo
          key={selectedSuggestedTalent.talent.id}
          candidate={selectedSuggestedTalent.talent}
          candidateType={CandidateType.SuggestedTalent}
          candidateTabClicked={onSuggestedTalentCandidateTabClicked}
          isBookmarked={!!selectedSuggestedTalent.bookmarkedOn}
          formattedDate={`Active ${getElapsedDays(selectedSuggestedTalent.talent.lastLogin)}`}
          onResumeClick={() => onSuggestedTalentsResumeClicked(DOWNLOAD_RESUME_GA_LABEL.DETAIL)}
          onCandidateBookmarkClick={onCandidateBookmarkClick}
          contactSuggestedTalent={contactSuggestedTalent}
        />
      </ErrorBoundary>
    ) : (
      <EmptySuggestedTalent />
    ),
  };

  return (
    <div data-cy="suggested-talents-view">
      <div className="w-100 h2" />
      <div className="tr mr3">
        <SuggestedTalentsOnboarding />
      </div>
      <CandidatesPanes {...suggestedTalentsPanesProps} />
    </div>
  );
};
