import {connect} from 'react-redux';
import {SuggestedTalents} from './SuggestedTalents';
import {
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
} from '~/flux/analytics';
import {IAppState} from '~/flux/index';
import {onSuggestedTalentClicked} from '~/flux/suggestedTalents/suggestedTalents.actions';

export interface ISuggestedtalentsContainerStateProps {
  fromLastLogin: string;
  jobUuid: string;
  shouldDisplayContactButton: boolean;
  threshold: number;
  unavailableSuggestedTalents: boolean;
  refetchBookmarkedCandidatesTotal?: () => Promise<any>;
}

export type ISuggestedTalentsProps = ISuggestedtalentsContainerStateProps & ISuggestedTalentsContainerDispatchProps;

export interface ISuggestedTalentsContainerDispatchProps {
  onSuggestedTalentsPageClicked: typeof onSuggestedTalentsPageClicked;
  onSuggestedTalentsResumeClicked: typeof onSuggestedTalentsResumeClicked;
  onSuggestedTalentClicked: typeof onSuggestedTalentClicked;
  onSuggestedTalentCandidateTabClicked: typeof onSuggestedTalentCandidateTabClicked;
}

const mapStateToProps = ({suggestedTalents: {fromLastLogin, threshold}}: IAppState) => ({
  fromLastLogin,
  threshold,
});

export const SuggestedTalentsContainer = connect(mapStateToProps, {
  onSuggestedTalentCandidateTabClicked,
  onSuggestedTalentClicked,
  onSuggestedTalentsPageClicked,
  onSuggestedTalentsResumeClicked,
})(SuggestedTalents);
