import React from 'react';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {MissingCandidate} from '~/components/Candidates/MissingCandidate';
import {ErrorBoundary} from '~/components/Core/ErrorBoundary';
import {GetSuggestedTalentsTalents, Maybe} from '~/graphql/__generated__/types';
import {DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';
import {getElapsedDays} from '~/util/date';

export interface ISuggestedTalentsListProps {
  onSelectSuggestedTalent: (suggestedTalent: GetSuggestedTalentsTalents, positionIndex: number) => void;
  selectedSuggestedTalent?: GetSuggestedTalentsTalents;
  suggestedTalents: Array<Maybe<GetSuggestedTalentsTalents & {bookmarkedOn?: string}>>; // TODO: remove the additional bookmarkedOn key once the backend returns this value
  selectedTalent?: GetSuggestedTalentsTalents;
  onResumeClick: (gaLabel: DOWNLOAD_RESUME_GA_LABEL) => void;
}

export const SuggestedTalentList: React.FunctionComponent<ISuggestedTalentsListProps> = ({
  onSelectSuggestedTalent,
  selectedSuggestedTalent,
  suggestedTalents,
  onResumeClick,
}) => {
  return (
    <ul className="ma0 pl3 pr1 list" data-cy="suggested-talent-list">
      {suggestedTalents.map((suggestedTalent, index) => {
        const key = suggestedTalent?.talent.id || index;
        if (!suggestedTalent) {
          return <MissingCandidate key={key} candidateType="suggested talent" />;
        }
        const {talent} = suggestedTalent;
        const {id, lastLogin} = talent;
        const isSelected = selectedSuggestedTalent ? selectedSuggestedTalent.talent.id === id : false;
        const lastLoginInfo = `Active ${getElapsedDays(lastLogin)}`;

        return (
          <ErrorBoundary key={key} fallback={<MissingCandidate candidateType="suggested talent" />}>
            <li>
              <CandidatesListItem
                key={key}
                selected={isSelected}
                isViewed={false}
                isBookmarked={!!suggestedTalent.bookmarkedOn}
                candidate={talent}
                candidateType={CandidateType.SuggestedTalent}
                date={lastLoginInfo}
                onClick={() => onSelectSuggestedTalent(suggestedTalent, index)}
                onResumeClick={() => onResumeClick(DOWNLOAD_RESUME_GA_LABEL.LIST)}
              />
            </li>
          </ErrorBoundary>
        );
      })}
    </ul>
  );
};
