import {isNil} from 'lodash/fp';
import React from 'react';
import {Query} from 'react-apollo';
import {useSelector} from 'react-redux';
import {TotalCount} from '~/components/JobList/TotalCount';
import {GetSuggestedTalentsCountQuery, GetSuggestedTalentsCountQueryVariables} from '~/graphql/__generated__/types';
import {GET_SUGGESTED_TALENTS_COUNT, SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {isSuggestedTalentUnavailable, IFormattedJobPost} from '~/util/jobPosts';
import {IAppState} from '~/flux';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';

const SUGGESTED_TALENTS = 'Suggested Talents';

export interface ISuggestedTalentsCountProps {
  fromLastLogin: string;
  formattedJob: IFormattedJobPost;
  threshold: number;
}

export const SuggestedTalentsCount: React.FunctionComponent<ISuggestedTalentsCountProps> = ({
  fromLastLogin,
  formattedJob,
  threshold,
}) => {
  const x0paSuggestedTalentsFeatureToggle = useSelector(({features}: IAppState) => features.x0paSuggestedTalents);

  return (
    <Query<GetSuggestedTalentsCountQuery, GetSuggestedTalentsCountQueryVariables>
      query={GET_SUGGESTED_TALENTS_COUNT}
      variables={{
        fromLastLogin,
        jobId: formattedJob.uuid,
        threshold,
      }}
      skip={isSuggestedTalentUnavailable(formattedJob)}
      context={{
        headers: x0paSuggestedTalentsFeatureToggle ? {} : {[API_VERSION_HEADER_KEY]: '2021-05-24'},
      }}
    >
      {({loading, error, data}) => {
        if (loading || error || isNil(data) || isNil(data.suggestedTalentsForJob)) {
          return <TotalCount title={SUGGESTED_TALENTS} loading={loading} />;
        }

        const {total} = data.suggestedTalentsForJob;
        const count = total > SUGGESTED_TALENTS_TOTAL_LIMIT ? SUGGESTED_TALENTS_TOTAL_LIMIT : total;
        return <TotalCount count={count} title={SUGGESTED_TALENTS} loading={loading} />;
      }}
    </Query>
  );
};
