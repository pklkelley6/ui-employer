import {MockedProvider, MockedProviderProps} from '@apollo/react-testing';
import {mount, ReactWrapper} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {SuggestedTalents} from '../SuggestedTalents';
import {ISuggestedTalentsProps} from '../SuggestedTalentsContainer';
import {suggestedTalentsMock} from '~/__mocks__/suggestedTalents/suggestedTalents.mocks';
import {TemporarilyUnavailable} from '~/components/Layouts/TemporarilyUnavailable';
import {Pagination} from '~/components/Navigation/Pagination';
import {OnboardingContext} from '~/components/Onboarding/OnboardingContext';
import {EmptySuggestedTalentList} from '~/components/SuggestedTalents';
import {DEFAULT_SUGGESTED_TALENTS_THRESHOLD} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {
  GET_SUGGESTED_TALENTS,
  SUGGESTED_TALENTS_LIMIT,
  SUGGESTED_TALENTS_TOTAL_LIMIT,
} from '~/graphql/suggestedTalents';
import {nextTick} from '~/testUtil/enzyme';
import * as dateUtil from '~/util/date';
import {CardLoader} from '~/components/Layouts/CardLoader';
import {IAppState} from '~/flux';
jest.spyOn(dateUtil, 'getElapsedDays').mockImplementation(() => '3 days ago');

const paginationChange = async (wrapper: ReactWrapper, page: number) => {
  wrapper.find(Pagination).prop('onPageChange')(page, SUGGESTED_TALENTS_LIMIT);
};

describe('SuggestedTalents', () => {
  afterEach(() => {
    onboardingContextProps.setSuggestedTalentsViewed.mockReset();
  });
  describe('Suggested talents view when data is properly received', () => {
    it('should render a loader when loading and then Suggested talents view after data is received', async () => {
      const wrapper = mountSuggestedTalentsComponent();
      expect(wrapper.find(CardLoader)).toMatchSnapshot();

      await nextTick(wrapper);

      const suggestedTalents = wrapper.find('[data-cy="suggested-talents-view"]');
      expect(suggestedTalents).toHaveLength(1);

      const pagination = suggestedTalents.find(Pagination);
      expect(pagination.prop('totalItemsLength')).toEqual(queryResult.data.suggestedTalentsForJob.total);

      expect(suggestedTalents).toMatchSnapshot();
    });

    it('should render the pagination to a limit when total count is above SUGGESTED_TALENTS_TOTAL_LIMIT', async () => {
      const wrapper = mountSuggestedTalentsComponent([
        {
          request: {
            query: GET_SUGGESTED_TALENTS,
            variables: baseVariables,
          },
          result: {
            data: {
              suggestedTalentsForJob: {
                talents: suggestedTalentsMock,
                total: SUGGESTED_TALENTS_TOTAL_LIMIT + 1,
              },
            },
          },
        },
      ]);

      await nextTick(wrapper);

      const suggestedTalents = wrapper.find('[data-cy="suggested-talents-view"]');
      expect(suggestedTalents).toHaveLength(1);
      const pagination = suggestedTalents.find(Pagination);
      expect(pagination.prop('totalItemsLength')).toEqual(SUGGESTED_TALENTS_TOTAL_LIMIT);
    });

    it('should return the correct position when onTalentClicked is called', async () => {
      const wrapper = mountSuggestedTalentsComponent();

      await nextTick(wrapper);

      wrapper.find('[data-cy="candidate-info"]').first().simulate('click');
      expect(suggestedTalentsProps.onSuggestedTalentClicked).toBeCalledWith(
        suggestedTalentsMock[0].talent.id,
        1,
        suggestedTalentsProps.jobUuid,
      );

      paginationChange(wrapper, 10);
      await nextTick(wrapper);

      wrapper
        .find('[data-cy="candidate-info"]')
        .at(0) // click on first candidate
        .simulate('click');
      expect(suggestedTalentsProps.onSuggestedTalentClicked).toBeCalledWith(
        suggestedTalentsMock[0].talent.id,
        201,
        suggestedTalentsProps.jobUuid,
      );

      wrapper
        .find('[data-cy="candidate-info"]')
        .at(3) // click on fourth candidate
        .simulate('click');
      expect(suggestedTalentsProps.onSuggestedTalentClicked).toBeCalledWith(
        suggestedTalentsMock[3].talent.id,
        204,
        suggestedTalentsProps.jobUuid,
      );
    });
  });

  it('should render a no longer available message when the job post is earlier than 31 Oct 2018', async () => {
    const mockQuery = [
      {
        request: {
          query: GET_SUGGESTED_TALENTS,
          variables: baseVariables,
        },
        result: {
          data: null,
        },
      },
    ];

    const props = {
      ...suggestedTalentsProps,
      unavailableSuggestedTalents: true,
    };

    const wrapper = mountSuggestedTalentsComponent(mockQuery, props);

    await nextTick(wrapper);

    const unavailableSuggestedTalentMessage = wrapper.find(TemporarilyUnavailable);
    expect(unavailableSuggestedTalentMessage).toHaveLength(1);
    expect(unavailableSuggestedTalentMessage).toMatchSnapshot();
  });

  it('should render EmptySuggestedTalentList when an empty suggested talent list is received', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_SUGGESTED_TALENTS,
          variables: baseVariables,
        },
        result: {
          data: {
            suggestedTalentsForJob: {
              talents: [],
              total: 0,
            },
          },
        },
      },
    ];

    const wrapper = mountSuggestedTalentsComponent(queryMocks);

    await nextTick(wrapper);

    const emptySuggestedTalentList = wrapper.find(EmptySuggestedTalentList);
    expect(emptySuggestedTalentList).toHaveLength(1);
    expect(emptySuggestedTalentList).toMatchSnapshot();
  });

  it('should render CandidatesFetchError when a network error is received', async () => {
    const queryMocks = [
      {
        error: new Error(),
        request: {
          query: GET_SUGGESTED_TALENTS,
          variables: baseVariables,
        },
      },
    ];

    const wrapper = mountSuggestedTalentsComponent(queryMocks);

    await nextTick(wrapper);

    const candidatesFetchError = wrapper.find(TemporarilyUnavailable);
    expect(candidatesFetchError).toHaveLength(1);
    expect(candidatesFetchError).toMatchSnapshot();
  });

  it('should render TemporarilyUnavailable message when data returned is null', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_SUGGESTED_TALENTS,
          variables: baseVariables,
        },
        result: {
          data: null,
        },
      },
    ];

    const wrapper = mountSuggestedTalentsComponent(queryMocks);

    await nextTick(wrapper);

    const candidatesFetchError = wrapper.find(TemporarilyUnavailable);
    expect(candidatesFetchError).toHaveLength(1);
    expect(candidatesFetchError).toMatchSnapshot();
  });

  it('should navigate to another page when pagination is clicked', async () => {
    const wrapper = mountSuggestedTalentsComponent();

    await nextTick(wrapper);

    const pagination = wrapper.find(Pagination);
    expect(pagination.find('.bg-secondary').text()).toEqual('1');

    const secondPage = pagination.findWhere((node) => node.type() === 'span' && node.text() === '2');
    secondPage.simulate('click');

    await nextTick(wrapper);

    const pageChange = wrapper.find(Pagination);
    expect(pageChange.find('.bg-secondary').text()).toEqual('2');
  });

  it('should render errors for null values and render the rest of suggested talents', async () => {
    const queryMocks = [
      {
        request: {
          query: GET_SUGGESTED_TALENTS,
          variables: baseVariables,
        },
        result: {
          data: {
            suggestedTalentsForJob: {
              talents: [...suggestedTalentsMock, null, null],
              total: suggestedTalentsMock.length,
            },
          },
        },
      },
    ];

    const wrapper = mountSuggestedTalentsComponent(queryMocks);

    await nextTick(wrapper, 100);

    const suggestedTalents = wrapper.find('[data-cy="suggested-talents-view"]');
    expect(suggestedTalents).toHaveLength(1);
    expect(suggestedTalents).toMatchSnapshot();
  });
});

const suggestedTalentsProps = {
  fromLastLogin: '2019-01-01',
  jobUuid: 'afab75dafd1c63f8dc3a8d909be8ef4f',
  onSuggestedTalentCandidateTabClicked: jest.fn(),
  onSuggestedTalentClicked: jest.fn(),
  onSuggestedTalentsPageClicked: jest.fn(),
  onSuggestedTalentsResumeClicked: jest.fn(),
  threshold: DEFAULT_SUGGESTED_TALENTS_THRESHOLD,
  unavailableSuggestedTalents: false,
  shouldDisplayContactButton: false,
};

const baseVariables = {
  fromLastLogin: suggestedTalentsProps.fromLastLogin,
  jobId: suggestedTalentsProps.jobUuid,
  limit: SUGGESTED_TALENTS_LIMIT,
  page: 0,
  threshold: suggestedTalentsProps.threshold,
};
const queryResult = {
  data: {
    suggestedTalentsForJob: {
      talents: suggestedTalentsMock,
      total: SUGGESTED_TALENTS_LIMIT * 2,
    },
  },
};

const defaultQueryMocks = [
  {
    request: {
      query: GET_SUGGESTED_TALENTS,
      variables: baseVariables,
    },
    result: queryResult,
  },
  {
    request: {
      query: GET_SUGGESTED_TALENTS,
      variables: {
        ...baseVariables,
        page: 1,
      },
    },
    result: queryResult,
  },
  {
    request: {
      query: GET_SUGGESTED_TALENTS,
      variables: {
        ...baseVariables,
        page: 10,
      },
    },
    result: queryResult,
  },
];

const onboardingContextProps = {
  isSuggestedTalentsViewed: true,
  setSuggestedTalentsViewed: jest.fn(),
};

const reduxState: {
  features: Partial<IAppState['features']>;
} = {
  features: {x0paSuggestedTalents: true},
};
const store = configureStore()(reduxState);
const mountSuggestedTalentsComponent = (
  queryMocks: MockedProviderProps['mocks'] = defaultQueryMocks,
  props: ISuggestedTalentsProps = suggestedTalentsProps,
): ReactWrapper => {
  return mount(
    <MockedProvider mocks={queryMocks} addTypename={false}>
      <Provider store={store}>
        <Router>
          <OnboardingContext.Provider value={onboardingContextProps}>
            <SuggestedTalents {...props} />
          </OnboardingContext.Provider>
        </Router>
      </Provider>
    </MockedProvider>,
  );
};
