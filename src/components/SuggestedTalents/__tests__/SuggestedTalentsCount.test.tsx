import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {mount} from 'enzyme';
import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {act} from 'react-dom/test-utils';
import {format, subYears} from 'date-fns';
import {print} from 'graphql';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {PactBuilder} from '~/__mocks__/pact';
import {SuggestedTalentsCount} from '~/components/SuggestedTalents/SuggestedTalentsCount';
import {DEFAULT_SUGGESTED_TALENTS_THRESHOLD} from '~/flux/suggestedTalents/suggestedTalents.constants';
import {GET_SUGGESTED_TALENTS_COUNT, SUGGESTED_TALENTS_TOTAL_LIMIT} from '~/graphql/suggestedTalents';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {formatJob} from '~/util/jobPosts';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {IAppState} from '~/flux';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

describe('SuggestedTalents/SuggestedTalentsCount', () => {
  let pactBuilder: PactBuilder;
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });
  afterAll(async () => pactBuilder.provider.finalize());

  const variables = {
    fromLastLogin: '2019-01-01',
    jobId: '65d4da4a5d33b7cf7f1b9b65fd6e012b',
    threshold: DEFAULT_SUGGESTED_TALENTS_THRESHOLD,
  };
  const getSuggestedTalentsCountQueryFactory = () =>
    new ApolloGraphQLInteraction()
      .withQuery(print(GET_SUGGESTED_TALENTS_COUNT))
      .withOperation('getSuggestedTalentsCount')
      .withRequest({
        headers: {
          'content-type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
        },
        method: 'POST',
        path: '/profile',
      })
      .withVariables(variables);

  it('render SuggestedTalentsCount', async () => {
    const queryMockResult = {
      data: {
        suggestedTalentsForJob: {
          total: Matchers.integer(),
        },
      },
    };

    const getSuggestedTalentsCountQuery = getSuggestedTalentsCountQueryFactory();
    getSuggestedTalentsCountQuery.uponReceiving('getSuggestedTalentsCount').willRespondWith({
      body: queryMockResult,
      status: 200,
    });
    await pactBuilder.provider.addInteraction(getSuggestedTalentsCountQuery);

    const wrapper = mountSuggestedTalentsCount();

    expect(wrapper.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toContain(
      queryMockResult.data.suggestedTalentsForJob.total.getValue(),
    );
  });

  it('render SuggestedTalentsCount with SUGGESTED_TALENTS_TOTAL_LIMIT when the total count is above SUGGESTED_TALENTS_TOTAL_LIMIT', async () => {
    const queryMockResult = {
      data: {
        suggestedTalentsForJob: {
          total: Matchers.integer(SUGGESTED_TALENTS_TOTAL_LIMIT + 1),
        },
      },
    };

    const getSuggestedTalentsCountQuery = getSuggestedTalentsCountQueryFactory();
    getSuggestedTalentsCountQuery.uponReceiving('getSuggestedTalentsCountAboveLimit').willRespondWith({
      body: queryMockResult,
      status: 200,
    });
    await pactBuilder.provider.addInteraction(getSuggestedTalentsCountQuery);

    const wrapper = mountSuggestedTalentsCount();

    expect(wrapper.find('.spinnerContainer')).toHaveLength(1);

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toContain(SUGGESTED_TALENTS_TOTAL_LIMIT);
  });

  it('should not fetch getSuggestedTalentsCount when job status is closed and original posting date past 1 year', () => {
    const wrapper = mountSuggestedTalentsCount({
      ...jobPostMock,
      status: {id: JobStatusCodes.Closed, jobStatus: 'Closed'},
      metadata: {...jobPostMock.metadata, originalPostingDate: format(subYears(new Date(), 1), 'yyyy-MM-dd')},
    });

    expect(wrapper.find('h4[data-cy="total-count"]').text()).toEqual('-');
  });

  const reduxState: {
    features: Partial<IAppState['features']>;
  } = {
    features: {x0paSuggestedTalents: true},
  };
  const store = configureStore()(reduxState);
  const mountSuggestedTalentsCount = (job = {...jobPostMock, uuid: variables.jobId}) =>
    mount(
      <ApolloProvider client={pactBuilder.getApolloClient()}>
        <Provider store={store}>
          <SuggestedTalentsCount
            formattedJob={formatJob(job)}
            threshold={variables.threshold}
            fromLastLogin={variables.fromLastLogin}
          />
        </Provider>
      </ApolloProvider>,
    );
});
