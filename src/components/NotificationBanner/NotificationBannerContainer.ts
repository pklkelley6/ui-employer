import {connect} from 'react-redux';
import {NotificationBanner} from './NotificationBanner';
import {IAppState} from '~/flux';

const mapStateToProps = ({notification: {features, maintenances}}: IAppState) => ({
  features,
  maintenances,
});

export const NotificationBannerContainer = connect(mapStateToProps)(NotificationBanner);
