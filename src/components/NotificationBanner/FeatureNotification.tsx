import React from 'react';
import styles from './NotificationBanner.scss';
import {IFeatureNotification} from '~/services/notification/getNotifications';

interface IFeatureNotificationProps {
  feature: IFeatureNotification;
  onClose: () => void;
}

export const FeatureNotification: React.FunctionComponent<IFeatureNotificationProps> = ({
  feature: {bannerColor, message, textColor},
  onClose,
}) => {
  return (
    <aside className="pv3" style={{backgroundColor: bannerColor}} data-cy="feature-notification">
      <div className="ph3-ns pv0 center">
        <div className="flex-ns justify-between pv0 ph3 items-center">
          <p className="f6 ma0 pv0 pr3 fw4 v-mid lh-copy" style={{color: textColor}}>
            <span className={`fw7 pr2 ${styles.iconNewYellow}`}>New!</span>
            {message}
          </p>
          <button data-cy="feature-close-button" className={styles.notificationCloseButton} onClick={onClose} />
        </div>
      </div>
    </aside>
  );
};
