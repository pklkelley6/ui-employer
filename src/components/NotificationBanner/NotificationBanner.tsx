import {formatISO, isSameDay, isWithinInterval, parseISO} from 'date-fns';
import React from 'react';
import {useLocalStorage} from 'react-use';
import {MaintenanceNotification} from './MaintenanceNotification';
import {FeatureNotification} from './FeatureNotification';
import {IFeatureNotification, IMaintenanceNotification} from '~/services/notification/getNotifications';

export interface INotificationBannerProps {
  features: IFeatureNotification[];
  maintenances: IMaintenanceNotification[];
}

const FEATURE_ACKNOWLEDGEMENT_TIME_LOCAL_STORAGE_KEY = 'feature-acknowledgement-time';

const isCurrentNotification = ({fromDate, toDate}: IFeatureNotification | IMaintenanceNotification) =>
  isWithinInterval(new Date(), {start: parseISO(fromDate), end: parseISO(toDate)});

export const NotificationBanner: React.FunctionComponent<INotificationBannerProps> = ({features, maintenances}) => {
  const currentMaintenance = maintenances.find(isCurrentNotification);
  const currentFeature = features.find(isCurrentNotification);

  const [featureAcknowledgementTime, setFeatureAcknowledgementTime] = useLocalStorage<string>(
    FEATURE_ACKNOWLEDGEMENT_TIME_LOCAL_STORAGE_KEY,
    '',
    {raw: true},
  );
  const featureAcknowledgedToday = featureAcknowledgementTime
    ? isSameDay(new Date(), parseISO(featureAcknowledgementTime))
    : false;

  return currentMaintenance ? (
    <MaintenanceNotification maintenance={currentMaintenance} />
  ) : currentFeature && !featureAcknowledgedToday ? (
    <FeatureNotification
      feature={currentFeature}
      onClose={() => setFeatureAcknowledgementTime(formatISO(new Date()))}
    />
  ) : null;
};
