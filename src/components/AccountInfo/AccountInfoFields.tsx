import {TextInput} from '@govtechsg/mcf-mcfui';
import React from 'react';
import {Field, useField} from 'react-final-form';
import {composeValidators, contactNumber, defaultRequired, email} from '~/util/fieldValidations';

export const AccountInfoFields: React.FunctionComponent = () => {
  const name = useField('name', {
    subscription: {value: true},
  });

  return (
    <div className="pt3">
      <section className="flex mb2">
        <div className="mv3 w-50 pr2">
          <div className="mb2 f5 fw6 black-60 pl1 db"> Name </div>
          <div className="fw4 f4-5 pa3 bg-white-30 ph2"> {name.input.value} </div>
        </div>
        <div className="w-50 pl2">
          <Field name="designation">
            {({input, meta}) => {
              return (
                <TextInput
                  id="designation"
                  label="Designation (optional)"
                  placeholder="Enter Designation"
                  input={input}
                  meta={meta}
                />
              );
            }}
          </Field>
        </div>
      </section>
      <section className="flex mb3">
        <div className="w-50 pr2">
          <Field name="email" validate={composeValidators(defaultRequired, email)}>
            {({input, meta}) => {
              return (
                <div>
                  <TextInput
                    id="email"
                    label="Email Address"
                    placeholder="Enter Email Address"
                    input={input}
                    meta={meta}
                  />
                </div>
              );
            }}
          </Field>
        </div>
        <div className="w-50 pl2">
          <Field
            name="contactNumber"
            validate={composeValidators(defaultRequired, contactNumber)}
            parse={(value: string): string => (value ? value : value.replace(/[^\d]/g, ''))}
          >
            {({input, meta}) => {
              return (
                <div>
                  <TextInput
                    id="contact-number"
                    label="Contact Number"
                    placeholder="Enter Contact Number"
                    input={input}
                    meta={meta}
                  />
                </div>
              );
            }}
          </Field>
        </div>
      </section>
    </div>
  );
};
