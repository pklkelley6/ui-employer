import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {noop} from 'lodash';
import React from 'react';
import {Form} from 'react-final-form';
import {AccountInfoFields} from '../AccountInfoFields';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('AccountInfo/AccountInfoFields', () => {
  it('should render AccountInfoFields correctly', async () => {
    const systemContact = {
      email: 'foo@bar.baz',
      contactNumber: '61234567',
      name: 'foo bar',
      designation: 'foo director',
    };
    const wrapper = mount(<Form onSubmit={noop} initialValues={systemContact} render={() => <AccountInfoFields />} />);

    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
});
