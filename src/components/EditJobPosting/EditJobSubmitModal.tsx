import React from 'react';
import styles from '../Jobs/JobPostModal.scss';
import {Modal} from '~/components/Core/Modal';
import {MAX_JOB_POST_EDITS} from '~/components/EditJobPosting/EditJobPost.constants';

interface IEditJobSubmitModalProps {
  editCount: number;
  onCancel: () => void;
  onSubmit: () => any;
}

export const EditJobSubmitModal: React.FunctionComponent<IEditJobSubmitModalProps> = ({
  editCount,
  onCancel,
  onSubmit,
}) => {
  const editsLeftAfterSubmit = MAX_JOB_POST_EDITS - editCount - 1;
  return (
    <Modal>
      <div data-cy="edit-job-submit-modal">
        <div className="pa4 f4 black-80 bg-white">
          <p>Are you sure you want to submit this edited job posting?</p>
          <p data-cy="edits-left-edit-job-submit-modal">
            After submitting, you will have {editsLeftAfterSubmit} edit{editsLeftAfterSubmit === 1 ? '' : 's'} left.
          </p>
        </div>
        <div className="pa2 tc bg-light-gray">
          <button
            data-cy="cancel-button-edit-job-submit-modal"
            className={`ma2 pa3 primary ${styles.cancelButton}`}
            onClick={onCancel}
          >
            No, Cancel
          </button>
          <button
            data-cy="submit-button-edit-job-submit-modal"
            className={`ma2 pa3 bg-primary ${styles.submitButton}`}
            onClick={onSubmit}
          >
            Yes, Submit
          </button>
        </div>
      </div>
    </Modal>
  );
};
