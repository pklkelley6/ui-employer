import {mount} from 'enzyme';
import {noop} from 'lodash/fp';
import React from 'react';
import {EditJobSubmitModal} from '../EditJobSubmitModal';

describe('EditJobSubmitModal', () => {
  it('should display 1 edit left if edit count is 0', () => {
    const component = mount(<EditJobSubmitModal onCancel={noop} onSubmit={noop} editCount={0} />);
    expect(component.find('[data-cy="edits-left-edit-job-submit-modal"]').text()).toEqual(
      'After submitting, you will have 1 edit left.',
    );
  });

  it('should display 0 edits left if edit count is 1', () => {
    const component = mount(<EditJobSubmitModal onCancel={noop} onSubmit={noop} editCount={1} />);
    expect(component.find('[data-cy="edits-left-edit-job-submit-modal"]').text()).toEqual(
      'After submitting, you will have 0 edits left.',
    );
  });
});
