import {RouteComponentProps, withRouter} from 'react-router';
import {differenceInDays, parseISO} from 'date-fns';
import {compact, gte} from 'lodash/fp';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {CloseJobPostModalContainer} from './CloseJobPostModalContainer';
import {ExtendJobPostModalContainer} from './ExtendJobPostModalContainer';
import {IJobPostMenuItem} from './JobPostMenuItem';
import {RemoveScreeningQuestionsModalContainer} from './RemoveScreeningQuestionsModalContainer';
import {IAppState} from '~/flux';
import {MCFManageJobPostMenu} from '~/components/Jobs/MCFManageJobPostMenu';
import {MCFRepostJobPostButton} from '~/components/Jobs/MCFRepostJobPostButton';
import {ViewJobPostButton} from '~/components/Jobs/ViewJobPostButton';
import {JobStatusCodes} from '~/services/employer/jobs.types';
import {isMCFJob} from '~/util/isMCFJob';
import {jobToPath} from '~/util/url';
import {IJobPost} from '~/services/employer/jobs.types';
import {FeatureFlag} from '~/components/Core/ToggleFlag';

const MAX_DURATION = 30;
export interface IJobPostMenuProps extends RouteComponentProps {
  job: IJobPost;
}

export enum JobPostModal {
  None,
  CloseJobPostModal,
  ExtendJobPostModal,
  RemoveScreeningQuestionsModal,
}

export const JobPostMenu: React.FunctionComponent<IJobPostMenuProps> = ({job, history}) => {
  const removeScreeningQuestionsReleaseToggle = useSelector(({features}: IAppState) => features.screeningQuestions);
  const [jobPostModal, setJobPostModal] = useState(JobPostModal.None);
  const {jobPostId, expiryDate, newPostingDate} = job.metadata;
  const jobDuration = differenceInDays(parseISO(expiryDate), parseISO(newPostingDate));
  const isMaxDuration = gte(jobDuration, MAX_DURATION);
  const companyName = job.hiringCompany ? job.hiringCompany.name : job.postedCompany.name;
  const jobLinkUrl = jobToPath({
    company: companyName,
    jobTitle: job.title,
    uuid: job.uuid,
  });
  const onViewJobPostClick = () => {
    history.push({
      pathname: `${jobLinkUrl}/view`,
    });
  };
  const menuItems: IJobPostMenuItem[] = compact([
    {
      id: 'manage-job-edit-item',
      onClick: onViewJobPostClick,
      title: 'View/Edit Job Details',
    },
    removeScreeningQuestionsReleaseToggle &&
      job.screeningQuestions &&
      job.screeningQuestions.length > 0 && {
        id: 'manage-job-remove-screening-questions',
        onClick: () => setJobPostModal(JobPostModal.RemoveScreeningQuestionsModal),
        title: 'Remove Screening Questions',
      },
    isMaxDuration
      ? {
          disabled: true,
          hoverMessage: 'The maximum duration has already been selected for this job',
          id: 'manage-job-extend-item-disabled',
          title: 'Extend Posting Duration',
        }
      : {
          id: 'manage-job-extend-item',
          onClick: () => setJobPostModal(JobPostModal.ExtendJobPostModal),
          title: 'Extend Posting Duration',
        },
    {
      id: 'manage-job-close-job-item',
      onClick: () => setJobPostModal(JobPostModal.CloseJobPostModal),
      title: 'Close Job',
    },
  ]);

  const showMcfMenu = () =>
    job.status.id === JobStatusCodes.Closed ? (
      <MCFRepostJobPostButton onClick={onViewJobPostClick} />
    ) : (
      <MCFManageJobPostMenu menuItems={menuItems} />
    );
  const showMsfMenu = () => <ViewJobPostButton onClick={onViewJobPostClick} />;

  const renderJobPostModal = (modal: JobPostModal) => {
    switch (modal) {
      case JobPostModal.RemoveScreeningQuestionsModal:
        return (
          <FeatureFlag
            name="screeningQuestions"
            render={() => (
              <RemoveScreeningQuestionsModalContainer job={job} onCancel={() => setJobPostModal(JobPostModal.None)} />
            )}
          />
        );
      case JobPostModal.ExtendJobPostModal:
        return <ExtendJobPostModalContainer job={job} onCancel={() => setJobPostModal(JobPostModal.None)} />;
      case JobPostModal.CloseJobPostModal:
        return <CloseJobPostModalContainer job={job} onCancel={() => setJobPostModal(JobPostModal.None)} />;
      default:
        return null;
    }
  };

  return (
    <>
      {isMCFJob(jobPostId) ? showMcfMenu() : showMsfMenu()}
      {renderJobPostModal(jobPostModal)}
    </>
  );
};

export default withRouter(JobPostMenu);
