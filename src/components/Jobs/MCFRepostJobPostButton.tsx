import React from 'react';
import styles from './RepostJobPost.scss';

interface IMCFRepostJobPostButton {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const MCFRepostJobPostButton: React.FunctionComponent<IMCFRepostJobPostButton> = ({onClick}) => {
  return (
    <button
      data-cy="repost-job-post-button"
      className={`relative f3 white pa3 bg-washed-blue pointer ${styles.repostJobPostButton}`}
      onClick={onClick}
    >
      <div className="flex flex-column items-center justify-around">
        <i className={`w-30 db relative ${styles.iconRepostJobMenu}`} />
        <span className="f6 mt2">View/Repost Job Post</span>
      </div>
    </button>
  );
};
