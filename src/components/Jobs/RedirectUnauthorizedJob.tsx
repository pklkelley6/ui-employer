import React, {ReactNode} from 'react';
import {Redirect} from 'react-router-dom';
import {useSelector} from 'react-redux';
import {IAppState} from '~/flux';
import {getAccountUen} from '~/flux/account';
import {PageUnavailable} from '~/pages/PageUnavailable/PageUnavailable';
import {IJobPost} from '~/services/employer/jobs.types';

export interface IRedirectUnauthorizedJobProps {
  job?: IJobPost;
  children: ReactNode;
}

export const RedirectUnauthorizedJob: React.FunctionComponent<IRedirectUnauthorizedJobProps> = ({children, job}) => {
  const newErrorPageToggle = useSelector((state: IAppState) => state.features.newErrorPage);
  const accountUen = useSelector((state: IAppState) => getAccountUen(state));
  const isPostedCompany = accountUen === job?.postedCompany.uen;

  return isPostedCompany ? <>{children}</> : newErrorPageToggle ? <PageUnavailable /> : <Redirect to="/jobs" />;
};
