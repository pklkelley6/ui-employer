import {connect} from 'react-redux';
import {RemoveScreeningQuestionsModal} from './RemoveScreeningQuestionsModal';
import {jobPostingFetchRequested} from '~/flux/jobPosting';
import {onJobPostingScreeningQuestionsRemoved} from '~/flux/analytics';
import {IJobPost} from '~/services/employer/jobs.types';

export interface IRemoveScreeningQuestionsModalProps {
  onCancel: () => void;
  job: IJobPost;
}

export interface IRemoveScreeningQuestionsModalContainerDispatchProps {
  fetchJob: typeof jobPostingFetchRequested;
  onJobPostingScreeningQuestionsRemoved: typeof onJobPostingScreeningQuestionsRemoved;
}

export const RemoveScreeningQuestionsModalContainer = connect(null, {
  fetchJob: jobPostingFetchRequested,
  onJobPostingScreeningQuestionsRemoved,
})(RemoveScreeningQuestionsModal);

export type IRemoveScreeningQuestionsModalComponentProps = IRemoveScreeningQuestionsModalContainerDispatchProps &
  IRemoveScreeningQuestionsModalProps;
