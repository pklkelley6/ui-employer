import React from 'react';
import {
  CLOSED_JOBS_REQUEST_FAILED,
  CLOSED_JOBS_REQUESTED,
  OPEN_JOBS_REQUEST_FAILED,
  OPEN_JOBS_REQUESTED,
} from '~/flux/jobPostings/jobPostings.constants';
import {IJobsResponse} from '~/flux/jobPostings/jobPostings.reducer';

interface IJobsQueryResult {
  loading: boolean;
  error?: string;
  data: IJobsResponse;
}

interface IJobsQuery {
  status?: string;
  payload: IJobsResponse;
  children: (result: IJobsQueryResult) => React.ReactElement<any> | null;
}

/**
 * Query-like component
 *
 * @description Partial replacement of Query component for upcoming switch to apollo
 */
export const JobsQuery: React.FunctionComponent<IJobsQuery> = ({status, payload, children}) => {
  const loading = status === OPEN_JOBS_REQUESTED || status === CLOSED_JOBS_REQUESTED;

  let error: string;
  if (status === OPEN_JOBS_REQUEST_FAILED || status === CLOSED_JOBS_REQUEST_FAILED) {
    error = 'Error';
  }

  const result = () =>
    children({
      data: payload,
      error,
      loading,
    });

  return result();
};
