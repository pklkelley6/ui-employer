import React from 'react';
import {useQuery} from 'react-apollo';
import {GET_JOB_SYSTEM_CONTACT} from '~/graphql/systemContact/systemContact.query';
import {GetJobSystemContactQuery, GetJobSystemContactVariables} from '~/graphql/__generated__/types';

export interface IJobPostingContactProps {
  jobUuid: string;
}

export const JobPostingContact: React.FunctionComponent<IJobPostingContactProps> = ({jobUuid}) => {
  const {data} = useQuery<GetJobSystemContactQuery, GetJobSystemContactVariables>(GET_JOB_SYSTEM_CONTACT, {
    variables: {
      jobUuid,
    },
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  });
  const emailRecipientContact = data?.jobByUuid?.emailRecipientContact?.email;
  const createdByContact = data?.jobByUuid?.createdByContact?.email;
  const contact = emailRecipientContact || createdByContact;

  return contact ? (
    <div data-cy="job-posting-contact" className="bg-black-05 pa3 lh-title black-80 f5 mb3 flex flex-wrap">
      <p className="w-100 ma0 fw7 mb3">CONTACT EMAIL</p>
      <div className="f6 lh-copy ma0 nowrap">Emails about this job will be sent to:</div>
      <div data-cy="job-posting-contact-email" className="f6 fw7 lh-copy ma0">
        {contact}
      </div>
    </div>
  ) : (
    <></>
  );
};
