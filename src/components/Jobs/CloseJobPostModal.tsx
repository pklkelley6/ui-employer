import React, {useReducer} from 'react';
import {Link} from 'react-router-dom';
import styles from './CloseJob.scss';
import {
  closeJobReducer,
  ERROR,
  PENDING,
  SELECT_JOB_IS_VACANT,
  SELECT_JOB_IS_VACANT_PROMPT,
  SUCCESS,
} from './CloseJobPostModal.reducer';
import {ICloseJobPostModalComponentProps} from './CloseJobPostModalContainer';
import {Modal} from '~/components/Core/Modal';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {closeJobPosting} from '~/services/employer/jobs';

export const CloseJobPostModal: React.FunctionComponent<ICloseJobPostModalComponentProps> = ({
  job,
  fetchClosedJobs,
  fetchOpenJobs,
  closedJobsParams,
  openJobsParams,
  onCancel,
}) => {
  const {jobPostId} = job.metadata;

  const [state, dispatch] = useReducer(closeJobReducer, {
    employerSelection: '',
    employerSelectionPrompt: false,
    isLoading: false,
    submitted: false,
  });

  const FILLED = 'Filled';
  const AVAILABLE = 'Available';

  const isJobVacant = (value: string) => value === AVAILABLE;

  const fetchAllJobs = () => {
    fetchClosedJobs(closedJobsParams);
    fetchOpenJobs(openJobsParams);
  };

  const onCloseJobRequest = async () => {
    dispatch({type: PENDING});
    try {
      await closeJobPosting(job.uuid, isJobVacant(state.employerSelection));
      dispatch({type: SUCCESS});
    } catch (error) {
      dispatch({type: ERROR});
      onCancel();
      growlNotification(
        <div>
          Temporarily unable to close job. <br /> Please try again.
        </div>,
        GrowlType.ERROR,
      );
    }
  };

  return (
    <Modal>
      <div data-cy="close-job-module">
        {state.isLoading ? (
          <div className="bg-white tc ph5 pv4">
            <div className={styles.loadingContainer}>
              <NumberLoader />
            </div>
            <p className="primary ma0 mt2 f4-5">Please wait while job closes</p>
          </div>
        ) : (
          <div className="flex-column">
            <div className={`bg-white pa4 black-80 ${styles.closeJobOption}`}>
              {!state.submitted ? (
                <div>
                  <div className="lh-copy fw6 pv2">{`To close the job post for ${job.title} (${jobPostId}), please select one:`}</div>
                  <div data-cy="select-type-of-vacancy" className="flex flex-column pv2">
                    <div className="ma2">
                      <input
                        type="radio"
                        className={`${styles.selectVacancyInput}`}
                        id={FILLED}
                        name="employerSelection"
                        value={FILLED}
                        onChange={(e) => {
                          dispatch({type: SELECT_JOB_IS_VACANT, payload: e.target.value});
                        }}
                      />
                      <label htmlFor={FILLED}>This position has been filled</label>
                    </div>
                    <div className="ma2">
                      <input
                        type="radio"
                        id={AVAILABLE}
                        className={`${styles.selectVacancyInput}`}
                        name="employerSelection"
                        value={AVAILABLE}
                        onChange={(e) => {
                          dispatch({type: SELECT_JOB_IS_VACANT, payload: e.target.value});
                        }}
                      />
                      <label htmlFor={AVAILABLE}>This position has not been filled</label>
                    </div>
                  </div>
                  {state.employerSelectionPrompt ? (
                    <span data-cy="empty-option-error" className="bg-white f5-5 pt2 ph2 db red">
                      Please select an option
                    </span>
                  ) : null}
                </div>
              ) : (
                <div data-cy="close-job-success" className="lh-copy fw6 pv2">
                  {`The job post for ${job.title} (${jobPostId}) has been closed successfully.`}
                </div>
              )}
            </div>
            <div className="pa2 tc bg-light-gray">
              {!state.submitted ? (
                <>
                  <button data-cy="cancel-close-job" className={`ma2 pa3 ${styles.cancelCloseJob}`} onClick={onCancel}>
                    Cancel
                  </button>
                  <button
                    data-cy="proceed-close-job"
                    className={`ma2 pa3 bg-primary ${styles.proceedCloseJob}`}
                    disabled={state.isLoading}
                    onClick={() =>
                      !state.employerSelection ? dispatch({type: SELECT_JOB_IS_VACANT_PROMPT}) : onCloseJobRequest()
                    }
                  >
                    Proceed to Close
                  </button>
                </>
              ) : (
                <div className={`pa3 tc w-50 ${styles.returnToJobsButton}`} data-cy="return-to-jobs">
                  <Link
                    to="/jobs"
                    className={`link tr ${styles.allJobsLink}`}
                    onClick={() => {
                      fetchAllJobs();
                      onCancel();
                    }}
                  >
                    Return to All Jobs
                  </Link>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    </Modal>
  );
};
