import React, {useState} from 'react';
import {useAsyncFn} from 'react-use';
import styles from './RemoveScreeningQuestionsModal.scss';
import {IRemoveScreeningQuestionsModalComponentProps} from './RemoveScreeningQuestionsModalContainer';
import {Modal} from '~/components/Core/Modal';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {removeScreeningQuestions} from '~/services/employer/jobs';
import {IJobScreeningQuestion} from '~/services/employer/jobs.types';

export const RemoveScreeningQuestionsModal: React.FunctionComponent<IRemoveScreeningQuestionsModalComponentProps> = ({
  job,
  fetchJob,
  onJobPostingScreeningQuestionsRemoved,
  onCancel,
}) => {
  const {
    uuid,
    screeningQuestions,
    metadata: {jobPostId},
  } = job;
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [state, fetch] = useAsyncFn(async () => {
    try {
      await removeScreeningQuestions(uuid);
      setIsSubmitted(true);
      onJobPostingScreeningQuestionsRemoved(jobPostId, (screeningQuestions ?? []).length);
    } catch (error) {
      growlNotification(
        'Temporarily unable to remove screening questions. Please try again.',
        GrowlType.ERROR,
        {toastId: 'removeScreeningQuestionsError'}, // Added toastId to prevent multiple copies of this growl to show up in one time
      );
      onCancel();
    }
  }, [job.uuid]);

  const renderLoader = () => {
    return (
      <div className="bg-white tc ph5 pv4">
        <NumberLoader />
      </div>
    );
  };

  const renderRemoveScreeningQuestions = () => {
    return (
      <>
        <div data-cy="remove-screening-question-modal" className={`bg-white black-70 pa4 w-100 ${styles.modal}`}>
          <div className="f4 ma0 pb3 fw6">Remove Screening Questions</div>
          <div className="b--black-20 bt mt2 mb4 w-100 lh-copy" />
          <div className="fw6 lh-copy">
            You are about to remove all screening questions, and will not be able to add any questions after removal.
            You will also no longer be able to view and filter applicants' responses.
          </div>
          <div className="pt3 pb3 fw6 lh-copy">Are you sure you want to remove them?</div>
          <div className="b--black-20 f4-5 mt2 mb2">
            {screeningQuestions?.map((question: IJobScreeningQuestion, index: number) => {
              return (
                <div className="flex lh-copy pb3" key={index}>
                  <div>{`Q${index + 1}.`}</div>
                  <div className="pl3">
                    <div>{question.question}</div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="flex pa2 tc bg-light-gray">
          <button
            data-cy="remove-screening-question-cancel"
            className="w-50 fw6 primary ma2 pa3 ba b--primary bg-white pointer"
            onClick={onCancel}
          >
            Cancel
          </button>
          <button
            data-cy="remove-screening-question-confirm"
            className="w-50 ma2 pa3 ba b--primary bg-primary white pointer"
            onClick={fetch}
          >
            Yes, Remove
          </button>
        </div>
      </>
    );
  };

  const renderRemoveScreeningQuestionsSuccess = () => {
    return (
      <>
        <div
          data-cy="remove-screening-question-modal-success"
          className={`bg-white black-70 pa4 w-100 ${styles.modal}`}
        >
          <div className="f4 ma0 pb3 fw6">Screening questions removed successfully</div>
          <div className="b--black-20 bt mt2 mb4 w-100 lh-copy" />
          <div className="fw6 lh-copy">There will no longer be any screening questions for this job posting</div>
        </div>
        <div className="flex pa2 tc bg-light-gray">
          <button
            data-cy="remove-screening-question-success-ok"
            className={`w-50 ma2 pa3 ba b--primary bg-primary white pointer db ${styles.successButton}`}
            onClick={() => {
              fetchJob(job.uuid);
              onCancel();
            }}
          >
            Ok
          </button>
        </div>
      </>
    );
  };

  return (
    <Modal>
      <div data-cy="remove-screening-questions-modal">
        {state.loading
          ? renderLoader()
          : isSubmitted
          ? renderRemoveScreeningQuestionsSuccess()
          : renderRemoveScreeningQuestions()}
      </div>
    </Modal>
  );
};
