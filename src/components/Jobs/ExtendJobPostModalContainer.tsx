import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {ExtendJobPostModal} from './ExtendJobPostModal';
import {IAppState} from '~/flux/index';
import {jobPostingFetchRequested} from '~/flux/jobPosting';
import {fetchOpenJobsRequested} from '~/flux/jobPostings';
import {IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {IJobPost} from '~/services/employer/jobs.types';

export interface IExtendJobPostModalProps {
  onCancel: () => void;
  job: IJobPost;
  openJobsParams: IFetchOpenJobs;
}

export const mapStateToProps = ({jobPostings: {openJobsParams}}: IAppState) => ({
  openJobsParams,
});

export interface IExtendJobPostModalContainerDispatchProps {
  fetchOpenJobs: (openJobsParams: IFetchOpenJobs) => ActionType<typeof fetchOpenJobsRequested>;
  fetchJob: (uuid: string) => ActionType<typeof jobPostingFetchRequested>;
}

export const ExtendJobPostModalContainer = connect(mapStateToProps, {
  fetchJob: jobPostingFetchRequested,
  fetchOpenJobs: fetchOpenJobsRequested,
})(ExtendJobPostModal);

export type IExtendJobPostModalComponentProps = IExtendJobPostModalContainerDispatchProps & IExtendJobPostModalProps;
