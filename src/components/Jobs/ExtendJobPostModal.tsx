import {addDays, differenceInDays, format, parseISO} from 'date-fns';
import {eq} from 'lodash/fp';
import React, {useState} from 'react';
import {useAsyncFn} from 'react-use';
import {IExtendJobPostModalComponentProps} from './ExtendJobPostModalContainer';
import styles from './JobPostModal.scss';
import {Dropdown} from '~/components/Core/Dropdown';
import {Modal} from '~/components/Core/Modal';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {NumberLoader} from '~/components/JobList/NumberLoader';
import {durationOptions} from '~/pages/JobPosting/JobPosting.constants';
import {extendJobPost} from '~/services/employer/jobs';
import {formatISODate} from '~/util/date';

export const getDurationOptions = (jobDuration: number) => {
  const isSevenDays = eq(jobDuration, durationOptions[0].value);
  const isFourteenDays = eq(jobDuration, durationOptions[1].value);
  return isSevenDays ? durationOptions.slice(1) : isFourteenDays ? durationOptions.slice(2) : durationOptions.slice(3);
};

export const ExtendJobPostModal: React.FunctionComponent<IExtendJobPostModalComponentProps> = ({
  fetchJob,
  job,
  onCancel,
  fetchOpenJobs,
  openJobsParams,
}) => {
  const {expiryDate, newPostingDate, jobPostId} = job.metadata;
  const currentJobDuration = differenceInDays(parseISO(expiryDate), parseISO(newPostingDate));
  const newDurationOptions = getDurationOptions(currentJobDuration);

  const [newJobDuration, setNewJobDuration] = useState(newDurationOptions[0].value);
  const newExpiryDate = addDays(parseISO(newPostingDate), newJobDuration);

  const newJobDurationPayload = {
    jobPostDuration: newJobDuration,
  };

  const [state, fetch] = useAsyncFn(
    () => extendJobPost(job.uuid, newJobDurationPayload),
    [job.uuid, newJobDurationPayload],
  );

  const renderLoader = () => {
    return (
      <div className="bg-white tc ph5 pv4">
        <NumberLoader />
      </div>
    );
  };

  const renderExtensionForm = () => {
    return (
      <>
        <div className={`bg-white pa4 black-80 ${styles.extendJobOption}`}>
          <h3 className="f4 ma0 pb3 fw6">Extend posting duration</h3>
          <div className="w-70 mb4">
            <Dropdown
              id="extend-jobpost-modal-duration-dropdown"
              label={`Extend posting duration from ${currentJobDuration} days to:`}
              data={newDurationOptions}
              input={{
                onChange: (value) => {
                  setNewJobDuration(value);
                },
                value: newJobDuration,
              }}
            />
          </div>
          <p className="f6 mb0">
            This will extend the auto-close date from{' '}
            <span data-cy="extend-jobpost-modal-original-expiry-date">{formatISODate(expiryDate)}</span> to{' '}
            <span className="purple b" data-cy="extend-jobpost-modal-new-date">
              {format(newExpiryDate, 'd MMM yyyy')}.
            </span>
          </p>
        </div>
        <div className="pa2 tc bg-light-gray">
          <button
            data-cy="cancel-button-extend-jobpost-modal"
            className={`ma2 pa3 primary ${styles.cancelButton}`}
            onClick={onCancel}
          >
            Cancel
          </button>
          <button
            data-cy="extend-button-extend-jobpost-modal"
            className={`ma2 pa3 bg-primary ${styles.submitButton}`}
            onClick={() => fetch()}
          >
            Extend
          </button>
        </div>
      </>
    );
  };

  const renderExtensionSuccess = () => {
    return (
      <>
        <div className={`bg-white pa4 black-80 ${styles.extendJobOption}`}>
          <h3 className="f4 ma0 pb3 fw6">Extension Successful</h3>
          <div data-cy="extend-job-success" className="lh-copy f6 pv2">
            <span>{`The job posting duration for ${job.title} (${jobPostId}) has been extended successfully.`}</span>
          </div>
          <div className="dib">
            <span className="f6 mb0">New auto-close date: </span>
            <span data-cy="extend-jobpost-new-expiry-date" className="fw6">{`${format(
              newExpiryDate,
              'd MMM yyyy',
            )}`}</span>
          </div>
        </div>
        <div className="pa2 tc bg-light-gray">
          <div className="tr" data-cy="return-to-jobs-details">
            <button
              data-cy="success-button-extend-jobpost-modal"
              className={`ma2 pa3 bg-primary ${styles.submitButton}`}
              onClick={() => {
                fetchJob(job.uuid);
                fetchOpenJobs(openJobsParams);
                onCancel();
              }}
            >
              Ok
            </button>
          </div>
        </div>
      </>
    );
  };

  const renderExtensionError = () => {
    onCancel();
    growlNotification(
      <div>Temporarily unable to extend job posting duration. Please try again.</div>,
      GrowlType.ERROR,
      {toastId: 'extendJobError'}, // Added toastId to prevent multiple copies of this growl to show up in one time
    );
    return null;
  };

  return (
    <Modal>
      <div data-cy="extend-jobpost-modal">
        {state.loading ? (
          renderLoader()
        ) : state.error ? (
          renderExtensionError()
        ) : state.value ? (
          <div className="flex-column">{renderExtensionSuccess()}</div>
        ) : (
          <div className="flex-column">{renderExtensionForm()}</div>
        )}
      </div>
    </Modal>
  );
};
