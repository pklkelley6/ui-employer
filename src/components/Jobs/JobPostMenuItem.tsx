import React from 'react';
import {useHover} from 'react-use';
import styles from './JobPostMenuItem.scss';

export interface IJobPostMenuItem {
  title: string;
  onClick?: () => void;
  id: string;
  disabled?: boolean;
  hoverMessage?: string;
}

export const JobPostMenuItem: React.FunctionComponent<IJobPostMenuItem> = ({
  title,
  id,
  onClick,
  disabled,
  hoverMessage,
}) => {
  // Did not use the button's disabled property to disable since useHover does not work properly if it is set
  const menuItem = (hovered: boolean) => (
    <button
      data-cy={id}
      className={`relative pa3 fw6 f6 db ${styles.menuItemButton} ${
        disabled ? `cursor-not-allowed ${styles.disabledItem}` : 'pointer hover-dark-pink'
      }`}
      onClick={onClick}
    >
      {hovered && hoverMessage && (
        <span
          className={`absolute bg-mid-gray white f6 fw4 lh-copy pa3 shadow-1 ${styles.menuItemTooltip}`}
          data-cy={`${id}-tooltip`}
        >
          {hoverMessage}
        </span>
      )}
      {title}
    </button>
  );
  const [menuItemHoverable] = useHover(menuItem);
  return menuItemHoverable;
};
