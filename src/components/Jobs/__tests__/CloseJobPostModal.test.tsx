import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {MemoryRouter as Router} from 'react-router-dom';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {CloseJobPostModal} from '~/components/Jobs/CloseJobPostModal';
import {JobsSortCriteria} from '~/flux/jobPostings';
import * as closeJobServices from '~/services/employer/jobs';

describe('Jobs/CloseJobPostModal', () => {
  const closeJobRequestMock = jest.spyOn(closeJobServices, 'closeJobPosting');
  afterEach(() => {
    closeJobRequestMock.mockClear();
  });

  const mcfJobPostId = 'MCF-2009-1234567';
  const mockProps = {
    closedJobsParams: {
      limit: 20,
      page: 0,
    },
    fetchClosedJobs: jest.fn(),
    fetchOpenJobs: jest.fn(),
    job: {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        jobPostId: mcfJobPostId,
      },
    },
    onCancel: jest.fn(),
    openJobsParams: {
      limit: 20,
      orderBy: JobsSortCriteria.EXPIRY_DATE,
      page: 0,
    },
  };

  it('should show a message prompt if no option is selected when closing a job', () => {
    const optionPrompt = 'Please select an option';
    const component = mount(<CloseJobPostModal {...mockProps} />);
    component.find('[data-cy="proceed-close-job"]').simulate('click');

    expect(component.find('[data-cy="empty-option-error"]')).toHaveLength(1);
    expect(component.find('[data-cy="empty-option-error"]').text()).toEqual(optionPrompt);
  });

  it('should render success message on modal if proceed to close job succeeds', async () => {
    closeJobRequestMock.mockImplementation(() => Promise.resolve());
    const successMessage = `The job post for ${jobPostMock.title} (${mcfJobPostId}) has been closed successfully.`;

    const component = mount(
      <Router>
        <CloseJobPostModal {...mockProps} />
      </Router>,
    );
    component.find('[data-cy="select-type-of-vacancy"]').find('input').first().simulate('change');

    await act(async () => {
      component.find('[data-cy="proceed-close-job"]').simulate('click');
    });
    component.update();

    expect(component.find('[data-cy="close-job-success"]').text()).toEqual(successMessage);
    expect(component.find('[data-cy="return-to-jobs"]')).toHaveLength(1);
    expect(component.find('[data-cy="close-job-module"]')).not.toContain('[data-cy="cancel-close-job"]');
  });

  it('should call onCancel function and dismiss closeJobModal when close job fails', async () => {
    closeJobRequestMock.mockImplementation(() => Promise.reject(new Error()));

    const component = mount(
      <Router>
        <CloseJobPostModal {...mockProps} />
      </Router>,
    );

    component.find('[data-cy="select-type-of-vacancy"]').find('input').first().simulate('change');

    await act(async () => {
      component.find('[data-cy="proceed-close-job"]').simulate('click');
    });
    expect(mockProps.onCancel).toHaveBeenCalled();
  });
});
