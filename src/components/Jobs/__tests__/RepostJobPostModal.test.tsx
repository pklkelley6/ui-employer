import {Interaction, Matchers} from '@pact-foundation/pact';
import {IAccountState} from '@govtechsg/redux-account-middleware';
import {act} from 'react-dom/test-utils';
import {mount} from 'enzyme';
import {noop} from 'lodash/fp';
import {addDays, format, fromUnixTime} from 'date-fns';
import {formatToTimeZone} from 'date-fns-timezone';
import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {RepostJobPostModal} from '../RepostJobPostModal';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {PactBuilder, transformArrayToEachLikeMatcher} from '~/__mocks__/pact';
import {API_JOB_VERSION} from '~/services/util/getApiJobRequestInit';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {IAppState} from '~/flux';
import {IUser} from '~/flux/account';
import {uenUserMock} from '~/__mocks__/account/account.mocks';

const v2ApiMock = jest.fn();
jest.mock('~/config', () => ({
  config: {
    url: {
      apiJob: {
        get v2() {
          return v2ApiMock();
        },
      },
    },
  },
}));

describe('RepostJobPostModal', () => {
  let pactBuilder: PactBuilder;

  const jobUuid = '77e25f585fba0c8c212e8b9f1779755f';
  const jobPosting = {
    ...jobPostMock,
    hiringCompany: undefined,
    uuid: jobUuid,
  };
  const mockSystemContact: IAppState['user'] = {
    contactNumber: '91234567',
    email: 'email@test.mcf',
    designation: 'director',
    name: 'foo bar',
    individualId: 'foobar-individual',
    isThirdParty: false,
    licence: 'license',
    error: false,
    acceptedAt: '2021-08-05T03:46:32',
    loaded: true,
  };
  const mockAccount: IAccountState<IUser> = {
    data: {
      ...uenUserMock,
      exp: 1633061459, // 2021-10-01,
    },
  };
  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-job');
    await pactBuilder.setup();
    v2ApiMock.mockImplementation(() => `http://${pactBuilder.host}:${pactBuilder.port}/v2`);
  });
  beforeEach(async () => {
    const interaction = new Interaction()
      .given(`job ${jobUuid} is closed and has 0 repost count`)
      .uponReceiving('repostJobPosting')
      .withRequest({
        body: {
          jobPostDuration: 30,
        },
        headers: {
          'Content-Type': 'application/json',
          [API_VERSION_HEADER_KEY]: API_JOB_VERSION,
        },
        method: 'PATCH',
        path: `/v2/jobs/77e25f585fba0c8c212e8b9f1779755f/repost`,
      })
      .willRespondWith({
        body: Matchers.like(transformArrayToEachLikeMatcher(jobPosting)),
        status: 200,
      });
    await pactBuilder.provider.addInteraction(interaction);
  });

  afterAll(async () => pactBuilder.provider.finalize());

  it('should call the repost job api and display the correct information', async () => {
    const mockStore = configureStore<{
      user: IAppState['user'];
      releaseToggles: Partial<IAppState['releaseToggles']>;
      account: IAccountState<Pick<IUser, 'exp'>>;
    }>()({
      user: mockSystemContact,
      releaseToggles: {'179716621RepostJobDisplayEmailRecipient': true},
      account: {
        data: {
          exp: 1633061459, // 2021-10-01,
        },
      },
    });
    const wrapper = mount(
      <MemoryRouter>
        <Provider store={mockStore}>
          <RepostJobPostModal onCancel={noop} job={jobPosting} />
        </Provider>
      </MemoryRouter>,
    );

    const mcfSystemDate =
      mockAccount.data &&
      formatToTimeZone(fromUnixTime(mockAccount.data.exp), 'YYYY-MM-DD', {
        timeZone: 'Asia/Singapore',
      });
    const expectedExpiryDate = mcfSystemDate && format(addDays(new Date(mcfSystemDate), 30), 'd MMM yyyy');
    expect(wrapper.find('[data-cy="repost-close-date"]').text()).toEqual(expectedExpiryDate);
    expect(wrapper.find('[data-cy="repost-modal-repost-count"]').text()).toEqual('1 repost');
    expect(wrapper.find('[data-cy="repost-modal-reposter-name"]').text()).toEqual(mockSystemContact.name);
    expect(wrapper.find('[data-cy="repost-modal-reposter-email"]').text()).toEqual(mockSystemContact.email);

    await act(async () => {
      wrapper.find('[data-cy="submit-button-repost-jobpost-modal"]').simulate('click');
    });

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('[data-cy="repost-acknowledgement"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="repost-close-date-acknowledgement"]').text()).toEqual(expectedExpiryDate);
    expect(wrapper.find('[data-cy="repost-acknowledgement-reposter-name"]').text()).toEqual(mockSystemContact.name);
    expect(wrapper.find('[data-cy="repost-acknowledgement-reposter-email"]').text()).toEqual(mockSystemContact.email);
  });

  it('should not display system contact name/email when there is no change in poster', async () => {
    const mockStore = configureStore<{
      user: IAppState['user'];
      releaseToggles: Partial<IAppState['releaseToggles']>;
      account: IAccountState<Pick<IUser, 'exp'>>;
    }>()({
      user: {...mockSystemContact, individualId: jobPostMock.metadata.emailRecipient!},
      releaseToggles: {'179716621RepostJobDisplayEmailRecipient': true},
      account: {
        data: {
          exp: 1633061459, // 2021-10-01,
        },
      },
    });
    const wrapper = mount(
      <Provider store={mockStore}>
        <MemoryRouter>
          <RepostJobPostModal onCancel={noop} job={jobPosting} />
        </MemoryRouter>
        ,
      </Provider>,
    );
    const mcfSystemDate =
      mockAccount.data &&
      formatToTimeZone(fromUnixTime(mockAccount.data.exp), 'YYYY-MM-DD', {
        timeZone: 'Asia/Singapore',
      });
    const expectedExpiryDate = mcfSystemDate && format(addDays(new Date(mcfSystemDate), 30), 'd MMM yyyy');
    expect(wrapper.find('[data-cy="repost-close-date"]').text()).toEqual(expectedExpiryDate);
    expect(wrapper.find('[data-cy="repost-modal-repost-count"]').text()).toEqual('1 repost');
    expect(wrapper.find('[data-cy="repost-modal-reposter-name"]')).toHaveLength(0);
    expect(wrapper.find('[data-cy="repost-modal-reposter-email"]')).toHaveLength(0);

    await act(async () => {
      wrapper.find('[data-cy="submit-button-repost-jobpost-modal"]').simulate('click');
    });

    await act(async () => {
      await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
    });

    expect(wrapper.find('[data-cy="repost-acknowledgement"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="repost-close-date-acknowledgement"]').text()).toEqual(expectedExpiryDate);
    expect(wrapper.find('[data-cy="repost-acknowledgement-reposter-name"]')).toHaveLength(0);
    expect(wrapper.find('[data-cy="repost-acknowledgement-reposter-email"]')).toHaveLength(0);
  });
});
