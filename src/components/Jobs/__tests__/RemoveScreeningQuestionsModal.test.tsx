import React from 'react';
import {Provider} from 'react-redux';
import {mount} from 'enzyme';
import {MockedProvider} from '@apollo/react-testing';
import {act} from 'react-dom/test-utils';
import configureStore from 'redux-mock-store';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {RemoveScreeningQuestionsModal} from '~/components/Jobs/RemoveScreeningQuestionsModal';
import * as removeScreeningQuestions from '~/services/employer/jobs';
import * as GrowlNotification from '~/components/GrowlNotification/GrowlNotification';

describe('Jobs/RemoveScreeningQuestionsModal', () => {
  const removeScreeningQuestionRequestMock = jest.spyOn(removeScreeningQuestions, 'removeScreeningQuestions');
  const growlNotificationMock = jest.spyOn(GrowlNotification, 'growlNotification');

  afterEach(() => {
    removeScreeningQuestionRequestMock.mockClear();
  });

  const store = configureStore()({
    features: {screeningQuestions: true},
  });

  const mockProps = {
    job: {
      ...jobPostMock,
      jobScreeningQuestions: [{question: 'Do you have a driving license?'}],
    },
    onCancel: jest.fn(),
    fetchJob: jest.fn(),
    onJobPostingScreeningQuestionsRemoved: jest.fn(),
  };

  it('should show success message modal when removing job screening questions succeeds and emptied job screening question', async () => {
    removeScreeningQuestionRequestMock.mockImplementation(() => Promise.resolve());

    const component = mount(
      <MockedProvider addTypename={false}>
        <Provider store={store}>
          <RemoveScreeningQuestionsModal {...mockProps} />
        </Provider>
      </MockedProvider>,
    );

    await act(async () => {
      component.find('[data-cy="remove-screening-question-confirm"]').simulate('click');
    });

    component.update();
    expect(component.find('[data-cy="remove-screening-question-modal-success"]')).toHaveLength(1);
  });

  it('should show error growl when removing job screening questions fails', async () => {
    removeScreeningQuestionRequestMock.mockImplementation(() => Promise.reject());

    const component = mount(
      <MockedProvider addTypename={false}>
        <Provider store={store}>
          <RemoveScreeningQuestionsModal {...mockProps} />
        </Provider>
      </MockedProvider>,
    );

    await act(async () => {
      component.find('[data-cy="remove-screening-question-confirm"]').simulate('click');
    });

    component.update();

    expect(growlNotificationMock).toBeCalledWith(
      'Temporarily unable to remove screening questions. Please try again.',
      GrowlNotification.GrowlType.ERROR,
      {
        toastId: 'removeScreeningQuestionsError',
      },
    );
  });
});
