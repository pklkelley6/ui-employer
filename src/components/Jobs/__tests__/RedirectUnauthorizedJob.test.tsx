import React from 'react';
import {mount} from 'enzyme';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {MemoryRouter} from 'react-router-dom';
import {Route, Switch} from 'react-router';
import {RedirectUnauthorizedJob} from '../RedirectUnauthorizedJob';
import {uenUserMock} from '~/__mocks__/account/account.mocks';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {IJobPost} from '~/services/employer/jobs.types';
import {PageUnavailable} from '~/pages/PageUnavailable/PageUnavailable';

describe('RedirectUnauthorizedJob', () => {
  describe('when newErrorPage feature is true', () => {
    const mountComponent = (job: IJobPost) => {
      const reduxState = {
        account: {
          data: uenUserMock,
        },
        features: {
          newErrorPage: true,
        },
      };
      const store = configureStore()(reduxState);
      return mount(
        <Provider store={store}>
          <MemoryRouter>
            <RedirectUnauthorizedJob job={job}>
              <div id="child-component" />
            </RedirectUnauthorizedJob>
          </MemoryRouter>
        </Provider>,
      );
    };

    it('should render page unavailable if account UEN is not the same as job UEN', () => {
      const jobMock = {
        ...jobPostMock,
        postedCompany: {
          ...jobPostMock.postedCompany,
          uen: 'some-other-uen',
        },
      };
      const wrapper = mountComponent(jobMock);
      expect(wrapper.find(PageUnavailable)).toHaveLength(1);
      expect(wrapper.find('#jobs-page')).toHaveLength(0);
      expect(wrapper.find('#child-component')).toHaveLength(0);
    });

    it('should render child component if account UEN matches the job UEN', () => {
      const jobMock = {
        ...jobPostMock,
        postedCompany: {
          ...jobPostMock.postedCompany,
          uen: uenUserMock.userInfo.entityId,
        },
      };
      const wrapper = mountComponent(jobMock);
      expect(wrapper.find(PageUnavailable)).toHaveLength(0);
      expect(wrapper.find('#child-component')).toHaveLength(1);
    });
  });

  describe('RedirectUnauthorizedJob newErrorPage feature false', () => {
    const mountComponent = (job: IJobPost) => {
      const reduxState = {
        account: {
          data: uenUserMock,
        },
        features: {
          newErrorPage: false,
        },
      };
      const store = configureStore()(reduxState);
      return mount(
        <Provider store={store}>
          <MemoryRouter>
            <Switch>
              <Route path="/jobs" render={() => <div id="jobs-page" />} />
              <RedirectUnauthorizedJob job={job}>
                <div id="child-component" />
              </RedirectUnauthorizedJob>
            </Switch>
          </MemoryRouter>
        </Provider>,
      );
    };

    it('should redirect to /jobs page if account UEN is not the same as job UEN', () => {
      const jobMock = {
        ...jobPostMock,
        postedCompany: {
          ...jobPostMock.postedCompany,
          uen: 'some-other-uen',
        },
      };
      const wrapper = mountComponent(jobMock);
      expect(wrapper.find(PageUnavailable)).toHaveLength(0);
      expect(wrapper.find('#jobs-page')).toHaveLength(1);
      expect(wrapper.find('#child-component')).toHaveLength(0);
    });

    it('should render child component if account UEN matches the job UEN', () => {
      const jobMock = {
        ...jobPostMock,
        postedCompany: {
          ...jobPostMock.postedCompany,
          uen: uenUserMock.userInfo.entityId,
        },
      };
      const wrapper = mountComponent(jobMock);
      expect(wrapper.find(PageUnavailable)).toHaveLength(0);
      expect(wrapper.find('#jobs-page')).toHaveLength(0);
      expect(wrapper.find('#child-component')).toHaveLength(1);
    });
  });
});
