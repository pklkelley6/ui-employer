import {MockedProvider} from '@apollo/react-testing';
import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import Tooltip from 'rc-tooltip';
import {JobPostMenu} from '../JobPostMenu';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {JobsSortCriteria} from '~/flux/jobPostings';
import {JobStatusCodes} from '~/services/employer/jobs.types';

describe('Jobs/JobPostMenu', () => {
  const store = configureStore()({
    jobPostings: {
      closedJobsParams: {
        limit: 20,
        page: 0,
      },
      openJobsParams: {
        limit: 20,
        orderBy: JobsSortCriteria.EXPIRY_DATE,
        page: 0,
      },
    },
    features: {screeningQuestions: true},
  });

  describe('when job is a MCF job with prefix starting with "MCF"', () => {
    const MCFJobId = 'MCF-2009-7654321';
    const mock: any = {};
    const routeComponentProps = {
      history: {
        ...mock,
        push: jest.fn(),
      },
      location: mock,
      match: mock,
    };
    const jobProps = {
      job: {
        ...jobPostMock,
        metadata: {
          ...jobPostMock.metadata,
          jobPostId: MCFJobId,
        },
      },
      ...routeComponentProps,
    };
    it('should display MCFJobPostMenu when job status is open', () => {
      const mockProps = {
        ...jobProps,
      };
      const component = mount(
        <MockedProvider addTypename={false}>
          <Provider store={store}>
            <MemoryRouter>
              <JobPostMenu {...mockProps} />
            </MemoryRouter>
          </Provider>
        </MockedProvider>,
      );
      expect(component.find(Tooltip)).toHaveLength(1);
      expect(component.find('[data-cy="manage-job-post-button"]')).toHaveLength(1);
    });

    it('should display MCFRepostJobPostButton when job status is closed', () => {
      const mockProps = {
        ...jobProps,
        features: {viewMCFClosedJobDetails: true},
        job: {
          ...jobProps.job,
          status: {
            id: JobStatusCodes.Closed,
            jobStatus: 'Closed',
          },
        },
      };
      const component = mount(
        <MockedProvider addTypename={false}>
          <Provider store={store}>
            <MemoryRouter>
              <JobPostMenu {...mockProps} />
            </MemoryRouter>
          </Provider>
        </MockedProvider>,
      );
      expect(component.find('[data-cy="repost-job-post-button"]')).toHaveLength(1);
    });
  });

  describe('when job is a non MCF job with prefix starting with "JOB"', () => {
    const nonMCFJobId = 'JOB-2009-7654321';
    const mock: any = {};
    const routeComponentProps = {
      history: {
        ...mock,
        push: jest.fn(),
      },
      location: mock,
      match: mock,
    };
    const jobProps = {
      job: {
        ...jobPostMock,
        metadata: {
          ...jobPostMock.metadata,
          jobPostId: nonMCFJobId,
        },
      },
      ...routeComponentProps,
    };

    it('should display ViewJobPostButton and not render menu dropdown', () => {
      const component = mount(
        <MockedProvider addTypename={false}>
          <Provider store={store}>
            <MemoryRouter>
              <JobPostMenu {...jobProps} />
            </MemoryRouter>
          </Provider>
        </MockedProvider>,
      );
      expect(component.find('[data-cy="view-job-post-button"]')).toHaveLength(1);
    });
  });
});
