import {MockedProvider} from '@apollo/react-testing';
import {mount, ReactWrapper} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {JobPostingContact} from '../JobPostingContact';
import {GET_JOB_SYSTEM_CONTACT} from '~/graphql/systemContact/systemContact.query';
import {nextTick} from '~/testUtil/enzyme';

describe('JobPostingContact', () => {
  const jobWithEmailRecipient = 'jobWithEmailRecipient';
  const jobWithoutEmailRecipient = 'jobWithoutEmailRecipient';
  const nonExistingJob = 'nonExistingJob';

  const jobByUuidMocks = [
    {
      request: {
        query: GET_JOB_SYSTEM_CONTACT,
        variables: {
          jobUuid: jobWithEmailRecipient,
        },
      },
      result: {
        data: {
          jobByUuid: {
            emailRecipientContact: {
              name: 'emailRecipientContact name',
              email: 'emailRecipientContact email',
            },
            createdByContact: {
              name: 'createdByContact name',
              email: 'createdByContact email',
            },
          },
        },
      },
    },
    {
      request: {
        query: GET_JOB_SYSTEM_CONTACT,
        variables: {
          jobUuid: jobWithoutEmailRecipient,
        },
      },
      result: {
        data: {
          jobByUuid: {
            emailRecipientContact: null,
            createdByContact: {
              name: 'createdByContact name',
              email: 'createdByContact email',
            },
          },
        },
      },
    },
    {
      request: {
        query: GET_JOB_SYSTEM_CONTACT,
        variables: {
          jobUuid: nonExistingJob,
        },
      },
      error: new Error('cannot find job'),
      result: {
        data: {
          jobUuid: null,
        },
      },
    },
  ];

  it('should render emailRecipientContact', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <MockedProvider mocks={jobByUuidMocks} addTypename={false}>
          <JobPostingContact jobUuid={jobWithEmailRecipient} />
        </MockedProvider>,
      );
    });

    await nextTick(wrapper);
    expect(wrapper.find('[data-cy="job-posting-contact"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="job-posting-contact-email"]').text()).toEqual('emailRecipientContact email');
  });

  it('should render createdByContact if emailRecipientContact is null', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <MockedProvider mocks={jobByUuidMocks} addTypename={false}>
          <JobPostingContact jobUuid={jobWithoutEmailRecipient} />
        </MockedProvider>,
      );
    });

    await nextTick(wrapper);
    expect(wrapper.find('[data-cy="job-posting-contact"]')).toHaveLength(1);
    expect(wrapper.find('[data-cy="job-posting-contact-email"]').text()).toEqual('createdByContact email');
  });

  it('should not render anything if there is an error fetching for job', async () => {
    let wrapper!: ReactWrapper;
    await act(async () => {
      wrapper = mount(
        <MockedProvider mocks={jobByUuidMocks} addTypename={false}>
          <JobPostingContact jobUuid={nonExistingJob} />
        </MockedProvider>,
      );
    });

    await nextTick(wrapper);
    expect(wrapper.find('[data-cy="job-posting-contact"]')).toHaveLength(0);
  });
});
