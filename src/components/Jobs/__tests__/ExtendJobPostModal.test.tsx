import {addDays, differenceInDays, format, parseISO} from 'date-fns';
import {mount} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {Provider} from 'react-redux';
import {MemoryRouter as Router} from 'react-router-dom';
import configureStore from 'redux-mock-store';
import {getDurationOptions} from '../ExtendJobPostModal';
import {ExtendJobPostModalContainer} from '../ExtendJobPostModalContainer';
import {jobPostMock} from '~/__mocks__/jobs/jobs.mocks';
import {Dropdown} from '~/components/Core/Dropdown';
import {JobsSortCriteria} from '~/flux/jobPostings';
import * as updateJobServices from '~/services/employer/jobs';
import {formatISODate} from '~/util/date';

describe('Jobs/ExtendJobPostModal', () => {
  const updateJobRequestMock = jest.spyOn(updateJobServices, 'extendJobPost');
  afterEach(() => {
    updateJobRequestMock.mockClear();
  });

  const store = configureStore()({
    jobPostings: {
      openJobsParams: {
        limit: 20,
        orderBy: JobsSortCriteria.EXPIRY_DATE,
        page: 0,
      },
    },
  });

  const props = {
    job: {
      ...jobPostMock,
      metadata: {
        ...jobPostMock.metadata,
        expiryDate: '2018-02-01T00:00:00.000Z',
        jobPostId: jobPostMock.metadata.jobPostId,
        newPostingDate: '2018-01-25T00:00:00.000Z',
      },
    },
    onCancel: jest.fn(),
  };

  const {expiryDate, newPostingDate} = props.job.metadata;
  const jobDuration = differenceInDays(parseISO(expiryDate), parseISO(newPostingDate));
  const newDuration = getDurationOptions(jobDuration)[0].value;
  const getNewExpiryDate = addDays(parseISO(newPostingDate), newDuration);

  it('should match the selected duration to the new extended expiry date displayed', () => {
    const component = mount(
      <Provider store={store}>
        <Router>
          <ExtendJobPostModalContainer {...props} />
        </Router>
      </Provider>,
    );
    const dropdown = component.find(Dropdown);
    expect(dropdown).toHaveLength(1);
    expect(dropdown.prop('input').value).toEqual(newDuration);
    expect(component.find('[data-cy="extend-jobpost-modal-original-expiry-date"]').text()).toEqual(
      `${formatISODate(expiryDate)}`,
    );
    expect(component.find('[data-cy="extend-jobpost-modal-new-date"]').text()).toEqual(
      `${format(getNewExpiryDate, 'd MMM yyyy')}.`,
    );
  });

  it('should display a success message when extend job post is successful', async () => {
    updateJobRequestMock.mockImplementation(() => Promise.resolve(jobPostMock));
    const successMessage = `The job posting duration for ${jobPostMock.title} (${jobPostMock.metadata.jobPostId}) has been extended successfully.`;

    const component = mount(
      <Provider store={store}>
        <Router>
          <ExtendJobPostModalContainer {...props} />
        </Router>
      </Provider>,
    );
    await act(async () => {
      component.find('[data-cy="extend-button-extend-jobpost-modal"]').simulate('click');
    });

    component.update();
    expect(component.find('[data-cy="extend-job-success"]').text()).toEqual(successMessage);
    expect(component.find('[data-cy="return-to-jobs-details"]')).toHaveLength(1);
    expect(component.find('[data-cy="extend-jobpost-new-expiry-date"]').text()).toEqual(
      `${format(getNewExpiryDate, 'd MMM yyyy')}`,
    );
    expect(component.find('[data-cy="close-job-module"]')).not.toContain(
      '[data-cy="cancel-button-extend-jobpost-modal"]',
    );
  });

  it('should call onCancel function and dismiss extendJobPostModal when extension fails', async () => {
    updateJobRequestMock.mockImplementation(() => Promise.reject(new Error()));
    const component = mount(
      <Provider store={store}>
        <Router>
          <ExtendJobPostModalContainer {...props} />
        </Router>
      </Provider>,
    );
    await act(async () => {
      component.find('[data-cy="extend-button-extend-jobpost-modal"]').simulate('click');
    });

    component.update();
    expect(props.onCancel).toHaveBeenCalled();
  });
});
