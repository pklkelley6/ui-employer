import {mount} from 'enzyme';
import React from 'react';
import {JobsQuery} from '../JobsQuery';
import {
  OPEN_JOBS_RECEIVED,
  OPEN_JOBS_REQUEST_FAILED,
  OPEN_JOBS_REQUESTED,
} from '~/flux/jobPostings/jobPostings.constants';

describe('Jobs/JobsQuery', () => {
  it('should render JobsQuery with loading = true', () => {
    const childrenMock = jest.fn();
    const jobQueryProps = {
      children: childrenMock,
      payload: {
        results: [],
        total: 0,
      },
      status: OPEN_JOBS_REQUESTED,
    };
    childrenMock.mockReturnValue(null);

    mount(<JobsQuery {...jobQueryProps} />);

    expect(childrenMock).toBeCalledWith({
      data: {
        results: [],
        total: 0,
      },
      error: undefined,
      loading: true,
    });
  });

  it('should render JobsQuery with loading = false', () => {
    const childrenMock = jest.fn();
    const jobQueryProps = {
      children: childrenMock,
      payload: {
        results: [],
        total: 0,
      },
      status: OPEN_JOBS_RECEIVED,
    };
    childrenMock.mockReturnValue(null);

    mount(<JobsQuery {...jobQueryProps} />);

    expect(childrenMock).toBeCalledWith({
      data: {
        results: [],
        total: 0,
      },
      error: undefined,
      loading: false,
    });
  });

  it('should render JobsQuery with error', () => {
    const childrenMock = jest.fn();
    const jobQueryProps = {
      children: childrenMock,
      payload: {
        results: [],
        total: 0,
      },
      status: OPEN_JOBS_REQUEST_FAILED,
    };
    childrenMock.mockReturnValue(null);

    mount(<JobsQuery {...jobQueryProps} />);

    expect(childrenMock).toBeCalledWith({
      data: {
        results: [],
        total: 0,
      },
      error: 'Error',
      loading: false,
    });
  });
});
