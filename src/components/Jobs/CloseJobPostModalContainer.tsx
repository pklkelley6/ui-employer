import {connect} from 'react-redux';
import {ActionType} from 'typesafe-actions';
import {CloseJobPostModal} from './CloseJobPostModal';
import {IAppState} from '~/flux/index';
import {fetchClosedJobsRequested, fetchOpenJobsRequested} from '~/flux/jobPostings';
import {IFetchClosedJobs, IFetchOpenJobs} from '~/flux/jobPostings/jobPostings.types';
import {IJobPost} from '~/services/employer/jobs.types';

export interface ICloseJobPostModalProps {
  onCancel: () => void;
  job: IJobPost;
  closedJobsParams: IFetchClosedJobs;
  openJobsParams: IFetchOpenJobs;
}

export const mapStateToProps = ({jobPostings: {closedJobsParams, openJobsParams}}: IAppState) => ({
  closedJobsParams,
  openJobsParams,
});

export interface ICloseJobPostModalContainerDispatchProps {
  fetchClosedJobs: (closedJobsParams: IFetchClosedJobs) => ActionType<typeof fetchClosedJobsRequested>;
  fetchOpenJobs: (openJobsParams: IFetchOpenJobs) => ActionType<typeof fetchOpenJobsRequested>;
}

export const CloseJobPostModalContainer = connect(mapStateToProps, {
  fetchClosedJobs: fetchClosedJobsRequested,
  fetchOpenJobs: fetchOpenJobsRequested,
})(CloseJobPostModal);

export type ICloseJobPostModalComponentProps = ICloseJobPostModalContainerDispatchProps & ICloseJobPostModalProps;
