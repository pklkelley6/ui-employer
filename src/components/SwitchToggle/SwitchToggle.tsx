import React from 'react';
import styles from './SwitchToggle.scss';

export interface ISwitchToggleProps {
  id: string;
  input: {
    value: boolean;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  };
}

export const SwitchToggle: React.FunctionComponent<any> = ({id, input}) => {
  const {value, onChange} = input;
  return (
    <>
      <input
        id={`${id}-switch-toggle-checkbox`}
        data-cy={`${id}-switch-toggle-checkbox`}
        type="checkbox"
        className={styles.switchToggleCheckbox}
        onChange={onChange}
        checked={value}
      />
      <label className={styles.switchToggle} htmlFor={`${id}-switch-toggle-checkbox`} />
    </>
  );
};
