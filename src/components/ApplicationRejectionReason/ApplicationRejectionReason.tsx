import React, {useState} from 'react';
import {stateFromHTML} from 'draft-js-import-html';
import {Field, Form} from 'react-final-form';
import {useMutation} from 'react-apollo';
import {Condition} from '~/components/Core/Condition';
import {Dropdown} from '~/components/Core/Dropdown';
import {TextEditor} from '~/components/JobPosting/TextEditor';
import {composeValidators, maxLength, required, defaultRequired} from '~/util/fieldValidations';
import styles from '~/components/ApplicationRejectionReason/ApplicationRejectionReason.scss';
import {growlNotification, GrowlType} from '~/components/GrowlNotification/GrowlNotification';
import {SET_APPLICATION_REJECTION_REASON} from '~/graphql/applications/applications.query';
import {Mutation, MutationSetApplicationRejectionReasonArgs} from '~/graphql/__generated__/types';

export const applicationRejectionReasonOptions = [
  {
    label: 'Did not turn up for interview',
    value: 'Did not turn up for interview',
  },
  {
    label: 'Did not meet competency requirements',
    value: 'Did not meet competency requirements',
  },
  {
    label: 'Did not have enough relevant experience',
    value: 'Did not have enough relevant experience',
  },
  {
    label: 'Did not fit company culture',
    value: 'Did not fit company culture',
  },
  {
    label: 'Turned down job offer',
    value: 'Turned down job offer',
  },
  {
    label: 'Others',
    value: 'Others',
  },
];

const OTHERS_REJECT_REASON_MAX_LENGTH = 1000;

interface IApplicationRejectionReasonProps {
  applicationId: string;
  applicationRejectReason?: string;
  othersRejectReason?: string;
}

interface IApplicationRejectReasonState {
  applicationRejectReason: string;
  othersRejectReason?: string;
}

const getValueFromHTML = (value?: string) =>
  stateFromHTML(value || '')
    .getPlainText()
    .trim();

export const ApplicationRejectionReason: React.FunctionComponent<IApplicationRejectionReasonProps> = ({
  applicationId,
  applicationRejectReason,
  othersRejectReason,
}) => {
  const initialValues: IApplicationRejectReasonState = {
    applicationRejectReason: applicationRejectReason ?? '',
    othersRejectReason,
  };

  const [editable, setEditable] = useState(!applicationRejectReason);
  const [setApplicationRejectionReason] = useMutation<
    {setApplicationRejectionReason: Mutation['setApplicationRejectionReason']},
    MutationSetApplicationRejectionReasonArgs
  >(SET_APPLICATION_REJECTION_REASON, {
    onCompleted: () => setEditable(false),
    onError: () => {
      growlNotification('Temporarily unable to save reason. Please try again.', GrowlType.ERROR, {
        toastId: 'saveApplicationRejectionReasonError',
      });
    },
  });

  const editableMode = (
    <div data-cy="application-rejection-reason" className="pb3 bb b--black-05 mb4">
      <div className="f5">
        <h4 data-cy="application-rejection-reason-title" className="f5-5 ma0 mb3 black-70 fw6">
          Reason for unsuccessful status
        </h4>
        <p className="black-50">
          This reason will not be shared with the applicant, but may be used to analyse why this applicant was not
          selected for the job.
        </p>
      </div>
      <Form<IApplicationRejectReasonState>
        onSubmit={async ({applicationRejectReason, othersRejectReason}: IApplicationRejectReasonState) =>
          await setApplicationRejectionReason({
            variables: {
              applicationId,
              rejectionReason: applicationRejectReason,
              rejectionReasonExtended: applicationRejectReason === 'Others' ? othersRejectReason : undefined,
            },
          })
        }
        initialValues={initialValues}
        render={({handleSubmit}) => {
          return (
            <form onSubmit={handleSubmit}>
              <div className={styles.optionsDropdown}>
                <Field<string> name="applicationRejectReason" validate={required('Please select an option')}>
                  {({input, meta}) => {
                    return (
                      <Dropdown
                        innerProps={{
                          'data-cy': 'application-rejection-reason-dropdown',
                        }}
                        placeholder="Select one"
                        data={applicationRejectionReasonOptions}
                        input={input}
                        meta={meta}
                      />
                    );
                  }}
                </Field>
              </div>
              <div className={`pt2 pb2 w-100 ${styles.textBox}`} data-cy="application-rejection-reason-others">
                <Condition when="applicationRejectReason" is="Others">
                  <Field<string>
                    name="othersRejectReason"
                    parse={(value) => getValueFromHTML(value)}
                    validate={(value) =>
                      composeValidators(
                        defaultRequired,
                        maxLength(
                          OTHERS_REJECT_REASON_MAX_LENGTH,
                          `Please keep within ${OTHERS_REJECT_REASON_MAX_LENGTH.toLocaleString()} characters`,
                        ),
                      )(value)
                    }
                  >
                    {({input, meta}) => {
                      return (
                        <TextEditor
                          input={input}
                          maxLength={OTHERS_REJECT_REASON_MAX_LENGTH}
                          meta={meta}
                          showToolbar={false}
                        />
                      );
                    }}
                  </Field>
                </Condition>
              </div>
              <button
                data-cy="application-rejection-reason-save-button"
                className={`pa3 bg-primary ${styles.saveButton}`}
                type="submit"
              >
                Save
              </button>
            </form>
          );
        }}
      />
    </div>
  );

  const readonlyMode = (
    <section className="bb b--black-05 mb4">
      <div className="flex justify-between">
        <div className="pr2 w-80 pb2 mb3">
          <h4 data-cy="readonly-application-rejection-reason-header" className="f5-5 ma0 mb3 black-70 fw6">
            Reason for unsuccessful status
          </h4>
          <p data-cy="readonly-application-rejection-reason" className="f4-5 mv2 black-80 lh-copy">
            {applicationRejectReason}
          </p>
          {othersRejectReason ? (
            <p data-cy="readonly-application-rejection-reason-others" className="f5 mv2 black-80 lh-copy">
              {othersRejectReason}
            </p>
          ) : (
            ''
          )}
        </div>
        <div>
          <a
            data-cy="readonly-application-rejection-reason-edit-button"
            className="icon-edit f5-5 blue pointer"
            onClick={() => setEditable(true)}
          >
            Edit
          </a>
        </div>
      </div>
    </section>
  );

  return editable ? editableMode : readonlyMode;
};
