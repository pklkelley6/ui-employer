import {mount, ReactWrapper} from 'enzyme';
import React from 'react';
import {act} from 'react-dom/test-utils';
import {ApolloProvider} from 'react-apollo';
import {ApolloGraphQLInteraction, Matchers} from '@pact-foundation/pact';
import {print} from 'graphql';
import toJson from 'enzyme-to-json';
import {nextTick, cleanSnapshot} from '../../../testUtil/enzyme';
import {ApplicationRejectionReason} from '../ApplicationRejectionReason';
import {Dropdown} from '~/components/Core/Dropdown';
import {PactBuilder} from '~/__mocks__/pact';
import {SET_APPLICATION_REJECTION_REASON} from '~/graphql/applications/applications.query';
import {MutationSetApplicationRejectionReasonArgs} from '~/graphql/__generated__/types';
import {TextEditor} from '~/components/JobPosting/TextEditor';
import * as GrowlNotification from '~/components/GrowlNotification/GrowlNotification';
import {API_VERSION_HEADER_KEY} from '~/services/util/constants';
import {API_PROFILE_VERSION} from '~/graphql';

let pactBuilder: PactBuilder;

describe('ApplicationRejectionReason', () => {
  let wrapper: ReactWrapper;
  const pactApplicationIds = ['Pact-1004', 'Pact-1005'];
  const growlNotificationMock = jest.spyOn(GrowlNotification, 'growlNotification');

  beforeAll(async () => {
    pactBuilder = new PactBuilder('api-profile');
    await pactBuilder.setup();
  });

  afterAll(async () => pactBuilder.provider.finalize());

  describe('editable mode', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should display dropdown box when there is no existing rejection reason', () => {
      wrapper = mountComponentWithoutRejectReason(pactApplicationIds[0]);
      expect(wrapper.find(Dropdown)).toHaveLength(1);
      expect(wrapper.find(TextEditor)).toHaveLength(0);
    });

    it('should display the others TextEditor if selected rejection reason is Others', async () => {
      wrapper = mountComponentWithoutRejectReason(pactApplicationIds[0]);
      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange('Others');
      });
      await nextTick(wrapper);

      expect(wrapper.find(TextEditor)).toHaveLength(1);
    });

    it('rejection reason has been selected, save button clicked and request sent successfully', async () => {
      wrapper = mountComponentWithoutRejectReason(pactApplicationIds[0]);
      addInteraction({
        description: 'request setApplicationRejectionReason for application',
        variables: {
          applicationId: pactApplicationIds[0],
          rejectionReason: 'Turned down job offer',
          rejectionReasonExtended: undefined,
        },
        response: {
          body: {
            data: {
              setApplicationRejectionReason: {
                id: Matchers.like(pactApplicationIds[0]),
                rejectionReason: 'Turned down job offer',
                rejectionReasonExtended: undefined,
              },
            },
          },
          status: 200,
        },
      });

      await act(async () => {
        wrapper.find(Dropdown).prop('input').onChange('Turned down job offer');
      });
      await nextTick(wrapper);

      await act(async () => {
        wrapper.find('[data-cy="application-rejection-reason-save-button"]').simulate('submit');
      });

      await act(async () => {
        await pactBuilder.verifyInteractions({wrapperToUpdate: wrapper});
      });

      expect(growlNotificationMock).toBeCalledTimes(0);
    });
  });

  describe('readonly mode', () => {
    beforeEach(() => {
      wrapper = mount(
        <ApolloProvider client={pactBuilder.getApolloClient()}>
          <ApplicationRejectionReason
            applicationId={pactApplicationIds[0]}
            applicationRejectReason="Others"
            othersRejectReason="Cannot recite the litany against fear"
          />
        </ApolloProvider>,
      );
    });

    it('displays readonly data without editable fields', () => {
      expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
    });

    it('should switch to edit mode when the edit button is clicked', async () => {
      await act(async () => {
        wrapper.find('[data-cy="readonly-application-rejection-reason-edit-button"]').simulate('click');
      });
      await nextTick(wrapper);

      expect(wrapper.find('[data-cy="readonly-application-rejection-reason-edit-button"]')).toHaveLength(0);
      expect(wrapper.find('[data-cy="application-rejection-reason-dropdown"]')).toHaveLength(1);
      expect(wrapper.find('[data-cy="application-rejection-reason-others"]')).toHaveLength(1);
    });
  });
});

const addInteraction = async ({
  description,
  variables,
  response,
}: {
  description: string;
  variables: MutationSetApplicationRejectionReasonArgs;
  response: any;
}) => {
  const interaction = new ApolloGraphQLInteraction()
    .uponReceiving(description)
    .withQuery(print(SET_APPLICATION_REJECTION_REASON))
    .withOperation('setApplicationRejectionReason')
    .withRequest({
      headers: {
        'content-type': 'application/json',
        [API_VERSION_HEADER_KEY]: API_PROFILE_VERSION,
      },
      method: 'POST',
      path: '/profile',
    })
    .withVariables(variables)
    .willRespondWith(response);
  await pactBuilder.provider.addInteraction(interaction);
};

const mountComponentWithoutRejectReason = (applicationId: string) => {
  return mount(
    <ApolloProvider client={pactBuilder.getApolloClient()}>
      <ApplicationRejectionReason applicationId={applicationId} />
    </ApolloProvider>,
  );
};
