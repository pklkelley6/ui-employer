import {connect} from 'react-redux';
import {Banner} from './Banner';
import {login} from '~/flux/account';
import {onCorppassGetStartedClicked} from '~/flux/analytics';

export interface IBannerProps {
  login: typeof login;
  onCorppassGetStartedClicked: typeof onCorppassGetStartedClicked;
}

export const BannerContainer = connect(null, {login, onCorppassGetStartedClicked})(Banner);
