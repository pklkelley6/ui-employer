import React from 'react';
import Slider from 'react-slick';
import {Row} from './../Layouts/Row';
import styles from './banner.scss';
import {IBannerProps} from './BannerContainer';
import {LoginButton} from '~/components/Login';
import {BannerItem} from '~/components/Banner/BannerItem';

export const Banner: React.FunctionComponent<IBannerProps> = ({login, onCorppassGetStartedClicked}) => {
  const settings = {
    arrows: false,
    autoplay: true,
    autoplaySpeed: 8000,
    dots: true,
    draggable: false,
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 1,
    speed: 500,
    vertical: true,
  };

  const banners = [
    {
      text: 'Not enough applicants? Find a list of potential candidates who are open to being contacted for opportunities.',
      title: 'Discover suggested talents',
    },
    {
      text: 'Add screening questions to your job post to help you find the most suitable candidates.',
      title: 'Screen applicants quickly',
    },
  ];

  return (
    <Row className={`w-100 white container ${styles.banner}`}>
      <section className="w-100 mw9 center flex items-center justify-between ph4 pv5 relative">
        <div className={`${styles.textContainer}`} data-cy="employer-carousel">
          <Slider {...settings}>
            {banners.map((item, index) => (
              <BannerItem key={index} id={index} {...item} />
            ))}
          </Slider>
        </div>
        <div data-cy="banner-corppass-login" className={`tc bg-black-30 pa4 z-1 ${styles.bannerLoginContainer}`}>
          <h3 className="f3 fw3 white pb2">For Employers</h3>
          <LoginButton onClick={login} />
          <a
            data-cy="banner-corppass-help"
            className="white-80 pl1 f6 mv3 db"
            href="/corppass-help"
            onClick={() => onCorppassGetStartedClicked()}
          >
            Need help logging in?
          </a>
        </div>
      </section>
    </Row>
  );
};
