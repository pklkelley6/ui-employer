import {cssTransition, toast, ToastOptions} from 'react-toastify';
import styles from './GrowlNotification.scss';

export enum GrowlType {
  ERROR = 'error',
  SUCCESS = 'success',
}

const SlideTop = cssTransition({
  duration: [500, 500],
  enter: styles.slideInTop,
  exit: styles.slideOutTop,
});

export const growlNotification = (message: string | JSX.Element, type: string, options?: ToastOptions) => {
  const defaultSettings: ToastOptions = {
    autoClose: 5000,
    className: `${styles.growl} ${type === GrowlType.ERROR ? `${styles.error}` : `${styles.success}`}`,
    closeButton: false,
    draggable: false,
    hideProgressBar: true,
    position: toast.POSITION.TOP_CENTER,
    transition: SlideTop,
  };
  toast(message, {...defaultSettings, ...options});
};

export const maxSelectedSkillsGrowl = (maxSelectedSkills: number) =>
  growlNotification(`You can only include up to ${maxSelectedSkills} skills`, GrowlType.ERROR, {
    toastId: 'maxSelectedSkills', // Added toastId to prevent multiple copies of this growl to show up in one time
  });
