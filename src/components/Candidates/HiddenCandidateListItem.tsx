import React from 'react';
import styles from './CandidatesList.scss';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {HiddenCandidate} from '~/graphql/__generated__/types';

export interface IHiddenCandidateListItemProps {
  isViewed: boolean;
  selected: boolean;
  isBookmarked: boolean;
  candidate: HiddenCandidate;
  candidateType: CandidateType;
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const HiddenCandidateListItem: React.FunctionComponent<IHiddenCandidateListItemProps> = ({
  selected,
  candidate,
  candidateType,
  onClick,
  isViewed,
  isBookmarked,
}) => {
  const bgColorClassName = isViewed ? `${styles.cardViewed} bg-white-80` : 'bg-white';
  const selectedClassName = selected ? `${styles.cardActive}` : '';

  return (
    <div className="flex items-center w-100" data-cy="candidates-list-item">
      <div
        className={`flex items-center flex-auto lh-copy pa2 pl0 mb1 relative ${styles.card} ${bgColorClassName} ${selectedClassName}`}
      >
        <div className={'b--blue o-60 bw2 bl db self-stretch ml2 pr2'} />
        <div className="flex justify-between items-center flex-auto pointer relative" onClick={onClick}>
          <div className="pl2 mr2 pv2 pr2 flex-auto">
            <div className="flex items-center justify-between">
              <h3
                data-cy="hidden-candidate-list-name"
                className={'primary fw6 ma0 f4-5 lh-title truncate underline-hover'}
                title={candidate.name}
              >
                {candidate.name}
              </h3>
            </div>
            <div className="db pt0 black-50 f6 truncate lh-title mt1 i">
              This {candidateType.toLowerCase()} is no longer available
            </div>
          </div>
        </div>
        <a className={`tc bl b--moon-gray dark-blue pl2 o-20 cursor-not-allowed ${styles.resumeBox}`}>
          <i className={styles.iconDownload} />
          <span className="f6-5 fw4 db blue">Get Resume</span>
        </a>
        {isBookmarked ? (
          <div className={styles.iconBookmark} data-cy="candidates-list-item-bookmark-indicator" />
        ) : null}
      </div>
    </div>
  );
};
