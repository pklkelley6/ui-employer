import {mount} from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import toJson from 'enzyme-to-json';
import {HiddenCandidateListItem} from '../HiddenCandidateListItem';
import {cleanSnapshot} from '~/testUtil/enzyme';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';

describe('Candidates/HiddenCandidatesListItem', () => {
  const store = configureStore()();
  const hiddenCandidate = {
    name: 'dragon',
  };
  it('should render HiddenCandidateListItem', () => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateListItem
          candidate={hiddenCandidate}
          selected={true}
          candidateType={CandidateType.SuggestedTalent}
          onClick={jest.fn()}
          isViewed={true}
          isBookmarked={true}
        />
      </Provider>,
    );
    expect(toJson(wrapper.find(HiddenCandidateListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it.each`
    type                             | result
    ${CandidateType.Applicant}       | ${'applicant'}
    ${CandidateType.SuggestedTalent} | ${'talent'}
  `('should display message with relevant candidate type: $type', ({type, result}) => {
    const wrapper = mount(
      <Provider store={store}>
        <HiddenCandidateListItem
          candidate={hiddenCandidate}
          selected={true}
          candidateType={type}
          onClick={jest.fn()}
          isViewed={true}
          isBookmarked={true}
        />
      </Provider>,
    );

    const hiddenCandidateListItem = wrapper.find(HiddenCandidateListItem);
    expect(hiddenCandidateListItem.text()).toContain(`This ${result} is no longer available`);
  });
});
