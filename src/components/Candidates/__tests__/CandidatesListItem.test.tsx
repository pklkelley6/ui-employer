import {mcf} from '@mcf/constants';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import {set} from 'lodash/fp';
import React from 'react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {applicationMock} from '~/__mocks__/applications/applications.mocks';
import {CandidatesListItem} from '~/components/Candidates/CandidatesListItem';
import {CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {TopMatcherBadge} from '~/components/ScoreBadge/TopMatcherBadge';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('Candidates/CandidatesListItem', () => {
  const store = configureStore()();
  const candidatesListItemBaseProps = {
    candidate: applicationMock.applicant,
    candidateType: CandidateType.Applicant,
    checked: false,
    date: '',
    isViewed: false,
    onClick: jest.fn(),
    onResumeClick: jest.fn(),
    selected: false,
    isBookmarked: false,
  };
  it('should render CandidatesListItem', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      candidate: set('resume.lastDownloadedAt', '2018-01-01T00:00:00.000', applicationMock.applicant),
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    expect(toJson(wrapper.find(CandidatesListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it('should render CandidatesListItem with selected Candidates', () => {
    const candidatesListItemProps = {...candidatesListItemBaseProps, selected: true};
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    expect(toJson(wrapper.find(CandidatesListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it('should change CandidatesListItem with checked color if the candidate is both checked and viewed', () => {
    const candidatesListItemProps = {...candidatesListItemBaseProps, checked: true, isViewed: true, selected: true};
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    expect(toJson(wrapper.find(CandidatesListItem), cleanSnapshot())).toMatchSnapshot();
  });

  it('should not display date if resume.lastDownloadedAt is not provided', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      candidate: set('resume.lastDownloadedAt', null, applicationMock.applicant),
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const lastDownloadedAtSpan = wrapper.find('[data-cy="candidates-resume-downloaded-date"]');
    expect(lastDownloadedAtSpan).toMatchSnapshot();
  });

  it('should reduce the opacity of download button when there is no resume', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      candidate: set('resume', undefined, applicationMock.applicant),
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const downloadButton = wrapper.find('[data-cy="candidates-list-item-download-button"]');
    expect(downloadButton).toMatchSnapshot();
  });

  it('should render Among top matchers badge when showTopMatch is true', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      candidate: set('resume', undefined, applicationMock.applicant),
      showTopMatch: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const topMatchBadge = wrapper.find(TopMatcherBadge);
    expect(topMatchBadge).toMatchSnapshot();
  });

  it('should render hired application status label when status is hired', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      applicationStatus: {
        label: 'Hired',
        value: mcf.JOB_APPLICATION_STATUS.SUCCESSFUL,
      },
      candidate: set('resume', undefined, applicationMock.applicant),
      showTopMatch: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const applicationStatusLabel = wrapper.find('[data-cy="application-status-label"]');
    expect(applicationStatusLabel).toMatchSnapshot();
  });
  it('should render To Interview application status label when application status is under review and To Interview', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      applicationStatus: {
        label: 'To Interview',
        value: mcf.JOB_APPLICATION_STATUS.UNDER_REVIEW,
      },
      candidate: set('resume', undefined, applicationMock.applicant),
      showTopMatch: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const applicationStatusLabel = wrapper.find('[data-cy="application-status-label"]');
    expect(applicationStatusLabel).toMatchSnapshot();
  });
  it('should render unsuccessful application status label when application status is unsuccessful', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      applicationStatus: {
        label: 'Unsuccessful',
        value: mcf.JOB_APPLICATION_STATUS.UNSUCCESSFUL,
      },
      candidate: set('resume', undefined, applicationMock.applicant),
      showTopMatch: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const applicationStatusLabel = wrapper.find('[data-cy="application-status-label"]');
    expect(applicationStatusLabel).toMatchSnapshot();
  });
  it('should not render any application status label when application status is empty array', () => {
    const candidatesListItemProps = {
      ...candidatesListItemBaseProps,
      candidate: set('resume', undefined, applicationMock.applicant),
      showTopMatch: true,
    };
    const wrapper = mount(
      <Provider store={store}>
        <CandidatesListItem {...candidatesListItemProps} />
      </Provider>,
    );
    const applicationStatusLabel = wrapper.find('[data-cy="application-status-label"]');
    expect(applicationStatusLabel.exists()).toBe(false);
  });
  describe('bookmark indicator', () => {
    it('should render bookmark indicator when isBookmarked is true', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidatesListItem {...candidatesListItemBaseProps} isBookmarked={true} />
        </Provider>,
      );
      const bookmarkIndicator = wrapper.find('[data-cy="candidates-list-item-bookmark-indicator"]');
      expect(bookmarkIndicator.exists()).toBe(true);
    });
    it('should not render bookmark indicator when isBookmarked is false', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidatesListItem {...candidatesListItemBaseProps} isBookmarked={false} />
        </Provider>,
      );
      const bookmarkIndicator = wrapper.find('[data-cy="candidates-list-item-bookmark-indicator"]');
      expect(bookmarkIndicator.exists()).toBe(false);
    });
  });
  describe('invited talent tag', () => {
    it('should render invite talent tag when isInvitedTalent is true', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidatesListItem {...candidatesListItemBaseProps} isInvitedTalent={true} />
        </Provider>,
      );
      const invitedTalentTag = wrapper.find('[data-cy="invited-talent-label"]');
      expect(invitedTalentTag.exists()).toBe(true);
    });
    it('should not render invite talent tag when isInvitedTalent is false', () => {
      const wrapper = mount(
        <Provider store={store}>
          <CandidatesListItem {...candidatesListItemBaseProps} isInvitedTalent={false} />
        </Provider>,
      );
      const invitedTalentTag = wrapper.find('[data-cy="invited-talent-label"]');
      expect(invitedTalentTag.exists()).toBe(false);
    });
  });
});
