import React from 'react';

interface IMissingCandidateProps {
  candidateType: 'application' | 'suggested talent' | 'candidate';
}

export const MissingCandidate: React.FunctionComponent<IMissingCandidateProps> = ({candidateType}) => (
  <div className="light-red mv3">* Missing {candidateType} due to an unexpected error.</div>
);
