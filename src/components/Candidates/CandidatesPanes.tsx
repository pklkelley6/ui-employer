import React from 'react';
import styles from './CandidatesPanes.scss';
import {PanesLayout} from '~/components/Layouts/PanesLayout';

export interface ICandidatesPanesProps {
  left: (resetRightScroll: () => void) => JSX.Element;
  right: JSX.Element;
}

export class CandidatesPanes extends React.Component<ICandidatesPanesProps> {
  private leftPaneScroll: React.RefObject<HTMLDivElement>;
  private rightPaneScroll: React.RefObject<HTMLDivElement>;
  constructor(props: ICandidatesPanesProps) {
    super(props);
    this.leftPaneScroll = React.createRef();
    this.rightPaneScroll = React.createRef();
    this.resetRightScroll = this.resetRightScroll.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  public componentDidMount() {
    if (this.leftPaneScroll.current) {
      this.leftPaneScroll.current.addEventListener('scroll', this.onScroll, false);
    }
    if (this.rightPaneScroll.current) {
      this.rightPaneScroll.current.addEventListener('scroll', this.onScroll, false);
    }
  }

  public componentWillUnmount() {
    if (this.leftPaneScroll.current) {
      this.leftPaneScroll.current.removeEventListener('scroll', this.onScroll, false);
    }
    if (this.rightPaneScroll.current) {
      this.rightPaneScroll.current.removeEventListener('scroll', this.onScroll, false);
    }
  }

  public onScroll() {
    const TOP_THRESHOLD = 10;
    if (
      (this.leftPaneScroll.current && this.leftPaneScroll.current.scrollTop > TOP_THRESHOLD) ||
      (this.rightPaneScroll.current && this.rightPaneScroll.current.scrollTop > TOP_THRESHOLD)
    ) {
      /* Uncaught TypeError while run cypress test when use 'window.scrollTo()'
         workaround to overcome this error based on the following link:
         https://github.com/cypress-io/cypress/issues/2761
      */
      try {
        window.scrollTo({
          behavior: 'smooth',
          top: window.innerHeight,
        });
      } catch (err) {
        if (err instanceof TypeError) {
          window.scrollTo(0, window.innerHeight);
        } else {
          throw err;
        }
      }
    }
  }

  public resetRightScroll() {
    if (this.rightPaneScroll.current) {
      this.rightPaneScroll.current.scrollTop = 0;
    }
  }

  public render() {
    const panesProps = {
      left: (
        <div data-cy="scrollable-panel" ref={this.leftPaneScroll} className={`${styles.candidateList} pr1`}>
          {this.props.left(this.resetRightScroll)}
        </div>
      ),
      right: (
        <div className={`${styles.candidateDetails} pl1`}>
          <div
            data-cy="scrollable-details-panel"
            ref={this.rightPaneScroll}
            className={`${styles.candidateHiddenContainer} flex`}
          >
            {this.props.right}
          </div>
        </div>
      ),
    };
    return <PanesLayout {...panesProps} />;
  }
}
