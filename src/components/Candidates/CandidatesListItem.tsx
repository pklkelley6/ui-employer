import {format, parseISO} from 'date-fns';
import React from 'react';
import styles from './CandidatesList.scss';
import {ApplicationStatusLabel, IApplicationStatus} from '~/components/ApplicationStatusLabel/ApplicationStatusLabel';
import {CandidateLabel, CandidateType} from '~/components/CandidateLabel/CandidateLabel';
import {TopMatcherBadge} from '~/components/ScoreBadge/TopMatcherBadge';
import {getPosition} from '~/flux/applications';
import {ICandidate, isApplicationResume} from '~/graphql/candidates';
import {debounceAsync} from '~/util/debounceAsync';
import {downloadFile} from '~/util/files';
import {getDocumentUrl} from '~/util/getDocumentUrl';
import {InvitedTalentLabel} from '~/components/InvitedTalentLabel/InvitedTalentLabel';

export interface ICandidatesListItemProps {
  isViewed: boolean;
  selected: boolean;
  isBookmarked: boolean;
  isGetResumeVisible?: boolean;
  isInvitedTalent?: boolean;
  candidate: ICandidate;
  date: string;
  showTopMatch?: boolean;
  applicationStatus?: IApplicationStatus;
  candidateType: CandidateType;
  onClick: () => any;
  onResumeClick: () => any;
}

export const CandidatesListItem: React.FunctionComponent<ICandidatesListItemProps> = ({
  selected,
  candidate,
  candidateType,
  date,
  onClick,
  onResumeClick,
  isViewed,
  showTopMatch,
  applicationStatus,
  isBookmarked,
  isGetResumeVisible = true,
  isInvitedTalent = false,
}) => {
  const {resume} = candidate;
  const bgColorClassName = isViewed ? `${styles.cardViewed} bg-white-80` : 'bg-white';
  const selectedClassName = selected ? `${styles.cardActive}` : '';
  const position = getPosition(candidate);
  const formattedDate =
    isApplicationResume(resume) && resume.lastDownloadedAt
      ? `Downloaded ${format(parseISO(resume.lastDownloadedAt), 'd MMM')}`
      : '';
  return (
    <div className="flex items-center flex-auto" data-cy="candidates-list-item">
      <div
        className={`flex items-center flex-auto lh-copy pa2 pl0 mb1 relative ${styles.card} ${bgColorClassName} ${selectedClassName}`}
      >
        <div className={`${isViewed ? 'b--black-10' : 'b--blue o-60'} bw2 bl db self-stretch ml2 pr2`} />
        <div
          data-cy="candidate-info"
          className="flex justify-between items-center flex-auto pointer relative"
          onClick={onClick}
        >
          <div className="pl2 mr2 pv2 pr2 flex-auto">
            <div className="flex items-center justify-between">
              <h3
                data-cy="candidates-name"
                className={`${isViewed ? 'black-60 fw6' : 'primary fw6'} ma0 f4-5 lh-title truncate underline-hover`}
                title={candidate.name}
              >
                {candidate.name}
              </h3>
              {applicationStatus ? <ApplicationStatusLabel applicationStatus={applicationStatus} /> : null}
            </div>
            <div data-cy="candidates-job-title" className="db pt0 black-70 f6 truncate lh-title" title={position}>
              {position}
            </div>
            <div className="flex mt2">
              <CandidateLabel label={candidateType} />
              {isInvitedTalent ? <InvitedTalentLabel /> : undefined}
              <div data-cy="candidates-apply-date" className="db pt0 mr3 f6 black-40 truncate lh-title" title={date}>
                {date}
              </div>
            </div>
          </div>
          {showTopMatch ? <TopMatcherBadge /> : null}
        </div>
        {isGetResumeVisible ? (
          resume ? (
            <a
              className={`tc bl b--moon-gray dark-blue pl2 dim pointer ${styles.resumeBox}`}
              data-cy="candidates-list-item-download-button"
              onClick={debounceAsync(async (event) => {
                event.preventDefault();
                if (resume) {
                  await downloadFile(getDocumentUrl(resume.filePath), resume.fileName);
                  onResumeClick();
                }
              })}
            >
              <i data-cy="candidates-resume-icon" className={styles.iconDownload} />
              <span data-cy="candidates-get-resume-text" className="f6-5 fw4 db blue">
                Get Resume
              </span>
              <span data-cy="candidates-resume-downloaded-date" className="f7 fw4 lh-copy db black-50">
                {formattedDate}
              </span>
            </a>
          ) : (
            <div className={`tc bl b--moon-gray black-30 pl2 ${styles.resumeBox}`}>
              <span data-cy="candidates-get-resume-text" className="f6-5 fw4 db">
                No Resume Available
              </span>
            </div>
          )
        ) : null}

        {isBookmarked ? (
          <div className={styles.iconBookmark} data-cy="candidates-list-item-bookmark-indicator" />
        ) : null}
      </div>
    </div>
  );
};
