import React from 'react';
import styles from '~/components/Login/loginButton.scss';

interface ILoginButtonProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const LoginButton: React.FunctionComponent<ILoginButtonProps> = ({onClick}) => {
  return (
    <button
      id="mainLoginButton"
      onClick={onClick}
      className={`pa3 white fw6 tc f4 bg-primary w-100 br2 generic-btn ${styles.mainLoginButton}`}
      data-cy="main-login-button"
    >
      Log in now
    </button>
  );
};
