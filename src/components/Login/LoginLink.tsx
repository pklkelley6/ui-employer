import React from 'react';
import styles from '~/components/Login/loginButton.scss';

interface ILoginLinkProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

export const LoginLink: React.FunctionComponent<ILoginLinkProps> = ({onClick}) => {
  return (
    <a data-cy="login-with-corppass-link" href="#" onClick={onClick} className="mt2 f4 underline blue">
      Log in with CorpPass
    </a>
  );
};

export const NavLoginLink: React.FunctionComponent<ILoginLinkProps> = ({onClick}) => {
  return (
    <a
      data-cy="navbar-login-with-corppass"
      href="#"
      onClick={onClick}
      className={`fw6 no-underline pv1 v-mid dib ph3 primary bl b--light-silver ${styles.iconLogin}`}
    >
      Log in
    </a>
  );
};
