import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';
import {EditsRemainingCounter} from '../EditsRemainingCounter';
import {MAX_JOB_POST_EDITS} from '~/components/EditJobPosting/EditJobPost.constants';
import {cleanSnapshot} from '~/testUtil/enzyme';

describe('EditsRemainingCounter', () => {
  it('should display red background when there are zero edits left', () => {
    const editsMadeProp = MAX_JOB_POST_EDITS;
    const wrapper = mount(<EditsRemainingCounter editsMade={editsMadeProp} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
  it('should display gray background when there are more than zero edits left', () => {
    const editsMadeProp = 1;
    const wrapper = mount(<EditsRemainingCounter editsMade={editsMadeProp} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
  it('should display red background and zero edits left when there are more than 2 editsMade', () => {
    const editsMadeProp = MAX_JOB_POST_EDITS + 1;
    const wrapper = mount(<EditsRemainingCounter editsMade={editsMadeProp} />);
    expect(toJson(wrapper, cleanSnapshot())).toMatchSnapshot();
  });
});
