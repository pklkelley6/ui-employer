import React from 'react';
import {MAX_JOB_POST_EDITS} from '~/components/EditJobPosting/EditJobPost.constants';

export interface IEditsRemainingCounterProps {
  editsMade: number;
}

export const EditsRemainingCounter: React.FunctionComponent<IEditsRemainingCounterProps> = ({editsMade}) => {
  const editsLeft = Math.max(MAX_JOB_POST_EDITS - editsMade, 0);
  const editText = editsLeft === 1 ? 'Edit' : 'Edits';
  return (
    <div
      data-cy="edits-remaining-counter"
      className={`flex justify-between ${
        !editsLeft ? 'bg-red white' : 'bg-black-05 black-80'
      } f5-5 pa3 tl lh-copy relative pa3 mb3`}
      title={`${editsLeft} ${editText} left`}
    >
      <div className="flex flex-column tc w-30 lh-title">
        <span data-cy="edits-left" className="f2-5 fw3">
          {editsLeft}
        </span>
        <span data-cy="edit-text" className="f5-5 db">{`${editText} left`}</span>
      </div>
      <div className="f6 w-70 pl3">Please note that each job posting can only be edited twice</div>
    </div>
  );
};
