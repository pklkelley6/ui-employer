import {CandidateTabs} from '~/components/CandidateInfo';
import {
  googleAnalytics,
  EVENT_ACTION,
  EVENT_CATEGORY,
  CUSTOM_DIMENSIONS,
  DOWNLOAD_RESUME_GA_LABEL,
} from '~/util/analytics';

export const handleCandidateTabClick = (
  tabIndex: CandidateTabs,
  candidateType:
    | EVENT_CATEGORY.JOB_APPLICANT
    | EVENT_CATEGORY.JOB_SUGGESTED_TALENT
    | EVENT_CATEGORY.JOB_SAVED_TAB
    | EVENT_CATEGORY.JOB_TALENT_SEARCH,
  analytics = googleAnalytics,
) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.CANDIDATE_TAB_CLICKED,
    eventCategory: candidateType,
    eventLabel: CandidateTabs[tabIndex],
  });
};

export const handleCandidateResumeClick = (
  label: DOWNLOAD_RESUME_GA_LABEL,
  candidateType:
    | EVENT_CATEGORY.JOB_APPLICANT
    | EVENT_CATEGORY.JOB_SUGGESTED_TALENT
    | EVENT_CATEGORY.JOB_SAVED_TAB
    | EVENT_CATEGORY.JOB_TALENT_SEARCH,
  analytics = googleAnalytics,
) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.DOWNLOAD_RESUME_INDIVIDUAL,
    eventCategory: candidateType,
    eventLabel: DOWNLOAD_RESUME_GA_LABEL[label],
  });
};

export interface ICandidateCustomFields {
  applicationId?: string;
  candidateId: string;
  jobId: string;
  position: number;
}

export const handleCandidateClick = (
  fields: ICandidateCustomFields,
  candidateType: EVENT_CATEGORY.JOB_APPLICANT | EVENT_CATEGORY.JOB_SUGGESTED_TALENT | EVENT_CATEGORY.JOB_SAVED_TAB,
  analytics = googleAnalytics,
) => {
  analytics.set({
    [CUSTOM_DIMENSIONS.JOB_HIT]: fields.jobId,
    [CUSTOM_DIMENSIONS.CANDIDATE_HIT]: fields.candidateId,
  });
  analytics.sendEvent({
    eventAction: EVENT_ACTION.ITEM_POSITION,
    eventCategory: candidateType,
    eventLabel: fields.applicationId ?? fields.candidateId,
    eventValue: fields.position,
  });
};

export const handleCandidatesPageClick = (
  {jobId, page}: {jobId: string | null; page: number},
  candidateType:
    | EVENT_CATEGORY.JOB_APPLICANT
    | EVENT_CATEGORY.JOB_SUGGESTED_TALENT
    | EVENT_CATEGORY.JOB_SAVED_TAB
    | EVENT_CATEGORY.JOB_TALENT_SEARCH,
  analytics = googleAnalytics,
) => {
  analytics.set({
    [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
  });
  analytics.sendEvent({
    eventAction: EVENT_ACTION.PAGE_NUMBER,
    eventCategory: candidateType,
    eventLabel: String(page),
  });
};
