import {
  handleCandidateTabClick,
  handleCandidatesPageClick,
  handleCandidateResumeClick,
  handleCandidateClick,
  ICandidateCustomFields,
} from '../candidates';
import {CandidateTabs} from '~/components/CandidateInfo';
import {EVENT_ACTION, EVENT_CATEGORY, DOWNLOAD_RESUME_GA_LABEL} from '~/util/analytics';

describe('analytics/candidates', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('handleCandidateTabClick', () => {
    it('should send event for candidate tab click', () => {
      const candidateType = EVENT_CATEGORY.JOB_APPLICANT;
      handleCandidateTabClick(CandidateTabs.OVERVIEW, candidateType, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.CANDIDATE_TAB_CLICKED,
        eventCategory: candidateType,
        eventLabel: 'OVERVIEW',
      });
    });
  });

  describe('handleCandidatesPageClick', () => {
    it('should set custom dimensions and send event for suggested talents page click', () => {
      const fields = {
        jobId: 'job-id-123',
        page: 3,
      };
      handleCandidatesPageClick(fields, EVENT_CATEGORY.JOB_SAVED_TAB, analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {dimension1: fields.jobId});
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.PAGE_NUMBER,
        eventCategory: EVENT_CATEGORY.JOB_SAVED_TAB,
        eventLabel: String(fields.page),
      });
    });
  });
  describe('handleCandidateResumeClick', () => {
    it('should send event for suggested talent resume click', () => {
      handleCandidateResumeClick(DOWNLOAD_RESUME_GA_LABEL.LIST, EVENT_CATEGORY.JOB_SUGGESTED_TALENT, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.DOWNLOAD_RESUME_INDIVIDUAL,
        eventCategory: EVENT_CATEGORY.JOB_SUGGESTED_TALENT,
        eventLabel: 'LIST',
      });
    });
  });
  describe('handleCandidateClick', () => {
    it('should set custom dimensions and send event', () => {
      const fields: ICandidateCustomFields = {
        jobId: 'job-id-123',
        position: 10,
        candidateId: 'candidate-id-999',
        applicationId: 'app-id-123',
      };
      handleCandidateClick(fields, EVENT_CATEGORY.JOB_APPLICANT, analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {
        dimension1: fields.jobId,
        dimension2: fields.candidateId,
      });
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.ITEM_POSITION,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: fields.applicationId,
        eventValue: fields.position,
      });
    });
  });
});
