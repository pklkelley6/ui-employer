import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY, CUSTOM_DIMENSIONS} from '~/util/analytics';

export const handleScreeningQuestionsChanged = (
  {jobId, numberOfQuestions}: {jobId: string; numberOfQuestions: number},
  eventAction: EVENT_ACTION.ADD_SCREENING_QUESTIONS | EVENT_ACTION.REMOVE_SCREENING_QUESTIONS,
  analytics = googleAnalytics,
) => {
  analytics.set({
    [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
  });
  analytics.sendEvent({
    eventAction,
    eventCategory: EVENT_CATEGORY.JOB_POSTING,
    eventLabel: String(numberOfQuestions),
  });
};
