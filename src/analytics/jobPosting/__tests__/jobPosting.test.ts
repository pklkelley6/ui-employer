import {handleScreeningQuestionsChanged} from '../jobPosting';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('analytics/jobPosting', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('handleScreeningQuestionsChanged', () => {
    it('should set custom dimensions and send event when screening question changed', () => {
      const fields = {
        jobId: 'job-id-123',
        numberOfQuestions: 3,
      };
      handleScreeningQuestionsChanged(fields, EVENT_ACTION.ADD_SCREENING_QUESTIONS, analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {dimension1: fields.jobId});
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.ADD_SCREENING_QUESTIONS,
        eventCategory: EVENT_CATEGORY.JOB_POSTING,
        eventLabel: String(fields.numberOfQuestions),
      });
    });
  });
});
