import {mcf} from '@mcf/constants';
import {
  handleApplicationSortingType,
  handleApplicationsFilter,
  handleApplicationTopMatchFilter,
  handleApplicationTopMatchLabel,
  handleApplicationTopMatchTotal,
  handleApplicationMassUpdateSelectionClick,
  handleApplicationMassUpdateStatusClick,
  transformSortLabel,
  TOP_MATCH_FILTERED,
  TOP_MATCH,
  APPLICATION_STATUS,
  TOP_MATCH_APPLICANT,
  SCREENING_QUESTIONS,
} from '../applications';
import {ApplicationSortCriteria} from '~/flux/applications';
import {EVENT_ACTION, EVENT_CATEGORY, CUSTOM_DIMENSIONS} from '~/util/analytics';
import * as getEmailInvitations from '~/services/suggestedTalents/getEmailInvitations';

jest.spyOn(getEmailInvitations, 'getEmailInvitations').mockImplementation(() => Promise.resolve([]));

describe('analytics/applications', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });
  describe('handleApplicationSortingType', () => {
    it('should send event for application sort', () => {
      const sortLabel = transformSortLabel(ApplicationSortCriteria.SCORES_WCC);
      handleApplicationSortingType(ApplicationSortCriteria.SCORES_WCC, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_SORTING_TYPE,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: sortLabel,
      });
    });
  });
  describe('handleApplicationsFilter', () => {
    it('should only send TOP_MATCH label when isTopMatch is toggled true', () => {
      const isTopMatch = true;
      const applicationStatus: number[] = [];
      const screeningQuestions: string[] = [];
      const jobId = 'somejobid';
      handleApplicationsFilter(isTopMatch, applicationStatus, screeningQuestions, jobId, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: TOP_MATCH,
      });
    });
    it('should only send APPL_STATUS label for filtering applicants when there are application statuses selected', () => {
      const isTopMatch = false;
      const applicationStatus = [1, 2];
      const screeningQuestions: string[] = [];
      const jobId = 'somejobid';
      handleApplicationsFilter(isTopMatch, applicationStatus, screeningQuestions, jobId, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: APPLICATION_STATUS,
      });
    });
    it('should only send SCREENING_QNS label for filtering applicants when there are screening questions selected', () => {
      const isTopMatch = false;
      const applicationStatus: number[] = [];
      const screeningQuestions = [mcf.SCREEN_QUESTION_RESPONSE.YES, mcf.SCREEN_QUESTION_RESPONSE.NO];
      const jobId = 'somejobid';
      handleApplicationsFilter(isTopMatch, applicationStatus, screeningQuestions, jobId, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: SCREENING_QUESTIONS,
      });
    });
    it('should send APPL_STATUS, SCREENING_QNS and TOP_MATCH labels for filtering applicants when all filters are selected', () => {
      const isTopMatch = true;
      const applicationStatus = [1, 2];
      const screeningQuestions = [mcf.SCREEN_QUESTION_RESPONSE.YES, mcf.SCREEN_QUESTION_RESPONSE.NO];
      const jobId = 'somejobid';
      handleApplicationsFilter(isTopMatch, applicationStatus, screeningQuestions, jobId, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(3);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: APPLICATION_STATUS,
      });
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(2, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: TOP_MATCH,
      });
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(3, {
        eventAction: EVENT_ACTION.APPLICANTS_FILTER,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: SCREENING_QUESTIONS,
      });
    });
  });
  describe('handleApplicationTopMatchFilter', () => {
    it('should send event for filtering applicants when topmatch checkbox is checked', () => {
      const isChecked = true;
      handleApplicationTopMatchFilter(isChecked, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.TOP_MATCH_FILTER_CLICKED,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: TOP_MATCH_FILTERED,
      });
    });
  });
  describe('handleApplicationTopMatchLabel', () => {
    it('should send event for clicking applicants with top match label', () => {
      handleApplicationTopMatchLabel(analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.APPLICATION_TOP_MATCH_LABEL,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: TOP_MATCH_APPLICANT,
      });
    });
  });
  describe('handleApplicationTopMatchTotal', () => {
    it('should send event for total number of applicants with top matches', () => {
      const total = 12;
      const sortLabel = transformSortLabel(ApplicationSortCriteria.CREATED_ON);
      const jobUuid = 'abc1234xyz';
      handleApplicationTopMatchTotal(total, ApplicationSortCriteria.CREATED_ON, jobUuid, analytics);
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.TOP_MATCH_TOTAL,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: `${sortLabel}: ${total}`,
      });
    });
  });
  describe('handleApplicationMassUpdateSelectionClick', () => {
    it('should send event for total number of applicants selected for mass update', () => {
      const numberOfApplicationSelected = 12;
      const jobId = 'abc1234xyz';
      handleApplicationMassUpdateSelectionClick({jobId, numberOfApplicationSelected}, analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {
        [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
      });
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.MASS_UPDATE_SELECTION,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: `${numberOfApplicationSelected}`,
      });
    });
  });
  describe('handleApplicationMassUpdateStatusClick', () => {
    it('should send event for application status selected for mass update', () => {
      const applicationStatus = mcf.JOB_APPLICATION_STATUS.SUCCESSFUL;
      const jobId = 'abc1234xyz';
      handleApplicationMassUpdateStatusClick({jobId, applicationStatus}, analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {
        [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
      });
      expect(analytics.sendEvent).toHaveBeenNthCalledWith(1, {
        eventAction: EVENT_ACTION.MASS_UPDATE_STATUS,
        eventCategory: EVENT_CATEGORY.JOB_APPLICANT,
        eventLabel: mcf.JOB_APPLICATION_STATUSES[mcf.JOB_APPLICATION_STATUS.SUCCESSFUL],
      });
    });
  });
});
