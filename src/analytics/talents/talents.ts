import {CUSTOM_DIMENSIONS, googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

export const handleInviteToApplyClick = (
  {candidateId, jobId}: {candidateId: string; jobId: string},
  category: EVENT_CATEGORY,
  analytics = googleAnalytics,
) => {
  analytics.set({
    [CUSTOM_DIMENSIONS.JOB_HIT]: jobId,
  });
  analytics.sendEvent({
    eventAction: EVENT_ACTION.INVITE_TO_APPLY,
    eventCategory: category,
    eventLabel: candidateId,
  });
};

export const handleTalentSearchQuery = (query: string, analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventCategory: EVENT_CATEGORY.JOB_TALENT_SEARCH,
    eventAction: EVENT_ACTION.SEARCH_TERM,
    eventLabel: query,
  });
};
