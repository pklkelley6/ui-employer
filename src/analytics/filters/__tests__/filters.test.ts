import {handleAllJobsSortingType, handlePageChange} from '../filters';
import {ALL_JOBS_CLOSED, ALL_JOBS_OPEN} from '~/flux/analytics';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('analytics/filters', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('handleAllJobsSortingType()', () => {
    it('should send event with the type of sorting selected', () => {
      const sortType = 'foo-sort';
      handleAllJobsSortingType(sortType, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.JOBS_SORTING_TYPE,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_PAGE_SORTING,
        eventLabel: sortType,
      });
    });
  });
  describe('handlePageChange()', () => {
    it('should send event with the open job status tab and the page number clicked', () => {
      const jobStatus = ALL_JOBS_OPEN;
      const page = 3;
      handlePageChange(jobStatus, page, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.PAGE_NUMBER,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_OPEN,
        eventLabel: '3',
      });
    });

    it('should send event with the closed job status tab and the page number clicked', () => {
      const jobStatus = ALL_JOBS_CLOSED;
      const page = 5;
      handlePageChange(jobStatus, page, analytics);
      expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
      expect(analytics.sendEvent).toHaveBeenCalledWith({
        eventAction: EVENT_ACTION.PAGE_NUMBER,
        eventCategory: EVENT_CATEGORY.ALL_JOBS_CLOSED,
        eventLabel: '5',
      });
    });
  });
});
