import {ALL_JOBS_OPEN} from '~/flux/analytics';
import {googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

export const handleAllJobsSortingType = (sortType: string, analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.JOBS_SORTING_TYPE,
    eventCategory: EVENT_CATEGORY.ALL_JOBS_PAGE_SORTING,
    eventLabel: sortType,
  });
};

export const handlePageChange = (jobStatus: string, page: number, analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.PAGE_NUMBER,
    eventCategory: jobStatus === ALL_JOBS_OPEN ? EVENT_CATEGORY.ALL_JOBS_OPEN : EVENT_CATEGORY.ALL_JOBS_CLOSED,
    eventLabel: String(page),
  });
};
