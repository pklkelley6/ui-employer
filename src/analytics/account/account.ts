import {CUSTOM_DIMENSIONS, googleAnalytics, EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

const LABEL_LOGIN = 'login-employer';

export const handleEmployerLogin = (analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.LOGIN,
    eventCategory: EVENT_CATEGORY.ACCOUNT,
    eventLabel: LABEL_LOGIN,
  });
};

export const handleEmployerLogout = (pathname: string, analytics = googleAnalytics) => {
  analytics.sendEvent({
    eventAction: EVENT_ACTION.LOGOUT,
    eventCategory: EVENT_CATEGORY.ACCOUNT,
    eventLabel: pathname,
  });
};

export const tagUserSession = (entityId: string, analytics = googleAnalytics) => {
  analytics.set({
    [CUSTOM_DIMENSIONS.ENTITY_ID_SESSION]: entityId,
  });
};
