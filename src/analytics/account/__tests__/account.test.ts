import {handleEmployerLogin, tagUserSession} from '../account';
import {EVENT_ACTION, EVENT_CATEGORY} from '~/util/analytics';

describe('Account Analytics', () => {
  const analytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should trigger an event on employer login', () => {
    handleEmployerLogin(analytics);
    expect(analytics.sendEvent).toHaveBeenCalledTimes(1);
    expect(analytics.sendEvent).toHaveBeenCalledWith({
      eventAction: EVENT_ACTION.LOGIN,
      eventCategory: EVENT_CATEGORY.ACCOUNT,
      eventLabel: 'login-employer',
    });
  });

  describe('tagUserSession', () => {
    it('should tag custom dimensions 3 with user uen', () => {
      tagUserSession('some uen', analytics);
      expect(analytics.set).toHaveBeenNthCalledWith(1, {dimension3: 'some uen'});
    });
  });
});
