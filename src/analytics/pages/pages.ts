import {googleAnalytics, IPageViewFields, wogaa} from '~/util/analytics';

export interface IPageProps {
  rePage: RegExp;
  title: string;
}

const PAGES: IPageProps[] = [
  {
    rePage: /^\/$/,
    title: 'Home Page',
  },
  {
    rePage: /^\/jobs$/,
    title: 'All Jobs Page',
  },
  {
    rePage: /^\/jobs\/[0-9a-zA-z\-]+\/applications$/,
    title: 'Job Applications Page',
  },
  {
    rePage: /^\/jobs\/[0-9a-zA-z\-]+\/suggested-talents$/,
    title: 'Job Suggested Talents Page',
  },
  {
    rePage: /^\/logout$/,
    title: 'Log Out Page',
  },
  {
    rePage: /^\/session-timeout$/,
    title: 'Session Timeout Page',
  },
];

export const startJobPosting = (analytics = wogaa()) => {
  analytics.startTransactionalService();
};

export const completeJobPosting = (analytics = wogaa()) => {
  analytics.completeTransactionalService();
};

export const handlePageView = (page: string, analytics = googleAnalytics) => {
  const pageField = PAGES.find((p) => p.rePage.test(page));
  const fieldsObject: IPageViewFields = {
    page,
    title: pageField?.title ?? 'Untracked',
  };
  analytics.set(fieldsObject);
  analytics.sendPageView(fieldsObject);
};
