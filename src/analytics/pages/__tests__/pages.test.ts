import {handlePageView} from '../pages';
import {IPageViewFields} from '~/util/analytics';

describe('analytics/pages', () => {
  const mockAnalytics = {
    sendEvent: jest.fn(),
    sendPageView: jest.fn(),
    set: jest.fn(),
  };

  afterEach(() => {
    jest.resetAllMocks();
  });
  it('should call googleAnalytics set, sendPageView with right params when /', () => {
    const gaFieldObject = {page: '/', title: 'Home Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with right params when /jobs', () => {
    const gaFieldObject = {page: '/jobs', title: 'All Jobs Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with right params when /jobs/{uuid}/applications', () => {
    const gaFieldObject = {page: '/jobs/foo-bar/applications', title: 'Job Applications Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with right params when /jobs/{uuid}/suggested-talents', () => {
    const gaFieldObject = {page: '/jobs/foo-bar/suggested-talents', title: 'Job Suggested Talents Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with right params when /logout', () => {
    const gaFieldObject = {page: '/logout', title: 'Log Out Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with right params when /session-timeout', () => {
    const gaFieldObject = {page: '/session-timeout', title: 'Session Timeout Page'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  it('should call googleAnalytics set, sendPageView with title Untracked when a page not required to be track is visited', () => {
    const gaFieldObject = {page: '/not-required-to-track', title: 'Untracked'};
    handlePageView(gaFieldObject.page, mockAnalytics);
    expectGAToBeCalledWith(gaFieldObject);
  });

  const expectGAToBeCalledWith = (fieldsObject: IPageViewFields) => {
    expect(mockAnalytics.set).toHaveBeenNthCalledWith(1, fieldsObject);
    expect(mockAnalytics.sendPageView).toHaveBeenNthCalledWith(1, fieldsObject);
    expect(mockAnalytics.sendEvent).not.toBeCalled();
  };
});
