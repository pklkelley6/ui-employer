import {RouteProps} from 'react-router-dom';
import {CandidatesTabs} from './pages/Candidates/Candidates.constants';
import {CreateJobPostingContainer} from './pages/JobPosting/CreateJobPostingContainer';
import {JobPostSuccessContainer} from './pages/JobPosting/JobPostSuccessContainer';
import {TermsOfUse} from './pages/static/TermsOfUse';
import {CorpPassHelp} from './pages/static/CorpPassHelp';
import {AccountInfoContainer} from './pages/AccountInfo/AccountInfoContainer';
import {AppContainer} from '~/components/App/AppContainer';
import {CandidatesContainer} from '~/pages/Candidates/CandidatesContainer';
import {FirstTimeLoginContainer} from '~/pages/FirstTimeLogin/FirstTimeLoginContainer';
import {JobsPageContainer} from '~/pages/JobsPage/JobsPageContainer';
import {LogOutContainer, TimeOutContainer} from '~/pages/LogOut';
import {SurveyPage} from '~/pages/SurveyPage/SurveyPage';
import {UnauthorisedContainer} from '~/pages/Unauthorised';
import {ViewEditRepostJobContainer} from '~/pages/ViewEditRepostJobPost/ViewEditRepostJobContainer';
import {CompanyProfileContainer} from '~/pages/CompanyProfile/CompanyProfileContainer';
import {TalentSearchPageContainer} from '~/pages/TalentSearch/TalentSearchPageContainer';

export const publicRoutes: RouteProps[] = [
  {path: '/', exact: true, component: AppContainer},
  {path: '/logout', exact: true, component: LogOutContainer},
  {path: '/session-timeout', exact: true, component: TimeOutContainer},
  {path: '/unauthorised', exact: true, component: UnauthorisedContainer},
  {path: '/survey', exact: true, component: SurveyPage},
  {path: '/terms-of-use', exact: true, component: TermsOfUse},
  {path: '/corppass-help', exact: true, component: CorpPassHelp},
];

export const privateRoutes: RouteProps[] = [
  {path: '/jobs/:uuid/success', exact: true, component: JobPostSuccessContainer},
  {path: '/jobs', exact: true, component: JobsPageContainer},
  {path: '/jobs/:titleId/:action(view|edit)', exact: true, component: ViewEditRepostJobContainer},
  {
    component: CandidatesContainer,
    exact: true,
    path: `/jobs/:titleId/:tab(${CandidatesTabs.Applications}|${CandidatesTabs.SuggestedTalents}|${CandidatesTabs.Saved})`,
  },
  {path: '/jobs/new', component: CreateJobPostingContainer},
  {path: '/company-profile', exact: true, component: CompanyProfileContainer},
  {path: '/talent-search', exact: true, component: TalentSearchPageContainer},
  {path: '/terms-and-conditions', component: FirstTimeLoginContainer},
  {path: '/account-info', component: AccountInfoContainer},
];
