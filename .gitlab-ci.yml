#    _____   _____ _____   _____ _____      __
#   / _ \ \ / / __| _ \ \ / /_ _| __\ \    / /
#  | (_) \ V /| _||   /\ V / | || _| \ \/\/ /
#   \___/ \_/ |___|_|_\ \_/ |___|___| \_/\_/
#
## ASCII art generated from: http://patorjk.com/software/taag/#p=display&f=Small&t=

include:
  - local: '.gitlab/pipeline/deploy.yml'
  - remote: https://gitlab.com/mycf.sg/central-cicd/-/raw/master/common/latest-bundle.yml

variables:
  GIT_DEPTH: 100
  FLAG_FORCE_BUILD: '[force build]'
  MAJOR_BUMP_LABEL: '[major release]'
  MINOR_BUMP_LABEL: '[minor release]'
  SPEC_ALL_JOBS_PAGE: 'cypress/integration/FunctionalTestingSuite/AllJobsPage/**'
  SPEC_APPLICANTS_DETAILS: 'cypress/integration/FunctionalTestingSuite/ApplicantsDetails/**'
  SPEC_APPLICANTS_FEATURES: 'cypress/integration/FunctionalTestingSuite/ApplicantsFeatures/**'
  SPEC_APPLICANTS_GREY_BAR_MASS_UPDATE: 'cypress/integration/FunctionalTestingSuite/ApplicantsGreyBarMassUpdates/**'
  SPEC_APPLICANTS_MASS_SELECTION_UPDATE: 'cypress/integration/FunctionalTestingSuite/ApplicantsMassSelectionUpdates/**'
  SPEC_BOOKMARK_SAVED: 'cypress/integration/FunctionalTestingSuite/BookmarkSaved/**'
  SPEC_COMPANY_PROFILE_ACCOUNT_INFO_PAGE: 'cypress/integration/FunctionalTestingSuite/CompanyProfilePage/**,cypress/integration/FunctionalTestingSuite/AccountInfoPage/**'
  SPEC_JOB_POST_CREATION: 'cypress/integration/FunctionalTestingSuite/JobPostCreation/**'
  SPEC_JOB_POST_EDIT_AND_SCREENING_QUESTIONS: 'cypress/integration/FunctionalTestingSuite/JobPostEdit/**,cypress/integration/FunctionalTestingSuite/ScreeningQuestions/**'
  SPEC_JOB_POST_REPOST_EXTEND_CLOSE: 'cypress/integration/FunctionalTestingSuite/JobPostRepostExtendAndClose/**'
  SPEC_JOB_POST_SUCCESSFUL_SUBMISSION: 'cypress/integration/FunctionalTestingSuite/JobPostSuccessfulSubmission/**'
  SPEC_JOB_POST_VALIDATION_AND_SCHEMES: 'cypress/integration/FunctionalTestingSuite/JobPostValidation/**,cypress/integration/FunctionalTestingSuite/JobPostSchemes/**'
  SPEC_JOB_POST_VIEW: 'cypress/integration/FunctionalTestingSuite/JobPostView/**'
  SPEC_JOB_POST_LANDING_PAGE_FIRST_LOGIN: 'cypress/integration/FunctionalTestingSuite/LandingPageFeaturesAndFirstTimeLogin/**'
  SPEC_SUGGESTED_TALENTS: 'cypress/integration/FunctionalTestingSuite/SuggestedTalents/**'
  SPEC_TALENT_SEARCH: 'cypress/integration/FunctionalTestingSuite/TalentSearch/**'
  NODE_IMAGE: public.ecr.aws/l5k6t5t7/cicd-images:node14-latest
  CYPRESS_DOCKER_IMAGE: public.ecr.aws/l5k6t5t7/cicd-images:cypress-8.4.1_chrome-91.0.4472.114_firefox-89.0.2
  CIVAR_DEPLOYER_ID: $GITLAB_DEPLOY_USER
  CIVAR_DEPLOYER_TOKEN: $GITLAB_DEPLOY_TOKEN

stages:
  - setup
  - post_setup
  - test
  - post_test
  - cicd version bump
  - cicd create version badge
  - update_pact
  - pre_build
  - build
  - deploy_qa
  - pre_integration_test
  - integration_test
  - post_integration_test
  - deploy_uat
  - slack_notification

#   ___ ___ _____ _   _ ___
#  / __| __|_   _| | | | _ \
#  \__ \ _|  | | | |_| |  _/
#  |___/___| |_|  \___/|_|

dependencies:
  stage: setup
  image: ${NODE_IMAGE}
  except: ['tags']
  tags: ['private']
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
  script:
    - yarn install --check-files

graphql:
  stage: post_setup
  image: ${NODE_IMAGE}
  except: ['tags']
  tags: ['private']
  artifacts:
    paths: ['src/graphql/__generated__']
  script:
    - yarn graphql:get-schema
    - yarn graphql:generate-types
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull

#   _____ ___ ___ _____
#  |_   _| __/ __|_   _|
#    | | | _|__ \\ | |
#    |_| |___|___/ |_|
#

unit_tests:
  stage: test
  image: ${NODE_IMAGE}
  services:
    - name: public.ecr.aws/govtechsg/cicd-images:pact-mock-service-3.7.0
      alias: pact-mock-service-api-profile
      command:
        - sh
        - -c
        - bundle exec pact-mock-service service
          --consumer=ui-employer
          --provider=api-profile
          --host=0.0.0.0
          --port=4444
          --cors=true
          --pact-dir=${CI_PROJECT_DIR}/pacts
          --log=${CI_PROJECT_DIR}/pacts/logs/pact-api-profile.log
          --pact-specification-version=2
    - name: public.ecr.aws/govtechsg/cicd-images:pact-mock-service-3.7.0
      alias: pact-mock-service-api-job
      command:
        - sh
        - -c
        - bundle exec pact-mock-service service
          --consumer=ui-employer
          --provider=api-job
          --host=0.0.0.0
          --port=4445
          --cors=true
          --pact-dir=${CI_PROJECT_DIR}/pacts
          --log=${CI_PROJECT_DIR}/pacts/logs/pact-api-job.log
          --pact-specification-version=2
  artifacts:
    paths: ['coverage', 'pacts']
    expire_in: 1 day
    when: always
  except: ['tags']
  variables:
    TZ: Asia/Singapore
  script:
    - PACT_CI=true yarn test --coverage --ci ${JEST_OPTIONS}
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull
  dependencies:
    - graphql

lint:
  stage: test
  image: ${NODE_IMAGE}
  except: ['tags']
  tags: ['private']
  script:
    - yarn lint
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull
  dependencies:
    - graphql

security:
  stage: test
  image: ${NODE_IMAGE}
  except: ['tags']
  script:
    - yarn audit --groups=dependencies
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull

coverage:
  stage: post_test
  image: sonarsource/sonar-scanner-cli:latest
  except: ['tags']
  script:
    - sonar-scanner
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull
  dependencies:
    - unit_tests

pact_verify_api_profile:
  extends: .pact_verify
  variables:
    PACT_URL: ./pacts/ui-employer-api-profile.json
    PROVIDER_STATE_SETUP: -c=https://api-profile-qa.ci.mcf.sh/provider-state

pact_verify_api_job:
  extends: .pact_verify
  variables:
    PACT_URL: ./pacts/ui-employer-api-job.json
    PROVIDER_STATE_SETUP: -c=https://api-job-qa.ci.mcf.sh/provider-state

.pact_verify:
  stage: post_test
  image: public.ecr.aws/govtechsg/cicd-images:pact-provider-verifier-latest
  except: ['tags']
  tags: ['private']
  variables:
    PROVIDER_BASE_URL: https://api-job-qa.ci.mcf.sh
  before_script:
    - export AUTHORIZATION_TOKEN=$(wget -q -S -O- ${SVC_AUTH_URL}cp/token\?authCode=${AUTHORIZATION_TOKEN_PAYLOAD_BASE64} 2>&1 | sed -n "s/^\s*Authorization:\s*\(\S*\)/\1/p")
    - export CUSTOM_PROVIDER_HEADER=--custom-provider-header=Authorization:${AUTHORIZATION_TOKEN}
  script:
    - pact-provider-verifier ${PACT_URL} -h=${PROVIDER_BASE_URL} ${CUSTOM_PROVIDER_HEADER} ${PROVIDER_STATE_SETUP}
  dependencies:
    - unit_tests

##   _   _ ___ ___   _ _____ ___   ___  _   ___ _____
##  | | | | _ \   \ /_\_   _| __| | _ \/_\ / __|_   _|
##  | |_| |  _/ |) / _ \| | | _|  |  _/ _ \ (__  | |
##   \___/|_| |___/_/ \_\_| |___| |_|/_/ \_\___| |_|
##

update_pact_api_profile:
  extends: .update_pact
  variables:
    PACT_FILE: ui-employer-api-profile.json

update_pact_api_job:
  extends: .update_pact
  variables:
    PACT_FILE: ui-employer-api-job.json

.update_pact:
  stage: update_pact
  only:
    - master
  tags: ['private']
  image:
    name: public.ecr.aws/l5k6t5t7/cicd-images:awscli-latest
    entrypoint: ['/bin/bash', '-l', '-c']
  before_script:
    - VERSION=$(cat .version)
  script:
    - aws s3 cp ./pacts/${PACT_FILE} s3://mcf-ci-pipeline/pacts/ui-employer/${PACT_FILE} --cache-control no-cache --content-type application/json
    - aws s3 cp ./pacts/${PACT_FILE} s3://mcf-ci-pipeline/pacts/ui-employer/${VERSION}/${PACT_FILE} --cache-control no-cache --content-type application/json
  dependencies:
    - unit_tests
    - version bump

##   ___      _ _    _
##  | _ )_  _(_) |__| |
##  | _ \ || | | / _` |
##  |___/\_,_|_|_\__,_|
##
.build_application:
  tags: ['private']
  stage: pre_build
  image: ${NODE_IMAGE}
  script:
    - yarn build:prod
  artifacts:
    paths: ['dist']
  cache:
    key:
      files:
        - package.json
        - yarn.lock
    paths:
      - node_modules
    policy: pull

build_application_non_master:
  extends: .build_application
  except:
    - tags
    - master
  dependencies:
    - graphql
  before_script:
    - touch .version

build_application_master:
  extends: .build_application
  only:
    - master
  dependencies:
    - graphql
    - version bump

build_image:
  only:
    - master
    - /^ops/master.*$/
    - /^integration/.*$/
  services:
    - docker:18.09.7-dind
  image: public.ecr.aws/mycfsg/gitlab-ci-runner-k8s-dind:latest
  stage: build
  tags: ['private']
  variables:
    LOCALVAR_DOCKER_IMAGE_NAME: "mcf/ui-employer"
    LOCALVAR_AWS_REGION: "ap-southeast-1"
    LOCALVAR_ECR_URL: $CIVAR_ECR_URL
  before_script:
    - DOCKER_IMAGE_NAME_NEXT=${LOCALVAR_ECR_URL}/${LOCALVAR_DOCKER_IMAGE_NAME}:$(cat .version)
    - aws ecr get-login-password --region ${LOCALVAR_AWS_REGION} | docker login --username AWS --password-stdin ${LOCALVAR_ECR_URL}
  script:
    - docker build -f ./provisioning/images/production.Dockerfile -t ${DOCKER_IMAGE_NAME_NEXT} .;
    - docker push ${DOCKER_IMAGE_NAME_NEXT};
  after_script:
    - docker logout ${LOCALVAR_ECR_URL}
  dependencies:
    - build_application_master
    - version bump

wait for qa to be deployed:
  stage: pre_integration_test
  image: public.ecr.aws/l5k6t5t7/cicd-images:dind-latest
  dependencies:
    - version bump
  only:
    - master
    - /^ops/master.*$/
    - /^integration/.*$/
  tags:
    - private
  script:
    - |
      set +e
      VERSION=$(cat .version);
      MAX_COUNT=60;
      while :; do
        _=$(curl -vv "${CYPRESS_baseUrl}/version" | grep -e "${VERSION}");
        if [ "$?" = "0" ]; then
          printf "${VERSION} has been deployed\n";
          break;
        elif [ "${MAX_COUNT}" = "0" ]; then
          printf "Dying with status code 1.\n";
          exit 1;
        else
          MAX_COUNT=$((MAX_COUNT - 1));
          printf "${MAX_COUNT} tries left...\n";
          sleep 5;
        fi;
      done;

##  ___ _  _ _____ ___ ___ ___    _ _____ ___ ___  _  _   _____ ___ ___ _____
## |_ _| \| |_   _| __/ __| _ \  /_\_   _|_ _/ _ \| \| | |_   _| __/ __|_   _|
##  | || .` | | | | _| (_ |   / / _ \| |  | | (_) | .` |   | | | _|\__ \ | |
## |___|_|\_| |_| |___\___|_|_\/_/ \_\_| |___\___/|_|\_|   |_| |___|___/ |_|
##

.integration_test:
  stage: integration_test
  image: ${CYPRESS_DOCKER_IMAGE}
  only:
    - master
    - /^integration/.*$/
  tags: ['private']
  artifacts:
    when: always
    paths:
      - cypress/reports/*
      - "${QA_API_JOB_VERSION_FILE}"
      - "${QA_API_PROFILE_VERSION_FILE}"
    expire_in: 24 hours
  before_script:
    - npm install -g $(cat yarn.lock | grep -o -E "@mcf/constants@[0-9]+\.[0-9]+\.[0-9]+")
    - npm install -g cypress-mochawesome-reporter
    - npm link $(ls -1 $(npm root -g)/ | sed -e "s/@mcf/@mcf\/constants/g")
  script:
    - cypress run --spec "${SPEC_FILE}" --browser chrome || CY_RESULT=$?;
    - 'curl -s -X GET https://api-job-qa.ci.mcf.sh/version -H "Accept: application/json" | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+" > ${QA_API_JOB_VERSION_FILE}'
    - 'curl -s -X GET https://api-job-qa.ci.mcf.sh/profile/version -H "Accept: application/json" | grep -o -E "[0-9]+\.[0-9]+\.[0-9]+" > ${QA_API_PROFILE_VERSION_FILE}'
    - exit ${CY_RESULT};

all_jobs_page:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_ALL_JOBS_PAGE'
    QA_API_JOB_VERSION_FILE: 'cypress/all_jobs_page_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/all_jobs_page_api_profile_version.json'

applicants_details:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_DETAILS'
    QA_API_JOB_VERSION_FILE: 'cypress/applicants_details_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/applicants_details_api_profile_version.json'

applicants_features:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_FEATURES'
    QA_API_JOB_VERSION_FILE: 'cypress/applicants_features_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/applicants_feature_api_profile_version.json'

applicants_grey_bar_mass_update:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_GREY_BAR_MASS_UPDATE'
    QA_API_JOB_VERSION_FILE: 'cypress/mass_update_grey_bar_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/mass_update_grey_bar_api_profile_version.json'

applicants_mass_selection_update:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_MASS_SELECTION_UPDATE'
    QA_API_JOB_VERSION_FILE: 'cypress/mass_selection_update_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/mass_selection_update_api_profile_version.json'

bookmark_saved:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_BOOKMARK_SAVED'
    QA_API_JOB_VERSION_FILE: 'cypress/bookmark_saved_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/bookmark_saved_api_profile_version.json'

company_profile_account_info_page:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_COMPANY_PROFILE_ACCOUNT_INFO_PAGE'
    QA_API_JOB_VERSION_FILE: 'cypress/company_profile_account_info_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/company_profile_account_info_api_profile_version.json'

job_post_creation:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_CREATION'
    QA_API_JOB_VERSION_FILE: 'cypress/job_post_creation_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/job_post_creation_api_profile_version.json'

job_post_edit_and_screening_questions:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_EDIT_AND_SCREENING_QUESTIONS'
    QA_API_JOB_VERSION_FILE: 'cypress/job_post_edit_screening_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/job_post_edit_screening_api_profile_version.json'

job_post_repost_extend_close:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_REPOST_EXTEND_CLOSE'
    QA_API_JOB_VERSION_FILE: 'cypress/job_post_repost_extend_close_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/job_post_repost_extend_close_api_profile_version.json'

job_post_successful_submission:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_SUCCESSFUL_SUBMISSION'
    QA_API_JOB_VERSION_FILE: 'cypress/successful_submission_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/successful_submission_api_profile_version.json'

job_post_validation_and_schemes:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_VALIDATION_AND_SCHEMES'
    QA_API_JOB_VERSION_FILE: 'cypress/job_post_validation_and_schemes_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/job_post_validation_and_schemes_api_profile_version.json'

job_post_view:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_VIEW'
    QA_API_JOB_VERSION_FILE: 'cypress/job_post_view_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/job_post_view_api_profile_version.json'

landing_page_first_login:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_LANDING_PAGE_FIRST_LOGIN'
    QA_API_JOB_VERSION_FILE: 'cypress/landing_page_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/landing_page_api_profile_version.json'

suggested_talents:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_SUGGESTED_TALENTS'
    QA_API_JOB_VERSION_FILE: 'cypress/suggested_talents_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/suggested_talents_api_profile_version.json'

talent_search:
  extends: .integration_test
  variables:
    SPEC_FILE: '$SPEC_TALENT_SEARCH'
    QA_API_JOB_VERSION_FILE: 'cypress/talent_search_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/talent_search_api_profile_version.json'

#
# Used for QE to run integration tests on their branches
#
.ops_integration_test:
  extends: .integration_test
  allow_failure: true
  only:
    - /^ops/integration.*$/

ops_all_jobs_page:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_ALL_JOBS_PAGE'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_all_jobs_page_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_all_jobs_page_api_profile_version.json'

ops_applicants_details:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_DETAILS'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_applicants_details_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_applicants_details_api_profile_version.json'

ops_applicants_features:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_FEATURES'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_applicants_features_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_applicants_feature_api_profile_version.json'

ops_applicants_grey_bar_mass_update:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_GREY_BAR_MASS_UPDATE'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_mass_update_grey_bar_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_mass_update_grey_bar_api_profile_version.json'

ops_applicants_mass_selection_update:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_APPLICANTS_MASS_SELECTION_UPDATE'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_mass_selection_update_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_mass_selection_update_api_profile_version.json'

ops_bookmark_saved:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_BOOKMARK_SAVED'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_bookmark_saved_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_bookmark_saved_api_profile_version.json'

ops_company_profile_account_info_page:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_COMPANY_PROFILE_ACCOUNT_INFO_PAGE'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_company_profile_account_info_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_company_profile_account_info_api_profile_version.json'

ops_job_post_creation:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_CREATION'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_job_post_creation_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_job_post_creation_api_profile_version.json'

ops_job_post_edit_and_screening_questions:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_EDIT_AND_SCREENING_QUESTIONS'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_job_post_edit_screening_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_job_post_edit_screening_api_profile_version.json'

ops_job_post_repost_extend_close:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_REPOST_EXTEND_CLOSE'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_job_post_repost_extend_close_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_job_post_repost_extend_close_api_profile_version.json'

ops_job_post_successful_submission:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_SUCCESSFUL_SUBMISSION'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_successful_submission_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_successful_submission_api_profile_version.json'

ops_job_post_validation_and_schemes:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_VALIDATION_AND_SCHEMES'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_job_post_validation_and_schemes_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_job_post_validation_and_schemes_api_profile_version.json'

ops_job_post_view:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_VIEW'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_job_post_view_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_job_post_view_api_profile_version.json'

ops_landing_page_first_login:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_JOB_POST_LANDING_PAGE_FIRST_LOGIN'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_landing_page_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_landing_page_api_profile_version.json'

ops_suggested_talents:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_SUGGESTED_TALENTS'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_suggested_talents_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_suggested_talents_api_profile_version.json'

ops_talent_search:
  extends: .ops_integration_test
  variables:
    SPEC_FILE: '$SPEC_TALENT_SEARCH'
    QA_API_JOB_VERSION_FILE: 'cypress/ops_talent_search_api_job_version.json'
    QA_API_PROFILE_VERSION_FILE: 'cypress/ops_talent_search_api_profile_version.json'

audit:
  stage: integration_test
  only:
    - master
  tags:
    - private
  image: femtopixel/google-lighthouse-puppeteer:v8.6.0-v11.0.0-1.2.0
  artifacts:
    when: always
    paths:
      - "*.report.json"
      - "*.report.html"
      - "summary.json"
  script:
    - lighthouse-puppeteer ${CI_PROJECT_DIR}/test/lighthouse/auth-login.js -d ${CI_PROJECT_DIR} --verbose -v --html -l "- --preset=desktop --only-categories=accessibility,best-practices,performance,seo" -c "- --no-sandbox --disable-setuid-sandbox"

upload audit:
  stage: post_integration_test
  image:
    name: public.ecr.aws/l5k6t5t7/cicd-images:awscli-latest
    entrypoint: ['/bin/bash', '-l', '-c']
  only:
    - master
  before_script:
    - VERSION=$(cat .version)
    - VERSION=test
  script:
    - aws s3 cp --exclude="*" --include="*.report.json" ./ s3://mcf-ci-pipeline/audits/lighthouse/ui-employer/test/${VERSION}/ --cache-control no-cache --content-type application/json
    - aws s3 cp --exclude="*" --include="*.report.html" ./ s3://mcf-ci-pipeline/audits/lighthouse/ui-employer/test/${VERSION}/ --cache-control no-cache --content-type text/html
    - aws s3 cp ./summary.json s3://mcf-ci-pipeline/audits/lighthouse/ui-employer/test/${VERSION}/ --cache-control no-cache --content-type application/json
  dependencies:
    - audit
    - version bump

slack_notification:
  stage: slack_notification
  only:
    - master
  dependencies:
    - all_jobs_page
    - applicants_details
    - applicants_features
    - applicants_grey_bar_mass_update
    - applicants_mass_selection_update
    - bookmark_saved
    - company_profile_account_info_page
    - job_post_creation
    - job_post_edit_and_screening_questions
    - job_post_repost_extend_close
    - job_post_successful_submission
    - job_post_validation_and_schemes
    - job_post_view
    - landing_page_first_login
    - suggested_talents
    - talent_search
    - version bump
    - audit
  before_script:
    - VERSION=$(cat .version)
    - DEPLOYED_QA_API_JOB_VERSION=`( cat cypress/*api_job_version.json ) | tr " " "\n" | sort -nr | head -n 1`
    - DEPLOYED_QA_API_PROFILE_VERSION=`( cat cypress/*api_profile_version.json ) | tr " " "\n" | sort -nr | head -n 1`
    - LIGHTHOUSE_SCORES=`( cat ./summary.json ) | jq -r '(["Perf", "Access", "SEO", "Best Prac", "URL"] | (., map(length*"-"))), (.[] | [.detail.performance, .detail.accessibility, .detail.seo, .detail."best-practices", .url]) | @tsv'`
    - LIGHTHOUSE_LATEST_ARTIFACT_URL='https://gitlab.com/mycf.sg/ui-employer/-/jobs/artifacts/master/browse?job=audit'
  script:
    - 'curl -X POST -H "Content-type: application/json" -d "{\"text\":\"new #greenbuild\n*ui-employer : ${VERSION}*\n\nQA API-Job Version:\n${DEPLOYED_QA_API_JOB_VERSION}\nQA API-Profile Version:\n${DEPLOYED_QA_API_PROFILE_VERSION}\n\n\`\`\`${LIGHTHOUSE_SCORES}\`\`\`\n\n<${LIGHTHOUSE_LATEST_ARTIFACT_URL}|Browse latest Lighthouse reports>\"}" ${SLACK_WEBHOOK}'
    - 'curl -X POST -H "Content-type: application/json" -d "{\"text\":\"new #greenbuild\n*ui-employer : ${VERSION}*\n\nQA API-Job Version:\n${DEPLOYED_QA_API_JOB_VERSION}\nQA API-Profile Version:\n${DEPLOYED_QA_API_PROFILE_VERSION}\n\n\`\`\`${LIGHTHOUSE_SCORES}\`\`\`\"}" ${SLACK_WEBHOOK_PO}'
